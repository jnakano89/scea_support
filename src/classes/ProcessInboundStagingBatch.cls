// Class           : ProcessInboundStagingBatch.cls
// Description     : Batch class to process Staging_Service_Inbound__c
// Developer       : (c) 2013 Appirio, Inc. - Shyam Sundar
// Created Date    : 01/06/2014
// Modified        : 05/07/2014 : Leena Mandadapu : SF-587 : Added If condition to Asset.Purchase_date__c value update logic
//                   05/07/2014 : Leena Mandadapu : SF-557 : Added If conditions to check for Null values in the inbound data before updating the Base data.
//                   06/16/2014 : Aaron Briggs : SF-1074 : Excluded Charge Type parsing from the inbound parsing logic.
//                   01/22/2015 : Aaron Briggs : SMS-796 : Unit Shipped - Update Asset Last Repair Date
//                   04/23/2015 : Aaron Briggs : SMS-1228 : Ordered Inbound Staging Processing to Mirror MTC Business Process
//                                                        : Modified Staging Record Update so that those with Duplicate Cases Count as Successfully Processed
//                   05/15/2015 : Aaron Briggs : SMS-1099 : Implemented Exchange Mechanism in Unit-Shipped Process
//                   06/02/2015 : Leena Mandadapu : SMS-1997 : New Asset Exchange process failures fix. Re-structured the code to log the failures in the Staging Object

global class ProcessInboundStagingBatch implements Database.Batchable<Sobject>, ProcessInboundStagingSch.ISchedule {

	public Database.QueryLocator start(Database.BatchableContext BC){

		//AB : 04/23/15 : Modified Query to Include Order By for Ordered Processing
		String query = 'Select Product_SKU__c, Id, Exchanged_Serial_Number__c, Exchanged_SKU__c, ' + 
				'Exchanged_Flag__c, Exchanged_Condition__c, Case_Work_Order__c, Case_POP_Received__c, ' +
				'Case_Walk_In_Flag__c, Case_Status_Date__c, Case_Ship_Method__c, Case_Received_Date__c, Case_Waybill__c,  ' + 
				'Case_Problem_Found__c, Case_Outbound_Tracking_Number__c, Case_Inbound_Tracking_Number__c, Case_Number__c, Case_Promo_Order_Number__c, ' +
				'Case_Date_Purchased__c, Processed_Status__c, Processed_Date_Time__c, Processed_Error_Message__c, Case_Status__c, ' +
				'Case_Charge_Type__c, Case_Box_Shipped_Date__c, Asset_Serial_Number__c From Staging_Service_Inbound__c ' +
				'where Processed_Date_Time__c = null ORDER BY Processed_Order__c ASC ';

		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<Staging_Service_Inbound__c> scope) {

		Set<String> setCaseNumbers = new Set<String>();
		Set<String> setSuccessCases = new Set<String>();
		//AB : 05/15/15 : Create New Sets
		Set<String> setAssetNumbers = new Set<String>();
		//LM : 06/05/15 : set for storing the new assets created during Unit-Shipped exchanged process
		Set<Asset__c> setNewExchangedAssets = new Set<Asset__c>();
		Set<String> mtcexchangedSKUset = new Set<String>();
		map<String,String> exchangedSKUmap = new map<String,String>();
		map<String,String>mapFailedCases = new map<String,String>();
		map<String,String>caseIdStgIdmap = new map<String,String>();
		Set<String> caseFailed = new set<String>();
		map<String,Asset__c> oldNewAssetExchMap = new map<String,Asset__c>();
		map<String,String>mapFailedAssets = new map<String,String>();
		Set<String> assetFailed = new set<String>();
		Set<String> setSuccessAssets = new Set<String>();
		Set<String> setSuccessNewExchAssets = new Set<String>();
		map<String,String> mapFailedNewExchAssets = new map<String,String>();
		Set<String> newExchAssetFailed = new Set<String>();
		
		map<String,String> mapConsumerAssets = new map<String,String>();
		map<String,Order__c> mapNewAssetOrders = new map<String,Order__c>();

		// Collecting Case Numbers
		system.debug('=========================> scope: ' + scope);
		for(Staging_Service_Inbound__c s: scope){
			if(s.Case_Number__c != null && s.Case_Number__c != ''){
				setCaseNumbers.add(s.Case_Number__c);
			}
			if(s.Exchanged_SKU__c != null && s.Exchanged_SKU__c != 'NULL') {
				mtcexchangedSKUset.add(s.Exchanged_SKU__c);
			}
		}

		// Creating map to pair the CaseNumber with Case
		Map<String, Case> mapCase = new Map<String, Case>();
		//AB : 05/15/15 : Created mapAsset and mapConsumerAsset Maps
		Map<String, Asset__c> mapAsset = new Map<String, Asset__c>();
		Map<String, Order__c> mapOrder = new Map<String, Order__c>();

        //LM 05/12/2014 : Added Asset__r.Purchase_Date__c to the SOQL
        //AB : 05/15/15 : Added ContactId to Query
        system.debug('=========================> setCaseNumbers: ' + setCaseNumbers);
		for(Case c : [Select Id, CaseNumber, Work_order_Number__c, ASB_Outbound_Carrier__c, Inbound_Tracking_Number__c, Status, 
		              Problem_Found__c, Proof_of_Purchase__c, Asset__c, Shipped_Date__c, Asset__r.Model_Number__c, Asset__r.Serial_Number__c, Asset__r.Purchase_Date__c,
		              Promo_Order_Number__c, External_Case_Number__c, Walk_In_Flag__c, ContactId
		              from Case where External_Case_Number__c IN :setCaseNumbers]){

			mapCase.put(c.External_Case_Number__c , c);
			setAssetNumbers.add(c.Asset__c);
		}
		
		//AB : 05/15/15 : Insert Case-Asset into mapAsset
		system.debug('=========================> setAssetNumbers: ' + setAssetNumbers);
		for(Asset__c a : [SELECT Serial_Number__c, Genre__c, Sub_Genre__c, Model_Number__c, Model_Type__c, Party_Type__c, Platform__c, Id,Purchase_Date__c, Product__c, 
		                  PPP_Product__c, PPP_Purchase_Date__c, PPP_Contract_Number__c, PPP_Start_Date__c, Asset_Status__c, PPP_End_Date__c,  PPP_Status__c, 
		                  PPP_Claim_Reason__c, PPP_Last_Update_Date__c, PPP_Service_Level__c, Coverage_Type__c, Warranty_Details__c, PPP_POP_Attachment__c, 
		                  PPP_Suspended_Date__c, PPP_Record_Lock__c, PPP_Refund_Eligible__c, ESP_Cancellation_Date__c, Console_ID__c
		                  FROM Asset__c
		                  WHERE Id IN :setAssetNumbers]){
		                  	
		    mapAsset.put(a.Id, a);
		}
		
		//AB : 05/15/15 : Insert Asset-Order into mapOrder
		for(Order__c o : [SELECT Id, Asset__c
		                  FROM Order__c
		                  WHERE Asset__c IN :setAssetNumbers]){
          	
          	mapOrder.put(o.Asset__c, o);
        }
        
        //LM : 06/05/15 : map to store the MTC to Atlas exchanged Model Number mapping
        system.debug('=========================> mtcexchangedSKUset: ' + mtcexchangedSKUset);
        for(Product__c p : [select Id, SKU__c, MTC_Model_Number__c from Product__c where sub_type__c = 'Hardware' and Status__c = 'Active' and MTC_Model_Number__c = :mtcexchangedSKUset limit 1]) {
           exchangedSKUmap.put(p.MTC_Model_Number__c,p.SKU__c);
        }
        
		Map<String, Case> mapSSICase = new Map<String, Case>();
		Map<String, Asset__c> mapSSIAsset = new Map<String, Asset__c>();
		Set<Id> caseIds = new Set<Id>();

		for(Staging_Service_Inbound__c s: scope) {
			System.debug('=========================> mapCase: ' + mapCase);
			System.debug('=========================> s.Case_Number__c: ' + s.Case_Number__c);

			//IF [Staging Record has a Case Number] AND [Staging Case Number matches (External_Case_Number__c) Key in mapCase]
			if(s.Case_Number__c != null && s.Case_Number__c != '' && mapCase.containsKey(s.Case_Number__c)){

				Case cs = mapCase.get(s.Case_Number__c);
			
				if((s.Case_Status__c == 'ASB-Shipped') || (s.Case_Status__c == 'ASB-DoubleBox')){
					System.debug('=========================> s.Case_Status__c: ' + s.Case_Status__c);

					cs.Work_order_Number__c = (s.Case_Work_Order__c <> 'NULL' && s.Case_Work_Order__c <> '' && s.Case_Work_Order__c <> null) ? s.Case_Work_Order__c : cs.Work_order_Number__c;
					cs.ASB_Outbound_Carrier__c = (s.Case_Ship_Method__c <> null && s.Case_Ship_Method__c <> 'NULL' && s.Case_Ship_Method__c <> '') ? s.Case_Ship_Method__c : cs.ASB_Outbound_Carrier__c;
					cs.ASB_Outbound_Tracking_Number__c = (s.Case_Outbound_Tracking_Number__c <> null && s.Case_Outbound_Tracking_Number__c <> 'NULL' && s.Case_Outbound_Tracking_Number__c <> '') ? 
					                                      s.Case_Outbound_Tracking_Number__c : cs.ASB_Outbound_Tracking_Number__c;
					cs.Inbound_Tracking_Number__c = (s.Case_Inbound_Tracking_Number__c <> null && s.Case_Inbound_Tracking_Number__c <> 'NULL' && s.Case_Inbound_Tracking_Number__c <> '') ? 
					                                 s.Case_Inbound_Tracking_Number__c : cs.Inbound_Tracking_Number__c;

					if(s.Case_Status__c == 'ASB-Shipped') {
						//LM 05/08/2014 : Added 's.Case_Box_Shipped_Date__c!='NULL' to below if condition
						if(s.Case_Box_Shipped_Date__c != null && s.Case_Box_Shipped_Date__c != '' && s.Case_Box_Shipped_Date__c != 'NULL'){
						  System.Debug('=========================> ValidationRulesUtility.parseDateTimeGMT(datetimeGMT): ' + ValidationRulesUtility.parseDateTimeMMDDYYY(s.Case_Box_Shipped_Date__c));
        				  
        				  cs.ASB_First_Sent_Date__c = ValidationRulesUtility.parseDateTimeMMDDYYY(s.Case_Box_Shipped_Date__c);
						}
					}
				}
				else if(s.Case_Status__c == 'Fulfillment-Shipped'){
					System.debug('=========================> Fulfillment-Shipped: ' + s);

					//LM 05/08/2014 : Added If conditions to the below statements
					cs.Promo_Order_Number__c = (s.Case_Promo_Order_Number__c <> null && s.Case_Promo_Order_Number__c <> 'NULL' && s.Case_Promo_Order_Number__c <> '') ? 
					                            s.Case_Promo_Order_Number__c : cs.Promo_Order_Number__c; 
					cs.Work_order_Number__c =  (s.Case_Work_Order__c <> null && s.Case_Work_Order__c <> 'NULL' && s.Case_Work_Order__c <> '') ? s.Case_Work_Order__c : cs.Work_order_Number__c;
					cs.Outbound_Tracking_Number__c = (s.Case_Waybill__c <> null && s.Case_Waybill__c <> 'NULL' && s.Case_Waybill__c <> '') ? s.Case_Waybill__c : cs.Outbound_Tracking_Number__c;
				}
				else if(s.Case_Status__c == 'Unit-Completed'){
					System.debug('=========================> Unit-Completed: ' + s);

					//LM 05/08/2014 : Added If conditions to the below statements
					cs.Work_order_Number__c = (s.Case_Work_Order__c <> null && s.Case_Work_Order__c <> 'NULL' && s.Case_Work_Order__c <> '') ? s.Case_Work_Order__c : cs.Work_order_Number__c;
					cs.Problem_Found__c     = (s.Case_Problem_Found__c <> null && s.Case_Problem_Found__c <> 'NULL' && s.Case_Problem_Found__c <> '') ? s.Case_Problem_Found__c : cs.Problem_Found__c;
					cs.Service_Date__c      = (s.Case_Status_Date__c <> null && s.Case_Status_Date__c <> 'NULL' && s.Case_Status_Date__c <> '') ? 
					                           ValidationRulesUtility.parseDateTime(s.Case_Status_Date__c) : cs.Service_Date__c;
					cs.Status = 'Ship Ready';
				}
				else if(s.Case_Status__c == 'Unit-Shipped'){
					System.debug('=========================> Unit-Shipped: ' + s);

					cs.Work_order_Number__c = (s.Case_Work_Order__c <> null && s.Case_Work_Order__c <> 'NULL' && s.Case_Work_Order__c <> '') ? s.Case_Work_Order__c : cs.Work_order_Number__c;
					cs.SKU__c = (s.Product_SKU__c <> null && s.Product_SKU__c <> 'NULL' && s.Product_SKU__c <> '') ? s.Product_SKU__c : cs.SKU__c;
					cs.Outbound_Carrier__c = (s.Case_Ship_Method__c <> null && s.Case_Ship_Method__c <> 'NULL' && s.Case_Ship_Method__c <> '') ? s.Case_Ship_Method__c : cs.Outbound_Carrier__c;
					
                    try {
                        if(s.Case_Box_Shipped_Date__c!=null && s.Case_Box_Shipped_Date__c!='' && s.Case_Box_Shipped_Date__c != 'NULL') {
                            cs.Shipped_Date__c = ValidationRulesUtility.parseDateTimeMMDDYYY(s.Case_Box_Shipped_Date__c).date();
						}
					} catch(Exception ex) {continue;}
					try {
						if(s.Case_Walk_In_Flag__c != null) {
							cs.Walk_In_Flag__c = Boolean.valueOf(s.Case_Walk_In_Flag__c);
						}
					} catch(Exception ex) {continue;}
					try {
						if(s.Case_POP_Received__c != null) {
							cs.Proof_of_Purchase__c = Boolean.valueOf(s.Case_POP_Received__c);
						}
					}catch(Exception ex) {continue;}

					if(cs.Asset__c != null) {
						Asset__c asset = mapSSIAsset.get(s.Id);
						Asset__c newAsset;
						Order__c existingOrder;

						if(asset == null) {
							asset = new Asset__c(id = cs.Asset__c);
							mapSSIAsset.put(s.Id, asset);
						}

						if(s.Exchanged_Flag__c <> null && s.Exchanged_Flag__c.toUpperCase() == 'FALSE') {
							cs.Service_Type__c = 'Same Unit Repair';
							//LM 05/07/2014 - Added if condition t update the Purchase_Date__c only if inbound Case_Date_Purchase__c is not NULL
                            asset.Purchase_Date__c = (s.Case_Date_Purchased__c <> null && s.Case_Date_Purchased__c <> 'NULL' && s.Case_Date_Purchased__c <> '') ? 
                                                      ValidationRulesUtility.parseDateTime(s.Case_Date_Purchased__c) : cs.Asset__r.Purchase_Date__c;
                            //AB : 01/22/2015 : Added condition to update the Last Repair Date on the asset if the inbound Box Shipped Date it is not NULL
                            asset.Last_Repair_Date__c = (s.Case_Box_Shipped_Date__c <> null && s.Case_Box_Shipped_Date__c <> 'NULL' && s.Case_Box_Shipped_Date__c <> '') ?
                                                         ValidationRulesUtility.parseDateTime(s.Case_Box_Shipped_Date__c) : cs.Asset__r.Last_Repair_Date__c;
						}
						else {
							if(s.Exchanged_Condition__c == 'RFB') {cs.Service_Type__c = 'Recertified Exchange';}
                            else if(s.Exchanged_Condition__c == 'NEW') {cs.Service_Type__c = 'A Stock Replacement';}
                            
							//LM 05/08/2014 : Added If conditions to the below statements
							cs.Leg_3_Model__c = (s.Exchanged_SKU__c <> null && s.Exchanged_SKU__c <> 'NULL' && s.Exchanged_SKU__c <> '') ? 
							                    (s.Exchanged_SKU__c <> null ? s.Exchanged_SKU__c : cs.Leg_3_Model__c) : cs.Leg_3_Model__c;
							cs.Leg_3_Serial__c = (s.Exchanged_Serial_Number__c <> null && s.Exchanged_Serial_Number__c <> 'NULL' && s.Exchanged_Serial_Number__c <> '') ? s.Exchanged_Serial_Number__c : cs.Leg_3_Serial__c;
							
							//AB : 05/14/15 : Added Exchange Asset Logic
							newAsset = mapAsset.get(cs.Asset__c).clone(false,true);
							newAsset.Asset_Status__c ='Active';
							//LM 06/06 : Added to correct the wrong model numbers issues during new Asset insert process.
							newAsset.Model_Number__c = exchangedSKUmap.get(s.Exchanged_SKU__c) <> null ? exchangedSKUmap.get(s.Exchanged_SKU__c) : cs.Asset__r.Model_Number__c;
							newAsset.Serial_Number__c = cs.Leg_3_Serial__c;
							newAsset.Last_Repair_Date__c = (s.Case_Box_Shipped_Date__c <> null && s.Case_Box_Shipped_Date__c <> 'NULL' && s.Case_Box_Shipped_Date__c <> '') ?
                                                         ValidationRulesUtility.parseDateTime(s.Case_Box_Shipped_Date__c) : cs.Asset__r.Last_Repair_Date__c;
                            system.debug('=========================> newAsset.Model_Number__c: ' + newAsset.Model_Number__c);
							//insert newAsset;
							setNewExchangedAssets.add(newAsset);
							oldNewAssetExchMap.put(asset.Id,newAsset);
							system.debug('=========================> oldNewAssetExchMap: ' + oldNewAssetExchMap);
						    system.debug('=========================> setNewExchangedAssets: ' + setNewExchangedAssets);

							//AB : 05/14/15 : Create Junction Reference to newAsset
							//Consumer_Asset__c ca = new Consumer_Asset__c();
							//ca.Consumer__c = cs.ContactId;
							//LM : Commented because newAsset.Id will be blank here as New asset will be inserted later in the proces
							//ca.Asset__c = newAsset.Id;
							mapConsumerAssets.put(asset.Id,cs.ContactId);
							system.debug('=========================> mapConsumerAssets: ' + mapConsumerAssets);
							//AB : 05/14/15 : Inactivate Old Asset
							asset.PPP_Status__c = 'Inactive';
                            asset.Asset_Status__c = 'Inactive';
                            //AB : 05/14/15 : Update Asset Reference on Original PPP Order
                            try{
                        	   if(mapOrder.size() > 0){
		                            existingOrder = mapOrder.get(asset.Id);
		                            //LM : 06/06/15 : Commented the below statement as newAssetId will be null since we are inserting the newAsset at the end of the process.
		                            //existingOrder.Asset__c = newAsset.Id;
		                            mapNewAssetOrders.put(asset.Id,existingOrder);
                        	   }
                            }catch(Exception ex){continue;}
						}
						
						cs.Status = 'Ship Complete';
						//LM 05/08/2014 : Added If condition to the below statement
						cs.Outbound_Tracking_Number__c = (s.Case_Waybill__c <> 'NULL' && s.Case_Waybill__c <> '' && s.Case_Waybill__c <> null) ? s.Case_Waybill__c : cs.Outbound_Tracking_Number__c ;
					}
				}
				else if(s.Case_Status__c == 'Unit-Received'){
					System.debug('=========================> Unit-Received: ' + s);

					//LM 05/08/2014 : Added If condition to the below statement
					cs.Work_order_Number__c = (s.Case_Work_Order__c <> null && s.Case_Work_Order__c <> 'NULL' && s.Case_Work_Order__c <> '') ? s.Case_Work_Order__c : cs.Work_order_Number__c;
					cs.Status = 'Received';
					//LM 05/08/2014 : Added If condition to the below statement
					cs.Console_Serial_received__c = (s.Asset_Serial_Number__c <> null && s.Asset_Serial_Number__c <> 'NULL' && s.Asset_Serial_Number__c <> '') ? s.Asset_Serial_Number__c : cs.Console_Serial_received__c;
					cs.Console_Model_Received__c =  (s.Product_SKU__c <> null && s.Product_SKU__c <> 'NULL' && s.Product_SKU__c <> '') ? s.Product_SKU__c : cs.Console_Model_Received__c;
					try{
						if(s.Case_Walk_In_Flag__c != null){
							cs.Walk_In_Flag__c = Boolean.valueOf(s.Case_Walk_In_Flag__c);
						}
					}catch(Exception ex){continue;}
					try{
						//LM 05/08/2014 : Added 's.Case_Received_Date__c != null' and 's.Case_Received_Date__c != null' conditions to the below if statement
						if(s.Case_Received_Date__c != null && s.Case_Received_Date__c != 'NULL' && s.Case_Received_Date__c != ''){
							cs.Received_Date__c = ValidationRulesUtility.parseDateTime(s.Case_Received_Date__c);
						}
					}catch(Exception ex){continue;}

					//LM 05/08/2014 : added conditions to the the Model Number and Serial Number fields below
					if(cs.Asset__c != null){
						mapSSIAsset.put(s.Id, new Asset__c(Id = cs.Asset__c, 
					 	                Model_Number__c = (s.Exchanged_SKU__c <> null && s.Exchanged_SKU__c <> 'NULL' && s.Exchanged_SKU__c <> '') ? s.Exchanged_SKU__c : cs.Asset__r.Model_Number__c ,
						                Serial_Number__c = (s.Exchanged_Serial_Number__c <> null && s.Exchanged_Serial_Number__c <> 'NULL' && s.Exchanged_Serial_Number__c <> '') ? s.Exchanged_Serial_Number__c : cs.Asset__r.Serial_Number__c));
					}

				}
				System.debug('=========================> Case to Update: ' + cs);
				System.debug('=========================> Staging Record to Update: ' + s);
				
				if(!caseIds.contains(cs.Id)) {
					mapSSICase.put(s.Id, cs);
					caseIds.add(cs.Id);	
					//LM : 06/06/15 : to process the Asset updates down in the process
					caseIdStgIdmap.put(cs.Id,s.Id);
				}
			}
		}//LM end for
		
		// LM : 06/06/15 : Update Cases and compile the update failures in a map for setting the Process Error field in Staging Object
		system.debug('=========================> mapSSICase: ' + mapSSICase);
		system.debug('mapSSICase.keySet().size(): ' + mapSSICase.keySet().size());

		if(mapSSICase.keySet().size() > 0){

      	   Database.SaveResult[] srList = Database.update(mapSSICase.values(), false);
		   system.debug('=========================> srList: ' + srList);
				
		   List<Case> c = new List<Case>();
		   for(Case cRec : mapSSICase.values()) {
				c.add(cRec);	
		   }

		   for(integer i=0; i<c.size(); i++) {
			   system.debug('=========================> srList: ' + srList);
			   Database.SaveResult sr = srList[i];
			   Case cinDML = c[i];
               system.debug('=========================> sr: ' + sr);
			   if (sr.isSuccess()) {
				  system.debug('=========================> Record Successfully Updated: ' + sr);
				  setSuccessCases.add(sr.getId());	
			   } else {
				  system.debug('=========================> Record Unsuccessfully Updated: ' + sr.getErrors());
				  system.debug('=========================> cinDML: ' + cinDML.Id);
				  //LM 06/06/15 : Added to capture the failed cases in a map
				  for(Database.Error err : sr.getErrors()) {
					 // System.debug('=========================> Failed Cases : ' + sr.getId());
					  mapFailedCases.put(cinDML.Id,err.getMessage()+err.getFields());
					  caseFailed.add(cinDML.Id);
				   }
			   }
		   }
		}   
		    
		//LM : 06/06/15 : if there are no successfully updated cases then don't update related objects
		if(setSuccessCases.size() > 0){
           // Updating Assets
		   system.debug('=========================> mapSSIAsset: ' + mapSSIAsset);
           system.debug('mapSSIAsset.keySet().size(): ' + mapSSIAsset.keySet().size());
           system.debug('=========================> mapFailedCases: ' + mapFailedCases);
           Map<Id, Asset__c> AssetMap = new Map<Id, Asset__c>();
            
		   if(mapSSIAsset.keySet().size() > 0){	
			  //LM 06/06/15 : added to remove the failed transactions from the map.
			  if(mapFailedCases <> null && mapFailedCases.size() > 0) {
				 system.debug('=========================> mapFailedCases.size(): ' + mapFailedCases.size());
				 system.debug('=========================> caseIdStgIdmap: ' + caseIdStgIdmap);
				 system.debug('=========================> caseFailed: ' + caseFailed);

				 if(caseIdStgIdmap <> null && caseIdStgIdmap.keySet().size() > 0 && caseFailed.size() > 0) {
				  	system.debug('=========================> caseFailed.size(): ' + caseFailed.size());
				    for(String cId : caseFailed) {
				        mapSSIAsset.keySet().remove(caseIdStgIdmap.get(cId));
				    }  
			     }    
		      } 
		   }  
			    
		   system.debug('=========================> mapSSIAsset: ' + mapSSIAsset);
           system.debug('mapSSIAsset.keySet().size(): ' + mapSSIAsset.keySet().size());
			    
		   for(Asset__c asset: mapSSIAsset.values()){
			   system.debug('=========================> asset: ' + asset);
			   AssetMap.put(asset.Id, Asset);    
		    }

			Database.SaveResult[] srList1 = Database.update(AssetMap.values(), false);
            system.debug('=========================> srList1: ' + srList1);
                
            List<Asset__c> a = new List<Asset__c>();
			for(Asset__c aRec : AssetMap.values()) {
				a.add(aRec);	
			}

			for(integer i=0; i<a.size(); i++) {
				system.debug('=========================> srList1: ' + srList1);
				Database.SaveResult sr = srList1[i];
				Asset__c ainDML = a[i];
                system.debug('=========================> sr: ' + sr);
				if (sr.isSuccess()) {
					system.debug('=========================> Record Successfully Updated: ' + sr);
					setSuccessAssets.add(sr.getId());	
				} else {
				    system.debug('=========================> Record Unsuccessfully Updated: ' + sr.getErrors());
					system.debug('=========================> ainDML: ' + ainDML.Id);
					//LM 06/06/15 : Added to capture the failed cases in a map
					for(Database.Error err : sr.getErrors()){
					    mapFailedAssets.put(ainDML.Id,err.getMessage()+err.getFields());
					    assetFailed.add(ainDML.Id);
					}
				}
			  }
		}
			
			//LM : 06/06/15 : If there are no successfully updated Assets then no need to update the related objects
			if(setSuccessAssets.size() > 0) {
			
			//LM : 06/06/2015 : Added New Exchanged Asset Insert logic
			if(setNewExchangedAssets <> null && setNewExchangedAssets.size() > 0){	
				List<Asset__c> newExchAssets = new List<Asset__c>();
				Database.SaveResult[] srList2;
			    
			    System.debug('=========================> setNewExchangedAssets: ' + setNewExchangedAssets); 
			    if(setNewExchangedAssets <> null && setNewExchangedAssets.size() > 0){ 
			       newExchAssets.addAll(setNewExchangedAssets);
			       srList2 = Database.insert(newExchAssets, false);
                   System.debug('=========================> srList2: ' + srList2);
			    }
			    
			    System.debug('=========================> newExchAssets: ' + newExchAssets);
			    for(integer i=0; i<newExchAssets.size(); i++) {
					Database.SaveResult sr = srList2[i];
					Asset__c ainDML = newExchAssets[i];
                    System.debug('=========================> sr: ' + sr);
					if (sr.isSuccess()) {
						System.debug('=========================> Record Successfully Updated: ' + sr);
						setSuccessNewExchAssets.add(sr.getId());	
					} else {
					    System.debug('=========================> Record Unsuccessfully Updated: ' + sr.getErrors());
					    System.debug('=========================> ainDML: ' + ainDML.Id);
					    //LM 06/06/15 : Added to capture the failed cases in a map
					    for(Database.Error err : sr.getErrors()){
					       mapFailedNewExchAssets.put(ainDML.Id,err.getMessage()+err.getFields());
					       newExchAssetFailed.add(ainDML.Id);
					    }
					}
				}
			}    
			
			//LM : 06/06/15 : Insert Consumer-Asset and Update Order to Asset reference for the newly created Exchanged Assets
			
			if(setSuccessNewExchAssets <> null && setSuccessNewExchAssets.size() > 0) {
			   List<Consumer_Asset__c> caList = new List<Consumer_Asset__c>();
			   Consumer_Asset__c conAst = new Consumer_Asset__c();
			   set<String> oldAssetIdsSet = new set<String>();
			   set<String> existingorderIdsSet = new set<String>();
			   
			   if(mapConsumerAssets.keySet().size() > 0) {
			   	  oldAssetIdsSet = mapConsumerAssets.keySet();
			   }
			   
			   if(mapNewAssetOrders.keySet().size() > 0) {
			   	  existingorderIdsSet = mapNewAssetOrders.keySet();
			   }
			   
			   system.debug('=========================> setSuccessNewExchAssets: ' + setSuccessNewExchAssets);
			   system.debug('=========================> mapConsumerAssets: ' + mapConsumerAssets);
			   system.debug('=========================> oldAssetIdsSet: ' + oldAssetIdsSet);
			   
			   for(string OldAstId : oldAssetIdsSet) {
			   	   system.debug('=========================> OldAstId: ' + OldAstId);
			   	   for(String newAstId : setSuccessNewExchAssets){
			   	   	   system.debug('=========================> newAstId: ' + newAstId);
			   	   	   if(oldNewAssetExchMap.get(OldAstId).Id == newAstId) {
			   	   	   	  conAst.Asset__c = newAstId;
			   	   	   	  conAst.Consumer__c = mapConsumerAssets.get(OldAstId);
			   	   	   	  caList.add(conAst);
			   	   	   }
			   	   }
			   }
			   
			   System.debug('=========================> caList: ' + caList);

               if(caList <> null && caList.size() > 0){
            	  Database.SaveResult[] caseAssetSaveResult = Database.insert(caList, false);
            	  System.debug('=========================> caseAssetSaveResult: ' + caseAssetSaveResult);
               }

               System.debug('=========================> mapNewAssetOrders: ' + mapNewAssetOrders);
               System.debug('mapNewAssetOrders.size(): ' + mapNewAssetOrders.size());
               List<Order__c> ordLst = new List<Order__c>();
               
               for(string OldAstId : existingorderIdsSet) {
			   	   system.debug('=========================> OldAstId: ' + OldAstId);
			   	   for(String newAstId : setSuccessNewExchAssets){
			   	   	   system.debug('=========================> newAstId: ' + newAstId);
			   	   	   if(oldNewAssetExchMap.get(OldAstId).Id == newAstId) {
			   	   	   	  Order__c ord = new Order__c(Id=mapNewAssetOrders.get(OldAstId).Id,Asset__c=newAstId);
			   	   	   	  system.debug('=========================> ord: ' + ord); 
			   	   	   	  if(ord <> null) {
			   	   	   	  	 ordLst.add(ord);
			   	   	   	  }
			   	   	   }
			   	   }
			   }
			   System.debug('=========================> ordLst: ' + ordLst);
               if(ordLst <> null && ordLst.size() > 0){
            	  Database.SaveResult[] orderSaveResult = Database.update(ordLst, false);
            	  System.debug('=========================> orderSaveResult: ' + orderSaveResult);
               }
			}
		  }  

		// Setting processed status of Staging_Service_Inbound__c object
		List<Staging_Service_Inbound__c> lstSSI = new List<Staging_Service_Inbound__c>();
        system.debug('=========================> scope:' + scope);
		for(Staging_Service_Inbound__c s: scope) {
			System.debug('=========================> s:' + s);
			
			s.Processed_Status__c = '';
			s.Processed_Date_Time__c = System.now();
			s.Processed_Error_Message__c = '';

			system.debug('=========================> setCaseNumbers: ' + setCaseNumbers);
			system.debug('=========================> s.Id: ' + s.Id);
			system.debug('=========================> mapSSICase: ' + mapSSICase);
			system.debug('=========================> mapSSICase.containsKey(s.Id): ' + mapSSICase.containsKey(s.Id));
			system.debug('=========================> mapFailedCases: ' + mapFailedCases);
			system.debug('=========================> setSuccessCases: ' + setSuccessCases);
			system.debug('=========================> mapFailedAssets: ' + mapFailedAssets);
			system.debug('=========================> mapFailedNewExchAssets: ' + mapFailedNewExchAssets);
			system.debug('=========================> oldNewAssetExchMap: ' + oldNewAssetExchMap);
			system.debug('=========================> setSuccessNewExchAssets: ' + setSuccessNewExchAssets);
			
		    //AB : 04/23/15 : Modified Logic to Allow Staging Records with Duplicate Cases to be Updated as Successes
		    Case caseObj = mapCase.get(s.Case_Number__c);
		    System.debug('=========================> caseObj: ' + caseObj);
		    
		    //LM : 06/06/15 : updated logic to set the failure status with failure messages.
			if(caseObj != null){

				//LM : Added for setting the error message on failed records
				if(mapFailedCases <> null && mapFailedCases.containsKey(caseObj.id)) {
					s.Processed_Error_Message__c = mapFailedCases.get(caseObj.id);
					s.Processed_Status__c = 'Fail';
				}
				if(mapFailedAssets.keySet().size() > 0 && mapFailedAssets.containsKey(caseObj.Asset__c)) {
					s.Processed_Error_Message__c =+ mapFailedAssets.get(caseObj.Asset__c);
					s.Processed_Status__c = 'Fail';
				}
				if(s.Case_Status__c == 'Unit-Shipped' && mapFailedNewExchAssets.keySet().size() > 0 && 
				   oldNewAssetExchMap.get(caseObj.Asset__c) <> null &&
				   mapFailedNewExchAssets.containsKey(oldNewAssetExchMap.get(caseObj.Asset__c).Id)){
					s.Processed_Error_Message__c =+ mapFailedNewExchAssets.get(oldNewAssetExchMap.get(caseObj.Asset__c).Id);
					s.Processed_Status__c = 'Fail';
				}
				
				system.debug('=========================> setSuccessCases.contains(caseObj.id): ' + setSuccessCases.contains(caseObj.id));
			    system.debug('=========================> setSuccessAssets.contains(caseObj.Asset__c): ' + setSuccessAssets.contains(caseObj.Asset__c));
			    system.debug('=========================> oldNewAssetExchMap.get(caseObj.Asset__c).Id: ' + oldNewAssetExchMap.get(caseObj.Asset__c));
			    				
				if(setSuccessCases.contains(caseObj.id) && setSuccessAssets.contains(caseObj.Asset__c) && s.Processed_Status__c == ''){
				    s.Processed_Status__c = 'Success';
				}
				
				if(s.Case_Status__c == 'Unit-Shipped' && setSuccessCases.contains(caseObj.id) &&
				   setSuccessNewExchAssets.size() > 0 && oldNewAssetExchMap.get(caseObj.Asset__c) <> null &&
				   setSuccessNewExchAssets.contains(oldNewAssetExchMap.get(caseObj.Asset__c).Id)) {
				    s.Processed_Status__c = 'Success';
				}
				
				if(s.Case_Status__c <> 'Unit-Shipped' && s.Case_Status__c <> 'Unit-Received' && setSuccessCases.contains(caseObj.id)) {
					s.Processed_Status__c = 'Success';
				}
				
			} else {
			       s.Processed_Status__c = 'Fail';
			       s.Processed_Error_Message__c =+ 'Case not Found in the Map.';
			}			
			
			lstSSI.add(s);
		} 

		System.debug('=========================> lstSSI: ' + lstSSI);
		System.debug('=========================> lstSSI.size(): ' + lstSSI.size());

		// Updating status of Staging_Service_Inbound__c
		if(lstSSI.size() > 0){
			Database.update(lstSSI, false);
		}
	}

	global void execute(SchedulableContext SC) {
		DataBase.executeBatch(this, 5);
	}

	global void finish(Database.BatchableContext BC) {

	}

	private String mapFeeTypeValues(String inboundValue) {

		if(!String.IsEmpty(inboundValue)) {
			if('W'.equalsignoreCase(inboundValue)){
				return 'In Warranty';
			}
			if('O'.equalsignoreCase(inboundValue)){
				return 'Out Of Warranty';
			}
		}
		return '';
	} 

}