/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SocialPostTest {

    static testMethod void socialPostUnitTest() {
        // TO DO: implement unit test
        String userScsName = 'SCS Integrator';
        list<User> scsIntUser = [Select  id from User  where name =:userScsName order by LastModifiedDate ASC Limit 1];
        Contact con = new Contact(LastName = 'lname', FirstName = 'fname');
        insert con; 
        SocialPersona sper = new SocialPersona(ParentId = con.Id, Provider = 'Twitter', Name = 'random1');
        insert sper;       
        Case acCase = new Case();        
        SocialPost sp = new SocialPost(Content = 'TESTSOCIALPOST', 
        	Handle = 'TESTDigitlSuperStar',
        	PersonaId = sper.Id,
        	Headline = 'TEST TWEET FROM: DigitlSuperStar',
	        MediaProvider = 'TWITTER',
	        MediaType = 'Twitter',
	        MessageType = 'Reply', //twitter
	        Name = 'TEST TWEET FROM: DigitlSuperStar',
	        PostPriority = 'High',
	        PostTags = 'Help,Question',
	        Provider = 'Twitter',
	        TopicProfileName = '@PlayStation',
	        OwnerId = scsIntUser.get(0).id);			                                       
        insert sp;        
        sp = [select Id, Name, ParentId, PostTags from SocialPost where Id = :sp.Id];
        System.assertEquals(sp.Name, 'TEST TWEET FROM: DigitlSuperStar');
        Contact con1 = new Contact(LastName = 'lname1', FirstName = 'fname1');
        insert con1; 
        SocialPersona sper1 = new SocialPersona(ParentId = con.Id, Provider = 'Twitter', Name = 'random11');
        insert sper1;
        SocialPost sp1 = new SocialPost(Content = 'TESTSOCIALPOST', 
        	Handle = 'TESTDigitlSuperStar',
        	PersonaId = sper1.Id,
        	Headline = 'TEST TWEET FROM: DigitlSuperStar',
	        MediaProvider = 'TWITTER',
	        MediaType = 'Twitter',
	        MessageType = 'Reply', //twitter
	        Name = 'TEST TWEET FROM: DigitlSuperStar',
	        PostPriority = 'High',
	        PostTags = 'Help,Question',
	        Provider = 'Twitter',
	        TopicProfileName = '@AskPlayStation',
	        OwnerId = scsIntUser.get(0).id);			                                       
        insert sp1;  
        sp1 = [select Id, Name, ParentId, PostTags from SocialPost where Id = :sp1.Id];
        System.assertEquals(sp1.Name, 'TEST TWEET FROM: DigitlSuperStar');  
        if(String.isNotBlank(sp1.ParentId)){
        	acCase = [select subject from Case where id = :sp.ParentId];
        	//System.assertEquals(acCase.subject, sp.PostTags);	
        } 
        Account acc = new Account(LastName='lname9', FirstName='fname9');
        insert acc;        
        String returnString = SocialPersonaController.checkSocialPersona('random11', acc.Id);  
        System.assertNotEquals(returnString, 'Nothing');     
        String returnString1 = SocialPersonaController.checkSocialPersona('random11', acc.Id);
        System.assertNotEquals(returnString1, 'Nothing');
    }        
}