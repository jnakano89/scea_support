@isTest
private class UtilityGenericWebCallout_Test {

    static testMethod void myUnitTest() {
       User usr = [select OAuthAccessToken__c, CompanyName from User where Id = :Userinfo.getUserId()];
       usr.OAuthAccessToken__c = '123123';	
       usr.CompanyName = 'SCEA';
       update usr;
       
       String oauthToken = UtilityGenericWebCallout.getOAuthToken();
       System.assertEquals(oauthToken, '123123', 'Auth Token should be same');
       
       Web_Call_Setting__c webSetting = UtilityGenericWebCallout.createWebCallSetting(true, 'Test Settings');
       UtilityGenericWebCallout.createWebCallSetting(false, 'Test Settings');
       
       UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'intuit_offeringId',
         'Key_Location__c' => 'Header',
         'Setting_Link__c' => null,
         'Value__c'=> 'Intuit.wcg.turbotax.salesforce.support',
         'Web_Call_Setting__c' => webSetting.Id
        });
         UtilityGenericWebCallout.createWebCallSettingKey(true, new map<string, object>{
        'Name'=> 'intuit_offeringId2',
         'Key_Location__c' => 'Header',
         'Setting_Link__c' => null,
         'Value__c'=> 'Intuit.wcg.turbotax.salesforce.support',
         'Web_Call_Setting__c' => webSetting.Id
        });
        
        
        Date todayDate = Date.today().addMonths(2);
        
        Boolean isCardExpired = UtilityGenericWebCallout.IsCreditCardExpire(String.valueOf(todayDate.year()), String.valueOf(todayDate.month()));
        
        system.assert(!isCardExpired, 'Card should not be expired');
        
       
    }
}