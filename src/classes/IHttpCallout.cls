/**
  Contract for a simple web service callout using Apex.
  Only a single method is available for abstracting the stuff out for ease of Testing.
  Test classes can provide implmentations of this interface to return custom/fake/mock HTTP responses.
*/
public interface IHttpCallout {
  /**
    Accepts a ready to send requests and makes a callout using that.
  */
  IHttpResponse send(HttpRequest req);  
}