//Generated by wsdl2apex
/**
This class is generated from final wsdl for Account Asset. This class has new parameter in service call "Source" which sould be "SFDC"
when a callout is made from this class.
Updates:
  9/20/2013 : Urminder(JDC) : added OSB__c Custom Setting to get EndPoint Url.
//

*/
public class Sony_MiddlewareAssetdata {
	
    public class ErrorType {
        public String ErrorCode;
        public String ErrorMessage;
        public String Description;
        private String[] ErrorCode_type_info = new String[]{'ErrorCode','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] ErrorMessage_type_info = new String[]{'ErrorMessage','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Description_type_info = new String[]{'Description','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/assetdata/','true','false'};
        private String[] field_order_type_info = new String[]{'ErrorCode','ErrorMessage','Description'};
    }
    public class PSConsumerAccountAssets_Output_element {
        public Sony_MiddlewareAssetData.ListOfAccount ListOfAccount;
        public Sony_MiddlewareAssetData.ErrorType ErrorMessage;
        public String TextAttribute1;
        public String TextAttribute2;
        public String TextAttribute3;
        private String[] ListOfAccount_type_info = new String[]{'ListOfAccount','http://xmlns.sony.com/middleware/assetdata/','ListOfAccount','1','1','false'};
        private String[] ErrorMessage_type_info = new String[]{'ErrorMessage','http://xmlns.sony.com/middleware/assetdata/','ErrorType','1','1','false'};
        private String[] TextAttribute1_type_info = new String[]{'TextAttribute1','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TextAttribute2_type_info = new String[]{'TextAttribute2','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TextAttribute3_type_info = new String[]{'TextAttribute3','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/assetdata/','true','false'};
        private String[] field_order_type_info = new String[]{'ListOfAccount','ErrorMessage','TextAttribute1','TextAttribute2','TextAttribute3'};
    }
    public class Account {
        public String MDMAccountRowId;
        public Sony_MiddlewareAssetData.ListOfAsset ListOfAsset;
        private String[] MDMAccountRowId_type_info = new String[]{'MDMAccountRowId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] ListOfAsset_type_info = new String[]{'ListOfAsset','http://xmlns.sony.com/middleware/assetdata/','ListOfAsset','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/assetdata/','true','false'};
        private String[] field_order_type_info = new String[]{'MDMAccountRowId','ListOfAsset'};
    }
    public class Asset {
        public String ConsoleType;
        public String MDMAssetRowId;
        public String AssetGroup;
        public String ConsoleID;
        public String PurchaseDate;
        public String RegistrationDate;
        public String Out_of_BoxWarrantyDate;
        public String ConsoleDescription;
        public String ModelNumber;
        public String SerialNumber;
        public String SoftwareTitleID;
        public String SoftwareTitleDesc;
        public String Publisher;
        public String PartyType;
        public String GameType;
        public String Platform;
        public String ReleaseDate;
        public String Genre;
        public String Franchise;
        public String PublisherName;
        public String TitleName;
        public String ProductID;
        public String ProductName;
        public String In_GamePurchaseFlag;
        public String ContentType;
        public String MoveCompatible;
        public String Flag3DAsset;
        public String ESRBRating;
        public String Sub_Genre;
        private String[] ConsoleType_type_info = new String[]{'ConsoleType','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] MDMAssetRowId_type_info = new String[]{'MDMAssetRowId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] AssetGroup_type_info = new String[]{'AssetGroup','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] ConsoleID_type_info = new String[]{'ConsoleID','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] PurchaseDate_type_info = new String[]{'PurchaseDate','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] RegistrationDate_type_info = new String[]{'RegistrationDate','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Out_of_BoxWarrantyDate_type_info = new String[]{'Out-of-BoxWarrantyDate','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] ConsoleDescription_type_info = new String[]{'ConsoleDescription','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] ModelNumber_type_info = new String[]{'ModelNumber','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] SerialNumber_type_info = new String[]{'SerialNumber','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] SoftwareTitleID_type_info = new String[]{'SoftwareTitleID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] SoftwareTitleDesc_type_info = new String[]{'SoftwareTitleDesc','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Publisher_type_info = new String[]{'Publisher','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] PartyType_type_info = new String[]{'PartyType','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] GameType_type_info = new String[]{'GameType','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Platform_type_info = new String[]{'Platform','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] ReleaseDate_type_info = new String[]{'ReleaseDate','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Genre_type_info = new String[]{'Genre','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Franchise_type_info = new String[]{'Franchise','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] PublisherName_type_info = new String[]{'PublisherName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] TitleName_type_info = new String[]{'TitleName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] ProductID_type_info = new String[]{'ProductID','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] ProductName_type_info = new String[]{'ProductName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] In_GamePurchaseFlag_type_info = new String[]{'In-GamePurchaseFlag','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] ContentType_type_info = new String[]{'ContentType','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] MoveCompatible_type_info = new String[]{'MoveCompatible','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Flag3DAsset_type_info = new String[]{'Flag3DAsset','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] ESRBRating_type_info = new String[]{'ESRBRating','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Sub_Genre_type_info = new String[]{'Sub-Genre','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/assetdata/','true','false'};
        private String[] field_order_type_info = new String[]{'ConsoleType','MDMAssetRowId','AssetGroup','ConsoleID','PurchaseDate','RegistrationDate','Out_of_BoxWarrantyDate','ConsoleDescription','ModelNumber','SerialNumber','SoftwareTitleID','SoftwareTitleDesc','Publisher','PartyType','GameType','Platform','ReleaseDate','Genre','Franchise','PublisherName','TitleName','ProductID','ProductName','In_GamePurchaseFlag','ContentType','MoveCompatible','Flag3DAsset','ESRBRating','Sub_Genre'};
    }
    public class ListOfAccount {
        public Sony_MiddlewareAssetData.Account[] Account;
        private String[] Account_type_info = new String[]{'Account','http://xmlns.sony.com/middleware/assetdata/','Account','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/assetdata/','true','false'};
        private String[] field_order_type_info = new String[]{'Account'};
    }
    public class ConsumerAccountAssetsQSPort {
    	OSB__c settings = OSB__c.getOrgDefaults();
    	public String endpoint_x = settings <> null ? settings.URL_for_Asset_Lookup__c : '' ;
        //public String endpoint_x = 'https://devosb.scea.com:443/SONYConsumerAccountAssets/ProxyService/PSConsumerAccountAssets';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x = 120000;
        private String[] ns_map_type_info = new String[]{'http://xmlns.sony.com/middleware/assetdata/', 'Sony_MiddlewareAssetData'};
        public Sony_MiddlewareAssetData.PSConsumerAccountAssets_Output_element PSConsumerAccountAssets(String MDMAccountId,String Source,String TextAttribute1,String TextAttribute2,String TextAttribute3) {
            Sony_MiddlewareAssetData.PSConsumerAccountAssets_Input_element request_x = new Sony_MiddlewareAssetData.PSConsumerAccountAssets_Input_element();
            Sony_MiddlewareAssetData.PSConsumerAccountAssets_Output_element response_x;
            request_x.MDMAccountId = MDMAccountId;
            request_x.Source = Source;
            request_x.TextAttribute1 = TextAttribute1;
            request_x.TextAttribute2 = TextAttribute2;
            request_x.TextAttribute3 = TextAttribute3;
            Map<String, Sony_MiddlewareAssetData.PSConsumerAccountAssets_Output_element> response_map_x = new Map<String, Sony_MiddlewareAssetData.PSConsumerAccountAssets_Output_element>();
            response_map_x.put('response_x', response_x);
            
           
	            WebServiceCallout.invoke(
	              this,
	              request_x,
	              response_map_x,
	              new String[]{endpoint_x,
	              'document/http://xmlns.sony.com/middleware/assetdata/:PSConsumerAccountAssets',
	              'http://xmlns.sony.com/middleware/assetdata/',
	              'PSConsumerAccountAssets_Input',
	              'http://xmlns.sony.com/middleware/assetdata/',
	              'PSConsumerAccountAssets_Output',
	              'Sony_MiddlewareAssetData.PSConsumerAccountAssets_Output_element'}
	            );

            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }  
    public class PSConsumerAccountAssets_Input_element {
        public String MDMAccountId;
        public String Source;
        public String TextAttribute1;
        public String TextAttribute2;
        public String TextAttribute3;
        private String[] MDMAccountId_type_info = new String[]{'MDMAccountId','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Source_type_info = new String[]{'Source','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TextAttribute1_type_info = new String[]{'TextAttribute1','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TextAttribute2_type_info = new String[]{'TextAttribute2','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] TextAttribute3_type_info = new String[]{'TextAttribute3','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/assetdata/','true','false'};
        private String[] field_order_type_info = new String[]{'MDMAccountId','Source','TextAttribute1','TextAttribute2','TextAttribute3'};
    }
    public class ListOfAccountTopElmt {
        public Sony_MiddlewareAssetData.ListOfAccount ListOfAccount;
        private String[] ListOfAccount_type_info = new String[]{'ListOfAccount','http://xmlns.sony.com/middleware/assetdata/','ListOfAccount','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/assetdata/','true','false'};
        private String[] field_order_type_info = new String[]{'ListOfAccount'};
    }
    public class ListOfAsset {
        public Sony_MiddlewareAssetData.Asset[] Asset;
        private String[] Asset_type_info = new String[]{'Asset','http://xmlns.sony.com/middleware/assetdata/','Asset','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/assetdata/','true','false'};
        private String[] field_order_type_info = new String[]{'Asset'};
    }
}