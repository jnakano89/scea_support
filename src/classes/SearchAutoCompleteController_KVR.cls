/*
    @Author: Venkat Reddy
    @Version: 1.0
    @Description: Auto complete and search funtionalty for the Articles
*/
global class SearchAutoCompleteController_KVR{
    /*Global variables*/
    public String searchedRecordId { get; set; }
    //public static list<ResultSet> searchedRecord {get;set;}
    //public list<ResultSet> searchedResultList {get;set;}
    public String searchedString {get;set;}
    //properties for guided browse category
    public List<Category__c> categoryList{set;get;}
    public String selectedProduct{set;get;}
    public String selectedProductCategory{set;get;}
    public String selectedAccountCategory{set;get;}
    public String selectedAllTopicCategory{set;get;}
    public List<KnowledgeArticleVersion> articleList{set;get;}
    public List<ResultSet> guidedBrowseResultsList{set;get;}
    public Boolean showArticlesFlag{set;get;}
    public Boolean isProdutSelectedFlag{set;get;}
    public Boolean isAccountSelectedFlag{set;get;}
    public Boolean isAllTopicsSelectedFlag{set;get;}
    public Boolean showMoreButton{set;get;}
    public KnowledgeArticleVersion featureArticle{set;get;}
    //property for error code
    public Error_Code_PKB__c errorCodeCS{get;set;}
    public String errorCodeString{get;set;}
    public String errorCodeAllTopicString{get;set;}
    
    static String howToArticleKeyPrefix = How_To__kav.sObjectType.getDescribe().getKeyPrefix();
    static String genInfoArticleKeyPrefix = General_Info__kav.sObjectType.getDescribe().getKeyPrefix();
    static String decisionTreeArticleKeyPrefix = Decision_Tree__kav.sObjectType.getDescribe().getKeyPrefix();
    static String newsArticleKeyPrefix = News__kav.sObjectType.getDescribe().getKeyPrefix();
    static String virtualGuidesArticleKeyPrefix = Virtual_Guides__kav.sObjectType.getDescribe().getKeyPrefix();
    static String kcArticleKeyPrefix = KC_Article__kav.sObjectType.getDescribe().getKeyPrefix();
    
    Integer pageRecordSize = Integer.valueOf(Label.Browse_Guide_Search_Results_Per_Page)+1;
    Integer searchTextMinimumLength = Integer.valueOf(Label.Search_Text_Minimum_Length);
    static Integer articleSummaryTrimLength = Integer.valueOf(Label.Article_Summary_Trim_Length);
    
    global SearchAutoCompleteController_KVR(){
        fetchFeaturedArticles();
    }
    
    global PageReference searchForArticles() {
        //searchedResultList = new list<ResultSet>();
        PageReference pageRef = null;
        if(searchedString != null && searchedString.length()>searchTextMinimumLength){
            pageRef = Page.SCEA_SearchResults;
            pageRef.getParameters().put('selStr',searchedString);
            pageRef.setRedirect(true);
            return pageRef;
        }
        return pageRef;
    }
    
    public void fetchCategoriesBasedOnSelection(String selStr){
        categoryList = new List<Category__c>();
        if(selStr != null && selStr.equalsIgnoreCase('Product'))
            categoryList = [select Name, Display_Order__c, Category_API_Name__c from Category__c where Display_in_Product__c=true order by Display_Order__c asc];
        else if(selStr != null && selStr.equalsIgnoreCase('Account'))
            categoryList = [select Name, Display_Order__c, Category_API_Name__c from Category__c where Display_in_Account__c=true order by Display_Order__c asc];
        else if(selStr != null && selStr.equalsIgnoreCase('All Topics'))
            categoryList = [select Name, Display_Order__c, Category_API_Name__c from Category__c order by Display_Order__c asc];
    }
    
    public void fetchCategoriesForProduct(){
        fetchCategoriesBasedOnSelection('Product');
        showArticlesFlag = false;
        isProdutSelectedFlag = true;
        isAccountSelectedFlag = false;
        showMoreButton = false;
        guidedBrowseResultsList = new List<ResultSet>();
        selectedAccountCategory = null;
        isAllTopicsSelectedFlag = false;
        selectedAllTopicCategory = null;
        errorCodeCS = null;
        errorCodeString = null;
        errorCodeAllTopicString = null;
    }
    
    public void fetchCategoriesForAccount(){
        fetchCategoriesBasedOnSelection('Account');
        showArticlesFlag = false;
        isProdutSelectedFlag = false;
        isAccountSelectedFlag = true;
        showMoreButton = false;
        guidedBrowseResultsList = new List<ResultSet>();
        selectedProduct = null;
        selectedProductCategory = null;
        isAllTopicsSelectedFlag = false;
        selectedAllTopicCategory = null;
        errorCodeCS = null;
        errorCodeString = null;
        errorCodeAllTopicString = null;
    }
    
    public void fetchCategoriesForAllTopics(){
        fetchCategoriesBasedOnSelection('All Topics');
        showArticlesFlag = false;
        isProdutSelectedFlag = false;
        isAccountSelectedFlag = false;
        showMoreButton = false;
        guidedBrowseResultsList = new List<ResultSet>();
        selectedProduct = null;
        selectedProductCategory = null;
        isAllTopicsSelectedFlag = true;
        selectedAccountCategory = null;
        errorCodeCS = null;
        errorCodeString = null;
        errorCodeAllTopicString = null;
    }
    
    public void fetchArticlesBasedOnSubCategory(){
        showArticlesFlag = true;
        system.debug('---'+selectedProduct+'---'+selectedProductCategory+'---'+selectedAccountCategory);
        guidedBrowseResultsList = new List<ResultSet>();
        List<sObject> searchList = new List<sObject>();
        if(selectedProduct != null && selectedProductCategory != null){
            //as summary is not part of KnowledgeArticleVersion object, this has been commented
            /*
            String soqlQuery = 'select Id, KnowledgeArticleId, ArticleNumber, Title, LastPublishedDate, ArticleType from KnowledgeArticleVersion where PublishStatus=\'Online\' AND  Language=\'en_US\' WITH DATA CATEGORY Topic__c AT '+selectedProductCategory+' AND Products__c AT '+selectedProduct+' ORDER BY ArticleNumber DESC LIMIT '+pageRecordSize;
            articleList = (List<KnowledgeArticleVersion>)Database.query(soqlQuery);
            */
            //query on How To article based on category and/or product
            String soqlQuery = 'select Id, ArticleNumber, Title, Overview_Summary__c from How_To__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedProductCategory+' AND Products__c AT '+selectedProduct+' ORDER BY ArticleNumber DESC LIMIT '+pageRecordSize;
            List<sObject> sobjectList = Database.query(soqlQuery);
            searchList.addAll(sobjectList);
            if(searchList.size() < pageRecordSize){
                //query on General Info article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, Overview_Summary__c from General_Info__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedProductCategory+' AND Products__c AT '+selectedProduct+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
            if(searchList.size() < pageRecordSize){
                //query on Decision Tree article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, summary from Decision_Tree__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedProductCategory+' AND Products__c AT '+selectedProduct+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
            if(searchList.size() < pageRecordSize){
                //query on News article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, Overview_Summary__c from News__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedProductCategory+' AND Products__c AT '+selectedProduct+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
            if(searchList.size() < pageRecordSize){
                //query on Virtual Guides article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, Overview_Summary__c from Virtual_Guides__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedProductCategory+' AND Products__c AT '+selectedProduct+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
            if(searchList.size() < pageRecordSize){
                //query on Decision Tree article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, summary from KC_Article__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedProductCategory+' AND Products__c AT '+selectedProduct+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
        }else if(selectedProduct == null && selectedAccountCategory != null){
            //as summary is not part of KnowledgeArticleVersion object, this has been commented
            /*
            String soqlQuery = 'select Id, KnowledgeArticleId, ArticleNumber, Title, LastPublishedDate, ArticleType from KnowledgeArticleVersion where PublishStatus=\'Online\' AND  Language=\'en_US\' WITH DATA CATEGORY Topic__c AT '+selectedAccountCategory+' ORDER BY ArticleNumber DESC LIMIT '+pageRecordSize;
            articleList = (List<KnowledgeArticleVersion>)Database.query(soqlQuery);
            */
            //query on How To article based on category and/or product
            String soqlQuery = 'select Id, ArticleNumber, Title, Overview_Summary__c from How_To__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedAccountCategory+' ORDER BY ArticleNumber DESC LIMIT '+pageRecordSize;
            List<sObject> sobjectList = Database.query(soqlQuery);
            searchList.addAll(sobjectList);
            if(searchList.size() < pageRecordSize){
                //query on General Info article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, Overview_Summary__c from General_Info__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedAccountCategory+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
            if(searchList.size() < pageRecordSize){
                //query on Decision Tree article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, summary from Decision_Tree__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedAccountCategory+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
            if(searchList.size() < pageRecordSize){
                //query on News article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, Overview_Summary__c from News__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedAccountCategory+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
            if(searchList.size() < pageRecordSize){
                //query on Virtual Guides article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, Overview_Summary__c from Virtual_Guides__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedAccountCategory+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
            if(searchList.size() < pageRecordSize){
                //query on Decision Tree article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, summary from KC_Article__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedAccountCategory+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
        }else if(selectedProduct == null && selectedAccountCategory == null && selectedAllTopicCategory != null){
            //as summary is not part of KnowledgeArticleVersion object, this has been commented
            /*
            String soqlQuery = 'select Id, KnowledgeArticleId, ArticleNumber, Title, LastPublishedDate, ArticleType from KnowledgeArticleVersion where PublishStatus=\'Online\' AND  Language=\'en_US\' WITH DATA CATEGORY Topic__c AT '+selectedAccountCategory+' ORDER BY ArticleNumber DESC LIMIT '+pageRecordSize;
            articleList = (List<KnowledgeArticleVersion>)Database.query(soqlQuery);
            */
            //query on How To article based on category and/or product
            String soqlQuery = 'select Id, ArticleNumber, Title, Overview_Summary__c from How_To__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedAllTopicCategory+' ORDER BY ArticleNumber DESC LIMIT '+pageRecordSize;
            List<sObject> sobjectList = Database.query(soqlQuery);
            searchList.addAll(sobjectList);
            if(searchList.size() < pageRecordSize){
                //query on General Info article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, Overview_Summary__c from General_Info__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedAllTopicCategory+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
            if(searchList.size() < pageRecordSize){
                //query on Decision Tree article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, summary from Decision_Tree__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedAllTopicCategory+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
            if(searchList.size() < pageRecordSize){
                //query on News article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, Overview_Summary__c from News__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedAllTopicCategory+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
            if(searchList.size() < pageRecordSize){
                //query on Virtual Guides article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, Overview_Summary__c from Virtual_Guides__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedAllTopicCategory+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
            if(searchList.size() < pageRecordSize){
                //query on Decision Tree article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, summary from KC_Article__kav where PublishStatus=\'Online\' AND Language=\'en_US\' with data category Topic__c AT '+selectedAllTopicCategory+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
        }
        /*
        if(articleList != null && !articleList.isEmpty() && articleList.size() == pageRecordSize){
            articleList.remove(articleList.size()-1);
            showMoreButton = true;
        }else
            showMoreButton = false;
        */
        if(searchList != null && !searchList.isEmpty() && searchList.size() == pageRecordSize){
            searchList.remove(searchList.size()-1);
            showMoreButton = true;
        }else
            showMoreButton = false;
        
        for(SObject s : searchList){
            guidedBrowseResultsList.add(new SearchAutoCompleteController_KVR.ResultSet(s));
        }
    }
    
    public void initializeValuesFormySystem(){
        isProdutSelectedFlag = false;
        isAccountSelectedFlag = false;
        categoryList = new List<Category__c>();
        //articleList = new List<KnowledgeArticleVersion>();
        showArticlesFlag = false;
        showMoreButton = false;
        guidedBrowseResultsList = new List<ResultSet>();
        isAllTopicsSelectedFlag = false;
        errorCodeCS = null;
        errorCodeString = '';
        errorCodeAllTopicString = '';
    }
    
    public PageReference showMoreArticles(){
        PageReference pageRef = Page.scea_searchresults;
        if(selectedProduct != null && selectedProductCategory != null){
            pageRef.getParameters().put('guideStr',selectedProductCategory);
            pageRef.getParameters().put('guideProdStr',selectedProduct);
        }else if(selectedProduct == null && selectedAccountCategory != null)
            pageRef.getParameters().put('guideStr',selectedAccountCategory);
        else if(selectedProduct == null && selectedAccountCategory == null && selectedAllTopicCategory != null)
            pageRef.getParameters().put('guideStr',selectedAllTopicCategory);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public void fetchFeaturedArticles(){
        List<Feature_Article__c> featureArticleCSList = Feature_Article__c.getAll().values();
        if(featureArticleCSList != null && !featureArticleCSList.isEmpty() && featureArticleCSList.get(0).Article_Id__c != null && featureArticleCSList.get(0).Article_Id__c != ''){
            List<KnowledgeArticleVersion> kavList = [select Id, Title from KnowledgeArticleVersion where Id=:featureArticleCSList.get(0).Article_Id__c];
            if(kavList != null && !kavList.isEmpty())
                featureArticle = kavList.get(0);
        }
    }
    
    public void searchForErrorCode(){
        errorCodeCS = null;
        //List<Error_Codes__c> errorCodeCSList = Error_Codes__c.getAll().values();
        List<Error_Code_PKB__c> errorCodeCSList = [select Id, Name, Error_Code__c, Description__c, Action__c from Error_Code_PKB__c];
        if(errorCodeCSList != null && !errorCodeCSList.isEmpty() && errorCodeString != null && errorCodeString != ''){
            //if matches with error code then return error code which is a single record to show in search results
            for(Error_Code_PKB__c errorCodeObj : errorCodeCSList){
                if(errorCodeObj.Error_Code__c.contains(errorCodeString)){
                    errorCodeCS = errorCodeObj;
                    break;
                }
            }
        }else if(errorCodeCSList != null && !errorCodeCSList.isEmpty() && errorCodeString != null && errorCodeAllTopicString != ''){
            //if matches with error code then return error code which is a single record to show in search results
            for(Error_Code_PKB__c errorCodeObj : errorCodeCSList){
                if(errorCodeObj.Error_Code__c.contains(errorCodeAllTopicString)){
                    errorCodeCS = errorCodeObj;
                    break;
                }
            }
        }
    }
    
    /*Record Wrapper*/
    global class ResultSet{
        public String Id {get;set;} 
        public String ArticleNumber{get;set;}
        public String Title{get;set;}
        public String sObjectName {get;set;}
        public String overviewSummary{set;get;}
        
        public ResultSet(sObject s){
            this.Id = s.Id;
            this.ArticleNumber = s.get('ArticleNumber')+'';
            this.Title = s.get('Title')+'';
            
            if(String.valueOf(s.Id).startsWith(howToArticleKeyPrefix)){
                this.overviewSummary = s.get('Overview_Summary__c')+'';
                this.sObjectName = 'How To';   
            }else if(String.valueOf(s.Id).startsWith(genInfoArticleKeyPrefix)){
                this.overviewSummary = s.get('Overview_Summary__c')+'';
                this.sObjectName = 'General Info';        
            }else if(String.valueOf(s.Id).startsWith(decisionTreeArticleKeyPrefix)){
                this.overviewSummary = s.get('Summary')+'';
                this.sObjectName = 'Decison Tree';        
            }else if(String.valueOf(s.Id).startsWith(newsArticleKeyPrefix)){
                this.overviewSummary = s.get('Overview_Summary__c')+'';
                this.sObjectName = 'News';        
            }else if(String.valueOf(s.Id).startsWith(virtualGuidesArticleKeyPrefix)){
                this.overviewSummary = s.get('Overview_Summary__c')+'';
                this.sObjectName = 'Virtual Guides';        
            }else if(String.valueOf(s.Id).startsWith(kcArticleKeyPrefix)){
                this.overviewSummary = s.get('Summary')+'';
                this.sObjectName = 'KC Article';        
            }
            
            if(this.overviewSummary.length()>articleSummaryTrimLength)
                this.overviewSummary = this.overviewSummary.substring(0,(articleSummaryTrimLength-2))+'...';
            this.overviewSummary = this.overviewSummary.replaceAll('<br/>','\n');
            this.overviewSummary = this.overviewSummary.replaceAll('<br />','\n');
            String htmlTagPattern = '<.*?>';
            Pattern patternTemp = Pattern.compile(htmlTagPattern);
            Matcher matcherTemp = patternTemp.matcher(this.overviewSummary);
            this.overviewSummary = matcherTemp.replaceAll('');
        }     
    }
}