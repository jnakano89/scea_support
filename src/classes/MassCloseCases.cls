public class MassCloseCases {

    public integer recSelectionSize{get;set;}
    public integer recTotalSize{get;set;}
    public boolean frmRender{get; set;}  
    public ApexPages.StandardSetController stdsetContoller;
    public String customPermissionName = 'Enable_Mass_Case_Close';
    public boolean agentAccessChk;

    public MassCloseCases(ApexPages.StandardSetController controller) {
      stdsetContoller = controller;
      frmRender = true;
      agentAccessChk = true;
      recSelectionSize= stdsetContoller.getSelected().size();     
      recTotalSize= stdsetContoller.getRecords().size();
      system.debug('<<<<<< Agent Access Check Variable =>>>>>>>>>>>>'+agentAccessChk );
      if(!checkPermission()) {
       frmRender = false;
       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You do not have access to Mass Close Cases'));
      } else if(recSelectionSize < 1) {
          frmRender = false;
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a record'));
      } 
    }
    
    public List<SelectOption> getstatusValues(){
       List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Open','Open'));
        options.add(new SelectOption('Closed','Closed'));
        options.add(new SelectOption('Transfer to Chat','Transfer to Chat'));
        options.add(new SelectOption('No Response Necessary','No Response Necessary'));
        
        return options;
    } 
    
    public boolean checkPermission(){
      List<SetupEntityAccess> access;
      List<CustomPermission> customPerms;
      
      access =[SELECT SetupEntityId FROM SetupEntityAccess WHERE SetupEntityType='CustomPermission' AND ParentId IN (SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId=:UserInfo.getUserId())];
      Set<Id> customPermIds = new Set<Id>();
      for (SetupEntityAccess sea : access) {
          customPermIds.add(sea.SetupEntityId);
      }
      customPerms = [SELECT Id, DeveloperName, MasterLabel FROM CustomPermission WHERE Id IN :customPermIds];
      for (CustomPermission perm : customPerms) {
           if (perm.DeveloperName.equals(customPermissionName)) return true;
      }
      
      return false;
    }  
    
    public PageReference reDirectPage(){ 
      PageReference pg = stdsetContoller.cancel();
      return pg;  
    }  
}