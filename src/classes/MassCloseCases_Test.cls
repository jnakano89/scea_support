/*********************
 CREATED: 
     04/16/2015 LEENA MANDADAPU : Jira# SMS-1298
 *********************/
@isTest
private class MassCloseCases_Test {

    public static CustomPermission cp;
    public static Profile csTier1;
    public static User socialAgent;
    public static PermissionSet ps;
    public static PermissionSetAssignment psa;
    public static Profile csSupervisor;
    public static User socialSupervisor;
    public static PermissionSet psSUP;
    public static SetupEntityAccess sea;
    public static PermissionSetAssignment psaSUP;
    public static Account conAccnt;
    public static Contact contactRec;
    public static List<case> cList = new List<case>();
    public static List<case> cListNoRecs = new List<case>();
    public static case socialCase = new case();
       
    static void createGlobalUserData() {
        //Custom Permission
        cp = [select Id from CustomPermission where DeveloperName = 'Enable_Mass_Case_Close'][0];
        //Social Agent User
        csTier1 = [select Id from Profile where Name = 'CS-Tier 1'][0];
        
        System.runAs(new User(Id=UserInfo.getUserId())) {              
        socialAgent = new User(ProfileId=csTier1.Id,
                                Username = 'socialagent@test.com',
                                LastName = 'socialAgent',
                                Email = 'socialagent@test.com',
                                Alias = 'socialA',
                                CommunityNickname = 'socialAgent',
                                TimeZoneSidKey = 'America/Los_Angeles',
                                LocaleSidKey = 'en_US',
                                EmailEncodingKey = 'UTF-8',
                                LanguageLocaleKey = 'en_US',
                                CompanyName = 'Test');
                                     
        insert socialAgent; 
        
        ps = new PermissionSet(Name='SocialAgentTest',Label='SocialAgentTest');
        insert ps;
        
        psa = new PermissionSetAssignment(AssigneeId=socialAgent.Id,PermissionSetId=ps.Id);
        insert psa;                   
                          
        //Social Supervisor Agent User
        csSupervisor = [select Id from Profile where name = 'CS-Bureau Supervisor'][0];
                
        socialSupervisor = new User(ProfileId=csSupervisor.Id,
                                    Username = 'socialSUP@test.com',
                                    LastName = 'socialSUP',
                                    Email = 'socialSUP@test.com',
                                    Alias = 'socialS',
                                    CommunityNickname = 'socialSUP',
                                    TimeZoneSidKey = 'America/Los_Angeles',
                                    LocaleSidKey = 'en_US',
                                    EmailEncodingKey = 'UTF-8',
                                    LanguageLocaleKey = 'en_US',
                                    CompanyName='Test');
                                     
        insert socialSupervisor; 
        
        psSUP = new PermissionSet(Name='SocialSupervisorTest',Label='SocialSupervisorTest');
        insert psSUP;
        //give access to custom permission only for Social Supervisor
        sea = new SetupEntityAccess(ParentId=psSUP.Id,SetupEntityId=cp.Id);
        insert sea;
        
        psaSUP = new PermissionSetAssignment(AssigneeId=socialSupervisor.Id,PermissionSetId=psSUP.Id);
        insert psaSUP;         
        }   
    }
    
    static void createObjectsTestData() {
    	  	
    	String accRecTypeId= [select Id from RecordType where SobjectType='Account' and IsPersonType = true limit 1].Id;
    	conAccnt = new Account();
    	conAccnt.LastName = 'ApexTestLast';
        conAccnt.FirstName = 'ApexTestFirst';
        conAccnt.RecordTypeID = accRecTypeId;
        insert conAccnt;
        
        contactRec = [select id,FirstName, LastName from Contact where isPersonAccount = true and AccountId = :conAccnt.id limit 1];
    	
    	String SocialRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Social').getRecordTypeId();   	
    	
    	//for(integer i=0; i<=2; i++) {
    	  socialCase.RecordTypeId = SocialRecTypeId;
    	  socialCase.ContactId = contactRec.Id;
    	  socialCase.status = 'Opened';
    	  socialCase.Origin = 'Social';
    	 // cList.add(socialCase);
    	//}		
    	insert socialCase;
    	
    	cList = [select Id from case where Id =: socialCase.Id];
    	//cListNoRecs = 

    }
    
    static testMethod void myPositiveTestCase_SocialAgent() {
        
        createGlobalUserData();
        createObjectsTestData();
        system.runas(socialAgent) {
          Test.startTest();	
          ApexPages.StandardSetController setCtr = new ApexPages.StandardSetController(cList);
    	  setCtr.setSelected(cList);
          MassCloseCases controller = new MassCloseCases(setCtr); 
          List<SelectOption> statusValuesList = controller.getstatusValues();
          controller.reDirectPage();
          //controller.Save(); 
          Test.stopTest();   
        }   
    }
    
    static testMethod void myPositiveTestCase_SocialSupervisor() {
        
        createGlobalUserData();
        createObjectsTestData();
        system.runas(socialSupervisor) {
          Test.startTest(); 	
          ApexPages.StandardSetController setCtr = new ApexPages.StandardSetController(cList);
    	  setCtr.setSelected(cList);
          MassCloseCases controller = new MassCloseCases(setCtr);
          List<SelectOption> statusValuesList = controller.getstatusValues();
          controller.reDirectPage();
          //controller.Save(); 
          Test.stopTest();   
        }   
    }
    
    static testMethod void myPositiveTestCase_SocialSupervisor_NoRecsSelected() {
        
        createGlobalUserData();
        createObjectsTestData();
        system.runas(socialSupervisor) {
          Test.startTest(); 	
          ApexPages.StandardSetController setCtr = new ApexPages.StandardSetController(cListNoRecs);
    	  setCtr.setSelected(cListNoRecs);
          MassCloseCases controller = new MassCloseCases(setCtr);
          List<SelectOption> statusValuesList = controller.getstatusValues();
          controller.reDirectPage();
          //controller.Save(); 
          Test.stopTest();   
        }   
    }
    
    
}