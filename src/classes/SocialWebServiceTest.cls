/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SocialWebServiceTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
         String userScsName = 'CTI Integrator';
         List<string> recordList = new List<string>();
         list<User> scsIntUser = [Select  id from User  where name =:userScsName order by LastModifiedDate ASC Limit 1];
         Contact con = new Contact(LastName = 'lname', FirstName = 'fname');
         insert con; 
         SocialPersona sper = new SocialPersona(ParentId = con.Id, Provider = 'Twitter', Name = 'random1');
         insert sper;       
         SocialPost sp = new SocialPost(Content = 'TESTSOCIALPOST', 
        	Handle = 'TESTDigitlSuperStar',
        	PersonaId = sper.Id,
        	Headline = 'TEST TWEET FROM: DigitlSuperStar',
	        MediaProvider = 'TWITTER',
	        MediaType = 'Twitter',
	        MessageType = 'Reply', //twitter
	        Name = 'TEST TWEET FROM: DigitlSuperStar',
	        PostPriority = 'High',
	        PostTags = 'Help,Question',
	        Provider = 'Twitter',
	        TopicProfileName = '@PlayStation',
	        OwnerId = scsIntUser.get(0).id);			                                       
        insert sp;                
        //SocialPostWebService spws = new SocialPostWebService();
        String str1 = sp.Id;
        String str = SocialPostWebService.createSocialCase(str1);
        recordList.add(str1);                                
        String str2 = SocialPostWebService.createSocialCases(recordList);
        system.assert(str.length()>5);
        system.assert(str2.length()>5);
    }
    
}