//**************************************************************************/
// (c) 2013 Appirio, Inc.
//
// Description : Controller Class For VF Page ConsumerSearch.
//                  
// Original September   05,2013  : KapiL Choudhary(JDC) Created for the Task T-180277
// Updated : 
//   9/10/2013 : Urminder(JDC) : updated returnToContact() to view record in Console.[T-181516]
//          
// ***************************************************************************/

public with sharing class LinkConsumerController {

    // user is linking from this record
    public Account currentPersonAccount {get;set;} 
    
    
    String mdmContactId;
    String mdmAccountId; 
    String contactId;
    public Contact selectedContact;
    public string selectedMdmContactId {get;set;}
    
    
    public String paramEmail{get;set;}   
    public String paramAccountId{get;set;}    
    
    public ConsumerSearchResult searchResultMember{get;set;}
    public String consumerPhoneToSearch{get;set;}
    public String consumerEmailToSearch{get;set;}
    public String consumerPSNOnlineIdToSearch{get;set;}
    public Boolean hasErrorInPage{get;set;}
    public Boolean isInConsole{get;set;}
 
    public final String CLASS_NAME = 'LinkConsumerController';
    
    public List<WebServiceUtility.ConsumerSearchResult> consumerSearchResultList {get;set;}
    
    public ConsumerSearchResult searchResult;
    
    public boolean NO_RESULT_FOUND{get;set;}
    public list<WrapperContacts> resultList{get;set;}


    public LinkConsumerController(ApexPages.StandardController controller) {

        resultList = new list<WrapperContacts>();
        
    }

    public LinkConsumerController(){
        
        resultList = new list<WrapperContacts>();

        paramEmail  = ApexPages.currentPage().getParameters().get('email');
        paramAccountId  = ApexPages.currentPage().getParameters().get('accountId');

        if(paramEmail != null){
            consumerEmailToSearch = paramEmail;
        }
        
        if(paramAccountId!=null && paramAccountId.trim().length()>0){
            
            for(Account acct : [Select MDM_ID__pc, LastName, FirstName, Id From Account 
                                    where Id=:paramAccountId]){
                currentPersonAccount = acct;
            }//end-for
            
        }
        
    }


    public Pagereference gotoAccount(){
        if(paramAccountId != null && paramAccountId != ''){
            return new ApexPages.StandardController(new account(id=paramAccountId)).view();
        }
        return null;
    }
    
    

    
    public void resetSearchStatus(){
        consumerPhoneToSearch ='';
        consumerEmailToSearch ='';
        consumerPSNOnlineIdToSearch = '';
        resultList = new list<WrapperContacts>();
    }
    
 
    public PageReference selectConsumer() {
        
        String mdmId;
        
        if(selectedMdmContactId!=null && selectedMdmContactId!=''){
            mdmId = selectedMdmContactId;
            mdmContactId = mdmId;    
        }else{
            mdmId = Apexpages.currentPage().getParameters().get('ctRadio');
            mdmContactId = mdmId;
        }
        
        system.debug('mdmContactId::>>>> '+mdmContactId);   
      
        if(mdmId == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Please select a consumer.'));
            hasErrorInPage = true;
            return null;
        }
        else{
            hasErrorInPage = false;
        }
      
      for(Contact cnt : [select Id, MDM_Account_ID__c from Contact where MDM_ID__c = :mdmId]) {
        
        contactId = cnt.Id;
        selectedContact = cnt;
        
      }//end-for
      
      system.debug('>>>> selectedContact :: '+selectedContact);
      
      WrapperContacts wrapContact = new WrapperContacts();
        for(WrapperContacts wrapCnt : resultList){
            if(wrapCnt.mdmContactId == mdmContactId){
                wrapContact = wrapCnt;
            }
        }
        
      //No Existing Contact Found Against This MDM ID So we Have To update current Person Account.
      if(selectedContact == null){

        system.debug('>>>> searchResult :: '+searchResult);

          if(wrapContact <> null){

 
              try{  
                
                currentPersonAccount.MDM_ID__pc = wrapContact.mdmContactId;
                update currentPersonAccount;
                                
                for(Contact cnt : [select Id, MDM_Account_ID__c from Contact where MDM_ID__c = :mdmId]) {
                    contactId = cnt.Id;
                    selectedContact = cnt;
                }//end-for
                
             }catch(Exception ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
                return null;
             }//end-try-catch


        }//end-if        

        if(searchResult <> null && searchResult.ErrorMessage <> null) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, searchResult.ErrorMessage));
          return null;
        }
      } 
      
      return null;
    }
 
  public void searchConsumerByFilter(){
 
    system.debug('>>>> consumerPhoneToSearch = '+consumerPhoneToSearch);
    system.debug('>>>> consumerEmailToSearch = '+consumerEmailToSearch);
    system.debug('>>>> consumerPSNOnlineIdToSearch = '+consumerPSNOnlineIdToSearch);  

    if(validateSearch()){
        searchResult= fetchConsumer(consumerPhoneToSearch,consumerEmailToSearch,consumerPSNOnlineIdToSearch);
    }else{
      resultList = new list<WrapperContacts>();
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please only search 1 value at a time.'));
    }
 
  }
  
  private Boolean validateSearch() {
    
    if((consumerPhoneToSearch != null && consumerPhoneToSearch.trim().length()>0) && (consumerEmailToSearch != null && consumerEmailToSearch.trim().length()>0)) {
        
        return false;
    
    } else if((consumerPhoneToSearch != null && consumerPhoneToSearch.trim().length()>0) && (consumerPSNOnlineIdToSearch != null && consumerPSNOnlineIdToSearch.trim().length()>0)) {
    
        return false;
    
    } else if((consumerEmailToSearch != null && consumerEmailToSearch.trim().length()>0) && (consumerPSNOnlineIdToSearch != null && consumerPSNOnlineIdToSearch.trim().length()>0)) {
    
        return false;
    
    } else if((consumerPhoneToSearch != null && consumerPhoneToSearch.trim().length() == 0) 
            && (consumerEmailToSearch != null && consumerEmailToSearch.trim().length() == 0) 
            && consumerPSNOnlineIdToSearch != null && consumerPSNOnlineIdToSearch.trim().length() == 0){
    
        return false;
    
    }
    
    return true;    
  }
  
  public PageReference callGetConsumers() {
    
    if(paramEmail != null && paramEmail !=''){
        fetchConsumer(null,paramEmail,null);
    }
    return null;
  }
  
    private String formatPhoneNumber(string phoneNumber) {
        if(phoneNumber<> null && phoneNumber.length() == 10) {
            phoneNumber = '(' + phoneNumber.substring(0, 3) + ') ' + phoneNumber.substring(3, 6) + '-' + 
                            phoneNumber.substring(6, phoneNumber.length());
        }
        return phoneNumber;
    }
    
  public ConsumerSearchResult fetchConsumer(String phone, String email, String psnOnlineId){
    
    
    System.debug('>>>>>>>>>>>>>>>isInConsole='+isInConsole);
    
    system.debug('>> phone='+phone+', email='+email+', PSN Online ID='+psnOnlineId);
    
    resultList = new list<WrapperContacts>();
    
    try {
        
      searchResult = ConsumerServiceUtility.getConsumers(phone, email, psnOnlineId);
      
    } catch(Exception e) {
      
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
      IntegrationAlert.addException('Lookup Consumer', 'email='+email + ', phone='+phone + ', PSN Online ID='+psnOnlineId, e);    
            
    }
    
    if(searchResult <> null && searchResult.listOfAccounts == null && searchResult.ErrorMessage == null) {      
        NO_RESULT_FOUND = true;
    }
    else {
        NO_RESULT_FOUND = false;
    }

     
     if(searchResult <> null && searchResult.listOfAccounts != null){
        
       system.debug('listOfAccounts>>>> '+searchResult.listOfAccounts);
       
       for(Sony_MiddlewareConsumerdata3.Account_mdm acc : searchResult.listOfAccounts) {
         
        
        if(acc.ListOfContact.Contact != null){
            for(Sony_MiddlewareConsumerdata3.Contact_mdm cnt : acc.ListOfContact.Contact) {
                WrapperContacts wrapCnt = new WrapperContacts();
                wrapCnt.fName = cnt.FirstName;
                wrapCnt.lName = cnt.LastName;
                wrapCnt.psnSignInId = cnt.PSNSignInId;
                wrapCnt.psnPhone = formatPhoneNumber(cnt.PSNPhone);             
                wrapCnt.crmPhone = formatPhoneNumber(cnt.CRMPhone);
                wrapCnt.psnEmail = cnt.PSNEmail;
                wrapCnt.siebelEmail = cnt.SiebelEmail;
                wrapCnt.birthDate = cnt.BirthDate;
                wrapCnt.mdmContactId = cnt.MDMRowId;
                wrapCnt.PSNHandle = acc.PSNHandle;
                wrapCnt.PreferredLanguageCode = cnt.PreferredLanguageCode;
                wrapCnt.sex = cnt.sex;
                wrapCnt.mdmAccountId = acc.MDMAccountRowId;
                wrapCnt.PSPlusExpiration = getMyDateTime(acc.PSPlusStackedEndDate); //code added by Preetu
                system.debug('acc psplussubscriber value>>>' + acc.PSPlusSubscriber);
                wrapCnt.PSPlusSubscriber = acc.PSPlusSubscriber; //code added by Preetu
                system.debug('>>>> cnt = '+cnt);
                system.debug('>>>> wrapCnt = '+wrapCnt);
                system.debug('>>>> ListOfAddress = '+cnt.ListOfAddress.Address);
                
                if(cnt.ListOfAddress.Address != null){
                    
                    for(Sony_MiddlewareConsumerdata3.Address_mdm add : cnt.ListOfAddress.Address) {
                      wrapCnt.addressList.add(add); 
                    }
                }
                
                resultList.add(wrapCnt);
            }
        }
      }
    }
    
    if(searchResult <> null 
        && searchResult.ErrorMessage <> null) {
            
        if (searchResult.ErrorCode == '20'){
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No records to display.' ));
        }else{  
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, searchResult.ErrorMessage));
        }
    }

    return searchResult;
  }
  
  

  public PageReference refreshConsumer(){
    

        System.debug('>>>>>>>mdmContactId='+mdmContactId);
        System.debug('>>>>>>>mdmAccountId='+mdmAccountId); 
            
        if(mdmContactId==null ){
            return null;
        }
    
        searchResultMember = searchResult;
        PSN_Account__c psnAcc;
    
        Sony_MiddlewareConsumerdata3.Contact_mdm contact_mdm;
        Sony_MiddlewareConsumerdata3.Account_mdm account_mdm;
      
        Set<String> addressMdmIds = new Set<String>();
        
        list<Address__c> listOfAdressToUpsert = new list<Address__c>();
        Map<String, Address__c> existingAddressMap = new map<String, Address__c>();
        
        list<Sony_MiddlewareConsumerdata3.Address_mdm> addressList = 
                                new list<Sony_MiddlewareConsumerdata3.Address_mdm>();


        system.debug('>>> searchResult.listOfAccounts.size() = '+searchResult.listOfAccounts.size());       
       
        for(Sony_MiddlewareConsumerdata3.Account_mdm acc : searchResult.listOfAccounts) {

        
            system.debug(':::listOfContacts>>>> '+acc.ListOfContact.Contact);
        
            if(acc.ListOfContact.Contact != null){
                
                for(Sony_MiddlewareConsumerdata3.Contact_mdm cnt : acc.ListOfContact.Contact) {
                    
                    if(cnt.MDMRowId==mdmContactId){

                        contact_mdm = cnt;
                        account_mdm = acc;
                            
                        if(cnt.ListOfAddress.Address != null){
                            
                            for(Sony_MiddlewareConsumerdata3.Address_mdm add : cnt.ListOfAddress.Address) {
                              addressList.add(add);
                              addressMdmIds.add(add.AddressId);
                            }
                        }
                    }//end-if = This is the selected contact
                    
                }//end-for Contact_mdm
            }//end-if
        }//end-for Account_mdm
        
        system.debug('account_mdm>>>>> '+account_mdm);
        system.debug('selectedContact:::: '+selectedContact);
        
        
        if(account_mdm.MDMAccountRowId != null && account_mdm.MDMAccountRowId != ''){
            mdmAccountId=account_mdm.MDMAccountRowId;
            if(selectedContact.id == null){
                for(Contact con :[select Id, MDM_Account_ID__c 
                                                from Contact 
                                                    where MDM_Account_ID__c=:account_mdm.MDMAccountRowId]){
                    selectedContact = con;
                }
            }
            selectedContact.MDM_Account_ID__c = account_mdm.MDMAccountRowId;
            update selectedContact;
        }
        
        //Find a child PSN Account record with the same MDM Account ID in the response.
        for(PSN_Account__c acc : [select Id
                                    from PSN_Account__c
                                    where Consumer__c = :contactId
                                    AND MDM_Account_ID__c = :account_mdm.MDMAccountRowId]) {
            psnAcc = new PSN_Account__c(Id = acc.Id);
        }
        // If there isn't, create a new PSN account record
        if(psnAcc == null) {
            
          psnAcc = new PSN_Account__c(Consumer__c = contactId, 
                                        MDM_Account_ID__c = account_mdm.MDMAccountRowId);
                                        
          system.debug('psnAcc>>>> '+psnAcc);
        }
        
        psnAcc.Account_ID__c =  account_mdm.PSNAccountId;
        psnAcc.Creation_Date__c = account_mdm.CreatedDate;
        psnAcc.PSN_Account_ID__c = account_mdm.PSNAccountId;
        psnAcc.Suspension_Date__c = parseDate(account_mdm.AccountSuspendDate);
        psnAcc.Suspension_Reason__c = account_mdm.AccountSuspendReason;
        psnAcc.Unsuspended_Date__c = parseDate(account_mdm.AccountUnsuspendDate);
        psnAcc.Status__c = account_mdm.AccountStatus;
        psnAcc.First_Name__c = contact_mdm.FirstName;
        psnAcc.Last_Name__c = contact_mdm.LastName;
        psnAcc.Language__c = getLanguage(contact_mdm.PreferredLanguageCode);
        psnAcc.Date_of_Birth__c = parseDate(contact_mdm.BirthDate);
        psnAcc.Gender__c = contact_mdm.sex;
        psnAcc.PSN_Sign_In_ID__c = contact_mdm.PSNSignInId;
        psnAcc.Name = account_mdm.PSNHandle;
        psnAcc.Email__c = fixEmail(contact_mdm.PSNEmail);
        psnAcc.Phone__c = contact_mdm.PSNPhone;
        //Code added by Preetu Vashista on 7/10/2014 to add PSSubscriber Information
        if(account_mdm.PSPlusSubscriber != null){
        psnAcc.PS_Plus_Indicator__c = Boolean.valueOf(account_mdm.PSPlusSubscriber);
        System.debug('---->' + psnAcc.PS_Plus_Subscriber__c);
        }
       /* if((account_mdm.PSPlusSubscriber.equalsIgnoreCase('False') && account_mdm.PSPlusStackedEndDate == '')){
        psnAcc.PS_Plus_Start_Date__c = '';
        System.debug('---->' + psnAcc.PS_Plus_Start_Date__c);
        }
        else 
        {*/
         psnAcc.PS_Plus_Start_Date__c = getMyDateTime(account_mdm.PSPlusStartDate);
        //}
        psnAcc.PS_Plus_End_Date__c = getMyDateTime(account_mdm.PSPlusStackedEndDate); 
        System.debug('---->' + psnAcc.PS_Plus_End_Date__c);
        //psnAcc.PS_Plus_Subscription_Source__c = account_mdm.PSPlusSUbscriptionSource;
        if(account_mdm.PSPlusAutoRenewalFlag != null){
        if(account_mdm.PSPlusAutoRenewalFlag.equalsIgnoreCase('True')){
        psnAcc.Auto_Renewal__c = 'On';
        }
        else if(account_mdm.PSPlusAutoRenewalFlag.equalsIgnoreCase('False')){
        psnAcc.Auto_Renewal__c = 'Off';
        }
        }
        psnAcc.PSN_Plus_Country__c = account_mdm.PSPlusAccountCountry;
        

        try { 
          upsert psnAcc;
        } 
        catch(Exception ex) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'PSN Account could not inserted reason :'+ex.getMessage());
        }
    
        for(Address__c adrs : [select Id, MDM_ID__c  
                                from Address__c
                                where Consumer__c = :contactId
                                AND MDM_ID__c IN :addressMdmIds]) {
                                    
          existingAddressMap.put(adrs.MDM_ID__c, adrs); 
          
        }
        
        
        for(Sony_MiddlewareConsumerdata3.Address_mdm adrs : addressList){
            
          Address__c address;
          if(existingAddressMap.containsKey(adrs.AddressId)) {
            address = new Address__c(Id = existingAddressMap.get(adrs.AddressId).Id);
          } else {
            address = new Address__c(Consumer__c = contactId, MDM_ID__c = adrs.AddressId);
          }
          
          address.Address_Line_1__c = adrs.StreetAddress;
          address.Address_Line_2__c = adrs.StreetAddress2;
          address.Address_Line_3__c = adrs.StreetAddress3;
          address.Address_Type__c = adrs.ContactAddressType;
          address.City__c = adrs.City;
          address.Country__c = adrs.Country;
          address.Postal_Code__c = adrs.PostalCode;
          address.State__c = adrs.State;
          address.Address_Type__c = adrs.ContactAddressType;
          
          listOfAdressToUpsert.add(address);
          
        }
        if(!listOfAdressToUpsert.isEmpty()) {
          try { 
            upsert listOfAdressToUpsert;
          }
          catch(Exception ex) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Address could not inserted reason :'+ex.getMessage());
          }
        }
 
    return null;
  } 
   
  public PageReference createAssetRecords() {
    
    System.debug('========createAssetRecords=========');
    
    if(mdmAccountId <> null && mdmAccountId <> '') {
        
        GetAssetsController assetCtrl = new GetAssetsController(mdmAccountId, contactId);       
        assetCtrl.populateRecords();
    }
    
    return returnToContact();
  }
  
  private Date parseDate(String dateString){ // dateString is in this format "12/26/2007".
    if(dateString != null && dateString !=''){
        try{
            date assetDate = Date.parse(dateString);
            return assetDate;
        }
        catch(Exception ex){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Date Format Is InValid : '+dateString);
        }
    }
    return null;
  }
 
    public PageReference returnToContact(){
    
        system.debug('contactId:::::::: '+contactId);
        
        if(contactId != null && contactId != ''){
            
            PageReference contactPage = new ApexPages.StandardController(new Contact(id=contactId)).view();
            
         return contactPage;
        }
        
     return null;
  }
  
  public PageReference searchConsumer() {

      PageReference pg = new PageReference('/apex/SmartContactSearch');
      pg.setRedirect(true);
      return pg;
      
  }
  //Code added by Preetu to process date format:
    
    public static String getMyDateTime(string strDt){ 
      if(strDt != null && strDt !=''){ 
          try{
              String[] DTSplitted = strDT.split(' ');
              string year = DTSplitted.get(0).split('-').get(0);
              string month = DTSplitted.get(0).split('-').get(1);
              string day = DTSplitted.get(0).split('-').get(2);
      

              string stringDate = month + '/' + day + '/' + year;

              return stringDate;
           }
         catch(Exception ex){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Date Format Is InValid : '+strDt);
         }
   
      }
       return null;
         }


  private static String getLanguage(String languageCode) {
  
    System.debug('>>>>>>>>>>>>getLanguageCode:languageCode='+languageCode);
        
    map<String, Language__c> langSettingMap = Language__c.getAll();
    
    System.debug('>>>>>>>>>>>>langSettingMap='+langSettingMap);
    
    if(languageCode==null || languageCode.trim().length()==0){
        return languageCode;
    }
 
    if(langSettingMap.containsKey(languageCode)){
        return langSettingMap.get(languageCode).Description__c;
    } 
 
    return languageCode;
 
  }
 
     private String fixEmail(string email) {
        
        String newEmail;
        
        if(email==null || email.trim().length()==0){
            return email;
        }
        
        newEmail = email.replace('/','');
        
        return newEmail;
    }
  
  
  public Class WrapperContacts {
    public String mdmContactId{get;set;}
    public String mdmAccountId{get;set;}
    public String fName{get;set;}
    public String lName{get;set;}
    public String PSNHandle{get;set;}
    public String psnSignInId{get;set;}
    public String psnPhone{get;set;}
    public String psnEmail{get;set;}
    public String siebelEmail{get;set;}
    public String crmPhone{get;set;}
    public String birthDate{get;set;}
    public String PreferredLanguageCode{get;set;}
    public String sex{get;set;}
    public list<Sony_MiddlewareConsumerdata3.Address_mdm> addressList{get;set;}
    //To add PS Plus Subscriber attributes :Code changed by Preetu
    public String PSPlusExpiration{get;set;}
    public String PSPlusSubscriber{get;set;}
    //End of code change
    
    public WrapperContacts() {
        addressList = new list<Sony_MiddlewareConsumerdata3.Address_mdm>();
    }
  }
 
}