//**************************************************************************/
// (c) 2013 Appirio, Inc.
//
// Description : Test class for GetAssetsController.
//                  
// Original August 28, 2013  : KapiL Choudhary(JDC)
// 
//
// ***************************************************************************/

@isTest
global class GetAssetsController_Test {

    static testMethod void getAssetsUnitTest() {
    	OSB__c setting = new OSB__c();
    	setting.Asset_Lookup_Enabled__c = true;
    	insert setting;
    	list<Contact> contactList = new   list<Contact>();
    	contactList = TestClassUtility.createContact(1, true);
    	 Test.startTest();
         Test.setMock(WebServiceMock.class, new WebServiceMockImpl2());
    	    
        	//PageReference pageRef = Page.getAssets;
        	//Test.setCurrentPageReference(pageRef);
        	
        	
        	if(!contactList.isEmpty()){
        	  // Add parameters to page URL
        	  ApexPages.currentPage().getParameters().put('mdmAccountId','1-J1-7618');//Hardcoded Id For Remote WebService.
        	  ApexPages.currentPage().getParameters().put('contactId', contactList[0].id);
        	  GetAssetsController gAsset = new GetAssetsController();
        	  
        	  gAsset.populateRecords();
        	  //gAsset.returnToContact();
        	  
        	  
        	  GetAssetsController ctrl = new GetAssetsController('1-J1-7618', contactList[0].id);
        	  Test.stopTest();
        	}
    }
    static testMethod void getAssetsUnitTest2() {
    	OSB__c setting = new OSB__c();
    	setting.Asset_Lookup_Enabled__c = true;
    	insert setting;
    	list<Contact> contactList = new   list<Contact>();
    	contactList = TestClassUtility.createContact(1, true);
    	 Test.startTest();
         Test.setMock(WebServiceMock.class, new WebServiceMockImpl2());
        	Asset__c ast = new Asset__c();
        	ast.MDM_ID__c = '1-1-1';
        	ast.Serial_Number__c = '123123123';
        	insert ast;
        	
        	if(!contactList.isEmpty()){
        	  // Add parameters to page URL
        	  ApexPages.currentPage().getParameters().put('mdmAccountId','1-J1-7618');//Hardcoded Id For Remote WebService.
        	  ApexPages.currentPage().getParameters().put('contactId', contactList[0].id);
        	  GetAssetsController gAsset = new GetAssetsController();
        	  gAsset.populateRecords();
        	  Test.stopTest();
        	}
    }
      global class WebServiceMockImpl2 implements WebServiceMock {
	   global void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,	
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	      		Sony_MiddlewareAssetdata.PSConsumerAccountAssets_Output_element responseElm = 
	      		  	new Sony_MiddlewareAssetdata.PSConsumerAccountAssets_Output_element();
	       		
	       		
	       		Sony_MiddlewareAssetdata.Asset ast = new Sony_MiddlewareAssetdata.Asset();
	       		ast.MDMAssetRowId = '1-1-1';
	       		ast.RegistrationDate = '09/14/2013';
	       		ast.PurchaseDate = '09/14/2013';
	       		
	       		Sony_MiddlewareAssetdata.ListOfAsset lstAst = new Sony_MiddlewareAssetdata.ListOfAsset();
	       		lstAst.Asset = new list<Sony_MiddlewareAssetdata.Asset>{ast};
	       		
	       		Sony_MiddlewareAssetdata.Account acc = new Sony_MiddlewareAssetdata.Account();
	       		acc.MDMAccountRowId = '1-4EG-1';
	       		acc.ListOfAsset = lstAst;
	       		
	       		responseElm.ListOfAccount = new Sony_MiddlewareAssetData.ListOfAccount();
	       		responseElm.ListOfAccount.Account = new list<Sony_MiddlewareAssetdata.Account>{acc};
	       		
	       		response.put('response_x', responseElm); 
	   		   }
    }
}