public with sharing class IntegrationAlert {

  public static void addException(String integrationName, String details, Exception ex) {

	Integration_Alert__c newAlert = new Integration_Alert__c();

    try {

		newAlert.integration_name__c = integrationName;
		newAlert.details__c = details;
		newAlert.message__c = ex.getMessage().left(32000);
      	newAlert.stack_trace__c = ex.getStackTraceString().left(32000);
      	insert newAlert;
      	
    } catch (Exception ex2) {
    
      System.debug('\n\n>>> IntegrationAlert is swallowing exception on addException(): ' + ex2);
    
    }//end-try-catch
    
  }//end-method



  public static void addException(String integrationName, Exception ex) {

	Integration_Alert__c newAlert = new Integration_Alert__c();

    try {

		newAlert.integration_name__c = integrationName;
		newAlert.message__c = ex.getMessage().left(32000);
      	newAlert.stack_trace__c = ex.getStackTraceString().left(32000);
      	insert newAlert;
      	
    } catch (Exception ex2) {
    
      System.debug('\n\n>>> IntegrationAlert is swallowing exception on addException(): ' + ex2);
    
    }//end-try-catch
    
  }//end-method

  public static void addException(String integrationName, Exception ex, String requestMessage, String responseMessage) {

	Integration_Alert__c newAlert = new Integration_Alert__c();

    try {

		newAlert.integration_name__c = integrationName;
		newAlert.message__c = ex.getMessage().left(32000);
      	newAlert.stack_trace__c = ex.getStackTraceString().left(32000);
      	if (requestMessage!=null){
      		newAlert.request__c = requestMessage;	
      	}
      	if (responseMessage!=null){
      		newAlert.response__c = responseMessage;	
      	}
      	insert newAlert;
      	
    } catch (Exception ex2) {
    
      System.debug('\n\n>>> IntegrationAlert is swallowing exception on addException(): ' + ex2);
    
    }//end-try-catch
    
  }//end-method

  public static void addMessage(String integrationName, String message) {

	Integration_Alert__c newAlert = new Integration_Alert__c();

    try {

		newAlert.integration_name__c = integrationName;
		newAlert.message__c = message;
		insert newAlert;
 
      
    } catch (Exception ex2) {
    
      System.debug('\n\n>>> IntegrationAlert is swallowing exception on addMessage(): ' + ex2);
    
    }//end-try-catch
    
  }//end-method

}