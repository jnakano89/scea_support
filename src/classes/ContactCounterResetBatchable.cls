global with sharing class ContactCounterResetBatchable implements Database.Batchable<sobject> {
    
    public static String contactCounterQuery = 'SELECT Daily_Counter__c, Weekly_Counter__c, Blocked_Consumer__c FROM Contact ';
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = contactCounterQuery;     
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<contact> batchList) {
        String dayNow =  System.now().format('EEEE');
        try{
            for(contact con:batchList){
                if((dayNow).equalsIgnoreCase('Monday')){
                    con.Weekly_Counter__c = 0;
                }
                //reset daily counter.
                con.Daily_Counter__c = 0;
            }
            Update batchlist;
        }catch(Exception ex) {
            ExceptionHandler.logException(ex);
            Apex_Log__c al = new Apex_Log__c();
            al.Message__c = ex.getMessage();
            al.Exception_Cause__c = BC.getJobId();
            al.Class_Name__c = 'Contact Counter SFDC-Update';
            insert al;
        }
    }
    global void finish(Database.BatchableContext BC){
    }
}