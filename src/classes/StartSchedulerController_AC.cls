/******************************************************************************
Name        :   Start_Sceduler_Controller_AC
Author      :   Jeegar Brahmakshatriya
                Mark Passovoy
                
Date        :   01/10/2013
Description :   Controller for Start_Scheduler_VP page. 
                -This Class fetches the records from the Script_Manager__c 
                	object and starts the Scheduled job.
                -If the job is already running, it will show an error message.
                -If the job is already running, the Execute button will disappear
                	and the Kill Job button will show for the selected process 
                 
*******************************************************************************/
public with sharing class StartSchedulerController_AC {
    
    public List<Script_Manager__c> activeProcesses {get;set;}
    
    public Id thisId {get;set;} // Id of the script_Manager__c that was clicked
    public boolean topErrMsg {get;set;}
  	public String topErrorMessage {get;set;}
  	public boolean topConfirm {get;set;}
  	public String confirmation {get;set;}
  	private Map<Id,Script_Manager__c> activeProcessesMap; 
  	private final String confirmationMsg = ' has been successfully scheduled.';
  	private final string unschConfMsg = ' has been successfully unscheduled.';
  	public final string runningAsUserErr = 'You must run this script as '; 
    
/*
   * Method called when the page loads. 
   * This loads all the Script_Manager__c records that are 'Active'
*/
  public void loadProcesses(){
    activeProcesses = [Select id,Name, Active__c, 
                          Description__c, Job_Name__c,
                          Schedule__c, Class_Name__c, Last_Executed__c,
                          Must_Run_As__c, Scheduled_Job_Id__c, 
                          Must_Run_As__r.Name, Last_Deleted__c,
                          LastModifiedBy.Name
                           from Script_Manager__c 
                           where Active__c = true order by Job_Name__c asc];
        activeProcessesMap = new Map<Id,Script_Manager__c> (); 
      for(Script_Manager__c activeProc : activeProcesses) {
        activeProcessesMap.put(activeProc.Id,activeProc);
      }
       
  }
  
/*
   * Method tries to schedule the batch on which the Execute button is clicked. 
   * If an exception occurs, the exception is shown on the page. 
*/
  public void exectApex(){
//      String myId =Apexpages.currentPage().getParameters().get('thisId');  
    topErrMsg = false;
    topConfirm = false;
    topErrorMessage = '';
    Script_Manager__c activeProcess;
    String jobName; 
    String schedule;
    String className;
    string mustRunAs;
    
    if(activeProcessesMap.size() > 0 && thisId != null) {
        
        activeProcess = activeProcessesMap.get(thisId);
        if(thisID != null
            && activeProcess.Must_Run_As__c != null
            && !String.valueOf(activeProcess.Must_Run_As__c).equals(
              Userinfo.getUserId())) {
          topErrMsg = true;
          topErrorMessage = runningAsUserErr + activeProcess.Must_Run_As__r.Name;
          return;
      }
        jobName = activeProcess.job_Name__c;
        schedule = activeProcess.schedule__c;
        className = activeProcess.class_name__c; 
        System.debug('className = ' + className);
        Type myType = Type.forName(className);
        if(myType == null) {
        	//system.debug('Wrong class');
        	topErrMsg = true;
        	topErrorMessage = 'Incorrect Class Name.';
        } else {
	    	  Schedulable cds = (Schedulable)myType.newInstance();
	        try {
	          String jobId = System.schedule(jobName, schedule, cds);
	          //System.debug('Is This Job Id? ' + jobId);
	          if(jobId != null) {
	          	activeProcess.Scheduled_Job_Id__c = Id.valueOf(jobId);
	          	activeProcess.Last_Executed__c = system.now();
	          	update activeProcess;
	          }
	        } catch (Exception ex) {
	            topErrMsg = true; 
	            topErrorMessage = ex.getMessage();
	            System.debug('####'+ex.getMessage());
	        }
        }
        if(!topErrMsg) {
            topConfirm = true;
            
            confirmation = activeProcess.Job_Name__c + confirmationMsg;
        } else {
            topConfirm = false; 
        }
    }
  }
  
  
 /*
   * When a job is currently running and was scheduled through the 
   * script scheduler, the Execute button will disappear and
   * show the 'Kill Job' button which will kill that process.
   *
   * If the job was created using Execute Anonymous or Dev Console,
   * this process will not work.
*/
  public void unscheduleApexJob(){
    topErrMsg = false;
    topConfirm = false;
    topErrorMessage = '';
    Script_Manager__c activeProcess;
    
    if(activeProcessesMap.size() > 0 && thisId != null) {
      activeProcess = activeProcessesMap.get(thisId);
    }
    
    if(activeProcess != null && activeProcess.Scheduled_Job_Id__c != null
        && activeProcess.Scheduled_Job_Id__c != '' ) {
      try {
      	  system.abortJob(activeProcess.Scheduled_Job_Id__c);
	      activeProcess.Scheduled_Job_Id__c = null;
	      activeProcess.Last_Deleted__c = system.now();
	      update ActiveProcess;
	      
	      topConfirm = true;
	      confirmation = activeProcess.Job_Name__c + unschConfMsg;
      } catch (Exception ex) {
      	topErrMsg = true; 
      	topErrorMessage = ex.getMessage();
      }
    }
  } 
  
}