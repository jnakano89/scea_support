public class LookUpFiledUpdate{
    
    // Used in the Trigger on before insert and before update actions.(consumer__r.AccountId)
    public void setConsumerLookupPSNID(map<Id,PSN_Account__c> newPSNAccount) {
        map<Id,String> accountToPsnOnlineId = new Map<Id,String>();        // this will contain mapping of accountid and psnlineid
        List<Account> psnAccountsToBeupdated = new List<Account>(); 
        set<ID> personcontactidValue = new set<Id>();
        
         
        for(PSN_Account__c psnAccountObj : [select Id,consumer__r.AccountId,Name from PSN_Account__c where id in:newPSNAccount.keySet()]){
           
           personcontactidValue.add(psnAccountObj.consumer__c);
           system.debug('psnAccountObj-----' + psnAccountObj);
           accountToPsnOnlineId.put(psnAccountObj.consumer__r.AccountId,psnAccountObj.Id);
        }  
        System.debug('accountToPsnOnlineId------' +accountToPsnOnlineId);
                   
            for(Id accountId : accountToPsnOnlineId.keySet()){
            String psnonlineId = accountToPsnOnlineId.get(accountId);
            Id i = Id.valueOf(psnonlineId);
            psnAccountsToBeupdated.add(new Account(Id=accountId,PSN_Account__C = i));
            //Account accountObject = new Account(Id= accountId,PSN_Account__C = i);                  
            //psnAccountsToBeupdated.add(accountObject);
         }
        update psnAccountsToBeupdated;
    
     
       
   
}
}