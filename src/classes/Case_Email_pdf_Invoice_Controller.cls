/*******************************************************************************************************************
Class         : Case_Email_pdf_Invoice_Controller
Description   : Class to construct print invoice email
Developed by  : Appirio

Updates       : 05/02/14 - Aaron Briggs - SF-682 - Addressed Merge Fields Not Resolving
                05/06/14 - Aaron Briggs - SF-682 - Allow Non-OOW Service Cases to Print Invoices
*******************************************************************************************************************/
public with sharing class Case_Email_pdf_Invoice_Controller {
    public Case aCase{get;set;}
    public Id TemplateId
    {
        get{            
            return ApexPages.currentPage().getParameters().get('templateId');           
        }
    } 

    public Case_Email_pdf_Invoice_Controller(ApexPages.StandardController s) {
        aCase = (Case)s.getRecord();
        //AB - 05/06/14 - Modified SOQL query to exclude Order__c; Order not necessary for Invoice.
        //aCase = [select id, ContactId, Order__c from Case where Id=:aCase.id];
        aCase = [select id, ContactId from Case where Id=:aCase.id];
        
        //AB - 05/02/14 - Handled exception for no order number being associated.
        //if(aCase.Order__c==null){
        //    parsedEmailTemplate = 'No Order Found. Case must have an order to Print Invoice.';
        //} else{
        parsedEmailTemplate='';
        //}          
    }

    public string parsedEmailTemplate{get;set;}

    public void updateParsedTemplate(){
        string templateBody='';
        List<EmailTemplate> Template = [Select e.Markup, e.body, e.HtmlValue From EmailTemplate e where Id=:TemplateId limit 1];
        system.debug('***********11:'+TemplateId+'::'+aCase.ContactId+'>>'+aCase.Id);

        if(Template.size() != 0){
            templateBody = Template[0].HtmlValue;//Template[0].HtmlValue;  
            map<string, string>mapAliasNames= new map<string, string>();

            if((parsedEmailTemplate==null|| parsedEmailTemplate.trim()=='') && Template[0].body!=null){                 
                    EmailTemplateProcessor eTemplateProcessor = new EmailTemplateProcessor(Template[0].HtmlValue);
                    eTemplateProcessor.parseTemplate();
                    map<string, string>mapObject_Name_Id = new map<string, string>();   
                    mapObject_Name_Id.put('user', ' Id =\''+UserInfo.getUserId()+'\'');

                    if(aCase.ContactId!=null){
                        mapObject_Name_Id.put('contact', ' Id =\''+aCase.ContactId+'\'');
                    }

                    if(aCase.Id!=null){
                        mapObject_Name_Id.put('case', ' Id =\''+aCase.Id+'\'');
                    }

                    //AB - 05/06/14 - Commented the following lines; Order not necessary for Invoice.
                    //if(aCase.Order__c!=null){
                    //    mapObject_Name_Id.put('Order__c', ' Id =\''+aCase.Order__c+'\'');
                    //}

                    parsedEmailTemplate = eTemplateProcessor.fillTemplate(mapObject_Name_Id);
                    system.debug('---------------------1>' + parsedEmailTemplate);      
            }
        }

        if(String.isEmpty(parsedEmailTemplate)){
            parsedEmailTemplate = 'No Email Template found for print.';
        }

        //AB - 05/02/14 - Commented the following line to allow the template merge fields to populate
        //parsedEmailTemplate = templateBody ;
        system.debug('---------------------2>' + parsedEmailTemplate); 
    }

    public static map<string, string> getTagParam(string checkTag, string zTemplateBody){     
        map<string, string>mapAliasNames= new map<string, string>();
        Integer indexStart= zTemplateBody.indexOf('<'+checkTag);
        if(indexStart!=-1){
            string MessageHeader='';
            Integer indexlast=zTemplateBody.indexOf('>', indexStart);
            if(indexlast!= -1){                 
                system.debug('^^^^^^^:'+indexStart+':'+indexlast+'::'+checkTag.length()+'<>'+zTemplateBody.length()+'>>>>>>>'+(indexStart+checkTag.length())+':'+(indexlast-checkTag.length()));
                MessageHeader=zTemplateBody.substring(indexStart+checkTag.length(),indexlast);
                List<string> TemplateParamTokens =MessageHeader.split(' ', 0);
                for(string zToken:TemplateParamTokens){
                    List<string> splitToken= zToken.split('=');
                    if(splitToken.size()>1){
                        system.debug('*************::'+splitToken);
                        string paramName= splitToken[0];
                        string paramValue= splitToken[1];
                        paramValue = paramValue.replaceAll('"', '');
                        if(paramName.endsWith('type')){
                            paramName = paramName.substring(0, paramName.length()-4);
                        }
                        mapAliasNames.put(paramName, paramValue);
                        system.debug('*************::<>::'+mapAliasNames);
                    }
                }
            }
        }       
        return mapAliasNames;
    }
    public static string getTagContent(string checkTag, string zTemplateBody, string orgTemplateBody){
        Integer indexStart= zTemplateBody.indexOf('<'+checkTag);
        if(indexStart!=-1){
            string MessageHeader='';
            Integer indexlast=zTemplateBody.indexOf('>', indexStart);
            if(indexlast!= -1){                 
                MessageHeader=zTemplateBody.substring(indexStart+checkTag.length(),indexlast);
                

                Integer indexEndTagStart= zTemplateBody.indexOf('</'+checkTag, indexlast);
                if(indexEndTagStart!=-1){
                    return orgTemplateBody.substring(indexlast+1,indexEndTagStart);
                }
            }
        }
        return '';
    }

}