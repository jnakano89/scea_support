global with sharing class SocialPersonaController {
		
	//constructor
	public SocialPersonaController(){}
		     
    @RemoteAction
    global static String checkSocialPersona(String twitterHandle, String hostAccountId){    	
    	String hostContactId = getContactFromAccount(hostAccountId);    	
    	String returnMessage = 'Nothing';
    	Boolean hasTwitterAcc = false;
    	String oldContactId = null;
    	if(!String.isNotBlank(twitterHandle)){
    		returnMessage = 'This link is for Social to Chat Only.'; 
    		return returnMessage;
    	}
    	Integer checkSpCount = [Select  count() from SocialPersona where name =:twitterHandle];    	    	
    	if(checkSpCount>0){
    		list<SocialPersona> spList = [Select  id, ParentId from SocialPersona where name =:twitterHandle];    		
    		for(SocialPersona spAcc : spList){
    			if(String.isNotBlank(spAcc.ParentId)){
    				if(checkParentId(spAcc.ParentId)){    		    					
    					if(spAcc.ParentId==hostContactId){
    						returnMessage = 'This Account Has a valid Twitter Acc.';
    						hasTwitterAcc = true;    						
    					}
    				}else{    		    					
    					if(spAcc.ParentId==hostAccountId){
    						returnMessage = 'This Account Has a valid Twitter Acc.';
    						hasTwitterAcc = true;
    					}
    				}    				
    			}    			
    		}     		
    		if(!hasTwitterAcc){
    			if(spList.size() == 1){    				    				
    				if(checkParentId(spList.get(0).ParentId)){    			    					
    					oldContactId = spList.get(0).ParentId;
    				}else{    					    					
    					oldContactId = getContactFromAccount(spList.get(0).ParentId);
    				}
    				//spList.get(0).ParentId = hostContactId;
    				spList.get(0).ParentId = hostAccountId;
    				update spList.get(0);       				 				
    				mvCasesToNewSp(hostContactId, oldContactId, hostAccountId);     				
    				returnMessage = 'This Account Has a valid Twitter Acc. NOW';
    			}else{
    				returnMessage = 'This Account Has a valid Twitter Acc. If you need to change Delete the current Acc and try again.';    				
    			}    			
    		}      		
    	}else{
    		returnMessage = 'No Valid Social Persona Found.';
    	}    	 	    	
  		return returnMessage;  		        				    	    	
    }
    
    static void mvCasesToNewSp(String hostContactId, String oldContactId, String hostAccountId){      	  
    	list<Case> caseList = new List<Case>();
    	list<Account> accList = new List<Account>();
    	//Integer caseListCount = [Select  count() FROM Case where accountid =:oldAccountId and origin = 'Social'];
    	Integer caseListCount = [Select  count() FROM Case where ContactId  =:oldContactId and origin = 'Social'];    	    	
    	if(caseListCount > 0){
    		//caseList = [ SELECT id, CaseNumber, accountid, origin FROM Case where accountid =:oldAccountId and origin = 'Social'];
    		caseList = [ SELECT id, CaseNumber, accountid, origin FROM Case where ContactId =:oldContactId and origin = 'Social'];
    		if(caseList.size() > 0){
    			for(Case caseObj : caseList){
    				caseObj.AccountId = hostAccountId;
    				caseObj.ContactId = hostContactId;    				
    				/*accList = [Select personContactId, Id from Account where Id =: hostAccountId order by LastModifiedDate ASC Limit 1];					
					if(accList.size() > 0){
						for(Account a : accList){
							caseObj.ContactId = a.personContactId;
						}
					}*/																											    				    				
    				update caseObj;
    			}    			
    		}    		
    	}    	    	
    } 
    
    static String getContactFromAccount(String sentAccId){
    	List<Account> accSourceList = [select PersonContactId, id from Account where id =: sentAccId limit 1];
    	String returnConId = null;
    	if(accSourceList.size() > 0){
    		returnConId =  accSourceList.get(0).PersonContactId;
    	}
    	return returnConId;
    }
    
    static Boolean checkParentId(String parentIdRef){    	
    	Boolean isContact = false;
    	Integer accList1Count = [Select count() from Account where Id =: parentIdRef ];
		Integer conList1Count = [Select count() from Contact where Id =: parentIdRef ];
		if(accList1Count > 0){			
			isContact = false;
			return	isContact;		
		}
		if(conList1Count > 0){			
			isContact = true;
			return	isContact;
		}
		if(conList1Count == 0 && accList1Count == 0){			
			System.debug('There was no Contact or Account ');					
		}    	    	    	
		return null;
    }
               
}