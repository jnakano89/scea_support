//**************************************************************************/
// (c) 2013 Appirio, Inc.
//
// Description : Test Class For EmailMessageTrigger.
//                  
// Original October 25, 2013  : KapiL Choudhary(JDC) Created for the Task T-204275
// Updated : 
//		
// ***************************************************************************/

@isTest() 
private class EmailMessageTrigger_Test {
 
 static testMethod void emailMessageTest() {
 	EmailMessage eMessage = new EmailMessage();
 	eMessage.FromAddress = 'Testmail@test.com';
 	try{
 	insert eMessage;
 	}
 	catch(exception ex){
 		system.assertEquals(true, ex.getMessage().contains(label.Invalid_From_Email));
 	}
 }
}