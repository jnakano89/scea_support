/***********************************************************************************************************************************************
Class           : sObjectUtility
Description     : Standard Object Utility Class
Developer       : Appirio
Modified        :
                : 10/28/2014 : Aaron Briggs : SF-371 : Clone AccConsumer Method
                : 02/20/2015 : Aaron Briggs : SMS-996 : Create getExactAddress Method
                : 06/16/2015 : Leena Mandadapu : SMS-1384 : Added fields to the SOQL as City,State are showing null in the Cybersource post method in WebRMA controller
********************************************************************************************************************************************/
public without sharing class  sObjectUtility {

    /*
     *  Create Contact SFDC sObject from input request.
     */
    public static Account getAccConsumer(WebRMAServiceRequestProcess.ServiceRequestInput input){
        return getAccConsumer( input.firstName, input.lastName, input.phoneNumber, input.email);
    }
    public static Account getAccConsumer(CreatePPPOrderService.WebServiceInput input){
        return getAccConsumer( input.firstName, input.lastName, input.phone, input.email);
    }
    static String getValidKey(List<string>values){
        String Result = '';
        for(String val : values){
            if(!String.isEmpty(val)){
                Result = Result+val;
            }
            Result = Result+'-';                
        }
        return Result;
    }
    //AB - 10/31/2014 - New method to support more stringent duplicate user validation
    public static Account getAccConsumer(String firstName, String lastName, String phoneNumber, String email, String channel){
        system.debug('==============================> firstName: ' + firstName + ' > lastName: ' + lastName + ' > phoneNumber: ' + phoneNumber + ' > email: ' + email + ' > channel: ' + channel);
        String personAccountRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        List<Account> acctList = new List<Account>();
        Account consumerAcct = new Account();

        if(firstName != null && lastName != null && phoneNumber != null && email != null && channel != null){
        	system.debug('==============================> No Null Values');
        	if(channel == 'WebRMA'){
        		system.debug('==============================> Channel is WebRMA');
	            acctList = [SELECT id, FirstName, LastName, Phone, PersonEmail, PersonContactId 
	                                FROM Account 
	                                WHERE (FirstName =: firstName AND LastName =: lastName AND PersonEmail =: email AND Phone =: phoneNumber AND IsPersonAccount = true)
	                                LIMIT 1];
        	}
        }
        
        system.debug('==============================> acctList.size(): ' + acctList.size());
        if(acctList.size() > 0){
            consumerAcct = acctList.get(0);
        }else{
            consumerAcct.RecordTypeId = personAccountRecId;
            consumerAcct.FirstName = firstName;
            consumerAcct.LastName = lastName;
            consumerAcct.Phone = phoneNumber;
            consumerAcct.PersonEmail = email;
            insert consumerAcct;
        }
        system.debug('==============================> consumerAcct: ' + consumerAcct);
        return consumerAcct;
    }
    public static Account getAccConsumer(String firstName, String lastName, String phoneNumber, String email){

        String personAccountRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

        Account accConsumer = new Account();
        map<String, Account>consumerMap = new map<String, Account>();
        if(email <> null){
            for(Account acc : [select id, FirstName, LastName, Phone, PersonEmail, Bill_to__pc, PersonContactId 
                              from Account 
                              where (PersonEmail=:email AND IsPersonAccount = true ) 
                              /* OR (FirstName =: firstName AND LastName = : lastName AND Phone = : phoneNumber AND IsPersonAccount = true) */
            				  /* OR (FirstName =: firstName AND LastName =: lastName  AND IsPersonAccount = true)*/
            				]){
                //consumerMap.put(getValidKey(new String[]{firstName, lastName, phoneNumber}), acc);
                consumerMap.put(getValidKey(new String[]{email}), acc);
                
                // DEMO Feedback Changed by CM 
                // consumerMap.put(getValidKey(new String[]{firstName, lastName}), acc);       
            }
            if(consumerMap.size()>0){
                String cntKey = '';
                if(!String.isEmpty(email)) {
                    cntKey = getValidKey(new String[]{email});
                    accConsumer = consumerMap.get(cntKey);
                }
            }
        }
        System.Debug('********** AccConsumer @@  ' +AccConsumer);
        if(accConsumer!=null && accConsumer.id<>null){
            return accConsumer;

        }else{
            accConsumer.RecordTypeId = personAccountRecId;
            accConsumer.FirstName = FirstName;
            accConsumer.LastName =lastName;
            accConsumer.Phone =phoneNumber;
            accConsumer.PersonEmail = Email;

            insert accConsumer;
            
             System.Debug('>>>>>>AccConsumer<<<<<< @@  ' +AccConsumer);
                    System.Debug('>>>>>>AccConsumer.PersonContactId<<<<<<@@   ' +AccConsumer.PersonContactId);
                    
            return accConsumer;
        } 
    }
    public static Contact getContact(WebRMAServiceRequestProcess.ServiceRequestInput input){
        return getContact( input.firstName, input.lastName, input.phoneNumber, input.email);
    }
    public static Contact getContact(String firstName, String lastName, String phoneNumber, String email){
        Contact contact = null;
        if(!String.isEmpty(firstName) && !String.isEmpty(lastName)&& !String.isEmpty(phoneNumber)){
            //Search for existing contact.
            for(Contact objContacts : [select id, FirstName, LastName, Phone, Email 
                                       from Contact where email=:email or (FirstName =: firstName AND LastName =: lastName )or (FirstName =: firstName AND LastName =: lastName AND Phone =: phoneNumber) ]){
                if(String.isEmpty(objContacts.Email)){
                    contact = objContacts;//Found macth with FirstName, LastName, PhoneNumber.
                    contact.Email = Email;
                }else{
                    //If matched with email, got exact-match contact for email-prefered.
                    if(Email.equalsIgnoreCase(objContacts.Email)){
                        contact = objContacts;
                        break;
                    }
                }
            }
        }
        if(contact==null){
            contact = new Contact();
        }
        if(!String.isEmpty(Email)){
            //Set email, if not set.
            if(contact.Email==null){
                contact.Email = Email;
            }
        }
        //Create new contact as Contact now found.
        if(!String.isEmpty(firstName) ){            
            contact.FirstName = firstName;
        }
        if(!String.isEmpty(lastName) ){

            contact.LastName = lastName;
        }
        if(!String.isEmpty(phoneNumber) ){
            contact.Phone = phoneNumber;
        }
        upsert contact;
        return contact;
    }

    /*
     *  Create Address SFDC sObject from input request.
     */ 
    public static Address__c getAddress(Contact contact, String billingAddress1,  String billingAddress2,  String billingCity, 
            String billingState, String billingCountry, String billingZipCode, String Address_Type){
        Id contactId = null;
        if(contact!=null){
            contactId = contact.id;
        }
        return getAddress_deDups(contactId, billingAddress1,  billingAddress2,  billingCity, 
                billingState, billingCountry, billingZipCode, Address_Type);
    }
    public static Address__c getAddress_deDups(Id contactId, String billingAddress1,  String billingAddress2,  String billingCity, 
            String billingState, String billingCountry, String billingZipCode, String Address_Type){

        for(Address__c add : [select Id, Consumer__r.Email, Consumer__c, Address_Line_1__c, Postal_Code__c  from Address__c where Address_Line_1__c =: billingAddress1 
                AND Postal_Code__c =:billingZipCode AND Address_Line_1__c!= NULL and Postal_Code__c!=NULL]) {
            if(contactId!=null && add.Consumer__c==null){
                //Reset the address info, for  Contact not set.
                add.Consumer__c = contactId;
            }                                   
            return add;
        }
        return getAddress(contactId, billingAddress1,  billingAddress2,  billingCity, billingState, billingCountry, billingZipCode, Address_Type);
    }
    public static Address__c getAddress(Id contactId, String billingAddress1,  String billingAddress2,  String billingCity, 
    String billingState, String billingCountry, String billingZipCode, String Address_Type){

      System.Debug('>>>>>>>>>>>+contactId<<<<<<<<<< ' +contactId);
      
      Address__c billToAddress = null;
      Address__c shipToAddress = null;
      
      
    if (Address_Type=='Shipping'){
        
        
       
       for(Address__c add : [select id, Address_Line_1__c, Postal_Code__c, Address_Line_2__c, City__c, State__c, Country__c
                              from Address__c where Address_Line_1__c=:billingAddress1 
                              AND Postal_Code__c =:billingZipCode]) {

            shipToAddress=add;
        }

        if(shipToAddress==null || shipToAddress.Id==null){

            if(!String.isEmpty( billingAddress1) || !String.isEmpty( billingAddress2) || !String.isEmpty( billingCity)
                    || !String.isEmpty( billingState)|| !String.isEmpty( billingCountry)|| !String.isEmpty( billingZipCode)
                    ){

                shipToAddress = new Address__c();
                shipToAddress.Address_Line_1__c = billingAddress1;
                shipToAddress.Address_Line_2__c = billingAddress2;
                shipToAddress.City__c = billingCity;
                shipToAddress.State__c = billingState;
                shipToAddress.Country__c = billingCountry;
                shipToAddress.Postal_Code__c = billingZipCode;

                if(contactId!=null){
                    shipToAddress.Consumer__c = contactId;
                }

                shipToAddress.Address_Type__c = Address_Type;               
            }
        }

        return shipToAddress;
       
       } else if (Address_Type=='Billing'){
       
      
       
       for(Address__c add : [select id, Address_Line_1__c, Postal_Code__c, Address_Line_2__c, City__c, State__c, Country__c
                              from Address__c where Address_Line_1__c=:billingAddress1 
                              AND Postal_Code__c =:billingZipCode]) {

            billToAddress=add;
        }

        if(billToAddress==null || billToAddress.Id==null){

            if(!String.isEmpty( billingAddress1) || !String.isEmpty( billingAddress2) || !String.isEmpty( billingCity)
                    || !String.isEmpty( billingState)|| !String.isEmpty( billingCountry)|| !String.isEmpty( billingZipCode)
                    ){

                billToAddress = new Address__c();
                billToAddress.Address_Line_1__c = billingAddress1;
                billToAddress.Address_Line_2__c = billingAddress2;
                billToAddress.City__c = billingCity;
                billToAddress.State__c = billingState;
                billToAddress.Country__c = billingCountry;
                billToAddress.Postal_Code__c = billingZipCode;
   
                if(contactId!=null){
                    billToAddress.Consumer__c = contactId;
                }

                billToAddress.Address_Type__c = Address_Type;               
            }
        }

        return billToAddress;
       
       } else return null;
     
    }
    //AB - 02/20/15 - Address Must Match Exactly or New Address Created
    public static Address__c getExactAddress(Id contactId, String address1,  String address2,  String city,
        String state, String country, String zipCode, String addressType){

	    system.debug('==============================> contactId: ' + contactId);
	    system.debug('==============================> address1: ' + address1);
	    system.debug('==============================> address2: ' + address2);
	    system.debug('==============================> city: ' + city);
	    system.debug('==============================> state: ' + state);
	    system.debug('==============================> country: ' + country);
	    system.debug('==============================> zipCode: ' + zipCode);
	    system.debug('==============================> addressType: ' + addressType);
	    
	    //LM : 06/16/2015 : Added City, County and State to the SOQL
	    List<Address__c> addList = new List<Address__c>([SELECT Id, Consumer__c, Address_Line_1__c, Postal_Code__c,City__c,Country__c,State__c FROM Address__c WHERE Address_Line_1__c =: Address1.normalizeSpace() 
	                            AND Postal_Code__c =:ZipCode AND City__c =:City AND State__c =:State AND Country__c =:Country AND Consumer__c =:contactId LIMIT 1]);
	                            
	    system.debug('==============================> addList.size(): ' + addList.size());
	    system.debug('==============================> addList: ' + addList);
	    
	    if(addList.size() > 0) {
	        system.debug('==============================> Address Returned: ' + addList[0]);
	        return addList[0];
        }
        
        Address__c addr = null;
        
        if(!String.isEmpty(address1) && !String.isEmpty(city) && !String.isEmpty(state) && !String.isEmpty(country) && !String.isEmpty(zipCode) && contactId <> null){
			addr = new Address__c();
			addr.Address_Line_1__c = address1;
			addr.Address_Line_2__c = address2;
			addr.City__c = city;
			addr.State__c = state;
			addr.Country__c = country;
			addr.Postal_Code__c = zipCode;
			addr.Consumer__c = contactId;
			addr.Address_Type__c = addressType;
			system.debug('==============================> addr: ' + addr);

            try{
                if(addr <> null){
                    insert addr;
                }
            }catch(DMLException e){
            	system.debug('==============================> Unable to Insert Address: ' + e);
                return null;
            }
            return addr;
        }
        else{
            return null; 
        }
    }

    /*
     *  Create Asset SFDC sObject from input request.
            webservice string systemType;               // [Asset__c.Product_Genre__c]
            webservice string modelNumber;          // [Asset__c.Model_Number__c]
            webservice string serialNumber;         // [Asset__c.Serial_Number__c]
            webservice Date purchaseDate;               // [Asset__c.Purchase_Date__c]
     */ 
    public static Asset__c getAsset(WebRMAServiceRequestProcess.ServiceRequestInput input, Account consumer){
        //For WebRMA PPPSKU =Modelno, and product.Sub-type = hardware 
        
        return getAsset(input.systemType, input.serialNumber, '', input.modelNumber, input.purchaseDate, input.Target_Date, consumer, 'hardware');
         
        // return new Asset__c ();
    }
    public static Asset__c getAsset(CreatePPPOrderService.WebServiceInput input, Account Consumer){
        //For PPP product.Sub-type = hardware 
        return getAsset(input.systemType, input.serialNumber, input.PPPSKU, input.modelNumber, input.proofOfPurchaseDate, input.Target_Date, consumer, 'ESP');
    }
    public static Asset__c getAsset(String systemType, String serialNumber, String PPPSKU, String modelNumber, Date purchaseDate, Date TargetDate, Account consumer, String Sub_type){
        
        Asset__c asset = null;
        Product__c HardWareProduct;
        Product__c PPP_Product;
        
        if(!String.isEmpty( systemType) || !String.isEmpty( serialNumber) || !String.isEmpty( modelNumber)  || purchaseDate!=null){ 
            asset = new Asset__c();
            
            if(PPPSKU!=null && !String.isEmpty(PPPSKU) && PPPSKU!=''){
                
                // Query PPP Product
                try{
                PPP_Product = [select Id,SKU__c from Product__c where SKU__c =: PPPSKU and (sub_type__c ='ESP' OR sub_type__c='ESP_CANADA' OR sub_type__c='AD') limit 1];
                }catch (QueryException e){
                
                System.Debug('>>>>>>>+QueryException e<<<<<<<<<<  ' +e);
                }
                
                if(PPP_Product<>null){asset.PPP_Product__c= PPP_Product.id;}
            
            }
                
                if(modelNumber!=null &&!String.isEmpty(modelNumber)){
                    
                try{    
                
                HardWareProduct = [select Id,SKU__c from Product__c where SKU__c =: modelNumber and sub_type__c ='Hardware' limit 1];
                
                }catch (QueryException e){
                
                System.Debug('>>>>>>>+QueryException e<<<<<<<<<<  ' +e);
                }
                
                    if(HardWareProduct<>null){asset.Product__c=HardWareProduct.id;}
                
                }
                
                //Product SKU__c equalient to modelNumber.
                /*List<Product__c>products = [select Id,SKU__c from Product__c where SKU__c =: PPPSKU and sub_type__c =:Sub_type limit 1];
                if(products.size()>0){
                    if('hardware'.equalsIgnoreCase(Sub_type)){
                        //For RMA lookup
                        //Update the Lookup.
                        asset.Product__c= products[0].id;
                    }
                    if('ESP'.equalsIgnoreCase(Sub_type)){
                        //Update Both Lookup, for PPP.
                        asset.Product__c= products[0].id;
                        asset.PPP_Product__c= products[0].id;
                    }

                    System.Debug('>>>>><<<<< +asset.PPP_Product__c' +asset.PPP_Product__c);
                }else{
                    asset.addError('Unable to create Asset. Unable to find a product with modelNumber ='+modelNumber);
                } */
            

            
            
            asset.Serial_Number__c = serialNumber;
            asset.Model_Number__c = modelNumber;
            asset.Purchase_Date__c = purchaseDate;
            
            System.Debug('<<<<<<+Sub_type>>>>>> ' +Sub_type);
            System.Debug('<<<<<<<+asset.PPP_Product__c>>>>>>>>' +asset.PPP_Product__c); 
                    
            if(asset.PPP_Product__c!=null && Sub_type=='ESP'){
                asset.PPP_Purchase_Date__c=TargetDate;
                asset.PPP_Status__c='Pending Confirm';
                asset.Asset_Status__c='Active';
                asset.Coverage_Type__c='';
            }
            insert asset;
            //Create AssetConsumer.
            //Create Asset and Consumer mapping object.
            Consumer_Asset__c consumerAsset = new Consumer_Asset__c();
            consumerAsset.Asset__c = asset.id;
            consumerAsset.Consumer__c = consumer.PersonContactId;
            insert consumerAsset;
        }

        return asset;
    }

    /*
        Create Order__c SFDC sObject from input request.

            webservice string Order_Number;         // (Order.External_Order_Number__c)/External_ID__c
            webservice string Target_Date;          // (Order.Order_Date__c)
     */
    public static Order__c getOrder(WebRMAServiceRequestProcess.ServiceRequestInput input, Case objCase, Asset__c asset, Address__c shipToAddress, Account consumer, String PPPOrderSource){
        return getOrder(input.Target_Date, objCase, asset, PPPOrderSource,null,  shipToAddress, consumer);
    }
    public static Order__c getOrder(WebRMAServiceRequestProcess.ServiceRequestInput input, Case objCase, Asset__c asset){
        return getOrder(input.Target_Date, objCase, asset, null);
    }
    public static Order__c getOrder(CreatePPPOrderService.WebServiceInput input, Case objCase, Asset__c asset, String PPPOrderSource){
        return getOrder(input.Target_Date, objCase, asset, PPPOrderSource, input.Order_Number);
    }
    public static Order__c getOrder(CreatePPPOrderService.WebServiceInput input, Case objCase, Asset__c asset, String PPPOrderSource, Account consumer){
        return getOrder(input.Target_Date, objCase, asset, PPPOrderSource, input.Order_Number, null, consumer);
    }
    public static Order__c getOrder(Date Target_Date, Case objCase, Asset__c asset, String PPPOrderSource){
        return getOrder(Target_Date, objCase, asset, PPPOrderSource, '');
    }
    public static Order__c getOrder(Date Target_Date, Case objCase, Asset__c asset, String PPPOrderSource, String Order_Number){
        return getOrder(Target_Date, objCase, asset, PPPOrderSource, Order_Number, null, null);
    }
    public static Order__c getOrder(Date Target_Date, Case objCase, Asset__c asset, String PPPOrderSource, String Order_Number, Address__c shipToAddress, Account consumer){

        Order__c order = new Order__c();
        order.Order_Date__c = Target_Date;
        order.External_ID__c = Order_Number;
        if(objCase!=null){
            order.Case__c = objCase.id;
        }
        if(asset!=null){
            order.Asset__c = asset.id;
        }
        if(shipToAddress!=null){
            order.Ship_To__c = shipToAddress.id;
        }
        if(consumer!=null){
            order.Consumer__c = consumer.personcontactid;
        }
        // Added by CM
        if (!String.isEmpty(PPPOrderSource)){

            if(PPPOrderSource=='WebPPP'){
                order.Order_Origin_Source__c='PS';
            }

            if(PPPOrderSource=='SEL'){
                order.Order_Origin_Source__c='S';
            }

            if(PPPOrderSource=='PSN'){
                order.Order_Origin_Source__c='P';
            }

            if(PPPOrderSource=='Siebel'){
                order.Order_Origin_Source__c='1';
            }

            if(PPPOrderSource=='SFDC'){
                order.Order_Origin_Source__c='CS';
                order.Esp_Record_Type__c = null;
                order.Order_Status__c='Open';
                order.Order_Type__c ='Out Of Warranty';
            }

            if(PPPOrderSource=='GameStop'){
                order.Order_Origin_Source__c='G';
            }

        }

        insert order;
        system.debug('********order Id::'+order.id+' >> '+order.External_ID__c);
        return order;
    }

    /*
     *  Create Case SFDC sObject from input request.
        webservice string External_SR_Number;   // [Case.External_SR_Number__c]
        webservice string issueType;                // [case.Sub_Area__c]
        webservice string issueDescription;     // [Case.Feed]
        webservice string warrantyStatus;       // [Case.Status]     
     */ 
    public static Case getCase(WebRMAServiceRequestProcess.ServiceRequestInput input, Address__c shipToAddress, Id ContactId, Asset__c asset){
        return getCase(input.issueDescription, input.warrantyStatus, input.issueType, input.External_SR_Number, shipToAddress, ContactId, asset, input.systemType);
    }
    public static Case getCase(String issueDescription, String warrantyStatus, String issueType, String External_SR_Number,
            Address__c shipToAddress, Contact contact, Asset__c asset){
        return getCase(issueDescription, warrantyStatus, issueType, External_SR_Number,
                shipToAddress, contact.id, asset, '');
    }
    public static Case getCase(String issueDescription, String warrantyStatus, String issueType, String External_SR_Number,
        Address__c shipToAddress, id ContactId, Asset__c asset, String systemType){
        Case objCase = null;
        system.debug('**** Case Inside');
        if(ContactId!=null || asset!=null){
            objCase = new Case();
            //Setting the Product picklist on Case.//T-242757
            if(asset!=null && asset.Product__c!=null){
                List<Product__c>products = [select Id,SKU__c,Genre__c from Product__c where id=:asset.Product__c  limit 1];
                if(products.size()>0){
                    //Update the Lookup.
                    objCase.Product__c = products[0].Genre__c;
                }
            }
            objCase.ASB_Type__c = 'Standard';
            if(ContactId!=null ){
                //Fix : objCase.Contact = contact,, Stoped working some reason
                objCase.ContactId = ContactId;
            }
            if(shipToAddress !=null){
                objCase.Ship_To__c = shipToAddress.id;
            }
            if(asset!=null){
                objCase.Asset__c = asset.id;
            }
            List<RecordType> recTypes = [select id,Name from RecordType where name ='Service' and SobjectType='case' limit 1];
            if(recTypes.size()>0){
                objCase.recordTypeID = recTypes[0].id;
            }else{
                objCase.addError('Unable to create Service-Request-Case. The \'Service\' record not defined on Case.');
            }
            //Commented by Ranjeet for T-242390, Do not Map Warranty  Status to Case Status picklist field.
            //objCase.Status = warrantyStatus;
            objCase.Sub_Area__c = issueType;
            objCase.External_SR_Number__c = External_SR_Number;
            insert objCase;
            if(!String.isEmpty(issueDescription)){
                //Only One Feed expected from API for Case.
                FeedItem csFeed = New FeedItem( );
                csFeed.ParentId=objCase.id;
                csFeed.Body = issueDescription;
                insert csFeed;
            }
        }
        system.debug('**** Case outside'+objCase);      
        return objCase;
    }

    //Create "Order Line" for Order.
    public static Order_Line__c getOrderLine(Id orderId, Id PPP_ProductId, Id HW_ProductId, Integer lineNumber, Decimal cyberPrice) {

        /* order line */
        Order__c Order = new Order__c();
        Order=orderId<>null? [Select Id, Order_Type__c from Order__c where Id=:orderId][0] :Order;
        
        Order_Line__c orderLine = new Order_Line__c();
        orderLine.Tax__c = 0;
        
        Product__c PPP_Product;
        Product__c HardWareProduct;
        
        if(!String.isEmpty(PPP_ProductId)){
                
                // Query PPP Product
        try{
        
                PPP_Product = [select Id, Name, SKU__c, List_Price__c from Product__c where Id=:PPP_ProductId and (sub_type__c ='ESP' OR sub_type__c='ESP_CANADA') limit 1];
           }catch (QueryException e){}
        
        }
            if(!String.isEmpty(HW_ProductId)){
                try{
                HardWareProduct = [select Id, Name, SKU__c from Product__c where Id =: HW_ProductId and sub_type__c ='Hardware' limit 1];
                }catch (QueryException e){}
            }

        if(Order.Order_Type__c=='ESP' && PPP_Product!=null ){
        
           orderLine.SKU__c = PPP_Product.SKU__c;
            orderLine.Product_Name__c = PPP_Product.Name;
            orderLine.Product__c = PPP_Product.Id;
            orderLine.List_Price__c = PPP_Product.List_Price__c;
        } 
        
        if(Order.Order_Type__c=='Out of Warranty' && HardWareProduct!=null){
           orderLine.SKU__c = HardWareProduct.SKU__c;
            orderLine.Product_Name__c = HardWareProduct.Name;
            orderLine.Product__c = HardWareProduct.Id;
        } 

        orderLine.Line_Number__c =  String.valueOf(lineNumber);
        orderLine.Ship_Cost__c = 0;
        orderLine.Ship_Priority__c = 'Standard';
        orderLine.Order__c = orderId;
        orderLine.Total__c = cyberPrice;

        insert orderLine;
        return orderLine;
    }   

    public static Payment__c getPaymentFromCybersource_New(String OrderNumber, Date TargetDate, Id OrderId, Id ProductId, String paymentType){
        
        System.debug('>>>>>>PaymentType<<<<<< ' +PaymentType);
        
        Payment__c payment = CyberSourceUtility.getTransaction_New(OrderNumber, ValidationRulesUtility.dateAsString(TargetDate), '', paymentType);
        if(payment!=null && (payment.Cybersource_Response_Code__c == 1)){
            /*if(!String.isEmpty(request.serviceCost)){
                payment.Amount__c = Decimal.valueOf(request.serviceCost);
            }*/
            
            Order__c Order;
            
            if(OrderId<>null){
                
                Order=[Select id, Asset__r.PPP_Product__c, Asset__r.Product__c from Order__c where Id=:OrderId limit 1][0];
            
            }

            payment.Order__c = OrderId;
            payment.Primary_Account_Number__c = payment.Credit_Card_Number__c;
            insert payment;
            getOrderLine(OrderId, Order.Asset__r.PPP_Product__c, Order.Asset__r.Product__c, 1, payment.Amount__c);
        }
        return payment;
    }

    public static Set<String> getPSN_SKUSet(){
       Set<String> PSN_SKUSet= new Set<String>();

         for(PSN_SKU_Mapping__c psm : [select LUCID_SKU__c, PSN_SKU__c, Country_Code__c from PSN_SKU_Mapping__c]) { 
        
              PSN_SKUSet.add(psm.PSN_SKU__c);

            }
    
       return PSN_SKUSet;
    }
    
    public static Staging_PPP_Outbound__c getStaging_PPP_Outbound(CreatePPPOrderService.WebServiceInput input, Order__c order){
        Staging_PPP_Outbound__c Staging_PPP_Outbound = new Staging_PPP_Outbound__c();
        Staging_PPP_Outbound.Order__c = order.id;

        Staging_PPP_Outbound.Consumer_First_Name__c = input.firstName;
        Staging_PPP_Outbound.Consumer_Last_Name__c = input.lastName;
        Staging_PPP_Outbound.Consumer_Email_Address__c = input.email;
        Staging_PPP_Outbound.Consumer_Phone_Number_1__c = input.phone;

        Staging_PPP_Outbound.Proof_of_Purchase_Date__c = ValidationRulesUtility.dateAsString(input.proofOfPurchaseDate);
        Staging_PPP_Outbound.PPP_SKU__c = input.pppSKU;
        Staging_PPP_Outbound.Unit_Serial_Number__c = input.serialNumber;
        Staging_PPP_Outbound.Unit_Model_Number__c = input.modelNumber;
        Staging_PPP_Outbound.Order_Record_Type__c = 'A';

        return Staging_PPP_Outbound;
    }

    public static String getCaseFeed(Id caseId){
        String StrBody = '';
        StrBody=[select  Body from FeedItem where ParentId=:caseId order by LastModifiedDate DESC limit 1][0].body;

        if(!String.isEmpty(StrBody) && StrBody.length()>254){ 

            StrBody=StrBody.substring(0, 255);
        }
        return String.isEmpty(StrBody) ? '' : StrBody.normalizeSpace();
    }

}