public with sharing class StagingAssetManagement {

    
    public static void beforeInsert(List<Staging_Asset__c> assetList){
    } 

    public static void beforeUpdate(List<Staging_Asset__c> assetList, Map<Id, Staging_Asset__c> oldAssetMap){
	}   

    public static void afterInsert(List<Staging_Asset__c> assetList){
    	
		OSB__c osbSettings = OSB__c.getOrgDefaults();
    	
    	if (osbSettings.Update_Asset_Enabled__c){
			callWebService(assetList);
    	}	 
    } 

    public static void afterUpdate(List<Staging_Asset__c> assetList, Map<Id, Staging_Asset__c> oldAssetMap){	
	}   
 
    public static void callWebService(List<Staging_Asset__c> assetList){
    	set<Id> stAssetIdSet = new set<Id>();
    	
    	for(Staging_Asset__c sAsset : assetList){
    		stAssetIdSet.add(sAsset.id);
    	}
    	if(!stAssetIdSet.isEmpty()){
    		updateAssetList(stAssetIdSet);
    	}
    }  
    
    @future(callout=true)
    // Future method to call http callout.
    private static void updateAssetList(set<Id> stAssetIdSet){
    	 
    	 ConsumerSearchResult conResult = new ConsumerSearchResult();
    	 list<Sony_MiddlewareUpdateAccountAsset.Account_mdm> Account_mdmList = new list<Sony_MiddlewareUpdateAccountAsset.Account_mdm>();
    	 map<Id, String> assetIdMdmIdMap = new map<Id, String>();
    	 list<Staging_Asset__c> stagingAstList = new list<Staging_Asset__c>();
    	 
    	 System.debug('==========stAssetIdSet=======' + stAssetIdSet);
    	 
    	 for(Staging_Asset__c sAst :[select Id, PPP_Purchase_Date__c, PPP_End_Date__c, Name, Model_Number__c, Serial_Number__c,  
    	 								MDM_Account_Id__c, PPP_Start_Date__c, Product__c, Product_SKU__c, MDM_Asset_ID__c, 
    	 								PPP_Product__c, PPP_Product_SKU__c, Processed_Date_Time__c,Errors__c, Asset__c, 
    	 								PPP_Product_Description__c, Product_Name__c 
    	 								from Staging_Asset__c
    	 								where Id IN :stAssetIdSet]) {
    	 	stagingAstList.add(sAst);
    	 }
    	 for(Staging_Asset__c sAst :stagingAstList){
    	 	
    	 	
	    		list<Sony_MiddlewareUpdateAccountAsset.Asset_mdm> assetMdmList = new list<Sony_MiddlewareUpdateAccountAsset.Asset_mdm>();
	    		
	    		Sony_MiddlewareUpdateAccountAsset.Asset_mdm assetMdm = new Sony_MiddlewareUpdateAccountAsset.Asset_mdm();
	    		
	    		assetMdm.AssetRowId = sAst.MDM_Asset_ID__c;	    		
	    		assetMdm.PPPPurchaseDate = parseDate(sAst.PPP_Purchase_Date__c);
	    		assetMdm.PPPEndDate = parseDate(sAst.PPP_End_Date__c);
	    		assetMdm.PPPStartDate = parseDate(sAst.PPP_Start_Date__c);
	    		assetMdm.PPPEndDate = parseDate(sAst.PPP_End_Date__c);
	    		assetMdm.ModelNumber = sAst.Model_Number__c;
	    		assetMdm.SerialNumber = sAst.Serial_Number__c;	  
	    		assetMdm.ProductSKU = sAst.Product_SKU__c;
	    		assetMdm.PPPProductDesc = sAst.PPP_Product_Description__c; 
	    		assetMdm.ProductName = sAst.Product_Name__c; 
	   	
	    			    
	    		assetMdmList.add(assetMdm);
	    		
	    		Sony_MiddlewareUpdateAccountAsset.ListOfAsset_mdm  ListOfAsset_mdm = new Sony_MiddlewareUpdateAccountAsset.ListOfAsset_mdm ();
	    		ListOfAsset_mdm.Asset = assetMdmList;
	    		
	    		Sony_MiddlewareUpdateAccountAsset.Account_mdm Account_mdm = new Sony_MiddlewareUpdateAccountAsset.Account_mdm();
	    		Account_mdm.MDMAccountRowId = sAst.MDM_Account_Id__c;
	    		Account_mdm.ListOfAsset  = ListOfAsset_mdm;
	    		Account_mdmList.add(Account_mdm);
    	 }
    	   if(!Account_mdmList.isEmpty()){
		    		Sony_MiddlewareUpdateAccountAsset.ListOfAccount_mdm  ListOfAccount_mdm = new Sony_MiddlewareUpdateAccountAsset.ListOfAccount_mdm();
		    		ListOfAccount_mdm.Account = Account_mdmList;
		    		 System.debug('====ListOfAccount_mdm_____============' + ListOfAccount_mdm);
		    		conResult = ConsumerServiceUtility.updateAsset(ListOfAccount_mdm);
		    }
		    System.debug('====conResult===' + conResult);
		    for(Staging_Asset__c sAsset : stagingAstList){
    			if(conResult.ErrorMessage != null){
    				sAsset.Errors__c = conResult.ErrorMessage;
    			}
    			else{
 					sAsset.Processed_Date_Time__c = Datetime.now();
 					sAsset.MDM_Asset_ID__c = getMdmAssetId(conResult);
 					System.debug('====sAsset.MDM_Asset_ID__c===' + sAsset.MDM_Asset_ID__c);
 					assetIdMdmIdMap.put(sAsset.Asset__c, sAsset.MDM_Asset_ID__c);   				
    			}
    		}
	    	if(!stagingAstList.isEmpty()){
	    		update stagingAstList;
	    	}
	    	list<Asset__c> udpatedAssets = getUpdatedAssets(assetIdMdmIdMap);
	    	if(!udpatedAssets.isEmpty()) {
	    		update udpatedAssets;
	    	}
    }
    
    public static list<Asset__c> getUpdatedAssets(map<Id, String> assetIdMdmIdMap) {
    	list<Asset__c> udpatedAssets = new list<Asset__c>();
    	for(Asset__c ast : [select MDM_ID__c from Asset__c where Id IN :assetIdMdmIdMap.keySet()]) {
    		ast.MDM_ID__c = assetIdMdmIdMap.get(ast.id);
    		udpatedAssets.add(ast);
    	}
    	return udpatedAssets;
    }
    public static String getMdmAssetId(ConsumerSearchResult conResult) {
		if(conResult.listOfAccountForUpdateAccountAsset <> null) {
		  for(Sony_MiddlewareUpdateAccountAsset.Account_mdm acc : conResult.listOfAccountForUpdateAccountAsset) {
		    if(acc.ListOfAsset <> null && acc.ListOfAsset.Asset <> null) {
		      for(Sony_MiddlewareUpdateAccountAsset.Asset_mdm ast : acc.ListOfAsset.Asset) {
		  	    return ast.AssetRowId;
		      }
		    }
		  }
		}
    	return null;
    }
    public static String parseDate(Date dt) {
    	if(dt == null) return '';
    	return String.valueOf(dt.format());
    }
}