@isTest
global class StagingSurveyManagement_Test {
   
   static testmethod void testStagingSurvey() {
   	 Test.startTest();
   	 Test.setMock(HttpCalloutMock.class, new HTTPMockCyberSource());
   	 Integration__c integration = TestClassUtility.createIntegrationRec(false);
   	 integration.Survey_End_Point__c = 'https://playstation--pro.custhelp.com/cc/siebel/survey_trigger/';
   	 integration.Survey_Security_String__c = 'S0nyC0mput3rEnt3rt41nm3ntOfAm3r1c4h34vyR41n';
   	 insert integration;
   	 
   	 list<Contact> cntList = TestClassUtility.createContact(1, false);
   	 Contact cnt = cntList[0];
   	 cnt.FirstName = 'fName';
   	 cnt.Email = 'test@email.com';
   	 cnt.Phone_Unformatted__c = '123123123';
   	 cnt.Preferred_Language__c = 'English';
   	 cnt.LATAM_Country__c = 'US';
   	 insert cnt;
   	 
   	 list<Address__c> addList = TestClassUtility.createAddress(1, true);
   	 list<Product__c> prodList = TestClassUtility.createProduct(1, true);
   	 
   	 Case testcase = new Case();
   	 testCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Voucher').getRecordTypeId();
     testcase.Status = 'Open';
     testcase.received_date__c = date.today();
     testcase.shipped_date__c = date.today().addDays(1);
     testcase.outbound_tracking_number__c = '101';
     testcase.outbound_carrier__c = 'TYest1';
     testCase.Origin = 'Social';
     testCase.Offender__c = 'Test';
     testCase.ContactId = cnt.Id;
     testCase.Bill_To__c = addList[0].Id;
     testCase.Product__c =prodList[0].Id;
     testCase.Product__c = 'PS4';
     testCase.Sub_Area__c = 'Account Help';
     insert testCase;
     
   	  
   	  Staging_Survey__c survey = new Staging_Survey__c();
   	  survey.Case__c = testCase.Id;
   	  insert Survey;
   	  
   	  StagingSurveyManagement.sendSurvey(new list<Staging_Survey__c>{survey});
   	  Test.stopTest(); 
   	  
   	   
   	  Staging_Survey__c updatedSurvey = [select Processed_Date_Time__c, Errors__c 
  										from Staging_Survey__c 
  										where Id = : survey.Id];
   	  System.assertNotEquals(null, updatedSurvey.Processed_Date_Time__c, 'Survey should be sent successfully');
   }
   static testmethod void testStagingSurvey2() {
   	 Test.startTest();
   	 Test.setMock(HttpCalloutMock.class, new HTTPMockCyberSource());
   	 Integration__c integration = TestClassUtility.createIntegrationRec(false);
   	 integration.Survey_End_Point__c = 'https://playstation--pro.custhelp.com/cc/siebel/survey_trigger/';
   	 integration.Survey_Security_String__c = 'S0nyC0mput3rEnt3rt41nm3ntOfAm3r1c4h34vyR41n';
   	 insert integration;
   	 
   	 list<Contact> cntList = TestClassUtility.createContact(1, false);
   	 Contact cnt = cntList[0];
   	 cnt.FirstName = 'fName';
   	 cnt.Email = 'test@email.com';
   	 cnt.Phone_Unformatted__c = '123123123';
   	 cnt.Preferred_Language__c = 'English';
   	 cnt.LATAM_Country__c = 'US';
   	 insert cnt;
   	 
   	 
   	 
   	 Case testcase = new Case();
   	 testCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
     testcase.Status = 'Open';
     testcase.received_date__c = date.today();
     testcase.shipped_date__c = date.today().addDays(1);
     testcase.outbound_tracking_number__c = '101';
     testcase.outbound_carrier__c = 'TYest1';
     testCase.Origin = 'Social';
     testCase.Offender__c = 'Test';
     testCase.ContactId = cnt.Id;
     testCase.Product__c = 'PS4';
     testCase.Sub_Area__c = 'Account Help';
     insert testCase;
     
      Address__c address = new Address__c(); 
        address.Consumer__c = cnt.Id;
        address.Address_Line_1__c = 'Test Line1';
	    address.Address_Line_2__c = 'Test Line2';
		address.Address_Line_3__c = 'Test Line3';
		address.City__c			  = 'ca';
		address.Country__c		  = 'US';
		address.State__c		  = 'TS';
		address.Postal_Code__c    = '123456';
		address.Status__c 		  = 'Active';
		insert address;
		
	 cnt.Bill_To__c = address.Id;
     update cnt;
     
   	  Staging_Survey__c survey = new Staging_Survey__c();
   	  survey.Case__c = testCase.Id;
   	  insert Survey;
   	  
   	  StagingSurveyManagement.sendSurvey(new list<Staging_Survey__c>{survey});
   	  Test.stopTest(); 
   	  
   	   
   	  Staging_Survey__c updatedSurvey = [select Processed_Date_Time__c, Errors__c 
  										from Staging_Survey__c 
  										where Id = : survey.Id];
   	  System.assertNotEquals(null, updatedSurvey.Processed_Date_Time__c, 'Survey should be sent successfully');
   }
   
   static testmethod void testStagingSurvey3() {
   	 Test.startTest();
   	 Test.setMock(HttpCalloutMock.class, new HTTPMockCyberSource());
   	 Integration__c integration = TestClassUtility.createIntegrationRec(false);
   	 integration.Survey_End_Point__c = 'https://playstation--pro.custhelp.com/cc/siebel/survey_trigger/';
   	 integration.Survey_Security_String__c = 'S0nyC0mput3rEnt3rt41nm3ntOfAm3r1c4h34vyR41n';
   	 insert integration;
   	 
   	 list<Contact> cntList = TestClassUtility.createContact(1, false);
   	 Contact cnt = cntList[0];
   	 cnt.FirstName = 'fName';
   	 cnt.Email = 'test@email.com';
   	 cnt.Phone_Unformatted__c = '123123123';
   	 cnt.Preferred_Language__c = 'English';
   	 cnt.LATAM_Country__c = 'US';
   	 insert cnt;
   	 
   	 
   	 
   	 Case testcase = new Case();
   	 testCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
     testcase.Status = 'Open';
     testcase.received_date__c = date.today();
     testcase.shipped_date__c = date.today().addDays(1);
     testcase.outbound_tracking_number__c = '101';
     testcase.outbound_carrier__c = 'TYest1';
     testCase.Origin = 'Social';
     testCase.Offender__c = 'Test';
     testCase.ContactId = cnt.Id;
     testCase.Product__c = 'PS4';
     testCase.Sub_Area__c = 'Account Help';
     insert testCase;
     
     Country__c cntry = new Country__c();
     cntry.X2_Letter_Code__c = 'US';
     cntry.Name = 'US';
     cntry.X3_Letter_Code__c = 'USA';
     insert cntry;
     
     Staging_Survey__c survey = new Staging_Survey__c();
   	 survey.Case__c = testCase.Id;
   	 insert Survey;
   	  
   	  StagingSurveyManagement.sendSurvey(new list<Staging_Survey__c>{survey});
   	  Test.stopTest(); 
   	  
   	   
   	  Staging_Survey__c updatedSurvey = [select Processed_Date_Time__c, Errors__c 
  										from Staging_Survey__c 
  										where Id = : survey.Id];
   	  System.assertNotEquals(null, updatedSurvey.Processed_Date_Time__c, 'Survey should be sent successfully');
   }
   
   global class HTTPMockCyberSource implements HttpCalloutMock  {
	   global HTTPResponse respond(HTTPRequest req) {
      		System.assert(req.getEndpoint().contains('https://playstation--pro.custhelp.com/cc/siebel/survey_trigger/'));
	        System.assertEquals('POST', req.getMethod());
	        HttpResponse res = new HttpResponse();
	        //res.setHeader('Content-Type', 'application/json');
	        String body = getBody();
	        res.setBody(body);
	        res.setStatusCode(200);
	        return res;
	   }
	   String getBody() {
	   	String body = 'Success' ;
		return body; 
	   }
    }
}