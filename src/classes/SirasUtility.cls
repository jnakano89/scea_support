//**************************************************************************/
// (c) 2013 Appirio, Inc.
//
// Description : This class maintains all function related to Siras webservice.
//                  
// Original August 13, 2013   : KapiL Choudhary(JDC) Created for the Task T-171820
// Updated :
//			 August 15, 2013  : KapiL Choudhary(JDC) for the Task T-171820.
//
// ***************************************************************************/

public class SirasUtility {
	public static final String SIRAS_PARTNER_ID;
	public static final String SIRAS_END_POINT;
	public static final String SIRAS_PARTNER_PASSKEY;
	
	static{
		Integration__c settings = Integration__c.getOrgDefaults();
		SIRAS_PARTNER_ID = settings <> null ? settings.Siras_Partner_ID__c : '';
		SIRAS_END_POINT = settings <> null ? settings.Siras_Endpoint__c : '';
		SIRAS_PARTNER_PASSKEY = settings <> null ? settings.Siras_Partner_Passkey__c : ''; 
	}
	
	// This Method calls web service and convert returned xml to SirasResult class object
	// Realted Task:  T-170654
	// Author: Dipika Gupta (Appirio Offshore)
	// Date: 12 Aug,2013
	// Method Moved From Class WebServiceUtility To Here. 
	public SirasResult getPOP(String serialNumber){
		
		SirasResult sr;
		Http h;
		HttpRequest req;
		HttpResponse res;
		String response;
		String request;
 
		try{
		h = new Http();
		// Instantiate a new HTTP request, specify the method (GET) as well as the endpoint 
        req = new HttpRequest();
	    req.setEndpoint(SIRAS_END_POINT);
	    req.setMethod('POST');
	    req.setTimeout(120000); // timeout in milliseconds
	    req.setHeader('Content-Type', 'application/xml');
	    request = requestBody(serialNumber);
	    req.setBody(request);
	    res = h.send(req);
	    response = res.getBody();
	    system.debug('response>>>> '+response);
	    sr = convertResponseToSirasResult(response);
	    
		}catch(Exception e){
			System.debug('>>>>>>>> Siras Exception <<<<<<<<<>');
			IntegrationAlert.addException('Call Siras: Serial Number='+serialNumber, e, request, response);
		}
		
	    return sr;
	}
	
	// This Method convert returned xml to SirasResult class object
	private SirasResult convertResponseToSirasResult(String response){
		DOM.Document doc = new DOM.Document();     
        try {
            doc.load(response);   
            SirasResult SR = new SirasResult(); 
            for(dom.XmlNode node : doc.getRootElement().getChildElements() ){
               if(node.getName()=='Product-Information'){
        			 for(dom.XmlNode childNode : node.getChildElements()) {
        			 	if(childNode.getName()=='Reason-Code') {
                            SR.reasonCode = childNode.getText();
                        } else if (childNode.getName()=='Reason-Description') {
                            SR.reasonDescription = childNode.getText();
                        } else if (childNode.getName()=='SiRAS-Inquiry-ID') {
                            SR.sirasInquiryID = childNode.getText();
                        } else if (childNode.getName()=='UPC') {
                            SR.UPC = childNode.getText();
                        } else if (childNode.getName()=='Serial-Number') {
                            SR.serialNumber = childNode.getText();
                        } else if (childNode.getName()=='Item-Description') {
                              SR.itemDescription = childNode.getText(); 
                        } else if (childNode.getName()=='Item-Number') {
                              SR.itemNumber = childNode.getText();                                   
                        } else if (childNode.getName()=='Brand-Name') {
                              SR.brandName = childNode.getText();                                   
                        } else if (childNode.getName()=='Brand-ID') {
                               SR.brandID = childNode.getText();                                  
                        } else if (childNode.getName()=='Sold-By-Retailer') {
                             SR.soldByRetailer = childNode.getText();                                    
                        } else if (childNode.getName()=='Sold-By-Retailer-ID') {
                              SR.soldByRetailerID = childNode.getText();                                   
                        }  else if (childNode.getName()=='Sold-Date') {
                               if(childNode.getText()!=null && childNode.getText()!=''){
                               	  system.debug('nodiiii >>>>> '+childNode.getText());
                               	  SR.soldDate = validateDate(childNode.getText());
                               }                                 
                        }  else if (childNode.getName()=='Manufacturer-Warranty-Determination') {
                        	    if(SR.soldDate != null){//Sold
                        	    	if(childNode.getText() == '13'){//Sale date found and code is 13-> Valid Warranty
                        	    		SR.manufacturerWarrantyDetermination = 'Valid Warranty';
                        	    	}
                        	    	else if(childNode.getText() == '14'){//Sale date found and code is 14 -> Invalid Warranty 
                        	    		SR.manufacturerWarrantyDetermination = 'Invalid Warranty';
                        	    	}
                        	    }
                        	    else{//Not Sold
                        	    	if(childNode.getText() == '15'){//Sale date not found and code is 15-> Product is already returned
                        	    		SR.manufacturerWarrantyDetermination = 'Product is already returned';
                        	    	}
                        	    	else if (childNode.getText() == '16'){//ale date not found and code is 16->Reported Stolen
                        	    		SR.manufacturerWarrantyDetermination = 'Reported Stolen';
                        	    	}
                        	    	else if (childNode.getText() == '17'){//ale date not found and code is 17->Never sold
                        	    		SR.manufacturerWarrantyDetermination = 'Never sold';
                        	    	}
                        	    	else if (childNode.getText() == '12'){//ale date not found and code is 12->Never sold
                        	    		SR.manufacturerWarrantyDetermination = 'Invalid Serial Number';
                        	    	}
                        	    }
                        }  else if (childNode.getName()=='Manufacturer-Warranty-Parts-Determination') {
                               SR.manufacturerWarrantyPartsDetermination = childNode.getText();                                  
                        }  else if (childNode.getName()=='Manufacturer-Warranty-Labor-Determination') {
                               SR.manufacturerWarrantyLaborDetermination = childNode.getText();                                  
                        }   
                         
        			 }
                }
            }
            return SR;
        } catch (System.XMLException e) {  // invalid XML
            system.debug(e.getMessage());
            return null;
        }
	}
	
	// This Method returns body for webservice request.
	public String requestBody(String serialNumber){
		String body ='<?xml version="1.0" encoding="UTF-8"?>';
		body += '<Serial-Number-Inquiry-Request version="3.0">';
		body += '<Request-Header>';
		body += '<Partner-ID>' + SIRAS_PARTNER_ID + '</Partner-ID>';
		body += '<Passkey>' + SIRAS_PARTNER_PASSKEY + '</Passkey>';
		body += '<Confirmation-Type>2</Confirmation-Type>';
		body += '<Transmission-ID></Transmission-ID>';
		body += '<Location-ID></Location-ID>';
		body += '<Creation-Date>'+Datetime.now().format('yyyyMMdd')+'</Creation-Date>';
		body += '<Creation-Time>'+Datetime.now().format('HHmmss')+'</Creation-Time>';
		body += '<Creation-Time-Zone>GMT-08:00</Creation-Time-Zone>';
		body += '</Request-Header>';
		body += '<Product-Information>';
		body += '<Store-ID></Store-ID>';
		body += '<Operator-ID></Operator-ID>';
		body += '<Terminal-ID></Terminal-ID>';
		body += '<Transaction-ID></Transaction-ID>';
		body += '<Reason-Code></Reason-Code>';
		body += '<Reason-Description></Reason-Description>';
		body += '<UPC></UPC>';
		body += '<Brand-ID>9</Brand-ID>';
		body += '<Serial-Number>'+serialNumber+'</Serial-Number>';
		body += '<Zip-Province-Code></Zip-Province-Code>';
		body += '</Product-Information>';
		body += '<Request-Trailer>';
		body += '<Record-Count>1</Record-Count>';
		body += '</Request-Trailer>';
		body += '</Serial-Number-Inquiry-Request>';
		
		System.debug('Request Body='+ body);
		return body;
	} 
	 private Date validateDate(string dateString){
   	 	 if(dateString.length() == 8){ //Checks if date in this format :- YYYYMMDD
       	  	try{
           	  	System.Debug('dateString:>:>:>:> '+dateString);
           	  	Date dT = date.valueOf(dateString.substring(0,4)+'-'+ dateString.substring(4,6)+'-'+ dateString.substring(6));
           	    return Dt;
       	  	}
       	  	catch(Exception ex){
       	  		System.Debug('Date Exception>>>>> '+ex.getMessage());
       	  		return null;
       	  	}
       	 }
	 	return null;
	 }
}