/******************************************************************************
Class         : AssurantInboundToSfdcBatchable
Description   : This class will retrieve a list of  Staging_PPP_Inbound__c (inbound) 6:00PM on the fifth day of every month to pickup unprocessed records from 
    Staging_PPP_Inbound__c object and update the relevant SFDC base objects based on the mapping as mentioned in the folowing worksheet and the 
    corresponding logic  [T-166695]
Developed by  : Harish Khatri (JDC)
Date          : Jan 07, 2014

Updated By : Ranjeet Singh
Date       : Jan 23, 2014
Reason		: Fixed code to update the Claim_Reason field on asset.

 ******************************************************************************/
global class AssurantInboundToSfdcBatchable implements Database.Batchable<Sobject>, AssurantInboundClaimScheduler.ISchedule{
	public static String claimQueryTest = '';
	public static String claimQuery = 'Select Id,Order_Number__c, Unit_Model_Number__c,Unit_Serial_Number__c,PPP_Service_Level__c,'+
			'PPP_Contract_Number__c,PPP_Claim_Reason__c,Order_Record_Type__c,Order_Dealer__c,'+
			'Loss_Date__c,Invoice_Number__c,Invoice_Date__c,Invoice_Amount__c,Shipping_Cost__c,'+
			'PPP_Currency_Code__c,PPP_Tax__c'+
			' from Staging_PPP_Inbound__c ' + 
			' where Processed_Date_Time__c = null and Order_Record_Type__c = \'L\'  ';
	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = claimQuery;
		if(Test.isRunningTest()){
			query = claimQuery + claimQueryTest;
		}
		system.debug('*************Database Query : '+query+' >> '+Database.Query(query));
		return Database.getQueryLocator(query);
	}
	global void execute(SchedulableContext SC){
		DataBase.executeBatch(this, 5);
	}
	global void execute(Database.BatchableContext BC, List<sObject> batchList) {
		List<Staging_PPP_Inbound__c> inboundList = (List<Staging_PPP_Inbound__c>)batchList;
		map<String, Staging_PPP_Inbound__c> inboundMap = new map<String, Staging_PPP_Inbound__c>();
		map<String, Order__c> orders = new map<String, Order__c>();
		map<Id, Order__c> ordersIdMap = new map<Id, Order__c>();
		map<Id, Set<String>> assetExternalNos = new map<Id, Set<String>>();

		try{
			Set<String>invalidOrders = new Set<String>();
			Set<Asset__c> updateOrders = new Set<Asset__c>();
			//Validate the input Stagining objects. 
			for(Staging_PPP_Inbound__c inbound : inboundList){
				inbound.Processed_Date_Time__c = system.now();
				inbound.Invalid_Record__c = false;
				inbound.Error_Message__c = '';
				inboundMap.put(inbound.Order_Number__c, inbound);
				if(!fieldValidation(inbound) ){
					invalidOrders.add(inbound.Order_Number__c);
				}else{
					orders.put(inbound.Order_Number__c, null);
				}
			}
			system.debug('********>>'+invalidOrders+'----Valid :' +orders.keySet());
			//Update existing SFDC order by Order_Number__c.
			ordersIdMap = New Map<Id, Order__c>([select id, External_Order_Number__c, Asset__c, Asset__r.PPP_Claim_Reason__c  from Order__c where External_Order_Number__c in:orders.keySet() AND External_Order_Number__c NOT IN:invalidOrders]);
			system.debug('******** ordersIdMap >>'+ordersIdMap);
			for(Order__c order : ordersIdMap.values()){
				orders.put( order.External_Order_Number__c, order);
				Staging_PPP_Inbound__c inbound = inboundMap.get(order.External_Order_Number__c);
				if(order.Asset__c!=null){
					//assetExternalNos
					Set<String> stags = assetExternalNos.get(order.Asset__c);
					if(stags==null){
						stags = new Set<String>();
						assetExternalNos.put(order.Asset__c, stags);
					}
					stags.add(order.External_Order_Number__c);
					//updateOrders
					Asset__c asset = new Asset__c(id = order.Asset__c);
					asset.PPP_Claim_Reason__c = inbound.PPP_Claim_Reason__c;
					updateOrders.add(asset);
				}else{
					order.Error_Message__c = order.Error_Message__c +'\nFailed to updated Claim_Reason as related Asset(SFDC) not found, for Assurance processing.';
					inbound.Invalid_Record__c = true;
					inbound.Error_Message__c = inbound.Error_Message__c +'\nFailed to updated Claim_Reason as related Asset(SFDC) not found, for Assurance processing.';
					invalidOrders.add(order.External_Order_Number__c);
				}
			}

			//Update the Staging-PPP object for which Order(SFDC) not yet created.
			for(String orderNumber : orders.keySet()){
				if(orders.get(orderNumber)==null){
					Staging_PPP_Inbound__c inbound = inboundMap.get(orderNumber);
					inbound.Invalid_Record__c = true;
					inbound.Error_Message__c = inbound.Error_Message__c +'\nFailed to updated Claim_Reason as related Order(SFDC) not found, for Assurance processing.';
				}
			}
			//Update the Order with Latest PPP_Claim_Reason__c.
			if(updateOrders.size()>0){
				Database.UpsertResult[] srList = Database.upsert(new List<Asset__c>(updateOrders), false);
				// Iterate through each returned result
				for (Database.UpsertResult sr : srList) {
					if (!sr.isSuccess()) {
						Id AssetId = sr.getId();
						String erDetails = '';
						for(Database.Error err : sr.getErrors()) {
							erDetails = erDetails + '\n\n' + err.getMessage();
						}   
						Set<String> extIds = assetExternalNos.get(AssetId);
						if(extIds!=null){
							for(String extId : extIds){
								Staging_PPP_Inbound__c inbound = inboundMap.get(extId);
								inbound.Invalid_Record__c = true;
								inbound.Error_Message__c = inbound.Error_Message__c + '\nFailed to updated Claim_Reason as related Order(SFDC) update Failed, for Assurance processing. Detail :'+erDetails;
								invalidOrders.add(extId);
							}
						}
					}                 
				}
			}
			//Updated status for Staging objects.
			update inboundList;

		}catch(Exception ex) {
			ExceptionHandler.logException(ex);
			Apex_Log__c al = new Apex_Log__c();
			al.Message__c = ex.getMessage();
			al.Exception_Cause__c = BC.getJobId();
			al.Class_Name__c = 'Assurance In Bound SFDC-Update';
			insert al;

			System.debug('**********Error Occured while running batch*********' + ex.getMessage());           
		}
	}
	global void finish(Database.BatchableContext BC){
	}
	/*
	 *   check Field Validation in Staging_PPP_Inbound__c.
	 */  

	static Boolean fieldValidation(Staging_PPP_Inbound__c inbound){
		String errorMessage = '';
		/*
        if(String.IsEmpty(inbound.Unit_Model_Number__c) || !inbound.Unit_Model_Number__c.equalsIgnoreCase('SONY')){
            errorMessage = 'Invalid Unit Model Number (Must be \'SONY\').';

        } 
        if(String.IsEmpty(inbound.Unit_Serial_Number__c) || inbound.Unit_Serial_Number__c.length() > 8){
            errorMessage += 'Invalid Unit Serial Number (Length >8).';

        }
         if(String.IsEmpty(inbound.PPP_Service_Level__c) || inbound.PPP_Service_Level__c.length() > 2){
            errorMessage += 'Invalid Service Level (Length >2).';

        }
         if(String.IsEmpty(inbound.PPP_Contract_Number__c) || inbound.PPP_Contract_Number__c.length() > 12){
            errorMessage += 'Invalid Contract Number (Length >12).';

        }*/
		if(String.IsEmpty(inbound.PPP_Claim_Reason__c) || inbound.PPP_Claim_Reason__c.length() > 30){
			errorMessage += 'Invalid Claim Reason (Length >30).';

		}
		if(String.IsEmpty(inbound.Order_Record_Type__c) || inbound.Order_Record_Type__c.length() != 1){
			errorMessage += 'Invalid Record Type (Length >1).';

		}
		if(String.IsEmpty(inbound.Order_Number__c)){
			errorMessage += 'Invalid Order Number.';

		}   
		/*if(String.IsEmpty(inbound.Order_Dealer__c) || inbound.Order_Dealer__c.length() != 4){
            errorMessage += 'Invalid Dealer.';

        }*/

		if(!String.IsEmpty(errorMessage)) { 
			inbound.Error_Message__c =  errorMessage;
			inbound.Invalid_Record__c = true;
		}else{
			return true;
		}   
		return false;   
	}

}