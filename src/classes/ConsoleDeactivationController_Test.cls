//******************************************************************************************************************************************************
// CREATED BY : Leena Mandadapu 03/25/2015 : Jira# SMS-1080 - Web console Deactivation Requests
//********************************************************************************************************************************************************/
@isTest
global class ConsoleDeactivationController_Test {

    static User guestUser = [select ID from User where communityNickName =: 'ConsoleDeactivations' limit 1];
    public static PageReference webPageRef = Page.ConsoleDeactivationForm;
    public static ConsoleDeactivationController cntrlclass = new ConsoleDeactivationController();
    public static Date_Filter_Web_Console_Deactivations__c timefilterforDupeChk;
    public static ConsumerSearchResult searchResult = new ConsumerSearchResult();
    public static Account conAccnt;
    public static PSN_Account__c psnAccnt;
    public static Contact contactRec;
    public static case dupeCase;
    public static Blocklist__c blockUserIP;
    public static Group caseGroup;
    public static QueueSObject caseQueue;
    
    static void createGlobalTestData() {
    	
    	caseGroup = new Group(Name='ConsoleDeactivations', type='Queue');
        insert caseGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId())) {
        caseQueue = new QueueSObject(QueueID = caseGroup.id, SobjectType = 'Case');
        insert caseQueue;
        }
        
    	timefilterforDupeChk = new Date_Filter_Web_Console_Deactivations__c();
		timefilterforDupeChk.Date_Filter_Value__c = 1.00;
		insert timefilterforDupeChk;
		system.debug('<<<<<<<<<<<<timefilterforDupeChk>>>>>>>>>>>>>>'+timefilterforDupeChk);
		
		blockUserIP = new Blocklist__c();
		blockUserIP.Start_IP_Address__c = '1.2.3.4';
		blockUserIP.End_IP_Address__c = '1.2.3.4';
		blockUserIP.Status__c = 'Blocked';
		insert blockUserIP;

    }
    
     static void createObjectsTestData() {
    	  	
    	String accRecTypeId= [select Id from RecordType where SobjectType='Account' and IsPersonType = true limit 1].Id;
    	conAccnt = new Account();
    	conAccnt.LastName = 'ApexTestLast';
        conAccnt.FirstName = 'ApexTestFirst';
        conAccnt.Phone = '12345';
        conAccnt.RecordTypeID = accRecTypeId;
        conAccnt.MDM_ID__pc = '123321';
        insert conAccnt;
        
        contactRec = [select id,FirstName, LastName from Contact where isPersonAccount = true and AccountId = :conAccnt.id limit 1];
    	
    	psnAccnt = new PSN_Account__c();
    	psnAccnt.Name = 'ApexTest';
    	psnAccnt.Consumer__c = contactRec.id;
    	psnAccnt.MDM_Account_ID__c = '99999';
    	insert psnAccnt; 	
    	
    	dupeCase = new case();
    	dupeCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('PS Network').getRecordTypeId();
    	dupeCase.Sub_Area__c = 'Deactivate (Console)';
    	dupeCase.PSN_Account__c = psnAccnt.Id;
    	dupeCase.ContactId = contactRec.Id;
    	insert dupeCase;

    }
    
    static void MDMMockService(){
      searchResult = new ConsumerSearchResult();
      Test.setMock(WebServiceMock.class, new WebServiceMockGetConsumer());
      searchResult = ConsumerServiceUtility.getConsumers(null, null, cntrlclass.PSNOnlineId);
    }
    
    //Case creation with new Consumer    
    static testMethod void myPositiveTestCases_caseCreation() {
        system.debug('<<<<<<<<<<<<<<<<<Guest User Id =>>>>>>>>>>>>>>>'+guestUser);
        Test.setCurrentPage(webPageRef);
        system.debug('<<<<<<<<<<<<<<<<<Current Page =>>>>>>>>>>>>>>>'+webPageRef);
        createDataforPostiveTestCases_ENG();
        MDMMockService();
        createGlobalTestData();
        system.runas(guestUser) {	
        Test.StartTest();
        cntrlclass.validateSession();
        List<SelectOption> countryValuesList = cntrlclass.getCountryValues();
        List<SelectOption> ModelValuesList = cntrlclass.getModelTypeValues();
        cntrlclass.getSerialNumHelpText();
        cntrlclass.switchLanguage();
        cntrlclass.languageSwap();
        cntrlclass.ValidateInputData();
        Test.StopTest();
        }
        
    }
    
    //Case creation with existing Consumer    
    static testMethod void myPositiveTestCases_caseCreationextConsumer() {
        system.debug('<<<<<<<<<<<<<<<<<Guest User Id =>>>>>>>>>>>>>>>'+guestUser);
        Test.setCurrentPage(webPageRef);
        system.debug('<<<<<<<<<<<<<<<<<Current Page =>>>>>>>>>>>>>>>'+webPageRef);
        createDataforPostiveTestCases_ENG();
        MDMMockService();
        createGlobalTestData();
        createObjectsTestData();
        system.runas(guestUser) {	
        Test.StartTest();
        cntrlclass.validateSession();
        List<SelectOption> countryValuesList = cntrlclass.getCountryValues();
        List<SelectOption> ModelValuesList = cntrlclass.getModelTypeValues();
        cntrlclass.getSerialNumHelpText();
        cntrlclass.switchLanguage();
        cntrlclass.languageSwap();
        cntrlclass.ValidateInputData();
        Test.StopTest();
        }
        
    }
    
    static testMethod void myPositiveTestCases_DupeChk() {
        system.debug('<<<<<<<<<<<<<<<<<Guest User Id =>>>>>>>>>>>>>>>'+guestUser);
        Test.setCurrentPage(webPageRef);
        system.debug('<<<<<<<<<<<<<<<<<Current Page =>>>>>>>>>>>>>>>'+webPageRef);
        createDataforPostiveTestCases_DupeChk();
        MDMMockService();
        createGlobalTestData();
        createObjectsTestData();
        system.runas(guestUser) {	
        Test.StartTest();
        cntrlclass.validateSession();
        List<SelectOption> countryValuesList = cntrlclass.getCountryValues();
        List<SelectOption> ModelValuesList = cntrlclass.getModelTypeValues();
        cntrlclass.getSerialNumHelpText();
        cntrlclass.switchLanguage();
        cntrlclass.languageSwap();
        cntrlclass.ValidateInputData();
        Test.StopTest();
        }
        
    }
    
    static testMethod void myPositiveTestCases_ipBlock() {
        system.debug('<<<<<<<<<<<<<<<<<Guest User Id =>>>>>>>>>>>>>>>'+guestUser);
        createGlobalTestData();
        system.runas(guestUser) {	
        Test.setCurrentPage(webPageRef);
        ApexPages.currentPage().getHeaders().put('True-Client-IP','1.2.3.4');	
        Test.StartTest();
        system.debug('<<<<<<<<<<<<Page Header Parameters>>>>>>>>>>>>'+ApexPages.currentPage().getHeaders().get('True-Client-IP'));
        cntrlclass.validateSession();
        List<SelectOption> countryValuesList = cntrlclass.getCountryValues();
        List<SelectOption> ModelValuesList = cntrlclass.getModelTypeValues();
        cntrlclass.getSerialNumHelpText();
        cntrlclass.switchLanguage();
        cntrlclass.languageSwap();
        cntrlclass.ValidateInputData();
        Test.StopTest();
        }
        
    }
    
    static testMethod void myNegativeTestCases_blankformfields() {
        system.debug('<<<<<<<<<<<<<<<<<Guest User Id =>>>>>>>>>>>>>>>'+guestUser);
        Test.setCurrentPage(webPageRef);
        system.debug('<<<<<<<<<<<<<<<<<Current Page =>>>>>>>>>>>>>>>'+webPageRef);
        createDataforTestCase_blankformfields();
        //MDMMockService();
        createGlobalTestData();
        system.runas(guestUser) {	
        Test.StartTest();
        cntrlclass.validateSession();
        List<SelectOption> countryValuesList = cntrlclass.getCountryValues();
        List<SelectOption> ModelValuesList = cntrlclass.getModelTypeValues();
        cntrlclass.getSerialNumHelpText();
        cntrlclass.switchLanguage();
        cntrlclass.languageSwap();
        cntrlclass.ValidateInputData();
        Test.StopTest();
        }
    }
    
    static testMethod void myNegativeTestCases_invalidEmailformat() {
        system.debug('<<<<<<<<<<<<<<<<<Guest User Id =>>>>>>>>>>>>>>>'+guestUser);
        Test.setCurrentPage(webPageRef);
        system.debug('<<<<<<<<<<<<<<<<<Current Page =>>>>>>>>>>>>>>>'+webPageRef);
        createDataforNegativeTestCases_invalidEmail();
        MDMMockService();
        system.runas(guestUser) {	
        Test.StartTest();
        cntrlclass.validateSession();
        List<SelectOption> countryValuesList = cntrlclass.getCountryValues();
        List<SelectOption> ModelValuesList = cntrlclass.getModelTypeValues();
        cntrlclass.getSerialNumHelpText();
        cntrlclass.switchLanguage();
        cntrlclass.ValidateInputData();
        Test.StopTest();
        }
    }
    
    static testMethod void myNegativeTestCases_invalidPSNSignInformat() {
        system.debug('<<<<<<<<<<<<<<<<<Guest User Id =>>>>>>>>>>>>>>>'+guestUser);
        Test.setCurrentPage(webPageRef);
        system.debug('<<<<<<<<<<<<<<<<<Current Page =>>>>>>>>>>>>>>>'+webPageRef);
        createDataforNegativeTestCases_invalidPSNSignInformat();
        MDMMockService();
        system.runas(guestUser) {	
        Test.StartTest();
        cntrlclass.validateSession();
        List<SelectOption> countryValuesList = cntrlclass.getCountryValues();
        List<SelectOption> ModelValuesList = cntrlclass.getModelTypeValues();
        cntrlclass.getSerialNumHelpText();
        cntrlclass.switchLanguage();
        cntrlclass.ValidateInputData();
        Test.StopTest();
        }
    }
    
    static testMethod void myNegativeTestCases_invalidSerialNumformat() {
        system.debug('<<<<<<<<<<<<<<<<<Guest User Id =>>>>>>>>>>>>>>>'+guestUser);
        Test.setCurrentPage(webPageRef);
        system.debug('<<<<<<<<<<<<<<<<<Current Page =>>>>>>>>>>>>>>>'+webPageRef);
        createDataforNegativeTestCases_invalidSerialNumformat();
        MDMMockService();
        system.runas(guestUser) {	
        Test.StartTest();
        cntrlclass.validateSession();
        List<SelectOption> countryValuesList = cntrlclass.getCountryValues();
        List<SelectOption> ModelValuesList = cntrlclass.getModelTypeValues();
        cntrlclass.getSerialNumHelpText();
        cntrlclass.switchLanguage();
        cntrlclass.ValidateInputData();
        Test.StopTest();
        }
    }
    
    static void createDataforPostiveTestCases_ENG() {
    	//set page variables
       cntrlclass.language = 'en';
       cntrlclass.FirstName = 'ApexTestFirst';
       cntrlclass.LastName = 'ApexTestLast';
       cntrlclass.PersonEmail = 'ApexTest@test.com';
       cntrlclass.PSNOnlineId = 'ApexTest123';
       cntrlclass.PSNSigninId = 'ApexTest@test.com';
       cntrlclass.SerialNumber = 'MB123456789';
       cntrlclass.SelectedModelType = 'PS4';
       cntrlclass.SelectedCountry = 'United States';       
    }
    
    static void createDataforPostiveTestCases_DupeChk() {
    	//set page variables
       cntrlclass.language = 'en';
       cntrlclass.FirstName = 'ApexTestFirst';
       cntrlclass.LastName = 'ApexTestLast';
       cntrlclass.PersonEmail = 'ApexTest@test.com';
       cntrlclass.PSNOnlineId = 'ApexTest';
       cntrlclass.PSNSigninId = 'ApexTest@test.com';
       cntrlclass.SerialNumber = 'MB123456789';
       cntrlclass.SelectedModelType = 'PS4';
       cntrlclass.SelectedCountry = 'United States';  
       //createDupeCaseDate();    
        
    }
    
    static void createDataforTestCase_blankformfields() {
       //set page variables to all blank for required fields validation
       cntrlclass.language = '';
       cntrlclass.FirstName = '';
       cntrlclass.LastName = '';
       cntrlclass.PersonEmail = ''; 
       cntrlclass.PSNOnlineId = '';
       cntrlclass.PSNSigninId = '';
       cntrlclass.SerialNumber = '';
       cntrlclass.SelectedModelType = '';
       cntrlclass.SelectedCountry = ''; 	
    }
    
    static void createDataforNegativeTestCases_invalidEmail() {
       //set page variables to all blank for required fields validation
       cntrlclass.language = 'en';
       cntrlclass.FirstName = 'ApexTestFirst';
       cntrlclass.LastName = 'ApexTestLast';
       cntrlclass.PersonEmail = 'ApexTest@Test'; 
       cntrlclass.PSNOnlineId = 'Apextest';
       cntrlclass.PSNSigninId = 'ApexTest@Test';
       cntrlclass.SerialNumber = 'MB123456789';
       cntrlclass.SelectedModelType = 'PS4';
       cntrlclass.SelectedCountry = 'Candada'; 	
    }
    
    static void createDataforNegativeTestCases_invalidPSNSignInformat() {
       //set page variables to all blank for required fields validation
       cntrlclass.language = 'en';
       cntrlclass.FirstName = 'ApexTestFirst';
       cntrlclass.LastName = 'ApexTestLast';
       cntrlclass.PersonEmail = 'ApexTest@Test.com'; 
       cntrlclass.PSNOnlineId = 'Apextest';
       cntrlclass.PSNSigninId = 'ApexTest@Test';
       cntrlclass.SerialNumber = 'MB123456789';
       cntrlclass.SelectedModelType = 'PS3';
       cntrlclass.SelectedCountry = 'Candada'; 	
    }
    
    static void createDataforNegativeTestCases_invalidSerialNumformat() {
       //set page variables to all blank for required fields validation
       cntrlclass.language = 'en';
       cntrlclass.FirstName = 'ApexTestFirst';
       cntrlclass.LastName = 'ApexTestLast';
       cntrlclass.PersonEmail = 'ApexTest@Test.com'; 
       cntrlclass.PSNOnlineId = 'Apextest';
       cntrlclass.PSNSigninId = 'ApexTest@Test.com';
       cntrlclass.SerialNumber = 'MB1234567';
       cntrlclass.SelectedModelType = 'PS3';
       cntrlclass.SelectedCountry = 'Candada'; 	
    }
    

}