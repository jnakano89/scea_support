/*
    Class Name : GenericWSHandler
    Created By : Ranjeet Singh (JDC)
    Created Date : 27 Aug 2013
    Reason  :   T-175519, Utility for plug in XML request building and XML response parsing as generic calls from this class.
 */

public with sharing class GenericWSHandler {
    public IHttpCallout CALLOUT { get; set; } 
    public IOAuth OAUTH { get; set; }
    private String settingName{get;set{settingName = value;}}
    public String sessionToken{get;set;}
    public String urlParams = '';


    public GenericWSHandler(String settingNameP) {
        //try{
        settingName = settingNameP; 
        //Use this class by default. in test classes we will override this one with a mock http callout class
        CALLOUT = new DefHttpCallout();
        //Use Generic IOAUTH.
        //OAUTH = new GenericOAuth();
        //Read the settings from sfObjects for Request.
        map<String, Web_Call_Setting_key__c> Web_Call_SettingsL = Web_Call_Settings;
        //}
        //catch (Exception ex){
        //ExceptionHandler.logException(ex);
        //throw ex;
        //}
    }
    ////////////////////////////////////////////////////////////////
    //
    //property to get settings from sfdc objects.
    //
    ////////////////////////////////////////////////////////////////
    public map<String, Web_Call_Setting_key__c> Web_Call_Settings{
        get{
            //try{
            if(Web_Call_Settings==null){
                map<String, Web_Call_Setting_key__c> settings = new map<String, Web_Call_Setting_key__c>();
                //Query setting container Object.
                List<Web_Call_Setting__c> Web_Call_SettingList = [Select Name, Id From Web_Call_Setting__c where Name=:settingName limit 1];
                Set<id>linkedSetting = new Set<id>();
                if(Web_Call_SettingList.size()>0){
                    linkedSetting.add(Web_Call_SettingList[0].id);
                    buildRecursiveSettingMap(linkedSetting, settings);
                }
                Web_Call_Settings = settings;
            }
            return Web_Call_Settings;
            //}
            //catch (Exception ex){
            //ExceptionHandler.logException(ex);
            //return null;
            //}
        }
        private set;
    }
    ////////////////////////////////////////////////////////////////
    //
    //Build the request setting map recursively.
    //
    ////////////////////////////////////////////////////////////////    
    private void buildRecursiveSettingMap(Set<id>settingIds, map<String, Web_Call_Setting_key__c> settings){
        if(settingIds.size()>0){            
            Set<id>linkedSetting = new Set<id>();
            for(Web_Call_Setting_key__c  setting : [Select Web_Call_Setting__r.Name, Web_Call_Setting__c, Value__c, Setting_Link__c, Name, Key_Location__c, Id From Web_Call_Setting_key__c where Web_Call_Setting__c in:settingIds]){
                if(!settings.containsKey(setting.Name)){
                    ///Preserve the overridden setting key.
                    settings.put(setting.Name, setting);
                }
                if(OAUTH==null && setting.Key_Location__c!=null && setting.Key_Location__c.equalsIgnoreCase('OAUTH')){
                    OAUTH = new GenericOAuth();
                }
                if(setting.Setting_Link__c!=null){
                    linkedSetting.add(setting.Setting_Link__c);
                }
            }
            settingIds.clear();
            if(linkedSetting.size()>0){             
                buildRecursiveSettingMap(linkedSetting, settings);
            }
        }
    }

    ////////////////////////////////////////////////////////////////
    //
    //Build Http object from settings.
    //
    ////////////////////////////////////////////////////////////////    

    public HttpRequest setHttpCall(HttpRequest req){
        // HttpRequest from settings.
        for(Web_Call_Setting_key__c setting :Web_Call_Settings.values()){
            String templateValue = GenericTemplateParser.getGlobalValues(setting.value__c);
            if(!String.IsEmpty(templateValue)){//{!system.guid},{!$User.userName}
                setting.Value__c = templateValue;
                system.debug('***********@@'+setting.Name +' >> '+setting.Value__c);
            }

            //Set HttpRequest Headers.
            if(setting.Key_Location__c.equalsIgnoreCase('header')){
                req.setHeader(setting.Name, setting.value__c);
                system.debug('REQUEST : Header > ' + setting.Name + ' : ' + setting.value__c);
            }
            //Set HttpRequest custom setting.
            if(setting.Key_Location__c.equalsIgnoreCase('url')){
                req.setEndpoint(setting.value__c + urlParams);
                system.debug('REQUEST : setEndpoint > ' + setting.value__c);
            }else
                if(setting.Key_Location__c.equalsIgnoreCase('method')){
                    req.setMethod(setting.value__c);
                    system.debug('REQUEST : method > ' + setting.value__c);
                }else
                    if(setting.Key_Location__c.equalsIgnoreCase('timeout')){
                        req.setTimeout(integer.ValueOf(setting.value__c)); // This is just a temporary solution for the CFP Exception ReadTimedOut Exception.
                        system.debug('REQUEST : timeout > ' + setting.value__c);
                    }
        }
        return req;
    }

    ////////////////////////////////////////////////////////////////
    //
    //Make the Request Call.
    //
    ////////////////////////////////////////////////////////////////
    public String callStartSession(GenericRequestBuilder builder)
    {
        
        return callStartSession('',builder);
    }

    public String callStartSession(String requestBody, GenericRequestBuilder builder) {
        String respBody = '';
        IHttpResponse res=null;
        //try{
        HttpRequest req = new HttpRequest(); 
        system.debug('*********req::'+setHttpCall(req));
        setHttpCall(req);
        if (OAUTH!=null) {              
            OAUTH.sign(req, Web_Call_Settings);
        }       
        DateTime beforeRequest = System.now();
        if (String.isEmpty(requestBody)){
            if(Web_Call_Settings.containsKey('requestbody') && builder != null){
                requestbody = Web_Call_Settings.get('requestbody').value__c;                    
                requestBody = builder.buildRequest(requestBody, getTemplateValues());
            }
        }
        system.debug('*********requestBody::'+requestBody);
        if (requestBody != null && requestBody.length() > 0){            
            req.setBody(requestBody);
        }
        
        System.Debug('>>>>>>Req<<<<<<<< '+req);
        
        res = CALLOUT.send(req);
        
        System.Debug('>>>>>>res<<<<<<<< '+res);
        string[] headerkeys = res.getHeaderKeys();
        Map<string, string> headers = new map<string, string>();
        for(string s : headerkeys){
            if (s != null)
                system.debug('header: ' + s + ' value: ' + res.getHeader(s));
        }

        DateTime afterRequest = System.now();
        respBody = res.getBody();
        system.debug('*********responseBody::'+respBody);
        //}catch(Exception ex){
        //      system.debug('*********Ex2'+ex);
        //    ExceptionHandler.logException(ex);
        //    throw ex;
        //}       
        /*
                Sample for Response Exception
                CALLOUT_RESPONSE|[10]|System.HttpResponse[Status=Code:GatewayTimeout,Message:Socket read timeout api=Order 1.0,Details:Read timed out outbound host: atle2ebossi1.bosptc.intuit.com,Type:SYSTEM, StatusCode=504]
         */            
        if(res!=null && res.getStatusCode()!=200){
            GenericOAuth.GenericException exp = new GenericOAuth.GenericException(String.valueOf( res.getStatusCode()),res.toStrings());
            exp.details.put('Status', String.valueOf(res.getStatus()));
            //exp.details.put('Message', res.resp.Message);
            //exp.details.put('api', res.resp.api);
            exp.details.put('Details', res.toStrings());
            //exp.details.put('host', res.resp.host);
            //exp.details.put('Type', res.resp.Type);
            exp.details.put('StatusCode', String.valueOf( res.getStatusCode()));
            exp.details.put('responseBody', respBody);
            throw exp; 
        }
        return respBody;
    }

    map<string, Object>getTemplateValues(){
        map<string, Object> tmlMaps = new map<string, Object>();
        for(Web_Call_Setting_key__c setting :Web_Call_Settings.values()){
            tmlMaps.put(setting.name, setting.Value__c);
        }
        return tmlMaps;
    }

    public String callStartSession(String requestBody) {
        return callStartSession(requestBody, null);
    }

    public static String getTemplateBySettingName(String name)
    {
        //try
        //{
        return [SELECT value__c FROM Web_Call_Setting_key__c WHERE name = 'requestbody' AND web_call_setting__r.name = :name][0].value__c;
        //}
        // catch (Exception ex)
        // {
        //    ExceptionHandler.logException(ex);
        //    throw ex;
        //    return '';
        //}
    }


    public String callStartSession_1(String requestBody, GenericRequestBuilder builder) {
        return callStartSession_1('', requestBody, builder);
    }
    // testing purpose
    public String callStartSession_1(String respBody, String requestBody, GenericRequestBuilder builder) {        
        try{
            /* HttpRequest req = new HttpRequest();        
            setHttpCall(req);
            if (OAUTH!=null) {              
                OAUTH.sign(req, Web_Call_Settings);
            }*/       
            DateTime beforeRequest = System.now();
            if (String.isEmpty(requestBody)){
                if(Web_Call_Settings.containsKey('requestbody') && builder != null){
                    //use Web_Call_SettingsP

                    //GenericTemplateParser DefGenericBuilder = new GenericTemplateParser(Web_Call_Settings.get('requestbody').value__c);
                    //requestBody = DefGenericBuilder.getParsedContent(getTemplateValues(Web_Call_SettingsP));
                    requestbody = Web_Call_Settings.get('requestbody').value__c;                    
                    //System.debug('@@@@@ Req body 1: '+requestBody);   
                    requestBody = builder.buildRequest(requestBody, getTemplateValues());
                    //System.debug('@@@@@ Req body 2: '+requestBody);   
                }
            }
            /*if (requestBody != null && requestBody.length() > 0){
                            req.setBody(requestBody);
                        }
            System.debug('GenericWSHandler: Req: '+requestBody);
            System.debug('GenericWSHandler: Req: '+req);
            IHttpResponse res = CALLOUT.send(req);
            DateTime afterRequest = System.now();
            respBody = res.getBody();
            System.debug('GenericWSHandler: respBody: '+respBody);*/
        }catch(Exception ex){
            system.debug('*********Ex2'+ex);
            ExceptionHandler.logException(ex);
            throw ex;
        }       
        return respBody;
    }

    public static String genericExceptionSample = '<ConfirmBOD xmlns:Lacerte="http://www.lacertesoftware.com" xmlns:VistaPrint="http://www.vistaprint.com" xmlns:PSB="http://www.intuit.com/PSB" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ETX="http://www.intuit.com/ETX" xmlns:ERP="http://www.intuit.com/ERP" xmlns:CRM="http://www.intuit.com/CRM" xmlns:Harland="http://www.harland.com" xmlns:Payroll="http://www.intuit.com/Payroll" xmlns:Intuit="http://www.intuit.com" revision="8.0" xmlns="http://www.openapplications.org/oagis"><ApplicationArea><CreationDateTime>2013-10-08T04:19:45-07:00</CreationDateTime><UserArea><Intuit:Version>V2-0</Intuit:Version></UserArea></ApplicationArea><DataArea><Confirm /><BOD><Header><OriginalApplicationArea><CreationDateTime>2013-10-08T04:19:45-07:00</CreationDateTime><UserArea><Intuit:Version /></UserArea></OriginalApplicationArea><BODFailure><ErrorMessage><Description>Transaction failed.  At least one data error occurred.</Description><ReasonCode>101</ReasonCode></ErrorMessage></BODFailure></Header><NounOutcome><NounFailure><ErrorMessage><Description>There is an error in XML document (29, 50). Input string was not in a correct format.</Description><ReasonCode>1000</ReasonCode></ErrorMessage></NounFailure></NounOutcome></BOD></DataArea></ConfirmBOD>';


}