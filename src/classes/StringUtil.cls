public class StringUtil {

	/**
	* Check the input str is null or empty string
	* @param str 
	* @return true is str is null or empty string
	*/ 
	public static Boolean isNullOrEmpty(String str) { 
		return str == null || str.length() == 0;
	}
	
	/**
	* Format string using params. For example, if base is 'Hello, {0}. Welcome to {1}' and
	* param is {'Tom', 'Intuit'}, then the output is 'Hello Tom. Welcome to Intuit'.
	* @param base  
	* @para params
	*/ 
	public static String formatMessage(String base, List<String> params) { 
		if(isNullOrEmpty(base)) { 
			return base;
		}
		return String.format(base, params);
	}
	
	/**
	* Format string using params. For example, if base is 'Hello, {0}. Welcome to {1}' and
	* param is {'Tom', 'Intuit'}, then the output is 'Hello Tom. Welcome to Intuit'.
	* @param base  
	* @para params
	*/ 
	public static String formatMessage(String base, String param) { 
		return formatMessage(base, new List<String>{param});
	}
	
	/**
	* Escape single quotes for SOQL 
	* @param str A strin to be escaped. 
	* @return escaped string. If input str is null or empty, it isn't escaped. 
	*/ 
	/*
	public static String escapeSingleQuotes(String str) { 
		return isNullOrEmpty(str) ? str : String.escapeSingleQuotes(str);
	}
	*/	
	
	public static String getUUID() {
		Blob b = Crypto.GenerateAESKey(128);
		String h = EncodingUtil.ConvertTohex(b);
		String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
		return guid;
	}
}