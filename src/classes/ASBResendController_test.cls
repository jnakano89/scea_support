@isTest
private class ASBResendController_test{

    static testMethod void myUnitTest() {
        Case cs = TestClassUtility.createCase('Open', '', true);
        Apexpages.currentPage().getParameters().put('Id', cs.Id);
        ASBResendController ctrl = new ASBResendController();
        ctrl.doLoad();
        
        Staging_Service_Outbound__c ob = [select Batch_type__c from Staging_Service_Outbound__c];
        System.assertNotEquals(null, ob, 'Outbound record should be inserted for this case.');
    }
     static testMethod void myUnitTest2() {
        ASBResendController ctrl = new ASBResendController();
        ctrl.doLoad();
        
        Staging_Service_Outbound__c ob;
        for(Staging_Service_Outbound__c o : [select Batch_type__c from Staging_Service_Outbound__c]) {
        	ob = o;
        }
        System.assertEquals(null, ob, 'No Outbound record should insert');
    }
}