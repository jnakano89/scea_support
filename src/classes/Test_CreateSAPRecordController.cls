@isTest
global class Test_CreateSAPRecordController {
  
  @isTest
	static void myUnitTest() {
		CreateSAPRecordController cont = new CreateSAPRecordController();
		cont.createSAP();
		System.assert(cont.invalidRecord);
		try{
			Apexpages.currentPage().getParameters().put('caseId', '000000000000000');
	    cont = new CreateSAPRecordController();
	    cont.createSAP();
		}catch(exception e){
			System.assert(cont.invalidRecord);
		}
		list<Contact> cntList = TestClassUtility.createContact(1, false);
		Case cs = TestClassUtility.createCase('New', 'Test', false);
	  cs.ContactId = cntList[0].Id;
	  cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
    insert cs;
    Apexpages.currentPage().getParameters().put('caseId', cs.id);
    cont = new CreateSAPRecordController();
    cont.createSAP();
    System.assert(cont.invalidRecord);
    
	}
	
	static testMethod void testSuccess() {
		list<Contact> cntList = TestClassUtility.createContact(1, true);
		list<Asset__c> assetList = TestClassUtility.createAssets(1, true);
    list<Address__c> addList = TestClassUtility.createAddress(1, false);
	  addList.get(0).State__c = 'CA';
	  insert addList;
	  
		Case cs = TestClassUtility.createCase('New', 'Test', false);
	  cs.ContactId = cntList[0].Id;
	  cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service').getRecordTypeId();
	  cs.Asset__c = assetList[0].id;
	  cs.Ship_To__c = addList[0].id;
	  cs.Received_Date__c = Date.today();
    insert cs;
    cs = [Select Asset__c, Ship_To__c, Ship_To__r.Postal_Code__c, Received_Date__c from Case where id = :cs.id];
    Zip_Code__c zipCode = new Zip_Code__c(Name = cs.Ship_To__r.Postal_Code__c);
    insert zipCode;
    FeedItem feedItem = new FeedItem();
    feedItem.Body = 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest';
    feedItem.ParentId = cs.id;
    insert feedItem;
    Apexpages.currentPage().getParameters().put('caseId', cs.id);
    Test.startTest();
    	Test.setMock(WebServiceMock.class, new WebServiceMockCreateSAPRecord());
    CreateSAPRecordController cont = new CreateSAPRecordController();
    cont.createSAP();
    System.assert(cont.caseId != null);
    cont.redirectToCase();
    
    Test.stopTest();
	}
	
	global class WebServiceMockCreateSAPRecord implements WebServiceMock {
	   global void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,	
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	      		SonyComMiddlewareSfdcToSap_New.Output_details_element responseElm = 
	      		  	new SonyComMiddlewareSfdcToSap_New.Output_details_element();
	      		responseElm.Status_msg = 'File created Successfully !';
	       		  response.put('response_x', responseElm); 
	   		   }
    }
}