//8/26/2013 : Urminder(JDC) : created this test class.
@isTest
private class SelectAssetController_Test {
   static case testcase;
   static Asset__c ast;
   static testMethod void myUnitTest() {
       Test.startTest();
       createData();
       Apexpages.currentPage().getParameters().put('caseId', testCase.Id);
       SelectAssetController ctrl = new SelectAssetController();
       
       ApexPages.currentPage().getParameters().put('ctRadio',ast.Id);
       ctrl.selectAsset();
       
       
       Case updatedCase = [select Asset__c from Case where ID = :testCase.Id];
       System.assertEquals(updatedCase.Asset__c, ast.Id,'Case should be udpated with selected asset');
       
       ctrl.clearAsset();
       
       updatedCase = [select Asset__c from Case where ID = :testCase.Id];
       System.assertEquals(updatedCase.Asset__c, null,'Case should be udpated with null');
       
       ctrl.createAsset();
       Test.stopTest();
   }
    
   static void createData() {
	 Contact cnct = new Contact();
     cnct.LastName = 'test';
     cnct.FirstName = 'ftest';
     cnct.Phone = '12345';
  	 insert cnct;
  	 
  	 testcase = new Case();
     testcase.Status = 'Open';
     testcase.received_date__c = date.today();
     testcase.shipped_date__c = date.today().addDays(1);
     testcase.outbound_tracking_number__c = '101';
     testcase.outbound_carrier__c = 'TYest1';
     testCase.Origin = 'Social';
     testCase.ContactId = cnct.Id;
     testCase.Offender__c = 'Test';
     testCase.Product__c = 'PS4';
     testCase.Sub_Area__c = 'Account Help';
     insert testCase;
     
     ast = new Asset__c();
     ast.Serial_Number__c = 'ABCD-1234';
     ast.Purchase_Date__c = date.today();
     ast.Warranty_Details__c = 'Test Details';
     ast.Registration_Date__c = date.today();
     insert ast;
     
     Consumer_Asset__c ca = new Consumer_Asset__c();
     ca.Asset__c = ast.Id;
     ca.Consumer__c = cnct.Id;
     insert ca;
   }
}