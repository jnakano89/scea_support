/*************************************************************************************************************************************************************
//Class         : CyberSourcePaymentController
//Description   : This is controller class for CyberSourcePayment page. Which excecutes from payment button 
//                from purchase record type Case detail page.
//Created Date  : August 7, 2013
//Developed by  : Urminder Vohra (JDC)
//Updates       :
//
//              : 09/06/2013 Urminder(JDC) : remove debug pages insert Order and Payment Records for each transaction.[T-176726]
//              : August 30, 2013  KapiL Choudhary(JDC) : Added X2_Letter_Country_Code__c in query for the Task T-182702.
//              : 12/11/2013 : Urminder : added a new parameter in url to calculate Tax amount. 
//				: 03/31/2014 : Leena Mandadapu : added code to reset the Asset PPP Product when Payment is declined. JIRA# SF-136 
                : 07/02/2014 : Leena Mandadapu : Jira# SF-1108 - Added logic for PPP Tax calculation
//**************************************************************************************************************************************************************/

public class CyberSourcePaymentController {

	public static string PAYMENT_STATUS_CHARGED             = 'Charged';
	public static string PAYMENT_STATUS_AUTHORIZED          = 'Authorized';  
	public static string PAYMENT_STATUS_SETTLED             = 'Settled';          
	public static string PAYMENT_STATUS_DECLINED            = 'Declined'; 
	public static string CASE_STATUS_FAILED                 = 'Payment Fail';
	
	//LM 07/02/2014 : Added
    public static String casePurchaseRecordTypeId = GeneralUtiltyClass.RT_PURCHASE_ID;
    public static String caseServiceRecordTypeId = GeneralUtiltyClass.RT_SERVICE_ID;

	public String caseId{get;set;}
	public String orderType{get;set;}


	public String mac1{get;set;}

	public String response{get;set;}
	//public boolean showDetails{get;set;}

	public String access_key{get;set;}
	public String profile_id{get;set;}
	public String transaction_uuid{get;set;}
	public String signed_field_names{get;set;}
	public String unsigned_field_names{get;set;}
	public String signed_date_time{get;set;}
	public String locale{get;set;}
	public String transaction_type{get;set;}
	public String reference_number{get;set;}
	public string amount{get;set;}
	public String crncy{get;set;}
	public String str{get;set;}
	public boolean paymentPage{get;set;}
	public String tax_amount{get;set;}
	public String PAYMENT_REQUEST_URL{get;set;}
	public String country{get;set;}
	public String req_allow_payment_token_update{get;set;}

	public String nexus{get;set;}

	public Case selectedCase{get;set;}
	public Order__c newOrder{get;set;}

	public boolean isAuthorized{get;set;}
	public boolean isSettlement{get;set;}

	public final Integer MAX_ORDERS = 5;
	public final String PAYMENT_TOKEN_URL = 'https://testsecureacceptance.cybersource.com/token/create';

	final String EXCEEDED_MAX_ORDERS_ERROR_MESSAGE = 'You have exceeded the maximum number of payments.  Please contact your supervisor.'; 

	String ACCESS_KEY_STR; 
	String SECRET_KEY;
	String ALGORITHM_NAME;
	String PROFILE_ID_STR;
	String LOCALE_STR;
	Integer NUMBER_OF_RETRIES;

	public String paymentToken{get;set;}
	public String purchaseType;
	public String cashPayment;

	public CyberSourcePaymentController() {

		if(Apexpages.currentPage() <> null) {
			caseId = Apexpages.currentPage().getParameters().get('caseId');
			orderType = Apexpages.currentPage().getParameters().get('OrderType');
			transaction_type = Apexpages.currentPage().getParameters().get('TransactionType');
			paymentToken = Apexpages.currentPage().getParameters().get('PaymentToken');
			purchaseType = Apexpages.currentPage().getParameters().get('purchaseType');
			cashPayment = Apexpages.currentPage().getParameters().get('cashPayment');
			isSettlement = Apexpages.currentPage().getParameters().get('Settle') == '1';
		}
		// get static values from custom setting
		populateValuesFromCustomSetting();

		str ='';
		access_key = ACCESS_KEY_STR;
		profile_id = PROFILE_ID_STR;
		locale = LOCALE_STR;

		paymentPage = false;
		if(transaction_type == 'authorization,create_payment_token') {
			req_allow_payment_token_update = 'true';
		} else {
			req_allow_payment_token_update = 'false';
		}
		// get detail of selected case
		selectedCase = getCaseDetails(caseId);

		if(selectedCase <> null 
				&& (selectedCase.Bill_To__r.X2_Letter_Country_Code__c == 'US' || selectedCase.Bill_To__r.Country__c == 'USA')) { 
			crncy = 'USD';
			country = 'US';
			nexus = CyberSourceUtility.getNexusStatesString();
			//nexus =  'CA,NJ,NY,GA,MA,TX,UT,IL,OH,MN,WA,VA,NE,AR,WI,AL,KS';
		} else if(selectedCase <> null 
				&& (selectedCase.Bill_To__r.X2_Letter_Country_Code__c == 'CA' || selectedCase.Bill_To__r.Country__c == 'Canada')) {
			crncy = 'CAD';
			country = 'CA';
		}

	}

	// get values from custom setting
	private void populateValuesFromCustomSetting() {

		Cybersource__c settings = Cybersource__c.getOrgDefaults();

		if(settings <> null) {
			if(purchaseType == 'PPP') {
				ACCESS_KEY_STR  = settings.Access_Key__c <> null ? settings.Access_Key__c : '';
				PROFILE_ID_STR = settings.Profile_ID__c <> null ? settings.Profile_ID__c : '';
				SECRET_KEY = Label.CyberSource_Secret_Key; 
				system.debug('===========PPP==========');
			} else {
				ACCESS_KEY_STR = settings.RMA_Access_Key__c <> null ? settings.RMA_Access_Key__c: '';
				PROFILE_ID_STR = settings.RMA_Profile_ID__c <> null ? settings.RMA_Profile_ID__c : '';
				SECRET_KEY = Label.RMA_Secret_Key;
				system.debug('===========RMA=========='); 
			}
			LOCALE_STR = settings.Locale__c <> null ? settings.Locale__c : '';
			ALGORITHM_NAME = settings.Algorithm_Name__c <> null ? settings.Algorithm_Name__c : '';
			NUMBER_OF_RETRIES = settings.Number_of_Retries__c.intvalue();
			PAYMENT_REQUEST_URL = settings.Payment_Request_URL__c; 
			//PAYMENT_REQUEST_URL = PAYMENT_TOKEN_URL;   
		}
	}

	private Case getCaseDetails(String cId) {

		Case selCase;

		for(Case c : [select ContactId, Asset__c, Asset__r.PPP_Product__c, Bill_To__c, Ship_To__c, SCEA_Product__r.SKU__c,SIRAS_Date__c,
		              Bill_To__r.City__c, Bill_To__r.State__c, Bill_To__r.X2_Letter_Country_Code__c, Bill_To__r.Address_Line_1__c,
		              Bill_To__r.Address_Line_2__c, Bill_To__r.Address_Line_3__c, Bill_To__r.Postal_Code__c, Bill_To__r.Country__c, 
		              Ship_To__r.Country__c, Total_Tax__c, Total_Amount_To_PAy__c, Fee_Type__c,
		              SCEA_Product__r.Name, SCEA_Product__r.List_Price__c, SCEA_Product__r.Trade_In_Price__c, Order__c, Order__r.Name, Order__r.Order_Date__c, Unit_Price__c,
		              Contact.FirstName, Contact.LastName, Contact.Email ,OwnerId, RecordTypeId
		              from Case
		              Where Id = :cId]) {

			selCase = c;
			reference_number=c.Order__r.Name;

		}

		return selCase;
	}

	public void showPaymentPage() {
		paymentPage = true;
	}

	public Pagereference showDetails() {
		//String pToken = paymentToken;
		//paymentToken = null;
		if(paymentToken == null){

			isAuthorized = updateCaseDetails(selectedCase); 

			if(!isAuthorized) {
				return null;
			} 


			isAuthorized = validateCurrentTransaction(selectedCase);

			if(!isAuthorized) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, EXCEEDED_MAX_ORDERS_ERROR_MESSAGE));
				return null;
			}else{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, LABEL.Note_Cyber_Source));
			}

			createOrder();

			Order__c updatedOrder = [select Name, Order_Total__C, Total_Tax__c from Order__c where Id = :newOrder.Id];
			amount = String.valueOf(updatedOrder.Order_Total__C);

			if(cashPayment <> null) {
				Payment__c payment = new Payment__c();
				payment.Name_on_Card__c = selectedCase.Contact.FirstName + ' ' +  selectedCase.Contact.LastName; 
				payment.Transaction_Date__c = date.today();
				payment.Payment_Status__c = 'Paid';
				payment.Amount__c = decimal.valueOf(amount);
				payment.Order__c = newOrder.Id;
				payment.Payment_Method__c = cashPayment;
				payment.Payment_Type__c = cashPayment;
				payment.Purchase_Type__c = 'RMA';
				insert payment;

				return cancel();
			}

			tax_amount = String.valueOf(updatedOrder.Total_Tax__c == null ? 0  : updatedOrder.Total_Tax__c);
			//amount = String.valueOf(decimal.valueOf(tax_amount) + decimal.valueOf(amount));
			reference_number = updatedOrder.Name;

		}
		else{
			Order__c updatedOrder = [select Name, Order_Total__C, Total_Tax__c from Order__c where Id = :selectedCase.Order__c];
			amount = String.valueOf(updatedOrder.Order_Total__C);
			tax_amount = String.valueOf(updatedOrder.Total_Tax__c == null ? 0  : updatedOrder.Total_Tax__c);

			//amount = String.valueOf(decimal.valueOf(tax_amount) + decimal.valueOf(amount));


			reference_number = updatedOrder.Name; 
		}
		//showDetails = true;
		transaction_uuid = String.valueOf(Math.round((Math.random() * 1000000000 )));

		if(paymentToken == null){
			signed_field_names = 'access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,tax_amount';
			unsigned_field_names = 'req_allow_payment_token_update,taxService_nexus,bill_to_forename,bill_to_surname,bill_to_email,bill_to_address_state,bill_to_address_line1,bill_to_address_line2,bill_to_address_country,bill_to_address_city,bill_to_address_postal_code';
		}
		else{
			signed_field_names = 'access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,payment_token';
			unsigned_field_names = '';
		}

		//formatting date
		signed_date_time = String.valueOf(DateTime.now().timeGmt()).substring(0,8)  + 'Z';
		signed_date_time = String.valueOf(DateTime.now().dateGmt()).substring(0, 10) + 'T' + signed_date_time;
		//System.debug('======Test DateTime======' + signed_date_time);


		//signed_date_time = String.valueOf(DateTime.now().timeGmt()).substring(0,8)  + 'Z';
		//signed_date_time = String.valueOf(System.today()).substring(0, 10) + 'T' + signed_date_time;
		//System.debug('======Test DateTime======' + signed_date_time);

		// formating all input fields to be encode
		str += 'access_key=' + access_key + ',';
		str += 'profile_id=' + profile_id + ',';
		str += 'transaction_uuid=' + transaction_uuid + ',';
		str += 'signed_field_names=' + signed_field_names + ',';
		str += 'unsigned_field_names=' + unsigned_field_names + ','; 
		str += 'signed_date_time=' + signed_date_time + ',';
		str += 'locale=' + locale + ',';
		if(paymentToken == null){
			str += 'transaction_type=' + transaction_type + ',';
			str += 'reference_number=' + reference_number + ',';
			str += 'amount=' + amount  + ',';
			str += 'currency=' + crncy + ',';
			str += 'tax_amount=' + tax_amount;
		}
		else{
			str += 'transaction_type=sale,';
			str += 'reference_number=' + reference_number + ',';
			str += 'amount=' + amount  + ',';
			str += 'currency=' + crncy + ',';
			str += 'payment_token=' + paymentToken + '';
		}



		//str += 'taxService_nexus=' + nexus;
		//str += 'bill_to_forename=' + 'TestName';

		// Encoding inoputs in hMac
		String algorithmName = ALGORITHM_NAME;
		String key = SECRET_KEY;
		Blob privateKey =Blob.valueOf(key);

		Blob mac = Crypto.generateMac(algorithmName,  Blob.valueOf(str), 
				privateKey); 

		mac1 = EncodingUtil.base64Encode(mac);
		//paymentToken = pToken;
		System.debug('======STR======' + str);
		//System.debug(updatedOrder + '======selectedCase======' + selectedCase);
		return null;
	}

	public PageReference createOrder() {

		Integer lineNumber = 1;

		/* new order */
		newOrder = new Order__c();
		newOrder.Case__c = caseId;

		if (selectedCase.Fee_Type__c=='Out of Warranty'){
			newOrder.Order_Type__c = 'Out of Warranty';
		} else newOrder.Order_Type__c = OrderType;


		newOrder.Asset__c = selectedCase.Asset__c;
		newOrder.Order_Status__c = 'Open';
		newOrder.Order_Date__c = Date.today();
		newOrder.Bill_To__c = selectedCase.Bill_To__c;
		newOrder.Consumer__c = selectedCase.ContactId;
		newOrder.Ship_To__c = selectedCase.Ship_To__c;
		newOrder.Order_Origin_Source__c = 'CS';
		insert newOrder;

		insertOrderLine(newOrder, lineNumber++);

		System.debug('======Insert Order Record======' + newOrder);

		if (selectedCase!=null){
			selectedCase.Order__c = newOrder.Id;
			update selectedCase;
		}

		return null;
	}

	public void insertOrderLine(Order__c newOrder, Integer lineNumber) {

		/* order line */
		Order_Line__c orderLine = new Order_Line__c();

		orderLine.SKU__c = selectedCase.SCEA_Product__r.SKU__c;
		orderLine.Product_Name__c = selectedCase.SCEA_Product__r.Name;


		orderLine.List_Price__c = selectedCase.SCEA_Product__c == null ? 0 : selectedCase.SCEA_Product__r.List_Price__c;

		System.Debug('-------------->   orderLine.List_Price__c' +orderLine.List_Price__c);


		orderLine.List_Price__c = orderLine.List_Price__c == null ? 0 : orderLine.List_Price__c;

		System.Debug('-------------->   orderLine.List_Price__c' +orderLine.List_Price__c);
		orderLine.Quantity__c = String.valueOf(1);
		//if(Apexpages.currentPage().getParameters().get('calculateTax') == 'true') {
		orderLine.Tax__c = selectedCase.Total_Tax__c; 
		//} else {
		//  orderLine.Tax__c = 0;
		//}

		if(transaction_type == 'authorization,create_payment_token') {
			//orderLine.List_Price__c = (selectedCase.Total_Amount_To_PAy__c);
			orderLine.List_Price__c = (selectedCase.Unit_Price__c);
		}

		System.debug('======orderLine.Tax__c======' + newOrder);

        //LM 07/02/2014 : Updated below if condition - added Purchase case condition
		//if (selectedCase.Fee_Type__c=='Out of Warranty'){
		if ((selectedCase.RecordTypeId == caseServiceRecordTypeId && selectedCase.Fee_Type__c=='Out of Warranty') || (selectedCase.RecordTypeId == casePurchaseRecordTypeId)){
			orderLine.Total__c=SelectedCase.Total_Amount_to_Pay__c;
		} else orderLine.Total__c = orderLine.List_Price__c * Integer.valueOf(orderLine.Quantity__c);


		orderLine.Line_Number__c =  String.valueOf(lineNumber);
		orderLine.Ship_Cost__c = 0;
		orderLine.Ship_Priority__c = 'Standard';
		orderLine.Order__c = newOrder.Id;

		insert orderLine;

		System.debug('======Insert Order Line Record======' + orderLine);
	}


	public PageReference returnToCase() {

		//Datetime myDatetime = Datetime.now();

		System.Debug('>>>>>>+dateAsString(Datetime.now().Date())<<<<<' +(Datetime.now().Date()));
		String targetDate =   ValidationRulesUtility.dateAsString(Datetime.now().Date());
		System.debug('>>>>>>>>>targetDate<<<<<<<<<' +targetDate);
		/*myDatetime.format('YYYYMM');
        System.debug('=============targetDate YYYYMM=======' + targetDate);
        String day = String.valueOf(myDatetime.day()).length() == 2 ? '' + myDatetime.day() : '0' + myDatetime.day() ;
        targetDate += day;
        System.debug('=============reference_number=======' + reference_number);
        System.debug('=============targetDate=======' + targetDate);
		 */


		if(selectedCase!=null  && (selectedCase.Order__r.Order_Date__c<>null) && !isSettlement){

			targetDate =   ValidationRulesUtility.dateAsString(selectedCase.Order__r.Order_Date__c);  

		}//end-if

		System.debug('>>>>>>>>>targetDate<<<<<<<<<' +targetDate);

		//targetDate ='20140201';
		//Payment__c payment=CyberSourcePaymentController.insertPaymentRecord(reference_number, targetDate, newOrder.Id);
		
			
		if(newOrder == null){
			newOrder = new Order__c(Id = selectedCase.Order__c);
		}
		
			 
		Payment__c payment=CyberSourcePaymentController.insertPaymentRecord_New(reference_number, targetDate, newOrder.Id, null, transaction_type, paymentToken, false, purchaseType);

		// Update case to trigger workflow
				
		if(payment <> null && payment.Payment_Status__c == PAYMENT_STATUS_CHARGED) {
			if(selectedCase.SIRAS_Date__c == null) {
				selectedCase.Status = CaseManagement.CASE_STATUS_ASSIGNED_CORPORATE_TEAM_T3;
			} else {
				selectedCase.Status = CaseManagement.CASE_STATUS_PAYMENT_COMPLETE;
			}
		}
		
	 // If Payment is settled

		System.Debug('>>>>>>>>>>+Payment<<<<<<<<<' +payment);
		if(payment <> null && payment.Payment_Status__c == 'Settled' && paymentToken <> null) {
			selectedCase.Status = CaseManagement.CASE_STATUS_PAYMENT_COMPLETE;
		}else{
			if(String.isEmpty(paymentToken)&& Apexpages.currentPage().getParameters().ContainsKey('Settle') ){
				List<Group> groups = [Select g.Id From Group g where Name = 'Tier 2 Support' and Type = 'Queue' limit 1];
				if(groups.size() > 0)
					selectedCase.OwnerId = groups[0].Id;
				selectedCase.Status = CASE_STATUS_FAILED;
			}else{

				// If settled Payment is faile 
				if(payment <> null && payment.Payment_Status__c != 'Settled' && paymentToken <> null) {
					List<Group> groups = [Select g.Id From Group g where Name = 'Tier 2 Support' and Type = 'Queue' limit 1];
					if(groups.size() > 0)
						selectedCase.OwnerId = groups[0].Id;
					selectedCase.Status = CASE_STATUS_FAILED;
				}
			}
		}

		//LM 04/02/2014 : Commented below if statement.
		/*if(selectedCase!=null){
			update selectedCase;
		}//end-if
        */
              
		if(payment <> null ) {
			Asset__c ast;
			for(Product__c prod : [select Duration__c, Sub_type__c from Product__c where Id = : selectedCase.SCEA_Product__c]) {
				ast = new Asset__c(Id = newOrder.Asset__c);
				// I-102424 -> No PPP values should be set on assets which PPP has not been purchased.
				//LM 03/31/2014: Added Payment Status criteria to the If condition - JIRA# SF-136
				System.debug('>>>>>>>>>NewAsset<<<<<<<<<' +ast); 
				//LM 05/19/2014: updated "selectedCase.Asset__r.PPP_Product__c!=null" condition to selectedCase.SCEA_Product__c!=null
				if(selectedCase!=null && selectedCase.Asset__c!=null && selectedCase.SCEA_Product__c!=null && payment.Payment_Status__c == PAYMENT_STATUS_CHARGED){
					//LM 05/19/2014 Added below statement
					ast.PPP_Product__c=selectedCase.SCEA_Product__c;
					ast.PPP_Purchase_Date__c= payment.Transaction_Date__c;
					ast.PPP_Start_Date__c = payment.Transaction_Date__c;
					ast.PPP_End_Date__c = payment.Transaction_Date__c == null || prod.Duration__c == null ? null : payment.Transaction_Date__c.addMonths((Integer)prod.Duration__c);
					//if(payment.Payment_Status__c == PAYMENT_STATUS_CHARGED){
					ast.PPP_Status__c='Pending Confirm';
						//ast.PPP_Refund_Eligible__c = true;
					//}
					//LM 03/31/2014 : Added the if condition
					if(prod.Sub_Type__c == 'AD') {
					    ast.Coverage_Type__c = prod.Sub_Type__c;
				    } else {
					ast.Coverage_Type__c = 'Standard';
				    }
				}
				else {
				     if (payment.Order__c == newOrder.Id && newOrder.Order_Type__c == 'ESP' && payment.Payment_Status__c == PAYMENT_STATUS_DECLINED) {
						ast.PPP_Product__c = null;
					    ast.PPP_Purchase_Date__c = null;
					    ast.PPP_Start_Date__c = null;
					    ast.PPP_End_Date__c = null;
					    ast.PPP_Status__c = null;
					    ast.Coverage_Type__c = null;
					    ast.PPP_Refund_Eligible__c = false;
					    selectedCase.SCEA_Product__c = null;
					}
					
					//system.debug ('<<<<<<<<<< Asset PPP Product ' + ast.PPP_Product__c + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
				}
				
				//LM 03/31/2014 : commented the below if/else condition.
				/*if(prod.Sub_Type__c == 'AD') {
					ast.Coverage_Type__c = prod.Sub_Type__c;
				} else {
					ast.Coverage_Type__c = 'Standard';
				} */
			} 
			
			//LM 04/02/2014 : Added if statement
			if(selectedCase!=null){
			update selectedCase;
		    }
			
			if(ast <> null && ast.Id <> null) update ast;
			
						
			// In case payment token is passes as blank in URL
			if(paymentToken <> null && paymentToken == '') {
				payment.Payment_Status__c = 'Declined';
			}
			upsert payment;
			//Order__c updateOrder = new Order__c(Id = newOrder.Id);
			//updateOrder.Payment__c = payment.Id;
			//update updateOrder; 
		}

		return Cancel(); 
	}

	public static Payment__c insertPaymentRecord_New(String reference_number, String targetDate, string orderId, String paymentId, String transaction_type, String paymentToken, boolean InsertPay, String paymentType) {

		System.debug('======Within insertPaymentRecord============REF Number: '+reference_number+'---TargetDate: '+targetDate+'---OrderID: '+orderId+'---PaymentID: '+paymentId);

		Payment__c payment = CyberSourceUtility.getTransaction_New(reference_number, targetDate, '', paymentType);

		if(payment==null){
			return payment;
		}

		payment.Order__c = orderId;
		payment.Purchase_Type__c = paymentType;
		System.debug('======Payment Record To Be Inserted======' + payment);

		try {
			if(paymentId <> null) {
				payment.Id = paymentId;
				//upsert payment;
			} //else {
			//insert payment;
			//}
			System.debug('------transaction_type-----------' + transaction_type);
			if((payment.Payment_Status__c == PAYMENT_STATUS_CHARGED || payment.Payment_Status__c == PAYMENT_STATUS_SETTLED) && transaction_type == 'authorization,create_payment_token') {
				payment.Payment_Status__c = 'Authorized';
			}

			if(paymentToken <> null && payment.Payment_Status__c == PAYMENT_STATUS_CHARGED) {
				return null;
			}
			if(InsertPay){
				upsert payment;
				//Just update the Order to trigger the WF to send email if charged.
				Order__c order =new Order__C(id=orderId);
				update order;
			}

		} catch(Exception ex) {
			System.debug('==ERROR WHILE INSERTING PAYMENT RECORD===' + ex.getMessage());
		}

		//update the Payment__c field on the Order__c object with the new payment record id
		/*
        Order__c updateOrder = new Order__c(Id = orderId);
        updateOrder.Payment__c = payment.Id;
        update updateOrder; 
		 */

		return payment;

	}


	private boolean validateCurrentTransaction(Case cs) {

		boolean valid = true;

		Integer noOfOrders = [select count() from Order__c where Case__c = :cs.Id];

		if(noOfOrders >= MAX_ORDERS) {
			valid = false; 
		}

		return valid;
	}

	private boolean updateCaseDetails(Case cs) {

		boolean valid = true;

		for( Case c : [Select id, order__c, Asset__c, Asset__r.PPP_Product__c , order__r.order_date__c, Order__r.Payment__r.Payment_Status__c,SCEA_Product__c,Order__r.Asset__c, SIRAS_Date__c,
		               Order__r.Payment__c, Order__r.Name, Order__r.Payment__r.Transaction_Date__c, Order__r.Payment__r.Transaction_Number__c   
		               From Case 
		               where
		               Order__c != null 
		               and Id = :cs.Id ]){


			//if(c.Order__r.Payment__c == null || (c.Order__r.Payment__r.Payment_Status__c == PAYMENT_STATUS_DECLINED && c.Order__r.Payment__r.Transaction_Number__c == null)){
			
			//LM Added
			system.debug('<<<<In Update Case Details' + c + '>>>>>>>>>>>>');
			system.debug('<<<<Payment' + c.Order__r.Payment__c + '>>>>>>>>>>>>');
			system.debug('<<<<Payment Status' + c.Order__r.Payment__r.Payment_Status__c + '>>>>>>>>>>>>');
			
			if(c.Order__r.Payment__c == null || (c.Order__r.Payment__r.Payment_Status__c == PAYMENT_STATUS_DECLINED && c.Order__r.Payment__r.Transaction_Date__c == null)){

				// payment details are missing from previous order
				// need to get it before proceeding with payment

				System.debug('>>>>>>>>>>>>Order exists but Payment does not exist. Trying to retrieve payment status from Cybersource<<<<<<<<<<<>');

				Date orderDate = c.order__r.order_date__c;
				String year = String.valueOf(orderDate.year());
				String targetDate = year;               
				String month = String.valueOf(orderDate.month()).length() == 2 ? '' + orderDate.month() : '0' + orderDate.month();
				targetDate += month;
				String day = String.valueOf(orderDate.day()).length() == 2 ? '' + orderDate.day() : '0' + orderDate.day();
				targetDate += day;
				System.debug('>>>>>>>>>>>>targetDate='+targetDate);
				Payment__c payment=CyberSourcePaymentController.insertPaymentRecord_New(c.Order__r.Name, targetDate, c.order__c, c.Order__r.Payment__c, transaction_type, paymentToken, false, purchaseType);

				System.debug('>>>>>>>>>>>>PAYMENT INFO ==='+payment);

				if(payment==null){

					System.debug('>>>>>>>>>>>>PAYMENT IS NULL ===');
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to retrieve payment for existing order.  Please contact your supervisor.'));
					valid=false;

				}else{

					// Update case to trigger workflow
					System.debug('>>>>>>>>>>>>PAYMENT NOT NULL. UPDATING CASE STatus ===');

					if(cs!=null){
						if(payment <> null && payment.Payment_Status__c == PAYMENT_STATUS_CHARGED) {
							if(cs.SIRAS_Date__c == null) {
								cs.Status = CaseManagement.CASE_STATUS_ASSIGNED_CORPORATE_TEAM_T3;
							} else {
								cs.Status = CaseManagement.CASE_STATUS_PAYMENT_COMPLETE;
							}
						}
						update cs;
					}
					
										
					if(payment <> null ) {
						Asset__c ast;
						for(Product__c prod : [select Duration__c, Sub_Type__c from Product__c where Id = : c.SCEA_Product__c]) {
							ast = new Asset__c(Id = c.Order__r.Asset__c);
							// I-102424 -> No PPP values should be set on assets which PPP has not been purchased.
							
							//LM 03/31/2014 - Added Payment Status criteria to the if condition
							if(c!=null && c.Asset__c!=null && c.Asset__r.PPP_Product__c!=null && payment.Payment_Status__c <> PAYMENT_STATUS_DECLINED){
								ast.PPP_Start_Date__c = payment.Transaction_Date__c;
								ast.PPP_End_Date__c = payment.Transaction_Date__c == null || prod.Duration__c == null ? null : payment.Transaction_Date__c.addMonths((Integer)prod.Duration__c);
							}
						}
						if(payment.Payment_Status__c == PAYMENT_STATUS_CHARGED) {
							/// I-102424 -> No PPP values should be set on assets which PPP has not been purchased.
							if(c!=null && c.Asset__c!=null && c.Asset__r.PPP_Product__c!=null){
								ast.PPP_Refund_Eligible__c = true;
							}
						}
						
					  if(ast <> null) update ast;

						//Order__c updateOrder = new Order__c(Id = c.order__c);
						//updateOrder.Payment__c = payment.Id;
						//update updateOrder; 
						upsert payment;
					}


					if(payment.Payment_Status__c == PAYMENT_STATUS_CHARGED || payment.Payment_Status__c == PAYMENT_STATUS_SETTLED || payment.Payment_Status__c == PAYMENT_STATUS_AUTHORIZED){

						ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Payment Details have been retrieved for existing order.  Payment is not required.'));
						valid=false;

					}
				}

			}
			//end-if

		}//end-for


		return valid;
	}

	public PageReference cancel() {
		PageReference nextPage = Page.CyberSourcePaymentComplete;
		nextPage.setRedirect(true);
		nextPage.getParameters().put('caseId',caseId); 
		return nextPage;
	}

	public PageReference complete() {
		return new PageReference('/' + caseId);
	}
}