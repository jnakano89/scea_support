//**************************************************************************/
// (c) 2013 Appirio, Inc.
//
// Description : Test class for ContactButtonsController.
//                  
// Original August 30, 2013  : Vinod Kumar(JDC)
// 
//
// ***************************************************************************/
@isTest
private class ContactButtonsController_Test {

    static testMethod void myUnitTest() {
        // create test data
        Contact testContact = TestClassUtility.createContact(1,  true)[0];
        ApexPages.currentPage().getParameters().put('id', testContact.Id);
        ContactButtonsController controller = new ContactButtonsController();
        //controller.resetPassword();        
        controller.showPSNetwork();
        controller.purchase();
        controller.showGeneralSubs();
        controller.showVoucher();
        controller.troubleshoot();
        controller.refund();
        controller.showDetails();
        list<Case> lstCases = [select ID from case where ContactId =: testContact.ID];
        system.assert(lstCases.size() > 0);
    }
}