@isTest(seeAllData = true)
public class LookupFieldUpdateTriggerTest{
       
       
        
  static testMethod void myUnitTest() {
  
      String RecTypeId= [select Id from RecordType where (Name='Person Account') and (SobjectType='Account')].Id; 
      PSN_Account__C psnAcc1 = [select Id,consumer__r.AccountId,Name from PSN_Account__c where Name='xgamertomx'];
  
  
      Account acc = new Account();
      acc.LastName = 'test';
      acc.FirstName = 'ftest';
      acc.Phone = '12345';
      acc.PSN_Account__c = psnAcc1.Id;
      acc.RecordTypeID=RecTypeId;
      insert acc;
      
     Contact repContact = [SELECT id,FirstName, LastName
     FROM Contact
     WHERE isPersonAccount = true AND AccountId = :acc.id];
      
       
        
      PSN_Account__c psnAcc = new PSN_Account__c();
      psnAcc.Consumer__c = repContact.id;
      psnAcc.MDM_Account_ID__c = '13456';
      psnAcc.Status__c = 'Test';
      psnAcc.Name = 'Test001';
      insert psnAcc;
      
      
      psnAcc.Status__c = 'Test2';
      update psnAcc;
      
      
      acc.PSN_Account__c = psnAcc.Id;
      update acc;
      
      System.AssertEquals(psnAcc.id,acc.PSN_Account__c);
           
      }    
            
 }