/***************************************************************************************************
Class           : WebRMAController_Test
Description     : WebRMA Site Framework Controller Test Class
Developer       : Briggs, Aaron
Created Date    : 09/22/2014
Initiative      : WebRMA 3.0 [SF-1047]
Modified        :
                  04/28/2015 : Leena Mandadapu : Updated to fix the Too Many SOQLs issue.
                  06/20/2015 : Leena Mandadapu : SMS-1384 - Updated to add PPP Fee type test cases
***************************************************************************************************/
@isTest (SeeAllData=true)
private class WebRMAController_Test {
    static Account testAccount;
    static Contact testContact;
    static Address__c testAddress;
    static Order__c testOrder;
    static Order_Line__c testOrderLine;
    static Asset__c testAsset1;
    static Asset__c testAsset2;
    static Asset__c testAsset3;
    static Asset__c testAsset4;
    static Asset__c testAsset5;
    static Asset__c testAsset6;
    static Case testCase1;
    static Case testCase2;
    static Case testCase3;
    static Case testCase4;
    static Case testCase5;
    static Case testCase6;
    static Case testCase7;
    static Case testCase8;
    static Case testCase9;
    static Case testCase10;
    static Product__c ADprod;
    static String CaseRecTypeId = [SELECT Id FROM RecordType WHERE (Name='Service') AND (SobjectType='Case')].Id;

    static testMethod void myLookupTests() {
        createUserData();
        createLookupData1();
       // createAssetData();
        Test.startTest();
        createLookupData2();

        PageReference lookupRef = Page.ServiceLookup;
        Test.setCurrentPage(lookupRef);

        //Test.startTest();
        //System Shipped [FedEx]
        WebRMAController controller1 = new WebRMAController();
        controller1.serviceRequestNumber = testCase1.External_Case_Number__c;
        controller1.lookupServiceRequest();
        //System Shipped [Purolator]
        WebRMAController controller2 = new WebRMAController();
        controller2.serviceRequestNumber = testCase2.External_Case_Number__c;
        controller2.lookupServiceRequest();
        //System Shipped [USPS]
        WebRMAController controller3 = new WebRMAController();
        controller3.serviceRequestNumber = testCase3.External_Case_Number__c;
        controller3.lookupServiceRequest();
        //System Shipped [Incomplete]
        WebRMAController controller4 = new WebRMAController();
        controller4.serviceRequestNumber = testCase4.External_Case_Number__c;
        controller4.lookupServiceRequest();
        //System Not Received
        WebRMAController controller5 = new WebRMAController();
        controller5.serviceRequestNumber = testCase5.External_Case_Number__c;
        controller5.lookupServiceRequest();
        //System Received
        WebRMAController controller6 = new WebRMAController();
        controller6.serviceRequestNumber = testCase6.External_Case_Number__c;
        controller6.lookupServiceRequest();
        //Undefined
        WebRMAController controller7 = new WebRMAController();
        controller7.serviceRequestNumber = testCase7.External_Case_Number__c;
        controller7.lookupServiceRequest();
        //systemIncomplete
        WebRMAController controller8 = new WebRMAController();
        controller8.serviceRequestNumber = '';
        controller8.lookupServiceRequest();
        //systemError
        WebRMAController controller9 = new WebRMAController();
        controller9.serviceRequestNumber = 'ABC';
        controller9.lookupServiceRequest();
        Test.stopTest();
    }

    //In Warranty Lookup Test
    static testMethod void myServiceTest1() {
        createUserData();
        createAssetData();

        PageReference systemRef = Page.ServiceSystem;
        Test.startTest();
        Test.setCurrentPage(systemRef);
        WebRMAController controller1 =  new WebRMAController();
        controller1.switchLanguage();
        controller1.selectedAsset.Serial_Number__c = testAsset1.Serial_Number__c;
        controller1.identifyHardware();
        controller1.toIssuePage();
        controller1.getIssuetypes();
        controller1.getIssuedetails();
        controller1.getDiscstates();
        controller1.issuetype = 'Power';
        controller1.issuedetail = 'No Power Light';
        controller1.discstate = 'Yes';
        controller1.selectedCase.Disc_Title__c = 'Destiny';
        controller1.toInfoPage();
        controller1.getCountryValues();
        controller1.selectedShipAddress.Country__c = 'Canada';
        controller1.getStateValues();
        controller1.selectedShipAddress.Country__c = 'USA';
        controller1.getStateValues();
        controller1.selectedAccount.FirstName = testAccount.FirstName;
        controller1.selectedAccount.LastName = testAccount.LastName;
        controller1.selectedAccount.Phone = testAccount.Phone;
        controller1.selectedAccount.PersonEmail = testAccount.PersonEmail;
        controller1.selectedShipAddress.Address_Line_1__c = testAddress.Address_Line_1__c;
        controller1.selectedShipAddress.Address_Line_2__c = testAddress.Address_Line_2__c;
        controller1.selectedShipAddress.City__c = testAddress.City__c;
        controller1.selectedShipAddress.Country__c = testAddress.Country__c;
        controller1.selectedShipAddress.State__c = testAddress.State__c;
        controller1.selectedShipAddress.Postal_Code__c = testAddress.Postal_Code__c;
        controller1.selectedCase.Consumer_Confirmation__c = true;
        controller1.selectedCase.TOS_Confirmation__c = true;
        controller1.createServiceRecords();
        controller1.selectedAsset.Model_Number__c = testAsset1.Model_Number__c;
        controller1.toConfirmPage();
        controller1.getFeeOptions();
        controller1.toPaymentPage();
        
        //Validate Stand-Alone Pages
        controller1.toLookupPage();
        controller1.toSystemPage();
        controller1.toTermsPage();
        controller1.toErrorPage();
        controller1.toCompletePage();
        Test.stopTest();
    }

    //Out of Warranty Test
    static testMethod void myServiceTest2() {
        createUserData();
        createAssetData();

        PageReference systemRef = Page.ServiceSystem;
        Test.startTest();
        Test.setCurrentPage(systemRef);
        WebRMAController controller2 =  new WebRMAController();
        controller2.selectedAsset.Serial_Number__c = testAsset2.Serial_Number__c;
        controller2.identifyHardware();
        controller2.toIssuePage();
        controller2.issuetype = 'Power';
        controller2.issuedetail = 'White LED Blinking';
        controller2.selectedCase.Disc_Stuck__c = 'Yes';
        controller2.selectedCase.Disc_Title__c = 'Infamous';
        controller2.toInfoPage();
        controller2.selectedAccount.FirstName = testAccount.FirstName;
        controller2.selectedAccount.FirstName = testAccount.FirstName;
        controller2.selectedAccount.Phone = testAccount.Phone;
        controller2.selectedAccount.PersonEmail = testAccount.PersonEmail;
        controller2.selectedShipAddress.Address_Line_1__c = testAddress.Address_Line_1__c;
        controller2.selectedShipAddress.Address_Line_2__c = testAddress.Address_Line_2__c;
        controller2.selectedShipAddress.City__c = testAddress.City__c;
        controller2.selectedShipAddress.Country__c = testAddress.Country__c;
        controller2.selectedShipAddress.State__c = testAddress.State__c;
        controller2.selectedShipAddress.Postal_Code__c = testAddress.Postal_Code__c;
        controller2.selectedCase.Consumer_Confirmation__c = true;
        controller2.selectedCase.TOS_Confirmation__c = true;
        controller2.createServiceRecords();
        controller2.toConfirmPage();
        controller2.payServiceFee = 'Yes';
        controller2.newCase.Fee_Type__c = 'Out Of Warranty';
        controller2.toPaymentPage();
        controller2.selectedBillAddress.Address_Line_1__c = testAddress.Address_Line_1__c;
        controller2.selectedBillAddress.Address_Line_2__c = testAddress.Address_Line_2__c;
        controller2.selectedBillAddress.City__c = testAddress.City__c;
        controller2.selectedBillAddress.Country__c = testAddress.Country__c;
        controller2.selectedBillAddress.State__c = testAddress.State__c;
        controller2.selectedBillAddress.Postal_Code__c = testAddress.Postal_Code__c;
        controller2.selectedBillAddress.X2_Letter_Country_Code__c = 'US';
        controller2.toPaymentUnsigned();
        controller2.paymentRequestURL = 'https://testsecureacceptance.cybersource.com/silent/pay';
        controller2.accessKey = '4508a00257f6315dbb088c7cf1e32b90';
        controller2.profileID = 'WebRMA3';
        controller2.transactionUUID = String.valueOf(Math.round((Math.random() * 1000000000)));
        controller2.signedFieldNames = 'access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,currency,amount,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_phone,bill_to_address_line1,bill_to_address_city,bill_to_address_state,bill_to_address_postal_code,bill_to_address_country';
        controller2.unsignedFieldNames = 'card_type,card_number,card_cvn,card_expiry_date';
        //String signedDateTime = String.valueOf(DateTime.now().timeGmt()).substring(0,8)  + 'Z';
        controller2.signedDateTime = '12/12/2014';
        controller2.locale = 'en';
        controller2.transactionType = 'authorization,create_payment_token';
        controller2.selectedOrder.External_ID__c = 'TEST-0000000001';
        controller2.crncy = 'USD';
        controller2.newCase.Total_Amount_to_Pay__c = 99.00;
        controller2.paymentMethod = 'card';
        controller2.selectedBillAddress.X2_Letter_Country_Code__c = 'US';
        controller2.cardType = '001';
        controller2.ccNum = '4111111111111111';
        controller2.ccCVN = '123';
        controller2.ccExpDate = '12-2018';
        controller2.mac1 = 'test';
        //controller2.postToCyberSource();
        
        Test.stopTest();
    }

    //PlayStation Protection Plan Test
    static testMethod void myServiceTest3() {
        createUserData();
        createAssetData();

        PageReference systemRef = Page.ServiceSystem;
        Test.startTest();
        Test.setCurrentPage(systemRef);
        WebRMAController controller3 =  new WebRMAController();
        controller3.selectedAsset.Serial_Number__c = testAsset3.Serial_Number__c;
        controller3.identifyHardware();
        Test.stopTest();
    }
    
     
    //Multiple Assets Found
    static testMethod void myServiceTest4() {
        createUserData();
        createAssetData();

        PageReference systemRef = Page.ServiceSystem;
        Test.startTest();
        Test.setCurrentPage(systemRef);
        WebRMAController controller4 = new WebRMAController();
        controller4.selectedAsset.Serial_Number__c = testAsset4.Serial_Number__c;
        controller4.identifyHardware();
        Test.stopTest();
    }

    //Asset Does Not Exist in SFDC (No Siras)
    static testMethod void myServiceTest5() {
        createUserData();
        createAssetData();

        PageReference systemRef = Page.ServiceSystem;
        Test.startTest();
        Test.setCurrentPage(systemRef);
        WebRMAController controller5 = new WebRMAController();
        controller5.selectedAsset.Serial_Number__c = 'MB0TEST122345';
        controller5.identifyHardware();
        Test.stopTest();
    }

    //Asset Does Not Exist in SFDC (Siras)
    static testMethod void myServiceTest6() {
        createUserData();
        createAssetData();

        PageReference systemRef = Page.ServiceSystem;
        Test.startTest();
        Test.setCurrentPage(systemRef);
        WebRMAController controller6 = new WebRMAController();
        controller6.selectedAsset.Serial_Number__c = 'CF623190775';
        controller6.identifyHardware();
        Test.stopTest();
    }

    //Loop Checks
    static testMethod void myServiceTest7() {
        createUserData();
        createAssetData();

        PageReference systemRef = Page.ServiceSystem;
        //To ServiceIssue : Loop Checks
        Test.startTest();
        Test.setCurrentPage(systemRef);
        WebRMAController controller7 = new WebRMAController();
         //LM 06/19/2015 : Added for PPP Fee Type
        controller7.pppContractNumber = '12345678';
        controller7.selectedAsset.PPP_Contract_Number__c = '12345678';
        controller7.showPPP = false;
        controller7.toIssuePage(); //-[Serial]
        controller7.selectedAsset.Serial_Number__c = 'test123098';
        controller7.toIssuePage(); //[Serial] - [Model]
        controller7.selectedAsset.Serial_Number__c = 'test123098';
        controller7.selectedAsset.Model_Number__c = 'CUH-1001A';
        controller7.toIssuePage(); //[Serial] + [Model] - [Purchase Date]
        //To SerivceInfo : Loop Checks
        controller7.toInfoPage(); //-[Sub Area]
        controller7.selectedCase.Sub_Area__c = 'Console';
        controller7.toInfoPage(); //[Sub Area] - [Issue1]
        controller7.selectedCase.Issue1__c = 'N/A';
        controller7.toInfoPage(); //[Sub Area] + [Issue1] - [Disc Stuck]
        controller7.selectedCase.Disc_Stuck__c = 'Yes';
        controller7.toInfoPage(); //[Sub Area] + [Issue1] + [Disc Stuck] - [Disc Title]
        controller7.selectedCase.Disc_Title__c = 'Infamous';
        controller7.toInfoPage(); //[Sub Area] + [Issue1] + [Disc Stuck] + [Disc Title]
        Test.stopTest();
    }

    //Error Checks
    static testMethod void myServiceTest8() {
        createUserData();
        createAssetData();

        PageReference systemRef = Page.ServiceSystem;
        Test.startTest();
        Test.setCurrentPage(systemRef);
        WebRMAController controller8 = new WebRMAController();
        controller8.toCompletePage();
        controller8.selectedAsset.Model_Number__c = 'CUH-1001A';
        controller8.calculateServiceCosts();
        controller8.payServiceFee = 'Yes';
        controller8.toPaymentPage();
        controller8.newCase.Fee_Type__c = 'In Warranty';
        controller8.toPaymentPage();
        //Issue Picklist Check
        controller8.issueType = 'Power';
        controller8.getIssuedetails();
        controller8.issueType = 'Audio/Video';
        controller8.getIssuedetails();
        controller8.issueType = 'Blu-Ray Disc Drive (BDD)';
        controller8.getIssuedetails();
        controller8.issueType = 'Hard Disc Drive (HDD)';
        controller8.getIssuedetails();
        controller8.issueType = 'Damaged Port';
        controller8.getIssuedetails();
        controller8.issueType = 'Controls (Dpad, joystick, touchscreen)';
        controller8.getIssuedetails();
        
        Test.stopTest();
    }
    
    //Validation Checks
    static testMethod void myServiceTest9(){
    	PageReference issueRef = Page.ServiceIssue;
    	Test.startTest();
    	Test.setCurrentPage(issueRef);
    	
    	WebRMAController controller9 = new WebRMAController();
    	//LM 06/19/2015 : Added for PPP Fee Type
        controller9.pppContractNumber = '12345678';
        controller9.showInWarrantywithAD = false;
    	controller9.issuetype = 'Power';
        controller9.issuedetail = 'No Power Light';
        controller9.discstate = 'No';
        controller9.toInfoPage();
        controller9.selectedAccount.PersonEmail = 'Test';
        controller9.toConfirmPage();
    	
    	Test.stopTest();
    }
    
    //PPP Test with AD plan for Cracked Screen
    static testMethod void myServiceTest10() {
        createUserData();
        createAssetData();

        PageReference systemRef = Page.ServiceSystem;
        Test.startTest();
        Test.setCurrentPage(systemRef);
        WebRMAController controller10 =  new WebRMAController();
        controller10.selectedAsset.Serial_Number__c = testAsset2.Serial_Number__c;
        controller10.systemClass = 'VIT';
        controller10.identifyHardware();
        controller10.pppContractNumber = '12345678';
        controller10.selectedAsset.PPP_Contract_Number__c = '12345678';
        controller10.toIssuePage();
        controller10.issuetype = 'Controls (Dpad, joystick, touchscreen)';
        controller10.issuedetail = 'Cracked Screen';
        controller10.selectedCase.Disc_Stuck__c = 'Yes';
        controller10.selectedCase.Disc_Title__c = 'Infamous';
        controller10.toInfoPage();
        controller10.selectedAccount.FirstName = testAccount.FirstName;
        controller10.selectedAccount.FirstName = testAccount.FirstName;
        controller10.selectedAccount.Phone = testAccount.Phone;
        controller10.selectedAccount.PersonEmail = testAccount.PersonEmail;
        controller10.selectedShipAddress.Address_Line_1__c = testAddress.Address_Line_1__c;
        controller10.selectedShipAddress.Address_Line_2__c = testAddress.Address_Line_2__c;
        controller10.selectedShipAddress.City__c = testAddress.City__c;
        controller10.selectedShipAddress.Country__c = testAddress.Country__c;
        controller10.selectedShipAddress.State__c = testAddress.State__c;
        controller10.selectedShipAddress.Postal_Code__c = testAddress.Postal_Code__c;
        controller10.selectedCase.Consumer_Confirmation__c = true;
        controller10.selectedCase.TOS_Confirmation__c = true;
        controller10.createServiceRecords();
        controller10.toConfirmPage();
        controller10.payServiceFee = 'Yes';
        controller10.newCase.Fee_Type__c = 'PlayStation Protection Plan';
        controller10.toPaymentPage();
        controller10.selectedBillAddress.Address_Line_1__c = testAddress.Address_Line_1__c;
        controller10.selectedBillAddress.Address_Line_2__c = testAddress.Address_Line_2__c;
        controller10.selectedBillAddress.City__c = testAddress.City__c;
        controller10.selectedBillAddress.Country__c = testAddress.Country__c;
        controller10.selectedBillAddress.State__c = testAddress.State__c;
        controller10.selectedBillAddress.Postal_Code__c = testAddress.Postal_Code__c;
        controller10.selectedBillAddress.X2_Letter_Country_Code__c = 'US';
        controller10.toPaymentUnsigned();
        controller10.paymentRequestURL = 'https://testsecureacceptance.cybersource.com/silent/pay';
        controller10.accessKey = '4508a00257f6315dbb088c7cf1e32b90';
        controller10.profileID = 'WebRMA3';
        controller10.transactionUUID = String.valueOf(Math.round((Math.random() * 1000000000)));
        controller10.signedFieldNames = 'access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,currency,amount,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_phone,bill_to_address_line1,bill_to_address_city,bill_to_address_state,bill_to_address_postal_code,bill_to_address_country';
        controller10.unsignedFieldNames = 'card_type,card_number,card_cvn,card_expiry_date';
        //String signedDateTime = String.valueOf(DateTime.now().timeGmt()).substring(0,8)  + 'Z';
        controller10.signedDateTime = '12/12/2014';
        controller10.locale = 'en';
        controller10.transactionType = 'authorization,create_payment_token';
        controller10.selectedOrder.External_ID__c = 'TEST-0000000001';
        controller10.crncy = 'USD';
        controller10.newCase.Total_Amount_to_Pay__c = 99.00;
        controller10.paymentMethod = 'card';
        controller10.selectedBillAddress.X2_Letter_Country_Code__c = 'US';
        controller10.cardType = '001';
        controller10.ccNum = '4111111111111111';
        controller10.ccCVN = '123';
        controller10.ccExpDate = '12-2018';
        controller10.mac1 = 'test';
        
        Test.stopTest();
    }
    
    
    
    //Create Generic User Data
    static void createUserData(){
        String PersonAccountId = [SELECT Id FROM RecordType WHERE SobjectType='Account' AND IsPersonType=True].Id;

        testAccount = new Account();
        testAccount.FirstName = 'Test';
        testAccount.LastName = 'Monkey';
        testAccount.Phone = '1234567890';
        testAccount.PersonEmail = 'test@monkey.com';
        testAccount.RecordTypeId = PersonAccountId;
        insert testAccount;

        testContact = [SELECT id,FirstName, LastName FROM Contact WHERE isPersonAccount = true AND AccountId =: testAccount.id];
        
        testAddress = new Address__c();
        testAddress.Address_Line_1__c = '2207 Bridgepoint Pkwy';
        testAddress.Address_Line_2__c = 'N/A';
        testAddress.City__c = 'San Mateo';
        testAddress.State__c = 'CA';
        testAddress.Postal_Code__c = '94404';
        testAddress.Country__c = 'USA';
        testAddress.Status__c = 'Active';
        testAddress.Address_Type__c = 'Shipping';
        testAddress.Consumer__c = testContact.Id;
        insert testAddress;
        
         //Create AD Plan
     	ADprod = new Product__c();
     	ADprod.Description__c = 'Test AD Plan';
     	ADprod.Name = 'Test PS4 AD Plan';
    	ADprod.Status__c = 'Active';
	    ADprod.Genre__c = 'PS4';
	    ADprod.Product_Type__c = 'ESP';
	    ADprod.Sub_Type__c = 'AD';
	    ADprod.Target_Country__c= 'USA';
	    ADprod.SKU__c = 'PS4ADH';
	    ADprod.Duration__c = 24;
	    ADprod.List_Price__c = 59.99;
	    ADprod.Tax_Code__c = 'ESP';
	    insert ADprod;
    }
    
    //Create Assets
    static void createAssetData(){
        //Create In Warranty Asset
        testAsset1 = new Asset__c();
        testAsset1.Asset_Status__c = 'Active';
        testAsset1.Serial_Number__c = 'MB01234567890';
        testAsset1.Purchase_Date__c = Date.today();
        testAsset1.Model_Number__c = 'CUH-1001A';
        insert testAsset1;
        //Create Out of Warranty Asset
        testAsset2 = new Asset__c();
        testAsset2.Asset_Status__c = 'Active';
        testAsset2.Serial_Number__c = 'MB01234567891';
        testAsset2.Purchase_Date__c = Date.today() - 400;
        testAsset2.Model_Number__c = 'CUH-1001A';
        insert testAsset2;
        //Create PlayStation Protection Plan Asset
        testAsset3 = new Asset__c();
        testAsset3.Asset_Status__c = 'Active';
        testAsset3.Serial_Number__c = 'MB01234567892';
        testAsset3.Purchase_Date__c = Date.today();
        testAsset3.Model_Number__c = 'CUH-1001A';
        testAsset3.PPP_Status__c = 'Active';
        testAsset3.PPP_Contract_Number__c = '12345678';
        testAsset3.PPP_Product__c = ADprod.Id;
        insert testAsset3;
        //Create Incomplete Asset
        testAsset4 = new Asset__c();
        testAsset4.Asset_Status__c = 'Active';
        testAsset4.Serial_Number__c = 'MB01234567893';
        testAsset4.Model_Number__c = 'CUH-1001A';
        insert testAsset4;
        //Non-Unique Asset
        testAsset5 = new Asset__c();
        testAsset5.Asset_Status__c = 'Active';
        testAsset5.Serial_Number__c = 'MB01234567894';
        testAsset5.Model_Number__c = 'CUH-1001A';
        insert testAsset5;
        testAsset6 = new Asset__c();
        testAsset6.Asset_Status__c = 'Active';
        testAsset6.Serial_Number__c = 'MB01234567894';
        testAsset6.Model_Number__c = 'CUH-1001A';
        insert testAsset6;
    }

    //Create WebRMA Lookup Test Scenario Data
    static void createLookupData1() {
        //String CaseRecTypeId = [SELECT Id FROM RecordType WHERE (Name='Service') AND (SobjectType='Case')].Id;
        
        testAsset1 = new Asset__c();
        testAsset1.Asset_Status__c = 'Active';
        testAsset1.Serial_Number__c = 'MB01234567890';
        testAsset1.Purchase_Date__c = Date.today();
        testAsset1.Model_Number__c = 'CUH-1001A';
        insert testAsset1;
        //System Shipped [FedEx]
        testCase1 = new Case();
        testCase1.External_SR_Number__c = 'TEST-1234567890';
        testCase1.Status = 'Ship Complete';
        testCase1.Outbound_Carrier__c = 'FedEx';
        Date date1 = date.parse('04/8/2014');
        testCase1.Shipped_Date__c = date1;
        testCase1.Outbound_Tracking_Number__c = '052300889521850';
        testCase1.Asset__c = testAsset1.Id;
        testCase1.RecordTypeId = CaseRecTypeId;
        testCase1.Product__c = 'PS4';
        testCase1.Sub_Area__c = 'Console';
        insert testCase1;
        //System Shipped [Purolator]
        testCase2 = new Case();
        testCase2.External_SR_Number__c = 'TEST-1234567891';
        testCase2.Status = 'Ship Complete';
        testCase2.Outbound_Carrier__c = 'Purolator';
        Date date2 = date.parse('11/21/2013');
        testCase2.Shipped_Date__c = date2;
        testCase2.Outbound_Tracking_Number__c = '602936227639';
        testCase2.Asset__c = testAsset1.Id;
        testCase2.RecordTypeId = CaseRecTypeId;
        testCase2.Product__c = 'PS4';
        testCase2.Sub_Area__c = 'Console';
        insert testCase2;
        //System Shipped [USPS]
        testCase3 = new Case();
        testCase3.External_SR_Number__c = 'TEST-1234567892';
        testCase3.Status = 'Received';
        testCase3.Outbound_Carrier__c = 'USPS';
        Date date3 = date.parse('04/9/2014');
        testCase3.Shipped_Date__c = date3;
        testCase3.Outbound_Tracking_Number__c = 'CW878548732US';
        testCase3.Asset__c = testAsset1.Id;
        testCase3.RecordTypeId = CaseRecTypeId;
        testCase3.Product__c = 'PS4';
        testCase3.Sub_Area__c = 'Console';
        insert testCase3;
    }
    
    static void createLookupData2(){
    	testAsset1 = new Asset__c();
        testAsset1.Asset_Status__c = 'Active';
        testAsset1.Serial_Number__c = 'MB01234567890';
        testAsset1.Purchase_Date__c = Date.today();
        testAsset1.Model_Number__c = 'CUH-1001A';
        insert testAsset1;
    	//System Shipped [Incomplete]
        testCase4 = new Case();
        testCase4.External_SR_Number__c = 'TEST-1234567893';
        testCase4.Status = 'Pending';
        testCase4.Asset__c = testAsset1.Id;
        testCase4.RecordTypeId = CaseRecTypeId;
        testCase4.Product__c = 'PS4';
        testCase4.Sub_Area__c = 'Console';
        insert testCase4;
        //System Not Received
        testCase5 = new Case();
        testCase5.External_SR_Number__c = 'TEST-1234567894';
        testCase5.Status = 'Ready for Service';
        testCase5.Asset__c = testAsset1.Id;
        testCase5.RecordTypeId = CaseRecTypeId;
        testCase5.Product__c = 'PS4';
        testCase5.Sub_Area__c = 'Console';
        testCase5.Ship_To__c = testAddress.Id;
        testCase5.Fee_Type__c = 'In Warranty';
        insert testCase5;
        //System Received
        testCase6 = new Case();
        testCase6.External_SR_Number__c = 'TEST-1234567896';
        testCase6.Status = 'Ship Ready';
        Date date6 = date.parse('02/18/2014');
        testCase6.Received_Date__c = date6;
        testCase6.Asset__c = testAsset1.Id;
        testCase6.RecordTypeId = CaseRecTypeId;
        testCase6.Product__c = 'PS4';
        testCase6.Sub_Area__c = 'Console';
        insert testCase6;
        //Undefined
        testCase7 = new Case();
        testCase7.External_SR_Number__c = 'TEST-1234567897';
        testCase7.Status = 'New';
        testCase7.Asset__c = testAsset1.Id;
        testCase7.RecordTypeId = CaseRecTypeId;
        testCase7.Product__c = 'PS4';
        testCase7.Sub_Area__c = 'Console';
        insert testCase7;
    }
}