/*
Created By     : Ranjeet Singh (JDC)
Updated Date   : Sept 13, 2013
Description    : DescribeUtility
 */

public without sharing class DescribeUtility {
	public static Map<String, Schema.SObjectType> globalDescribe{	  	
		get{	   	 
			if(globalDescribe==null){
				globalDescribe = Schema.getGlobalDescribe();
			}
			return globalDescribe;
		}
		private set{
			globalDescribe = value;
		}
	}
	//Retrive  sObject records with all fields by Ids.
	public static List<sObject> getsObjectById(List<Id> sobjectIds){
		List<sObject> sobjects =null;      
		List<String>sobjectFields = new List<String>();
		if(sobjectIds!=null && sobjectIds.size()>0){
			Schema.DescribesObjectResult sobjDes = sobjectIds[0].getSObjectType().getDescribe();
			string sObjName = sobjDes.getName();        
			sobjects = Database.Query(' select '+String.join(getFieldsForsObject(sobjDes), ', ')+' from '+sObjName+' where Id in: sobjectIds');
		}
		return sobjects;
	}  
	public static List<string> getFieldsForsObject(String sObjectName){
		return getFieldsForsObject(globalDescribe.get(sObjectName).getDescribe());
	}
	//Retrive all field Names of a sObject.
	public static List<string> getFieldsForsObject(DescribesObjectResult SObjectTypeP){
		List<string> fieldNames = new List<string>();
		for(SObjectField fld: SObjectTypeP.Fields.getMap().values()){      	
			Schema.DescribeFieldResult F = fld.getDescribe();
			fieldNames.add(F.getName());
		}
		return fieldNames;
	}
}