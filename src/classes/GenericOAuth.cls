/*
    Class Name : GenericOAuth
    Created By : Ranjeet Singh (JDC)
    Created Date : 27 Aug 2013
    Reason  :   T-175519, Generic class to handle OAuth connection for B2C integrations 
 */
public with sharing class GenericOAuth implements IOAuth {

    ////////////////////////////////////////////////////////////////
    //
    //JG - these need to be methods instead of properties because this class implements the IOAuth interface,
    //BUT WE DON"T NEED THIS MEMBERS IN GENERIC CLASS
    //
    ////////////////////////////////////////////////////////////////
    private String privateKey;
    private String consumerKey;
    private String token;

    public void setPrivateKey(String pk) { privateKey = pk; }
    public void setToken(String tk) { token = tk; }
    public void setConsumerKey(String value) { consumerKey = value; }
    //Member for class uses.
    private Map<String,String> parameters = new Map<String,String>();

    ////////////////////////////////////////////////////////////////
    //
    //Build parameters to generate signature.
    //
    ////////////////////////////////////////////////////////////////
    private void refreshParameters(map<String, object>params) {
        parameters.clear();
        for(object sfObject :params.values()){
            Web_Call_Setting_key__c aObject = (Web_Call_Setting_key__c)sfObject ;
            if(aObject.Key_Location__c!=null && aObject.Key_Location__c.equalsIgnoreCase('OAUTH') && (aObject.value__c!=null && aObject.value__c!='')){
                parameters.put(aObject.name,aObject.value__c);
            }
        }
    }

    ////////////////////////////////////////////////////////////////
    //
    //Build parameters from URL to generate signature.
    //
    ////////////////////////////////////////////////////////////////
    private Map<String,String> getUrlParams(String value) {

        Map<String,String> res = new Map<String,String>();
        if(value==null || value=='') {
            return res;
        }
        for(String s : value.split('&')) {
            //System.debug('In OAuth.getUrlParams: getUrlParams: '+s);
            List<String> kv = s.split('=');
            if(kv.size()>1) {
                // RFC 5849 section 3.4.1.3.1 and 3.4.1.3.2 specify that parameter names 
                // and values are decoded then encoded before being sorted and concatenated
                // Section 3.6 specifies that space must be encoded as %20 and not +
                String encName = EncodingUtil.urlEncode(EncodingUtil.urlDecode(kv[0], 'UTF-8'), 'UTF-8').replace('+','%20');
                String encValue = EncodingUtil.urlEncode(EncodingUtil.urlDecode(kv[1], 'UTF-8'), 'UTF-8').replace('+','%20');
                //System.debug('In OAuth.getUrlParams: :  -> '+encName+','+encValue);
                res.put(encName,encValue);
            }
        }
        return res;
    }

    ////////////////////////////////////////////////////////////////
    //
    //Build Base-String from parameters to generate signature.
    //
    ////////////////////////////////////////////////////////////////
    private String createBaseString(Map<String,String> oauthParams, HttpRequest req) {
        Map<String,String> p = oauthParams.clone();
        if(req.getMethod()!=null && req.getMethod().equalsIgnoreCase('post') && req.getBody()!=null && 
                req.getHeader('Content-Type')=='application/x-www-form-urlencoded') {
            p.putAll(getUrlParams(req.getBody()));
        }
        String host = req.getEndpoint();
        Integer n = host.indexOf('?');
        if(n>-1) {
            p.putAll(getUrlParams(host.substring(n+1)));
            host = host.substring(0,n);
        }
        List<String> keys = new List<String>();
        keys.addAll(p.keySet());
        keys.sort();

        String s = keys.get(0)+'='+p.get(keys.get(0));
        for(Integer i=1;i<keys.size();i++) {
            s = s + '&' + keys.get(i)+'='+p.get(keys.get(i));
        }

        // According to OAuth spec, host string should be lowercased, but Google and LinkedIn
        // both expect that case is preserved.
        return req.getMethod().toUpperCase()+ '&' + 
                EncodingUtil.urlEncode(host, 'UTF-8') + '&' +
                EncodingUtil.urlEncode(s, 'UTF-8');
    }

    ////////////////////////////////////////////////////////////////
    //
    //Build Base-String from parameters to generate signature.
    //
    ////////////////////////////////////////////////////////////////
    public void sign(HttpRequest req, String intuitTid, String intuitRequestid) {
        throw new GenericException('Method Not Implemented', 'This object only support signature "sign(HttpRequest req, map<String, object>param)"');
    }

    ////////////////////////////////////////////////////////////////
    //
    //Build Base-String from parameters to generate signature.
    //
    ////////////////////////////////////////////////////////////////
    public void sign(HttpRequest req, map<String, object>param){    
        try{
            if(param.containsKey('oauth_nonce')){
                Web_Call_Setting_key__c web = (Web_Call_Setting_key__c)param.get('oauth_nonce');
                web.Value__c=String.valueOf(Crypto.getRandomLong());
            }  
            if(param.containsKey('oauth_timestamp')){
                Web_Call_Setting_key__c web = (Web_Call_Setting_key__c)param.get('oauth_timestamp');
                web.Value__c=String.valueOf(String.valueOf(DateTime.now().getTime()/1000));
            }
            if(param.containsKey('oauth_token')){
                Web_Call_Setting_key__c web = (Web_Call_Setting_key__c)param.get('oauth_token');
                web.Value__c = UtilityGenericWebCallout.getOAuthToken();
            }


            //START Building the BASE-Signature
            refreshParameters(param); 

            String s = createBaseString(parameters, req);    
            system.debug('REQUEST : Signature base string:'+s);
            //END Building the BASE-Signature

            //START Enctript/sign the BASE-Signature
            Blob encodedKey = null;
            if(param.containsKey('oauth_privateKey')){
                encodedKey = Encodingutil.base64Decode(((Web_Call_Setting_key__c)param.get('oauth_privateKey')).Value__c);
            }    
            //system.debug('REQUEST : privateKey > '+((Web_Call_Setting_key__c)param.get('oauth_privateKey')).Value__c);
            String ALGORITHM = '';
            if(param.containsKey('oauth_signature_method')){
                ALGORITHM = ((Web_Call_Setting_key__c)param.get('oauth_signature_method')).Value__c;
            }    
            Blob sig = Crypto.sign(ALGORITHM, Blob.valueOf(s), encodedKey); 

            String signature = EncodingUtil.urlEncode(EncodingUtil.base64encode(sig), 'UTF-8');
            //System.debug('In OAuth.sign(): Signature after encoding: '+signature);
            //END Enctript the BASE-Signature

            //START Building OAUTH request.
            String header = 'OAuth ';
            for (String key : parameters.keySet()) {
                header = header + key + '="'+parameters.get(key)+'", ';
            } 
            header = header + 'oauth_signature="'+signature+'"';

            if(param.containsKey('Authorization')){
                req.setHeader('Authorization', header);
                system.debug('REQUEST : Header > Authorization : ' + header);
            }
        }
        catch (Exception ex){
            system.debug('Outh Authorization Exceptio: ' + ex);
            ExceptionHandler.logException(ex);
        }
    }

    public ENUM webServiceEnum {
        None,
        TTD_Tax_Calc,
        Order,
        Price
    }

    public virtual class GenericException extends Exception
    {
        public String name{
            get{
                return details.get('name');
            }
            set{
                details.put('name', value);
            }
        }
        public String description{
            get{
                return details.get('description');
            }
            set{
                details.put('description', value);
            }
        }
        public map<String, String>details{get;set;}
        public GenericException(String n, String d)
        {
            this(d);
            details = new map<String, String>();
            name = n;
            description = d;

        }
    }  
}