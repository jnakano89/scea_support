// Class           : reCaptchaUtility_Test
// Description     : Test Class for reCaptchaUtility Class
// Developer       : Briggs, Aaron
// Created Date    : 04/27/2015
// Modified        : 

@isTest
global class reCaptchaUtility_Test {

    static testMethod void reCaptchaHTTPTest1() {
        // PreChatEnglishForm Force.com Site User
        User u = [SELECT Id FROM User WHERE Id = '005i0000004mYKNAA2']; 

        System.runAs(u) {
            System.debug('Current User: ' + UserInfo.getUserName());
            
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HTTPMockReCaptcha());
            reCaptchaUtility.reCaptchaVerify('test');
        }
    }
    
    static testMethod void reCaptchaHTTPTest2() {
        // PreChatEnglishForm Force.com Site User
        User u = [SELECT Id FROM User WHERE Id = '005i0000004mYKNAA2']; 

        System.runAs(u) {
            System.debug('Current User: ' + UserInfo.getUserName());
            
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HTTPMockReCaptcha());
            reCaptchaUtility.reCaptchaVerify('');
        }
    }
    
    static testMethod void reCaptchaPublicTest1() {
    	Test.startTest();
    	
    	StateValuesRemote svr = new StateValuesRemote();
    	reCaptchaUtility rcu = new ReCaptchaUtility(svr);
    	
    	System.assertNotEquals(rcu.publicKey, 'test');
    }
    
    global class HTTPMockReCaptcha implements HttpCalloutMock {
    	global HTTPRESPONSE respond(HTTPRequest req) {
    		HttpResponse res = new HttpResponse();
    		String body = getBody();
    		res.setBody(body);
    		res.setStatusCode(200);
    		return res;
    	}
    	String getBody() {
    		String body = 'secret=' + reCaptcha__c.getInstance(UserInfo.getProfileId()).secretKey__c + '&response=' + 'test';
    		return body;
    	}
    }
}