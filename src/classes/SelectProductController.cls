/*******************************************************************************************************************************
//8/26/2013 : Urminder(JDC) : created this class.
// 9/4/2013  : Kapil(JDC) : Modified : Task: T-179757
Updates:
		01/17/2014 : Monali: Modified : added purchase date and state filter to product display
		05/16/2014 : Leena Mandadapu : Jira# SF-847 - commented logic that populates the PPP Product on Asset before successfull payment completion
		07/02/2014 : Leena Mandadapu : Jira# SF-1108 - Added logic for PPP Tax calculation
*********************************************************************************************************************************/
public with sharing class SelectProductController {
  
  public list<Product__c> productList{get;set;}
  
  public string caseId;
  public string title{get;set;}
  public string subTitle{get;set;}
  public String selectedProductId{get;set;}
  public Case selectedCase;
  
  // ADDED BY CM (CODE MERGE MANALI)
  
 
   public string selectedState{get;set;}//added 1/17/2014
   public Date   selectedPurchaseDate{get;set;}//added 1/17/2014
  
  
  public SelectProductController() {
  	productList = new list<Product__c>();
  	subTitle = 'Select Product';
  	
  	caseId = Apexpages.currentPage().getParameters().get('caseId');
  	
  	
  	
  	if(caseId <> null) {
  	  selectedCase = getCaseDetails(caseId);
  	  productList = getProductList();	
  	}
  	
  	
  }
  
  private Case getCaseDetails(String cId) {
    Case cs;
     for(case c : [select Id, SCEA_Product__c,Asset__r.Warranty_Available__c,Asset__c, 
                  Asset__r.Product_Genre__c,Ship_To__c, Ship_To__r.Country__c, Ship_To__r.State__c, Purchase_Date__c, SIRAS_Date__c  from case 
                  where id = :cId]) {
        cs = c;
        selectedProductId = c.SCEA_Product__c; 
        selectedState = c.Ship_To__r.State__c; //added 1/17/2014
        selectedPurchaseDate = c.Purchase_Date__c; //added 1/17/2014
        if  (c.Purchase_Date__c == NULL)//added 1/23/2014
            { //added 1/23/2014
            selectedPurchaseDate = c.SIRAS_Date__c;//added 1/23/2014
            }//added 1/23/2014
     }

    return cs;	
  }
  private List<Product__c> getProductList() {
  	
  	list<Product__c> pList = new list<Product__c>();
  	
  	String shipToCountry;
  	
  
  	
  	if(selectedCase.Asset__c != null 
  		&& selectedCase.Asset__r.Warranty_Available__c =='Yes' 
  		&& selectedCase.Ship_To__c != null ){
  		
  		if(selectedCase.Ship_To__r.Country__c=='US'){
  			shipToCountry='USA';	
  		}else if(selectedCase.Ship_To__r.Country__c=='CA'){
  			shipToCountry='Canada';	
  		}else if(selectedCase.Ship_To__r.Country__c=='CAN'){
  			shipToCountry='Canada';	
  		}else{ 			
  			shipToCountry=selectedCase.Ship_To__r.Country__c;	
  		}
	
  		for(Product__c p : [select Name, Description__c, Product_Type__c, Sub_Type__c, 
  								Genre__c, SKU__c, List_Price__c 
  							from Product__c
  							where (Sub_Type__c = 'ESP' or Sub_Type__c = 'AD') 
  								and Target_Country__c =: shipToCountry
  								and Genre__c =: selectedCase.Asset__r.Product_Genre__c
  								and Status__c != 'Inactive' ]) {
	  	  
                If (selectedState == 'GU' || selectedState == 'VI' || selectedState == 'AS' || selectedState == 'PR' || selectedState == 'MH' )//added 1/29/2014
                {}else//added 1/29/2014
                
                {//added 1/17/2014
                if(selectedPurchaseDate != NULL)//added 1/17/2014
                  {//added 1/17/2014
                   Date ThirtyDays = selectedPurchaseDate.addDays(30) ;//added 1/17/2014
                   Date OneYear = selectedPurchaseDate.addYears(1);//added 1/17/2014
                   Date ToDaysDate = SYSTEM.TODAY();//added 1/17/2014
                       if ( ThirtyDays >= ToDaysDate && p.Sub_Type__c == 'AD' )//added 1/17/2014
                           { //added 1/17/2014
                              pList.add(p);                         //added 1/17/2014
                            }//added 1/17/2014
                       if ( OneYear >= ToDaysDAte && p.Sub_Type__c == 'ESP' )//added 1/17/2014
                           {//added 1/17/2014
                              pList.add(p);//added 1/17/2014
                            }//added 1/17/2014
                   }//added 1/17/2014
                   else//added 1/17/2014
                   {//added 1/17/2014
                        pList.add(p);
                   }//added 1/17/2014
               }//added 1/17/2014
        }

  	}
  	
  	return pList;
  }
  public Pagereference selectProduct() {
  	String selectedProductId = ApexPages.currentPage().getParameters().get('ctRadio');
  	  	
  	if(selectedProductId == null) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select a Product'));
      return null;
    }
    //LM 07/01/2014 : added
  	//return updateObject(selectedProductId);
  	return updateObject(selectedProductId);
  }
  
  public Pagereference clearProduct() {
  	return updateObject(null);
  }
  
  public Pagereference returnToRecord() {
  	Pagereference pg = new ApexPages.StandardController(selectedCase).view();
  	system.debug('<<<<<<<<<Page'+pg);
  	pg.setRedirect(true); 
  	return pg;
  }
  
  private Pagereference updateObject(String productId){
  	selectedCase.SCEA_Product__c = productId;
  	//LM 07/01/2014 : added below statements
  	system.debug('<<<<<<<<<productList'+productList);
  	selectedCase.Unit_Price__c = 0.00;
  	selectedCase.Total_Tax__c = 0.00;
  	selectedCase.Total_Amount_to_Pay__c = 0.00;
  	/*for(Product__c prd : [select Id, List_Price__c from Product__c where Id = :productId]) {
       if(prd <> null) {
       	selectedCase.Unit_Price__c = prd.List_Price__c;
       }                   
  	} */
  	try{
  		update selectedCase;
  	}catch(Exception ex) {
  		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,ex.getMessage()));
  	}
  
  // Task 	
  //LM 05/16/2014 : Commented below If condition as it is writing Warranty product to Asset before Payment Complete.
  /*	if(selectedCase.Asset__c!=null){
  	
  	   Asset__c asset = new Asset__c(Id=+selectedCase.Asset__c);
  	   asset.PPP_Product__c= productId;
  	   
  	   try{
  	     Update Asset;
  	   }
  	   catch(Exception e){
  	   
  	   
  	   }
  	} */
  	return returnToRecord(); 
  }  
}