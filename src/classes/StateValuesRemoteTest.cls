/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class StateValuesRemoteTest {

    static testMethod void sValRemoteTest() {
        // TO DO: implement unit test
        String tempCo = StateValuesRemote.generateCookie(null);
        System.Cookie tempCookie = new Cookie('pge_cntid', 'T0sBVtl5l3l1ir4CJ6rP3wbMwih2xUgFLnujP/T4mUBG1ltMz6AMRQmAK4+HPeuc', null, 100000, false);
        system.assert(tempCo.length() >10);          
        String[] tempStr = tempCo.split(';');        
        tempStr = tempStr[0].split('=');               
        //String tempCo1 = StateValuesRemote.generateCookie('T0sBVtl5l3l1ir4CJ6rP3wbMwih2xUgFLnujP/T4mUBG1ltMz6AMRQmAK4+HPeuc');
        String tempCo1 = StateValuesRemote.generateCookie(tempStr[1]);
        system.assert(tempCo1.length() >10);
        Contact con = new Contact(LastName = 'lname', FirstName = 'fname', Email='random101714@x.com', ChatSessionCookie_Value__c='T0sBVtl5l3l1ir4CJ6rP3wbMwih2xUgFLnujP/T4mUBG1ltMz6AMRQmAK4+HPeuc');
        insert con;
        Contact con1 = new Contact(LastName = 'lname1', FirstName = 'fname1', Email='random101714@x.com', ChatSessionCookie_Value__c='T0sBVtl5l3l1ir4CJ6rP3wbMwih2xUgFLnujP/T4mUBG1ltMz6AMRQmAK4+HPeuc');
        insert con1;
        Boolean boolCheck = StateValuesRemote.checkBlockedUser('random101714@x.com', 'T0sBVtl5l3l1ir4CJ6rP3wbMwih2xUgFLnujP/T4mUBG1ltMz6AMRQmAK4+HPeuc', 'Random1', 'testurl', 'testip');
        system.assert(!boolCheck);
        Boolean boolCheck1 = StateValuesRemote.checkCookieUser('T0sBVtl5l3l1ir4CJ6rP3wbMwih2xUgFLnujP/T4mUBG1ltMz6AMRQmAK4+HPeuc');
        system.assert(boolCheck1);
                        
        StateValuesRemote svr = new StateValuesRemote();
		PageReference pageRef = ApexPages.currentPage();
        Test.setCurrentPage(pageRef);           
        ApexPages.currentPage().setCookies(new Cookie[]{tempCookie});
        ApexPages.currentPage().getHeaders().put('True-Client-IP', '9.9.9.9999');              
        Pagereference pr = svr.validateIP();
        system.assert(pr==null);
        Pagereference pr1 = svr.validateCookie();
        system.assert(pr1.getUrl()=='/apex/LiveAgentError');
        Pagereference pr2 = svr.validateSession();
        system.assert(pr2.getUrl()=='/apex/LiveAgentError');
               
    }
}