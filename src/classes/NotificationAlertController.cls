//8/8/2013 : Urminder : created this class.
public class NotificationAlertController {
	public List<Notification__c> getMessageList(){		
		List<Notification__c> lst = [select Id, Name, Alert_Message__c, Detail_Text__c from Notification__c where Active__c = true order by Sort_Order__c];
		return lst;
	}
	
	static testMethod void NewsMessageAlert_Test(){
		Notification__c msg = new Notification__c();
		msg.Alert_Message__c = 'My test message';
		msg.Detail_Text__c = 'My Test Mesage detail';
		msg.Active__c = true;
		msg.Sort_Order__c = 1;
		insert msg;	
		NotificationAlertController ctrl = new NotificationAlertController();
		List<Notification__c> lst = ctrl.getMessageList();
		System.assertNotEquals(lst, null);
		
	}
}