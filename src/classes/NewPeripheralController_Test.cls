@isTest
private class NewPeripheralController_Test {

    static testMethod void myUnitTest() {
    	
    	Asset__c ast = TestClassUtility.createAssets(1, true)[0];
    	
    	Case testCase = TestClassUtility.createCase('Open', '', false);
    	testCase.Product__c = GeneralUtiltyClass.PRODUCT_PROMOTIONAL;
    	testCase.Asset__c = ast.Id;
    	insert testCase;
		
		Product_Filter_Mapping__c mapping = new Product_Filter_Mapping__c();
		mapping.Name = GeneralUtiltyClass.PRODUCT_PROMOTIONAL;
		mapping.Filter_Criteria__c = 'Product_Type__c = \'1st Party Peripheral\' AND Promo_Product__c = true AND Status__c = \'Active\'';
		insert mapping;
		
		    	
        list<Product__c> prodList = new list<Product__c>();
        for(Product__c prod : TestClassUtility.createProduct(10, false)) {
        	prod.Product_Type__c = '1st Party Peripheral';
        	prod.Promo_Product__c = true;
        	prod.Status__c = 'Active';
        	prodList.add(prod);
        }
        insert prodList;
        Apexpages.currentPage().getParameters().put('caseId', testCase.Id);
        NewPeripheralController ctrl = new NewPeripheralController();
        ctrl.selectPeripheral();
        ApexPages.currentPage().getParameters().put('ctRadio', prodList[0].Id);
        ctrl.selectPeripheral();
        
        Case updatedCase = [select Asset__c from Case where Id = :testCase.Id];
        System.assertNotEquals(updatedCase.Asset__c, ast.Id, 'Asset Id should be updated');
    }
}