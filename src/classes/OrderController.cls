public without sharing class OrderController {
	public Order__c orderObj{get;set;}
	   
	//---------------------------------------------------------------------------
    // Constructor and method
    //---------------------------------------------------------------------------
	public OrderController(ApexPages.StandardController controller) {
        orderObj =  (Order__c)controller.getRecord(); 
        orderObj = getOrderData();
    }
    //---------------------------------------------------------------------------
    // Method to get the order records
    //---------------------------------------------------------------------------
    private Order__c getOrderData() {
    	Order__c ord;
    	for(Order__c OrdData : [Select Id,Error_Message__c From Order__c Where Id = :orderObj.Id]){
    		ord = OrdData;	
    	}
    	return ord;	
    }
    
   
}