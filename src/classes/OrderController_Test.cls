@isTest
private class OrderController_Test {

    static testMethod void myUnitTest() {
        list<Asset__c> assetList = TestClassUtility.createAssets(1, false);
        assetList[0].Asset_Status__c = 'Inactive' ;
        assetList[0].PPP_Contract_Number__c = '1234';
        insert assetList;
        
        list<Address__c> addList = TestClassUtility.createAddress(1, true);
        list<Contact> cntList = TestClassUtility.createContact(2, false);
        list<Product__c> productList = TestClassUtility.createProduct(1, true);
        cntList[0].FirstName = 'Test Name';
        cntlist[0].Email = 'test@test.com';
        cntlist[0].Preferred_Language__c = 'Eng';
        cntlist[0].Bill_To__c = addList[0].Id;
        cntlist[0].Phone_Unformatted__c = '12345';
        insert cntlist;
        
        Case cs = TestClassUtility.createCase('New', 'TEst', false);
        cs.ContactId = cntList[0].Id;
        cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        cs.Sub_Area__c = 'NEw';
        cs.Status = 'New';
        cs.Fee_Type__c = 'PPP';
        cs.Shipped_Date__c = Date.today();
        cs.Service_Type__c = 'Recertified Exchange';
        insert cs;
        
        list<Order__c> ordrList = TestClassUtility.creatOrder(cntList[0].Id, 3, false);
        for(Order__c o : ordrList){
            o.Case__c = cs.Id;
            o.Asset__c = assetList[0].Id;
            o.Consumer__c = cntList[0].Id;
            o.Order_Type__c = 'ESP';
        }
        insert ordrList;
       
        Payment__c payment = new Payment__c();
        payment.Payment_Status__c = 'Charged';
        payment.Order__c = ordrList[0].Id;
        insert payment;
       
        ordrList[0].Payment__c = payment.Id;
        update ordrList[0];
         
        Test.startTest();
        PageReference pageRef = Page.OrderMessage;
        pageRef.getParameters().put('id', ordrList[0].Id);
        Test.setCurrentPageReference(pageRef);
        Apexpages.Standardcontroller stdCtrl = new Apexpages.Standardcontroller(ordrList[0]);
        OrderController ordController = new OrderController(stdCtrl);
        // verify Result
         
        System.assert(ordController.orderObj.Error_Message__c != null);
        Test.stopTest();    
    }
}