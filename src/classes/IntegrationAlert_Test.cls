@isTest
private class IntegrationAlert_Test {

    static testMethod void myUnitTest() {
       Contact cnt;
       try{
       	insert cnt;
       } catch(Exception ex) {
         IntegrationAlert.addException('Test Exception', ex);
         IntegrationAlert.addException('Test Exception', ex, 'requestMessage', ex.getMessage());
         IntegrationAlert.addException('Test Exception',  ex.getMessage(), ex);
         IntegrationAlert.addMessage('Test Exception',  ex.getMessage());
       }
       System.assertEquals(4, [select Id from Integration_Alert__c].size(),'Exception Records should be created');
    }
}