/*********************************************************************************************************************************************************************************************
CREATED BY: SALESFORCE PROFESSIONAL SERVICES TEAM

UPDATED BY:
            11/20/2014 : Leena Mandadapu : Jira# SMS-642 - Added debug statements.
            11/24/2014 : Leena Mandadapu : Jira# SMS-642 - Added SetReplyTo logic in buildParentCaseDefault method to fix the Social Case Threading issue
            02/24/2015 : Leena Mandadapu : Jira# SMS-813 - Added logic to fix the long ExternalPictureURL field length which is causing Social Persona creation issues.
            02/24/2015 : Leena Mandadapu : Jira# SMS-813 - Added logic to handle the Post tags greater than 255 chars. Case Social_Post_Tag__C field can only store upto 255 chars.
************************************************************************************************************************************************************************************************/
global virtual class SonyInboundSocialPostHandlerImpl implements Social.InboundSocialPostHandler {
    // Reopen case if it has not been closed for more than this number
    global virtual Integer getMaxNumberOfDaysClosedToReopenCase() {
        return 1;
    }
    global virtual String getDefaultAccountId() {
        return null;
    }

    global Social.InboundSocialPostResult handleInboundSocialPost(SocialPost post,
        SocialPersona persona, Map < String, Object > rawData) {
        system.debug('<<<<<<<<<<Inbound Social Post>>>>>>>>>>>>>>>'+post);
        system.debug('<<<<<<<<<<Inbound Social Persona>>>>>>>>>>>>'+persona);
        system.debug('<<<<<<<<<<Raw Data from Inbound post>>>>>>>>'+rawData);
        system.debug('<<<<<<<<<< Post information in socialpostimpl>>>>>>>'+post.isOutbound);
        
        //LM 02/26/2015 : Update input ExternalPictureURL value to have Max 255 chars due to field data type length limitation in Social Persona Object
        if(persona != null)
         if(persona.ExternalPictureURL != null && String.isNotBlank(persona.ExternalPictureURL) && persona.ExternalPictureURL.length() > 255)
              persona.ExternalPictureURL =  'Picture URL too long';    
         //else no need to update ExternalPictureURL                                                          
        //else no need to make any changes to the ExternalPictureURL                             
        
        Social.InboundSocialPostResult result = new Social.InboundSocialPostResult();
        result.setSuccess(true);
        matchPost(post);
        matchPersona(persona);
        
        if (post.Id != null) {
            handleExistingPost(post, persona);
            return result;
        }
        
        setReplyTo(post, persona, rawData);
        buildPersona(persona);
        Case parentCase = buildParentCase(post, persona, rawData);
        system.debug('<<<<<<<<Parent Case for handleInboundSocialPost Method>>>>>>>'+parentCase);
        setRelationshipsOnPost(post, persona, parentCase);
        system.debug('<<<<<<<<<< Post information in socialpostimpl before post upsert>>>>>>>'+post.isOutbound);
        upsert post;
        system.debug('<<<<<<<<<<Inbound Social Post Id>>>>>>>>>>>>>>>'+post.Id +'<<<<<<Post Content>>>>>>'+post.Content);
        return result;
    }
    private void handleExistingPost(SocialPost post, SocialPersona persona) {
        system.debug('<<<<<<<<<<Inbound Social Post in handleExistingPost>>>>>>>>>>>>>>>'+post); 
        system.debug('<<<<<<<<<<Inbound Social Post in handleExistingPost>>>>>>>>>>>>>>>'+post.Id +'<<<<<<Post Content>>>>>>'+post.Content);
        system.debug('<<<<<<<<<< Post information in socialpostimpl handleExistingPost>>>>>>>'+post.isOutbound);
        //LM 11/24/2014
        //setReplyTo(post, persona, rawData);
        update post;
        if (persona.id != null)
            update persona;
        system.debug('<<<<<<<<<<Inbound Social Post in handleExistingPost>>>>>>>>>>>>>>>'+post); 
        system.debug('<<<<<<<<<<Inbound Social Post in handleExistingPost>>>>>>>>>>>>>>>'+post.Id +'<<<<<<Post Content>>>>>>'+post.Content);
        system.debug('<<<<<<<<<< Post information in socialpostimpl handleExistingPost>>>>>>>'+post.isOutbound);    
    }
    private void setReplyTo(SocialPost post, SocialPersona persona, Map < String, Object >
        rawData) {
        SocialPost replyTo = findReplyTo(post, persona, rawData);
        system.debug('<<<<<<<<<Reply To>>>>>>>>'+replyTo);
        if (replyTo.id != null) {
            post.replyToId = replyTo.id;
            post.replyTo = replyTo;
        }
    }
    private SocialPersona buildPersona(SocialPersona persona) {
        if (persona.Id == null)
            createPersona(persona);
        else
            update persona;
        return persona;
    }
    private Case buildParentCase(SocialPost post, SocialPersona persona, Map < String, Object >
        rawData) {
        Case parentCase = findParentCase(post, persona);
        system.debug('<<<<<<<<<<Parent Case in buildParentCase Method>>>>>>>>>'+parentCase);
        system.debug('<<<<<<<<<<Case should Reopen or not>>>>>>>>>'+caseShouldBeReopened(parentCase));
        system.debug('<<<<<<<<<<Skip Case Creation Indicator>>>>>>'+hasSkipCreateCaseIndicator(rawData));
        if (caseShouldBeReopened(parentCase))
            reopenCase(parentCase);
        else if (!hasSkipCreateCaseIndicator(rawData) && (parentCase.id == null ||
                parentCase.isClosed))
            parentCase = createCase(post, persona);
        system.debug('<<<<<<<Parent Case to return>>>>>>>>>'+parentCase);  
        return parentCase;
    }
    private boolean caseShouldBeReopened(Case c) {
        //LM 11/19/2014: Added debug statements to research issue SMS-647
        system.debug('<<<<<<<<<<<<Case to reopen>>>>>>>>>>'+c);
        system.debug('<<<<<<<<<<<<Case needs to Reopened Id>>>>>>>>>>>>>>'+c.Id);
        system.debug('<<<<<<<<<<<<Case needs to Reopened isClosed>>>>>>>>>>>>>>'+c.isClosed);
       // system.debug('<<<<<<<<<<<<Case needs to Reopened System Date time>>>>>>>>>>>>>>'+System.now());
        if(c.Id != null) {}
        // system.debug('<<<<<<<<<<<<Case needs to Reopened Closed Date + 1>>>>>>>>>>>>>>'+c.closedDate.addDays(getMaxNumberOfDaysClosedToReopenCase()));
        
        return c.id != null && c.isClosed && System.now() <
            c.closedDate.addDays(getMaxNumberOfDaysClosedToReopenCase());
    }
    private void setRelationshipsOnPost(SocialPost postToUpdate, SocialPersona persona,
        Case parentCase) {
        if (persona.Id != null)
            postToUpdate.PersonaId = persona.Id;
        if (parentCase.id != null)
            postToUpdate.ParentId = parentCase.Id;
    }


    private Case createCase(SocialPost post, SocialPersona persona) {
        Case newCase = new Case(subject = post.Name);
        system.debug('<<<<<<<<<<Case Reference in createCase Method>>>>>>'+newCase);
        system.debug('<<<<<<<<<<Social Post>>>>>>>>>>>>>>'+post);
        system.debug('<<<<<<<<<<Social Persona>>>>>>>>>>>'+persona);
        if (persona != null && persona.ParentId != null) {
            if (persona.ParentId.getSObjectType() == Contact.sObjectType)
                newCase.ContactId = persona.ParentId;
        }

        /*note requirements state only one category will be passed in the PostTag however the code can handle more.*/
        newCase.Social_Post_Tag__c = getCategoriesFromPostTag(post.PostTags);
        system.debug('<<<<<<<Social Post Tags>>>>>>>>>>'+newCase.Social_Post_Tag__c);

        newcase.Origin = ('Social');
        /*newCase.Social_Post_Tag__c=Post.PostTags;*/
        newCase.Priority = post.PostPriority;
        newCase.Social_Network__c = post.Provider;


        insert newCase;
        return newCase;
    }


    private Case findParentCase(SocialPost post, SocialPersona persona) {
        system.debug('<<<<<<<<<<Social Post>>>>>>>>>>>>>>'+post);
        system.debug('<<<<<<<<<<Social Persona>>>>>>>>>>>'+persona);
        system.debug('<<<<<<<<<<Social Post Reply To>>>>>>>>>>>>>>'+post.ReplyTo);
        system.debug('<<<<<<<<<<Social Post IsOutbound>>>>>>>>>>>'+post.ReplyTo.IsOutbound);
        system.debug('<<<<<<<<<<Social Post PersonaId>>>>>>>>>>>'+post.ReplyTo.PersonaId);
        Case parentCase = new Case();
        if (post.ReplyTo != null && (post.ReplyTo.IsOutbound || post.ReplyTo.PersonaId ==
                persona.Id)){
            parentCase = findParentCaseFromPostReply(post);
            system.debug('<<<<<<<<<<<<<<<<<<Parent Case from Reply in findParentCase Method'+parentCase);
        }
        else if ((post.messageType == 'Direct' || post.messageType == 'Private') &&
            post.Recipient != null && String.isNotBlank(post.Recipient)) {
            parentCase = findParentCaseFromRecipient(post, persona);
            system.debug('<<<<<<<<<<<<<<<<<<Parent Case from Recipient in findParentCase Method'+parentCase);
        }    
        return parentCase;
    }


    private Case findParentCaseFromPostReply(SocialPost post) {
        List < Case > cases = [SELECT Id, IsClosed, Status, ClosedDate FROM Case WHERE Id = : post.ReplyTo.ParentId LIMIT 1];
        system.debug('<<<<<<<<<<Case record in findParentCaseFromPostReply Method>>>>>>>>>>'+cases);
        if (!cases.isEmpty())
            return cases[0];
        return new Case();
    }
    private Case findParentCaseFromRecipient(SocialPost post, SocialPersona persona) {
        List < Case > cases = [SELECT Id, IsClosed, Status, ClosedDate FROM Case WHERE id = : findReplyToBasedOnRecipientsLastPostToSender(post, persona).parentId LIMIT 1];
        system.debug('<<<<<<<<<<Case record in findParentCaseFromRecipient Method>>>>>>>>>>'+cases);
        if (!cases.isEmpty())
            return cases[0];
        return new Case();
    }
    private void reopenCase(Case parentCase) {
        system.debug('<<<<<<<<<Case to reopen>>>>>>>>>'+parentCase);
        SObject[] status = [SELECT MasterLabel FROM CaseStatus WHERE IsClosed = false AND
            IsDefault = true
        ];
        system.debug('<<<<<<<Case Status>>>>>>>>>'+status);
        parentCase.Status = ((CaseStatus) status[0]).MasterLabel;
        update parentCase;
    }
    private void matchPost(SocialPost post) {
        system.debug('<<<<<<<<<<Social Post>>>>>>>>>>>>>>'+post);
        if (post.Id != null || post.R6PostId == null) return;
        List < SocialPost > postList = [SELECT Id FROM SocialPost WHERE R6PostId = : post.R6PostId LIMIT 1];
        system.debug('<<<<<<<<<<Social Post List>>>>>>>>>>>>>>'+postList);
        if (!postList.isEmpty())
            post.Id = postList[0].Id;
    }
    private SocialPost findReplyTo(SocialPost post, SocialPersona persona, Map < String,
        Object > rawData) {
        system.debug('<<<<<<<<<<Social Post>>>>>>>>>>>>>>>'+post);
        system.debug('<<<<<<<<<<Social Persona>>>>>>>>>>>>'+persona);
        system.debug('<<<<<<<<<<Raw Data from Inbound post>>>>>>>>'+rawData);
        if (post.replyToId != null && post.replyTo == null)
            return findReplyToBasedOnReplyToId(post);
        if (rawData.get('replyToExternalPostId') != null &&
            String.isNotBlank(String.valueOf(rawData.get('replyToExternalPostId'))))
            return findReplyToBasedOnExternalPostIdAndProvider(post,
                String.valueOf(rawData.get('replyToExternalPostId')));
        return new SocialPost();
    }
    private SocialPost findReplyToBasedOnReplyToId(SocialPost post) {
        system.debug('<<<<<<<<<<Social Post>>>>>>>>>>>>>>>'+post);
        List < SocialPost > posts = [SELECT Id, ParentId, IsOutbound, PersonaId FROM SocialPost
            WHERE id = : post.replyToId LIMIT 1
        ];
        system.debug('<<<<<<<<<<findReplyToBasedOnReplyToId Post list>>>>>>>>>>>'+posts);
        if (posts.isEmpty())
            return new SocialPost();
        return posts[0];
    }
    private SocialPost findReplyToBasedOnExternalPostIdAndProvider(SocialPost post, String externalPostId) {
        system.debug('<<<<<<<<<<Social Post>>>>>>>>>>>>>>>'+post);
        system.debug('<<<<<<<<<<Social Post External Post Id>>>>>>>>>>>>>>>'+externalPostId);
        List < SocialPost > posts = [SELECT Id, ParentId, IsOutbound, PersonaId FROM SocialPost
            WHERE Provider = : post.provider AND ExternalPostId = : externalPostId LIMIT 1
        ];
        system.debug('<<<<<<<<<<findReplyToBasedOnExternalPostIdAndProvider Post list>>>>>>>>>>>'+posts);
        if (posts.isEmpty())
            return new SocialPost();
        return posts[0];
    }
    private SocialPost findReplyToBasedOnRecipientsLastPostToSender(SocialPost post,
        SocialPersona persona) {
        system.debug('<<<<<<<<<<Social Post>>>>>>>>>>>>>>>'+post);
        system.debug('<<<<<<<<<<Social Persona>>>>>>>>>>>>'+persona);   
        List < SocialPost > posts = [SELECT Id, ParentId, IsOutbound, PersonaId FROM SocialPost
            WHERE provider = : post.provider AND OutboundSocialAccount.ProviderUserId = : post.Recipient
            AND ReplyTo.Persona.id = : persona.id ORDER BY CreatedDate DESC LIMIT 1
        ];
        system.debug('<<<<<<<<<<findReplyToBasedOnRecipientsLastPostToSender Post list>>>>>>>>>>>'+posts);
        if (posts.isEmpty())
            return new SocialPost();
        return posts[0];
    }


    private void matchPersona(SocialPersona persona) {
        system.debug('<<<<<<<<<<Social Persona>>>>>>>>>>>>'+persona);
        system.debug('<<<<<<<<<<Social Persona External Id>>>>>>>>>>>>'+persona.ExternalId);
        if (persona != null && persona.ExternalId != null &&
            String.isNotBlank(persona.ExternalId)) {
            List < SocialPersona > personaList = [SELECT Id, ParentId FROM SocialPersona WHERE
                Provider = : persona.Provider AND
                ExternalId = : persona.ExternalId LIMIT 1
            ];
            system.debug('<<<<<<<<<<matchPersona Post list>>>>>>>>>>>'+personaList);
            if (!personaList.isEmpty()) {
                persona.Id = personaList[0].Id;
                persona.ParentId = personaList[0].ParentId;
            }
        }
    }
    private void createPersona(SocialPersona persona) {
        system.debug('<<<<<<<<<<Social Persona>>>>>>>>>>>>'+persona);
        if (persona == null || (persona.Id != null && String.isNotBlank(persona.Id)) ||
            !isThereEnoughInformationToCreatePersona(persona))
            return;
        SObject parent = createPersonaParent(persona);
        system.debug('<<<<<<<<<<Social Persona Parent>>>>>>>>>>>>'+parent);
        persona.ParentId = parent.Id;
        insert persona;
    }
    private boolean isThereEnoughInformationToCreatePersona(SocialPersona persona) {
        system.debug('<<<<<<<<<<Social Persona>>>>>>>>>>>>'+persona);
        return persona.ExternalId != null && String.isNotBlank(persona.ExternalId) &&
            persona.Name != null && String.isNotBlank(persona.Name) &&
            persona.Provider != null && String.isNotBlank(persona.Provider) &&
            persona.provider != 'Other';
    }
    private boolean hasSkipCreateCaseIndicator(Map < String, Object > rawData) {
        Object skipCreateCase = rawData.get('skipCreateCase');
        system.debug('#####################################################################################');
        system.debug('skipCreateCase'+skipCreateCase);
        system.debug('#####################################################################################');
        return skipCreateCase != null &&
            'true'.equalsIgnoreCase(String.valueOf(skipCreateCase));
    }
    global virtual SObject createPersonaParent(SocialPersona persona) {
        system.debug('<<<<<<<<<<createPersonaParent Method ------------ Social Persona>>>>>>>>>>>>'+persona);
        String name = persona.Name;
        if (persona.RealName != null && String.isNotBlank(persona.RealName))
            name = persona.RealName;
        String firstName = '';
        String lastName = 'unknown';
        if (name != null && String.isNotBlank(name)) {
            firstName = name.substringBeforeLast(' ');
            lastName = name.substringAfterLast(' ');
            if (lastName == null || String.isBlank(lastName))
                lastName = firstName;
        }
        //Code added by Preetu :10/10/2014
        if (firstName.length() > 40)
            firstName = firstName.substring(0, 38);
            
        //02-25-2015 LM : if the Last name is greater than 80 chars then truncate the string as Max field length is 80 chars.
        system.debug('<<<<<<<Last Name of the Social Persona = >>>>>>>>>>>>'+lastName);
        lastName = (lastName != null && lastName.length() > 80) ? lastName.substring(0,79) : lastName;    
                     
        Contact contact = new Contact(LastName = lastName, FirstName = firstName);
        system.debug('<<<<<<<<<<Contact>>>>>>>>>>>>'+contact);
        String defaultAccountId = getDefaultAccountId();
        system.debug('<<<<<<<<<<Default Account Id>>>>>>>>>>>>'+defaultAccountId);
        if (defaultAccountId != null)
            contact.AccountId = defaultAccountId;
        insert contact;
        return contact;
    }

    //Code change done:Preetu 

    /***
          Currently R6 is not setting catagories. We are using post tags with a token to pull our catagories.
          Catagory-VALUE:
      ****/
    private String getCategoriesFromPostTag(String postTags) {
        //adding null check on post tags
        system.debug('<<<<<<Post Tags>>>>>>>>'+postTags);
        if (String.isNotBlank(postTags)) {
            List < String > cat1 = postTags.split(',');
            system.debug('<<<<<<<Post Tags List>>>>>>>'+cat1);
            String catFinal = '';
            if (cat1.size() > 0)
                for (String str: cat1) {
                    catFinal = catFinal + str + ' ';
    
                }
            system.debug('<<<<<<Post Tags to return>>>>>>>>'+catFinal);  
            //LM 02/24/2015 : Added below logic to fix the Social_post_tag__c field length issue on case. Max field length for social_post_tag__c on case is 75 chars
            catFinal = catFinal != null ? (catFinal.length() < 255 ? catFinal : catFinal.substring(0,254)) : null;  
            return catFinal;
        }
        return null;
    }
    
    //creates a case for sp irrespective of hasSkipCreateCaseIndicator from rawdata-socialhub
    global virtual Case buildParentCaseDefault(SocialPost post, SocialPersona persona) { 
        
        system.debug('<<<<<<<<<<Social Post>>>>>>>>>>>>>>>'+post);
        system.debug('<<<<<<<<<<Social Persona>>>>>>>>>>>>'+persona); 
        
       //LM 11/24/2014 - Added below logic to fix the case threading issue - SMS-642
        Map<String, Object> rawdatamap = new map<String,Object>();
        list<SocialPost> replyTo = [select Id, ExternalPostId, ReplyToId from SocialPost where Id = :post.ReplyToId Limit 1];
        if(replyTo.size() > 0)
          rawdatamap.put('replyToExternalPostId', replyTo[0].ExternalPostId);  
        system.debug('<<<<<<<<<<RawdataMap in buildParentCaseDefault>>>>>>>>>>>>'+rawdatamap);  
        setReplyTo(post, persona, rawdatamap);
          
        Case parentCase = findParentCase(post, persona);
        system.debug('<<<<<<<<<<Parent Case from buildParentCaseDefault Method>>>>>>>>'+parentCase);
        if (caseShouldBeReopened(parentCase))
            reopenCase(parentCase);
        //else if (!hasSkipCreateCaseIndicator(rawData) && (parentCase.id == null || parentCase.isClosed))
        //remove hasSkipCreateCaseIndicator boolean dependency
        else if (parentCase.id == null || parentCase.isClosed)
            parentCase = createCase(post, persona);
            
        system.debug('<<<<<<<<<<Parent Case to return from buildParentCaseDefault Method>>>>>>>>'+parentCase);   
        return parentCase;
    }
    
}