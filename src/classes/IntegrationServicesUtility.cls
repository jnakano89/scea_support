/****************************************************************************************************************************************************************
Created : 09/08/2014 : Leena Mandadapu : Jira# SMS-654 - WebPPP Revamp project

Updates :
          02/18/2015 : Leena Mandadapu  : Jira# SMS-1088 - Corrected Order insert process to send PPP emails with the right price
          05/13/2015 : Leena Mandadapu  : Jira# SMS-1105 - Monthly Payment Option functionality enabled
*****************************************************************************************************************************************************************/
public class IntegrationServicesUtility {
 
 //custom Exceptions
 public class myException extends Exception {}  
 
 //Account record creation process
 public static Account getAccount(WebPPPOrdersService.WebServiceInput webinput){
    return createAccountRecord(webinput.FirstName, webinput.LastName, webinput.Email, webinput.Phonenum);
 }
 
 public static Account createAccountRecord(String Firstname, String Lastname, String email, String phone){
    String personAccountRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
    Account accConsumer = new Account();
    map<String, Account>consumerMap = new map<String, Account>();
    
    system.debug('<<<<<<Webservice Input Email>>>>>>>>>>'+email);
    system.debug('<<<<<<Webservice Input FirstName>>>>>>>>>>'+Firstname);
    system.debug('<<<<<<Webservice Input Lastname>>>>>>>>>>'+Lastname);
    system.debug('<<<<<<Webservice Input phone>>>>>>>>>>'+phone);
    
    if(email <> null && Firstname <> null && Lastname <> null ){
      for(Account acc : [select id, FirstName, LastName, Phone, PersonEmail, Bill_to__pc, PersonContactId from Account 
                         where (PersonEmail=:email AND FirstName=:Firstname AND LastName=:Lastname AND IsPersonAccount = true )]){
           consumerMap.put(email,acc);           
      }
    
    system.debug('<<<<<<consumerMap>>>>>>>>>>'+consumerMap);  
    system.debug('<<<<<<consumerMap Size>>>>>>>>>>'+consumerMap.size());  
    if(consumerMap.size()>0){
      if(!String.isEmpty(email)) {
          accConsumer = consumerMap.get(email);
       }           
    }
   system.Debug('<<<<<Account Consumer>>>>>' +AccConsumer);
   
   if(accConsumer!=null && accConsumer.id<>null){
            return accConsumer;
   }else{
         accConsumer.RecordTypeId = personAccountRecId;
         accConsumer.FirstName = Firstname;
         accConsumer.LastName  = Lastname;
         accConsumer.PersonEmail = email;
         accConsumer.Phone = phone;

         try{insert accConsumer;}
         catch(DMLException e){
            logexception('Unable to insert new consumer', e, 'Purchase');
            return null;
         } 
         system.Debug('<<<<<<Account Consumer>>>>>>' +AccConsumer);
         system.Debug('<<<<<<Account Consumer>>>>>>' +AccConsumer.PersonContactId);
                    
         return accConsumer;
   }
  } else {
         return null;   
  }
 }
 
 //Address record creation process
  public static Address__c getAddress(Id contactId, String Address1,  String Address2,  String City, String State, String Country, String ZipCode, String Address_Type){
        return findAddress(contactId, Address1, Address2, City, State, Country, ZipCode, Address_Type);
  }
    
  public static Address__c findAddress(Id contactId, String Address1,  String Address2,  String City, String State, String Country, String ZipCode, String Address_Type){
      
      system.Debug('<<<<<<ContactID>>>>>' +contactId);
      system.Debug('<<<<<<Address1>>>>>'  +Address1);
      system.Debug('<<<<<<Address2>>>>>'  +Address2);
      system.Debug('<<<<<<City>>>>>'      +City);
      system.Debug('<<<<<<State>>>>>'     +State);
      system.Debug('<<<<<<Country>>>>>'   +Country);
      system.Debug('<<<<<<ZipCode>>>>>'   +ZipCode);
      system.Debug('<<<<<<Address_Type>>>>>' +Address_Type);
              
      List<Address__c> addlist= new List<Address__c>([select Id, Consumer__c, Address_Line_1__c, Postal_Code__c  from Address__c where Address_Line_1__c =: Address1.normalizeSpace() 
                            AND Postal_Code__c =:ZipCode AND City__c =:City AND State__c =:State AND Country__c =:Country AND Consumer__c =:contactId limit 1]);
      system.debug('<<<<<<Address List Size>>>>>' +addlist.size());
      system.debug('<<<<<<Address List>>>>>'      +addlist);                            
      if(addlist.size() > 0) {
        system.debug('<<<<<<Address returned>>>>>' +addlist[0]);
        return addlist[0];
      }
      return CreateAddress(contactId, Address1,  Address2,  City, State, Country, ZipCode, Address_Type);
    }
     
  public static Address__c CreateAddress(Id contactId, String Address1,  String Address2,  String City, String State, String Country, String ZipCode, String Address_Type){
     
     system.Debug('<<<<<<ContactID>>>>>' +contactId);
     system.Debug('<<<<<<Address1>>>>>'  +Address1);
     system.Debug('<<<<<<Address2>>>>>'  +Address2);
     system.Debug('<<<<<<City>>>>>'      +City);
     system.Debug('<<<<<<State>>>>>'     +State);
     system.Debug('<<<<<<Country>>>>>'   +Country);
     system.Debug('<<<<<<ZipCode>>>>>'   +ZipCode);
     system.Debug('<<<<<<Address_Type>>>>>' +Address_Type);
     Address__c addr = null;
     if(!String.isEmpty(Address1)&& !String.isEmpty(City) && !String.isEmpty(State)&& !String.isEmpty(Country)&& !String.isEmpty(ZipCode) && contactId <> null) {
       addr = new Address__c();
       addr.Address_Line_1__c = Address1;
       addr.Address_Line_2__c = Address2;
       addr.City__c = City;
       addr.State__c = State;
       addr.Country__c = Country;
       addr.Postal_Code__c = ZipCode;   
       addr.Consumer__c = contactId;
       addr.Address_Type__c = Address_Type;
       system.Debug('<<<<<<New Address Record>>>>>' +addr);
       try{
          if (addr <> null){
             insert addr;
          }
       }catch(DMLException e){
             logexception('Unable to insert new address', e, 'Purchase Transaction');
             return null;
       }   
       return addr;    
      } 
      else { 
       return null; 
      } 
   }
  
  //Create Asset Process
  public static Asset__c getAsset(WebPPPOrdersService.WebServiceInput webinput, Account AccConsumer){
    return createAsset(webinput.ModelType, webinput.PPPSKU, webinput.ProofOfPurchaseDate, webInput.PPPPurchaseDate, webinput.ContractNumber,
                       webInput.ContractStartDt, webInput.ContractEndDt, webInput.PPPPlanType, webInput.DealerId, webInput.PaymentType, AccConsumer );               
  }
 
  public static Asset__c createAsset(String ModelType, String PPPSku, String PurchaseDate, String PPPPurchaseDate, String ContractNum, String PPPStartDate, 
                                     String PPPEndDate, String PPPPlanType, String DealerId, String PaymentType, Account AccConsumer){
    Asset__c asset = null;
    Product__c PPPproduct;
    map<String, Product__c> PPPProductmap = new map<String, Product__c>(); 
        
    try{
     asset = new Asset__c();
     try{
      for(Product__c p : [Select Id, Sku__c from Product__c where Product_Type__c='ESP' and Status__c = 'Active']) {
       if(p <> null) {  
       PPPProductmap.put(p.Sku__c, p);
       } else {
       throw new myException('Unable to find Active PPP Product');  
       }    
      }   
     }catch(Exception e){
       logexception('Not able to find Active PPP Product', e, 'Purchase Transaction');  
       return null;
     }
     
     system.debug('<<<<<<PPP Product Map>>>>>>'+PPPProductmap);
     system.debug('<<<<<<Webservice PPP SKU input>>>>>>'+PPPSku);
     system.debug('<<<<<<Webservice Dealer Id input>>>>>>'+DealerId);
     
     try{
      for(Assurant_Matrix__c[] am : [select SFDC_SKU__c, Assurant_SKU__c, Channel__c, Dealer_ID__c  from Assurant_Matrix__c where Assurant_SKU__c = : PPPSku and Dealer_ID__c = : DealerId]) {
       system.debug('<<<<<<Assurant Matrix List>>>>>>'+am); 
       system.debug('<<<<<<Assurant Matrix List Size>>>>>>'+am.size());
       if(am <> null && am.size() == 1) {
          asset.PPP_Product__c = (PPPProductmap.get(am[0].SFDC_SKU__c)).Id;
       } else { 
       throw new myException('Unable to find Assurant SKU mapping for this PPP Product');       
       }
      }
     }catch(Exception e){
       logexception('Not able to find Assurant SKU mapping', e, 'Purchase Transaction');     
       return null;
     }
     
     asset.Purchase_Date__c = converttoDate(PurchaseDate);
     asset.PPP_Purchase_Date__c = converttoDate(PPPPurchaseDate);
     asset.Coverage_Type__c = PPPPlanType == 'AD' ? 'AD' : 'ESP';
     asset.PPP_Start_Date__c = converttoDate(PPPStartDate);
     asset.PPP_End_Date__c = converttoDate(PPPEndDate);
     asset.PPP_Contract_Number__c = ContractNum;
     asset.PPP_Last_Update_Date__c = converttoDate(PPPPurchaseDate);
     asset.PPP_Status__c = 'Active';
     //LM: Uncommented for Monthly Payment option
     asset.PPP_Refund_Eligible__c = PaymentType.equalsignorecase('Monthly') ? false : true;
     asset.PPP_Refund_Eligible__c = true;
     asset.PPP_Record_Lock__c = true;
     asset.Model_Number__c = ModelType == 'PS4' ? 'PS4NA' : ModelType == 'PS3' ? 'PS3NA' : ModelType == 'VITA' ? 'VITANA' : 'NA'; 
     asset.Asset_Status__c = 'Active';
     
     try { insert asset;}
     catch(DMLException e) {
      logexception('Not able to insert Asset record', e, 'Purchase Transaction'); 
      return null;
     }
     
    } catch(Exception e) {
        logexception('Asset creation process failed', e, 'Purchase Transaction'); 
        return null;
    }
    
     //Insert Consumer Asset junction record
     Consumer_Asset__c conasst = new Consumer_Asset__c();
     conasst.Asset__c = asset.id;
     conasst.Consumer__c = AccConsumer.PersoncontactId;
     system.debug('<<<<<<Consumer Asset Record>>>>>>'+conasst);
     try{insert conasst;}
     catch(Dmlexception e){
      logexception('Asset-Consumer insert process failed', e, 'Purchase Transaction'); 
      return null;
     }    
     return asset;
  } 
  
  //Create Order process
  public static Order__c getOrder(WebPPPOrdersService.WebServiceInput input, Asset__c asset, String PPPOrderSource, Address__c shipToAddress, Address__c BillToAddress, Account consumer){
     return CreateOrder(input.PPPPurchaseDate, asset, PPPOrderSource, input.OrderNumber, input.PaymentType, 
                        input.BaseAmount, input.TaxAmount, shipToAddress, BillToAddress, consumer);
    //return null;                   
  }
  
  public static Order__c CreateOrder(String PPPSaleDate, Asset__c asset, String PPPOrderSource, String OrderNumber, String PaymentType, string BaseAmount, 
                                     string TaxAmount, Address__c shipToAddress, Address__c BillToAddress, Account consumer){
     
     Boolean isOrderalreadyexists = false;
     system.debug('<<<<<<PYB Order Number>>>>>>'+OrderNumber);
     isOrderalreadyexists = checkOrderExists(OrderNumber);
     system.debug('<<<<<<Is order already exists>>>>>>>>'+isOrderalreadyexists);
     if(isOrderalreadyexists){ 
        return null;
     }
     else {
     Order__c order = new Order__c();
     order.Order_Date__c = converttoDate(PPPSaleDate);
     order.External_ID__c = OrderNumber;
     //LM: Uncommented below for Monthly Payment option
     system.debug('<<<<<<Payment Type>>>>>>>>'+PaymentType);
     order.Billing_Type__c = PaymentType == 'SinglePay' ? 'Single' : PaymentType == 'Monthly' ? 'Monthly' : null;
     //order.Billing_Type__c = 'Single';
     //LM 02/18/2015: set status to Open to stop the PPP outbound emails. Send PPP emails after Order line is created so Order total will be updated with the right amount.
     order.Order_Status__c = 'Open';//'Assurant Enrolled';
     order.ESP_Record_Type__c = 'A'; //Added for Exchange process functionality

     if(asset!=null){
       order.Asset__c = asset.id;
     }
     if(shipToAddress!=null){
       order.Ship_To__c = shipToAddress.id;
     }
     if(BillToAddress!=null){
       order.Bill_To__c = BillToAddress.id;
     }
     if(consumer!=null){
       order.Consumer__c = consumer.personcontactid;
     }
     if (!String.isEmpty(PPPOrderSource)){
         if(PPPOrderSource=='PS.COM'){
            order.Order_Origin_Source__c='PYB';
         }
     }
     try {insert order; }
     catch(DMLException e) {
        logexception('Order insert process failed', e, 'Purchase Transaction');
        return null;
     }
     system.debug('<<<<<Order Id>>>>>'+order.id+ '<<<<<<External Order Number>>>>> '+order.External_ID__c);
     
     //create OrderLine for order
     Order_Line__c ordline = new Order_Line__c();
     ordline.Line_Number__c = '1';
     ordline.Product__c = asset.PPP_Product__c;
     ordline.Product_Name__c = asset.PPP_Product__r.Description__c;
     ordline.SKU__c = asset.PPP_Product_SKU__c;
     ordline.Quantity__c = '1';
     ordline.List_Price__c = Decimal.valueOf(BaseAmount);
     ordline.Tax__c = Decimal.valueOf(TaxAmount);
     ordline.Order__c = order.Id;
     ordline.Total__c = Decimal.valueOf(BaseAmount) + Decimal.valueOf(TaxAmount);
     
     try{insert ordline;} 
     catch(DMLException e){
      logexception('OrderLine insert process failed', e, 'Purchase Transaction');
      return null;
     }
     //LM 02/18/2015: Set Order status to "Assurant Enrolled" to send the PPP emails
     order.Order_Status__c = 'Assurant Enrolled';
     try{Update order;}
     catch(DMLException e) {
        logexception('Order update process failed', e, 'Purchase Transaction');
        return null;
     }
     return order;
    } 
  }  
  
  public static boolean checkOrderExists(String externalordernum) {
     system.debug('<<<<<<PYB Order Number>>>>>>'+externalordernum);
     List<Order__c> ord = new List<Order__c>([select Id from Order__c where External_ID__c = :externalordernum]);
     system.debug('<<<<<<<Order Exists List>>>>>'+ord);
     system.debug('<<<<<<<Order List IsEmpty?>>>>>'+ord.isEmpty());
     if(ord.isEmpty()){
        return false;
     } else{
        return true;
     }
  }
  
  public static date converttoDate(string dt){
     String[] newDateString = dt.split('/');
     if(newDateString != null){
       Integer Day = integer.valueOf(newDateString[1]);
       Integer Month = integer.valueOf(newDateString[0]);
       Integer Year = integer.valueOf(newDateString[2]);
       Date newDate = Date.newInstance(Year, Month, Day);
       return newDate;
     }
     return null;
  }
  
  public static void StagePYBCancellation(Id orderId, Boolean PPPClaimed, String status, String message) {
    Staging_PYB_Cancellation__c stgPYB = new Staging_PYB_Cancellation__c();
    stgPYB.Order__c = orderId;
    stgPYB.PYB_PPP_Claimed__c = PPPClaimed;
    stgPYB.Process_Status__c = status;
    stgPYB.Error_Message__c = message;
    stgPYB.Processed_Date__c = DateTime.now();
    try{
     insert stgPYB;
    } catch(DMLException dml) {
     logexception('Unable to Stage PYB Cancellation', dml, 'Cancellation');
    }       
  }
  
 public static boolean UpdatePYBOrderProcess(WebPPPOrdersService.WebServiceInput req) {
    List<Order__c> orders = [Select Id,Order_Origin_Source__c, Asset__r.PPP_Status__c,Order_Total__c,Order_Type__c,Payment_Status__c,Asset__r.Asset_Status__c,
                             Refund_Amount__c, Asset__r.PPP_Purchase_Date__c,Order_Date__c,Case__r.RecordTypeId,Case__c, Asset__r.PPP_Refund_Eligible__c,
                             Asset__r.PPP_Start_Date__c, Asset__r.PPP_End_Date__c,Consumer__c, PPP_Contract_Number__c, External_Order_Number__c, Asset__c
                             From Order__c Where External_Order_Number__c = :req.OrderNumber ];
                             
    system.debug('<<<<<Request>>>>>>'+req);
    system.debug('<<<<<Orders List>>>>>'+orders); 
    system.debug('<<<<<Orders List Size>>>>>'+orders.size());                         
        if(orders.size()== 1){
          Order__c ord = new Order__c(Id=orders[0].Id); 
          system.debug('<<<<<Order to update>>>>>'+ord); 
          Asset__c ast = new Asset__c(Id=orders[0].Asset__c);
          system.debug('<<<<<Asset to update>>>>>'+ast);    
          List <Account> acct = new list<Account>([select Id from Account where PersonContactId = :orders[0].Consumer__c]);
          system.debug('<<<<<Account List>>>>>'+acct);
          Address__c Billtoaddr = new Address__c();
          Address__c Shiptoaddr = new Address__c();
          boolean isBilltoaddrExists = false;
          boolean isShiptoaddrExists = false;
 
          //Order updates
          //LM: Uncommented below for Monthly Payment option
          if(ord.Billing_Type__c == 'Monthly') {
          ord.Order_Status__c = req.BillingStatus == 'Suspended'?'Assurant Suspended':
                                req.BillingStatus == 'Cancelled'?'Assurant Cancelled':
                                req.BillingStatus == 'Active'?'Assurant Enrolled':
                                ord.Order_Status__c;
          }                     
          
          //Asset updates
          if(req.SerialNumber != null && req.SerialNumber != ''){
            ast.Serial_Number__c = req.SerialNumber;
          } 
          if(req.ModelNumber != null && req.ModelNumber != '') {
            ast.Model_Number__c = req.ModelNumber;
          }
          if(req.POPattachmentURL != null && req.POPattachmentURL != ''){
            ast.PPP_POP_attachment__c = req.POPattachmentURL;
          }
          //LM: Uncommented below for Monthly Payment option
          if(req.BillingStatus != null && req.BillingStatus != '' && ord.Billing_Type__c == 'Monthly'){
            if(req.BillingStatus == 'Suspended') {
             ast.PPP_Status__c = req.BillingStatus;
             ast.PPP_Suspended_Date__c = Date.today();
            } else if(req.BillingStatus == 'Cancelled'){
              ast.PPP_Status__c = req.BillingStatus;
              ast.ESP_Cancellation_Date__c = Date.today();  
            } else if(req.BillingStatus == 'Active'){
              ast.PPP_Status__c = req.BillingStatus;
              ast.PPP_Suspended_Date__c = null; //reset Suspended date when the plan becomes active
            }
          } 
          
          //Consumer updates 
          if(req.Phonenum != null && req.Phonenum != '') {
            acct[0].Phone = req.Phonenum;
          }
          
          //Address inserts
          if(!String.isEmpty(req.BillingAddress1) && !String.isEmpty(req.BillingCity) && !String.isEmpty(req.BillingState) && !String.isEmpty(req.BillingCountry) && !String.isEmpty(req.BillingZipCode)){
           isBilltoaddrExists = true;   
           Billtoaddr = CreateAddress(orders[0].Consumer__c,req.BillingAddress1,req.BillingAddress2,req.BillingCity, req.BillingState, req.BillingCountry, req.BillingZipCode, 'Billing');
          }
          if(!String.isEmpty(req.ShippingAddress1) && !String.isEmpty(req.ShippingCity) && !String.isEmpty(req.ShippingState) && !String.isEmpty(req.ShippingCountry) && !String.isEmpty(req.ShippingZipCode)){
           isShiptoaddrExists = true;   
           Shiptoaddr = CreateAddress(orders[0].Consumer__c,req.ShippingAddress1,req.ShippingAddress2,req.ShippingCity, req.ShippingState, req.ShippingCountry, req.ShippingZipCode, 'Shipping');
          }
          
          //commit changes to database
          if(ord != null) {
          try { update ord;}
            catch(DMLException dml) {
             logexception('Unable to update Order'+req, dml, 'Update'); 
             return false;  
            }
          }
            
          if(ast != null) {
            try { update ast; }
            catch(DMLException dml) {
             logexception('Unable to update Asset'+req, dml, 'Update'); 
             return false;  
            }
          }
          
          if(acct != null) {    
            try { update acct;}
            catch(DMLException dml) {
             logexception('Unable to update Phone Number'+req, dml, 'Update'); 
             return false;  
            }
          }
          
          if(Billtoaddr == null && isBilltoaddrExists){
             try {
                throw new myException('Unable to insert Bill to Address for Order Number:' + req.OrderNumber); 
             } catch (Exception e) {    
             logexception('Unable to insert new Bill To address'+req, e, 'Update'); 
             return false;  
             }
            }
                    
          if(Shiptoaddr == null && isShiptoaddrExists){
             try {
                throw new myException('Unable to insert Ship to Address for Order Number:' + req.OrderNumber); 
             } catch (Exception e) {    
             logexception('Unable to insert new Ship To address'+req, e, 'Update'); 
             return false;  
             }
            }       
          return true;
        }
        return false;
  } 
  
  public static void logexception(String errmsg, Exception exlog, String transactiontype){
        Integration_Alert__c integrationalerts = new Integration_Alert__c();
        integrationalerts.Integration_Name__c = 'WebPPP';
        integrationalerts.Message__c = transactiontype + '<<<<>>>>' + errmsg;
        integrationalerts.Stack_Trace__c += exlog.getStackTraceString() + '\n';
        integrationalerts.Stack_Trace__c += String.valueOf(exlog.getLineNumber())+'\n';
        integrationalerts.Stack_Trace__c += exlog.getTypeName()+'\n';
        integrationalerts.Stack_Trace__c += exlog.getMessage();
        insert integrationalerts;
  }
}