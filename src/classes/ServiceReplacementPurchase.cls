/****************************************************************************************************************************************************************************************
Created 
        :   Preetu Vashista : 02/25/2015 :Service Replacement Purchase
*****************************************************************************************************************************************************************************************/
public class ServiceReplacementPurchase{
    public Case selectedCase{get;set;}
    public static list <Asset__c> assVal {get;set;} 
    public String value{get;set;}
    public Asset__c newAsset{get;set;}
    public Id caseId{get;set;}
    public Id contactId{get;set;}
    public Id serviceCaseId{get;set;}
    public Id  newAssetId{get;set;}
    public case cc{get;set;}
    public case insertedServiceCase{get;set;}
    public static final String RT_HARDWARE_NETWORKING_ID = GeneralUtiltyClass.RT_HARDWARE_NETWORKING_ID; 
    public static final String RT_SERVICE_ID = GeneralUtiltyClass.RT_SERVICE_ID;
   
   //Consutructor to get case id
    public ServiceReplacementPurchase(ApexPages.StandardController stdController)
    {
         //Added single line of code to get rid of ie issue
         Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');
         cc = (Case)stdController.getRecord();
         Case cValue = [select Id,Account.Id,recordTypeId,status,Contact.Name from Case where case.id = :cc.Id];
         system.debug ('contact id is' + cValue.Contact.ID);
         selectedCase = cValue;
         system.debug('selectedCase value' + selectedCase);
         caseId = Apexpages.currentPage().getParameters().get('ID');
         
         System.debug('case id value is' + caseId);
          //assVal = getReplacedProductDetail(caseId);  
    }
     public void getReplacedProductDetail(String caseId) {
     //Clone service case and insert the case
     contactId = selectedCase.contactId;
     System.debug('calling getReplacedProductDetail');
     list<Case> serviceCaseToInsert = new list<Case>();
     Id hardwareNetworkingCaseRecordTypeId   = RT_HARDWARE_NETWORKING_ID;
     if(selectedCase.recordTypeId == hardwareNetworkingCaseRecordTypeId &&selectedCase.status == 'service') { 
         Case serviceCase = cc.clone();
                serviceCase.RecordTypeId = RT_SERVICE_ID;
                serviceCase.Parent_Case__c = caseId;
                serviceCase.ASB_Type__c ='standard';
                serviceCase.Type = null;
                serviceCase.Status = 'Open';
                serviceCase.contactId = contactId;
                serviceCase.Survey__c = null;
                serviceCaseToInsert.add(serviceCase);
                
     }
      if(!serviceCaseToInsert.isEmpty()){
        System.debug('creating child case');
           Database.SaveResult[] srList = Database.insert(serviceCaseToInsert, false);
           system.debug('srList value is' +  srList);
           for(Integer i=0;i<srList.size();i++){

            if (srList.get(i).isSuccess()){
                system.debug('value of case recordis' + srList.get(i).getId());
                serviceCaseId = srList.get(i).getId();
            }
        }
     
    list<Asset__c> aList = new list<Asset__c>();
    Set<Id> assetIds = new Set<Id>();
    for(case ca : [select Asset__c from case where case.Id = :caseId]) {
       selectedCase = ca;
       system.debug('selectedCase value is----->' + selectedCase);
       assetIds.add(ca.Asset__c);
    
     }
     //To find existing Asset has replaceable product 
     Asset__c aValue = [select Name,product__r.Name,product__r.Replacement_Model__c from Asset__c where Asset__c.ID = :assetIds AND Asset__c.product__r.Replacement_Model__c != null LIMIT 1];
     system.debug('aValue is--------------->' + aValue.product__r.Replacement_Model__c);
  
     for(Product__c proVal : [select Id,Name,Genre__c,Replacement_Model__c,Description__c,Product_Type__c,Sub_Type__c,SKU__c,  
                        Tax_Code__c,Status__c,SAP_Material_Number__c,Target_Country__c,List_Price__c,Price_Type__c,Region__c from Product__c
                        where Name = :aValue.product__r.Replacement_Model__c]) {
        
                    
         newAsset = new Asset__c();
         newAsset.Product__C = proval.Id;
         newAsset.Model_Number__c = proVal.SKU__c;
         newAsset.Serial_Number__c = 'RPL-'+ aValue.Name;
         aList.add(newAsset);
            
     }
     system.debug('aList size is------------------>' + aList); 
     if(!aList.isEmpty()){
           Database.SaveResult[] assetList = Database.insert(aList, false);
           system.debug('srList value is' +  assetList);
           for(Integer i=0;i<assetList.size();i++){

            if (assetList.get(i).isSuccess()){
                newAssetId = assetList.get(i).getId();
                system.debug('value of asset recordis' + assetList.get(i).getId());
            }
            
            
       }     
       Consumer_Asset__c conAsset = new Consumer_Asset__c(consumer__c = contactId , Asset__c = newAssetId);
                insert conAsset;
                system.debug('conAsset id value is' + conAsset.Id);
       
       
       system.debug('ServcieCaseId value is:' + serviceCaseId);
       Asset__c insetedNewAsset = [select Name,product__r.Name,product__r.Replacement_Model__c,product__r.Genre__c from Asset__c where Asset__c.ID = :newAssetId];
       insertedServiceCase = [select Id,Account.Id,recordTypeId,status,Contact.Name from Case where case.id = :serviceCaseId];
       System.debug('insertedServiceCase value is---->' + insertedServiceCase);
    
       insertedServiceCase.Asset__c = newAssetId;
       insertedServiceCase.Fee_Type__c = 'Out of Warranty';
       insertedServiceCase.Product__c = insetedNewAsset.product__r.Genre__c;
       insertedServiceCase.sub_Area__c = 'Service Replacement';
       insertedServiceCase.ASB_Type__c = 'No Box Required';
       update insertedServiceCase;
        
         
        
    }
    } 
    
    }  
    public pagereference Saverec(){
    assVal = new list<Asset__c>();
     PageReference prf = null;
     if(value =='Yes'){
        System.debug('case id value is' + caseId);
        system.debug('Value is-------------->' + value);
        getReplacedProductDetail(caseId);
        selectedCase.Confirmation__c = False;
        update selectedCase; 
        system.debug('Hello123--------------->'+selectedCase.Confirmation__c);  
        //system.debug('assVal value is' + assVal);
      }
    else if(value =='No'){
          system.debug('selectedCase value is----->' + selectedCase);
          selectedCase.status = 'Decline Service';
          selectedCase.Confirmation__c = False;
          update selectedCase;           
         //prf = new PageReference('/'+ insertedServiceCase.Id);
          //prf.setRedirect(true); 
    }
     
        prf = new PageReference('/'+ caseId);
        //prf.setRedirect(true);
     return prf;
    } 
         
    }