global class ContactCounterResetScheduler implements Schedulable{
    /*public static String sched = '0 00 02 * * ?';  //Every Day at Midnight 
    global static String scheduleMe() {
        ContactCounterResetScheduler SC = new ContactCounterResetScheduler(); 
        return System.schedule('My batch Job', sched, SC);
    }*/

    global void execute(SchedulableContext sc) {

        ContactCounterResetBatchable b1 = new ContactCounterResetBatchable();
        ID batchprocessid = Database.executeBatch(b1,200);           
    }

}