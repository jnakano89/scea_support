public class DocumentMappingUtilities {

	private static String sampleXML = 
			'<ShowSalesOrder revision=\"8.0\" xmlns=\"http://www.maskedurl.com\">' +
			'<DataArea>' + 
			'<Show confirm=\"Never\" xmlns:NS1=\"http://www.maskedurl.com\" xmlns:NS2=\"http://www.maskedurl.com\" xmlns:MyNamespace=\"http://www.MyNamespace.com\" xmlns:NS3=\"http://www.maskedurl.com\" xmlns:NS4=\"http://www.maskedurl.com\" xmlns:NS5=\"http://www.maskedurl.com\" xmlns:NS6=\"http://www.maskedurl.com\" />' + 
			'<SalesOrder xmlns:NS1=\"http://www.maskedurl.com\" xmlns:NS2=\"http://www.maskedurl.com\" xmlns:MyNamespace=\"http://www.MyNamespace.com\" xmlns:NS3=\"http://www.maskedurl.com\" xmlns:NS4=\"http://www.maskedurl.com\" xmlns:NS5=\"http://www.maskedurl.com\" xmlns:NS6=\"http://www.maskedurl.com\">' + 
			'<Header>' + 
			'<RecordIds>' + 
			'<RecordId>' +
			'<Id>Test-12345</Id>' +
			'</RecordId>' + 
			'</RecordIds>' +
			'<TotalAmount currency=\"USD\">95.75</TotalAmount>' + 
			'<PaymentTerms>' + 
			'<UserArea>' + 
			'<MyNamespace:PaymentMethod>' +  
			'<MyNamespace:MethodId>Credit Card</MyNamespace:MethodId>' + 
			'<MyNamespace:CCInfo>' + 
			'<MyNamespace:AccountType>Visa</MyNamespace:AccountType>' + 
			'<MyNamespace:AccountName>Deadbeat Lebowski</MyNamespace:AccountName>' +  
			'<MyNamespace:ExpMonth>08</MyNamespace:ExpMonth>' + 
			'<MyNamespace:ExpYear>2013</MyNamespace:ExpYear>' + 
			'</MyNamespace:CCInfo>' +
			'</MyNamespace:PaymentMethod>' + 
			'</UserArea>' + 
			'</PaymentTerms>' +
			'</Header>' + 
			'<Line>' + 
			'<LineNumber>1</LineNumber>' + 
			'<OrderItem>' + 
			'<Description>Sham-WOW</Description>' + 
			'</OrderItem>' + 
			'<OrderQuantity uom=\"Each\">1</OrderQuantity>' + 
			'<UnitPrice>' + 
			'<Amount currency=\"USD\">49.95</Amount>' + 
			'<PerQuantity uom=\"Each\">1</PerQuantity>' + 
			'</UnitPrice>' + 
			'<Priority>Standard</Priority>' + 
			'<Tax>' + 
			'<TaxAmount currency=\"USD\">3.25</TaxAmount>' + 
			'<PercentQuantity uom=\"Each\">6.5</PercentQuantity>' +
			'</Tax>' + 
			'<ShipDate>2013-08-02T00:00:00-08:00</ShipDate>' +
			'<Part>' +
			'<Name>Sham</Name>' +
			'<PartNumber>43567</PartNumber>' +
			'</Part>' +
			'<Part>' +
			'<Name>WOW</Name>' +
			'<PartNumber>76857</PartNumber>' +
			'</Part>' +
			'</Line>' + 
			'<Address>' +
			'<Type>Shipping</Type>' +
			'<AddressLine>9 Glebe Road</AddressLine>' +
			'<City>Keswick</City>' +
			'<County>Cumbria</County>' +
			'<Country>United Kingdom</Country>' +
			'<PostCode>KW13 2EG</PostCode>' +
			'</Address>' +
			'<Line>' + 
			'<LineNumber>2</LineNumber>' + 
			'<OrderItem>' +
			'<Description>Slap Chop</Description>' + 
			'</OrderItem>' + 
			'<OrderQuantity uom=\"Each\">1</OrderQuantity>' + 
			'<UnitPrice>' + 
			'<Amount currency=\"USD\">39.95</Amount>' + 
			'<PerQuantity uom=\"Each\">1</PerQuantity>' + 
			'</UnitPrice>' + 
			'<Priority>Standard</Priority>' + 
			'<Tax>' + 
			'<TaxAmount currency=\"USD\">2.6</TaxAmount>' + 
			'<PercentQuantity uom=\"Each\">6.5</PercentQuantity>' + 
			'</Tax>' + 
			'<ShipDate>2009-10-10T00:00:00-08:00</ShipDate>' +
			'<Part>' +
			'<Name>Slap</Name>' +
			'<PartNumber>34211</PartNumber>' +
			'</Part>' +
			'<Part>' +
			'<Name>Chop</Name>' +
			'<PartNumber>94567</PartNumber>' +
			'</Part>' +
			'</Line>' + 
			'<Address>' +
			'<Type>Invoice</Type>' +
			'<AddressLine>Flat 1</AddressLine>' +
			'<AddressLine>10 Glebe Road</AddressLine>' +
			'<City>Keswick</City>' +
			'<County>Cumbria</County>' +
			'<Country>United Kingdom</Country>' +
			'<PostCode>KW13 2EG</PostCode>' +
			'</Address>' +
			'</SalesOrder>' + 
			'</DataArea>' + 
			'</ShowSalesOrder>';

	public static Dom.Document getSampleDocument() {
		//Load Response into a Document object
		Dom.Document document = new Dom.Document();
		document.load(sampleXML);
		return document;
	}

	/*
    Coverts an XML Document to an object represented as a JSON string by applying the given
    Object Map 
	 */
	public static String executeObjectMap(Dom.Document sourceDocument, String objectMapName) {

		//Ensure input arguments have been initialised
		System.assert(sourceDocument != null);
		System.assert(objectMapName != null);

		//Initialise JSON String
		String jsonStr = '';
		try {
			//Load Object Map Transformations
			Map<String, sObject> transformations = getTransformations(objectMapName);
			System.debug(' json ::'+JSON.serialize(transformations));

			system.debug('**transformation instanceof Object_Map__c::@@'+transformations);
			//Transform Document
			Dom.Document targetDocument = new Dom.Document();
			transform(transformations, sourceDocument.getRootElement().getName(), sourceDocument.getRootElement(), targetDocument.createRootElement('Result', '', ''), true);
			System.debug('transform :: '+targetDocument.toXmlString());

			//Convert targetDocument to JSON string
			for (Dom.XmlNode node : targetDocument.getRootElement().getChildElements()) {
				System.debug('json !! ::'+node.getText()+ ' > ' +node.getName());
				jsonStr += xmlNodeToJson(node);
				System.debug('final json 1 ::'+jsonStr+' >> '+node);
			}
			System.debug('final json ::'+jsonStr);
		}
		catch (DocumentMappingUtilitiesException e) {
			throw e;
		}
		catch (Exception e) {
			throw new DocumentMappingUtilitiesException('DocumentMappingUtilities.documentToObject: ' +
					e.getMessage());
		}

		if (jsonStr.startsWith('{{},'))
		{
			jsonStr = jsonStr.replaceFirst('\\{\\{\\},','{');
		}

		return jsonStr;
	}

	/*
    Returns a complete set of transformations for the given Object Map
	 */
	private static Map<String, sObject> getTransformations(String objectMapName) {

		//Ensure input arguments have been initialised
		System.assert(objectMapName != null);

		//Initialise transformations Map 
		Map<String, sObject> transformations = new Map<String, sObject>();

		try {
			//Get the Object Map
			List<Object_Map__c> objectMaps = getObjectMaps(objectMapName);

			//Ensure only one Object Map is returned
			if (objectMaps.size() == 0) {
				throw new DocumentMappingUtilitiesException('ObjectMap.getTransformations: ' +
						'Unable to locate Object Map with \"Name\":\"' + objectMapName +'\"');
			}
			else if (objectMaps.size() > 1) {
				throw new DocumentMappingUtilitiesException('ObjectMap.getTransformations: ' +
						'Multiple Object Maps identified with \"Name\":\"' + objectMapName +'\"');
			}

			//Get reference to first Object Map
			Object_Map__c objectMap = objectMaps.get(0);

			//Put objectMap into the transformations Map
			transformations.put(objectMap.Source__c, objectMap);

			//Put each objectAttributeMap into the transformationsMap
			if (objectMap.Object_Attribute_Maps__r != null)
				for (Object_Attribute_Map__c objectAttributeMap : objectMap.Object_Attribute_Maps__r) {
					transformations.put(objectAttributeMap.Source__c, objectAttributeMap);
				}

			//Put each childObjectMap into the transformationsMap
			if (objectMap.Child_Object_Maps__r != null) {
				addChildTransformations(transformations, getListOfIds(objectMap.Child_Object_Maps__r));
			}
		}
		catch (DocumentMappingUtilitiesException e) {
			throw e;
		}
		catch (Exception e) {
			throw new DocumentMappingUtilitiesException('ObjectMap.getTransformations: ' +
					e.getMessage());
		}

		return transformations;
	}

	/*
    Adds a list of childObjectMaps, denoted by their IDs, to the transformations Map
	 */  
	private static void addChildTransformations(Map<String, sObject> transformations, List<Id> childObjectMapIds) {

		//Ensure input arguments have been initialised
		System.assert(transformations != null);
		System.assert(childObjectMapIds != null);

		try {
			//Get the Object Maps
			List<Object_Map__c> objectMaps = getObjectMaps(childObjectMapIds);

			//Put each objectMap into documentToObjectMap
			for (Object_Map__c objectMap : objectMaps) {

				//Put objectMap into transformations Map
				transformations.put(objectMap.Source__c, objectMap);

				//Put each objectAttributeMap into transformations Map
				if (objectMap.Object_Attribute_Maps__r != null)
					for (Object_Attribute_Map__c objectAttributeMap : objectMap.Object_Attribute_Maps__r) {
						transformations.put(objectAttributeMap.Source__c, objectAttributeMap);
					}

				//Put each childObjectMap into transformations Map
				if (objectMap.Child_Object_Maps__r != null) {
					addChildTransformations(transformations, getListOfIds(objectMap.Child_Object_Maps__r));
				}
			}
		}
		catch (Exception e) {
			throw new DocumentMappingUtilitiesException('ObjectMap.addChildTransformations: ' +
					e.getMessage());
		}

	}

	/*
    Returns a list of Id when given a list of sObject
	 */
	private static List<Id> getListOfIds(List<sObject> objects) {

		//Ensure input arguments have been initialised
		System.assert(objects != null);

		//Initialise the List of IDs
		List<Id> ids = new List<Id>();

		//Add the ID of each object to the List
		for (sObject obj : objects) {
			ids.add(obj.Id);
		}

		return ids;
	}

	/*
    Retrieves Object Maps by performing a search on the Name
    Provided
	 */
	private static List<Object_Map__c> getObjectMaps(String name) {

		//Ensure input arguments have been initialised
		System.assert(name != null);

		//Initialise the List of Object Maps
		List<Object_Map__c> objectMaps = null;

		try {
			//Get Object Maps
			objectMaps = 
					[SELECT 
					 Id, Name, Source__c, Target__c, Create_Attributes_Map__c, Target_Collection_Type__c, Object_Map_Key__r.Target__c,
					 (SELECT Id, Source__c, Source_Cardinality__c, Target__c, Target_Data_Type__c FROM Object_Attribute_Maps__r),
					 (SELECT Id FROM Child_Object_Maps__r) 
					 FROM 
					 Object_Map__c
					 WHERE 
					 Name = :name];
		}
		catch (Exception e) {
			throw new DocumentMappingUtilitiesException('ObjectMap.getObjectMaps: ' +
					e.getMessage());
		}

		return objectMaps;
	}

	/*
    Retrieves Object Maps by performing a search on the list of IDs
    Provided
	 */
	private static List<Object_Map__c> getObjectMaps(List<Id> objectMapIds) {

		//Ensure input arguments have been initialised
		System.assert(objectMapIds != null);

		//Initialise the List of Object Maps
		List<Object_Map__c> objectMaps = null;

		try {
			//Get Object Maps
			objectMaps = 
					[SELECT 
					 Id, Name, Source__c, Target__c, Create_Attributes_Map__c, Target_Collection_Type__c, Object_Map_Key__r.Target__c,
					 (SELECT Id, Source__c, Source_Cardinality__c, Target__c, Target_Data_Type__c FROM Object_Attribute_Maps__r),
					 (SELECT Id FROM Child_Object_Maps__r) 
					 FROM 
					 Object_Map__c
					 WHERE 
					 Id IN :objectMapIds];
		}
		catch (Exception e) {
			throw new DocumentMappingUtilitiesException('ObjectMap.getObjectMaps: ' +
					e.getMessage());
		}

		return objectMaps;
	}

	/*
    Looks at a Source string and determines if there is a reference to an attribute 
    at the end
	 */
	private static Boolean isAttributeMapping(String source) {

		//Ensure input arguments have been initialised
		System.assert(source != null);

		//Initilise isAttributeMapping
		Boolean isAttributeMapping = false;

		//If '@' symbol is found in source string then isAttributeMapping = true
		if (source.indexOf('@') >= 0) {
			isAttributeMapping = true;
		}

		return isAttributeMapping;
	}

	/*
    Looks at a Source string and returns the reference to an attribute if there is one
	 */
	private static String getAttributeMapping(String source) {

		//Ensure input arguments have been initialised
		System.assert(source != null);

		//Initilise attribute
		String attribute = '';

		//Check whether the Source contains an Attribute Mapping
		if (IsAttributeMapping(source)) {
			attribute = source.Right(source.Length() - (source.indexOf('@') + 1));
		}

		return attribute;
	}

	/*
    Recursively maps the extDocNode hierarchy into a intDocNode hierarchy representation of 
    an Apex Class using the documentToObjectMap provided
	 */
	private static void transform(Map<String, sObject> transformationsMap, String sourceDocPath, 
			Dom.Xmlnode sourceNode, Dom.Xmlnode targetNode, Boolean recursiveSearch)
	{
		//Ensure input arguments have been initialised
		System.assert(transformationsMap != null);
		System.assert(sourceDocPath != null);
		System.assert(sourceNode != null);
		System.assert(targetNode != null);
		System.assert(recursiveSearch != null);

		//Initialise newTargetNode to the existing targetNode
		Dom.Xmlnode newTargetNode = targetNode;

		//Search the transformationsMap to determine if the sourceDocPath is present
		sObject transformation = transformationsMap.get(sourceDocPath);
		system.debug('**transformation instanceof Object_Map__c::'+(transformation instanceof Object_Map__c)+'>'+sourceDocPath+'!!'+newTargetNode.getName()+'@@'+transformation);
		if (transformation != null)
		{
			//If the transformation is an Object_Map__c then add a child element to targetNode
			
			if (transformation instanceof Object_Map__c) {
				//Add the child element and return a reference to it
				newTargetNode = targetNode.addChildElement(((Object_Map__c)transformation).Target__c, '', '');
				system.debug('**transformation instanceof Object_Map__c::!!'+((Object_Map__c)transformation).Target__c+'@@'+newTargetNode.getName());
				//If the Object_Map__c has a collection type specified then add it as an attribute
				String targetCollectionType = ((Object_Map__c)transformation).Target_Collection_Type__c;
				if (targetCollectionType == 'Map') {
					newTargetNode.setAttribute('collectionType', targetCollectionType);
					newTargetNode.setAttribute('key', ((Object_Map__c)transformation).Object_Map_Key__r.Target__c);
				}
				else if (targetCollectionType == 'List' || targetCollectionType == 'Set') {
					newTargetNode.setAttribute('collectionType', targetCollectionType);
				}

				//Set a flag indicating whether attributes will ultimately be mapped to the 
				//attributes map collection or directly to attributes on the class
				if (((Object_Map__c)transformation).Create_Attributes_Map__c) {
					newTargetNode.setAttribute('createAttributesMap', 'true');
				}
				else {
					newTargetNode.setAttribute('createAttributesMap', 'false');
				}
			}
			//Else if the transformation is an Object_Attribute_Map__c 
			else if (transformation instanceof Object_Attribute_Map__c) {
				Dom.Xmlnode node;
				//Add attribute as element to the targetNode
				if (isAttributeMapping(((Object_Attribute_Map__c)transformation).Source__c)) {
					node = targetNode.addChildElement(((Object_Attribute_Map__c)transformation).Target__c, '', '');
					node.addTextNode(sourceNode.getAttribute(getAttributeMapping(((Object_Attribute_Map__c)transformation).Source__c), ''));
					node.setAttribute('cardinality', ((Object_Attribute_Map__c)transformation).Source_Cardinality__c);
					node.setAttribute('dataType', ((Object_Attribute_Map__c)transformation).Target_Data_Type__c);
				}
				//Add node as child element to targetNode
				else {
					node = targetNode.addChildElement(((Object_Attribute_Map__c)transformation).Target__c, '', '');
					node.addTextNode(sourceNode.getText());
					node.setAttribute('cardinality', ((Object_Attribute_Map__c)transformation).Source_Cardinality__c);
					node.setAttribute('dataType', ((Object_Attribute_Map__c)transformation).Target_Data_Type__c);
				}
			}
		}

		//Only perform recursive search when instructed
		if (recursiveSearch) {
			//Recursively transform over attributes but prevent further recursion
			for (Integer i = 0; i < sourceNode.getAttributeCount(); i++) {
				String newSourceDocPath = sourceDocPath + '@' + sourceNode.getAttributeKeyAt(i);
				transform(transformationsMap, newSourceDocPath, sourceNode, newTargetNode, false);
			}

			//Recursively transform over child elements
			for (Dom.XMLNode newSourceNode : sourceNode.getChildElements()) {
				String newSourceDocPath = sourceDocPath + '/' + newSourceNode.getName();
				system.debug('**transformation instanceof Object_Map__c:: new ->'+newSourceDocPath+'!!'+newTargetNode.getName());
				transform(transformationsMap, newSourceDocPath, newSourceNode, newTargetNode, true);
			}
		}
	}

	/*
    Recursively converts a node hierarchy (representation of an Apex Class) 
    into a JSON string
	 */
	private static String xmlNodeToJson(Dom.XmlNode node) {

		//Ensure input arguments have been initialised
		System.assert(node != null);

		//Initialise JSON string return variable
		String jsonStr = '';

		//When node is a complex type
		if (node.getText() == '') {
			//If a List or Set type collection
			if ((node.getAttribute('collectionType', '') == 'List') || 
					(node.getAttribute('collectionType', '') == 'Set')) {
				//Add opening square bracket and curly bracket
				jsonStr += '\"' + node.getName() + '\":[{';
				//Add each child node to the JSON string 
				jsonStr += xmlChildNodesToJson(node);
				//Remove trailing comma and add closing square bracket
				jsonStr = jsonStr.removeEnd(',');
				jsonStr += '}]';
			}
			//If a Map type collection
			else if (node.getAttribute('collectionType', '') == 'Map') {
				//Add Complex Type name and opening curly brace
				jsonStr += '\"' + node.getName() + '\":{';
				//Add key and opening curly brace
				jsonStr += '\"' + node.getChildElement(node.getAttribute('key',''), '').getText() + '\":{';
				//Add each child node to the JSON string 
				jsonStr += xmlChildNodesToJson(node);
				//Remove trailing comma and add closing curly brace
				jsonStr = jsonStr.removeEnd(',');
				jsonStr += '}}';
			}
			else
			{
				//Create JSON string
				jsonStr = '{' + xmlChildNodesToJson(node) + '}';
			}
		}

		return jsonStr;
	}

	/*
    Recursively converts a node hierarchy (representation of an Apex Class) 
    into a JSON string
	 */
	private static String xmlChildNodesToJson(Dom.XmlNode node) {

		String jsonStr = '';        
		Map<String, String> attributes = new Map<String, String>();

		//Add each child node to the JSON string 
		for (Dom.XmlNode childNode : node.getChildElements()) {
			//If the child node is a complex type 
			if (childNode.getText() == '') {
				//Create child JSON string
				String childJsonStr = xmlNodeToJson(childNode) + ',';
				//If an instance of the same complex type has previously been added to the JSON string
				//then insert the child JSON string into the existing JSON string before the existing
				//complex type
				if (jsonStr.contains(childNode.getName())) {
					jsonStr = jsonStr.replaceFirst('\"' + childNode.getName() + '\":.', childJsonStr.removeEnd('},').removeEnd('],') + ',');
				}
				//Just add the child JSON string to the existing JSON string
				else {
					jsonStr += childJsonStr;
				}
			}
			//Else the child node is a primitive type
			else {
				String attributeName = '\"' + childNode.getName() + '\":';
				String attributeValue = '\"' + childNode.getText() + '\"';

				if (node.getAttribute('createAttributesMap', '') == 'true') {
					attributes.put(attributeName, attributeValue);
				}
				else {
					//Check the cardinality of the attribute 
					if (childNode.getAttribute('cardinality', '') == 'Zero to Many') {
						//Check to see whether the attribute has already been added - if it has then append the new attribute value to the list
						if (attributes.containsKey(attributeName)) {
							attributeValue = ((String)attributes.get(attributeName)).removeEnd(']') + ',' + attributeValue + ']';
							attributes.put(attributeName, attributeValue);
						}
						//Else just insert the attribute value as the first item in the list
						else {
							attributes.put(attributeName, '[' + attributeValue + ']');
						}  
					}
					else if (childNode.getAttribute('cardinality', '') == 'Zero or One') {
						attributes.put(attributeName, attributeValue);
					}
				}
			}
		}

		//If attributes have been added to an attributes map then append this to the 
		//JSON string
		if (node.getAttribute('createAttributesMap', '') == 'true') {
			jsonStr += '\"attributes\":{';
			for (String key : attributes.keySet()) {
				jsonStr += key + attributes.get(key) + ',';
			}
			jsonStr = jsonStr.removeEnd(',');
			jsonStr += '}';
		}
		else {
			for (String key : attributes.keySet()) {
				jsonStr += key + attributes.get(key) + ',';
			}
			jsonStr = jsonStr.removeEnd(',');
		}

		return jsonStr;
	}
}