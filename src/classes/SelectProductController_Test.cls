/***************************************************************************************************************************************************
CREATED BY : APPIRIO
    27/08/2013 : Urminder(JDC) : created this test class.
UPDATES:
    01/24/2014 : Leena Mandadapu : updated to include AD/NonAD plans test data.
    04/23/2015 : Leena Mandadapu : Updated to fix the Too many SOQLs issue. Moved the Test.StartTest() method after test data creation method
****************************************************************************************************************************************************/

@isTest 
private class SelectProductController_Test {
    static Product__c  prod;
    static Case testCase;
    static Case testcase1;
    static Case testcase2;
    static Case testcase3;
    
    //Leena
    static Product__c HWprod;
    static Product__c ESPprod;
    static Product__c ADprod;
    static Case testADcaseESP;
    static Case testADcaseNOESP;
    static Case testNonADcaseESP;
    static Case testNoPurchDt;
    static Contact NOESPCnct;
    static Contact ESPCnct;
    static Asset__c thirtydays;
    static Asset__c lessoneyear;
    static Asset__c noPurchaseDt;
    static list<Address__c> addNoEspList;
    static list<Contact> contactList;
    static Contact cnct;
    static list<Product__c> prodList;
    static list<Asset__c> astList;
    static Asset__c ast;
    static  list<Address__c> addList1;
     
    static testMethod void myUnitTest() {
        
    try {   
      createprodData();
      //LM 04/23/2015 : moved the starttest statment after test data creation process to fix the too many SOQLs governor limit
      Test.startTest();
      createCaseData();
      Apexpages.currentPage().getParameters().put('caseId', testCase.Id);
      SelectProductController ctrl = new SelectProductController();
       
      ApexPages.currentPage().getParameters().put('ctRadio',prod.Id);
      ctrl.selectProduct();
      
      //Case updatedCase = [select SCEA_Product__c from Case where ID = :testCase.Id];
      //System.assertEquals(updatedCase.SCEA_Product__c, prod.Id,'Case should be udpated with selected Product');
      
      ctrl.clearProduct();
      //updatedCase = [select SCEA_Product__c from Case where Id = :testCase.Id];
      //System.assertEquals(updatedCase.SCEA_Product__c, null,'Case should be udpated with null');
      
      Apexpages.currentPage().getParameters().put('caseId', testCase1.Id);
      SelectProductController ctrl1 = new SelectProductController();
      
      Apexpages.currentPage().getParameters().put('caseId', testCase2.Id);
      SelectProductController ctrl2 = new SelectProductController();
      
      Apexpages.currentPage().getParameters().put('caseId', testCase3.Id);
      SelectProductController ctrl3 = new SelectProductController();
      
      //Leena AD/Non-AD Plan selections based on plan purchase dates
      
      //AD Plan selection with the valid ESP Eligible state address
      Apexpages.currentPage().getParameters().put('caseId', testADcaseESP.Id);
      SelectProductController ADctrl = new SelectProductController();
       
      ApexPages.currentPage().getParameters().put('ctRadio',ADprod.Id);
      ADctrl.selectProduct();
      
      //Case ADCase = [select SCEA_Product__c from Case where ID = :testADcaseESP.Id];
      //System.assertEquals(ADCase.SCEA_Product__c, ADprod.Id,'User was able to see and select AD Plan');
      
      //Non-AD Plan selection with the valid ESP Eligible state address
      Apexpages.currentPage().getParameters().put('caseId', testNonADcaseESP.Id);
      SelectProductController NonADctrl = new SelectProductController();
       
      ApexPages.currentPage().getParameters().put('ctRadio',ESPprod.Id);
      NonADctrl.selectProduct();
      
      //Case NonADCase = [select SCEA_Product__c from Case where ID = :testNonADcaseESP.Id];
      //System.assertEquals(NonADCase.SCEA_Product__c, ESPprod.Id,'User was able to see and select Non-AD Plan'); 
      
      //NO ESP State
      Apexpages.currentPage().getParameters().put('caseId', testADcaseNOESP.Id);
      SelectProductController NonESPctrl = new SelectProductController();
       
      ApexPages.currentPage().getParameters().put('ctRadio',ESPprod.Id);
      NonESPctrl.selectProduct();
      
     // Case NonESPCase = [select SCEA_Product__c from Case where ID = :testADcaseNOESP.Id];
     // System.assertEquals(NonESPCase.SCEA_Product__c, ESPprod.Id,'Non ESP State Consumer was able to see and select Non-AD Plan');
      
      //No Purchase Dt
      Apexpages.currentPage().getParameters().put('caseId', testNoPurchDt.Id);
      SelectProductController noPurchDt = new SelectProductController();
       
      ApexPages.currentPage().getParameters().put('ctRadio',ESPprod.Id);
      noPurchDt.selectProduct();
      
     // Case noPurchDtCase = [select SCEA_Product__c from Case where ID = :testNoPurchDt.Id];
      //System.assertEquals(noPurchDtCase.SCEA_Product__c, ESPprod.Id,'No Purchase Date');  
        
      //Leena - End
      
      Test.stopTest();
    } catch (Exception e) {
        System.debug('Exception: '+e);
    }
  }
    
    static void createprodData() {
     list<Case> caseList = new list<Case>(); 	
     contactList = new list<Contact>();	
     cnct = new Contact();
     cnct.LastName = 'test';
     cnct.FirstName = 'ftest';
     cnct.Phone = '12345';
     contactList.add(cnct);
     
     // Leena 
     //Create address with NO ESP state
     addNoEspList = TestClassUtility.createAddress(2, false);
     addNoEspList[0].State__c = 'PR';
     
     //Create Test Contact for PPP Plans validation
     NOESPCnct= new Contact();
     NOESPCnct.LastName = 'TEST_NOESPSTATE_LN';
     NOESPCnct.FirstName = 'TEST_NOESPSTATE_FN';
     NOESPCnct.Phone = '1112229999';
     NOESPCnct.Bill_To__c = addNoEspList[0].Id;
     NOESPCnct.Ship_To__c = addNoEspList[0].Id;
     
     contactList.add(NOESPCnct);
     
     
     // Create address with valid ESP Eligible state
     addNoEspList[1].State__c = 'CA';
     insert addNoEspList;
     
     // Create Test Contact for PPP Plans validation
     ESPCnct= new Contact();
     ESPCnct.LastName = 'TEST_ESPSTATE_LN';
     ESPCnct.FirstName = 'TEST_ESPSTATE_FN';
     ESPCnct.Phone = '1114449999';
     ESPCnct.Bill_To__c = addNoEspList[1].Id;
     ESPCnct.Ship_To__c = addNoEspList[1].Id;
     contactList.add(ESPCnct);
     
     insert contactList;
     // End Contact test data creation
     prodList = new list<Product__c>();
     prod = new Product__c();
     prod.Description__c = 'Test Desc';
     prod.Genre__c = 'Test Genre';
     prod.Product_Type__c = 'NA';
     prod.Sub_Type__c = 'Games';
     prod.SKU__c = 'SKU1';
     prod.ESP_Purchase_Available__c = true;
     prodList.add(prod);
     
     //Leena - Create Test Plans data
     
     //create Hardware product
     HWprod = new Product__c();
     HWprod.Description__c = 'Test HW Desc';
     HWprod.Genre__c = 'PS4';
     HWprod.Product_Type__c = 'PS4';
     HWprod.Sub_Type__c = 'Hardware';
     HWprod.SKU__c = 'PS4HW';
     HWprod.ESP_Purchase_Available__c = true;
     HWprod.Status__c = 'Active';
     prodList.add(HWprod);
     
     //Create Non-AD Plan
     ESPprod = new Product__c();
     ESPprod.Description__c = 'Test Non-AD Plan';
     ESPprod.Name = 'Test PS4 Non-AD Plan';
     ESPprod.Status__c = 'Active';
     ESPprod.Genre__c = 'PS4';
     ESPprod.Product_Type__c = 'ESP';
     ESPprod.Sub_Type__c = 'ESP';
     ESPprod.Target_Country__c= 'USA';
     ESPprod.SKU__c = 'PS4NON-ADH';
     ESPprod.Duration__c = 24;
     ESPprod.List_Price__c = 49.99;
     ESPprod.Tax_Code__c = 'ESP';
	 prodList.add(ESPprod);
	      
     //Create AD Plan
     ADprod = new Product__c();
     ADprod.Description__c = 'Test AD Plan';
     ADprod.Name = 'Test PS4 AD Plan';
     ADprod.Status__c = 'Active';
     ADprod.Genre__c = 'PS4';
     ADprod.Product_Type__c = 'ESP';
     ADprod.Sub_Type__c = 'AD';
     ADprod.Target_Country__c= 'USA';
     ADprod.SKU__c = 'PS4ADH';
     ADprod.Duration__c = 24;
     ADprod.List_Price__c = 59.99;
     ADprod.Tax_Code__c = 'ESP';
     prodList.add(ADprod);
     insert prodList;
         
     //Leena - End Test Plan Prod
     
     //Leena - Create test Asset 
     //ESP Purchase Date within 30 days of unit purchase date 
     astList = new list<Asset__c>();
     thirtydays = new Asset__c();
     thirtydays.Product__c = HWprod.Id;
     thirtydays.Purchase_Date__c = Date.today().adddays(-20);
     astList.add(thirtyDays);
     
     //ESP Purchase Date within 365 days of unit purchase date 
     lessoneyear = new Asset__c();
     lessoneyear.Product__c = HWprod.Id;
     lessoneyear.Purchase_Date__c = Date.today().addmonths(-11);
     astList.add(lessoneyear);
     
     ast = new Asset__c();
     ast.Product__c = prod.Id;
     ast.Siras_Purchase_Date__c = Date.today();
     insert ast;
     
     //Asset with no Purchase Date
     noPurchaseDt = new Asset__c();
     noPurchaseDt.Product__c = HWprod.Id;
     //noPurchaseDt.Purchase_Date__c = null;
     astList.add(noPurchaseDt);     
     
    //End Test Asset
    
     addList1 = TestClassUtility.createAddress(3, false);
     addList1[0].Country__c = 'CA';
     insert addList1[0]; 
     
      testcase = new Case();
     testcase.Status = 'Open';
     testcase.received_date__c = date.today();
     testcase.shipped_date__c = date.today().addDays(1);
     testcase.outbound_tracking_number__c = '101';
     testcase.outbound_carrier__c = 'TYest1';
     testCase.Origin = 'Social';
     testCase.ContactId = cnct.Id;
     testCase.Offender__c = 'Test';
     testCase.Asset__c = ast.Id;
     testCase.Ship_To__c = TestClassUtility.createAddress(1, true)[0].Id;
     testCase.Product__c = 'PS4';
     testCase.Sub_Area__c = 'Account Help';
     testCase.Product__c = 'PS4';
     testCase.Sub_Area__c = 'Account Help';
     caseList.add(testCase);
        
     testcase1 = new Case();
     testcase1.Status = 'Open';
     testcase1.received_date__c = date.today();
     testcase1.shipped_date__c = date.today().addDays(1);
     testcase1.outbound_tracking_number__c = '101';
     testcase1.outbound_carrier__c = 'TYest1';
     testcase1.Origin = 'Social';
     testcase1.ContactId = cnct.Id;
     testcase1.Offender__c = 'Test';
     testcase1.Asset__c = ast.Id;
     testcase1.Ship_To__c = addList1[0].Id;
     testCase1.Product__c = 'PS4';
     testCase1.Sub_Area__c = 'Account Help';
     caseList.add(testCase1);
     
     addList1[1].Country__c = 'CAN';
     insert addList1[1]; 
     
     testcase2 = new Case();
     testcase2.Status = 'Open';
     testcase2.received_date__c = date.today();
     testcase2.shipped_date__c = date.today().addDays(1);
     testcase2.outbound_tracking_number__c = '101';
     testcase2.outbound_carrier__c = 'TYest1';
     testcase2.Origin = 'Social';
     testcase2.ContactId = cnct.Id;
     testcase2.Offender__c = 'Test';
     testcase2.Asset__c = ast.Id;
     testcase2.Ship_To__c = addList1[1].Id;
     testCase2.Product__c = 'PS4';
     testCase2.Sub_Area__c = 'Account Help';
     caseList.add(testCase2);
     
     addList1[2].Country__c = 'IN';
     insert addList1[2]; 
     
     testcase3 = new Case();
     testcase3.Status = 'Open';
     testcase3.received_date__c = date.today();
     testcase3.shipped_date__c = date.today().addDays(1);
     testcase3.outbound_tracking_number__c = '101';
     testcase3.outbound_carrier__c = 'TYest1';
     testcase3.Origin = 'Social';
     testcase3.ContactId = cnct.Id;
     testcase3.Offender__c = 'Test';
     testcase3.Asset__c = ast.Id;
     testcase3.Ship_To__c = addList1[2].Id;
     testCase3.Product__c = 'PS4';
     testCase3.Sub_Area__c = 'Account Help';
     
     caseList.add(testCase3);
     insert caseList;
    }
    
    static void createCaseData() {  
    //Leena - Create Test Case 
    // AD Plan validation for No ESP State
    list<Case> caseList = new list<Case>();
     testADcaseNOESP = new Case();
     testADcaseNOESP.Status = 'Open';
     testADcaseNOESP.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
     testADcaseNOESP.ContactId = NOESPCnct.Id;
     testADcaseNOESP.Asset__c = thirtydays.Id;
     testADcaseNOESP.Ship_To__c = addNoEspList[0].Id;

     caseList.add(testADcaseNOESP);
     
     //AD Plan validation for ESP State
     testADcaseESP = new Case();
     testADcaseESP.Status = 'Open';
     testADcaseESP.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
     testADcaseESP.ContactId = ESPCnct.Id;
     testADcaseESP.Asset__c = thirtydays.Id;
     testADcaseESP.Ship_To__c = addNoEspList[1].Id;
     caseList.add(testADcaseESP);
     
     //Non-AD Plan validation for ESP State
     testNonADcaseESP = new Case();
     testNonADcaseESP.Status = 'Open';
     testNonADcaseESP.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
     testNonADcaseESP.ContactId = ESPCnct.Id;
     testNonADcaseESP.Asset__c = lessoneyear.Id;
     testNonADcaseESP.Ship_To__c = addNoEspList[1].Id;
     
     caseList.add(testNonADcaseESP); 
        
     //No Purchase Date
     testNoPurchDt = new Case();
     testNoPurchDt.Status = 'Open';
     testNoPurchDt.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
     testNoPurchDt.ContactId = ESPCnct.Id;
     testNoPurchDt.Asset__c = noPurchaseDt.Id;
     testNoPurchDt.Ship_To__c = addNoEspList[1].Id;
     
    /* testcase = new Case();
     testcase.Status = 'Open';
     testcase.received_date__c = date.today();
     testcase.shipped_date__c = date.today().addDays(1);
     testcase.outbound_tracking_number__c = '101';
     testcase.outbound_carrier__c = 'TYest1';
     testCase.Origin = 'Social';
     testCase.ContactId = cnct.Id;
     testCase.Offender__c = 'Test';
     testCase.Asset__c = ast.Id;
     testCase.Ship_To__c = TestClassUtility.createAddress(1, true)[0].Id;
     testCase.Product__c = 'PS4';
     testCase.Sub_Area__c = 'Account Help';
     testCase.Product__c = 'PS4';
     testCase.Sub_Area__c = 'Account Help';
     caseList.add(testCase);
        
     testcase1 = new Case();
     testcase1.Status = 'Open';
     testcase1.received_date__c = date.today();
     testcase1.shipped_date__c = date.today().addDays(1);
     testcase1.outbound_tracking_number__c = '101';
     testcase1.outbound_carrier__c = 'TYest1';
     testcase1.Origin = 'Social';
     testcase1.ContactId = cnct.Id;
     testcase1.Offender__c = 'Test';
     testcase1.Asset__c = ast.Id;
     testcase1.Ship_To__c = addList1[0].Id;
     testCase1.Product__c = 'PS4';
     testCase1.Sub_Area__c = 'Account Help';
     caseList.add(testCase1);
     
     addList1[1].Country__c = 'CAN';
     insert addList1[1]; 
     
     testcase2 = new Case();
     testcase2.Status = 'Open';
     testcase2.received_date__c = date.today();
     testcase2.shipped_date__c = date.today().addDays(1);
     testcase2.outbound_tracking_number__c = '101';
     testcase2.outbound_carrier__c = 'TYest1';
     testcase2.Origin = 'Social';
     testcase2.ContactId = cnct.Id;
     testcase2.Offender__c = 'Test';
     testcase2.Asset__c = ast.Id;
     testcase2.Ship_To__c = addList1[1].Id;
     testCase2.Product__c = 'PS4';
     testCase2.Sub_Area__c = 'Account Help';
     caseList.add(testCase2);
     
     addList1[2].Country__c = 'IN';
     insert addList1[2]; 
     
     testcase3 = new Case();
     testcase3.Status = 'Open';
     testcase3.received_date__c = date.today();
     testcase3.shipped_date__c = date.today().addDays(1);
     testcase3.outbound_tracking_number__c = '101';
     testcase3.outbound_carrier__c = 'TYest1';
     testcase3.Origin = 'Social';
     testcase3.ContactId = cnct.Id;
     testcase3.Offender__c = 'Test';
     testcase3.Asset__c = ast.Id;
     testcase3.Ship_To__c = addList1[2].Id;
     testCase3.Product__c = 'PS4';
     testCase3.Sub_Area__c = 'Account Help';
     
     caseList.add(testCase3);
     insert caseList;*/
     
   }
}