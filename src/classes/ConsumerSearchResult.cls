/******************************************************************************
Class         : ConsumerSearchResult
Description   : A webservice Response Class which has reponse fields that returned from Consumer services.
Developed by  : Urminder Vohra(JDC)
Date          : August 21, 2013
Task		  : T-176715                 
******************************************************************************/
public class ConsumerSearchResult {
  public String ErrorMessage;
  public String ErrorDescription;
  public String ErrorCode;
  public String Status;
  public list<Sony_MiddlewareConsumerdata3.Account_mdm> listOfAccounts;
  public list<Sony_MiddlewareConsumerRefresh_Final.Contact_mdm> listOfContacts;
  public list<Sony_MiddlewareUpdateInsertAddress2.Contact> listOfContactForUpdateInsertAddress;
  public list<Sony_MiddlewareUpdateAccountAsset.Account_mdm> listOfAccountForUpdateAccountAsset;
}