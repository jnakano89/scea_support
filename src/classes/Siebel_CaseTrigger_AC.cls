/******************************************************************************
Class         : Siebel_CaseTrigger_AC
Description   : Management class called by Siebel_CaseTrigger case Trigger
Developed by  : Rahul Mittal
Date          : March 25, 2013              
******************************************************************************/
public without sharing class Siebel_CaseTrigger_AC {
	
	//Method to update Current_Case_Owner field of Case on insertion or update of Case
	public static void updateCurrCaseOwner(List<Case> newCaseslst, Map<Id, Case> oldCasesMap){
		Boolean isInsert = oldCasesMap == null;

		for(Case caseObj : newCaseslst){
			String ownerId = string.valueOf(caseObj.OwnerId);
			Boolean isOwnerUpdate = false;
			//Checking if case is inserting or updating 
			if(isInsert && ownerId.startsWith('005'))
				isOwnerUpdate = true;
			else if(!isInsert && caseObj.OwnerId != oldCasesMap.get(caseObj.Id).OwnerId && ownerId.startsWith('005'))
				isOwnerUpdate = true;
			
			system.debug(isOwnerUpdate + ' =====isOwnerUpdate====== ');
			//Modifying CurrentCaseOwner field value if isOwnerUpdate is true
			if(isOwnerUpdate)
				caseObj.Current_Case_Owner__c = caseObj.OwnerId;
		}
	}
}