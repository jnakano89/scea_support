/**
9/13/2013 : Urminder(JDC) : created this test class for controller AssetServiceUtility.
 */
@isTest
global class AssetServiceUtility_Test {

    static testMethod void errorMessageTest() {
       Test.startTest();
       Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
       AssetSearchResult result = AssetServiceUtility.getAssets('1-4EG-1');
       System.assertNotEquals(result.ErrorMessage, null, 'This service should return error Message');
       Test.stopTest();
    }
    static testMethod void serviceResponseTest() {
       Test.startTest();
       Test.setMock(WebServiceMock.class, new WebServiceMockImpl2());
       AssetSearchResult result = AssetServiceUtility.getAssets('1-4EG-1');
       System.assertEquals(result.ErrorMessage, null, 'This service should not return error Message');
       System.assertEquals(result.listOfAssests.size(), 1, 'Assets should be return by this service');
       Test.stopTest();
    }
    global class WebServiceMockImpl implements WebServiceMock {
	   global void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,	
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	      		Sony_MiddlewareAssetdata.PSConsumerAccountAssets_Output_element responseElm = 
	      		  	new Sony_MiddlewareAssetdata.PSConsumerAccountAssets_Output_element();
	       		
	       		responseElm.ErrorMessage = new Sony_MiddlewareAssetdata.ErrorType();
	       		responseElm.ErrorMessage.ErrorMessage = 'TEst Error Message';
	       		response.put('response_x', responseElm); 
	   		   }
    }
    
    global class WebServiceMockImpl2 implements WebServiceMock {
	   global void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,	
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	      		Sony_MiddlewareAssetdata.PSConsumerAccountAssets_Output_element responseElm = 
	      		  	new Sony_MiddlewareAssetdata.PSConsumerAccountAssets_Output_element();
	       		
	       		
	       		Sony_MiddlewareAssetdata.Asset ast = new Sony_MiddlewareAssetdata.Asset();
	       		ast.MDMAssetRowId = '1-1-1';
	       		
	       		Sony_MiddlewareAssetdata.ListOfAsset lstAst = new Sony_MiddlewareAssetdata.ListOfAsset();
	       		lstAst.Asset = new list<Sony_MiddlewareAssetdata.Asset>{ast};
	       		
	       		Sony_MiddlewareAssetdata.Account acc = new Sony_MiddlewareAssetdata.Account();
	       		acc.MDMAccountRowId = '1-4EG-1';
	       		acc.ListOfAsset = lstAst;
	       		
	       		responseElm.ListOfAccount = new Sony_MiddlewareAssetData.ListOfAccount();
	       		responseElm.ListOfAccount.Account = new list<Sony_MiddlewareAssetdata.Account>{acc};
	       		
	       		response.put('response_x', responseElm); 
	   		   }
    }
}