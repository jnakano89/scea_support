//9/12/2013 : Urminder(JDC) : udpated updateFields() [T-181914]
public with sharing class OrderLineManagement {


    public static void beforeInsert(List<Order_Line__c> orderLineList){
	
		updateFields(orderLineList);	
		 
    } 

    public static void beforeUpdate(List<Order_Line__c> orderLineList, Map<Id, Order_Line__c> oldOrderLineMap){

		updateFields(orderLineList);	
		
	}   

    public static void afterInsert(List<Order_Line__c> orderLineList){
		 
    } 

    public static void afterUpdate(List<Order_Line__c> orderLineList, Map<Id, Order_Line__c> oldOrderLineMap){
	
 	
	}   

    public static void updateFields(List<Order_Line__c> orderLineList){
    	
    	Set<Id> orderIds = new Set<Id>();
    	
    	map<Id,String> orderNameIdMap = new map<Id, String>();
    	
    	map<Id,String> orderPPPSKU_Map = new map<Id, String>();
    	map<Id,Id> orderPPPProduct_Map = new map<Id, Id>();
    	map<Id,String> orderPPPName_Map = new map<Id, String>();
    	
    	
    	for(Order_Line__c ol : orderLineList){
		  orderIds.add(ol.Order__c);	    		
    	}
    	
    	for(Order__c ordr : [select Id, Name, Order_Type__c, Asset__r.PPP_Product_SKU__c,  Asset__r.PPP_Product__r.Id,   Asset__r.PPP_Product__r.Name from Order__c where Id IN : orderIds]) {
    		orderNameIdMap.put(ordr.Id, ordr.Name);
    		
    		System.Debug('>>>>+ordr.Asset__r.PPP_Product_SKU__c<<<<<'+ordr.Asset__r.PPP_Product_SKU__c );
    		System.Debug('>>>>+ordr.Order_Type__c<<<<<'+ordr.Order_Type__c );
    		
    		if(ordr.Asset__r.PPP_Product_SKU__c<>null && (ordr.Order_Type__c=='ESP' || ordr.Order_Type__c=='PSN')){
    		   orderPPPSKU_Map.put (ordr.Id, ordr.Asset__r.PPP_Product_SKU__c);
    		   orderPPPProduct_Map.put (ordr.Id, ordr.Asset__r.PPP_Product__r.Id);
    		   orderPPPName_Map.put (ordr.Id, ordr.Asset__r.PPP_Product__r.Name);
    		}
    	}
    	
     	for(Order_Line__c ol : orderLineList) {
     	  if(orderNameIdMap.containsKey(ol.Order__c)) {	
    	    ol.Name = orderNameIdMap.get(ol.Order__c) + '-' + ol.Line_Number__c;
    	
     	  }  
     	  
     	  System.Debug('>>>>>>>>>>>+orderPPPSKU_Map<<<<<<<<<' +orderPPPSKU_Map);
     	  System.Debug('>>>>>>>>>>>+orderPPPProduct_Map<<<<<<<<<' +orderPPPProduct_Map);
     	  System.Debug('>>>>>>>>>>>+orderPPPName_Map<<<<<<<<<' +orderPPPName_Map);
     	  
     	  
    	if(orderPPPSKU_Map.containsKey(ol.Order__c) && (ol.SKU__c=='' || ol.sku__c==null)) {	
    	    
    	     ol.SKU__c=orderPPPSKU_Map.get(ol.Order__c);
    	     ol.Product__c=orderPPPProduct_Map.get(ol.Order__c);
    	     ol.Product_Name__c=orderPPPName_Map.get(ol.Order__c);
    	     
     	 }  
    	
    	
    	}
    }
}