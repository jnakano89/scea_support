/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CounterResetTest {
	
	public static String CRON_EXP = '0 0 0 11 1 ? 2050';
        
   	static testmethod void counterResetSchBtchTest() {
   		
   	  Contact con = new Contact(LastName = 'lname', 
   	  FirstName = 'fname', 
   	  Email='random101714@x.com');
      insert con;
      Contact con1 = new Contact(LastName = 'lname1', 
      FirstName = 'fname1', 
      Email='random101714@x.com');
      insert con1;
      Test.startTest();      
      String jobId = System.schedule('ScheduleApexClassTest',CRON_EXP, new ContactCounterResetScheduler());               
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];     
      System.assertEquals(CRON_EXP, ct.CronExpression);     
      System.assertEquals(0, ct.TimesTriggered);      
      System.assertEquals('2050-01-11 00:00:00', 
      String.valueOf(ct.NextFireTime));      
      Test.stopTest();      
   }
}