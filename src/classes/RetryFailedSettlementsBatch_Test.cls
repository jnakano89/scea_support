@isTest
global class RetryFailedSettlementsBatch_Test {

    static testMethod void myUnitTest() {
        Case testCase = TestClassUtility.createCase('Open', '', true);
        
        Order__c ordr = TestClassUtility.creatOrder(null, 1, false)[0];
        ordr.Case__c = testCase.Id;
        ordr.Order_Date__c = date.today();
        insert ordr;
        
        Payment__c payment = new Payment__c();
        payment.Order__c = ordr.Id;
        payment.Cybersource_Response_Code__c = 150;
        payment.Payment_Status__c = 'Declined';
        insert payment;
        
        TestClassUtility.insertCybersourceSettings();
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HTTPMockCyberSource());
        RetryFailedSettlementsBatch btch = new RetryFailedSettlementsBatch();
        String qry = 'Select p.Token__c,p.Order__r.Case__c,p.Order__r.Case__r.SIRAS_Date__c, p.Order__r.order_date__c,Purchase_Type__c,'+
		'  p.Order__r.Name,p.Order__c, p.Id From Payment__c p '+
		'';
		
		Payment__c paymentNew = Database.query(qry);
        btch.execute(null, new list<Payment__c>{paymentNew});
        
        Case updatedCase = [select Status from Case where ID = :testCase.Id];
        System.assertEquals(updatedCase.Status, CaseManagement.CASE_STATUS_ASSIGNED_CORPORATE_TEAM_T3, 'Case Status should be changed');
		Test.stopTest();
    }
     static testMethod void myUnitTest2() {
        Case testCase = TestClassUtility.createCase('Open', '', true);
        
        Order__c ordr = TestClassUtility.creatOrder(null, 1, false)[0];
        ordr.Case__c = testCase.Id;
        ordr.Order_Date__c = date.today();
        insert ordr;
        
        Payment__c payment = new Payment__c();
        payment.Order__c = ordr.Id;
        payment.Cybersource_Response_Code__c = 150;
        payment.Payment_Status__c = 'Declined';
        insert payment;
        
        TestClassUtility.insertCybersourceSettings();
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HTTPMockCyberSource());
        Database.executeBatch(new RetryFailedSettlementsBatch());
		Test.stopTest();
    }
     global class HTTPMockCyberSource implements HttpCalloutMock  {
	   global HTTPResponse respond(HTTPRequest req) {
      		System.assertEquals('https://ebctest.cybersource.com/ebctest/Query', req.getEndpoint());
	        System.assertEquals('POST', req.getMethod());
	        HttpResponse res = new HttpResponse();
	        //res.setHeader('Content-Type', 'application/json');
	        String body = getBody();
	        res.setBody(body);
	        res.setStatusCode(200);
	        return res;
	   }
	   String getBody() {
	   	String body = '<Report xmlns="https://ebctest.cybersource.com/ebctest/reports/dtd/tdr_1_9.dtd" Name="Transaction Detail" Version="1.9" MerchantID="scea_esptest" ReportStartDate="2013-09-18 12:35:23.902-08:00" ReportEndDate="2013-09-18 12:35:23.902-08:00">' + 
		'<Requests>' + 
		'<Request MerchantReferenceNumber="OrderDev - 000300" RequestDate="2013-09-17T06:34:51-08:00" RequestID="3794248915050178147626" SubscriptionID="" Source="Secure Acceptance Web/Mobile" TransactionReferenceNumber="5067937032">' + 
		'<BillTo>' + 
		'<FirstName> SABRINA</FirstName>' + 
		'<LastName> CONSUMER</LastName>' + 
		'<Address1> 123 B St</Address1>' + 
		'<City> San Mateo</City>' + 
		'<State> CA</State>' + 
		'<Zip> 94402</Zip>' + 
		'<Email> martha@appirio.com</Email>' + 
		'<Country> US</Country>' + 
		'<Phone/>' + 
		'</BillTo>' + 
		'<PaymentMethod>' + 
		'<Card>' + 
		'<AccountSuffix> 1111</AccountSuffix>' + 
		'<ExpirationMonth> 01</ExpirationMonth>' + 
		'<ExpirationYear> 2015</ExpirationYear>' + 
		'<CardType> Visa</CardType>' + 
		'</Card>' + 
		'</PaymentMethod>' + 
		'<LineItems>' + 
		'<LineItem Number="0">' + 
		'<FulfillmentType/>' + 
		'<Quantity> 1</Quantity>' + 
		'<UnitPrice> 59.99</UnitPrice>' + 
		'<TaxAmount> 0.00</TaxAmount>' + 
		'<ProductCode> default</ProductCode>' + 
		'</LineItem>' + 
		'</LineItems>' + 
		'<ApplicationReplies>' + 
		'<ApplicationReply Name="ics_auth">' + 
		'<RCode> 1</RCode>' + 
		'<RFlag> SOK</RFlag>' + 
		'<RMsg> Request was processed successfully.</RMsg>' + 
		'</ApplicationReply>' + 
		'<ApplicationReply Name="ics_bill">' + 
		'<RCode> 1</RCode>' + 
		'<RFlag> SOK</RFlag>' + 
		'<RMsg> Request was processed successfully.</RMsg>' + 
		'</ApplicationReply>' + 
		'</ApplicationReplies>' + 
		'<PaymentData>' + 
		'<PaymentRequestID> 3794248915050178147626</PaymentRequestID>' + 
		'<PaymentProcessor> smartfdc</PaymentProcessor>' + 
		'<Amount> 59.99</Amount>' + 
		'<CurrencyCode> USD</CurrencyCode>' + 
		'<TotalTaxAmount> 0.00</TotalTaxAmount>' + 
		'<AuthorizationCode> 123456</AuthorizationCode>' + 
		'<AVSResult>  YYY</AVSResult>' + 
		'<AVSResultMapped> Y</AVSResultMapped>' + 
		'<EventType> TRANSMITTED</EventType>' + 
		'<RequestedAmount> 59.99</RequestedAmount>' + 
		'<RequestedAmountCurrencyCode> USD</RequestedAmountCurrencyCode>' + 
		'</PaymentData>' + 
		'</Request>' + 
		'</Requests>' + 
		'</Report>' ;
		return body; 
	   }
    }
}