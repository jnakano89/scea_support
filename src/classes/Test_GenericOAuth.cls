/**
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest 
private class Test_GenericOAuth {
	
    static testMethod void testGenericOAuth() {  
    	
    	  Web_Call_Setting__c Web_Call_Setting = UtilityGenericWebCallout.createWebCallSetting(true, 'Test Web Setting');    	  
    	  createWebKeys(Web_Call_Setting.Id);
    	  GenericWSHandler handler = new GenericWSHandler('Test Web Setting'); 
    	  map<String, Web_Call_Setting_key__c> web_setting = handler.Web_Call_Settings;  
    	  HttpRequest req = new HttpRequest(); 
    	  handler.setHttpCall(req);
    	  req.setMethod('post');
    	  req.setHeader('Content-Type','application/x-www-form-urlencoded');
    	  req.setBody(web_setting.get('requestbody').value__c);
    	  Test.startTest();  
        GenericOAuth OAuth = new GenericOAuth();
        OAuth.setConsumerKey('');
        OAuth.setToken('');
        OAuth.setConsumerKey('');
        OAuth.sign(req, web_setting);
        // Verify Authorization
        System.assertNotEquals(req.getHeader('Authorization'),null);        
        try{
        		OAuth.sign(req,'','');
        }
        catch(Exception e){
        		System.debug('Caught Exception is..'+e);
        }
        Test.stopTest();        
    }    
    
    static void createWebKeys(String webSettingId) {
        Web_Call_Setting_key__c[] Web_Call_Setting_keys = new Web_Call_Setting_key__c[]{
      	UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
      	'Name'=> 'URL',
      	 'Key_Location__c' => 'Url',
      	 'Setting_Link__c' => null,
      	 'Value__c'=> 'https://enterpriseorder-e2e.platform.intuit.com/v1/OrderLookup',
      	 'Web_Call_Setting__c' => webSettingId
        }),
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'requestbody',
         'Key_Location__c' => 'Content',
         'Setting_Link__c' => null,
         'Value__c'=> '<ns0:GetSalesOrder xmlns:ns0="http://www.openapplications.org/oagis" xmlns:PSB="http://www.intuit.com/PSB" xmlns:Lacerte="http://www.lacertesoftware.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:Intuit="http://www.intuit.com" xmlns:MACS="http://www.intuit.com/MACS" xmlns:CRM="http://www.intuit.com/CRM" xmlns:ERP="http://www.intuit.com/ERP" xmlns:Payroll="http://www.intuit.com/Payroll" xmlns:Harland="http://www.harland.com"><ns0:ApplicationArea><ns0:CreationDateTime>{!CreationDateTime}</ns0:CreationDateTime><ns0:UserArea><Intuit:Version>V3-0</Intuit:Version></ns0:UserArea></ns0:ApplicationArea><ns0:DataArea><ns0:Get confirm="Always"><ns0:ReturnCriteria><ns0:SelectExpression>ByDocumentId</ns0:SelectExpression></ns0:ReturnCriteria></ns0:Get><ns0:SalesOrder><ns0:Header><ns0:DocumentIds><ns0:DocumentId><ns0:Id>{!ByDocumentId}</ns0:Id></ns0:DocumentId></ns0:DocumentIds></ns0:Header></ns0:SalesOrder></ns0:DataArea></ns0:GetSalesOrder>',
         'Web_Call_Setting__c' => webSettingId
        }),
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'intuit_offeringId',
         'Key_Location__c' => 'Header',
         'Setting_Link__c' => null,
         'Value__c'=> 'Intuit.wcg.turbotax.salesforce.support',
         'Web_Call_Setting__c' => webSettingId
        }),
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'intuit_appid',
         'Key_Location__c' => 'Header',
         'Setting_Link__c' => null,
         'Value__c'=> 'Intuit.cg.turbotax.salesforce',
         'Web_Call_Setting__c' => webSettingId
        }),
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'Version',
         'Key_Location__c' => 'Misc',
         'Setting_Link__c' => null,
         'Value__c'=> 'V1-0',
         'Web_Call_Setting__c' => webSettingId
        }),
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'Method',
         'Key_Location__c' => 'Method',
         'Setting_Link__c' => null,
         'Value__c'=> 'POST',
         'Web_Call_Setting__c' => webSettingId
        }),
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'oauth_privateKey',
         'Key_Location__c' => 'Misc',
         'Setting_Link__c' => null,
         'Value__c'=> 'MIICXQIBAAKBgQCj1ArWC3QcDOTIgLtj0SkcR+JonzeVsGoJhKU0Vj1rH7hb02hK'+
											'WkwGeIhNt8XGik2DwJM7qxI/VcdxdhPLyt9VZZ14wqvYfXc7X8960LBV+Iss7L7C'+
											'h9bY+y7pmsYkW4U2jT9EV+2DhkXR8mccc8bUwCvbLvNoIRu1uPr6dLQCLQIDAQAB'+
											'AoGAZlawCjOPbXJUfDeGV30yDBuPgEFtbJGOO7HjrUTuCvv+o0X1kPTFJwmmQ743'+
											'yqVcdKAYmR5tQKkp9GOJTDXHukTXJHOHhEJaARI3bBlb205tBtiJ6B2mRnvHk6d2'+
											'IP0yTKzhPxl/lqKu29JBbXmS4bqsmNbZunzDBwLYT2SlvQECQQDgdhOEIY5pNH6B'+
											'8tWZ45jjm1kVsIRVXK1lhT3n3Pnjmm/01LH/u/wRGVZKEHfrKcSwNfwmQpKUGg9p'+
											'2H4E/93tAkEAutj8FhA6ZZz+b7b/xKKgFxRgM8YCEbIKtHWdxdJlKLMd5S/5FhYm'+
											'2ajhk+7EIY73U9MOcWWrJQ5h8ZDkJWItQQJBAMc55l6bziQw1KyQlehuK9CxnON0'+
											'djCx/rGiDu1SpHXRKNfdRVQgTT38CCva7CVNQGfKstBevG05qD6hSlEzrY0CQFw/'+
											'bJXoBSzNycCV9Mi8EZXTdXuaMsNRx3844mAcc90YMZZJjQyeeyfS/SvwIdz/nJRC'+
											'C0tpCooNA8Yj5r+eLQECQQCoFFK9n2oAMRx4QMbNeAvDRu9z16VrEMTmkNO3QVqZ'+
											'E0VSaWDTPgt3Z+EppNQHffZnUFQqlmt3HlAxTjFum3KT',
         'Web_Call_Setting__c' => webSettingId
        }),                 
         UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'oauth_consumer_key',
         'Key_Location__c' => 'OAUTH',
         'Setting_Link__c' => null,
         'Value__c'=> 'IntuitCustomerCareSalesforceE2E',
         'Web_Call_Setting__c' => webSettingId
        }), 
         UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'oauth_signature_method',
         'Key_Location__c' => 'OAUTH',
         'Setting_Link__c' => null,
         'Value__c'=> 'RSA-SHA1',
         'Web_Call_Setting__c' => webSettingId
        }),
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'Authorization',
         'Key_Location__c' => 'Header',
         'Setting_Link__c' => null,
         'Value__c'=> '[AuthorizationHeader]',
         'Web_Call_Setting__c' => webSettingId
        }), 
         UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'oauth_timestamp',
         'Key_Location__c' => 'OAUTH',
         'Setting_Link__c' => null,
         'Value__c'=> '[oauth_timestamp]',
         'Web_Call_Setting__c' => webSettingId
        })               
        };
        
        insert Web_Call_Setting_keys;
    }
}