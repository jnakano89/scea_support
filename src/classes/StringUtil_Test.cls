/**
 * Name     : StringUtil_Test
 * Usage    : Unit test class for StringUtil class
 *            
 *  
 * Created By      : Takayuki Shiroo
 * Modified By     : 
 * Modified Date   : October 16, 2012
 * 
 * (c) 2012 Appirio, Inc.
 */
@isTest(seeAllData=false)
private class StringUtil_Test {

	/**
	* Test for isNullOrEmpty
	*/
	private static testMethod void test_isNullOrEmpty1() { 
		User u = createUser();
		Test.startTest();
		System.runAs(u) { 
			Boolean isNullOrEmpty = StringUtil.isNullOrEmpty(null);
			System.assertEquals(isNullOrEmpty, true);
			
			isNullOrEmpty = StringUtil.isNullOrEmpty('');
			System.assertEquals(isNullOrEmpty, true);
			
			isNullOrEmpty = StringUtil.isNullOrEmpty('Hello');
			System.assertEquals(isNullOrEmpty, false);
			
		}
		Test.stopTest();
	}
	
	/**
	* Test for isNullOrEmpty
	*/
	private static testMethod void test_formatMessage1() { 
		User u = createUser();
		Test.startTest();
		System.runAs(u) { 
			String message = StringUtil.formatMessage('{0}', new List<String>{'Hello'});
			System.assertEquals(message, 'Hello');
			
			message = StringUtil.formatMessage('aaaa', new List<String>{'Hello'});
			System.assertEquals(message, 'aaaa');

			message = StringUtil.formatMessage(null, new List<String>{'Hello'});
			System.assertEquals(message, null);

			List<String> params = null;
			message = StringUtil.formatMessage('aaaa', params);
			System.assertEquals(message, 'aaaa');
			
		}
		Test.stopTest();
	}
	
	/**
	* Test for isNullOrEmpty
	*/
	private static testMethod void test_formatMessage2() { 
		User u = createUser();
		Test.startTest();
		System.runAs(u) { 
			String message = StringUtil.formatMessage('{0}', 'Hello');
			System.assertEquals(message, 'Hello');
			
			message = StringUtil.formatMessage('aaaa', 'Hello');
			System.assertEquals(message, 'aaaa');

			message = StringUtil.formatMessage(null, 'Hello');
			System.assertEquals(message, null);

			String param = null;
			message = StringUtil.formatMessage('aaaa', param);
			System.assertEquals(message, 'aaaa');
			
		}
		Test.stopTest();
	}
	
	
	/**
	* Create Test User
	*/ 
	private static User createUser() { 
		List<Profile> profiles = [Select Id From Profile Where Name = 'System Administrator'];
		
		User user = new User();
		user.username = 'test_intuit_test@test.intuit.com';
		user.Email = user.username;
		user.CommunityNickname = user.Email;
		user.FirstName = 'John';
		user.LastName = 'Doe';
		user.Alias = 'JD';
		user.ProfileId = profiles[0].Id;
		user.EmailEncodingKey = 'UTF-8';
		user.LanguageLocaleKey = 'en_US';
		user.LocaleSidKey = 'en_US';
		user.TimeZoneSidKey = 'Asia/Tokyo';
		user.CompanyName = 'SCEA';
		insert user;
		return user;		
	}
	
	private static testMethod void testUUIDGenerator() { 
		System.assertNotEquals(StringUtil.getUUID(),null);		
	}
}