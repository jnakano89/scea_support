/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=true)
private class Test_GenericWSHandler {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //createWebCallSetting
        //createWebCallSettingKey
        
        //UtilityGenericWebCallout
      Web_Call_Setting__c Web_Call_Setting = UtilityGenericWebCallout.createWebCallSetting(false, '!!@Create Order Test!!');
      Web_Call_Setting__c Web_Call_Setting_Oauth = UtilityGenericWebCallout.createWebCallSetting(false, '!!@Base Service OAUTH TETS!!');
      Web_Call_Setting__c Web_Call_Setting_baseOauth = UtilityGenericWebCallout.createWebCallSetting(false, '!!@import - OAUTH!!');
      insert new Web_Call_Setting__c[]{Web_Call_Setting, Web_Call_Setting_Oauth, Web_Call_Setting_baseOauth};

      Web_Call_Setting_key__c[] Web_Call_Setting_keys = new Web_Call_Setting_key__c[]{
      	UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
      	'Name'=> 'URL',
      	 'Key_Location__c' => 'Url',
      	 'Setting_Link__c' => null,
      	 'Value__c'=> 'https://enterpriseorder-e2e.platform.intuit.com/v1/OrderLookup',
      	 'Web_Call_Setting__c' => Web_Call_Setting.id
        }),
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'requestbody',
         'Key_Location__c' => 'Content',
         'Setting_Link__c' => null,
         'Value__c'=> '<ns0:GetSalesOrder xmlns:ns0="http://www.openapplications.org/oagis" xmlns:PSB="http://www.intuit.com/PSB" xmlns:Lacerte="http://www.lacertesoftware.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:Intuit="http://www.intuit.com" xmlns:MACS="http://www.intuit.com/MACS" xmlns:CRM="http://www.intuit.com/CRM" xmlns:ERP="http://www.intuit.com/ERP" xmlns:Payroll="http://www.intuit.com/Payroll" xmlns:Harland="http://www.harland.com"><ns0:ApplicationArea><ns0:CreationDateTime>{!CreationDateTime}</ns0:CreationDateTime><ns0:UserArea><Intuit:Version>V3-0</Intuit:Version></ns0:UserArea></ns0:ApplicationArea><ns0:DataArea><ns0:Get confirm="Always"><ns0:ReturnCriteria><ns0:SelectExpression>ByDocumentId</ns0:SelectExpression></ns0:ReturnCriteria></ns0:Get><ns0:SalesOrder><ns0:Header><ns0:DocumentIds><ns0:DocumentId><ns0:Id>{!ByDocumentId}</ns0:Id></ns0:DocumentId></ns0:DocumentIds></ns0:Header></ns0:SalesOrder></ns0:DataArea></ns0:GetSalesOrder>',
         'Web_Call_Setting__c' => Web_Call_Setting.id
        }),
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'intuit_offeringId',
         'Key_Location__c' => 'Header',
         'Setting_Link__c' => null,
         'Value__c'=> 'Intuit.wcg.turbotax.salesforce.support',
         'Web_Call_Setting__c' => Web_Call_Setting.id
        }),
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'intuit_appid',
         'Key_Location__c' => 'Header',
         'Setting_Link__c' => null,
         'Value__c'=> 'Intuit.cg.turbotax.salesforce',
         'Web_Call_Setting__c' => Web_Call_Setting.id
        }),
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'Version',
         'Key_Location__c' => 'Misc',
         'Setting_Link__c' => null,
         'Value__c'=> 'V1-0',
         'Web_Call_Setting__c' => Web_Call_Setting.id
        }),
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'Method',
         'Key_Location__c' => 'Method',
         'Setting_Link__c' => null,
         'Value__c'=> 'POST',
         'Web_Call_Setting__c' => Web_Call_Setting.id
        }),
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'Content-Type',
         'Key_Location__c' => 'Header',
         'Setting_Link__c' => null,
         'Value__c'=> 'application/xml',
         'Web_Call_Setting__c' => Web_Call_Setting.id
        }),
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'setTimeout',
         'Key_Location__c' => 'Timeout',
         'Setting_Link__c' => null,
         'Value__c'=> '60000',
         'Web_Call_Setting__c' => Web_Call_Setting.id
        }),
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'intuit_originatingip',
         'Key_Location__c' => 'Header',
         'Setting_Link__c' => null,
         'Value__c'=> '123.45.67.89',
         'Web_Call_Setting__c' => Web_Call_Setting.id
        }),
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'intuit_offeringId',
         'Key_Location__c' => 'Timeout',
         'Setting_Link__c' => null,
         'Value__c'=> 'Intuit.cto.iam.ius.tests',
         'Web_Call_Setting__c' => Web_Call_Setting.id
        }),        
        //Link import - Base Service OAUTH
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> '!!import - Base Service OAUTH TETS!!',
         'Key_Location__c' => 'OAUTH',
         'Setting_Link__c' => Web_Call_Setting_Oauth.id,
         'Value__c'=> '',
         'Web_Call_Setting__c' => Web_Call_Setting.id
        }),
        //Link Oauth
        UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> '!!import - OAUTH!!',
         'Key_Location__c' => 'Misc',
         'Setting_Link__c' =>Web_Call_Setting_baseOauth.id,
         'Value__c'=> '',
         'Web_Call_Setting__c' =>  Web_Call_Setting_Oauth.id
        }),
        //Define Oauth
         UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'oauth_privateKey',
         'Key_Location__c' => 'Misc',
         'Setting_Link__c' => null,
         'Value__c'=> 'MIICXQIBAAKBgQCj1ArWC3QcDOTIgLtj0SkcR+JonzeVsGoJhKU0Vj1rH7hb02hK'+
'WkwGeIhNt8XGik2DwJM7qxI/VcdxdhPLyt9VZZ14wqvYfXc7X8960LBV+Iss7L7C'+
'h9bY+y7pmsYkW4U2jT9EV+2DhkXR8mccc8bUwCvbLvNoIRu1uPr6dLQCLQIDAQAB'+
'AoGAZlawCjOPbXJUfDeGV30yDBuPgEFtbJGOO7HjrUTuCvv+o0X1kPTFJwmmQ743'+
'yqVcdKAYmR5tQKkp9GOJTDXHukTXJHOHhEJaARI3bBlb205tBtiJ6B2mRnvHk6d2'+
'IP0yTKzhPxl/lqKu29JBbXmS4bqsmNbZunzDBwLYT2SlvQECQQDgdhOEIY5pNH6B'+
'8tWZ45jjm1kVsIRVXK1lhT3n3Pnjmm/01LH/u/wRGVZKEHfrKcSwNfwmQpKUGg9p'+
'2H4E/93tAkEAutj8FhA6ZZz+b7b/xKKgFxRgM8YCEbIKtHWdxdJlKLMd5S/5FhYm'+
'2ajhk+7EIY73U9MOcWWrJQ5h8ZDkJWItQQJBAMc55l6bziQw1KyQlehuK9CxnON0'+
'djCx/rGiDu1SpHXRKNfdRVQgTT38CCva7CVNQGfKstBevG05qD6hSlEzrY0CQFw/'+
'bJXoBSzNycCV9Mi8EZXTdXuaMsNRx3844mAcc90YMZZJjQyeeyfS/SvwIdz/nJRC'+
'C0tpCooNA8Yj5r+eLQECQQCoFFK9n2oAMRx4QMbNeAvDRu9z16VrEMTmkNO3QVqZ'+
'E0VSaWDTPgt3Z+EppNQHffZnUFQqlmt3HlAxTjFum3KT',
         'Web_Call_Setting__c' => Web_Call_Setting_baseOauth.id
        }),                 
         UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'oauth_consumer_key',
         'Key_Location__c' => 'OAUTH',
         'Setting_Link__c' => null,
         'Value__c'=> 'IntuitCustomerCareSalesforceE2E',
         'Web_Call_Setting__c' => Web_Call_Setting_baseOauth.id
        }), 
         UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'oauth_signature_method',
         'Key_Location__c' => 'OAUTH',
         'Setting_Link__c' => null,
         'Value__c'=> 'RSA-SHA1',
         'Web_Call_Setting__c' => Web_Call_Setting_baseOauth.id
        }), 

         UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'intuit_tid',
         'Key_Location__c' => 'Header',
         'Setting_Link__c' => null,
         'Value__c'=> '3243243543543-SHA1',
         'Web_Call_Setting__c' => Web_Call_Setting_baseOauth.id
        }), 
         UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'intuit_requestid',
         'Key_Location__c' => 'Header',
         'Setting_Link__c' => null,
         'Value__c'=> '345435ertgfret',
         'Web_Call_Setting__c' => Web_Call_Setting_baseOauth.id
        }), 
         UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'Authorization',
         'Key_Location__c' => 'Header',
         'Setting_Link__c' => null,
         'Value__c'=> '[AuthorizationHeader]',
         'Web_Call_Setting__c' => Web_Call_Setting_baseOauth.id
        }), 
         UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'oauth_timestamp',
         'Key_Location__c' => 'OAUTH',
         'Setting_Link__c' => null,
         'Value__c'=> '[oauth_timestamp]',
         'Web_Call_Setting__c' => Web_Call_Setting_baseOauth.id
        }), 
         UtilityGenericWebCallout.createWebCallSettingKey(false, new map<string, object>{
        'Name'=> 'oauth_nonce',
         'Key_Location__c' => 'OAUTH',
         'Setting_Link__c' => null,
         'Value__c'=> '[oauth_nonce]',
         'Web_Call_Setting__c' => Web_Call_Setting_baseOauth.id
        })
      };
      insert Web_Call_Setting_keys;
      Test.startTest();
      Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(GenericWSHandler.genericExceptionSample));
      try{
        GenericWSHandler wsHandler = new GenericWSHandler('!!@Create Order Test!!');      
        GenericWSHandler.getTemplateBySettingName('!!@Create Order Test!!');
        wsHandler.callStartSession(new GenericRequestBuilder.defGenericRequestBuilder());
        wsHandler.callStartSession('test Request');
        wsHandler.callStartSession_1('', new GenericRequestBuilder.defGenericRequestBuilder());
      }catch(exception Ex){
      	
      }
      TEST.stopTest();        
    }

}