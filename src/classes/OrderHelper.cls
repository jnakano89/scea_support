/****************************************************************************************************************************************************************************************************
Developed by  : Appirio

Updates       :
 04/01/2014 Leena Mandadapu : JIRA #: SF-280 - ATLAS sending duplicate PPP transactions to Assurant. Added logic to check if the order
                                             already exists in Staging PPP outbound.
 04/09/2014 Leena Mandadapu : 
                              JIRA# SF-481 - Records are failing to be inserted into the Staging PPP object due to base object field values exceeding Staging Object field limits and remove char validations on City
                              JIRA# SF-464 - Format PPP Cancellation date before inserting to Staging Outbound.
                              JIRA# SF-280 - Added "Assurant Enrolled"/"Assurant Cancelled" conditions to the duplicate Order check process.
                               
 06/02/2014 Leena Mandadapu : JIRA# SF-1012 - Remove Asset Purchase Date validation as it is throwing unnecessary error messages on the Order
                                                object and also failing to insert the PPP transaction into Staging PPP outbound for Assurant processing.
                                                
 07/01/2014 Leena Mandadapu : JIRA# SF-1026 - PSN Discounted Price - Updated PPP_List_Price__c to get the value from Product object instead of from Order total. Added validation rule to prevent record to send to
                                                Assurant incase if the Price is null or Zero.    
                                                
 09/29/2014 Leena Mandadapu : JIRA# SMS-654 - Updated class to exclude PYB records in the Assurant daily outbound process.  
 02/27/2015 Leena Mandadapu : JIRA# SMS-1269 - Updated class to fix the Consumer Last Name and First Name validations for Special Characters.                                                                                                                                                   
*********************************************************************************************************************************************************************************************************/

public class OrderHelper {
    public static Boolean processOutbound = true;

    public static void afterInsert (Map<Id, Order__c> newMap){

        // Outbound Daily Process logic
        //ProcessOutboundAssurantDaily(newMap, null);

        // For other Process/Logic on Order Trigger add separate method;
        
        populateOrderLookUpOnCase(newMap.values());

    }

    public static void afterUpdate (Map<Id, Order__c> newMap, Map<Id, Order__c> oldMap){

        // Outbound Daily Process logic
        System.debug('==============processOutbound=============' + processOutbound);

        if(processOutbound == true)
            ProcessOutboundAssurantDaily(newMap, oldMap);

        //createServiceOutboundStatgingRecords(newMap, oldMap);

        processOutbound = false;
    } 

     private static void populateOrderLookUpOnCase(list <Order__c> newOrders){
 
    Map <Id, Id> CaseOrderIdMap = new Map <Id, Id>();
    List<Case> UpdateCaseList = new List<Case>();
    
    for(Order__c o : newOrders){
       if(o.Case__c<>null){
          CaseOrderIdMap.put(o.Case__c, o.Id);   
       }
    }
   
   if(CaseOrderIdMap.keyset().size()>0){ 
       for(Case c : [Select Id, Order__c from Case where Id IN: CaseOrderIdMap.keyset()]){
          if(CaseOrderIdMap.get(c.Id)<>null){
             c.Order__c= CaseOrderIdMap.get(c.Id);
             UpdateCaseList.add(c);
          }
       }
   }
   //LM 06/18/2014: Added
   system.debug('<<<<<<<<<<<<<<<<<UpdateCaseList>>>>>>>>>>>>>>>>>>>>>' + UpdateCaseList); 
   if(UpdateCaseList.size()>0){    
     try{   
         database.Update(UpdateCaseList);
        }
     catch(DMLException e){  
        System.Debug('>>>>>>>DML Exception<<<<<' +e);
     }
   }
}

    private static void ProcessOutboundAssurantDaily (Map<Id, Order__c> newMap, Map<Id, Order__c> oldMap){

        //LM 04/04/2014 : commented below statement as it is not used anywhere in the code. 
        //Set<Case>HotTopicUpdate = new Set<Case>();
        List<Staging_PPP_Outbound__c> SOA_List= new List<Staging_PPP_Outbound__c>();
        Set<Id> orderIds = new Set<Id>();
        //LM 06/17/2014 : Updated Set type to String as Dealer Id values are string but not SFDC ID type
        //Set<Id> dealerIds = new Set<Id>();
        Set<String> dealerIds = new Set<String>();
        
        Set<String> sfdcSkuValues = new Set<String>(); 
        //LM 04/05/2014 : Added to store the External Order Numbers
        Set<String> ExtOrderNums = new Set<String>(); 
        map<Id, Order_Line__c> orderLineMap = new Map<Id, Order_Line__c>();
        map<Id, Staging_PPP_Outbound__c> soaMap = new Map<Id, Staging_PPP_Outbound__c>();
        map<String, decimal> assurantMatrixMap = new Map<String, decimal>();
        boolean exists_in_PPP_outbound = false;
        
        //LM 04/05/2014 : Added External_Order_Number__c to the SOQL
        for(Order__c o : [select Asset__r.PPP_Product_SKU__c, Id, ESP_Dealer__c, External_Order_Number__c from Order__c where id In : newMap.values()]) {
            sfdcSkuValues.add(o.Asset__r.PPP_Product_SKU__c);
            orderIds.add(o.Id);
            dealerIds.add(o.ESP_Dealer__c);
            //LM 04/05/2014 : Adding External Order Numbers to the set
            ExtOrderNums.add(o.External_Order_Number__c);
           // StgUniqueIds.add(o.Order_Record_Type__c + o.Asset__r.Serial_Number__c);
        }
       
       //LM 04/05/2014: Added to check if the order already exists in Staging PPP outbound. 
        List<Staging_PPP_Outbound__c> existsInStgoutbound = [SELECT ID, Order_Number__c,Order_Record_Type__c  FROM Staging_PPP_Outbound__c 
                                                                                 WHERE Order_Number__c IN : ExtOrderNums];

        for(Order_Line__c oli : [select Order__c
                                 from Order_Line__c 
                                 where Order__c IN : orderIds]) {
            orderLineMap.put(oli.Order__c, oli);
        }
        
        //LM 06/17/2014 : Added SFDC_SKU__c, Dealer_Id__c to the SOQL 
        for(Assurant_Matrix__c am : [select Dealer_Cost__c, Assurant_SKU__c, SFDC_SKU__c, Dealer_Id__c 
                                     from Assurant_Matrix__c 
                                     where SFDC_SKU__c = : sfdcSkuValues
                                     and Dealer_Id__c = : dealerIds]) {
            assurantMatrixMap.put(am.SFDC_SKU__c + '-' + am.Dealer_Id__c, am.Dealer_Cost__c);
        }
        List<Order__c> toBeUpdate = new List<Order__c>();
        //LM 06/17/2014 : Added asset__r.PPP_Product__r to the SOQL
        for (Order__c o : [select Total_Tax__c, Order_Type__c, Payment__r.Payment_Status__c, Asset__r.PPP_Status__c, Asset__r.Asset_Status__c, Asset__r.PPP_Contract_Number__c,
                           Bill_To__r.Address_Line_1__c, Bill_To__r.Address_Line_2__c, Bill_To__r.City__c, Bill_To__r.State__c, Bill_To__r.Country__c, Bill_To__r.X2_Letter_Country_Code__c,
                           Bill_To__r.Postal_Code__c, Consumer__r.Email, Consumer__r.FirstName, Consumer__r.lastName, Consumer__r.Phone_Unformatted__c,
                           Consumer__r.Alt_Phone_Unformatted__c,Asset__r.Purchase_Date__c,Asset__r.PPP_Purchase_Date__c,Asset__r.PPP_Start_Date__c, Asset__r.Model_Number__c,
                           Asset__r.Price__c, Asset__r.Proof_of_Purchase_Date__c, Asset__r.Serial_Number__c, Asset__r.PPP_Product_SKU__c,Case__r.Sub_Area__c, 
                           ESP_Record_Type__c, ESP_Dealer__c, Name, External_Order_Number__c, Payment_Status__c, Order_Total__c,Order_Status__c, asset__r.product__r.List_Price__c,
                           Case__c, Case__r.Hot_Topics__c, Case__r.Error_Message__c, Case__r.Vendor_Number_Name__c,Case__r.CreatedDate, Case__r.CaseNumber, Case__r.Invoice_Date__c, 
                           Case__r.Invoice_Amount__c, Asset__r.PPP_Claim_Reason__c, Asset__r.PPP_Service_Level__c, Asset__r.ESP_Cancellation_Date__c, ESP_Cancel_After_30_Days__c, ESP_Refunded_by_Dealer__c,
                           Asset__r.PPP_Product__c, Asset__r.PPP_Product__r.List_Price__c, Order_Origin_Source__c
                           from Order__c where Id IN : orderIds]){

            Order__c oldOrder = oldMap <> null ? oldMap.get(o.Id) : null;
            boolean isInsert = oldMap == null;
            // Order__c newOrder = newMap.get(o.Id);

            System.debug('==o.Payment__r.Payment_Status__c===== ' + o.Payment__r.Payment_Status__c);
            // System.debug(o.Payment_Status__c + '==o.Payment_Status__c===== ' + oldOrder.Payment_Status__c); 
            
           if(o.Order_Origin_Source__c != 'PYB' && ((!isInsert && o.Order_Type__c == 'ESP' && 
                    (o.Order_Status__c == oldOrder.Order_Status__c && (o.Payment__r.Payment_Status__c == 'Charged' || o.Payment__r.Payment_Status__c == 'Refunded')) 
                    ) || 
                    (!isInsert && (o.Asset__c <> oldOrder.Asset__c)) ||                 
                    //"If status ="Pending Cancel" create "stg o/p object", if Asset__r.PPP_Status__c<> 'Pending Cancel', otherwise asset will trigger to create duplicate one.
                    (!isInsert && o.Order_Status__c <> oldOrder.Order_Status__c && o.Order_Status__c == 'Pending Cancel'))) {
                system.debug('**Order output created :: '+o.Payment__r.Payment_Status__c+' >>'+o.Asset__c +'<>'+ oldOrder.Asset__c);

                //if(o.Order_Type__c=='ESP' && (o.Payment__r.Payment_Status__c == 'Charged' || o.Payment__r.Payment_Status__c == 'Refunded') || 
                //                     (o.Asset__r.Asset_Status__c == 'Inactive' && o.Asset__r.PPP_Contract_Number__c <> null) )
                System.debug('=========oldOrder========' + oldOrder);
                //System.debug('=========oldOrder.Order_Type__c========' + oldOrder.Order_Type__c);
                System.debug('=========o.Order_Type__c========' + o.Order_Type__c);
                boolean isCreateSPO = true;
                //"Create "stg o/p object" for Order.status ="Pending Cancel" , only if Asset__r.PPP_Status__c<> 'Pending Cancel', otherwise asset will trigger to create duplicate one.
                if(!isInsert && o.Order_Status__c != oldOrder.Order_Status__c && o.Order_Status__c == 'Pending Cancel' && (o.Asset__c==null || o.Asset__r.PPP_Status__c != 'Pending Cancel')){
                    isCreateSPO = false;
                }
                Order__c toUpdateOrd = new Order__c();
                String validationMsgs = '';
                Order_Line__c oli = orderLineMap.get(o.Id);
                
                //system.debug('<<<<<CreateSPO>>>>' + isCreateSPO + '>>>>>>>>>');
                
                if(isCreateSPO){
                    Staging_PPP_Outbound__c SOA = new Staging_PPP_Outbound__c();
                    
                    //LM 04/05/2014 : Added to check if the record already sent to Staging Outbound object                                                                                             
                    if (existsInStgoutbound <> null) {
                     for (Staging_PPP_Outbound__c outbound : existsInStgoutbound) {
                      //system.debug('<<<<<<<<<<<<<<<<<< Outbound Order Num' + outbound.Order_Number__c + '>>>>>>>>>>');
                      if(outbound.Order_Number__c == o.External_Order_Number__c){
                       if(outbound.Order_Record_Type__c == 'A' && (o.Order_Status__c == 'Pending' || o.Order_Status__c == 'Open' || o.Order_Status__c == 'Assurant Enrolled')){
                                exists_in_PPP_outbound = true;
                                break;
                            }
                       if(outbound.Order_Record_Type__c == 'C' && (o.Order_Status__c == 'Pending Cancel' || o.Order_Status__c == 'Assurant Cancelled')){
                                exists_in_PPP_outbound = true;
                                break;
                            }
                        }
                     } 
                   }                   
                   
                    //system.debug('<<<<<<<<<<<<<<<<<< exists flg' + exists_in_PPP_outbound + '>>>>>>>>>>');                    
                    // Populate the values in all the fields for respective Product, Asset, Order, OrderLine, Contact Objects
                    //LM 04/05/2014 : Added if condition to insert the Order only if it is not already exists in Staging PPP outbound.                 
                    if(!exists_in_PPP_outbound){
                    SOA.Order_Number__c = o.External_Order_Number__c;
                  
                    //LM 04/03/2014 Code clean up - This Map is not used anywhere in the code so commented the lines 
                    /*if(soaMap.containsKey(o.id)) {
                        //SOA.Id = soaMap.get(o.Id).Id;   
                    }*/
                    
                    //LM 04/01/2014 : Added substring method as the max length for Address1 field in Staging Outbound PPP is 30. It is throwing an Insert trigger exception when the address has more than 30 chars. 
                    if (o.Bill_To__r.Address_Line_1__c <> null && (o.Bill_To__r.Address_Line_1__c).length() > 30 ) {
                        SOA.Billing_Address_1__c = (o.Bill_To__r.Address_Line_1__c).substring(0,30);
                    }
                    else {
                        SOA.Billing_Address_1__c = o.Bill_To__r.Address_Line_1__c;
                    }
                              
                    SOA.Billing_Address_2__c = o.Bill_To__r.Address_Line_2__c;
                    SOA.Billing_City__c = o.Bill_To__r.City__c;
                    SOA.Billing_State__c = o.Bill_To__r.State__c;
                    SOA.State_of_Sale__c = o.Bill_To__r.State__c;
                    
                    //LM 04/02/2014 : transforming Canada country code to 'CN' as Assurant can only accept 'CN' value for canadian transactions.
                    
                    if(o.Bill_To__r.X2_Letter_Country_Code__c <> null) {
                      if(o.Bill_To__r.X2_Letter_Country_Code__c == 'CA' || (o.Bill_To__r.Country__c.equalsIgnoreCase('Canada')) || (o.Bill_To__r.Country__c.equalsIgnoreCase('CAN'))) {
                        SOA.Billing_Country__c = 'CN';
                        SOA.Country_of_Sale__c = 'CN';
                      }
                      else { 
                        SOA.Billing_Country__c =  o.Bill_To__r.X2_Letter_Country_Code__c;
                        SOA.Country_of_Sale__c =  o.Bill_To__r.X2_Letter_Country_Code__c;
                      }  
                   } 
                   else {
                        SOA.Billing_Country__c = '';
                        SOA.Country_of_Sale__c = '';
                   } 
                    
                    //LM 04/02/2014 : formatting Zipcode to 6 chars as assurant can only accept 6 chars for canadian postal codes
                    if(o.Bill_To__r.Postal_Code__c <> null) {    
                    system.debug('<<<<<<<<<<<<<<<<<< Postal code before = ' + o.Bill_To__r.Postal_Code__c +'>>>>>>>>>>>>');              
                    SOA.Billing_Zip_Postal_Code__c = (o.Bill_To__r.Postal_Code__c.trim()).indexOf(' ',0) > -1 ? (o.Bill_To__r.Postal_Code__c.trim()).replaceAll('(\\s+)', '') : o.Bill_To__r.Postal_Code__c.trim();
                    SOA.Zip_Code_of_Sale__c = (o.Bill_To__r.Postal_Code__c.trim()).indexOf(' ',0) > -1 ? (o.Bill_To__r.Postal_Code__c.trim()).replaceAll('(\\s+)', '') : o.Bill_To__r.Postal_Code__c.trim();
                    //system.debug('<<<<<<<<<<<<<<<<<< Postal code after = ' + SOA.Billing_Zip_Postal_Code__c +'>>>>>>>>>>>>');
                    }
                    else {
                    SOA.Billing_Zip_Postal_Code__c = '';
                    SOA.Zip_Code_of_Sale__c = '';   
                    }                  
                    
                    //LM 02/27/2015 : Updated to remove all the special characters from Consumer Last name and First name fields
                    //Consumer First Name check                  
                    if (o.Consumer__r.FirstName <> null && o.Consumer__r.FirstName.replaceAll('[^a-zA-Z0-9 ]', '') <> null && o.Consumer__r.FirstName.replaceAll('[^a-zA-Z0-9 ]', '') <> '') {
                        if(o.Consumer__r.FirstName.replaceAll('[^a-zA-Z0-9 ]', '').length() > 15 ) {
                              SOA.Consumer_First_Name__c = o.Consumer__r.FirstName.replaceAll('[^a-zA-Z0-9 ]', '').substring(0,15);
                        } else {
                        	  SOA.Consumer_First_Name__c = o.Consumer__r.FirstName.replaceAll('[^a-zA-Z0-9 ]', '');
                        }      
                    } else {
                        SOA.Consumer_First_Name__c = null;
                    }
                    
                    // Consumer Last Name check                    
                    if (o.Consumer__r.LastName <> null && o.Consumer__r.LastName.replaceAll('[^a-zA-Z0-9 ]', '') <> null && o.Consumer__r.LastName.replaceAll('[^a-zA-Z0-9 ]', '') <> '' ) {
                        if(o.Consumer__r.LastName.replaceAll('[^a-zA-Z0-9 ]', '').length() > 15) {
                           SOA.Consumer_Last_Name__c = o.Consumer__r.LastName.replaceAll('[^a-zA-Z0-9 ]', '').substring(0,15);
                        } else {
                           SOA.Consumer_Last_Name__c =	o.Consumer__r.LastName.replaceAll('[^a-zA-Z0-9 ]', '');
                        }   
                    } else {
                        SOA.Consumer_Last_Name__c = null;
                    }
                    
                    SOA.Consumer_Email_Address__c = o.Consumer__r.Email;
                    
                    //LM 06/17/2014 : Added if condition to prevent any errors in future incase if Order set is null.
                    SOA.Order__c= o <> null ? o.Id : null;

                    if(o.Consumer__r.Alt_Phone_Unformatted__c <> null ) {
                        SOA.Consumer_Area_Code_2__c = o.Consumer__r.Alt_Phone_Unformatted__c.length() > 3 ? o.Consumer__r.Alt_Phone_Unformatted__c.subString(0,3) : '';
                        SOA.Consumer_Phone_Number_2__c = o.Consumer__r.Alt_Phone_Unformatted__c.length() > =10 ? o.Consumer__r.Alt_Phone_Unformatted__c.subString(3,10) : '';
                    }
                    if(o.Consumer__r.Phone_Unformatted__c <> null ) {
                        SOA.Consumer_Area_Code_1__c = o.Consumer__r.Phone_Unformatted__c.length() > 3 ? o.Consumer__r.Phone_Unformatted__c.subString(0,3) : '';
                        SOA.Consumer_Phone_Number_1__c = o.Consumer__r.Phone_Unformatted__c.length() >= 10 ? o.Consumer__r.Phone_Unformatted__c.subString(3,10) : '';
                    }

                    boolean isUpdatedStatus = false;
                    SOA.Unit_Warranty_Duration__c = '12';
                    SOA.Manufacture_Code__c = 'SONY';
                    //System.debug('o.Payment_Status__c '+isInsert +'<>' +o.Payment__r.Payment_Status__c);
                    if(!isInsert && o.Asset__c <> oldOrder.Asset__c){
                        SOA.Order_Record_Type__c = 'U';
                        isUpdatedStatus = true;
                    } else if(!isInsert && o.Payment__r.Payment_Status__c == 'Charged'){
                        //System.debug('o.Payment_Status__c >>: '+oldOrder+'<>' +o.Payment_Status__c <> oldOrder.Payment_Status__c);
                        if(oldOrder==null || o.Payment_Status__c <> oldOrder.Payment_Status__c){ 
                            SOA.Order_Record_Type__c = 'A';
                            isUpdatedStatus = true;
                            //System.debug('o.Payment_Status__c >>::: '+SOA.Order_Record_Type__c);
                        }
                    } 
                    
                    if(!isUpdatedStatus && !isInsert && o.Payment__r.Payment_Status__c == 'Refunded'){
                        SOA.Order_Record_Type__c = 'C';
                        isUpdatedStatus = true;
                    }
                    if(!isUpdatedStatus && o.Payment__r.Payment_Status__c == 'Charged'){
                        SOA.Order_Record_Type__c = 'A';
                        isUpdatedStatus = true;
                    }else if(!isUpdatedStatus && o.Order_Status__c == 'Pending Cancel') {
                        SOA.Order_Record_Type__c = 'C';
                    }

                    System.debug('======o.Asset__r.PPP_Purchase_Date__c=======' + o.Asset__r.PPP_Purchase_Date__c);
                    SOA.PPP_Purchase_Date__c = formatDate(o.Asset__r.PPP_Purchase_Date__c);
                    System.debug('=====SOA.PPP_Purchase_Date__c=======' + SOA.PPP_Purchase_Date__c);

                    SOA.Purchase_Date__c =  formatDate(o.Asset__r.Purchase_Date__c);
                    SOA.Proof_of_Purchase_Date__c =  formatDate(o.Asset__r.Proof_of_Purchase_Date__c);
                    System.debug('=====SOA.Proof_of_Purchase_Date__c=======' + SOA.Purchase_Date__c);
                    SOA.PPP_Start_Date__c = formatDate(o.Asset__r.PPP_Start_Date__c);
                    
                    if(o.Asset__r.PPP_Purchase_Date__c != null && o.Asset__r.Proof_of_Purchase_Date__c != null &&
                            (o.Asset__r.Proof_of_Purchase_Date__c.daysBetween( o.Asset__r.PPP_Purchase_Date__c))>30)
                        SOA.Order_Dealer__c = 'SCEB';//o.ESP_ealer__c;
                    else 
                        SOA.Order_Dealer__c = 'SCEA';
                        
                    SOA.PPP_SKU__c = o.Asset__r.PPP_Product_SKU__c;
                    SOA.PPP_Cancel_After_30_Days__c = o<> null ? String.valueOf(o.ESP_Cancel_After_30_Days__c) : '';
                    //LM 06/16/2014 : Update below statement as oli list is not used to set the value of the field
                    //SOA.PPP_Tax__c = oli <> null ? String.valueOf(o.Total_Tax__c) : '';
                    SOA.PPP_Tax__c = o <> null ? String.valueOf(o.Total_Tax__c) : '';
                    
                    SOA.PPP_Refund_by_Dealer__c = o<> null ? String.valueOf(o.ESP_Refunded_by_Dealer__c) : '';
                    SOA.PPP_Contract_Number__c = o.Asset__r.PPP_Contract_Number__c;
                    //LM 06/17/2014 - SF-1026 - Changed the statement to get the price from the Product table as we should not send discounted price to Assurant
                    //SOA.PPP_List_Price__c = String.valueOf(o.Order_Total__c);
                    if(o.asset__r.PPP_Product__r <> null){
                    SOA.PPP_List_Price__c = o.asset__r.PPP_Product__r.List_Price__c == null ? '0.00' : ''+o.asset__r.PPP_Product__r.List_Price__c;
                    }else {
                    SOA.PPP_List_Price__c = String.ValueOf(0.00);   
                    }
                      
                    SOA.Unit_Model_Number__c = o.Asset__r.Model_Number__c;

                    if(o.asset__r.product__r <> null){
                        SOA.Unit_Price__c = o.asset__r.product__r.List_Price__c == null ? '0' : ''+o.asset__r.product__r.List_Price__c;    
                    }
                    SOA.Unit_Serial_Number__c = o.Asset__r.Serial_Number__c;
                    
                    //LM 04/09/2014 : JIRA# SF-464 - Formatting the PPP Cancel date
                    //SOA.PPP_Cancel_Date__c= String.valueOf(o.Asset__r.ESP_Cancellation_Date__c);
                    SOA.PPP_Cancel_Date__c = formatDate(o.Asset__r.ESP_Cancellation_Date__c);
                    
                    //SOA.Vender_Number_Name__c = o.Case__r.Vender_Number_Name__c;
                    SOA.Loss_Date__c = o.Case__c == null ? '' : formatDate(o.Case__r.CreatedDate.date());
                    SOA.Invoice_Number__c = o.Case__r.CaseNumber; 
                    SOA.Invoice_Date__c = formatDate(o.Case__r.Invoice_Date__c);
                    SOA.Invoice_Amount__c = String.valueOf(o.Case__r.Invoice_Amount__c);
                    //UV : Fix-Claim Reason field Mapping (T-239028)
                    //SOA.PPP_Claim_Reason__c = o.Asset__r.PPP_Claim_Reason__c;
                    if(o.Case__c<> null && o.Case__r.Sub_Area__c <> null && o.Case__r.Sub_Area__c.length() > 30) {
                        SOA.PPP_Claim_Reason__c = o.Case__r.Sub_Area__c.subString(0,30);
                    } else if(o.Case__c<> null) {
                        SOA.PPP_Claim_Reason__c = o.Case__r.Sub_Area__c;
                    }
                    //SOA.PPP_Service_Level__c = o.Asset__r.PPP_Service_Level__c;

                    decimal dealerCost = assurantMatrixMap.get(SOA.PPP_SKU__c + '-' + SOA.Order_Dealer__c);
                    SOA.PPP_Dealer_Cost__c = dealerCost == null ? '' : String.valueOf(dealerCost);

                    SOA.Unique_Key__c= SOA.Order_Record_Type__c + o.Asset__r.Serial_Number__c;
                    validationMsgs = validateStagingAssurant(SOA);
                    if(String.isEMpty(validationMsgs )) {
                        toUpdateOrd.Error_Message__c = null;
                        SOA_List.add(SOA);
                        //SOA.Invalid_Record__c = false;
                        //SOA.Error_Message__c = null;
                    }
                    toUpdateOrd.ESP_Record_Type__c = SOA.Order_Record_Type__c;
                    System.debug(validationMsgs + '========SOA====' + SOA);
                 
                    }//End if exists_in_PPP_outbound 
                     
                }

                System.debug('*******************validationMsgs>>>'+validationMsgs);
                if(!String.isEMpty(validationMsgs )) {
                    toUpdateOrd.Error_Message__c =  validationMsgs;
                    //newMap.get(o.Id).addError(validationMsgs);

                    //SOA.Invalid_Record__c = true;
                    //SOA.Error_Message__c = validationMsgs;
                }
                toUpdateOrd.Id = o.Id;
                toUpdateOrd.Asset__c = o.Asset__c;
                toUpdateOrd.order_Status__c = o.Order_Status__c;

                toBeUpdate.add(toUpdateOrd);

                if(o.Case__c!=null && validationMsgs!=o.Case__r.Error_Message__c){
                    Case objCase = new Case(id=o.Case__c);
                    objCase.Error_Message__c = validationMsgs;
                    //HotTopicUpdate.add(objCase);
                }
                System.debug(SOA_list.size() + '========SOA_list====' + SOA_list);
                //SOA_List.add(SOA);
            }
        }
        // System.debug('toBeUpdate>>>'+toBeUpdate);
        //System.debug('SOA_list>>>'+SOA_list);
        if(toBeUpdate.size() > 0){
            processOutbound = false;
            update toBeUpdate;
        }
        list<Asset__c> astList = new list<Asset__c>();
        for(Order__c ordr : toBeUpdate)  {
            Asset__c ast = new Asset__c();
            if(ordr.Asset__c <> null) {
                ast.Id = ordr.Asset__c;
                if(ordr.Error_Message__c == null && ordr.Order_Status__c == 'Pending Cancel') {
                    ast.PPP_Status__c = ordr.Order_Status__c;
                    astList.add(ast);
                }
            }
        }
        if(!astList.isEmpty()) update astList;

        if(SOA_List.size()>0){

            try{
                system.debug('***********About to Insert:'+SOA_list);
                insert SOA_list;
                system.debug('***********Done to Insert:'+SOA_list);
                //upsert SOA_list Unique_Key__c;
            } 

            catch(Exception e){
                System.debug('Error While inserting/updating Staging_PPP_Outbound__c***********' + e.getMessage());
            }
        }

        // Commented by Charu

        /*if(HotTopicUpdate.size()>0){
        upsert new List<Case>(HotTopicUpdate);
       } */
    }

    public static void createServiceOutboundStatgingRecords(Map<Id, Order__c> newMap, Map<Id, Order__c> oldMap) {
        String MTCFUL = GeneralUtiltyClass.MTCFUL;
        String MTCFWFUL = GeneralUtiltyClass.MTCFWFUL;
        String FIRST_PARTY_PERIPHERAL = GeneralUtiltyClass.FIRST_PARTY_PERIPHERAL;
        String FIRST_PARTY_SOFTWARE = GeneralUtiltyClass.FIRST_PARTY_SOFTWARE;
        String PEROMOTIONAL = GeneralUtiltyClass.PEROMOTIONAL;
        map<Id, String> oliQuantityMap = new map<id, String>();
        list<Staging_Service_Outbound__c> SSO_List = new list<Staging_Service_Outbound__c>();
        for(Order_Line__c oli : [select Quantity__c, Order__c from Order_Line__c where Order__c IN :newMap.values()]) {
            oliQuantityMap.put(oli.Order__c, oli.Quantity__c);
        }

        for(Order__c ordr : [select Id, Case__c, Order_Status__c, Order_Type__c, Case__r.SCEA_Product__r.Product_Type__c, Case__r.Owner.Name, Case__r.Approved_Reason__c, 
                             Case__r.SCEA_Product__r.SKU__c, Consumer__r.FirstName, Consumer__r.LastName, Consumer__r.Email, Ship_To__r.Address_Line_1__c, 
                             Ship_To__r.Address_Line_2__c, Ship_To__r.City__c, Ship_To__r.State__c, Ship_To__r.Postal_Code__c 
                             from Order__c where Id IN : newMap.values()]) {
            Order__c oldOrder = oldMap.get(ordr.Id);
            Case cs = ordr.Case__r;             
            /*if( (ordr.Order_Status__c <> oldOrder.Order_Status__c || ordr.Order_Type__c <> oldOrder.Order_Type__c ) &&
                    (ordr.Order_Status__c == 'Open' || ordr.Order_Status__c == null) &&
                    (cs.SCEA_Product__r.Product_Type__c == FIRST_PARTY_PERIPHERAL ||  cs.SCEA_Product__r.Product_Type__c == FIRST_PARTY_SOFTWARE) && 
                     ordr.Order_Type__c == PEROMOTIONAL) {*/
            if(GeneralUtiltyClass.isValidPromoOrder(ordr, oldOrder, cs)){
                Staging_Service_Outbound__c sso = GeneralUtiltyClass.createStagingServiceOutbound(null, ordr.Consumer__r, cs.SCEA_Product__r, null, ordr.Ship_To__r, null);

                sso.Order_Number__c = ordr.External_Order_Number__c;                
                sso.Order_Type__c = ordr.Order_Type__c;

                sso.Case_Owner__c = cs.Owner.Name;              
                sso.Case_Approved_By__c = cs.Owner.Name;
                sso.Case_Approved_Reason__c = cs.Approved_Reason__c; 
                sso.Order_Shipping_Priority__c = 'Ship - Home Delivery';

                sso.Order_Quantity__c = oliQuantityMap.get(ordr.Id);

                if(ordr.Ship_To__r.Country__c == 'CANADA' && cs.Owner.Name == MTCFUL) {
                    sso.Batch_Type__c = 'MTC - Promo Order';
                    SSO_List.add(sso);
                } else if (ordr.Ship_To__r.Country__c == 'USA' && cs.Owner.Name == MTCFWFUL) { //Criteria 2: MTCFW - Fulfilment Owner - Promo Order
                    sso.Batch_Type__c = 'MTCFW - Promo Order';
                    SSO_List.add(sso);
                }
            }
        }

        if(!SSO_List.isEmpty()) insert SSO_List;
    }

    public static String validateStagingAssurant(Staging_PPP_Outbound__c SOA) {
        String errorMessages = '';
        boolean invalidRecord = false;
        if(SOA.Unit_Model_Number__c == null || SOA.Unit_Model_Number__c.trim() =='') {
            invalidRecord = true;
            errorMessages += '\n Unit Model Number cannot be Null';
        }

        if(SOA.Unit_Price__c == null || SOA.Unit_Price__c.trim() =='') {
            invalidRecord = true;
            errorMessages += '\n Unit Price cannot be Null';
        }

        if(SOA.Unit_Serial_Number__c == null || SOA.Unit_Serial_Number__c.trim() =='') {
            invalidRecord = true;
            errorMessages += '\n Unit Serial Number cannot be Null';
        }
        
        //LM 06/17/2014 : SF-1026 - Added Validation. PPP Price should not be 0.00 or null. 
         if(SOA.PPP_List_Price__c == null || SOA.PPP_List_Price__c == '0.00' || SOA.PPP_List_Price__c == '') {
            invalidRecord = true;
            errorMessages += '\n PPP Product Price can not be Blank or Zero.';
        }

        //LM 06/02/2014 - Commented below validation as it is not required. We are sending Proof of Purchase date to Assurant.
        //This validation check is unnecessarily failing the record to reach Staging PPP outbound
        /*if(ValidationRulesUtility.validateDate(SOA.Purchase_Date__c, 'Purchase Date') != 'Success') {
            invalidRecord = true;
            errorMessages += '\n ' + ValidationRulesUtility.validateDate(SOA.Purchase_Date__c, 'Purchase Date');
        }*/

        if(ValidationRulesUtility.validateDate(SOA.PPP_Purchase_Date__c, 'PPP Purchase Date') != 'Success') {
            invalidRecord = true;
            errorMessages += '\n ' + ValidationRulesUtility.validateDate(SOA.PPP_Purchase_Date__c, 'PPP Purchase Date');
        }


        if(ValidationRulesUtility.validateDate(SOA.Proof_of_Purchase_Date__c, 'Proof Purchase Date') != 'Success') {
            invalidRecord = true;
            errorMessages += '\n ' + ValidationRulesUtility.validateDate(SOA.Proof_of_Purchase_Date__c, 'Proof Purchase Date');
        }

        if(ValidationRulesUtility.validateDate(SOA.PPP_Start_Date__c, 'ESP Start Date') != 'Success') {
            invalidRecord = true;
            errorMessages += '\n ' + ValidationRulesUtility.validateDate(SOA.PPP_Start_Date__c, 'ESP Start Date');
        }

       /* if(ValidationRulesUtility.validateCharcters(SOA.Consumer_First_Name__c, 'First Name') != 'Success') {
            invalidRecord = true;
            errorMessages += '\n ' + ValidationRulesUtility.validateCharcters(SOA.Consumer_First_Name__c, 'First Name');
        }

        if(ValidationRulesUtility.validateCharcters(SOA.Consumer_Last_Name__c, 'Last Name') != 'Success') {
            invalidRecord = true;
            errorMessages += '\n ' + ValidationRulesUtility.validateCharcters(SOA.Consumer_Last_Name__c, 'Last Name');
        }
        */
        
        //LM 02/27/2015 : Updated Last name and First name validations. Instead of rejecting the record, remove the special characters and process the record into Assurant Outbound
        system.debug('<<<<<<<<Consumer Last Name>>>>>>>>>>>>>'+SOA.Consumer_Last_Name__c);
        system.debug('<<<<<<<<Consumer First Name>>>>>>>>>>>>'+SOA.Consumer_First_Name__c);
        
        if(SOA.Consumer_Last_Name__c == null) {
             invalidRecord = true;
             errorMessages += '\n Consumer Last Name cannot be blank';
        }
       
        if(SOA.Consumer_First_Name__c == null) {
            invalidRecord = true;
            errorMessages += '\n Consumer First Name cannot be blank';
        }

        if(SOA.Billing_Address_1__c == null || SOA.Billing_Address_1__c.trim() == '') {
            invalidRecord = true;
            errorMessages += '\n Billing Address Line 1 cannot be null/blank';
        }
        /*
         if(SOA.Billing_Address_2__c == null || SOA.Billing_Address_2__c.trim() == '') {
                invalidRecord = true;
                errorMessages += '\n Billing Address Line 2 cannot be null/blank';
         }*/

        //LM 04/09/2014 : JIRA# SF-481 - removed validations for city - commented the actual code
                
        /*if(ValidationRulesUtility.validateCharcters(SOA.Billing_City__c, 'Billing City') != 'Success') {
            invalidRecord = true;
            errorMessages += '\n ' + ValidationRulesUtility.validateCharcters(SOA.Billing_City__c, 'Billing City');
        }*/

        if(ValidationRulesUtility.validateCharcters(SOA.Billing_State__c, 'Billing State') != 'Success') {
            invalidRecord = true;
            errorMessages += '\n ' + ValidationRulesUtility.validateCharcters(SOA.Billing_State__c, 'Billing State');
        }

        if(ValidationRulesUtility.validateCharcters(SOA.Billing_Country__c, 'Billing Country') != 'Success') {
            invalidRecord = true;
            errorMessages += '\n ' + ValidationRulesUtility.validateCharcters(SOA.Billing_Country__c, 'Billing Country');
        }

        /*if(ValidationRulesUtility.validateNumeric(SOA.Billing_Zip_Postal_Code__c, 'Zip/Postal Code') != 'Success') {
            invalidRecord = true;
            errorMessages += '\n ' + ValidationRulesUtility.validateNumeric(SOA.Billing_Zip_Postal_Code__c, 'Zip/Postal Code');
        } */

        if(ValidationRulesUtility.isEligibleForEspPurchase(SOA.Billing_State__c)) {
            invalidRecord = true;
            errorMessages += '\n Your state is not validate for ESP Purchase.';
        }

        if(invalidRecord) {
            return errorMessages;
        }
        return null;
    }

    private static String formatDate(Date dt) {
        if(dt == null) return '';
        String month = dt.month() < 10 ? '0' + dt.month() : '' + dt.month();
        String day = dt.day() < 10 ? '0' + dt.day() : '' + dt.day();
        return '' + month  + day + dt.year();
    }
}