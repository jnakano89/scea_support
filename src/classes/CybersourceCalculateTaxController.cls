/*****************************************************************************************************************************************
Class Name  : CybersourceCalculateTaxController
Description : Apex class which calculates tax by calling api and if user agrees then redirect to payment page.
Created By  : Urminder Vohra (JDC)
Task        : T-166676

Updates     :
            : 07/02/2014 : Leena Mandadapu : Jira# SF-1108 - Added logic for PPP Tax calculation
****************************************************************************************************************************************/
public class CybersourceCalculateTaxController {
    public String caseId {get;set;}
    
    //LM 07/01/2014 : Added
    public static String casePurchaseRecordTypeId = GeneralUtiltyClass.RT_PURCHASE_ID;
    public static String caseServiceRecordTypeId = GeneralUtiltyClass.RT_SERVICE_ID;
    
    public CyberSourceUtility.TaxWrapper taxWrap{get;set;}
    public decimal unitPrice{get;set;}
    public decimal totalAmount{get;set;}
    public boolean isUpdated{get;set;}
    
    final String OOW = 'Out Of Warranty';
    final String ACCEPT = 'ACCEPT';
    
    Case selectedCase;
    
    public CybersourceCalculateTaxController(ApexPages.StandardController sc) {
        caseId = sc.getId();
        isUpdated = false;
        
        for(Case cs : [select Fee_Type__c, IsRMAOrigin__c, Asset__c, Asset__r.Product_SKU__c, Unit_Price__c,  Ship_To__r.Country__c, Trade_In__c, Total_Tax__c, Ship_To__r.State__c, SCEA_Product__c, RecordTypeId 
                        from  Case where Id = :CaseId]) {
            selectedCase = cs;
                        system.debug('*************: '+cs.Fee_Type__c+'.'+cs.Asset__c+' > '+cs.Ship_To__r.Country__c);
            if(cs.Fee_Type__c == OOW && cs.Asset__c <> null && cs.Ship_To__r.Country__c <> null) {                          
                unitPrice = getUnitPrice(cs.Trade_In__c);
            }    
             
            //LM 07/01/2014 : Added if condition
            system.debug('+++++++++++++RecordsType'+casePurchaseRecordTypeId + 'Case='+cs.RecordTypeId);
            if(cs.RecordTypeId == casePurchaseRecordTypeId) {                          
                unitPrice = getPPPListPrice(cs.SCEA_Product__c);   
            }
            system.debug('Unit Price' + unitPrice );
        }
    }
    
    public Pagereference udpateTaxInCase() {
    	
    	//LM 07/01/2014 : added
    	Tax_Nexus__c nexusvalues = new Tax_Nexus__c();
    	for(Tax_Nexus__c tn : [select State_Code__c, PPP_Tax__c, Service_Tax__c from Tax_Nexus__c where State_code__c =:selectedCase.Ship_To__r.State__c Limit 1]) {
           system.debug('Tax Nexus Values ' + tn );    	
    	   if(tn <> null){
    		  nexusvalues = tn;
    	   }	
    	   system.debug('Nexus Values ' + nexusvalues );
    	}

        if(selectedCase.IsRMAOrigin__c !=true){
        	//LM 07/01/2014 : Commented
            //Set<String> nexusStates = CyberSourceUtility.getNexusStates();
            system.debug(selectedCase.Ship_To__r.State__c+'< ======== >' + unitPrice);
            system.debug('Unit Price' + unitPrice );
            system.debug('selected Case Unit Price' + selectedCase.Unit_Price__c );
               
            if(unitPrice <> null && selectedCase.Unit_Price__c != unitPrice){
               // LM 07/01/2014 : Updated if condition to check for PPP/Service Tax eligibility
               // if(nexusStates.contains(selectedCase.Ship_To__r.State__c)) {
                  system.debug('Tax Nexus State Code' + nexusvalues.State_Code__c );
                  system.debug('Case State Code' + selectedCase.Ship_To__r.State__c );
                  system.debug('PPP Tax Flag' + nexusvalues.PPP_Tax__c  + 'Service Tax Flag' + nexusvalues.Service_Tax__c);
                  if(nexusvalues <> null && nexusvalues.State_Code__c <> null && nexusvalues.State_Code__c.equals(selectedCase.Ship_To__r.State__c)) {
                  	system.debug('Inside Ship to State String check' + nexusvalues.State_Code__c.equals(selectedCase.Ship_To__r.State__c));
                  	if((selectedCase.RecordTypeId == casePurchaseRecordTypeId && nexusvalues.PPP_Tax__c) || (selectedCase.RecordTypeId == caseServiceRecordTypeId && nexusvalues.Service_Tax__c)) {
                    /*
                    //commented code for using new API framework.
                    String taxServiceResponse = CyberSourceUtility.taxCalculation(caseId);
                    system.debug('***************===Response :: '+taxServiceResponse);
                    taxWrap = CyberSourceUtility.parseTaxResponse(taxServiceResponse);
                    */ 
                    taxWrap = CyberSourceUtility.taxCalculation(caseId, unitPrice); 
                    System.debug('bleboff');
                    System.debug('Tax Wrap>>>>>>>>>'+taxwrap);

                    if(taxWrap.decision == ACCEPT ) { 
                        selectedCase.Unit_Price__c = unitPrice;
                        selectedCase.City_Tax__c = decimal.valueOf(taxWrap.totalCityTaxAmount);
                        //selectedCase.City_Tax__c = decimal.valueOf(taxWrap.totalCityTaxAmount);
                        selectedCase.County_Tax__c = decimal.valueOf(taxWrap.totalCountyTaxAmount);
                        selectedCase.District_Tax__c = decimal.valueOf(taxWrap.totalDistrictTaxAmount);
                        selectedCase.City_Tax__c = decimal.valueOf(taxWrap.totalCityTaxAmount);
                        selectedCase.State_Tax__c = decimal.valueOf(taxWrap.totalStateTaxAmount);
                        selectedCase.Total_Tax__c = decimal.valueOf(taxWrap.totalTaxAmount);
                        selectedCase.Total_Amount_to_Pay__c = unitPrice + decimal.valueOf(taxWrap.totalTaxAmount);      
                    }                   
                   }
                   //LM 07/02/2014: Added
                   else{
                    selectedCase.Unit_Price__c = unitPrice;
                    selectedCase.Total_Amount_to_Pay__c = unitPrice;
                   }
                }
               
                //LM 07/02/2014 : Commented
                else{
                    selectedCase.Unit_Price__c = unitPrice;
                    selectedCase.Total_Amount_to_Pay__c = unitPrice;
                }
                try {
                    //make sure we updated unit price, otherwise we loop
                    if(unitPrice == selectedCase.Unit_Price__c){
                        update selectedCase;
                        isUpdated = true;
                    }

                } catch(Exception ex) {
                    System.debug('=====Update case failed due to=========' + ex.getMessage());
                }
            }

        }
        
        // Code for CANADA Charu
            /*if(unitPrice <> null && (selectedCase.Ship_To__r.Country__c=='Canada' || selectedCase.Ship_To__r.Country__c=='CANADA') && selectedCase.Unit_Price__c != unitPrice){
                  isUpdated = false;
                  selectedCase.Unit_Price__c = unitPrice;
                  selectedCase.Total_Tax__c =0;
                  selectedCase.Total_Amount_to_Pay__c = unitPrice;
                  System.debug('=================updated case ======' + selectedCase);
                   try {
                        update selectedCase;
                        isUpdated = true;
                    } catch(Exception ex) {
                        System.debug('=====Update case failed due to=========' + ex.getMessage());
                    }
                    
                    
                }  */
        return null; 
    }
    private decimal getUnitPrice(boolean isTradeIn) {
        
        System.debug('==============' + selectedCase.Asset__r.Product_SKU__c);
        System.debug('==============' + selectedCase.Ship_To__r.Country__c);
        for(Product__c prod : [select List_Price__c , Trade_In_Price__c 
                                from Product__c 
                                where SKU__c = : selectedCase.Asset__r.Product_SKU__c
                                //AND Exchange_Country__c = :selectedCase.Ship_To__r.Country__c
                                AND Sub_Type__c = 'Service']) {
            if(isTradeIn) { 
                return prod.Trade_In_Price__c;
                
            } else {
                return prod.List_Price__c;
            }
        }
        return 0; 
    }
    
    //LM 07/01/2014 : added below method
    private decimal getPPPListPrice(String PPPPrdId) {
        for(Product__c prod : [select List_Price__c from Product__c where Id = : PPPPrdId]) {
           if(prod <> null) {   
             return prod.List_Price__c;
           }     
        }
        return 0; 
    }
    
    public Pagereference validateRedirect() {
        String proceedToPayment = Apexpages.currentPage().getParameters().get('proceedToPayment');
        if(proceedToPayment<> null && proceedToPayment == 'true') {
            Pagereference paymentPage = Page.CyberSourcePayment;
            paymentPage.getParameters().put('caseId', caseId);
            paymentPage.getParameters().put('OrderType', 'ESP');
            paymentPage.getParameters().put('TransactionType', 'authorization,create_payment_token');
            
            String purchaseType = Apexpages.currentPage().getParameters().get('purchaseType');
            paymentPage.getParameters().put('purchaseType', purchaseType);
            
            String cashPayment = Apexpages.currentPage().getParameters().get('cashPayment');
            if(cashPayment <> null) {
                paymentPage.getParameters().put('cashPayment', cashPayment);
            }
            
            return paymentPage;
            
        }
        return null;
    }
    
}