public with sharing class CustomSettingLoader {
	
	//Checking whether current User is listed in Data Migration Custom Setting. Identifying Data Migration Operation
	public static boolean DATA_MIGRATION_SETTING = (DataMigrationSettings__c.getInstance(UserInfo.getUserId())).disableTrigger__c;
}