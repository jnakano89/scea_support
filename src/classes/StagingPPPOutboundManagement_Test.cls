@isTest
private class StagingPPPOutboundManagement_Test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
       Product__c prod = TestClassUtility.createProduct(1, false)[0];
       prod.List_Price__c = 100;
       prod.Product_Type__c = 'ESP';
       insert prod;
       
       Assurant_Matrix__c am = new Assurant_Matrix__c();
       am.SFDC_SKU__c = prod.Sku__c;
       am.Assurant_SKU__c = prod.Sku__c;
       am.Channel__c = 'POS';
       am.Dealer_ID__c = 'SCEA';
       insert am;
       
       Assurant_Matrix__c am1 = new Assurant_Matrix__c();
       am1.SFDC_SKU__c = prod.Sku__c;
       am1.Assurant_SKU__c = prod.Sku__c;
       am1.Channel__c = 'MPOS';
       am1.Dealer_ID__c = 'SCEB';
       insert am1;
       
       Assurant_Matrix__c am2 = new Assurant_Matrix__c();
       am2.SFDC_SKU__c = prod.Sku__c;
       am2.Assurant_SKU__c = prod.Sku__c;
       am2.Channel__c = 'POS';
       am2.Dealer_ID__c = 'SPSN';
       insert am2;

		Asset__c ast = TestClassUtility.createAssets(1, false)[0];
		ast.PPP_Product__c = prod.Id;
		ast.PPP_Purchase_Date__c = Date.today().addDays(-32);
		ast.Purchase_Date__c = Date.today();
		insert ast;
		
		Asset__c ast1 = TestClassUtility.createAssets(1, false)[0];
		ast1.PPP_Product__c = prod.Id;
		ast1.PPP_Purchase_Date__c = Date.today().addDays(32);
		ast1.Purchase_Date__c = Date.today();
		insert ast1;

		Address__c shipTo = testClassUtility.createAddress(1,true)[0];
		Address__c shipToCAN = testClassUtility.createAddress(1,false)[0];
		shipToCAN.Country__c  = 'Canada';
		insert shipToCAN;
		
		//US POS Order
        Order__c ordr = TestClassUtility.creatOrder(null, 1, false)[0];
   		ordr.Asset__c = ast.Id;
   		ordr.Order_Origin_Source__c = 'PS';
   		ordr.Ship_To__c = shipTo.Id;
   		ordr.Bill_To__c = shipTo.Id;
   		insert ordr;
   		
   		//US MPOS Order
   		Order__c ordr1 = TestClassUtility.creatOrder(null, 1, false)[0];
   		ordr1.Asset__c = ast1.Id;
   		ordr1.Order_Origin_Source__c = 'PS';
   		ordr1.Ship_To__c = shipTo.Id;
   		ordr1.Bill_To__c = shipTo.Id;
   		insert ordr1;
   		
   		//Canada POS Order
   		Order__c ordrPOSCAN = TestClassUtility.creatOrder(null, 1, false)[0];
   		ordrPOSCAN.Asset__c = ast.Id;
   		ordrPOSCAN.Order_Origin_Source__c = 'PS';
   		ordrPOSCAN.Ship_To__c = shipToCAN.Id;
   		ordrPOSCAN.Bill_To__c = shipToCAN.Id;
   		insert ordrPOSCAN;
   		
   		//Canada MPOS Order
   		Order__c ordrMPOSCAN = TestClassUtility.creatOrder(null, 1, false)[0];
   		ordrPOSCAN.Asset__c = ast1.Id;
   		ordrPOSCAN.Order_Origin_Source__c = 'PS';
   		ordrPOSCAN.Ship_To__c = shipToCAN.Id;
   		ordrPOSCAN.Bill_To__c = shipToCAN.Id;
   		insert ordrMPOSCAN;
       
       //US POS Order
       Staging_PPP_Outbound__c outbound = new Staging_PPP_Outbound__c();
       outbound.PPP_Cancel_After_30_Days__c  = 'true';
       outbound.Order_Record_Type__c = 'C';
       outbound.Order__c = ordr.Id;
       outbound.PPP_SKU__c = prod.SKU__c;
       outbound.Order_Dealer__c = 'SCEA';
       insert outbound;
       
       //US MPOS Order
       Staging_PPP_Outbound__c outbound1 = new Staging_PPP_Outbound__c();
       outbound1.PPP_Cancel_After_30_Days__c  = 'true';
       outbound1.Order_Record_Type__c = 'C';
       outbound1.Order__c = ordr1.Id;
       outbound1.PPP_SKU__c = prod.SKU__c;
       outbound1.Order_Dealer__c = 'SCEA';
       insert outbound1;
       
       //Canada POS Order
       Staging_PPP_Outbound__c outboundPOSCAN = new Staging_PPP_Outbound__c();
       outboundPOSCAN.PPP_Cancel_After_30_Days__c  = 'true';
       outboundPOSCAN.Order_Record_Type__c = 'C';
       outboundPOSCAN.Order__c = ordr.Id;
       outboundPOSCAN.PPP_SKU__c = prod.SKU__c;
       outboundPOSCAN.Order_Dealer__c = 'SCEA';
       insert outboundPOSCAN;
       
       //Canada MPOS Order
       Staging_PPP_Outbound__c outboundMPOSCAN = new Staging_PPP_Outbound__c();
       outboundMPOSCAN.PPP_Cancel_After_30_Days__c  = 'true';
       outboundMPOSCAN.Order_Record_Type__c = 'C';
       outboundMPOSCAN.Order__c = ordr1.Id;
       outboundMPOSCAN.PPP_SKU__c = prod.SKU__c;
       outboundMPOSCAN.Order_Dealer__c = 'SCEA';
       insert outboundMPOSCAN;
       
       System.assertEquals(outbound.PPP_SKU__c , prod.Sku__c , 'PPP Sku value should be updated');
       
    }
}