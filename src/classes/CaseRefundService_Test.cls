/*
CREATED BY : APPIRIO

UPDATED BT :
             10/29/2014 Leena Mandadapu : Jira# SMS-654 : Added code for PYB process

*/

@isTest
global class CaseRefundService_Test {
    //LM 10/29 added below variables
    static Asset__c pybast;
    static Order__c pybord;
    static Integration__c pybendpoint;
    
    static testMethod void myUnitTest() {
        Asset__c ast = TestClassUtility.createAssets(1, false)[0];
        ast.PPP_End_Date__c = Date.today().addDays(2);
        ast.PPP_Status__c = 'Active';
        ast.Asset_Status__c = 'Active';
        ast.PPP_Refund_Eligible__c = true;
        insert ast;
        
        Case cs = TestClassUtility.createCase('open', '', false);
        cs.recordTypeId = GeneralUtiltyClass.RT_PURCHASE_ID;
        insert cs;
        
        Order__c ordr = TestClassUtility.creatOrder(null, 1, false)[0];
        ordr.Asset__c = ast.Id;
        ordr.Order_Type__c = 'ESP';
        ordr.Case__c = cs.Id;
        insert ordr;
        
        Payment__c payment = new Payment__c();
        payment.Transaction_Date__c = date.today();
        payment.Payment_Status__c = 'Charged';
        payment.Amount__c = decimal.valueOf(100);
        payment.Purchase_Type__c = 'PPP';
        payment.Order__c = ordr.id;
        insert payment;
        
        ordr.Payment__c = payment.Id;
        update ordr;
        CaseRefundService.createCase(ordr.Id);
        
        Case refundCase = [select Id from Case where recordTypeId != :GeneralUtiltyClass.RT_PURCHASE_ID];
        System.assertNotEquals(refundCase, null, 'Refund case should be created');
    }
    
    //LM 10/29: added for PYB process
    static testMethod void myPYBPositiveUnitTest() {
        Test.startTest();
        createPYBdata();
        Test.setMock(HttpCalloutMock.class, new HTTPMockPYBCancellation());
        CaseRefundService.createCase(pybord.Id);
    }
    
    static void createPYBdata(){    
        pybast = TestClassUtility.createAssets(1, false)[0];
        pybast.PPP_Refund_Eligible__c =  true;
        pybast.Asset_Status__c = 'Active';
        pybast.PPP_Status__c = 'Active';
        pybast.PPP_End_Date__c = Date.today().addDays(2);
        insert pybast;
        
        pybord = TestClassUtility.creatOrder(null, 1, false)[0];
        pybord.Order_Type__c = 'ESP';
        pybord.Asset__c = pybast.Id;
        pybord.Order_Origin_Source__c = 'PYB';
        pybord.Order_Date__c = Date.today().addDays(35);
        insert pybord;   
        
        pybendpoint = new Integration__c();
        pybendpoint.PYB_End_Point_URL__c ='https://qasceasecure.protectyourbubble.com/PartnerPostSrv/PostServiceCRUD.svc/PolicyCancel';
        pybendpoint.PYB_User_Name__c = 'SonyApiUser@pyb.com';
        pybendpoint.PYB_Password__c ='hOqaeboP';
        insert pybendpoint;
        
    }
    
    global class HTTPMockPYBCancellation implements HttpCalloutMock  {
       global HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            String body = getBody();
            res.setBody(body);
            res.setStatusCode(200);
            return res;
       }
       String getBody() {
            String body = '<PolicyCancelReply xmlns="http://www.protectyourbubble.com/2011/09" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">' +
                      '<CancelDate>0001-01-01T00:00:00</CancelDate>'+
                      '<ContractNumber i:nil="true"/>'+
                      '<ErrorMessage xmlns:a="http://schemas.datacontract.org/2004/07/EaglePartnerData">'+
                      '<a:ErrStatus>false</a:ErrStatus>'+
                      '<a:ErrorCode></a:ErrorCode>'+
                      '<a:ErrorMsg></a:ErrorMsg>'+
                      '</ErrorMessage>'+
                      '<PolicyStatus i:nil="true"/>'+
                      '<RefundAmount>0</RefundAmount>'+
                      '</PolicyCancelReply>';
           return body; 
      }
    }
    
}