/*******************************************************************************
Class           : TestAttachmentShareBatch 
Description     : Test class for Start_Scheduler_Controller
Created by `    : Jeegar Brahmakshatriya
Created Date    : 11 July 2012
*******************************************************************************/

@isTest
private class Test_Start_Scheduler_Controller {
  static testMethod void testStartScheduler() {
    List<User> usersList = new List<User>();
    List<Script_Manager__c> scriptManagers = new List<Script_Manager__c>();
    List<Profile> sysAdminProfile = [Select Id
                            from Profile 
                            where name = 'System Administrator' Limit 1];
    User u = New User();
    u.FirstName = 'Testing';
    u.LastName = 'Tester';
    u.Alias = 'Alias';
    u.email = 'test123@test.com';
    u.userName =   string.valueof(Datetime.now()).replace('-','').
      replace(':','').replace(' ','') +  '@test.com';
    u.CommunityNickname = u.LastName;
    u.ProfileId = sysAdminProfile[0].Id;
    u.TimeZoneSidKey = 'GMT';
    u.TimeZoneSidKey = 'GMT';
    u.LocaleSidKey = 'en_US';
    u.EmailEncodingKey = 'ISO-8859-1';
    u.LanguageLocaleKey = 'en_US';
    u.CompanyName = 'SCEA';
    usersList.add(u);
    insert u;
    User testUser = New User();
    testUser.FirstName = 'Testing';
    testUser.LastName = 'Tester2';
    testUser.Alias = 'Alias2';
    testUser.email = 'test987@test.com';
    testUser.userName =  string.valueof(Datetime.now().addMinutes(5)).
      replace('-','').replace(':','').replace(' ','')  + '@test.com';
    testUser.CommunityNickname = testUser.LastName;
    testUser.ProfileId = sysAdminProfile[0].Id;
    testUser.TimeZoneSidKey = 'GMT';
    testUser.LocaleSidKey = 'en_US';
    testUser.EmailEncodingKey = 'ISO-8859-1';
    testUser.LanguageLocaleKey = 'en_US';
    testUser.CompanyName = 'SCEA';
    usersList.add(testUser);
    insert testUser;
    
    Script_Manager__c scriptManager1 = New Script_Manager__c ();
    scriptManager1.Active__c = true;
    scriptManager1.Class_Name__c ='TSTSched_AC';
    scriptManager1.Job_Name__c = 'SMJob1';
    scriptManager1.Schedule__c = '0 0 23 ? * SUN-SAT';
    scriptManager1.Must_Run_As__c = u.Id;
    
    Script_Manager__c scriptManager2 = New Script_Manager__c ();
    scriptManager2.Active__c = true;
    scriptManager2.Class_Name__c ='WrongClassName_AC';
    scriptManager2.Job_Name__c = 'SMJob2';
    scriptManager2.Schedule__c = '0 0 23 ? * SUN-SAT';
    //scriptManager2.Must_Run_As__c = testUser.Id;
    
    scriptManagers.add(scriptManager1);
    scriptManagers.add(scriptManager2);
    insert scriptManagers;
    Test.startTest();
    StartSchedulerController_AC testSSC = new StartSchedulerController_AC();
    testSSC.loadProcesses();
    System.assert(testSSC.activeProcesses.size()>0);
    testSSC.exectApex();
    System.assert(!testSSC.topErrMsg);
    testSSC.thisId= scriptManager1.Id; 
    testSSC.exectApex(); 
    System.assertEquals(testSSC.topErrorMessage,
        testSSC.runningAsUserErr + u.FirstName + ' ' + u.LastName );
    system.runAs(u){
      testSSC.thisId= scriptManager1.Id; 
      testSSC.exectApex(); 
      system.debug('Err Message = ' + testSSC.topErrorMessage);
      //System.assert(!testSSC.topErrMsg);
    }
    // Test for Incorrect Class Name
    testSSC.thisId= scriptManager2.Id; 
	  testSSC.exectApex(); 
	  System.assertEquals('Incorrect Class Name.', testSSC.topErrorMessage);
	  
	  //Give ScriptManager2 an invalid job Id
	  scriptManager2.Scheduled_Job_Id__c = '123456789012345';
    update scriptManager2;
    
	  // Refresh Script Manager for getting the job ID
	  
    scriptManagers = [Select Scheduled_Job_Id__c, Schedule__c, Must_Run_As__c, 
                      Last_Executed__c, Last_Deleted__c, Job_Name__c, 
                      Description__c, Class_Name__c, Active__c, 
                      Active_Scripts__c, Name, Id 
                      From Script_Manager__c 
                      where Id =: scriptManager1.id 
                        OR Id =: scriptManager2.Id];
    if(scriptManager1.Id == scriptManagers[0].Id) {
      scriptManager1 = scriptManagers[0];
      scriptManager2 = scriptManagers[1];
    } else {
        scriptManager2 = scriptManagers[0];
      scriptManager1 = scriptManagers[1];
    }
    testSSC.thisId = scriptManager2.Id;
    testSSC.loadProcesses();
    testSSC.unscheduleApexJob();
    system.assertEquals('Only batch and scheduled jobs are supported.',
      testSSC.topErrorMessage);
    testSSC.thisId = scriptManager1.Id;
    testSSC.loadProcesses();
    testSSC.unscheduleApexJob();
    system.assert(!testSSC.topErrMsg);
    Test.stopTest();
  }
}