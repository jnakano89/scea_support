/* ============================================================
 * This code is part of Richard Vanhook's submission to the 
 * Cloudspokes Geolocation Toolkit challenge.
 *
 * This software is provided "AS IS," and you, its user, 
 * assume all risks when using it. 
 * ============================================================
 * Updates : 
 			12/10/2013 : Urminder(JDC) : udated this class to show address in result and to show distance in miles or km as per user selection. 
 */
global class GeocodeSearchNearbyDemoController implements al.ObjectPaginatorListener{

    //search fields
    global String  addressInput       {get;set;}
    global Integer distance           {get;set;}
    global String  unit               {get;set;}
    global String  latitudeFieldName  {get;set;}
    global String  longitudeFieldName {get;set;}
    global String  objectName         {get;set;}

    //result fields
    global List<GeocodeService.SearchResult> results        {get;private set;}
    global al.ObjectPaginator                paginator      {get;private set;}
    global Boolean                           searchExecuted {get;private set;}
	
	global Case selectedCase;
	
    global GeocodeSearchNearbyDemoController(){
        String caseId = Apexpages.currentPage().getParameters().get('caseId');
        selectedCase =  getAddress(caseId);
        String address = selectedCase.Ship_To__r.Full_Address__c;
        this.addressInput       = address == null ? '' :address.replaceAll('<br>',',');
        this.distance           = 25;
        this.unit               = 'mi';
        //defaulting for Cloudspokes challenge
        this.latitudeFieldName  = 'Coordinates__Latitude__s';
        this.longitudeFieldName = 'Coordinates__Longitude__s';
        this.objectName         = 'Location__c';
        this.results            = null;
        this.paginator          = new al.ObjectPaginator(this);
        this.searchExecuted     = false;        
    }
    
    private Case getAddress(String caseId) {
    	Case selCase = new Case();
    	String fullAddress = '';
    	for(Case cs : [select Ship_To__r.Full_Address__c, Ship_to__r.Coordinates__Latitude__s, Ship_to__r.Coordinates__Longitude__s from Case where Id = :caseId]) {
    		fullAddress = cs.Ship_To__r.Full_Address__c;
    		selCase = cs;
    	}
    	return selCase;
    }
    
    global PageReference doSearchNearby(){
        searchExecuted = false;        
        if(al.StringUtils.isBlank(addressInput)) return error('Please make sure ship to address is selected in Case.');
        GeoPoint searchPoint = null;
        try{
        	if(selectedCase.Ship_to__r.Coordinates__Latitude__s <> null && selectedCase.Ship_to__r.Coordinates__Longitude__s <> null) {
            	searchPoint = new GeoPoint(selectedCase.Ship_to__r.Coordinates__Latitude__s, selectedCase.Ship_to__r.Coordinates__Longitude__s);
        	} else {
        		return error('Search failed: Address is not located');
        	}
        }catch(Exception e){
            return error('Search failed: ' + e.getMessage());
        }
        if(searchPoint == null) return error('\'' + addressInput + '\' not found');
        
        try{
            Double searchDistance = Double.valueOf(distance);
            if('mi'.equalsIgnoreCase(unit)) searchDistance = GeocodeService.milesToKilometers(searchDistance);
            paginator.setRecords(
                GeocodeService.findNearbyRecords(
                     searchPoint
                    ,searchDistance
                    ,null
                    ,this.latitudeFieldName
                    ,this.longitudeFieldName
                    ,this.objectName
                    ,null
                )
            );        
        }catch(Exception e){
            return error('Search failed: ' + e.getMessage());
        }
        searchExecuted = true;        
        return null;
    }
    
    private PageReference error(String msg){
        al.PageUtils.addError(msg);
        return null;
    }

    global void handlePageChange(List<Object> records){
        if(results == null) results = new List<GeocodeService.SearchResult>();
        results.clear();
        if(records != null && records.size() > 0){
            for(Object record : records){
                if(record != null && record instanceof GeocodeService.SearchResult){
                    results.add((GeocodeService.SearchResult) record);
                }
            }
        }
    }
    
    global List<SelectOption> distanceOptions {get{
        return new List<SelectOption>{
             new SelectOption('5','5')
            ,new SelectOption('10','10')
            ,new SelectOption('25','25')
            ,new SelectOption('50','50')
            ,new SelectOption('100','100')
            ,new SelectOption('200','200')
            ,new SelectOption('400','400')
            ,new SelectOption('800','800')
        };
    }}
    
    global List<SelectOption> unitOptions {get{
        return new List<SelectOption>{
             new SelectOption('km','Kilometers')
            ,new SelectOption('mi','Miles')
        };
    }}
    
    public Pagereference gotoCase() {
    	String caseId = Apexpages.currentPage().getParameters().get('caseId');
    	return new Pagereference('/' + caseId);
    }
    
    public Pagereference sendEmail() {
    	String locationId = Apexpages.currentPage().getParameters().get('ctRadio');
    	Case selectedCase;
    	if(locationId == null) {
    		return error('Please select a address!');
    	}
    	
    	String caseId = Apexpages.currentPage().getParameters().get('caseId');
    	String contactEmail;
    	for(Case cs : [select Contact.Email, Service_Location__c, ContactId from Case where Id = :caseId]) {
    		selectedCase = cs;
    	}
    	if(selectedCase <> null) {
    		selectedCase.Service_Location__c = locationId;
    		update selectedCase;
    	}
    	String[] toAddresses = new String[] {selectedCase.contact.Email}; 
	    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		
	    mail.setTargetObjectId(selectedCase.ContactId);
	    mail.setSenderDisplayName('SCEA');
	    mail.setUseSignature(false);
	    mail.setBccSender(false);
	    mail.setSaveAsActivity(false);
	    mail.setWhatId(locationId);
	    EmailTemplate et=[Select id from EmailTemplate where DeveloperName=:'Sony_Store_Referral'];
	    mail.setTemplateId(et.id);
	    Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});   
    	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Email has been send Successfully.'));
    	return null;
    }
    
}