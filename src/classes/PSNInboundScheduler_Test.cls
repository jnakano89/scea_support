/**********************************************************************************************************************************************************
Created : Appirio

Updates :
 07/01/2014 : Leena Mandadapu : JIRA# SF-1120 - Updated test class to include Console Id search on Asset and update the asset with PPP information
 08/27/2014 : Leena Mandadapu : JIRA# SMS-251 - Updated test class
***********************************************************************************************************************************************************/
@isTest
private class PSNInboundScheduler_Test {

    static testMethod void processSuccessfullyTest() {
        
        Product__c prod = TestClassUtility.createProduct(1, false)[0];
        prod.Status__c = 'Active';
        prod.Duration__c = 24;
        prod.Product_Type__c = 'ESP';
        prod.Sub_Type__c = 'AD';
        insert prod;
        
        //LM 08/25: Added
        Product__c HWprod = TestClassUtility.createProduct(1, false)[0];
        HWprod.Status__c = 'Active';
        HWprod.SKU__c = 'CUH-1001A';
        HWprod.Genre__c = 'PS';
        HWprod.PSN_Model_Number__c = 'TESTPSNNA';
        HWprod.sub_type__c = 'Hardware';
        insert HWprod;
        
        PSN_SKU_Mapping__c mapping = new PSN_SKU_Mapping__c();
        mapping.PSN_SKU__c = 'UP9007-NPUA30176_00-PS3ADH0000000000-U001';
        mapping.Country_Code__c = 'USA';
        mapping.LUCID_SKU__c = prod.SKU__c;
        insert mapping; 
        
        Account acc = TestClassUtility.createPersonAccount(1, false)[0];
        acc.FirstName = 'TESTFN';
        acc.LastName = 'TESTLN';
        acc.PersonEmail = 'Test@test.com';
        acc.Phone = '1234567890';
        insert acc;
        
        Contact con = TestClassUtility.createContact(1, False)[0];
        con.FirstName = 'TESTFN';
        con.LastName = 'TESTLN';
        con.Email = 'Test@test.com';
        con.Phone = '1234567890';
        insert con;
        
        PSN_Account__c psnacc = TestClassUtility.createPSNAccount(1, acc.Id, False)[0];
        psnacc.PSN_Account_ID__c = '1111111111';        
        insert psnacc;
                     
        //insert Staging with a new Console Id
        Staging_PPP_PSN_Inbound__c inbound = new Staging_PPP_PSN_Inbound__c();
        inbound.Address_Address_1__c = '20613 n.w. 4th ave.';
        inbound.Address_City__c = 'miami';
        inbound.Address_State__c = 'FL';
        inbound.Address_Country__c = 'USA';
        inbound.Address_Zip_Code__c = '33169';
        inbound.Asset_Console_ID__c = '0000000000062934646464646464TEST';
        inbound.PSN_Account_Sign_In_ID__c = 'testclass@testingScea.com';
        inbound.PSN_Account_ID__c = '4700275425113066687';
        inbound.PSN_Account_Name__c = 'TestExistingAsset';
        inbound.Asset_Purchase_Date__c = '01-28-2014';
        inbound.Order_Line_Sku__c = 'UP9007-NPUA30176_00-PS3ADH0000000000-U001';
        inbound.Asset_Creation_Date__c = '05-10-2013';
        inbound.Contact_First_Name__c = 'caple';
        inbound.Contact_Last_Name__c = 'palmer';
        inbound.Contact_Email__c = 'caple.palmer@testclassSCEA.com';
        inbound.Currency_Code__c = 'USD';
        inbound.Transaction_ID__c = '5440732666';
        inbound.Transaction_Type__c = 2;
        inbound.Order_Customer_Account_ID__c = '4700275425113066687';
        inbound.Order_Line_Total__c = 100;
        inbound.Order_Line_Quantity__c = 1;
        inbound.Order_Item_Id__c = '3469951666';
        //LM 08/25: Added
        inbound.Serial_Number__c = 'CC12346778';
        inbound.Model_Number__c = 'TESTPSNNA';
        
        insert inbound; 
        
        //create Asset with Console Id information
        Asset__c ast = TestClassUtility.createAssets(1, false)[0];
        ast.Console_ID__c = '00000000000646646464646464TEST';
        insert ast;
        
        Consumer_Asset__c conasset = TestClassUtility.createConsumerAsset(con.Id, ast.Id, 1, False)[0];
        insert conasset;

        //Insert Staging record with existing console Id
        Staging_PPP_PSN_Inbound__c inbound1 = new Staging_PPP_PSN_Inbound__c();
        inbound1.Address_Address_1__c = '123 Main St';
        inbound1.Address_City__c = 'San Mateo';
        inbound1.Address_State__c = 'CA';
        inbound1.Address_Country__c = 'USA';
        inbound1.Address_Zip_Code__c = '94401';
        inbound1.Asset_Console_ID__c = '00000000000646646464646464TEST';
        inbound1.PSN_Account_Sign_In_ID__c = 'test@test.com';
        inbound1.PSN_Account_ID__c = '1111111111';
        inbound1.PSN_Account_Name__c = 'TestingExistingAsset';
        inbound1.Asset_Purchase_Date__c = '06-10-2014';
        inbound1.Order_Line_Sku__c = 'UP9007-NPUA30176_00-PS3ADH0000000000-U001';
        inbound1.Asset_Creation_Date__c = '06-10-2014';
        inbound1.Contact_First_Name__c = 'TESTFN';
        inbound1.Contact_Last_Name__c = 'TESTLN';
        inbound1.Contact_Email__c = 'Test@test.com';
        inbound1.Currency_Code__c = 'USD';
        inbound1.Transaction_ID__c = '6004624977';
        inbound1.Transaction_Type__c = 2;
        inbound1.Order_Customer_Account_ID__c = '313728207653406944069';
        inbound1.Order_Line_Total__c = 59.99;
        inbound1.Order_Line_Quantity__c = 1;
        inbound1.Order_Item_Id__c = '38757488923';
        inbound1.Serial_Number__c = 'MB123123321';
        inbound1.Model_Number__c = 'TESTPSNNA';
        
        insert inbound1;
        
        system.debug('<<<<<<<<<<inbound1Rec>>>>>>'+inbound1);
        
        //Insert a Staging record which was already processed succesfully
        //Insert successfull transaction
        Staging_PPP_PSN_Inbound__c inbound2 = new Staging_PPP_PSN_Inbound__c();
        inbound2.Address_Address_1__c = '123 Main St';
        inbound2.Address_City__c = 'San Mateo';
        inbound2.Address_State__c = 'CA';
        inbound2.Address_Country__c = 'USA';
        inbound2.Address_Zip_Code__c = '94401';
        inbound2.Asset_Console_ID__c = '0000000000062934646464646464TEST';
        inbound2.PSN_Account_Sign_In_ID__c = 'test@test.com';
        inbound2.PSN_Account_ID__c = '123455667788';
        inbound2.PSN_Account_Name__c = 'TestingNewAsset';
        inbound2.Asset_Purchase_Date__c = '06-10-2014';
        inbound2.Order_Line_Sku__c = 'UP9007-NPUA30176_00-PS3ADH0000000000-U001';
        inbound2.Asset_Creation_Date__c = '06-10-2014';
        inbound2.Contact_First_Name__c = 'TESTTEST';
        inbound2.Contact_Last_Name__c = 'TESTLASTNAME';
        inbound2.Contact_Email__c = 'TestEmail@test.com';
        inbound2.Currency_Code__c = 'USD';
        inbound2.Transaction_ID__c = '1111112222';
        inbound2.Transaction_Type__c = 2;
        inbound2.Order_Customer_Account_ID__c = '313728207653406944069';
        inbound2.Order_Line_Total__c = 59.99;
        inbound2.Order_Line_Quantity__c = 1;
        inbound2.Order_Item_Id__c = '38757488923';
        inbound2.Serial_Number__c = 'MB123123321';
        inbound2.Model_Number__c = 'TESTPSNNA';
        inbound2.Processed_Status__c = 'Success';
        inbound2.Processed_Date_Time__c = Date.today();
        insert inbound2;
        
        //Insert duplicate transaction
        Staging_PPP_PSN_Inbound__c dupeinbound = new Staging_PPP_PSN_Inbound__c();
        dupeinbound.Address_Address_1__c = '123 Main St';
        dupeinbound.Address_City__c = 'San Mateo';
        dupeinbound.Address_State__c = 'CA';
        dupeinbound.Address_Country__c = 'USA';
        dupeinbound.Address_Zip_Code__c = '94401';
        dupeinbound.Asset_Console_ID__c = '0000000000062934646464646464TEST';
        dupeinbound.PSN_Account_Sign_In_ID__c = 'test@test.com';
        dupeinbound.PSN_Account_ID__c = '123455667788';
        dupeinbound.PSN_Account_Name__c = 'TestingNewAsset';
        dupeinbound.Asset_Purchase_Date__c = '06-10-2014';
        dupeinbound.Order_Line_Sku__c = 'UP9007-NPUA30176_00-PS3ADH0000000000-U001';
        dupeinbound.Asset_Creation_Date__c = '06-10-2014';
        dupeinbound.Contact_First_Name__c = 'TESTTEST';
        dupeinbound.Contact_Last_Name__c = 'TESTLASTNAME';
        dupeinbound.Contact_Email__c = 'TestEmail@test.com';
        dupeinbound.Currency_Code__c = 'USD';
        dupeinbound.Transaction_ID__c = '1111112222';
        dupeinbound.Transaction_Type__c = 2;
        dupeinbound.Order_Customer_Account_ID__c = '313728207653406944069';
        dupeinbound.Order_Line_Total__c = 59.99;
        dupeinbound.Order_Line_Quantity__c = 1;
        dupeinbound.Order_Item_Id__c = '38757488923';
        dupeinbound.Serial_Number__c = 'MB123123321';
        dupeinbound.Model_Number__c = 'TESTPSNNA';
        insert dupeinbound;
        
        //Insert staging record to perform data validations
        Staging_PPP_PSN_Inbound__c inbound3 = new Staging_PPP_PSN_Inbound__c();
        inbound3.Address_Address_1__c = '123 Main St';
        inbound3.Address_City__c = '';//Invalid city
        inbound3.Address_State__c = '';//Invalid State
        inbound3.Address_Country__c = '';//Invalid Country
        inbound3.Address_Zip_Code__c = '';//Invalid Zipcode
        inbound3.Asset_Console_ID__c = '';//Inavlid Console Id
        inbound3.PSN_Account_Sign_In_ID__c = 'test@test.com';
        inbound3.PSN_Account_ID__c = '123455667788';
        inbound3.PSN_Account_Name__c = 'TestingNewAsset';
        inbound3.Asset_Purchase_Date__c = '';//Invalid ppp purchase date
        inbound3.Order_Line_Sku__c = 'TEST';  //Invalid PSN SKU
        inbound3.Asset_Creation_Date__c = '';//Invalid Asset purchase date
        inbound3.Contact_First_Name__c = '';//Invalid First Name
        inbound3.Contact_Last_Name__c =  '';//Invalid Last name
        inbound3.Contact_Email__c = '';//Invalid email
        inbound3.Currency_Code__c = 'US';//invalid currency code
        inbound3.Transaction_ID__c = '6004624977';
        inbound3.Transaction_Type__c = 1; //Invalid Transaction Type
        inbound3.Order_Customer_Account_ID__c = '313728207653406944069';
        inbound3.Order_Line_Total__c = 59.99;
        inbound3.Order_Line_Quantity__c = 2;//Invalid Orde line Qty
        inbound3.Order_Item_Id__c = '38757488923';
        inbound3.Serial_Number__c = 'MB123123321';
        inbound3.Model_Number__c = 'TEST';//Invalid model number
        
        insert inbound3;
        
        Test.StartTest();
        Database.executeBatch(new PSNInboundScheduler());
        Test.StopTest();
        
        system.debug('<<<<<<<<<<inbound1Recafterprocessing>>>>>>'+inbound1.Processed_Error_Text__c);
        
        //Check if the new Staging PSN PPP record is processed successfully
        //Staging_PPP_PSN_Inbound__c processedRecord = [select Processed_Status__c from Staging_PPP_PSN_Inbound__c where Id = :inbound.Id];
        //System.assertEquals(processedRecord.Processed_Status__c, 'Success', 'Record should be processed successfully');
        
        //Check the Updated Asset record
        Asset__c updated_astrec = [select Console_ID__c, Model_Number__c, Serial_Number__c, Purchase_Date__c, PPP_Product__c, PPP_Purchase_Date__c,
                                   Coverage_Type__c, PPP_Start_Date__c, PPP_End_Date__c, PPP_Status__c from Asset__c where Id = :ast.Id ];  
        system.debug('<<<<<<Upd Asset>>>>>>'+updated_astrec);                         
                                                                                                                         
       /* System.assertEquals(updated_astrec.Console_ID__c, '00000000000646646464646464TEST','Console Id update Failed');
        System.assertEquals(updated_astrec.Model_Number__c, 'CUH-1001A','Model Number update Failed');
        //System.assertEquals(updated_astrec.Serial_Number__c, inbound1.Serial_Number__c,'Serial Num update Failed');
        System.assertEquals(updated_astrec.Purchase_Date__c, Date.parse(inbound1.Asset_Creation_Date__c.split(' ')[0].replace('-','/')),'Purchase Date update Failed');
        System.assertEquals(updated_astrec.PPP_Product__c, prod.Id,'PPP Product update - Failed');
        System.assertEquals(updated_astrec.PPP_Purchase_Date__c, Date.parse(inbound1.Asset_Purchase_Date__c.split(' ')[0].replace('-','/')),'PPP Purchase Date update Failed');
        System.assertNotEquals(updated_astrec.PPP_Start_Date__c, null,'PPP Start Date update Failed');
        System.assertNotEquals(updated_astrec.PPP_End_Date__c, null,'PPP End Date update Failed');
        System.assertEquals(updated_astrec.PPP_Status__c, 'Pending Confirm','PPP Status update Failed');
        System.assertEquals(updated_astrec.Coverage_Type__c, 'AD','PPP Coverage Type update Failed');   */
      
        //already processed successfully
        // Staging_PPP_PSN_Inbound__c processedRecord2 = [select Processed_Status__c from Staging_PPP_PSN_Inbound__c where Id = :dupeinbound.Id];
        //System.assertEquals(processedRecord2.Processed_Status__c, 'Duplicate', 'Record should be rejected with Duplicate status');                                   
    }
    
     static testMethod void processInvalidRecordTest() {
        
        Product__c prod = TestClassUtility.createProduct(1, false)[0];
        prod.Status__c = 'Active';
        prod.Duration__c = 24;
        insert prod;
        
        //LM 08/25: Added
       Product__c HWprod = TestClassUtility.createProduct(1, false)[0];
        HWprod.Status__c = 'Active';
        HWprod.SKU__c = 'TESTNA';
        HWprod.Genre__c = 'PS';
        HWprod.PSN_Model_Number__c = 'TESTPSNNA';
        HWprod.sub_type__c = 'Hardware';
        insert HWprod;
        
        PSN_SKU_Mapping__c mapping = new PSN_SKU_Mapping__c();
        mapping.PSN_SKU__c = 'UP9007-NPUA30176_00-PS3ADH0000000000-U001';
        mapping.Country_Code__c = 'USA';
        mapping.LUCID_SKU__c = prod.SKU__c;
        insert mapping;
        
        list<Staging_PPP_PSN_Inbound__c> inboundList = new list<Staging_PPP_PSN_Inbound__c>();
        
        //Insert staging record with blank Country, Invalid Transaction Type
        Staging_PPP_PSN_Inbound__c inbound = new Staging_PPP_PSN_Inbound__c();
        inbound.Address_Address_1__c = '20613 n.w. 4th ave.';
        inbound.Address_City__c = 'miami';
        inbound.Address_State__c = 'FL';
        inbound.Address_Country__c = '';// Invalid Currency Code
        inbound.Address_Zip_Code__c = '33169';
        inbound.Asset_Console_ID__c = '000000010064000F1402222EED55829D';
        inbound.PSN_Account_Sign_In_ID__c = 'testclass@testingScea.com';
        inbound.PSN_Account_ID__c = '4700275425113066687';
        inbound.PSN_Account_Name__c = 'dboifreshcc666';
        inbound.Asset_Purchase_Date__c = '01/28/2014 01:01:00.000000';
        inbound.Order_Line_Sku__c = 'UP9007-NPUA30176_00-PS3ADH0000000000-U001';
        inbound.Asset_Creation_Date__c = '05/10/2013 00:00:00.000000';
        inbound.Contact_First_Name__c = 'caple';
        inbound.Contact_Last_Name__c = 'palmer';
        inbound.Contact_Email__c = 'caple.palmer@testclassSCEA.com';
        inbound.Currency_Code__c = 'USD';
        inbound.Transaction_ID__c = '54409462666';
        inbound.Transaction_Type__c = 1; // Invalid Transaction Type
        inbound.Order_Customer_Account_ID__c = '4700275425113066687';
        inbound.Order_Line_Total__c = 100;
        inbound.Order_Line_Quantity__c = 1;
        inbound.Order_Item_Id__c = '3469951666.00889999111011';
        inboundList.add(inbound);
        
        //Insert Staging record with Invalid information - missing address, Consumer info etc
        Staging_PPP_PSN_Inbound__c inbound2 = new Staging_PPP_PSN_Inbound__c();
        //inbound.Address_Address_1__c = '20613 n.w. 4th ave.';
        //inbound.Address_City__c = 'miami';
        //inbound.Address_State__c = 'FL';
        //inbound.Address_Country__c = 'USA';
        //inbound.Address_Zip_Code__c = '33169';
        inbound2.Asset_Console_ID__c = '000000010064000F1402222EED55829D';
        inbound2.PSN_Account_Sign_In_ID__c = 'testclass@testingScea.com';
        inbound2.PSN_Account_ID__c = '4700275425113066687';
        inbound2.PSN_Account_Name__c = 'dboifreshcc666';
        inbound2.Asset_Purchase_Date__c = '01/28/2014 01:01:00.000000';
        inbound2.Order_Line_Sku__c = 'UP9007-NPUA30176_00-PS3ADH0000000000-U001';
        inbound2.Asset_Creation_Date__c = '05/10/2013 00:00:00.000000';
        //inbound.Contact_First_Name__c = 'caple';
        //inbound.Contact_Last_Name__c = 'palmer';
        //inbound.Contact_Email__c = 'caple.palmer@testclassSCEA.com';
        inbound2.Currency_Code__c = 'USD';
        inbound2.Transaction_ID__c = '54464432666';
        inbound2.Transaction_Type__c = 2;
        inbound2.Order_Customer_Account_ID__c = '4700275425113066687';
        inbound2.Order_Line_Total__c = 100;
        //inbound.Order_Line_Quantity__c = 1;
        inbound2.Order_Item_Id__c = '3469951666.00889999111011';
        inboundList.add(inbound2);
                
        insert inboundList;
        
        //Insert Staging record with invalid unit purchase date and Model number 
        Staging_PPP_PSN_Inbound__c inbound3 = new Staging_PPP_PSN_Inbound__c();
        inbound3.Address_Address_1__c = '20613 n.w. 4th ave.';
        inbound3.Address_City__c = 'miami';
        inbound3.Address_State__c = 'FL';
        inbound3.Address_Country__c = 'USA';
        inbound3.Address_Zip_Code__c = '33169';
        inbound3.Asset_Console_ID__c = '000000010064000F1402222EFFD55829D';
        inbound3.PSN_Account_Sign_In_ID__c = 'testclass@testingScea.com';
        inbound3.PSN_Account_ID__c = '4700275425113066687';
        inbound3.PSN_Account_Name__c = 'dboifreshcc666';
        inbound3.Asset_Purchase_Date__c = '01-28-2014';
        inbound3.Order_Line_Sku__c = 'UP9007-NPUA30176_00-PS3ADH0000000000-U001';
        inbound3.Asset_Creation_Date__c = '05-10-2014'; //unit purchase date greater than PPP purchase date
        inbound3.Contact_First_Name__c = 'caple';
        inbound3.Contact_Last_Name__c = 'palmer';
        inbound3.Contact_Email__c = 'caple.palmer@testclassSCEA.com';
        inbound3.Currency_Code__c = 'USD';
        inbound3.Transaction_ID__c = '54411332666';
        inbound3.Transaction_Type__c = 2;
        inbound3.Order_Customer_Account_ID__c = '4700275425113066687';
        inbound3.Order_Line_Total__c = 100;
        inbound3.Order_Line_Quantity__c = 1;
        inbound3.Order_Item_Id__c = '3469951666.00889999111011';
        inbound3.Model_Number__c = 'TESTNA'; //invalid model number
        inboundList.add(inbound3);
        
        //Insert Staging record with invalid PPP purchase date
        Staging_PPP_PSN_Inbound__c inbound4 = new Staging_PPP_PSN_Inbound__c();
        inbound4.Address_Address_1__c = '20613 n.w. 4th ave.';
        inbound4.Address_City__c = 'miami';
        inbound4.Address_State__c = 'FL';
        inbound4.Address_Country__c = 'USA';
        inbound4.Address_Zip_Code__c = '33169';
        inbound4.Asset_Console_ID__c = '000000010064000F156702222EFFD55829D';
        inbound4.PSN_Account_Sign_In_ID__c = 'testclass@testingScea.com';
        inbound4.PSN_Account_ID__c = '4700275425113066687';
        inbound4.PSN_Account_Name__c = 'dboifreshcc666';
        inbound4.Asset_Purchase_Date__c = '08-12-2014';//invalid PPP Purchase date. over 365 days
        inbound4.Order_Line_Sku__c = 'UP9007-NPUA30176_00-PS3ADH0000000000-U001';
        inbound4.Asset_Creation_Date__c = '05-10-2013'; 
        inbound4.Contact_First_Name__c = 'caple';
        inbound4.Contact_Last_Name__c = 'palmer';
        inbound4.Contact_Email__c = 'caple.palmer@testclassSCEA.com';
        inbound4.Currency_Code__c = 'USD';
        inbound4.Transaction_ID__c = '54456732666';
        inbound4.Transaction_Type__c = 2;
        inbound4.Order_Customer_Account_ID__c = '4700275425113066687';
        inbound4.Order_Line_Total__c = 100;
        inbound4.Order_Line_Quantity__c = 1;
        inbound4.Order_Item_Id__c = '3469951666.00889999111011';
        inbound4.Model_Number__c = 'TESTNA'; //invalid model number
        inboundList.add(inbound4);
        
        Test.startTest();
        Database.executeBatch(new PSNInboundScheduler());
        Test.stopTest();
        
        //LM 06/18/2014 : Added system assert statments
        System.assertEquals(0, [select Id from Asset__c where Console_ID__c =: inboundList[0].Asset_Console_ID__c].size(),'Asset inserted with Invalid PSN Transaction Type and Currrency - Failed');
        System.assertEquals(0, [select Id from Asset__c where Console_ID__c =: inboundList[1].Asset_Console_ID__c].size(),'Asset inserted with out required data - Failed');
    }
}