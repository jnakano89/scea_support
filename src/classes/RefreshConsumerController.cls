/******************************************************************************
Class         : RefreshConsumerController
Description   : Controller Class for RefreshConsumer [T-179284]
Developed by  : Urminder Vohra(JDC)
Date          : Sep 2, 2013

Updates       : Modified by preetu to add PS Plus consumer attributes (7/24/2014)
******************************************************************************/

public class RefreshConsumerController {
    
  String contactId;
  String mdmContactId;
  String mdmAccountId; 
  public ConsumerSearchResult searchResultMember{get;set;}
  public final String CLASS_NAME = 'RefreshConsumerController';
  
  public RefreshConsumerController() {
    
    searchResultMember = new ConsumerSearchResult();
    
    mdmContactId = Apexpages.currentPage().getParameters().get('mdmContactId');
    
    for(Contact cnt : [select Id, MDM_Account_ID__c from Contact where MDM_ID__c = :mdmContactId]) {
      contactId = cnt.Id;
      mdmAccountId = cnt.MDM_Account_ID__c;
      
    }//end-for
    
  }
  
  public PageReference refreshConsumer() {
    
        OSB__c osbSettings = OSB__c.getOrgDefaults();
        
        if (!osbSettings.Consumer_Refresh_Enabled__c){
            return returnToContact();
        }
    
        System.debug('>>>>>>>>>>>>refreshConsumer<<<<<<<<<<<<<');
   
        if(mdmContactId <> null && mdmContactId <> '') {
        
        //call refresh consumer webservice
      //    if(!Test.isRunningTest()){
            
            try
            {
                System.debug('>>>>>>>>mdmContactId='+mdmContactId);
                
                searchResultMember = ConsumerServiceUtility.refreshConsumer(mdmContactId);
            
            } catch(Exception e) {
            
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage())); 
                IntegrationAlert.addException('Refresh Consumer', 'MDM Contact ID='+mdmContactId, e);        
                return null;
            }
      //    }//end-if not test
        
        populatePSNAccount(searchResultMember);
        
        return null;
                
    } else {
        //If the Consumer does not have a MDM Contact ID, then navigate back to the Consumer record.
        return returnToContact();
    }   
  }//end-method
  
  
  
  public void populatePSNAccount(ConsumerSearchResult searchResultMember) {
    
    PSN_Account__c psnAcc;
    
    Sony_MiddlewareConsumerRefresh_Final.Contact_mdm contact_mdm;
    Sony_MiddlewareConsumerRefresh_Final.Account_mdm account_mdm;
    
    Set<String> addressMdmIds = new Set<String>();
    
    list<Address__c> listOfAdressToUpsert = new list<Address__c>();
    
    Map<String, Address__c> existingAddressMap = new map<String, Address__c>();
    
    list<Sony_MiddlewareConsumerRefresh_Final.Address_mdm> addressList = 
            new list<Sony_MiddlewareConsumerRefresh_Final.Address_mdm>();
     
    if(searchResultMember <> null && searchResultMember.listOfContacts <> null){
        
      for(Sony_MiddlewareConsumerRefresh_Final.Contact_mdm cnt : searchResultMember.listOfContacts) {
        
        contact_mdm = cnt;
        
        for(Sony_MiddlewareConsumerRefresh_Final.Account_mdm acc : cnt.Account) {
          account_mdm = acc;
        }
        
        if(cnt.ListOfAddress<> null && cnt.ListOfAddress.Address <> null) {
            
          for(Sony_MiddlewareConsumerRefresh_Final.Address_mdm adrs : cnt.ListOfAddress.Address)    {
            
            addressList.add(adrs);
            System.debug('>>>>>>>>>> adrs.AddressId = ' + adrs.AddressId);
            addressMdmIds.add(adrs.AddressId);
          
          }
        }
      }
    } else if(searchResultMember <> null && searchResultMember.ErrorMessage <> null) {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Error in service response :'+searchResultMember.ErrorMessage);
    }
    
    if(account_mdm <> null) {
        
        //Find a child PSN Account record with the same MDM Account ID in the response.
        for(PSN_Account__c acc : [select Id
                                    from PSN_Account__c
                                    where Consumer__c = :contactId
                                        AND MDM_Account_ID__c = :account_mdm.MDMAccountRowId]) {
                                            
            psnAcc = new PSN_Account__c(Id = acc.Id);
        }
        
        // If there isn't, create a new PSN account record
        if(psnAcc == null) {
            psnAcc = new PSN_Account__c(
                            Consumer__c = contactId , 
                            MDM_Account_ID__c = account_mdm.MDMAccountRowId);
        }
        
        psnAcc.Name = account_mdm.PSNHandle;
        psnAcc.Account_ID__c =  account_mdm.PSNAccountId;
        psnAcc.Creation_Date__c = account_mdm.CreatedDate;
        psnAcc.PSN_Account_ID__c = account_mdm.PSNAccountId;
        psnAcc.Suspension_Date__c = parseDate(account_mdm.AccountSuspendDate);
        psnAcc.Suspension_Reason__c = account_mdm.AccountSuspendReason;
        psnAcc.Unsuspended_Date__c = parseDate(account_mdm.AccountUnsuspendDate);
        psnAcc.Status__c = account_mdm.AccountStatus;
        psnAcc.First_Name__c = contact_mdm.FirstName;
        psnAcc.Last_Name__c = contact_mdm.LastName;
        psnAcc.Language__c = contact_mdm.PreferredLanguageCode;
        psnAcc.Date_of_Birth__c = parseDate(contact_mdm.BirthDate);
        psnAcc.Gender__c = contact_mdm.sex;
        psnAcc.PSN_Sign_In_ID__c = contact_mdm.PSNSignInId;
        
        //Code added by preetu for PS plus subscriber information
        if(account_mdm.PSPlusSubscriber != null){
        psnAcc.PS_Plus_Indicator__c = Boolean.valueOf(account_mdm.PSPlusSubscriber); 
        }
       /*if((account_mdm.PSPlusSubscriber.equalsIgnoreCase('False') && account_mdm.PSPlusStackedEndDate == '')){
        psnAcc.PS_Plus_Start_Date__c = '';
        System.debug('---->' + psnAcc.PS_Plus_Start_Date__c);
        }
        else 
        {*/
         psnAcc.PS_Plus_Start_Date__c = getMyDateTime(account_mdm.PSPlusStartDate);
        //}
        psnAcc.PS_Plus_End_Date__c = getMyDateTime(account_mdm.PSPlusStackedEndDate); 
        if(account_mdm.PSPlusAutoRenewalFlag != null){
        if(account_mdm.PSPlusAutoRenewalFlag.equalsIgnoreCase('True')){
        psnAcc.Auto_Renewal__c = 'On';
        }
        else if(account_mdm.PSPlusAutoRenewalFlag.equalsIgnoreCase('False')){
        psnAcc.Auto_Renewal__c = 'Off';
        }
        }
       //psnAcc.PS_Plus_Subscription_Source__c = account_mdm.PSPlusSUbscriptionSource;
        psnAcc.PSN_Plus_Country__c = account_mdm.PSPlusAccountCountry;
        
                
        //End of code change
        
        try { 
          upsert psnAcc;
          
        } catch(Exception ex) {
            
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'PSN Account could not inserted reason :'+ex.getMessage());
            
        }
    }
    
    for(Address__c adrs : [select Id, MDM_ID__c  
                            from Address__c
                            where Consumer__c = :contactId
                                AND MDM_ID__c !=null]) {
        
        existingAddressMap.put(adrs.MDM_ID__c, adrs);   
        
        system.debug('>>>>>>>> SFDC address ='+adrs);
    
    }
    
    
    for(Sony_MiddlewareConsumerRefresh_Final.Address_mdm adrs : addressList){
        
      Address__c address;
      System.debug('>>>> find = ' + adrs.AddressId);
      
      if(existingAddressMap.containsKey(adrs.AddressId)) {
        System.debug('>>>> address found! = ' + adrs.AddressId);
        address = new Address__c(Id = existingAddressMap.get(adrs.AddressId).Id);
      } else {
        System.debug('>>>> address NEW = ' + adrs.AddressId);
        address = new Address__c(Consumer__c = contactId, MDM_ID__c = adrs.AddressId);
      }
      
      address.Address_Line_1__c = adrs.StreetAddress;
      address.Address_Line_2__c = adrs.StreetAddress2;
      address.Address_Line_3__c = adrs.StreetAddress3;
      address.Address_Type__c = adrs.ContactAddressType;
      address.City__c = adrs.City;
      address.Country__c = adrs.Country;
      address.Postal_Code__c = adrs.PostalCode;
      address.State__c = adrs.State;
      address.Address_Type__c = adrs.ContactAddressType;
      
      system.debug('>>>>> address = '+ address);
      listOfAdressToUpsert.add(address);
      
    }//end-for
    
    if(!listOfAdressToUpsert.isEmpty()) {
        
      try { 
        upsert listOfAdressToUpsert;
        
      } catch(Exception ex) {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Address could not inserted reason :'+ex.getMessage());
      }
    }
  }
      
  public PageReference createAssetRecords() {
    
    System.debug('========createAssetRecords=========');
    System.debug('>>>>mdmAccountId='+mdmAccountId);
    
    //Do nothing if there is no MDM Account ID.
    if(mdmAccountId <> null && mdmAccountId <> '') {
        GetAssetsController assetCtrl = new GetAssetsController();
        assetCtrl.contactId = contactId;
        assetCtrl.mdmAccountId = mdmAccountId;
        assetCtrl.populateRecords();
    }
    return returnToContact();
  }
  
  private Date parseDate(String dateString){ // dateString is in this format "12/26/2007".
    if(dateString != null && dateString !=''){
        try{
            date assetDate = Date.parse(dateString);
            return assetDate;
        }
        catch(Exception ex){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Date Format Is InValid : '+dateString);
        }
    }
    return null;
  }

  //Code added by Preetu to process date format:
    
    public static String getMyDateTime(string strDt){ 
      if(strDt != null && strDt !=''){ 
          try{
              String[] DTSplitted = strDT.split(' ');
              string year = DTSplitted.get(0).split('-').get(0);
              string month = DTSplitted.get(0).split('-').get(1);
              string day = DTSplitted.get(0).split('-').get(2);
      

              string stringDate = month + '/' + day + '/' + year;

              return stringDate;
           }
         catch(Exception ex){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Date Format Is InValid : '+strDt);
         }
   
      }
       return null;
         }
  
    public PageReference returnToContact(){
        
        if(contactId != null && contactId != ''){
            
            PageReference contactPage = new ApexPages.StandardController(new Contact(id=contactId)).view();
            contactPage.setRedirect(true);
            return contactPage;
        }
        
        return null;
    }
  
}