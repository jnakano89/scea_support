/******************************************************************************
Class         : Siebel_TestUtility_AC
Description   : Utility class to store test records for Test coverage
Developed by  : Rahul Mittal
Date          : March 25, 2013              
******************************************************************************/
public without sharing class Siebel_TestUtility_AC {
    //Define value to No. of Responses at the time creation of Dummy Responses 
    public static Integer noOfDummyResponses;
    
    public static Integer uniqueusernumber = 0;
    static Profile sysAdminProfile = [Select id from Profile where Name = 'System Administrator'];
    
    //Method to create test case records
    public static List<Case> createCases(Integer noOfCases, Boolean isInsert){
        List<Case> cases = new List<Case>();
        for(Integer casNum=0; casNum < noOfCases ; casNum++){
            Case c1 = new Case();   
            c1.Status = 'Open';
            c1.Origin = 'Better Business Bureau';
            c1.Type = 'Account Management';
            c1.Product__c = '1st Party Games';
            c1.Offender__c = 'test';
            c1.Source_ID__c = 'TestCase' + casNum;
            cases.add(c1);
        }
        if(isInsert)
            insert cases;
        return cases;
    }
    
    //Method to create test Account records
    public static List<Account> createAccounts(Integer noOfAccounts, Boolean isInsert){
        List<Account> accounts = new List<Account>();
        for(Integer accNum=0; accNum < noOfAccounts ; accNum++){
            Account a1 = new Account();   
            a1.LastName = 'Test Acc ' + accNum;
            a1.MDM_Handle__c = 'test handle ' + accNum ;
            accounts.add(a1);
        }
        if(isInsert)
            insert accounts;
        return accounts;
    }
    
    //Method to create test users
    public static List<User> CreateUsers(string profileName, Integer numOfUsers, UserRole role) {
        List<Profile> profiles;
        if(profileName != null && profileName != '') 
            profiles = [Select id from Profile where Name = :profileName limit 1];
  
        List<User> lsUsers = new List<User>();
        for(integer i = 1; i <= numOfUsers; i++){  
            User u1 = new User();
            u1.firstName = 'test' + i;
            u1.LastName = 'test' + i; 
            u1.Alias = 'tst' + i;
            u1.Email = 'test' + uniqueusernumber + '.test@netapp.com';  
            u1.UserName='test'+ Math.random().format() + string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','') + uniqueusernumber +'user1@gmail.com';
        
            if(profiles != null && profiles.size() > 0)
                u1.ProfileId = profiles.get(0).Id;
            else 
                u1.ProfileId = sysAdminProfile.Id;
        
            u1.CommunityNickname = u1.firstname + '_' + + uniqueusernumber + Math.random().format() + u1.lastName;
            u1.EmailEncodingKey ='ISO-8859-1'; 
            u1.LanguageLocaleKey = 'en_US';
            u1.TimeZoneSidKey ='America/Los_Angeles';
            u1.LocaleSidKey = 'en_US';
            String fedId = uniqueusernumber + Math.random().format() + Datetime.now().format()+'1234567890ABCDEFG';       
            u1.FederationIdentifier = fedId.substring(0,20);
            if(role != null) 
              u1.UserRoleId = role.Id;
            lsUsers.add(u1);
            uniqueusernumber++;
        }
        return lsUsers;   
    }
    
}