/**  Controller class to redirect Decision Tree article to the right Flow 
  *  Based on Flow_Name__c for the article.  
  */
global with sharing class  ArticleExtensionDecisionTree{

    // Holds the current Decision Tree article.
    global final Decision_Tree__kav article;
    
    // Constructor to initiate current article.
    global ArticleExtensionDecisionTree(ApexPages.KnowledgeArticleVersionStandardController stdController) {
            this.article = (Decision_Tree__kav)stdController.getRecord();
    }
                 
    // Method for Internal Article view. If exists, display internal custom setting;
    // otherwise, display internal page layout (eg /flow/flowName)
    global PageReference redirectInternal()
    {
        // No redirect if article is null or flow_name__c is not defined for an article.
        if(this.article==null || this.article.get('Flow_Name__c') == null)
        {
            return null;
        }
        
       String flowName = (String)this.article.get('Flow_Name__c');
       String url = '/flow/' + flowName;
       if(settingsMap.get(flowName) != null)
       {
          String internalPageName = ((DecisionTreeArticleTypePageSettings__c)settingsMap.get(flowName)).InternalPageName__c;
           if(internalPageName !=null && internalPageName !=''){
             url = '/apex/' + internalPageName;
           }
       }

       PageReference p = new PageReference(url);
       p.setRedirect(true);
      return p;
    }
    
    // Method to get custom setting values that indicate which VF page to display
    private Map<String,DecisionTreeArticleTypePageSettings__c> settingsMap {
    get {
      if (settingsMap == null) settingsMap = DecisionTreeArticleTypePageSettings__c.getAll();
      return settingsMap;
    }
    private set;
    }
    
    // Method to redirect to appropriate VF Wrapper to display in External sites. 
    global String getDynamicFlowPage()
    {
        // No redirect if article is null or flow_name__c is not defined for an article.
        if(this.article==null || this.article.get('Flow_Name__c') == null)
        {
            return null;
        }
       String flowName = (String)this.article.get('Flow_Name__c');
       if(settingsMap.get(flowName) ==null)
       {
           return null;
       }
       return ((DecisionTreeArticleTypePageSettings__c)settingsMap.get(flowName)).ExternalPageName__c;
    }    
}