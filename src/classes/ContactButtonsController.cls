///******************************************************************************
//Class         : ContactButtonsController
//Description   : A Controller class for Contact Buttons which is added at service Console Page layout for Contact To create a
//                case record from selecting appropriate record type.
//Developed by  : Urminder Vohra(JDC)
//Date          : August 7, 2013

//Updates       :
//08/28/2013         Urminder(JDC) : updated Troubleshoot and Purchase to insert case on click [T-176778]
//08/28/2013         Urminder(JDC) : added a new record type Refund [T-179346]
//09/06/2013         Urminder(JDC) : remove PS Network Button [T-180792]
//September 13, 2013 KapiL Choudhary(JDC) : Craete a new function "fetchContactId" for the Task T-182942.
//September 13, 2013 Urminder (JDC) : remove all sub categories [T-187979]
//December 30,2013   Noopur(JDC) : added code to show alert for error messages
//******************************************************************************/
public with sharing class ContactButtonsController {
    
  public String caseId{get;set;}
  public String testContactId{get;set;}
  public String errorString {get;set;}
  
  final String RT_TROUBLESHOOT = 'Troubleshoot';
  final String RT_PS_NETWORK = 'PS_Network';
  final String RT_PURCHASE = 'Purchase';
  final String RT_VOUCHER = 'Voucher';
  final String RT_GENERAL = 'General';
  final String RT_REFUND = 'Refund';
  final String RT_SERVICE = 'Service';
  
  map<String, Id> caseRecordTypeMap;
  Id contactId;
  Id recordTypeId;
  Id accountId;
  Contact selectedConsumer;
  public ContactButtonsController(){
    
    string idParam =  ApexPages.currentPage().getParameters().get('id');

    if(idParam != null && idParam != ''){
        selectedConsumer = fetchContactId(idParam);
        contactId = selectedConsumer.Id;
        testContactId = contactId;
        accountId = selectedConsumer.AccountId;
    }

    caseRecordTypeMap = getRecordTypes();
  } 
  
  private map<String, Id> getRecordTypes() {
    map<String, Id> rtMap = new map<String, Id>();
    for(RecordType rt : [select Id, DeveloperName 
                            from RecordType
                            where sObjectType = 'Case']) {
      rtMap.put(rt.DeveloperName, rt.Id);                           
    }
    return rtMap;
  }
  
  private Id insertCase(Id rtId, Id cntId) {
    String contactOrigin = apexpages.currentpage().getparameters().get('UserSource');
    String contactSubject = apexpages.currentpage().getparameters().get('UserSubject');     
    //System.debug('**********************************1: '+ contactOrigin +'**********************************');
    //System.debug('**********************************'+ contactSubject +'**********************************');
    errorString = '';
    Case cs = new Case();
    cs.RecordTypeId = rtId;
    cs.ContactId = cntId;
    cs.AccountId = accountId;            
    if(String.isNotBlank(contactOrigin)){
        cs.Origin = contactOrigin;
    }            
    if(contactSubject != null){
        cs.Subject = contactSubject;
    }
    try{
        if(isUserAddedInPermissionSet('LATAM_Permissions')) {
            if(selectedConsumer.Preferred_Language__c == null) {
                throw new GeneralUtiltyClass.CustomException('Please select a PREFERRED LANGUAGE for this consumer.');
            } else 
            if(selectedConsumer.Preferred_Language__c == 'Spanish' && cs.RecordTypeId <> GeneralUtiltyClass.RT_SERVICE_ID) {
                if(selectedConsumer.LATAM_Country__c == null) {
                    throw new GeneralUtiltyClass.CustomException('LATAM Country must first be selected before creating this case');
                }
            }
        } 
        insert cs;
        if(cs.Origin == 'Chat'){                        
            insertChatTranscriptTempMap(cs.Id);
        }           
    }catch(Exception ex){

        System.debug('======ERROR Occured====' + ex.getMessage());
        errorString = ex.getMessage();
        return null;
    }
    
    return cs.Id;
  }
  
  private void insertChatTranscriptTempMap(Id chatCaseId){      
    String contactChatKeyId = apexpages.currentpage().getparameters().get('UserChatKeyId');
    //System.debug('**********************************'+ contactChatKeyId +'**********************************');
    ChatTranscriptTempMapping__c cttm = new ChatTranscriptTempMapping__c();
    cttm.CaseRefId__c = chatCaseId;
    cttm.ChatKeyRefId__c = contactChatKeyId;
    cttm.ContactRefId__c = contactId;
    try{
        insert cttm;        
    }catch(Exception ex){
        System.debug('======ERROR Occured during insertChatTranscriptTempMap====' + ex.getMessage());               
    }       
  }
  
  public Pagereference refund() {
    
    caseId = insertCase(caseRecordTypeMap.get(RT_REFUND), contactId);
    return null;
  }
  
  public Pagereference service() {
    
    caseId = insertCase(caseRecordTypeMap.get(RT_SERVICE), contactId);
    return null;
  }
  
  public Pagereference troubleshoot() {
    
    caseId = insertCase(caseRecordTypeMap.get(RT_TROUBLESHOOT), contactId);
    return null;
  }
  
  public Pagereference showPSNetwork() {
    
    caseId = insertCase(caseRecordTypeMap.get(RT_PS_NETWORK), contactId);
    return null;
  }
  
  public Pagereference purchase() {
    
    caseId = insertCase(caseRecordTypeMap.get(RT_PURCHASE), contactId);
    return null;
  }
  
  public Pagereference showVoucher() {
    
    caseId = insertCase(caseRecordTypeMap.get(RT_VOUCHER), contactId);
    return null; 
  }
  
  public Pagereference showGeneralSubs() {
    
    caseId = insertCase(caseRecordTypeMap.get(RT_GENERAL), contactId);
    return null;
  }
  
  public Pagereference showDetails() {
    caseId = insertCase(recordTypeId, contactId);
    return null;
  }
  
  private Contact fetchContactId(string accountOrContactId){
    
    Contact foundContact = new Contact();
    
    for(Contact c: [Select AccountId, Id, Preferred_Language__c, LATAM_Country__c from Contact 
            where Id =: accountOrContactId or AccountId =: accountOrContactId]){


        foundContact = c;   

    }

    
    // Called from Contact Page.
    return foundContact;
  }
  
  public boolean getShowServiceButton() {
    return isUserAddedInPermissionSet('SERVICE_CENTER');
  }
  
  public boolean isUserAddedInPermissionSet(String permissionSetName) {
    for(Profile p : [select Name from Profile where Id= :Userinfo.getProfileId()]) {
        if(p.Name == 'System Administrator') {
            if(permissionSetName == 'LATAM_Permissions') return false;
            return true;
        }
    }
    PermissionSet ps = [select Id from PermissionSet where Name = :permissionSetName];
    if(ps <> null) {
        for(PermissionSetAssignment psa : [select AssigneeId from PermissionSetAssignment where PermissionSetId = :ps.Id]) {
            if(psa.AssigneeId == Userinfo.getUserId()) {
                return true;
            }
        }
    }
    return false;
  }
  
}