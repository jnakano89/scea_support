//8/19/2013 Urmidner : created this class.
@isTest(seeAllData = true)
private class PSNAccountsController_Test {

    static testMethod void myUnitTest() {
     //code added by preetu
    
    String RecTypeId= [select Id from RecordType where (Name='Person Account') and (SobjectType='Account')].Id; 
      PSN_Account__C psnAcc1 = [select Id,consumer__r.AccountId,Name from PSN_Account__c where Name='xgamertomx'];
  
  
      Account acc = new Account();
      acc.LastName = 'test';
      acc.FirstName = 'ftest';
      acc.Phone = '12345';
      //acc.PSN_Account__c = psnAcc1.Id;
      acc.RecordTypeID=RecTypeId;
      insert acc;
      
     Contact repContact = [SELECT id,FirstName, LastName
     FROM Contact
     WHERE isPersonAccount = true AND AccountId = :acc.id];
    
    
    
    
     Contact cnct = new Contact();
     cnct.LastName = 'test';
     cnct.FirstName = 'ftest';
     cnct.Phone = '12345';
     insert cnct;
     
     Case testcase = new Case();
     testcase.Status = 'Open';
     testcase.received_date__c = date.today();
     testcase.shipped_date__c = date.today().addDays(1);
     testcase.outbound_tracking_number__c = '101';
     testcase.outbound_carrier__c = 'TYest1';
     testCase.Origin = 'Social';
     testCase.ContactId = cnct.Id;
     testCase.Offender__c = 'Test';
     testCase.Product__c = 'PS4';
     testCase.Sub_Area__c = 'Account Help';
     insert testCase;
     
     PSN_Account__c psnAcc = new PSN_Account__c();
     //psnAcc.Consumer__c = cnct.Id;
     psnAcc.Consumer__c=repContact.id;
     psnAcc.MDM_Account_ID__c = '13456';
     psnAcc.Status__c = 'Test';
     insert psnAcc;
     
     PSNAccountsController ctrl = new PSNAccountsController();
     ctrl.sObjId = testcase.Id;
     ctrl.init();
     
     //System.assert(ctrl.psnAccList.size() <> 0,'PSN account list should not be empty');
     
     psnAcc.Status__c = 'UpdatedTest';
     update psnAcc; 
        
    }
}