//**************************************************************************/
// (c) 2013 Appirio, Inc.
//
// Description:	Controller Class For VF Page GetAssets [T-176717].
//
// Original August 27,2013: KapiL Choudhary(JDC)
//
// Updated :
//			
///**************************************************************************/

public class GetAssetsController {
    
    public String mdmAccountId;
    public String contactId;
    public AssetSearchResult assetSearchResultMember{get;set;}
    
	//Constructor.
	public GetAssetsController(){
		
		mdmAccountId = Apexpages.currentPage().getParameters().get('mdmAccountId');
  		contactId = Apexpages.currentPage().getParameters().get('contactId');
  		assetSearchResultMember = new AssetSearchResult();
	}

	public GetAssetsController(String mdm_AccountId, String sfdc_ContactId){
		
		mdmAccountId = mdm_AccountId;
  		contactId = sfdc_ContactId;
  		assetSearchResultMember = new AssetSearchResult();
	}
	
	public void populateRecords(){
		
		//Get Assets Records 
		  		 
		OSB__c osbSettings = OSB__c.getOrgDefaults();
    	
    	if (osbSettings.Asset_Lookup_Enabled__c){		  		
			fetchAssets();
    	}
		
 
	}
	
	// Get the assets list from AssetServiceUtility class.
	private void fetchAssets(){
		

		try{
			assetSearchResultMember = AssetServiceUtility.getAssets(mdmAccountId);
		}catch(Exception e){
			IntegrationAlert.addException('Get Consumer Assets', 'MDM Account ID='+mdmAccountId+', Contact ID='+contactId, e);
			return;
		}
		
		//Having Search Result MDM key With Initial value true.
		map<String,Boolean> AssetSearchResultMDMIdMap = new map<String,Boolean>();
		
		map<String, Id> MdmIdAssetIdMap = new map<String,Id>();
		
		System.debug('===========assetSearchResultMember========' + assetSearchResultMember);
		
		for(Sony_MiddlewareAssetdata.Asset aSRAsset :assetSearchResultMember.listOfAssests){
		
			if(aSRAsset.MDMAssetRowId != null && aSRAsset.MDMAssetRowId != ''){
				AssetSearchResultMDMIdMap.put(aSRAsset.MDMAssetRowId,true);
			}
		}
		
		System.debug('===========AssetSearchResultMDMIdMap========' + AssetSearchResultMDMIdMap);
		
		System.debug('>>> AssetSearchResultMDMIdMap.size()=' + AssetSearchResultMDMIdMap.size());
		
		if(!AssetSearchResultMDMIdMap.isEmpty()){
			
			list<Asset__c>newAssetsList = new list<Asset__c>();
			list<Asset__c> existingAssetList = new list<Asset__c>();
			
			for(Asset__c asset : [select MDM_ID__c, Serial_Number__c 
									from Asset__c 
									where MDM_ID__c in :AssetSearchResultMDMIdMap.keySet()]){
										
				//Already an asset found with this MDM_ID__c id. Set the boolean value to false 
				//so that duplicate Asset will not be created.
				
				if(AssetSearchResultMDMIdMap.containsKey(asset.MDM_ID__c)){
					AssetSearchResultMDMIdMap.put(asset.MDM_ID__c,false);
					MdmIdAssetIdMap.put(asset.MDM_ID__c, asset.Id);
				} 

			}//end-for
			
			for(Sony_MiddlewareAssetdata.Asset aSRAsset :assetSearchResultMember.listOfAssests){
				
				if(aSRAsset.MDMAssetRowId != null && aSRAsset.MDMAssetRowId != ''){
				
					Asset__c asset;
					
					if(AssetSearchResultMDMIdMap.containsKey(aSRAsset.MDMAssetRowId) && 
								(!AssetSearchResultMDMIdMap.get(aSRAsset.MDMAssetRowId))){
						
						System.debug('>>>>existing asset');			
						asset = new Asset__c(Id = MdmIdAssetIdMap.get(aSRAsset.MDMAssetRowId));
						existingAssetList.add(asset);
						
					} else {
						System.debug('>>>>new asset');	
						asset = new Asset__c();
						newAssetsList.add(asset);
					}
					
					system.debug('aSRAsset>>>> '+aSRAsset);    
					asset.MDM_Account_ID__c     = mdmAccountId;
					asset.MDM_ID__c 			= aSRAsset.MDMAssetRowId;
					asset.Console_Type__c 		= aSRAsset.ConsoleType;
					
					if(aSRAsset.PurchaseDate!=null && aSRAsset.PurchaseDate!=''){
						asset.Purchase_Date__c 		= parseDate(aSRAsset.PurchaseDate);
					}
					asset.Console_ID__c 		= aSRAsset.ConsoleID;
					asset.Registration_Date__c  = parseDate(aSRAsset.RegistrationDate);
					asset.Model_Number__c   	= aSRAsset.ModelNumber;
					asset.Serial_Number__c  	= aSRAsset.SerialNumber;
					asset.Software_Title__c 	= aSRAsset.TitleName;
					asset.Publisher__c			= aSRAsset.Publisher;
					asset.Party_Type__c			= aSRAsset.PartyType;
					asset.Game_Type__c			= aSRAsset.GameType;
					asset.Platform__c			= aSRAsset.Platform;
					asset.Release_Date__c 		= parseDate(aSRAsset.ReleaseDate);
					asset.Genre__c				= aSRAsset.Genre;
					asset.Franchise__c			= aSRAsset.Franchise;
					asset.Publisher_Name__c 	= aSRAsset.PublisherName;
					asset.Title_Name__c			= aSRAsset.TitleName;
					asset.Product_ID__c 		= aSRAsset.ProductID;
					asset.Content_Type__c  		= aSRAsset.ContentType;
					asset.Move_Compatible__c 	= aSRAsset.MoveCompatible;
					asset.ESRB_Rating__c		= aSRAsset.ESRBRating;
					asset.Sub_Genre__c			= aSRAsset.Sub_Genre;
					asset.In_Game_Purchase_Flag__c = aSRAsset.In_GamePurchaseFlag;
					
					system.debug('aSRAsset>> '+asset);
				}
			}
			
			try{
 
   				if(!newAssetsList.isEmpty()){
					system.debug('New Asset>>>> '+newAssetsList);
					insert newAssetsList;					
   				}
   				if(!existingAssetList.isEmpty()){
					system.debug('Old Asset>>>> '+existingAssetList);
					update existingAssetList;
   				}
			}
			catch(Exception ex){
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Asset could not inserted reason :'+ex.getMessage());
				System.debug('Asset could not inserted reason :' + ex.getMessage());
				return;
			}
			
			System.debug(existingAssetList.size() + '===========existingAssetList=========' + existingAssetList);
			System.debug(newAssetsList.size() + '===========newAssetsList=========' + newAssetsList);
			
			if(!existingAssetList.isEmpty() || !newAssetsList.isEmpty()){
				newAssetsList.addAll(existingAssetList);
				createJunctionObject(newAssetsList);
			}
			
		}	
	}
	
	private void createJunctionObject(list<Asset__c> newAssetsList ){
		set<Id> existingConsumerAssetSet = new set<Id>();
		
		for(Consumer_Asset__c coAsset : [select id,Asset__c from Consumer_Asset__c where consumer__c=:contactId and Asset__c in: newAssetsList]){
			existingConsumerAssetSet.add(coAsset.Asset__c);
		}
		
		list<Consumer_Asset__c> consumerAssetList = new list<Consumer_Asset__c>();
		
		for(Asset__c asset: newAssetsList){
			if(!existingConsumerAssetSet.contains(asset.id)){
				consumerAssetList.add(new Consumer_Asset__c(consumer__c = contactId,Asset__c = asset.id));
			}
	    }
	    try{
	    	if(!consumerAssetList.isEmpty())
			insert consumerAssetList;
			system.debug('New Consumer Asset>>>> '+consumerAssetList);
		}
		catch(exception ex){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
		}
	}
	
	
	private Date parseDate(String dateString){ // dateString is in this format "12/26/2007".
		system.debug('dateString>>>> '+dateString);
		if(dateString != null && dateString !=''){
			try{
				date assetDate = Date.parse(dateString);
				return assetDate;
			}
			catch(Exception ex){
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Date Format Is InValid : '+dateString);
			}
		}
		return null;
	}
	
	
 
    
    
}