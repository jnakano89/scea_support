//8/27/2013 : Urminder : created this class.
public class AssetSearchResult {
	public String ErrorMessage;
	public String ErrorDescription;
	public String ErrorCode;
	public String MDMAccountRowId;
    public list<Sony_MiddlewareAssetdata.Asset> listOfAssests;
    
    public AssetSearchResult(){
    	listOfAssests = new list<Sony_MiddlewareAssetdata.Asset>();
    }
}