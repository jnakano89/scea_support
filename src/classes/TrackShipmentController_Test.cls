//8/1/2013 : UV : created this test class.
@isTest
private class TrackShipmentController_Test {

    static testMethod void myUnitTest() {
        Account acc = new Account();   
        acc.LastName = 'Test Acc ';
        insert acc;
        
        Contact cnt = new Contact();
        cnt.LastName = 'Test Contact';
        //cnt.AccountId = acc.id;
        insert cnt;
        
        Case testCase = new Case();
        testCase.ContactId = cnt.Id;
        testCase.AccountId = acc.Id;
        testCase.Offender__c = 'test';
        testCase.Product__c = 'PS4';
        testCase.Sub_Area__c = 'Account Help';
        insert testCase;
        
        map<String, String> urlMap = ApexPages.currentPage().getParameters();
        urlMap.put('CaseId', testCase.Id);
        urlMap.put('TrackingNumber', '1234567890');
        urlMap.put('Carrier', 'FedEx');
        TrackShipmentController ctrl = new TrackShipmentController();
        System.assert(ctrl.iframeSrc.contains('fedex'),'IFrame with fedex tracking should be loaded');
        
        Pagereference pg = ctrl.returnToCase();
        System.assert(pg.getUrl().contains(testCase.Id),'This should return to case Id');
        
        urlMap.put('Carrier', 'USPS');
        TrackShipmentController ctrl2 = new TrackShipmentController();
        System.assert(ctrl2.iframeSrc.contains('usps'),'IFrame with usps tracking should be loaded');
        
        urlMap.put('Carrier', 'Purolator');
        TrackShipmentController ctrl3 = new TrackShipmentController();
        System.assert(ctrl3.iframeSrc.contains('purolator'),'IFrame with Purolator tracking should be loaded');  
      
    }
}