/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ChatTranscTest {

    static testMethod void liveTranscriptTest() {
        // TO DO: implement unit test
        Contact testContact = TestClassUtility.createContact(1,  true)[0];
        ApexPages.currentPage().getParameters().put('id', testContact.Id);
        ApexPages.currentPage().getParameters().put('UserChatKeyId', 'Random1');
        ApexPages.currentPage().getParameters().put('UserSource', 'Chat');
        ContactButtonsController controller = new ContactButtonsController();
        //controller.resetPassword();        
        controller.refund();
        list<Case> lstCases = [select ID from case where ContactId =: testContact.ID];
        system.assert(lstCases.size() > 0);
        LiveChatVisitor lcv = new LiveChatVisitor();
        insert lcv;
        
        LiveChatTranscript lct = new LiveChatTranscript(ChatKey='Random1',LiveChatVisitorId=lcv.Id);
        insert lct;
        lct = [select Id, ContactId from LiveChatTranscript where Id = :lct.Id];
        System.debug('########################'+lct+'####################################');
        //system.assert(String.isNotBlank(lct.ContactId));
    }
}