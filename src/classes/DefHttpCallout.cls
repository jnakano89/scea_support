//JG - 2012-Aug-29 - code for mock testing http found at http://www.tgerm.com/2011/09/testing-apex-webservice-callout.html

/**
  Default implementation meant for use in actual apex code. It runs out of 
  standard Apex Http Class (http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_classes_restful_http_http.htm)
*/
public with sharing class DefHttpCallout implements IHttpCallout {
  public IHttpResponse send(HttpRequest req) {
    Http http = new Http();
    return new DefHttpResponse(http.send(req));
  }
}