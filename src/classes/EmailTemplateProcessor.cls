public class EmailTemplateProcessor {
	public string templateBody{get;set;}
	private string plainTextBody{get;set;}
	private string htmlTextBody{get;set;}
	private string htmlPdfBody{get;set;}
	map<string, objectFieldToken>objectFieldMap{get;set;}
	map<string, string>mapObjetctQuery{get;set;}

	public EmailTemplateProcessor(string templateBodyP){
		templateBody = templateBodyP;
		objectFieldMap = new map<string, objectFieldToken>();
		mapObjetctQuery =new map<string, string>();
	}
	public string fillTemplate(map<string, string>Object_Name_clause, string letterHeadValue){
		return fillTemplateEmail(Object_Name_clause, null, letterHeadValue);
	}	
	public string fillTemplate(map<string, string>Object_Name_clause){
		return fillTemplateEmail(Object_Name_clause, null, null);
	}	
	private static map <String, Map<String, Schema.SObjectField>>sObjectMap= new map<String, Map<String, Schema.SObjectField>>();
	private static map<String, Schema.DescribeFieldResult> sObjectFieldMap= new map<String, Schema.DescribeFieldResult>();
	public  Schema.DescribeFieldResult getField(string ObjectName, String FieldName){
		Schema.DescribeFieldResult fd = null;
		if(ObjectName!=null && ObjectName.trim()!='' && FieldName!=null && FieldName.trim()!=''){
			ObjectName = ObjectName.toLowerCase().trim();
			FieldName = FieldName.toLowerCase().trim();
			string key_sObjectFieldMap = ObjectName+'^-^'+FieldName; 
			fd = sObjectFieldMap.get(key_sObjectFieldMap);
			if(fd==null){
				Map<String, Schema.SObjectField> fMap= sObjectMap.get(ObjectName); 
				if(fMap==null){
					if(Schema.getGlobalDescribe().get(ObjectName) != null){
					fMap = Schema.getGlobalDescribe().get(ObjectName).getDescribe().Fields.getMap();
					if(fMap!=null){
						sObjectMap.put(ObjectName, fMap);						
					}
					}
				}
				if(fMap!=null){
					Schema.SObjectField fld=fMap.get(FieldName);
					if(fld!=null){
						fd = fld.getDescribe();
						if(fd!=null){
							sObjectFieldMap.put(key_sObjectFieldMap, fd);						
						}
					}
				}
			}
		}
		return fd;
	}

	public string getPlainTextBody(){
		return plainTextBody;
	}
	public string getHtmlBody(){
		return htmlTextBody;
	}
	public string getPdfBody(){
		return htmlPdfBody;
	}	
	public string fillTemplateEmail(map<string, string>Object_Name_clause, map<string, string>mapAliasNames, string letterHeadValue){
		plainTextBody=fillTemplate(Object_Name_clause, mapAliasNames);
		string templateParsed=plainTextBody+'';
				
		if(letterHeadValue!=null && letterHeadValue.trim()!=''){
				String EmailBody = '';
	            String imageurl='';
				system.debug('====>letter'+letterHeadValue.indexOf('headerImage',0)+'test'+letterHeadValue.indexOf('[CDATA[',0));
				if(letterHeadValue.indexOf('headerImage',0)>0){   
				    Integer starttag = letterHeadValue.indexOf('[CDATA[',letterHeadValue.indexOf('headerImage',0)) ; 
				    Integer endtag  =  letterHeadValue.indexOf(']',starttag+6);
				    imageurl =   letterHeadValue.substring(starttag+7,endtag);          
					system.debug('====>letter'+imageurl);	
					
				}
				
				if(imageurl != ''){
					EmailBody ='<img id="r1sp1" blabel="headerImage" beditid="r1sp1" src="'+imageurl+'"/><br/><br/>';
				}                
				//EmailBody +='</tr></tbody></table></td></tr><tr valign="top"><td style="height: 0pt; background-color: rgb(170, 170, 255);"></td></tr><tr valign="top"><td style="vertical-align: top; height: 100px; text-align: left; background-color: rgb(255, 255, 255);"></td></tr><tr valign="top"><td style="height: 0pt; background-color: rgb(170, 170, 255);"></td></tr></tbody></table></center>';
				EmailBody += templateParsed;
				 system.debug('***'+EmailBody);
				 //Code added by Varun
				 //system.assertEquals(0,1,'**Varun*'+letterHeadValue);
				  String FooterImageUrl='';
				  if(letterHeadValue.indexOf('footerImage',0)>0){   
				            Integer starttag = letterHeadValue.indexOf('[CDATA[',letterHeadValue.indexOf('footerImage',0)) ; 
				            Integer endtag  =  letterHeadValue.indexOf(']',starttag+6);
				            FooterImageUrl =   letterHeadValue.substring(starttag+7,endtag);          
				        	//  system.assertEquals(0,1,'====>letter'+FooterImageUrl);	
				        }
				      
				   if(FooterImageUrl != ''){
				   	    EmailBody +='<br/><br/><img id="r1sp1" blabel="footerImage" beditid="r1sp1" src="'+FooterImageUrl+'"/><br/><br/>';
				   }
				   //templateParsed = EmailBody;
				   templateParsed= EmailBody; 			
		}
		
		string pdfStyle='font-family:verdana,sans-serif;font-size:11px;';
		string EmailStyle='font-family:verdana,sans-serif;font-size:10px;';
		//CommunicationSettings__c CommunicationSettings =CommunicationSettings__c.getInstance();
		//if(CommunicationSettings!=null && CommunicationSettings.EmailStyle__c!=null && CommunicationSettings.EmailStyle__c.trim()!=''){
		//	EmailStyle = CommunicationSettings.EmailStyle__c;
		//}
		//if(CommunicationSettings!=null && CommunicationSettings.PdfStyle__c!=null && CommunicationSettings.PdfStyle__c.trim()!=''){
		//	pdfStyle = CommunicationSettings.PdfStyle__c;
		//}
		htmlPdfBody='<body style="'+pdfStyle+'">'+templateParsed+'</body>';
		htmlTextBody ='<body style="'+EmailStyle+'">'+templateParsed+'</body>';		
		return getHtmlBody();	
	}
	public string fillTemplate(map<string, string>Object_Name_clause, map<string, string>mapAliasNames){
		system.debug('++++++++++1:'+Object_Name_clause);
		map<string, List<sObject>>mapsobj = new map<string, List<sObject>>(); 
		//Make Query to get the objects.
		string filledTokenTemplateBody= templateBody;		
		for(string objectName:Object_Name_clause.keySet()){
			system.debug('++++++++++2:'+objectName);
			string objectNameNomalized = objectName.toLowerCase().trim();
			string queryBuilder = mapObjetctQuery.get(objectNameNomalized);
			string objectQueryClause=Object_Name_clause.get(objectName);
			system.debug('++++++++++3:'+objectQueryClause+', queryBuilder:'+queryBuilder);
			string actualObjectName=objectNameNomalized;
			if(queryBuilder!=null && queryBuilder!=''){
				if(mapAliasNames!=null){
					actualObjectName= mapAliasNames.get(objectNameNomalized);
					if(actualObjectName==null || actualObjectName==''){
						actualObjectName= objectNameNomalized;
					}
				}
				queryBuilder = 'select '+queryBuilder+' from '+actualObjectName+' where '+objectQueryClause +' limit 1';
				system.debug('++++++++++:'+queryBuilder);
				List<sObject> objects= Database.query(queryBuilder);
				if(objects.size()>0){
					//filledTokenTemplateBody = objects
					mapsobj.put(objectNameNomalized, objects);
				}
			}
		}
		if(mapsobj.size()>0){
			for(objectFieldToken tkn:objectFieldMap.values()){
				String value;
				string objectName=tkn.objectName;
				/*
				if(mapAliasNames!=null){
					string actualObjectName= mapAliasNames.get(objectName);
					if(actualObjectName!=null && actualObjectName!=''){
						objectName=actualObjectName;
					}
				}*/
				system.debug('****:::'+objectName+'::'+tkn.objectName);
				
				List<sObject> objects = mapsobj.get(objectName);				
				if(objects!=null && objects.size()>0){
					system.debug('****:1::'+objectName+'::'+tkn.objectName+' >>'+tkn.FieldSQL+' >>'+tkn.Field);
					sObject Val = objects[0];  
            		//system.debug('******rel Val:1'+tkn.objectNames+' >> '+Val);
            		String FieldName = tkn.FieldSql;
            		if(tkn.fieldParts.size()>1){
                		for(Integer nNestedObj=0; nNestedObj< tkn.fieldParts.size()-1; nNestedObj++){
                			String nestedObj = tkn.fieldParts[nNestedObj];
                			if(Val==null){
                				break;
                			}else{
                				system.debug('******rel Val:2'+nestedObj+' >> '+tkn.objectName);	                        				
                				Val = Val.getSObject(nestedObj);
                				system.debug('******rel Val:3'+nestedObj+' >> '+val);
                			}
                		}
                		FieldName = tkn.fieldParts[tkn.fieldParts.size() - 1];
            		}
	                if(Val!=null){
	                  			system.debug('******rel Val:4'+FieldName+'!'+tkn.fieldParts+'@@'+ tkn.fieldParts[tkn.fieldParts.size() - 1]+' :: '+Val);
	                        value = String.valueOf(Val.get(FieldName));
	                        //tkn.tokenText
	                 }
					//fieldParts
					//string objectValue = getValue(objects[0], tkn.Field);
					if(filledTokenTemplateBody != null && tkn.tokenText != null && value != null){
					    filledTokenTemplateBody = filledTokenTemplateBody.replace(tkn.tokenText, value);
				    }
				}
			}
		}
		system.debug('++++++*(*(*(*()))):'+filledTokenTemplateBody);
		return filledTokenTemplateBody;
	}/*
	public static string getValue(sObject aObject, string FieldName){		
		Integer lookup = FieldName.indexOf('.');
		while(lookup!=-1){
			if(FieldName!=null && FieldName.trim()!=''){								
				integer objectIndex = FieldName.indexOf('.');
				if(objectIndex!=-1){  
					string objectName = FieldName.substring(objectIndex).toLowerCase().trim();
					aObject = aObject.getSObject(objectName);
				}else{
					break;
				}
				if(FieldName.length()>(objectIndex+1)){
					FieldName= FieldName.substring(objectIndex+2, FieldName.length()-1).toLowerCase().trim();
				}
			}			
		}
		if(aObject!=null && FieldName!=null && aObject.get(FieldName)!=null){
			return aObject.get(FieldName)+'';
		}
		return '';
	}*/
	public void parseTemplate(){
		if(templateBody==null || templateBody.trim()==''){
			return;
		}
		string parseTexttemplate = templateBody;
		while(parseTexttemplate!=null && parseTexttemplate.length()>0){
			string zToken=null;
			integer tokenStart= parseTexttemplate.indexOf('{!');
			if(tokenStart!=-1){
				integer tokenEnd = parseTexttemplate.indexOf('}', tokenStart);
				if(tokenEnd!=-1){
					//Extracted the token.
					zToken= parseTexttemplate.substring(tokenStart, tokenEnd+1).trim();
					objectFieldToken token = objectFieldMap.get(zToken);
					if(token==null){ 
						token = new objectFieldToken(zToken);
					}					
					if(token.tkn_key!=null){
						objectFieldMap.put(token.tkn_key, token);
						string queryBuilder = mapObjetctQuery.get(token.objectName);
						system.debug('********---:'+token.Field+'::'+token.objectName);		
						if(queryBuilder==''||queryBuilder==null){
							Schema.DescribeFieldResult fd= getField(token.objectName, token.FieldSQL);
							if(fd!=null){
								system.debug(token.FieldSQL+'********fd1:'+fd);
							}
							queryBuilder = token.FieldSQL+' ';
							//find out if special-field type
						}else{
							if(queryBuilder.indexOf(token.Field+' ')== -1){
								Schema.DescribeFieldResult fd= getField(token.objectName, token.FieldSQL);
								if(fd!=null){
									system.debug(token.FieldSQL+'********fd2:'+fd);
								}
								queryBuilder = queryBuilder+','+token.FieldSQL+' ';
							}
						}
						system.debug('++++++::'+mapObjetctQuery);
						mapObjetctQuery.put(token.objectName, queryBuilder);
					}
					if(zToken!=null && zToken!='' && parseTexttemplate.length()>(tokenEnd+3)){
						parseTexttemplate=parseTexttemplate.substring(tokenEnd+1);
					}else{
						parseTexttemplate='';
					}
				}
			}
			if(zToken==null){	
				parseTexttemplate= null;
			}
		}
	}
	public class objectFieldToken{		
		public string tokenText{get;set;}
		public string objectName{get;set;}
		public List<string>fieldParts{get;set;}
		public string Field{get;set;}
		public string FieldSQL{get;set;}
		public string tkn_key{
			get{
				if(objectName==null || objectName.trim()=='' || Field==null || Field.trim()==''){					
					return null;
				}				
				return (objectName+'.'+Field).toLowerCase().trim();
			}
		}
		public objectFieldToken(string token){
			addToken(token);				
		}
		public void addToken(string token){									
			if(token!=null && token.trim()!=''){
				tokenText= token;				
				integer objectIndex = token.indexOf('.');
				if(objectIndex!=-1){  
					objectName = token.substring(2, objectIndex).toLowerCase().trim();
				}
				if(token.length()>(objectIndex+1)){
					Field= token.substring(objectIndex+1, token.length()-1).toLowerCase().trim();
				}
				if('case'.equalsIgnoreCase(objectName)){
					if('contact'.equalsIgnoreCase(Field)){
						FieldSql= 'contact.name';
					}
				}
				if(String.isEmpty(FieldSql)){
					FieldSql = Field;
				}

				if(!String.isEmpty(FieldSQL)){
					fieldParts = FieldSQL.split('\\.');
				}			

			}			
		}
	}
	@IsTest public static void Test_EmailTemplateProcessor() {
		string sampleMail= 'Dear Mr./Ms. {!Contact.LastName},'+
		'Thank you for bringing the matter at our restaurant to our attention. I would like to follow up with you directly on the telephone because at Chipotle we take this matter very seriously. Please provide me with your telephone number at your earliest convenience.'+
		'Sincerely,'+
		'{!User.FirstName}{!User.LastName}'+
		'{!User.FirstName}{!User.LastName} | {!User.Title}'+
		'Chipotle Mexican Grill';
		EmailTemplateProcessor tplProcer= new EmailTemplateProcessor(sampleMail);
		tplProcer.parseTemplate();
        List<User> otherUserList = [Select Id from User where Id <> :UserInfo.getUserId() and IsActive = true limit 1];
        User otherUser = null;
        if(otherUserList.size() == 0)
        {
            List<Profile> prof = [Select Id from Profile limit 1]; 
            otherUser = new User(FirstName='First Name _ Test', LastName='Last Name _ Test', Title='Title Test', Alias='testxyz1',emailencodingkey='UTF-8', languagelocalekey='en_US',timezonesidkey='America/Los_Angeles',  CommunityNickname='XYZTEST12301', Email='ranjeet.singh1@metacube.com', username='UnitTest123fake@testfake.com',localesidkey='en_US', ProfileId=prof.get(0).Id);
            insert otherUser;
        }else
        {
            otherUser = otherUserList.get(0);
        }
		Contact con = new Contact();
       	con.LastName = 'contLastName';              
       	insert con ;
        map<string, string>mapObject_Name_Id = new map<string, string>();
        mapObject_Name_Id.put('user', ' Id =\''+otherUser.id+'\''); 
        mapObject_Name_Id.put('contact', ' Id =\''+con.id+'\'');
        tplProcer.fillTemplate(mapObject_Name_Id);
	}
}