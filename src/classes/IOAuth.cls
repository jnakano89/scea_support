public interface IOAuth {
	void setPrivateKey(String pk);
	void setConsumerKey(String ck);
	void setToken(String tk);
	
	void sign(HttpRequest req, String intuitTid, String intuitRequestid);  
	void sign(HttpRequest req, map<String, object>param);
}