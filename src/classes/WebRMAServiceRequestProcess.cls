/***************************************************************************************************
Class         : WebRMAServiceRequestProcess
Description   : A webservice class to create a new service case from WebRMA
Developed by  : Appirio
Date          : ##/##/2014
Task          : T-166690
Modified      :
Aaron Briggs - 04/01/2014 - SF-371 - Changed Status to Ready for Service to Trigger MTC/PEC Process
***************************************************************************************************/
global class WebRMAServiceRequestProcess {

	global class ServiceRequestInput{

		webservice string systemType;               // *[Asset__c.Product_Genre__c]
		webservice string modelNumber;          // * [Asset__c.Model_Number__c]
		webservice string serialNumber;         //* [Asset__c.Serial_Number__c]
		webservice Date purchaseDate;               //* [Asset__c.Purchase_Date__c]

		webservice string External_SR_Number;   // *[Case.External_SR_Number__c]
		webservice string issueType;                // *[case.Sub_Area__c]
		webservice string issueDescription;     // * [Case.Feed]
		webservice string warrantyStatus;       // * [Case.Status]

		webservice string firstName;                // *[Contacts.FirstName]
		webservice string lastName;             // * [Contacts.LastName]
		webservice string phoneNumber;          // * [Contacts.Phone]
		webservice string email;                    // * [Contacts.Email]

		// COMMENTED BY CHARU. For cybersource use External_SR_Number instead of Order Number
		//webservice string Order_Number;           // (Order.External_Order_Number__c)
		webservice Date Target_Date;            // (Order.Order_Date__c)

		webservice string serviceCost;          // * [Payment.Amount]
		//      webservice string paymentToken;         // [Payment__c.Token__c] 
		//      webservice string consumerAccount;      // [Payment__c.Primary_Account_Number__c]
		//      webservice string transactionNumber;    // [Payment__c.Transaction_Number__c]

		webservice string shippingAddress1;     //* [Address__c.Address_Line_1__c]
		webservice string shippingAddress2;
		webservice string shippingCity;
		webservice string shippingState;
		webservice string shippingZipcode;
		webservice string shippingCountry;
	}

	global class ServiceRequestResponse{
		webservice string serviceCaseNumber;
		webservice ServiceRequestFailureResponse failure;   
	}

	global class ServiceRequestCreateCaseResponse{
		webservice string status;
		webservice ServiceRequestFailureResponse failure;   
	}

	global class ServiceRequestFailureResponse{   
		webservice string message;
		webservice string detailMessage;
		//webservice EmailAlertWrapper email;
	}

	webservice static ServiceRequestResponse searchExistingServiceCase(String serviceCaseNumber){
		ServiceRequestResponse response = new ServiceRequestResponse();
		List<Case> Cases = [select id,External_Case_Number__c, External_SR_Number__c, Asset__r.Serial_Number__c from Case where RecordType.name='Service' and Asset__r.Serial_Number__c =: serviceCaseNumber limit 1];
		if(Cases.size()>0){
			response.serviceCaseNumber = Cases[0].External_Case_Number__c;
		}else{
			response.failure = new ServiceRequestFailureResponse();
			response.failure.message='No Match found for  \''+serviceCaseNumber+'\'';
		}
		return response;
	} 

	webservice static ServiceRequestCreateCaseResponse createServiceRequestCase(ServiceRequestInput request){

		System.Debug('>>>>>>>>> WebRMA Request<<<<<<<<<<<<<<  ' +request);
		ServiceRequestCreateCaseResponse response = new ServiceRequestCreateCaseResponse();
		response.status = 'Failed';
		String errorDetails = '';
		Savepoint sp = Database.setSavepoint();
		try{
			if(String.isEmpty(System.Label.Email_For_Sony_Integration_Error)){
				CyberSourceUtility.failureEmailAlert(response, request, 'Unable to Complete Transaction. Please configure the email address at label \'Email_For_Sony_Integration_Error\'.', null);
				return response;
			}else{
				if(request!=null){
					system.debug('*******???? : '+response.status);
					if(String.isEmpty(request.External_SR_Number)){
						response.status = 'Failed';
						CyberSourceUtility.failureEmailAlert(response, request, 'External_SR_Number required', null);
						//system.debug('**********>>'+response);
						//system.debug('**********>>'+response.failure.message);
						return response;
					}
					system.debug('*******!!!! : '+response.status);
					//T-242381
					Decimal serviceCost =0;
					if(!String.IsEmpty(request.serviceCost )){
						try{
							serviceCost = Decimal.valueOf(request.serviceCost);
						}catch(Exception Ex){
							system.debug('*******Exception : '+Ex);
							response.status = 'Failed';
							CyberSourceUtility.failureEmailAlert(response, request, 'Invalid "service cost" :'+request.serviceCost, null);
							return response;
						}
					}
					system.debug('*******serviceCost : '+serviceCost+ response.status);



					List<Address__c>addresses = new List<Address__c>();

					//Contact contact = sObjectUtility.getContact( request.firstName, request.lastName, request.phoneNumber, request.email);

					Account AccConsumer = null;
					try{
						AccConsumer = sObjectUtility.getAccConsumer( request.firstName, request.lastName, request.phoneNumber, request.email);
					}catch (Exception e){
						response.status = 'Failed';
						CyberSourceUtility.failureEmailAlert(response, request, 'Unable to find or create Consumer 1.', e);
						return response;
					}      
					if(AccConsumer==null || AccConsumer.id ==null){
						response.status = 'Failed';
						CyberSourceUtility.failureEmailAlert(response, request, 'Unable to find or create Consumer 2.'+AccConsumer, null);
						return response;
					}

					AccConsumer= [Select Id, ispersonaccount, PersoncontactId from Account where Id=:AccConsumer.Id][0]; 
					if(AccConsumer.PersonContactId == null){
						response.status = 'Failed';
						CyberSourceUtility.failureEmailAlert(response, request, 'Unable to find Consumer for Account-Id:'+AccConsumer.id, null);
						return response;
					}
					System.Debug('>>>>>>AccConsumer<<<<<<   ' +AccConsumer);
					System.Debug('>>>>>>AccConsumer.PersonContactId<<<<<<   ' +AccConsumer.PersonContactId);

					Address__c shipToAddress = sObjectUtility.getAddress(AccConsumer.PersonContactId, request.shippingAddress1,  request.shippingAddress2,  request.shippingCity,   request.shippingState, request.shippingCountry, request.shippingZipCode, 'Shipping');
					if(shipToAddress!=null){
						addresses.add(shipToAddress);
					}
					if(addresses.size()>0){
						try{
							upsert addresses;
						}catch (Exception e){
							response.status = 'Failed';
							CyberSourceUtility.failureEmailAlert(response, request, 'Unable to update Address.', e);
							return response;
						}  
					}

					if(AccConsumer.PersonContactId<>null && shipToAddress<>null){

						AccConsumer.Ship_To__pc=addresses[0].id;
					}


					Asset__c asset = null;
					try{
						asset = sObjectUtility.getAsset(request, AccConsumer);
					}catch (Exception e){
						response.status = 'Failed';
						CyberSourceUtility.failureEmailAlert(response, request, 'Unable to create asset/Asset-Consumer.', e);
						return response;
					}  

					if(asset==null || asset.id==null){
						response.status = 'Failed';
						CyberSourceUtility.failureEmailAlert(response, request, 'Unable to create asset.', null);
						return response;
					}					

					// Asset__c asset;
					system.debug('***********asset.>>'+asset.Product__c);
					Case objCase = null;
					try{
						objCase = sObjectUtility.getCase(request, shipToAddress, AccConsumer.PersoncontactId, asset);
					}catch (Exception e){
						response.status = 'Failed';
						CyberSourceUtility.failureEmailAlert(response, request, 'Unable to create Case.', e);
						return response;
					}  
					if(objCase==null || objCase.id==null){
						response.status = 'Failed';
						CyberSourceUtility.failureEmailAlert(response, request, 'Unable to create Case.', null);
						return response;
					}					

					//Case ct =new Case(id= objCase.id);
					//ct.ContactId = objCase.Contact.id;
					//update ct;
					system.debug('*******objCase : '+objCase);
					if(objCase!=null && objCase.id!=null){

						objCase.External_SR_Number__c = request.External_SR_Number;
						// Added By CM for WebRMA case creation and updating Case with  Origin Source.
						if(serviceCost>0){
							objCase.Fee_Type__c = 'Out Of Warranty';
						}else{
							objCase.Fee_Type__c = 'In Warranty';
						}
						//Need to update Case, as needs to to be set for Case.
						try{
							objCase.Origin ='Web';
							//Set the flag, if Case created from RMA service.
							objCase.IsRMAOrigin__c = true;
							
							//AB - Changed Default Status to Ready for Service
							objCase.Status='Ready for Service';

							if(shiptoAddress<>null){
								objCase.Ship_to__c=addresses[0].id;
							}

							update objCase;
						} catch (Exception e){
							response.status = 'Failed';
							CyberSourceUtility.failureEmailAlert(response, request, 'Unable to create Case:', e);
							return response;
						}                   	
						//objCase.External_SR_Number__c= 'W-' + objCase.CaseNumber;



						if(serviceCost>0){
							Order__c order = null;
							try{
								order = sObjectUtility.getOrder(request, objCase, asset, shipToAddress, AccConsumer, 'SFDC');
							} catch (Exception e){
								response.status = 'Failed';
								CyberSourceUtility.failureEmailAlert(response, request, 'Unable to create Order.', e);
								return response;
							}
							if(order==null || order.id == null){
								response.status = 'Failed';
								CyberSourceUtility.failureEmailAlert(response, request, 'Unable to create Order.', null);
								return response;

							}
							system.debug('*******Order>>'+order.id);
							try{
								objCase.Order__c=order.id;
								update objCase;

							} catch (Exception e){
								response.status = 'Failed';
								CyberSourceUtility.failureEmailAlert(response, request, 'Unable to update Case:', e);
								return response;
							}                 
							response.status = 'Success';//.serviceCaseNumber = 'W-' + cases[0].CaseNumber;
							system.debug('***********asset.Product__c>>'+asset.Product__c+'order:'+order);
							//After APEX DML,  callout not allowed so needs to be done on different thread/Future,
							actionOnCyberSourceFurture(order.id, AccConsumer.PersonContactid, AccConsumer.id, asset.Product__c, request.External_SR_Number, objCase.id, JSON.serialize(request));

						}else{

							response.status = 'Success';
						}


						return response; 
					}
				}
				system.debug('*******<><><> : ' +response.status);
				CyberSourceUtility.failureEmailAlert(response, request, '', null);
			}
		}catch(Exception Ex){			
			CyberSourceUtility.failureEmailAlert(response, request, '', Ex);
		}
		finally{
			if('Failed'.equalsIgnoreCase(response.status)){
				Database.rollback(sp);
			}
		}
		return response;
	}


	/*
        Actions needs to be performed after CyberSource success.

        Create Payment__c SFDC sObject from input request.
            webservice string serviceCost;          // [Payment.Amount]
            webservice string paymentToken;         // [Payment__c.Token__c] 
            webservice string consumerAccount;      // [Payment__c.Primary_Account_Number__c]
            webservice string transactionNumber;    // [Payment__c.Transaction_Number__c]
	 */


	@future (callout=true)
	public static void actionOnCyberSourceFurture(Id OrderId, Id ContactId, Id ConsumerId, Id ProductId, String orderNo, Id CaseId, String responseJSON){
		actionOnCyberSource(OrderId, ContactId, ConsumerId, ProductId, orderNo, CaseId, responseJSON);
	}


	public static void actionOnCyberSource(Id OrderId, Id ContactId, Id ConsumerId, Id ProductId, String orderNo, Id CaseId, String responseJSON){

		String errorText = 'Unable to Complete Transaction. Cybersource API call failed.';

		WebRMAServiceRequestProcess.ServiceRequestInput request = (WebRMAServiceRequestProcess.ServiceRequestInput)JSON.deserialize(responseJSON, WebRMAServiceRequestProcess.ServiceRequestInput.class) ;

		try{

			Payment__c payment = sObjectUtility.getPaymentFromCybersource_New(orderNo, request.Target_Date, OrderId, ProductId, 'RMA');



			system.debug('**********11 WMA Debug::'+OrderId+ ' : '+ ContactId+ ' : '+ ProductId+ ' : '+ orderNo+ ' : '+ CaseId + '  >>> '+payment);

			if(payment!=null && (payment.Cybersource_Response_Code__c == 1)){

				payment.Payment_Status__c='Authorized';
				payment.Purchase_Type__c='RMA';
				payment.Transaction_Type__c = 'Authorize';
				try{
					database.upsert(payment);
					// We may need to comment this below case update for T-243119, as on Case layout we have CalculateTax embeded page, which reset this value. 
					if(!String.isEmpty(CaseId)){
						update new Case(Id=CaseId, Total_Amount_to_Pay__c=payment.Amount__c, Send_Email_On_Case__c='0', Status = 'Ready for service');
					}

				}catch (exception e){

					System.Debug('<------Exception--------->' +e);
				}

				//CyberSourceUtility.sendSuccessEmail(ContactId, CaseId, 'CS041_Ship_Complete');
				system.debug('**********WMA Debug::2323');
				//Email alert sent from SFDC


				//Address__c billAdr;

				Address__c billAdr = sObjectUtility.getAddress(ContactId, payment.Billing_Address__c, '', payment.Billing_City__c, payment.Billing_State__c, payment.Billing_Country__c, 
						payment.Billing_Zip__c, 'Billing');
				//Address__c billAdr;
				upsert billAdr;

				// UPDATE CONSUMER INFO
				Account Consumer = new Account (id=ConsumerId);
				consumer.Bill_To__pc=billAdr.id;
				update consumer;


				Order__c order = new Order__C(id=OrderId);
				order.Bill_To__c = billAdr.id;
				order.Consumer__c = ContactId;
				order.Order_Status__c='Open';
				update order;
				system.debug('***********>>'+order);


				payment.Purchase_Type__c='RMA';
				update payment;

				if(!String.isEmpty(CaseId)){
					update new Case(Id=CaseId, Bill_To__c=billAdr.id);
				}


			}else{
				//Send error email.
				if(payment!=null){
					errorText = errorText + '\n\nCybersource Payment Status :'+payment.Payment_Status__c;
				}
				system.debug('**********WMA Debug::9999'+errorText);
				//errorText
				CyberSourceUtility.failureEmailAlert(null, request, errorText, null);
			}
		}catch(Exception ex){
			system.debug('**********WMA Debug::777'+ex);
			CyberSourceUtility.failureEmailAlert(null, request, errorText, ex);         
		}
	}

}