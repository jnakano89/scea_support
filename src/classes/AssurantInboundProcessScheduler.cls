/******************************************************************************
Class         : AssurantInboundProcessScheduler
Description   : This class will retrieve a list of Staging_Siebel_PPP_Inbound__c (inbound) daily at 5:30 PM PST to pickup unprocessed records from 
	Staging_PPP_Inbound__c object and update the relevant SFDC base objects based on the mapping as mentioned in the folowing worksheet and the 
	corresponding logic and data validation below:[T-166694]
Developed by  : Ranjeet Singh (JDC)
Date          : Jan 06, 2014
******************************************************************************/

global class AssurantInboundProcessScheduler implements Schedulable {
	
 //================================================================//
  //Custom Schedule Interface, that the class that needs to be scheduled must implement
  //instead of implementing the standard Out of Box SFDC Schedulable Class
  //================================================================//
  public Interface ISchedule { 
    void execute(SchedulableContext sc); 
  }
 
  //===============================================================//
  //Global method from Standard Salesforce Interface Schedulable
  //===============================================================//
  global void execute(SchedulableContext sc){
    //Invoke the Interface method Execute for the custom Class
    //Account Merge is a class, that is to invoked and implements the custom ISchedule interface
    Type targetType = Type.forName('AssurantInboundProcessBatchable');   
    if(targetType != null) {  
      ISchedule obj = (ISchedule)targettype.NewInstance();     
      obj.execute(sc);
    }
  }

}