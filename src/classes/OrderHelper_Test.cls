/****************************************************************************************************************************************************************************************
Created :
         Leena Mandadapu 07/01/2014 : Created test class
*****************************************************************************************************************************************************************************************/
@isTest
private class OrderHelper_Test {

    static testMethod void myPositiveTestCases() {
        
        Test.startTest();
        
        Contact cnt = TestClassUtility.createContact(1, false)[0];
        cnt.FirstName = 'TestFN';
        cnt.LastName = 'TestLN';
        insert cnt;
            
        Country__c cntry = new Country__c(name='United States',X2_Letter_Code__c = 'US', X3_Letter_Code__c ='USA');
        insert cntry;
        
        Address__c addr = TestClassUtility.createAddress(1, false)[0];
        addr.Consumer__c = cnt.Id;
        addr.Address_Type__c = 'Billing';
        insert addr;
        
        Product__c prd = TestClassUtility.createProduct(1, false)[0];
        prd.SKU__c = 'PS436MOADH';
        prd.Duration__c = 24;
        prd.Product_Type__c = 'ESP';
        prd.Sub_Type__c = 'AD';
        prd.List_Price__c = 39.99;
        insert prd;
        
        Product__c hwprd = TestClassUtility.createProduct(1, false)[0];
        hwprd.SKU__c = 'CUH-1001A';
        hwprd.Product_Type__c = 'PS4';
        hwprd.Sub_Type__c = 'Hardware';
        hwprd.List_Price__c = 399.99;
        insert hwprd;
        
        Assurant_Matrix__c am = New Assurant_Matrix__c();
        am.Dealer_Cost__c = 24;
        am.Assurant_SKU__c = 'PS436MOADH';
        am.SFDC_SKU__c = prd.SKU__c;
        am.Dealer_Id__c = 'SCEA';
        insert am;
        
        Asset__c ast = TestClassUtility.createAssets(1, false)[0];
        ast.PPP_Purchase_Date__c = Date.Today();
        ast.PPP_Product__c = prd.Id;
        ast.PPP_Status__c = 'Pending Confirm';
        ast.Product__c = hwprd.Id;
        insert ast;
        
        Consumer_Asset__c conast = TestClassUtility.createConsumerAsset(cnt.Id, ast.Id, 1, true)[0];
        
        Case cs = TestClassUtility.createCase('Open', 'Phone', false);
        cs.ContactId = cnt.Id;
        cs.RecordTypeId =  GeneralUtiltyClass.RT_PURCHASE_ID;
        cs.Asset__c = ast.id;
        insert cs;
        
        Order__c ord = TestClassUtility.creatOrder(cnt.Id, 1, false)[0];
        ord.Bill_To__c = addr.Id;
        ord.Asset__c = ast.Id;
        ord.Order_Status__c = 'Open';
        ord.ESP_Dealer__c = 'SCEA';
        ord.Order_Type__c = 'ESP';
        ord.Case__c = cs.Id;
        insert ord;
        
        Payment__c payment = new Payment__c();
        payment.Payment_Status__c = 'Charged';
        payment.Order__c = ord.Id;
        insert payment;
        
        ord.Payment__c = payment.Id;
        update ord;
        
        //PPP Cancellation Staging
        ast.PPP_Status__c = 'Pending Cancel';
        ast.PPP_Refund_Eligible__c = false;
        update ast;
        
        ord.Order_Status__c = 'Pending Cancel';
        ord.Refund_Amount__c = 39.99;
        update ord;
        
        Test.stopTest();
        
        //Check if the record inserted successfully              
        System.assertnotEquals(0, [select id from Staging_PPP_Outbound__c where Order__c =: ord.Id].size(), 'Record not Staged - Failed' );
        System.assertEquals(1, [select id from Staging_PPP_Outbound__c where Order__c =: ord.Id and Order_record_Type__c = 'A'].size(), 'PPP Enroll transaction not Staged - Failed');
        System.assertEquals(1, [select id from Staging_PPP_Outbound__c where Order__c =: ord.Id and Order_record_Type__c = 'C'].size(), 'PPP Cancel transaction not Staged - Failed');
        List<Case> caseOrdref = [select Order__c from Case where id = : cs.Id];
        System.assertEquals(ord.Id, caseOrdref.get(0).Order__c, 'Case Order reference not updated - Failed');
        System.assertEquals(ord.Error_Message__c , null, 'Validations failed');
   
    }
    
     static testMethod void myNegativeTestCases() {
        
        Test.startTest();
        
        Contact cnt = TestClassUtility.createContact(1, false)[0];
        insert cnt;
                   
        Address__c addr = TestClassUtility.createAddress(1, false)[0];
        addr.Consumer__c = cnt.Id;
        addr.Address_Type__c = 'Billing';
        insert addr;
        
        Product__c prd = TestClassUtility.createProduct(1, false)[0];
        prd.SKU__c = 'PS436MOADH';
        prd.Duration__c = 24;
        prd.Product_Type__c = 'ESP';
        prd.Sub_Type__c = 'AD';
        insert prd;
        
        Product__c hwprd = TestClassUtility.createProduct(1, false)[0];
        hwprd.SKU__c = 'CUH-1001A';
        hwprd.Product_Type__c = 'PS4';
        hwprd.Sub_Type__c = 'Hardware';
        insert hwprd;
        
        Assurant_Matrix__c am = New Assurant_Matrix__c();
        am.Dealer_Cost__c = 24;
        am.Assurant_SKU__c = 'PS436MOADH';
        am.SFDC_SKU__c = prd.SKU__c;
        am.Dealer_Id__c = 'SCEA';
        insert am;
        
        Asset__c ast = TestClassUtility.createAssets(1, false)[0];
        ast.PPP_Product__c = prd.Id;
        ast.PPP_Status__c = 'Pending Confirm';
        ast.Product__c = hwprd.Id;
        ast.Model_Number__c = '';
        ast.Serial_Number__c = '';
        ast.PPP_Purchase_Date__c = null;
        insert ast;
        
        Consumer_Asset__c conast = TestClassUtility.createConsumerAsset(cnt.Id, ast.Id, 1, true)[0];
        
        Case cs = TestClassUtility.createCase('Open', 'Phone', false);
        cs.ContactId = cnt.Id;
        cs.RecordTypeId =  GeneralUtiltyClass.RT_PURCHASE_ID;
        cs.Asset__c = ast.id;
        insert cs;
        
        Order__c ord = TestClassUtility.creatOrder(cnt.Id, 1, false)[0];
        ord.Bill_To__c = addr.Id;
        ord.Asset__c = ast.Id;
        ord.Order_Status__c = 'Open';
        ord.ESP_Dealer__c = 'SCEA';
        ord.Order_Type__c = 'ESP';
        ord.Case__c = cs.Id;
        insert ord;
        
        Payment__c payment = new Payment__c();
        payment.Payment_Status__c = 'Charged';
        payment.Order__c = ord.Id;
        insert payment;
        
        ord.Payment__c = payment.Id;
        update ord;
        
        ast.PPP_Status__c = null;
        ast.PPP_Refund_Eligible__c = false;
        update ast;
        
        ord.Order_Status__c = 'Pending Cancel';
        ord.Refund_Amount__c = 39.99;
        update ord;
        
        Test.stopTest();
                    
        System.assertEquals(0, [select id from Staging_PPP_Outbound__c where Order__c =: ord.Id and Order_record_Type__c = 'A' limit 1].size(), 'PPP Enroll transaction Staged - Failed');
        System.assertEquals(0, [select id from Staging_PPP_Outbound__c where Order__c =: ord.Id and Order_record_Type__c = 'C' limit 1].size(), 'PPP Cancel transaction Staged - Failed');
        
    }
}