/********************************************************************************************
Class         : CaseRefundService
Description   : A webservice class to create a  new CaseInfo record
Developed by  : Harish Khatri (JDC)
Date          : 01/17/2014
Task          : T-236244,T-236245
Update        :
Aaron Briggs - 04/02/2014 - SF-286 - Set PPP_Refund_Eligible__c to false to prevent re-refund
Aaron Briggs - 04/03/2014 - SF-286 - Correct Refund Not Eligible Text
Leena Mandadapu - 09/29/2014 - SMS-654 -Updated class to include PYB cancellations
Leena Mandadapu - 03/17/2015 - SMS-1105 - PYB monthly PPP option functionality enabled
*********************************************************************************************/
global class  CaseRefundService {
    public static final String RT_SERVICE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service').getRecordTypeId();
    public static final String RT_REFUND_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Refund').getRecordTypeId();
    public static final String RT_PURCHASE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
    public static Boolean PPP_CLAIMED = false;
    
    webservice static string createCase(String orderId) {
        String result;  
        Boolean isStatusUpdate = false;
        Order__c order = getOrderDetail(orderId);
        system.debug('<<<<Order Id>>>>>>'+orderId);
        system.debug('<<<<Order record>>>>>>'+order);
        result = validateData(order);
        system.debug('<<<<Validate Order>>>>>>'+result);
        if(String.IsEmpty(result)) {
            Case cs = new Case();
            cs.RecordTypeId = RT_REFUND_ID;
            cs.Order__c = order.Id;
            System.debug('===='+order.Case__c+'======'+order.Case__r.RecordTypeId+'====='+RT_PURCHASE_ID);
            if( order.Case__c != null) {
                if (order.Case__r.RecordTypeId == RT_PURCHASE_ID) {
                    cs.Parent_Case__c = order.Case__c;
                    cs.ParentId = order.Case__c;  
                }
                if (order.Order_Type__c=='ESP') {
                    cs.Case_Type__c='PPP Refund';
                }
            }
            cs.Refund_Type__c = 'PPP';
            cs.Refund_Reason__c = null;//'No Longer Wants Plan';//null;
            //LM 03/17/2015 : Updated for Monthly payment option
            cs.Refund_Amount__c = order.Billing_Type__c == 'Monthly' ? 0 : order.Order_Total__c;
            cs.ContactId = order.Consumer__c; 
            if(order.Order_Date__c != null && (order.Order_Date__c.daysBetween( Date.today()))>30) {
                cs.Refund_Amount__c = 0;
            }
            
            //LM 09/29/2014 : commented below line as it is not necessary to set and check for this flag   
            //isStatusUpdate = true;
            try{
                //09/29/2014 : LM added if check for PYB orders
                system.debug('<<<<<<Order Origin Source>>>>>'+order.Order_Origin_Source__c);
                if(order.Order_Origin_Source__c != 'PYB'){
                  //AB - 04/02/2014 - set PPP_Refund_Eligible__c to false on refund to prevent multiple refunds
                  Asset__c assetRec = new Asset__c(Id = order.Asset__c, PPP_Status__c = 'Pending Cancel', PPP_Refund_Eligible__c = false);  
                  system.debug('<<<<<<<Asset to update>>>>>>'+assetRec);                 
                  update assetRec;
                  System.debug('OrderHelper.processOutbound11111' + OrderHelper.processOutbound);
                  order.Order_Status__c = 'Pending Cancel';
                  order.Refund_Amount__c = order.Order_Total__c;
                  order.ESP_Refunded_by_Dealer__c = order.Order_Total__c;
                  update order;
                  System.debug('OrderHelper.processOutbound222222' + OrderHelper.processOutbound);
                } else {
                 //PYB cancellation
                 system.debug('<<<<<<<External Order Number>>>>>>'+order.External_Order_Number__c); 
                 if(order.External_Order_Number__c <> null) { 
                 system.debug('<<<<<<<Order ID>>>>>>'+order.Id); 
                 system.debug('<<<<<<<PPP Claimed?>>>>>>'+PPP_CLAIMED);        
                 PYBCancellationCallout(order.Id, order.External_Order_Number__c, PPP_CLAIMED); 
                 //result = 'Refund case created';
                 } else {
                 IntegrationServicesUtility.StagePYBCancellation(order.Id, PPP_CLAIMED, 'Failed', 'Unable to make Cancel request due to missing Order number');
                 }  
                }
                system.debug('<<<<<<Refund Case to Insert>>>>>>>>'+cs);
                insert cs;
                result = 'Successfully created Refund case';
            }catch(Exception ex){
                System.debug('======ERROR Occured====' + ex.getMessage());
                result = ex.getMessage();
                return null;
           }    
        } 
        return result;      
    }
    
    //Validate REFUND action
    public static string validateData(Order__c order){
        String result;
        //Set<String>compareProfileNames = new Set<String>{'Svc Tech Agent','Svc Tech Supervisor'};
        Set<String>compareProfileNames = new Set<String>{'CS-Tier 1','CS-Tier 2','CS-Tier 3','CS-Supervisor', 'System Administrator'};
    
        List<Profile> profiles = [select Name from Profile where Name in:compareProfileNames And Id=:Userinfo.getProfileId() ]; //{//
        if(profiles.size()==0) {
            //result = '\n User Profile as Svc Tech Agent OR Svc Tech Supervisor ';
            result = 'Refund not allowed for this user type.';
        }
        
        List<case> cases = [Select Id,Asset__r.PPP_Status__c From case Where Order__c = :order.Id and  
                            RecordTypeId = :RT_SERVICE_ID and status ='Ship Complete' and Fee_Type__c = 'PlayStation Protection Plan' limit 1];
        System.debug('===========' + order.Order_Type__c);
        System.debug('===========' + order.Payment_Status__c);
        System.debug('===========' + order.Asset__r.Asset_Status__c);
        System.debug('===========' + order.Asset__r.PPP_Status__c);
        System.debug('===========' + order.Asset__r.PPP_End_Date__c);
        
        if(cases.size() > 0) {
            PPP_CLAIMED = true;
        }
        
       //LM 10/30/2014 : Added Order_Origin_Source__c != 'PYB' to filter PYB orders from this check
        if(order.Order_Origin_Source__c != 'PYB' && (
              (order.Order_Type__c == null || order.Order_Type__c != 'ESP') 
           || (order.Payment_Status__c == null || order.Payment_Status__c != 'Charged')
           || (order.Asset__r.Asset_Status__c == null || !order.Asset__r.Asset_Status__c.equals('Active'))
           || (order.Asset__r.PPP_Status__c == null || (!order.Asset__r.PPP_Status__c.equals('Pending Confirm') &&
                                                !order.Asset__r.PPP_Status__c.equals('Active')))
           || (order.Asset__r.PPP_End_Date__c < Date.today())
           || (cases.size () > 0))){
            
           result = 'Refund is ineligible for this order.';     
        }
        
        //LM 10/31/2014: Added for PYB orders.
        if(order.Order_Origin_Source__c == 'PYB' && order.Asset__r.PPP_Status__c.equals('Cancelled')) {
            result = 'Order already Cancelled';
        }
        
        //AB - 04/03/14 - Corrected error message to not mention existing refund record existing.
        //LM - 09/24/14 - added order.Order_Origin_Source__c != 'PYB' condition for PYB orders. 
        if(!order.Asset__r.PPP_Refund_Eligible__c && order.Order_Origin_Source__c != 'PYB'){
          result = 'PPP is not eligible for a refund. Asset PPP Refund Eligible flag is not set.'; 
        }
        return result;    
    }
    // Fetch the order detail
    public static Order__c getOrderDetail(String orderId){
        List<Order__c> orders = [Select Id,Order_Origin_Source__c, Asset__r.PPP_Status__c,Order_Total__c,Order_Type__c,Payment_Status__c,Asset__r.Asset_Status__c,Refund_Amount__c,
                         Asset__r.PPP_Purchase_Date__c,Order_Date__c,Case__r.RecordTypeId,Case__c, Asset__r.PPP_Refund_Eligible__c,
                         Asset__r.PPP_Start_Date__c, Asset__r.PPP_End_Date__c,Consumer__c, PPP_Contract_Number__c, External_Order_Number__c, Billing_Type__c
                         From Order__c Where Id = :orderId ];
        if(orders.size()>0){
            return orders[0];
        }
        return null;
    }
    
    //PYB Cancellation callout
    @future (Callout = true)
    public static void PYBCancellationCallout(Id orderId, String OrderNum, boolean pppclaimed) {
        PYBCancellationRequest req = new PYBCancellationRequest();
        PYBCancellationResponse res = new PYBCancellationResponse();
        boolean ordUpdated = false;
        system.debug('<<<<<<<Order ID>>>>>>>>>'+orderId);
        system.debug('<<<<<<<Order Number>>>>>>>>>'+OrderNum);
        system.debug('<<<<<<<PPP Claimed?>>>>>>>>>'+pppclaimed);
        try {
        res = req.CancelRequest(OrderNum, false);
        } catch (System.Calloutexception e){
        IntegrationServicesUtility.StagePYBCancellation(orderId, PPP_CLAIMED, 'Failed', e.getTypeName());   
        }   
        //Process Cancellation response
        system.debug('<<<<<<<PYB Cancellation Callout Response>>>>>>>>>'+res);
        if(res != null) {
           system.debug('<<<<<<<<<<<<Response Error Message>>>>>>>>>>'+res.ErrorMsg);
           system.debug('<<<<<<<<<<<<Response Error Status>>>>>>>>>>'+res.ErrStatus);
           system.debug('<<<<<<<<<<<<Response PPP Cancel Date>>>>>>>>>>'+res.PPPCancelDate);
           system.debug('<<<<<<<<<<<<Response Refund Amount>>>>>>>>>>'+res.RefundAmount);
           
           if(!res.ErrStatus){
              ordUpdated = updateCancelConfirmation(orderId, converttoDate(res.PPPCancelDate), res.RefundAmount);
              system.debug('<<<<<<<<<<<<Order updated?>>>>>>>>>>'+ordUpdated);
              if(ordUpdated) {
              IntegrationServicesUtility.StagePYBCancellation(orderId, PPP_CLAIMED, 'Success', 'Cancellation processed');
              } else {
              IntegrationServicesUtility.StagePYBCancellation(orderId, PPP_CLAIMED, 'Failed', 'Unable to update order/Asset');  
              }
           } else {
              IntegrationServicesUtility.StagePYBCancellation(orderId, PPP_CLAIMED, 'Failed', res.ErrorMsg);
           }
        } else { 
           IntegrationServicesUtility.StagePYBCancellation(orderId, PPP_CLAIMED, 'Failed', 'No response received'); 
        }
   }
   
   public static Boolean updateCancelConfirmation(Id orderId, Date PPPCancelDate, decimal RefundAmt ){
     List<Order__c> order = [Select Id,Order_Origin_Source__c, Asset__r.PPP_Status__c,Order_Total__c,Order_Type__c,Payment_Status__c,Asset__r.Asset_Status__c,Refund_Amount__c,
                         Asset__r.PPP_Purchase_Date__c,Order_Date__c,Case__r.RecordTypeId,Case__c, Asset__r.PPP_Refund_Eligible__c,
                         Asset__r.PPP_Start_Date__c, Asset__r.PPP_End_Date__c,Consumer__c, Asset__c, Billing_Type__c
                         From Order__c Where Id = :orderId ];
     system.debug('<<<<<<Order to update on PYB cancellation response>>>>>>'+order);
     system.debug('<<<<<<Order List Size>>>>>>'+order.size());                    
     if(order <> null && order.size() == 1){
        Asset__c ast = new Asset__c(Id = order[0].Asset__c, PPP_Status__c = 'Cancelled', PPP_Refund_Eligible__c = false, ESP_Cancellation_Date__c = PPPCancelDate); 
        List<Case> refundcase = [Select Id,Order__c, Refund_Amount__c from Case Where Order__c = :orderId and RecordTypeId =: RT_REFUND_ID ];
        system.debug('<<<<<<Asset to update>>>>>>'+ast);   
        system.debug('<<<<<<Case to update>>>>>>'+refundcase);     
        system.debug('<<<<<<Case to update list Size>>>>>>'+refundcase.size());                    
        order[0].Order_Status__c = 'Assurant Cancelled';
        //LM: 03/17/2015 - Set refund to 0 for Monthly billing option
        order[0].Refund_Amount__c = order[0].Billing_Type__c == 'Monthly' ? 0 : RefundAmt;
        //Update Refund case refund amount for PYB cancellations
        if(refundcase <> null && refundcase.size() > 0) {
           refundcase[0].Refund_Amount__c = order[0].Billing_Type__c == 'Monthly' ? 0 : RefundAmt;	
        } //else no update necessary
        
        Savepoint sp = Database.setSavepoint();
        try{
            update ast;
            update order; 
        } catch (System.DmlException dml) {
            Database.rollback(sp);
            IntegrationServicesUtility.StagePYBCancellation(orderId, PPP_CLAIMED, 'Failed', dml.getMessage());
        } 
        
        Savepoint sp1 = Database.setSavepoint();
        try{
            update refundcase;
        } catch (System.DmlException dml) {
            Database.rollback(sp1);
            IntegrationServicesUtility.StagePYBCancellation(orderId, PPP_CLAIMED, 'Failed', dml.getMessage());
        } 
        return true;        
     } else {
        IntegrationServicesUtility.StagePYBCancellation(orderId, PPP_CLAIMED, 'Failed', 'Not able to find the order to update or Multiple Orders found');
        return false;
     }                         
   }
   
   public static Date converttoDate(String dateStr) {
    try{
        system.debug('<<<<<<Date received in the Response>>>>>>>'+dateStr);
        list<String> inputdate = dateStr.split('T')[0].split('-');
        system.debug('<<<<<<Date List>>>>>>>'+inputdate);
        system.debug('<<<<<<Date List Size>>>>>>>'+inputdate.size());
        if(inputdate.size() == 3) { 
          Date outputdate = Date.parse(inputdate[1] + '/' + inputdate[2] + '/' + inputdate[0]);
          system.debug('<<<<<<Converted Date>>>>>>>'+outputdate);
          return outputdate;
        }
    } catch(Exception ex) {
        System.debug('<<<<<Not able to convert Response Date>>>>>'+ex.getMessage());
    }
   return null;
  }    
}