public class RefreshAssetsController {
	
  String contactId;
  String accountId;
    
  public final String CLASS_NAME = 'RefreshAssetsController';
  
  public RefreshAssetsController() {

  	accountId = Apexpages.currentPage().getParameters().get('accountId');
  	
  	for(Contact c : [select id from Contact where AccountId =: accountId]){
  		contactId = c.Id;
  	}

  }

  
  public PageReference updateAssetRecords() {
  	
  	List<Asset__c> updateAssetList = new List<Asset__c>();
  	
  	System.debug('========updateAssetRecords=========');
 
 	if(contactId==null || contactId.trim().length()==0){
 		  	return returnToContact();
 	}
  	
  	for(Consumer_Asset__c consumerAsset : [Select c.Consumer__c, c.Asset__r.Siras_Purchase_Date__c, 
  		c.Asset__r.Serial_Number__c, 
  		c.Asset__r.Retailer__c, c.Asset__r.Retailer_ID__c, c.Asset__r.Release_Date__c, 
  		c.Asset__r.Purchase_Date__c, c.Asset__r.Model_Number__c, 
  		c.Asset__c From Consumer_Asset__c c
  				where c.Consumer__c =: contactId
  						and c.Asset__c != null
  						and c.Asset__r.Serial_Number__c != null
  					]){
  			
 	
  			Asset__c currentAsset = consumerAsset.Asset__r;		
 
 			
 			SirasUtility sUtil = new SirasUtility();
			SirasResult sResult = new SirasResult();
			
			try{
			  sResult = sUtil.getPOP(currentAsset.Serial_Number__c);
			}catch(Exception e) {  	  		 
  	  		 	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
  	  		  	sResult=null;
  	  	    }
  	  	    
  	  	    if(sResult==null){
  	  	    	continue;
  	  	    }
 
			Map<String, String> warrantyCodeDetailsMap = getWarrantyCodeDetails();
			
			currentAsset.UPC_ID__c = sResult.UPC;
			currentAsset.Brand_Id__c = sResult.brandID;
			currentAsset.Brand_Name__c = sResult.brandName;
			currentAsset.Description__c = sResult.itemDescription;
			currentAsset.Release_Date__c = sResult.soldDate;
			currentAsset.Item_Number__c = sResult.itemNumber;
			currentAsset.Retailer__c = sResult.soldByRetailer;
			currentAsset.Retailer_Id__c = sResult.soldByRetailerID;
			currentAsset.Siras_Purchase_Date__c = sResult.soldDate; 
			 
			
			if(sResult.manufacturerWarrantyDetermination != null && sResult.manufacturerWarrantyDetermination != ''){
				String detail = warrantyCodeDetailsMap.get(sResult.manufacturerWarrantyDetermination);
				detail  = detail == null ? sResult.manufacturerWarrantyDetermination : detail;
				currentAsset.Warranty_Details__c = '['+ detail +']';
			}
			if(sResult.manufacturerWarrantyLaborDetermination  != null && sResult.manufacturerWarrantyLaborDetermination  != ''){
				String detail = warrantyCodeDetailsMap.get(sResult.manufacturerWarrantyLaborDetermination);
				detail  = detail == null ? sResult.manufacturerWarrantyLaborDetermination : detail;
				currentAsset.Warranty_Details__c += '[Labor: '+ detail +']';
				System.debug('========' + detail + ' ==========' + warrantyCodeDetailsMap + ' =======' + warrantyCodeDetailsMap.get(sResult.manufacturerWarrantyLaborDetermination));
			}
			if(sResult.manufacturerWarrantyPartsDetermination  != null && sResult.manufacturerWarrantyPartsDetermination  != ''){
				String detail = warrantyCodeDetailsMap.get(sResult.manufacturerWarrantyPartsDetermination);
				detail  = detail == null ? sResult.manufacturerWarrantyPartsDetermination : detail;
				currentAsset.Warranty_Details__c += '[Parts: '+ detail +']';
			}
 
  			updateAssetList.add(currentAsset);
  			
	}//end-for
  	
  	if(updateAssetList.size()>0){
  		
  		try{
  			update updateAssetList;
  		}catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
  		}
  	}
  	
  	return returnToContact();
  }

	public PageReference returnToContact(){
      	
		if(contactId != null && contactId != ''){
			
			PageReference contactPage = new ApexPages.StandardController(new Contact(id=contactId)).view();
	        contactPage.setRedirect(true);
       	 	return contactPage;
		}
		
		return null;
    }
  
	private Map<String, String> getWarrantyCodeDetails() {
		map<String, String> warrantyDetailsMap = new map<String, String>();
		for(Siras_Warranty_Code__c wc : Siras_Warranty_Code__c.getAll().values()) {
			warrantyDetailsMap.put(wc.Name, wc.Description__c);
		}
		return warrantyDetailsMap;
	}

}