/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AssurantInboundClaimScheduler_Test {

	static List<Staging_PPP_Inbound__c> stagingPPPList;
	/*tatic testMethod void myUnitTest() {
		createTestData();
		Test.startTest();

		String CRON_EXP = '0 0 0 * * ?';
		AssurantInboundClaimScheduler m = new AssurantInboundClaimScheduler(); 
		system.schedule('Test AssurantInboundClaimScheduler Job', CRON_EXP, m); 
		Test.stopTest();


	}*/
	static testMethod void SchedulerTest() {
		createTestData();
		for(Staging_PPP_Inbound__c stg : stagingPPPList){
			stg.PPP_Claim_Reason__c =  'Test!!2!';
		}
		AssurantInboundToSfdcBatchable.claimQueryTest = ' and PPP_Claim_Reason__c like \'Test claim reason%\'';
		insert stagingPPPList;
		Test.startTest();
		String CRON_EXP = '0 0 0 * * ?';
		AssurantInboundClaimScheduler m = new AssurantInboundClaimScheduler(); 
		system.schedule('Test AssurantInboundClaimScheduler Job2', CRON_EXP, m); 
		//Todo -- Need to put Assert
		Test.stopTest();

	}
	
	static testMethod void BatchTestWithOutSFDCObject() {
		createTestData();
		for(Staging_PPP_Inbound__c stg : stagingPPPList){
			stg.PPP_Claim_Reason__c =  'Test!!2!';
		}
		AssurantInboundToSfdcBatchable.claimQueryTest = ' and PPP_Claim_Reason__c like \'Test!!2!%\'';
		insert stagingPPPList;
		Test.startTest();
		DataBase.executeBatch(new AssurantInboundToSfdcBatchable(), 5);
		//Todo -- Need to put Assert
		Test.stopTest();

	}

	static testMethod void BatchTestWithSFDCObject() {
		list<Address__c> addList = TestClassUtility.createAddress(1, false);
	    insert addList;
	    list<Asset__c> assetList = TestClassUtility.createAssets(1, true);
	    assetList[0].PPP_Product__c = null;
	    update assetList[0];
	    
	    
	    list<Contact> cntList = TestClassUtility.createContact(2, false);
	    list<Product__c> productList = TestClassUtility.createProduct(1, true);
	    cntList[0].FirstName = 'Test Name';
	    cntlist[0].Email = 'test@test.com';
	    cntlist[0].Preferred_Language__c = 'Eng';
		   cntlist[0].Bill_To__c = addList[0].Id;
	    insert cntlist;
	    
	    Case cs = TestClassUtility.createCase('New', 'TEst', false);
	    cs.ContactId = cntList[0].Id;
	    cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
	    cs.Sub_Area__c = 'NEw';
	    cs.Status = 'New';
	    insert cs;
	           
	    list<Order__c> ordrList = TestClassUtility.creatOrder(cntList[0].Id, 5, false);
	    list<Account>personAccountList = TestClassUtility.createPersonAccount(1, true);
		    list<contact> lstcontact = [select id from contact where accountid in:personAccountList limit 1];
	    for(Order__c o : ordrList){
	    	o.Case__c = cs.Id;
	     o.Consumer__c =	lstcontact[0].Id;
	    }
	    insert ordrList;
	    
	    Payment__c payment = new Payment__c();
	    payment.Payment_Status__c = 'Charged';
	    payment.Order__c = ordrList[0].Id;
	    insert payment;
	    
	    ordrList[0].Payment__c = payment.Id;
	    update ordrList[0];
		createTestData();
		Integer ordIndx = 0;
		map<id, Order__c> ordMap = new map<id, Order__c>([select id, Name, External_Order_Number__c from Order__c where Id in: ordrList]);
		for(Staging_PPP_Inbound__c stg : stagingPPPList){
			stg.PPP_Claim_Reason__c =  'Test!!2!';
			stg.Order_Number__c = ordMap.get(ordrList[ordIndx].id).External_Order_Number__c;
		}
		AssurantInboundToSfdcBatchable.claimQueryTest = ' and PPP_Claim_Reason__c like \'Test!!2!%\'';
		update ordMap.values();
		Test.startTest();
			DataBase.executeBatch(new AssurantInboundToSfdcBatchable(), 5);
			//Todo -- Need to put Assert
		Test.stopTest();
	}
	
	static void createTestData() {
		stagingPPPList = TestClassUtility.createStagingPPPInbound(5, false);
	}
}