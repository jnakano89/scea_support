@isTest
private class UserManagement_Test {
  
  //static User u1; 
    
    
   static testMethod void myUnitTest() {
       
   try { 
        Test.startTest();
        createdata(); 
        
        Test.stopTest();
       }     
      
        catch (Exception e) {
        System.debug('Exception: '+e);      
       }
  
    }  
     static void createData() {   
         
           Profile p1 = [select id from profile where name='CS-Tier 1'];
           User u1 = new User(alias = 'CST1', email='CST1@testorg.com',
           emailencodingkey='UTF-8', lastname='CST1', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p1.Id,
           timezonesidkey='America/Los_Angeles', username='CST1@testorg.com', CompanyName = 'SCEA');
         
           Insert(u1);
                 
           Profile p2 = [select id from profile where name='CS-Tier 2'];
           User u2 = new User(alias = 'CST2', email='CST2@testorg.com',
           emailencodingkey='UTF-8', lastname='CST2', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p2.Id,
           timezonesidkey='America/Los_Angeles', username='CST2@testorg.com' , CompanyName = 'SCEA');
         
           Insert(u2);
         
           Profile p3 = [select id from profile where name='CS-Tier 3'];
           User u3 = new User(alias = 'CST3', email='CST3@testorg.com',
           emailencodingkey='UTF-8', lastname='CST3', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p3.Id,
           timezonesidkey='America/Los_Angeles', username='CST3@testorg.com', CompanyName = 'SCEA');
         
           Insert(u3);
         
           Profile p4 = [select id from profile where name='CS-Bureau Supervisor'];
           User u4 = new User(alias = 'CST4', email='CST4@testorg.com',
           emailencodingkey='UTF-8', lastname='CST4', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p4.Id,
           timezonesidkey='America/Los_Angeles', username='CST4@testorg.com', CompanyName = 'SCEA');
         
           Insert(u4);
         
           Profile p5 = [select id from profile where name='CS-Read Only'];
           User u5 = new User(alias = 'CST5', email='CST5@testorg.com',
           emailencodingkey='UTF-8', lastname='CST5', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p5.Id,
           timezonesidkey='America/Los_Angeles', username='CST5@testorg.com', CompanyName = 'SCEA');
         
           Insert(u5);
         
           Profile p6 = [select id from profile where name='CS-Reporting Analyst'];
           User u6 = new User(alias = 'CST6', email='CST6@testorg.com',
           emailencodingkey='UTF-8', lastname='CST6', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p6.Id,
           timezonesidkey='America/Los_Angeles', username='CST6@testorg.com', CompanyName = 'SCEA');
         
           Insert(u6);
         
           Profile p7 = [select id from profile where name='CS-Supervisor'];
           User u7 = new User(alias = 'CST7', email='CST7@testorg.com',
           emailencodingkey='UTF-8', lastname='CST7', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p7.Id,
           timezonesidkey='America/Los_Angeles', username='CST7@testorg.com', CompanyName = 'SCEA');
         
           Insert(u7);
         
           Profile p8 = [select id from profile where name='System Administrator'];
           User u8 = new User(alias = 'CST8', email='CST7@testorg.com',
           emailencodingkey='UTF-8', lastname='CST8', languagelocalekey='en_US',
           localesidkey='en_US', profileid = p8.Id,
           timezonesidkey='America/Los_Angeles', username='CST8@testorg.com', CompanyName = 'SCEA');
           
           Insert(u8);
               

           //Update
           u1.ProfileId = p2.Id;
           update u1;
         
           u2.ProfileId = p1.Id;
           update u2;
         
           u3.ProfileId = p4.Id;
           update u3;
         
           u4.ProfileId = p3.Id;
           update u4;
         
           u5.ProfileId = p6.Id;
           update u5;
         
           u6.ProfileId = p5.Id;
           update u6;
         
           u7.ProfileId = p8.Id;
           update u7;
         
           u8.ProfileId = p7.Id;
           update u8;
          
           
     }
 
}