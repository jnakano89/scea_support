/****************************************************************************************************************************************************************************************
Created 
        :   Preetu Vashista : 02/25/2015 :Service Replacement Purchase
*****************************************************************************************************************************************************************************************/
@isTest(seeAllData=true)
global class ServiceReplacementPurchase_Test {
    public static final String RT_HARDWARE_NETWORKING_ID = GeneralUtiltyClass.RT_HARDWARE_NETWORKING_ID; 
    public static final String RT_SERVICE_ID = GeneralUtiltyClass.RT_SERVICE_ID;

    static testMethod void myUnitTest(){
        Contact repContact = [SELECT id,FirstName, LastName,Phone FROM Contact
                              WHERE Email = 'aaron_briggs@playstation.sony.com' AND Phone != null
                              LIMIT 1];

        Case testcase = new Case();
        testcase.Status = 'Open';
        testCase.Origin = 'phone';
        testCase.ContactId = repContact.Id;
        testCase.recordTypeId = RT_HARDWARE_NETWORKING_ID;
        testCase.Product__c = 'PS4';
        testCase.Sub_Area__c = 'Audio/Video';
        insert testCase;
        System.assert(testCase != null);
                  
        Asset__C insertedAssetVal = [select Id from Asset__c Where Model_Number__c = 'CECHA01' LIMIT 1];
        testCase.status = 'Service';
        testCase.Asset__c = insertedAssetVal.Id;
          
        update testCase;
               
        ApexPages.StandardController stdctrl = new ApexPages.StandardController(testCase);
        Case insertedCase = [select Id,contactId,caseNumber from Case where id = :testCase.Id];
        String a = insertedCase.Id;
        String value= 'Yes';
        ServiceReplacementPurchase sc = new ServiceReplacementPurchase(stdctrl);
        sc.getReplacedProductDetail(insertedCase.Id);
        if(value=='Yes'){
        sc.Saverec();
        }
    }
 }