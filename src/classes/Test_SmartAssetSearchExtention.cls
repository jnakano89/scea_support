/*******************************************************************
Name  : Test_SmartAssetSearchExtention
Author: Poonam Varyani
Date  : August 8, 2013
*************************************************************************/
@isTest
private class Test_SmartAssetSearchExtention {
	static List<Asset__c> listAsset;
    static testMethod void testSmartAssetSearch() {
        //create asset data
        createAssetData();
        ApexPages.StandardController sc;
        Case testcase = new Case();
	    testcase.Status = 'Active';
	    testcase.received_date__c = date.today();
	    testcase.shipped_date__c = date.today().addDays(1);
	    testcase.outbound_tracking_number__c = '101';
	    testcase.outbound_carrier__c = 'TYest1';
    	testcase.Offender__c = 'Test';
        testCase.Product__c = 'PS4';
        testCase.Sub_Area__c = 'Account Help';
    	insert testCase;
    	
        ApexPages.currentPage().getParameters().put('caseId', testcase.Id);
        SmartAssetSearchExtention controller = new SmartAssetSearchExtention(sc);
        controller.assetSerialNumber = '123';
        
        controller.performSearch();
        
        system.assert(40== controller.searchCount);
        controller.sortData();
        controller.cancel();
        
        controller.nextAssetPage();
        System.assertEquals(21,controller.showingFrom);
        System.assertEquals(40,controller.showingTo);
        
        controller.previousAssetPage();
        controller.requestedPage = '2';
        controller.requestedAssetPage();
        
        
        //perform search where no records found
        SmartAssetSearchExtention controllerNew = new SmartAssetSearchExtention(sc);
        controllerNew.assetSerialNumber = '9999';
        
        controllerNew.performSearch();
        
        system.assert(0== controllerNew.searchCount);
        
        //
        Account testAccount = TestClassUtility.createAccount(true);
        ApexPages.Standardcontroller stdCtrl ;
        PageReference pg = Page.CyberSourcePayment; 
        Test.setCurrentPage(pg); 
        list<Contact> lstContact = TestClassUtility.createContact(2,false);
        for(Contact ct:lstContact){
        	ct.AccountId = testAccount.ID;
        }
        insert lstContact;
        ApexPages.currentPage().getParameters().put('AccountId', testAccount.Id);
        SmartAssetSearchExtention controllerNew1 = new SmartAssetSearchExtention(stdCtrl);
        controllerNew1.createConsumerAsset();
        new SmartAssetSearchExtention();
    }
     static testMethod void testSmartAssetSearch2() {
        //create asset data
        createAssetData();
        ApexPages.StandardController sc;
        Case testcase = new Case();
	    testcase.Status = 'Active';
	    testcase.received_date__c = date.today();
	    testcase.shipped_date__c = date.today().addDays(1);
	    testcase.outbound_tracking_number__c = '101';
	    testcase.outbound_carrier__c = 'TYest1';
    	testcase.Offender__c = 'Test';
        testCase.Product__c = 'PS4';
        testCase.Sub_Area__c = 'Account Help';
        testCase.Asset__c = listAsset[0].Id;
    	insert testCase;
    	
        ApexPages.currentPage().getParameters().put('caseId', testcase.Id);
        ApexPages.currentPage().getParameters().put('exchange', '1');
        SmartAssetSearchExtention controller = new SmartAssetSearchExtention(sc);
        controller.assetSerialNumber = '123';
        
        controller.performSearch();
        
        system.assert(40== controller.searchCount);
        controller.sortData();
        controller.cancel();
        
        controller.nextAssetPage();
        System.assertEquals(21,controller.showingFrom);
        System.assertEquals(40,controller.showingTo);
        
        controller.previousAssetPage();
        controller.requestedPage = '2';
        controller.requestedAssetPage();
        
        
        //perform search where no records found
        SmartAssetSearchExtention controllerNew = new SmartAssetSearchExtention(sc);
        controllerNew.assetSerialNumber = '9999';
        
        controllerNew.performSearch();
        
        system.assert(0== controllerNew.searchCount);
        
        //
        Account testAccount = TestClassUtility.createAccount(true);
        ApexPages.Standardcontroller stdCtrl ;
        PageReference pg = Page.CyberSourcePayment; 
        Test.setCurrentPage(pg); 
        list<Contact> lstContact = TestClassUtility.createContact(2,false);
        for(Contact ct:lstContact){
        	ct.AccountId = testAccount.ID;
        }
        insert lstContact;
        ApexPages.currentPage().getParameters().put('AccountId', testAccount.Id);
        ApexPages.currentPage().getParameters().put('caseId', testcase.Id);
        ApexPages.currentPage().getParameters().put('exchange', '1');
        SmartAssetSearchExtention controllerNew1 = new SmartAssetSearchExtention(stdCtrl);
        controllerNew1.selectedAsset = listAsset[1].Id;
        controllerNew1.createConsumerAsset();
        new SmartAssetSearchExtention();
    }
    
    private static void createAssetData(){
        listAsset = new List<Asset__c>();
        
        for(integer i=1;i<=40;i++){
            Asset__c asset = new Asset__c();
            asset.Serial_Number__c = '123'+i;
            asset.Purchase_Date__c = Date.today().addDays(-i);
            asset.Registration_Date__c = Date.today().addDays(i);
            asset.Console_Type__c = 'test'+i;
            asset.Model_Number__c = '9999'+i;
            
            listAsset.add(asset);
        }
        
        insert listAsset;
    }
}