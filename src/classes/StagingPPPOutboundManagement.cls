//==================================================================================================
// Updates:
// Mar 11, 2014 Leena Mandadapu : Updated the dealer Ids for Canada call center SKUs - JIRA# SF-133
// Apr 09, 2014 Leena Mandadapu : Updated the dealer Ids for Canada PSN SKUs - JIRA# SF-480 
// Feb 18, 2015 Leena Mandadapu : Re-activated Canadian dealer IDs - JIRA# SMS-1041
// May 19, 2015 Leena Mandadapu	: Added PYB SKUs for PPP PYB Claims
//==================================================================================================

public with sharing class StagingPPPOutboundManagement {
    public static void beforeInsert(list<Staging_PPP_Outbound__c> newList) {
        updateSKU(newList);
        updateListPrice(newList);
    }
    
    public static void updateListPrice(list<Staging_PPP_Outbound__c> newList) {
        Map <String, String> SKU_ProductMap = new Map <String, String>();

        for(Product__c p : [Select Id, Sku__c, Product_Type__c,  List_Price__c from Product__c where Product_Type__c='ESP']){
                SKU_ProductMap.put(p.sku__c,  String.valueof(p.List_Price__c));
        }
        
        for( Staging_PPP_Outbound__c  spo : newList){        
        //System.Debug('>>>>>SKU_ProductMap.get(spo.PPP_SKU__c.trim())<<<<<' +SKU_ProductMap.get(spo.PPP_SKU__c.trim()));
        //System.Debug('@@@@@@@ spo.PPP_SKU__c' +spo.PPP_SKU__c);
            if(spo.PPP_SKU__c <> null && (spo.PPP_List_Price__c==null||spo.PPP_List_Price__c.trim()=='' || spo.PPP_List_Price__c.trim()=='0.00' )){
                 if(SKU_ProductMap.get(spo.PPP_SKU__c.trim())!=null){
                       spo.PPP_List_Price__c=(SKU_ProductMap.get(spo.PPP_SKU__c.trim()));
                 }
            }
         }
    }
    
    
    public static void updateSKU(list<Staging_PPP_Outbound__c> newList) {
        
        Set<String> skuValues = new Set<String>();
        Set<Id> orderIds = new Set<Id>();
        map<Id, String> orderId_PPP_SKU_Map = new map<Id, String>();
        map<String, list<Assurant_Matrix__c>> assurantMap = new map<String, list<Assurant_Matrix__c>>();
        map<Id, Staging_PPP_Outbound__c> orderIdOutboundMap = new map<Id, Staging_PPP_Outbound__c>();
        map<Id, String> orderDealerMap = new map<Id, String>();
        for(Staging_PPP_Outbound__c outbound : newList) {   
            if (outbound.PPP_Cancel_After_30_Days__c != null && (outbound.PPP_Cancel_After_30_Days__c.contains('false')==true ||outbound.PPP_Cancel_After_30_Days__c=='R') && outbound.Order_Record_Type__c=='C'){
                  outbound.PPP_Cancel_After_30_Days__c='R';
            } else outbound.PPP_Cancel_After_30_Days__c='';
            
            if(outbound.Order__c <> null) {
                orderIds.add(outbound.Order__c);
                orderIdOutboundMap.put(outbound.Order__c, outbound);    
            }
        }
        // if order is not populated in staging record then do nothing.
        if(orderIds.isEmpty()) return;
        
        for(Order__c ordr : [select Asset__r.PPP_Product_SKU__c, Id, Order_Origin_Source__c, Asset__r.Purchase_Date__c,  Asset__r.PPP_Status__c, Asset__r.PPP_Purchase_Date__c, Ship_to__r.Country__c, Bill_to__r.Country__c,
                             Billing_Type__c from Order__c where Id IN : orderIds]) {
            Staging_PPP_Outbound__c outbound = orderIdOutboundMap.get(ordr.Id);
            
            // Change by CM as per Issue I-101348
            if(ordr.Asset__r.PPP_Status__c<>'Active' && outbound.Order_Record_Type__c=='U'){
                 outbound.adderror('Only assets with an active PPP should be sent to the staging object on service exchange');
            }
            
            if(ordr.Asset__r.PPP_Product_SKU__c <> null) {
                skuValues.add(ordr.Asset__r.PPP_Product_SKU__c);
                orderId_PPP_SKU_Map.put(ordr.Id, ordr.Asset__r.PPP_Product_SKU__c);
                if(ordr.Asset__c <> null && ordr.Asset__r.Purchase_Date__c <> null && ordr.Asset__r.PPP_Purchase_Date__c <> null) {
                      if(ordr.Asset__r.Purchase_Date__c.daysBetween(ordr.Asset__r.PPP_Purchase_Date__c) > 30) {
                         orderDealerMap.put(ordr.Id, 'MPOS');
                         if(ordr.Bill_to__r.Country__c== 'US' || ordr.Bill_to__r.Country__c == 'USA') {
                            if(ordr.Order_Origin_Source__c == 'P') {
                                outbound.Order_Dealer__c = 'SCEO';
                            } else if(ordr.Order_Origin_Source__c == 'PYB') {
                            	if(ordr.Billing_Type__c == 'Single') {
                            		outbound.Order_Dealer__c = 'SCEAP';
                            	} else {
                            		outbound.Order_Dealer__c = 'SCEAM';
                            	}	
                            } else {	
                                outbound.Order_Dealer__c = 'SCEB';
                            }
                         } else if(ordr.Bill_to__r.Country__c == 'Canada' || ordr.Bill_to__r.Country__c == 'CANADA' ||  ordr.Bill_to__r.Country__c == 'CA' || ordr.Bill_to__r.Country__c == 'ca') {
                            //LM 02/18/2015 : Jira# SMS-1041 - re-activated the Canadian dealer Ids.
                            if(ordr.Order_Origin_Source__c == 'P') {
                                outbound.Order_Dealer__c = 'SCECM';
                            } else {
                                outbound.Order_Dealer__c = 'SPSCM';
                            }
                        }
                      } else {
                               orderDealerMap.put(ordr.Id, 'POS');
                               if(ordr.Bill_to__r.Country__c == 'US' || ordr.Bill_to__r.Country__c == 'USA') {
                                  if(ordr.Order_Origin_Source__c == 'P') {
                                     outbound.Order_Dealer__c = 'SPSN';
                                  } else if(ordr.Order_Origin_Source__c == 'PYB') {
                            	      if(ordr.Billing_Type__c == 'Single') {
                            		     outbound.Order_Dealer__c = 'SCEAP';
                            	      } else {
                            		     outbound.Order_Dealer__c = 'SCEAM';
                            	      }	
                                } else {
                                     outbound.Order_Dealer__c = 'SCEA';
                                }
                               } else if(ordr.Bill_to__r.Country__c == 'Canada' || ordr.Bill_to__r.Country__c == 'CANADA' ||  ordr.Bill_to__r.Country__c == 'CA' || ordr.Bill_to__r.Country__c == 'ca') {
                        	   	//LM 02/18/2015 : Jira# SMS-1041 - re-activated the Canadian dealer Ids.
                                  if(ordr.Order_Origin_Source__c == 'P') {
                                      outbound.Order_Dealer__c = 'SCEC';
                                  } else {
                            	      outbound.Order_Dealer__c = 'SPSC';
                                  }
                               }
                        }
                 }
            }
        } //End for loop
        
        // if SKU is not populated in associated Asset record then do nothing.
        if(skuValues.isEmpty()) return;
        
        for(Assurant_Matrix__c am : [select SFDC_SKU__c, Assurant_SKU__c, Channel__c, Dealer_ID__c  from Assurant_Matrix__c where SFDC_SKU__c IN : skuValues]) {
            //The unique key is SFDCSKU+Channel+DealerId=Assurant SKU in the Assurant matrix Table.
            if(!assurantMap.containsKey(am.SFDC_SKU__c)) {
                assurantMap.put(am.SFDC_SKU__c, new list<Assurant_Matrix__c>());
            }
            assurantMap.get(am.SFDC_SKU__c).add(am);
        }
        
        for(Staging_PPP_Outbound__c outbound : newList) {
            String sfdcSKU = orderId_PPP_SKU_Map.get(outbound.Order__c);
            String channel = orderDealerMap.get(outbound.Order__c);
            if(assurantMap.get(sfdcSKU)<>null){
             for(Assurant_Matrix__c am : assurantMap.get(sfdcSKU)) {
                System.debug(am.SFDC_SKU__c + '==========================' + sfdcSKU);
                System.debug(am.Dealer_ID__c + '==========================' + outbound.Order_Dealer__c);
                System.debug(am.Channel__c + '==========================' + channel);
                
                if(am.SFDC_SKU__c == sfdcSKU && am.Channel__c == channel && am.Dealer_ID__c == outbound.Order_Dealer__c) {
                    outbound.PPP_SKU__c = am.Assurant_SKU__c;
                    break;
                }
             } //End inner for loop
          } //End if loop
        } //End outer For loop
   } //End Method
}