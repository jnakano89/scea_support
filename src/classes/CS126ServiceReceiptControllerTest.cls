@IsTest
public with sharing class CS126ServiceReceiptControllerTest {

	static testMethod void unitTest() {
		
		list<Contact> cntList = TestClassUtility.createContact(1, false);
    	insert cntList; 
    	   		   		
    	Case cs = TestClassUtility.createCase('New', 'Test', false);
	    cs.ContactId = cntList[0].Id;
	    cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
	    cs.Sub_Area__c = 'New';
	    cs.Status = 'New';
	    insert cs;
	    System.assert(cs.Id != null);
	    
	    list<Order__c> ordrList = TestClassUtility.creatOrder(cntList[0].Id, 1, false);
	    ordrList[0].Case__c = cs.Id;	    
    	insert ordrList;
	    
	    list<Product__c> productList = TestClassUtility.createProduct(1, false);
    	insert productList;
    	
   		TestClassUtility.createOrderLine(1, ordrList[0].Id, productList[0].Id, true);
	    
	    CS126ServiceReceiptController receiptController = new CS126ServiceReceiptController();
	    receiptController.caseId = cs.Id;
	    
	    Case caseTest = receiptController.caseObject;
	    System.assertEquals(caseTest.ContactId, cs.ContactId);
	    System.assertEquals(caseTest.Id, cs.Id);
	    
	    Contact contactTest = receiptController.contactObject;
	    System.assertEquals(receiptController.contactObject.Id, cntList[0].Id);
	    
	    Order__c orderTest = receiptController.orderObject;
	    System.assert(orderTest.Id == ordrList[0].Id);
	    
	    Order_Line__c orderLineTest = receiptController.orderLine;
	    System.assert(orderLineTest.Order__c == orderTest.Id);
	    
	    
	}
}