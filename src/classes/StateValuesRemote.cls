/******************************************************************************************************************************
Class           : StateValuesRemote
Description     : Remote class to check user eligibility to chat to an agent. Encrypt/Decrypt helper for session cookie store.
Developed by    : Vikram Gavini
Date            : September 26, 2014
Modified        : 12/23/2014 : Aaron Briggs : SMS-938 : Implemented validateIP() method to check for blocked IPs
                : 01/15/2015 : Aaron Briggs : SMS-1071 : Optimized Blocklist Query to Prevent Exceeding SOQL Governor Limit
                : 03/27/2015 : Aaron Briggs : SMS-1433 : Updated Daily / Weekly Counters to 50 Threshold
******************************************************************************************************************************/
global with sharing class StateValuesRemote {
    
    public String checkEmail { get; set; }  
    public String checkCookie { get; set; }
    public static String ip { get; set; }
    public static String url { get; set; }
    //constructor
    public StateValuesRemote(){}
    
    public static final Blob cryptoKey = EncodingUtil.base64Decode('hSK6Px2xysFXZjVa+gqJD9KBQ2ZdTiLzjja9Dw0RYes=');
    
    public PageReference validateSession(){
        PageReference pr = validateIP();
        system.debug('@@@@@ Here StateValuesRemote.validateSession.1 : '+pr); 
        pr = (pr == null ? validateCookie() : pr); 
        system.debug('@@@@@ Here StateValuesRemote.validateSession.2 : '+pr);
        return pr;
    }
    
    //AB - 12/19/14 - IP Restriction to Chat
    public PageReference validateIP(){    
        url = Apexpages.currentPage().getUrl();    
        ip = ApexPages.currentPage().getHeaders().get('True-Client-IP');
        
        if (ip == '' || ip == null){
            ip = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
        }
        
        if (ip == '' || ip == null){
            ip = ApexPages.currentPage().getHeaders().get('X-Forwarded-For');
        }

        system.debug('=========================> ip:' + ip);
        
        //AB - 01/15/15 - Optimize SOQL Query to Only Return Consumer IP Match
        List<Blocklist__c> Blocklist = new List<Blocklist__c>([SELECT Start_IP_Address__c FROM Blocklist__c WHERE Status__c = 'Blocked' AND Start_IP_Address__c =: ip]);
        system.debug('=========================> Blocklist: ' + Blocklist);
        
        for (Blocklist__c bl : Blocklist){
            system.debug('=========================> bl.Start_IP_Address__c = ip: ' + bl.Start_IP_Address__c + ' = ' + ip);

            if (bl.Start_IP_Address__c == ip){
                createBlockedUserLog('IP Block', null, url, ip);
                PageReference error = new PageReference('/apex/LiveAgentError');
                error.setRedirect(true);
                return error;
            }
        }
        return null;
    }
    
    public Pagereference validateCookie(){
        System.Cookie currentCookie = Apexpages.currentPage().getCookies().get('pge_cntid');
                system.debug('@@@@@ Here StateValuesRemote.ValidateCookie.currentCookie.0 : '+currentCookie);
        List<String> rawCookie = new List<String>();        
        String tempCookieVal = 'test';
        if(currentCookie == null){  
                rawCookie.addAll(generateCookieString(null));   
                system.debug('@@@@@ Here StateValuesRemote.ValidateCookie.rawCookie.1 : '+rawCookie);
                        //system.debug('@@@@@ Here StateValuesRemote.ValidateCookie.rawCookie.1 : '+(DateTime.parse(String.valueOf(rawCookie.get(1))).getTime() - datetime.now().getTime()));
                //currentCookie = new Cookie('pge_cntid', rawCookie.get(0), null, Integer.valueOf(DateTime.parse(String.valueOf(rawCookie.get(1))).getTime() - datetime.now().getTime())/1000, false);
                system.debug('@@@@@ Here StateValuesRemote.ValidateCookie.rawCookie.1 : '+ (Integer.valueOf(rawCookie.get(1)) - (datetime.now().getTime()/1000)));
                currentCookie = new Cookie('pge_cntid', rawCookie.get(0), null, Integer.valueOf(Integer.valueOf(rawCookie.get(1)) - (datetime.now().getTime()/1000)), false);
                        system.debug('@@@@@ Here StateValuesRemote.ValidateCookie.currentCookie.1 : '+currentCookie);
        }else{
                if(checkCookieUser(currentCookie.getValue())){
                        createBlockedUserLog('Cookie Block', null, url, ip);
                        PageReference error = new PageReference('/apex/LiveAgentError');
                                error.setRedirect(true);
                return error;
                }else{
                                rawCookie.addAll(generateCookieString(currentCookie.getValue()));
                                system.debug('@@@@@ Here StateValuesRemote.ValidateCookie.rawCookie.2 : '+rawCookie);
                                //system.debug('@@@@@ Here StateValuesRemote.ValidateCookie.rawCookie.2 : '+(DateTime.parse(String.valueOf(rawCookie.get(1))).getTime() - datetime.now().getTime()));
                        //currentCookie = new Cookie('pge_cntid', rawCookie.get(0), null, Integer.valueOf(DateTime.parse(String.valueOf(rawCookie.get(1))).getTime() - datetime.now().getTime())/1000, false);
                                system.debug('@@@@@ Here StateValuesRemote.ValidateCookie.rawCookie.1 : '+ (Integer.valueOf(rawCookie.get(1)) - (datetime.now().getTime()/1000)));
                        currentCookie = new Cookie('pge_cntid', rawCookie.get(0), null, Integer.valueOf(Integer.valueOf(rawCookie.get(1)) - (datetime.now().getTime()/1000)), false);
                                system.debug('@@@@@ Here StateValuesRemote.ValidateCookie.currentCookie.2 : '+currentCookie);
                }               
        }
        Apexpages.currentPage().setCookies(new Cookie[]{currentCookie});
                system.debug('@@@@@ Here StateValuesRemote.ValidateCookie.currentCookie.3 : '+currentCookie);
        return null;
    }

    @RemoteAction
    global static Boolean checkBlockedUser(String checkEmail, String cookieValue, String psnIdValue, String urlNow, String ipNow){  
        System.debug('************************DETAILS Email:'+ checkEmail + ':Cookie:' + cookieValue + ':PSNID:' + psnIdValue + ':url:' + urlNow  + ':ip:' + ipNow+'***********************');  
        Integer checkEmailCount = [Select count() from Contact where email  = :checkEmail];     
        List<Contact> checkContactList = new List<Contact>();
        List<Contact> returnContactList = new List<Contact>();
        List<Contact> blockContactList = new List<Contact>();      
        Boolean blockContact = false;
        Boolean cookieChanged = false;
        if(checkEmailCount > 0){
            checkContactList = [Select Id, Daily_Counter__c, Weekly_Counter__c, ChatSessionCookie_Value__c from Contact where email  =: checkEmail];            
            //look for correct contact
            returnContactList = getContactByPSNId(checkContactList, psnIdValue);
            for(Contact checkContact : returnContactList){
                cookieChanged = false;                              
                if(checkContact != null){               
                    //AB : 03/27/15 : Changed Value from 5 to 49, and from 10 to 49       
                    if(checkContact.Daily_Counter__c > 49 || checkContact.Weekly_Counter__c > 49){
                        blockContact = true;
                        checkContact.Blocked_Consumer__c = true;
                        blockContactList.add(checkContact);            
                        //add to block list
                    }
                    if(cookieValue != checkContact.ChatSessionCookie_Value__c){
                        //System.debug('************************ Change Cookied from: '+ checkContact.ChatSessionCookie_Value__c +' to:'+ cookieValue +'***********************');
                        checkContact.ChatSessionCookie_Value__c = cookieValue;
                        cookieChanged = true;               
                    }
                    //AB : 03/27/15 : Changed Value from 6 to 50
                    if(checkContact.Daily_Counter__c != null && checkContact.Daily_Counter__c < 50 && cookieChanged){
                        checkContact.Daily_Counter__c = checkContact.Daily_Counter__c + 1;
                    }else if (checkContact.Daily_Counter__c == null){
                        checkContact.Daily_Counter__c = 1;
                    }
                    //AB : 03/27/15 : Changed Value from 11 to 50
                     if(checkContact.Weekly_Counter__c != null && checkContact.Weekly_Counter__c < 50 && cookieChanged){
                        checkContact.Weekly_Counter__c = checkContact.Weekly_Counter__c + 1;
                    }else if (checkContact.Weekly_Counter__c == null){
                        checkContact.Weekly_Counter__c = 1;
                    }                                                                       
                    //update checkContact;
                }               
            }                                                                                                                                                                   
        }
        Database.update(returnContactList, false);
        if(blockContact){
                system.debug('@@@@@ Here StateValuesRemote.checkBlockedUser.blockContactList : '+blockContactList);
                system.debug('@@@@@ Here StateValuesRemote.checkBlockedUser.url : '+url);
                        createBlockedUserLog('User Block', blockContactList, urlNow, ipNow);
        }
        return blockContact;        
    }
       
    static List<Contact> getContactByPSNId(List<Contact> contactList, String psnId){                
        List<PSN_Account__c> psnAccList = new List<PSN_Account__c>();
        List<Contact> returnContactList = new List<Contact>();
        Boolean psnExists = false;              
        if(contactList.size() > 1){
            //if we get multiple contacts pull the contact which has matching PSNID                     
            for(Contact cont : contactList){                
                //psnAccList = [Select name from PSN_Account__c where Consumer__c  =: cont.id limit 1];             
                psnAccList = [Select name from PSN_Account__c where Consumer__c  =: cont.id ];
                if(!psnAccList.isEmpty()){
                    for(PSN_Account__c psnAc : psnAccList){                 
                        if(psnAc.name == psnId){
                            returnContactList.add(cont);
                            psnExists = true;                           
                        }                                           
                    }                                   
                }
            }
            if(psnExists){              
                return returnContactList;               
            }                       
        }else if (contactList.size() == 1) {                            
            return contactList;
        }       
        return contactList;
    }
    
    @RemoteAction
    global static Boolean checkCookieUser(String checkCookie){
                system.debug('@@@@@ Here checkCookieUser.checkCookie.1 :'+checkCookie);
        Boolean blockCookieUser = false;
        String cookieNow = null;
                
        if(checkCookie != null){
            cookieNow = decryptString(checkCookie);
                        system.debug('@@@@@ Here checkCookieUser.cookieNow.2 :'+cookieNow);
            Integer counter = Integer.valueOf(cookieNow.split('\\:')[0]);  
                        system.debug('@@@@@ Here checkCookieUser.counter.2 :'+counter);
            //System.debug('***'+ counter +'***'); 
            //changed counter value from 20 to 2000 for testing Date: 10/2/2014 (Preetu)      
            if(counter > 20){         
            //if(counter > 2000){
                blockCookieUser = true; 
            }           
        }                                       
        return blockCookieUser;
    }
    
    @RemoteAction
    global static String generateCookie(String cookieNow){
        String cookieStr = null;
        String cname = null;
        String dateString = null;
        if(String.isNotEmpty(cookieNow)){           
                                    
            cname = decryptString(cookieNow);                   
            Integer counter = Integer.valueOf(cname.split('\\:')[0]);
            dateString = cname.split('\\:')[2];         
            counter++;          
            cname = encryptString(counter+':tellmeasecret:'+dateString);
            cookieStr = 'pge_cntid='+cname+';expires='+dateString;
            //System.debug('***'+ counter +'***');
        }else{
            
            Date dt = System.now().dateGmt().addDays(2);
            dateString = dt.format();
            cname = encryptString('1:tellmeasecret:'+dateString);                       
            cookieStr = 'pge_cntid='+cname+';expires='+dateString;                      
        }                                       
        return cookieStr;
    }
    
    private static void createBlockedUserLog(String blockType, List<Contact> contactList, String urlNow, String ipNow){
        system.debug('@@@@@ Here StateValuesRemote.createBlockedUserLog : '+ blockType + ':' + contactList + ':' + urlNow + ':' + ipNow);
        List<Blocked_User_Log__c> blockList = new List<Blocked_User_Log__c>();
        if(contactList != null && contactList.size() > 0){
                        system.debug('@@@@@ Here StateValuesRemote.createBlockedUserLog.0 : '+ blockType + ':' + contactList + ':' + urlNow + ':' + ipNow);
                for(Contact c : contactList){                           
                        Blocked_User_Log__c bul = new Blocked_User_Log__c();
                        bul.Type_Of_Block__c = blockType;
                        bul.Blocked_User_Ref__c = c.Id;
                        if(urlNow != null){
                                bul.Block_URL_Source__c = urlNow.split('\\?')[0];
                        }                       
                        bul.Blocked_User_IP__c = ipNow;
                        blockList.add(bul);     
                }               
        }else{
                                system.debug('@@@@@ Here StateValuesRemote.createBlockedUserLog.1 : '+ blockType + ':' + contactList + ':' + urlNow + ':' + ipNow);
                                Blocked_User_Log__c bul = new Blocked_User_Log__c();
                        bul.Type_Of_Block__c = blockType;
                                if(urlNow != null){
                                bul.Block_URL_Source__c = urlNow.split('\\?')[0];
                        }                       
                        bul.Blocked_User_IP__c = ipNow;
                        blockList.add(bul);                     
        }
        List<Database.Saveresult> dsr = Database.insert(blockList, false);
        /*for (Database.SaveResult sr : dsr) {
                    if (sr.isSuccess()) {
                        System.debug('Successfully inserted account. Account ID: ' + sr.getId());
                    }
                    else {                
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('BUL fields that affected this error: ' + err.getFields());
                        }
                    }
        }*/
    }
    
    private static List<String> generateCookieString(String cookieNow){
        system.debug('@@@@@ Here generateCookieString.cookieNow :'+cookieNow);
        List<String> cookieStr = new List<String>();
        String cname = null;
        String dateString = null;
        if(String.isNotEmpty(cookieNow)){           
                        system.debug('@@@@@ Here generateCookieString.cookieNow.1 :'+cookieNow);                                    
            cname = decryptString(cookieNow);                   
            Integer counter = Integer.valueOf(cname.split('\\:')[0]);
            //dateString = cname.split('\\:')[2]+':'+cname.split('\\:')[3];
            dateString = cname.split('\\:')[2];         
            counter++;          
            cname = encryptString(counter+':tellmeasecret:'+dateString);
            cookieStr.add(cname);
            cookieStr.add(dateString);
            //cookieStr = 'pge_cntid='+cname+';expires='+dateString;
            //System.debug('***'+ counter +'***');
        }else{
            system.debug('@@@@@ Here generateCookieString.cookieNow.2 :'+cookieNow);
            Datetime dt = System.now().dateGmt().addDays(2);
            //dateString = dt.format();
            //dateString = dt.format('MM-dd-yyyy hh:mm a', 'GMT');
            dateString = String.valueOf(dt.getTime()/1000);
            system.debug('@@@@@ Here generateCookieString.dateString.2 :'+dateString);
            cname = encryptString('1:tellmeasecret:'+dateString);                     
            system.debug('@@@@@ Here generateCookieString.cname.2 :'+cname);  
            //cookieStr = 'pge_cntid='+cname+';expires='+dateString;
            cookieStr.add(cname);
            cookieStr.add(dateString);                      
        }                                       
        system.debug('@@@@@ Here generateCookieString.cookieStr :'+cookieStr);
        return cookieStr;
    }    
    
    static String encryptString(String str){
        //Blob cryptoKey = Crypto.generateAesKey(256);      
        Blob myString = Blob.valueOf(str);      
        Blob encryptedData = Crypto.encryptWithManagedIV('AES256', cryptoKey, myString);            
        return EncodingUtil.base64Encode(encryptedData);
    }
    
    static String decryptString(String encryptedData){
        Blob enyData = EncodingUtil.base64Decode(encryptedData);        
        //Blob cryptoKey = Crypto.generateAesKey(256);              
        Blob decryptedData  = Crypto.decryptWithManagedIV('AES256', cryptoKey, enyData);
        return decryptedData.toString();        
    }
    
}