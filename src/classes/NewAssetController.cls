/*******************************************************************************************************************************************************
// (c) 2013 Appirio, Inc.
//
// Description : Controller Class For VF Page NewAsset.
//                  
// Original August 13, 2013  : KapiL Choudhary(JDC) Created for the Task T-171820
// Updated : 
//          August      16,2013 : Urminder (JDC)          : Check for Existing Assets with serial Number. [T-173546]
//          August      16,2013 : KapiL Choudhary(JDC)    : Update Code for Link Asset to Contact [T-173077]
//          August      17,2013 : Urminder (JDC)          : Added Siras Warranty Map to get description of warranty code. [T-173723]
//          September   05,2013 : KapiL Choudhary(JDC)    : Update Code for Select/Create Asset when called from case object.Populate 'retId' pageParameter [T-180244]
//
//          05/07/2014 : Leena Mandadapu : Jira# SF-557 : Update old Asset's Siras Date on Asset only when Siras utility return Sold-date, otherwise do not update old asset's Siras date.
//          05/19/2014 : Leena Mandadapu : Jira# SF-683 : Added Model Number Validation logic
//          11/07/2014 : Leena Mandadapu : Jira# SF-654 : Added PPP POP attachement and PPP Record Lock fields to the Exchange process new asset creation logic
 *******************************************************************************************************************************************************/

public class NewAssetController {
    
    //private members
    String caseIdParam;
    String accountIdParam;
    String returnIdParam;
    String serialNumberPram;
    String oldAssetParam;
    public final String CLASS_NAME = 'NewAssetController';
    private Asset__c oldAsset;
    
    //Public Property
    public Asset__c newAsset{get;set;}
    public Contact consumerContact{get;set;}
    public boolean isInConsole{get;set;}    
    public String isExchange {get;set;}
    public String productSkuList{get;set;}
    //05/19/2014 LM: Added below list for Model Number validation
    map<String, String> ModelList = new map<String, String>();

    map<Id, Order_Line__c> orderLineMap = new Map<Id, Order_Line__c>();
    //Constructor
    public newAssetController(){
        
        newAsset = new Asset__c();
        
        serialNumberPram = ApexPages.currentPage().getParameters().get('SerialNumber');
        accountIdParam = ApexPages.currentPage().getParameters().get('accountId');
        caseIdParam = ApexPages.currentPage().getParameters().get('caseId');
        returnIdParam = ApexPages.currentPage().getParameters().get('retId');
        isExchange = Apexpages.currentPage().getParameters().get('exchange');
        oldAssetParam = Apexpages.currentPage().getParameters().get('oldAssetId');

        System.debug('>>>>> serialNumberPram='+ serialNumberPram );
        System.debug('>>>>> accountIdParam='+ accountIdParam );
        System.debug('>>>>> caseIdParam='+ caseIdParam );
        System.debug('>>>>> returnIdParam='+ returnIdParam );
        System.debug('>>>>> oldAssetParam='+ oldAssetParam);
        
        if(serialNumberPram != null && serialNumberPram != ''){
            newAsset.Serial_Number__c = serialNumberPram;
        }
        
        if(oldAssetParam != null && oldAssetParam != '' && isExchange == '1'){
          //LM 05/12/2014 : added Siras_Purchase_Date__c to the SOQL
          //LM 10/30/2014 : added PPP_POP_Attachment__c, PPP_Record_Lock__c to the SOQL
            oldAsset = [Select Id,Purchase_Date__c, Product__c, PPP_Product__c, PPP_Purchase_Date__c, PPP_Contract_Number__c, PPP_Start_Date__c, Serial_Number__c, 
                         Asset_Status__c, PPP_End_Date__c,  PPP_Status__c, PPP_Claim_Reason__c, PPP_Last_Update_Date__c, PPP_Service_Level__c, PPP_Refund_Eligible__c,
                         Coverage_Type__c, Warranty_Details__c, Siras_Purchase_Date__c, Proof_of_Purchase_Date__c, PPP_POP_Attachment__c, PPP_Record_Lock__c
                        From Asset__c 
                        Where Id = :oldAssetParam];
            newAsset.Purchase_Date__c = oldAsset.Purchase_Date__c;
        }
        
        if(accountIdParam != null && accountIdParam !=''){
            for(Contact c : [Select Id, MDM_ID__c, MDM_Account_ID__c From Contact where AccountId=:accountIdParam limit 1]){
                consumerContact=c;
            }
        }

        productSkuList = '';
        for(Product__c p : [select id, sku__c from Product__c where status__c = 'Active' and sub_type__c = 'Hardware']){
            productSkuList += '\"'+p.sku__c+'\",';
            //LM 05/19/2014 : added to store the products list
            ModelList.put(p.sku__c, p.id);
        }
        productSkuList = productSkuList.removeEnd(',');
    }
    
    public Pagereference saveAsset(){
        System.debug('>>>>> newAsset.Model_Number__c='+ newAsset.Model_Number__c );
        System.debug('>>>>> newAsset.Purchase_Date__c='+ newAsset.Purchase_Date__c );
        //System.debug('>>>>> oldAsset.Coverage_Type__c='+ oldAsset.Coverage_Type__c );
        
        if(newAsset.Serial_Number__c  == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Serial Number is required.'));
            return null;
        }
        if(newAsset.Model_Number__c  == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Model Number is required.'));
            return null;
        }

        //05/19/2014 LM : added model number validation
        system.debug('<<<<<<productSkuList' + productSkuList);
        if(ModelList != null && ModelList.size() > 0){
            if (!ModelList.containsKey(newAsset.Model_Number__c)) {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Model Number. Please select from the available list'));
                return null;
            }
        }

        System.debug('--oldAssetId-->'+oldAssetParam +'===>'+isExchange);
        //Asset__c oldAsset ;
        if(oldAssetParam != null && oldAssetParam != '' && isExchange == '1'){
            AssetManagement.isExchangeAsset = true;    

            //AB - 05/20/2014 - Retrieve Case information
            Case assetCase = [select Id, Leg_3_Model__c, Leg_3_Serial__c
                                     from Case
                                     where Id = :caseIdParam];
            
            if(caseIdParam !=null && caseIdParam != ''){
                assetCase.Leg_3_Model__c = newAsset.Model_Number__c;
                assetCase.Leg_3_Serial__c = newAsset.Serial_Number__c;
                
                update assetCase;
            }
            
            if(oldAsset!=null){
                System.debug('oldAsset.Product__c==>'+oldAsset.Product__c);
                //newAsset.Purchase_Date__c = oldAsset.Purchase_Date__c;
                newAsset.PPP_Claim_Reason__c = oldAsset.PPP_Claim_Reason__c;
                newAsset.PPP_Contract_Number__c = oldAsset.PPP_Contract_Number__c;
                newAsset.PPP_End_Date__c = oldAsset.PPP_End_Date__c;
                newAsset.PPP_Last_Update_Date__c = oldAsset.PPP_Last_Update_Date__c;
                newAsset.Product__c = oldAsset.Product__c;
                newAsset.PPP_Product__c = oldAsset.PPP_Product__c;            
                newAsset.PPP_Purchase_Date__c = oldAsset.PPP_Purchase_Date__c;
                //LM 05/12/2014 : Added to set the default value from Old Asset - incase if Siras Result is null in the below logic is null it will use this value
                newAsset.Siras_Purchase_Date__c =  oldAsset.Siras_Purchase_Date__c;
                newAsset.PPP_Service_Level__c = oldAsset.PPP_Service_Level__c;            
                newAsset.PPP_Start_Date__c = oldAsset.PPP_Start_Date__c;
                newAsset.PPP_Status__c = oldAsset.PPP_Status__c;
                newAsset.Coverage_Type__c = oldAsset.Coverage_Type__c;
                newAsset.Warranty_Details__c = oldAsset.Warranty_Details__c;
                newAsset.PPP_Refund_Eligible__c=oldAsset.PPP_Refund_Eligible__c;
                newAsset.Asset_Status__c = 'Active';
                //LM 10/30/2014 : Added for PYB project - POP Attachement and Suspended date copied to new Asset during exxchange process.
                newAsset.PPP_POP_Attachment__c = oldAsset.PPP_POP_Attachment__c <> null ? oldAsset.PPP_POP_Attachment__c : '';
                if(oldAsset.PPP_Record_Lock__c <> null)
                   newAsset.PPP_Record_Lock__c = oldAsset.PPP_Record_Lock__c;
                //LM 10/30/2014 : commented below as Monthly Payment option is not available for initial release.   
                /*
                if(oldAsset.PPP_Suspended_Date__c <> null)
                  newAsset.PPP_Suspended_Date__c = oldAsset.PPP_Suspended_Date__c;
                */  
                oldAsset.PPP_Status__c = 'Inactive';
                oldAsset.Asset_Status__c = 'Inactive';
                System.debug('>>>>> oldAsset.Coverage_Type__c='+ oldAsset.Coverage_Type__c +newAsset.Coverage_Type__c);
                update oldAsset;
            }

            newAsset.Last_Repair_Date__c = Date.today();

        }
        
        if(newAsset.Serial_Number__c  != null && newAsset.Serial_Number__c !=''){
            
          Asset__c existingAsset = checkExistingAsset(newAsset.Serial_Number__c, newAsset.Model_Number__c);
          
          //No Existing Asset Found For This Serial Number so we have to create Asset First.
          if(existingAsset == null) {
            
            SirasUtility sUtil = new SirasUtility();
            SirasResult  sResult = new SirasResult();
            
            try{
              sResult = sUtil.getPOP(newAsset.Serial_Number__c);
            }catch(Exception ex) {               
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage()));
                sResult=null;
            }
            
            if(sResult != null){
                
                Map<String, String> warrantyCodeDetailsMap = getWarrantyCodeDetails();
                
                newAsset.UPC_ID__c = sResult.UPC;
                newAsset.Brand_Id__c = sResult.brandID;
                newAsset.Brand_Name__c = sResult.brandName;
                newAsset.Description__c = sResult.itemDescription;
                newAsset.Release_Date__c = sResult.soldDate;
                newAsset.Item_Number__c = sResult.itemNumber;
                newAsset.Retailer__c = sResult.soldByRetailer;
                newAsset.Retailer_Id__c = sResult.soldByRetailerID;
                //LM 05/07/2014 - commented as we should not use the new Siras Purchase date as per Service Business process
                //AB 08/26/2014 - SMS-290 - Uncommented code to allow Siras_Purchase_Date__c population.
                //Added IF check to not overwrite Siras_Purchase_Date__c if exchange, to maintain original details for PPP / Service Processes
                if (isExchange == '1'){
                    newAsset.Siras_Purchase_Date__c = oldAsset.Siras_Purchase_Date__c;
                }else{
                    newAsset.Siras_Purchase_Date__c = sResult.soldDate; 
                }
  
                if(sResult.manufacturerWarrantyDetermination != null && sResult.manufacturerWarrantyDetermination != ''){
                    String detail = warrantyCodeDetailsMap.get(sResult.manufacturerWarrantyDetermination);
                    detail  = detail == null ? sResult.manufacturerWarrantyDetermination : detail;
                    newAsset.Warranty_Details__c = '['+ detail +']';
                }
                if(sResult.manufacturerWarrantyLaborDetermination  != null && sResult.manufacturerWarrantyLaborDetermination  != ''){
                    String detail = warrantyCodeDetailsMap.get(sResult.manufacturerWarrantyLaborDetermination);
                    detail  = detail == null ? sResult.manufacturerWarrantyLaborDetermination : detail;
                    newAsset.Warranty_Details__c += '[Labor: '+ detail +']';
                    System.debug('========' + detail + ' ==========' + warrantyCodeDetailsMap + ' =======' + warrantyCodeDetailsMap.get(sResult.manufacturerWarrantyLaborDetermination));
                }
                if(sResult.manufacturerWarrantyPartsDetermination  != null && sResult.manufacturerWarrantyPartsDetermination  != ''){
                    String detail = warrantyCodeDetailsMap.get(sResult.manufacturerWarrantyPartsDetermination);
                    detail  = detail == null ? sResult.manufacturerWarrantyPartsDetermination : detail;
                    newAsset.Warranty_Details__c += '[Parts: '+ detail +']';
                }
            }//end-if
            
            if(consumerContact!=null && consumerContact.MDM_Account_ID__c!=null){
                newAsset.MDM_Account_ID__c = consumerContact.MDM_Account_ID__c;
            }
            
            // insert asset
            try{
                newAsset.Asset_Status__c = 'Active';
                insert newAsset;
                if(isExchange == '1'){
                    Case assetCase;
                    Asset__c newAssetObj;
                    List<Staging_PPP_Outbound__c> SPOList = new List<Staging_PPP_Outbound__c>();
                    Set<Id> orderIds = new Set<Id>();
                    list<Order__c> orderList = new list<Order__c>();
                    if(caseIdParam !=null && caseIdParam != ''){
                        assetCase = [select Id,Bill_To__r.Address_Line_1__c,Bill_To__r.Address_Line_2__c,
                                            Bill_To__c,Bill_To__r.City__c,Bill_To__r.State__c,
                                            Bill_To__r.Country__c,Bill_To__r.Postal_Code__c,
                                            Contact.FirstName,Contact.LastName,Contact.Email,
                                            Contact.Alt_Phone_Unformatted__c, ContactId,Sub_Area__c, 
                                            Contact.Phone_Unformatted__c,CreatedDate,CaseNumber,Order__c,
                                            Invoice_Date__c,Invoice_Amount__c
                                     from Case
                                     where Id = :caseIdParam];
                    }
                    for(Asset__c ast : [select Id, PPP_Contract_Number__c,PPP_Purchase_Date__c, 
                                               Proof_of_Purchase_Date__c,PPP_Start_Date__c,
                                               PPP_Product_SKU__c,Model_Number__c,Price__c,
                                               Serial_Number__c,PPP_Claim_Reason__c,PPP_Service_Level__c
                                        from Asset__c
                                        where Id =:newAsset.Id]){
                        newAssetObj = ast;
                    }
                    if(newAssetObj != null && newAssetObj.Id != null){
                        //SPOList.add(createStagingPPPOutbound(newAssetObj, assetCase));
                    }
                    if(oldAsset != null && oldAsset.Id != null && assetCase.Order__c!=null){
                        //SPOList.add(createStagingPPPOutbound(oldAsset, assetCase));
                        for(Order__c o : [select Asset__c, Asset__r.Serial_Number__c, ESP_Cancel_After_30_Days__c, ESP_Refunded_by_Dealer__c, ESP_Record_Type__c from Order__c where ID = :assetCase.Order__c]) { //
                            orderIds.add(o.Id);
                            o.Asset__c = newAsset.Id;
                            orderList.add(o);
                        }
                    }                   
                    orderLineMap = getOliMap(orderIds);

                    if(orderList.isEmpty()) {
                        
                        System.debug('>>>>>>>>>>>Old Asset Serial Number<<<<<<<<<<<<<<< ' + oldAsset.Serial_Number__c);
            
                        for(Order__c o : [select Asset__c, Asset__r.Serial_Number__c, ESP_Record_Type__c, Asset__r.PPP_Status__c from Order__c where Asset__r.Serial_Number__c=:oldAsset.Serial_Number__c AND (ESP_Record_Type__c='A' OR ESP_Record_Type__c='U') AND Asset__r.PPP_Status__c='Inactive' limit 1]) {
                            o.Asset__c = newAsset.Id;
                            orderList.add(o);
                         }
            
                    }
                    if(orderList.size() > 0){
                        update orderList;
                    }
                   
                }
                
            }
            catch(Exception ex){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
                return null;
            }
 
          } 
          else{
            newAsset = existingAsset;
          }
        }
        
        insertJunctionRecord();

        String appId;
        String appName = Label.Service_Console_Name;
            
        for(AppMenuItem app : [select Id from AppMenuItem where Name = :appName]) {
            appId = app.Id;
        }//end-for

        //PageReference returnPage = new PageReference('/console?tsid='+ appId);

        PageReference returnPage;
        
        if(caseIdParam != null && caseIdParam != ''){


            Case currentCase = new Case (Id=caseIdParam);
        
            try{
 
                currentCase.Asset__c=newAsset.Id;
                update currentCase;
            }
            catch(Exception ex){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
                    return null;
            }
            
            returnPage= new ApexPages.StandardController(new Case (Id=caseIdParam)).view();
            //returnPage.setAnchor('%2F' + caseIdParam);
        }
        else if(consumerContact!=null){
            returnPage = new ApexPages.StandardController(new Contact(Id=consumerContact.Id)).view();
        }
        else{
            returnPage= new ApexPages.StandardController(newAsset).view();
            //returnPage.setAnchor('%2F' + newAsset.Id);
        }
        returnPage.setRedirect(true);
        return returnPage;
    }
    
    private map<Id, Order_Line__c> getOliMap(Set<Id> orderIds) { 
         map<Id, Order_Line__c> oliMap =  new map<Id, Order_Line__c>();
         for(Order_Line__c oli : [select Order__c 
                                        from Order_Line__c 
                                        where Order__c IN : orderIds]) {
            oliMap.put(oli.Order__c, oli);
        }
        return oliMap;
    }
    
    // Insert a Record to link between Consumer(Contact) and Asset__c.
    private void insertJunctionRecord(){

        if(consumerContact==null){
            return;
        }

        list<Consumer_Asset__c> currentConnectionList = [select id from Consumer_Asset__c 
                                                            where Consumer__c =:consumerContact.Id and Asset__c =:newAsset.id];
                                                                        
        if(currentConnectionList.isEmpty()){//No Record Exists for this consumer and this asset.
              try{
                Consumer_Asset__c conAsset = new Consumer_Asset__c(consumer__c = consumerContact.Id , Asset__c = newAsset.id);
                insert conAsset;
              }
              catch(Exception ex){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
              }
        }
 
    }
    
    public Pagereference cancelAsset(){

        String appId;
        String appName = Label.Service_Console_Name;
            
        for(AppMenuItem app : [select Id from AppMenuItem where Name = :appName]) {
            appId = app.Id;
        }//end-for

        PageReference returnPage = new PageReference('/console?tsid='+ appId);
            
        if(caseIdParam != null && caseIdParam != ''){
            //if(isInConsole) {
              returnPage= new ApexPages.StandardController(new Case (Id=caseIdParam)).view();
            /*} else {
              returnPage.setAnchor('%2F' + caseIdParam);
            }*/
        }
        else if(accountIdParam != null && accountIdParam != ''){
            //if(isInConsole) {
              returnPage= new ApexPages.StandardController(new Account (Id=accountIdParam)).view();
            /*} else {
              returnPage.setAnchor('%2F' + accountIdParam);
            }*/
        }
        
        returnPage.setRedirect(true);
        return returnPage;
    }
    
    private Asset__c checkExistingAsset(String serialNumber, String modelNumber) {
        for(Asset__c asset : [select id, Serial_Number__c from Asset__c where Model_Number__c =: modelNumber and Serial_Number__c = :serialNumber limit 1]) {
            if(serialNumber.equals(asset.Serial_Number__c))
                return asset;
        }
        return null;
    }
    
    private Map<String, String> getWarrantyCodeDetails() {
        map<String, String> warrantyDetailsMap = new map<String, String>();
        for(Siras_Warranty_Code__c wc : Siras_Warranty_Code__c.getAll().values()) {
            warrantyDetailsMap.put(wc.Name, wc.Description__c);
        }
        return warrantyDetailsMap;
    }
    
    private Staging_PPP_Outbound__c createStagingPPPOutbound (Asset__c assetObj, Case caseObj, Order__c o) {
        Staging_PPP_Outbound__c SPO = new Staging_PPP_Outbound__c();
        SPO.Billing_Address_1__c = caseObj.Bill_To__r.Address_Line_1__c;
        SPO.Billing_Address_2__c = caseObj.Bill_To__r.Address_Line_2__c;
        SPO.Billing_City__c = caseObj.Bill_To__r.City__c;
        SPO.Billing_State__c = caseObj.Bill_To__r.State__c;
        SPO.State_of_Sale__c = caseObj.Bill_To__r.State__c;
        SPO.Billing_Country__c = caseObj.Bill_To__r.Country__c;
        SPO.Country_of_Sale__c =  caseObj.Bill_To__r.Country__c;
        SPO.Billing_Zip_Postal_Code__c = caseObj.Bill_To__r.Postal_Code__c;
        SPO.Zip_Code_of_Sale__c = caseObj.Bill_To__r.Postal_Code__c;
        
        SPO.Consumer_First_Name__c = caseObj.Contact.FirstName;
        SPO.Consumer_Last_Name__c = caseObj.Contact.LastName;
        SPO.Consumer_Email_Address__c = caseObj.Contact.Email;
        SPO.PPP_Contract_Number__c = assetObj.PPP_Contract_Number__c;
        
        SPO.Order__c = o.Id;
        Order_Line__c oli = orderLineMap.get(o.Id);
        if(caseObj.Contact.Alt_Phone_Unformatted__c <> null) {
            SPO.Consumer_Area_Code_2__c = caseObj.Contact.Alt_Phone_Unformatted__c.length() > 3 ? caseObj.Contact.Alt_Phone_Unformatted__c.subString(0,3) : '';
            SPO.Consumer_Phone_Number_2__c = caseObj.Contact.Alt_Phone_Unformatted__c.length() > 10 ? caseObj.Contact.Alt_Phone_Unformatted__c.subString(3,10) : '';
        }

        SPO.Consumer_Area_Code_1__c = caseObj.Contact.Phone_Unformatted__c.length() > 3 ? caseObj.Contact.Phone_Unformatted__c.subString(0,3) : '';
        SPO.Consumer_Phone_Number_1__c = caseObj.Contact.Phone_Unformatted__c.length() >= 10 ? caseObj.Contact.Phone_Unformatted__c.subString(3,10) : '';
        
        SPO.Unit_Warranty_Duration__c = '12';
        SPO.Manufacture_Code__c = 'SONY';
        SPO.Order_Record_Type__c = o.ESP_Record_Type__c;
        
        SPO.PPP_Purchase_Date__c = String.valueOf(assetObj.PPP_Purchase_Date__c == null ? '' : String.valueOf(assetObj.PPP_Purchase_Date__c).replace(' ', 'T'));
        SPO.Proof_of_Purchase_Date__c =  String.valueOf(assetObj.Proof_of_Purchase_Date__c == null ? '' : String.valueOf(assetObj.Proof_of_Purchase_Date__c).replace(' ', 'T'));
        SPO.PPP_Start_Date__c = String.valueOf(assetObj.PPP_Start_Date__c == null ? '' : String.valueOf(assetObj.PPP_Start_Date__c).replace(' ', 'T'));
        SPO.Order_Dealer__c = o.ESP_Dealer__c;
        // SPO.PPP_SKU__c = assetObj.PPP_Product_SKU__c;
        SPO.PPP_SKU__c=o.Asset__r.PPP_Product_SKU__c;
        
        SPO.PPP_Cancel_After_30_Days__c = o<> null ? String.valueOf(o.ESP_Cancel_After_30_Days__c) : '';
        SPO.PPP_Tax__c = oli <> null ? String.valueOf(o.Total_Tax__c) : '';
        SPO.PPP_Refund_by_Dealer__c = o <> null ? String.valueOf(o.ESP_Refunded_by_Dealer__c) : '';
        SPO.Unit_Model_Number__c = assetObj.Model_Number__c;
        SPO.Unit_Price__c = String.valueOf(assetObj.Price__c == null ? '0' : ''+assetObj.Price__c);
        SPO.Unit_Serial_Number__c = assetObj.Serial_Number__c;
        
        //SPO.Vender_Number_Name__c = o.Case__r.Vender_Number_Name__c;
        SPO.Loss_Date__c = String.valueOf(caseObj.CreatedDate).subString(0, String.valueOf(caseObj.CreatedDate).indexOf(' '));
        SPO.Invoice_Number__c = caseObj.CaseNumber; 
        SPO.Invoice_Date__c = String.valueOf(caseObj.Invoice_Date__c);
        SPO.Invoice_Amount__c = String.valueOf(caseObj.Invoice_Amount__c);
        //UV : Fix-Claim Reason field Mapping (T-239028)
        //SPO.PPP_Claim_Reason__c = assetObj.PPP_Claim_Reason__c;
        if(caseObj.Sub_Area__c <> null && caseObj.Sub_Area__c.length() > 30) {
            SPO.PPP_Claim_Reason__c = caseObj.Sub_Area__c.subString(0,30);
        } else {
            SPO.PPP_Claim_Reason__c = caseObj.Sub_Area__c;
        }
        SPO.PPP_Service_Level__c = o.Asset__r.PPP_Service_Level__c;
        SPO.PPP_Contract_Number__c=o.Asset__r.PPP_Contract_Number__c;

        return SPO;
    }

}