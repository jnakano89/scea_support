/*********************************************************************************************************************************************************************************
Class Name  : CreateSAPRecordController
Description :  This class will create the On-demand SAP staging object from service case.
Task        :  T-221966
Created By  :  Urminder Vohra (JDC)

Updates     :
05/14/2014  : Leena Mandadapu - JIRA SF-687 - added Export to SAP Model/Material Number mapping logic - ATLAS Release 2.32 on 7/23/2014
 *********************************************************************************************************************************************************************************/
public with sharing class CreateSAPRecordController {
    public String caseId{get;set;}

    public boolean invalidRecord{get;set;} 
    public list<String> errors{get;set;}

    Case selectedCase;
    public CreateSAPRecordController() {
        invalidRecord = false; 
        errors = new list<String>();
    }

    public Pagereference createSAP() {
        caseId = Apexpages.currentPage().getParameters().get('caseId');
        String serviceRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        String SAPMaterialnum;
        Staging_SAP_Outbound__c sap;
        if(caseId == null) {
            invalidRecord = true;
            errors.add('CaseId must be passed in url as parameter.');
            return null;
        } else {
            selectedCase = getCaseDetails(caseId);

            if(selectedCase == null) {
                errors.add('Invalid Case id passed in parameter.');
                invalidRecord = true;
                return null;
            } else {
                if(selectedCase.RecordTypeId != serviceRecordTypeId) {
                    errors.add('This case is not service case.');
                    invalidRecord = true;
                    return null;
                }
                
                sap = new Staging_SAP_Outbound__c();
                sap.case_Id__c = selectedCase.External_Case_Number__c;
                sap.Case_Status__c = selectedCase.Status;
                sap.Case_Ship_To_First_Name__c = selectedCase.Contact.FirstName;
                sap.Case_Ship_To_Last_Name__c = selectedCase.Contact.LastName;
                sap.Consumer_Phone__c = selectedCase.Contact.Phone_Unformatted__c;
                sap.Consumer_Email__c = selectedCase.Contact.Email;
                sap.Case_Owner__c = selectedCase.Service_Location__r.Name;
                //sap.Case_Feed__c = selectedCase.Comments;

                System.Debug('<<<<<Case Comments>>>> '+selectedCase.CaseComments);
                sap.Case_Feed__c= sObjectUtility.getCaseFeed(selectedCase.Id);

                if(selectedCase.Asset__r.Purchase_Date__c <> null) {
                    DateTime dtValue = DateTime.newInstance(selectedCase.Asset__r.Purchase_Date__c.year(), selectedCase.Asset__r.Purchase_Date__c.month(), selectedCase.Asset__r.Purchase_Date__c.day());
                    string sFormattedDate = dtValue.format('MMYYYY');
                    String day = String.valueOf(selectedCase.Asset__r.Purchase_Date__c.day()).length() == 2 ? '' + selectedCase.Asset__r.Purchase_Date__c.day() : '0' + selectedCase.Asset__r.Purchase_Date__c.day();
                    sFormattedDate = sFormattedDate.subString(0,2) + day + sFormattedDate.subString(2,6);
                    sap.Asset_Outbound_Purchase_Date__c = sFormattedDate;
                }

                //LM 5/14: commented and Updated below if condition as 'OOW' is not a correct value
                //if(selectedCase.Fee_Type__c != 'OOW')
                if(selectedCase.Fee_Type__c != 'Out Of Warranty') {	
                    sap.Asset_Inbound_In_Warranty__c = 'N';
                } else {
                    sap.Asset_Inbound_In_Warranty__c = 'Y';
                }
                if(selectedCase.Ship_To__c == null) {
                    invalidRecord = true;
                    errors.add('Ship To address must be populated.');
                } else {
                    sap.Case_Ship_To_Address_1__c = selectedCase.Ship_To__r.Address_line_1__c;
                    sap.Case_Ship_To_Address_2__c = selectedCase.Ship_To__r.Address_line_2__c;
                    sap.Case_Ship_To_City__c = selectedCase.Ship_To__r.City__c;
                    sap.Case_Ship_To_State__c = selectedCase.Ship_To__r.State__c;
                    sap.Case_Ship_To_Country__c = selectedCase.Ship_To__r.Country__c;
                    sap.Case_Ship_To_Zip_Code__c = selectedCase.Ship_To__r.Postal_Code__c;
                    boolean recordFound = false;
                    for(Zip_Code__c a : [select Id from Zip_Code__c where Name = :sap.Case_Ship_To_Zip_Code__c]) {
                        recordFound = true;                 
                    }
                    if(!recordFound) {
                        invalidRecord = true;
                        errors.add('Invalid Postal Code in Ship To Address.');
                    }
                    if(selectedCase.Received_Date__c <> null) {
                        DateTime dtValue = DateTime.newInstance(selectedCase.Received_Date__c.year(), selectedCase.Received_Date__c.month(), selectedCase.Received_Date__c.day());
                        string sFormattedDate = dtValue.format('MMYYYY');
                        String day = String.valueOf(selectedCase.Received_Date__c.day()).length() == 2 ? '' + selectedCase.Received_Date__c.day() : '0' + selectedCase.Received_Date__c.day();
                        sFormattedDate = sFormattedDate.subString(0,2) + day + sFormattedDate.subString(2,6);
                        sap.Case_Unit_Received_Date__c= sFormattedDate; 
                    } else {
                    	//LM 5/14 : Commented below line and added statement to set received date to today 
                        //sap.Case_Unit_Received_Date__c = '';
                        sap.Case_Unit_Received_Date__c = formatDateMMDDYYYY(Date.today());
                    }
                    
                        //LM 5/14 : Updated below statements as per JIRA SF-687
                        //sap.Asset_Inbound_Serial_Number__c = selectedCase.Console_Serial_received__c <> null ? selectedCase.Console_Serial_received__c : '';
                        sap.Asset_Inbound_Serial_Number__c = selectedCase.Console_Serial_received__c <> null ? selectedCase.Console_Serial_received__c : 'NONE';
                        
                        //LM 5/15 : Added SAP Material# - ATLAS Model# logic
                        system.debug('<<<<<<Asset Model Num <<<<<<<<<' + selectedCase.Asset__r.Model_Number__c +'>>>>>>>>>>>>>>>>>>>>');
                        if (selectedCase.Asset__r.Model_Number__c != null) {
                         system.debug('<<<<<<GET SAP Num <<<<<<<<<' + getSAPMaterialNum(selectedCase.Asset__r.Model_Number__c) +'>>>>>>>>>>>>>>>>>>>>');
                         SAPMaterialnum = getSAPMaterialNum(selectedCase.Asset__r.Model_Number__c) <> null ? getSAPMaterialNum(selectedCase.Asset__r.Model_Number__c) : '';	
                             if (SAPMaterialnum != '') {
                                sap.Asset_Inbound_Model_Number__c = SAPMaterialnum;//getSAPMaterialNum(selectedCase.Asset__r.Model_Number__c);
                             }
                             else {
                             	invalidRecord = true;
                             	errors.add('No SAP Material# found for this Model#.');
                             }
                        }     
                        else {
                        	//add else logic
                        }     
                       //sap.Asset_Inbound_Model_Number__c = selectedCase.Console_Model_Received__c <> null ? ValidationRulesUtility.removeAllSpecialChars(selectedCase.Console_Model_Received__c) : '';          
                        sap.Asset_Inbound_Product__c = selectedCase.Product__c;
                        sap.Asset_Outbound_Model_Number__c = selectedCase.Leg_3_Model__c<>NULL?ValidationRulesUtility.removeAllSpecialChars(selectedCase.Leg_3_Model__c):  sap.Asset_Inbound_Model_Number__c;
                        sap.Asset_Outbound_Serial_Number__c = selectedCase.Leg_3_Serial__c<>NULL?selectedCase.Leg_3_Serial__c:'';
                    
                }
            }
            try {
                if(!invalidRecord) {

                    webServiceCallout( selectedCase, sap);
                    insert sap;

                }
            } catch(Exception ex) {
                invalidRecord = true;
                errors.add('Error Occured while inserting new SAP Record.' + ex.getMessage());
            }
        }
        return null;
    }

    private Case getCaseDetails(String csId) {
        Case selCase;
        for(Case cs : [select Id, RecordTypeId,  Status, Contact.FirstName, Contact.LastName, Contact.Phone_Unformatted__c, Contact.Email, ASB_Service_Center__c, Received_Date__c, Asset__r.Warranty_Available__c, Ship_To__c,
                       Ship_To__r.Address_line_1__c, Ship_To__r.Address_line_2__c, Ship_To__r.City__c, Ship_To__r.State__c, Ship_To__r.Country__c, Ship_To__r.Postal_Code__c,
                       Asset__r.Model_Number__c, Asset__r.Serial_Number__c, External_Case_Number__c, Service_Location__r.Name, Asset__r.Purchase_Date__c,Console_Model_Received__c, Product__c, Fee_Type__c, Console_Serial_received__c,Source_ID__c,Leg_3_Model__c,Leg_3_Serial__c,
                       (Select CommentBody From CaseComments)
                       from Case 
                       where Id = :csId]) {
            selCase = cs;
        }
        return selCase;
    }
    
    //LM 5/15/2014 : Added this method to get the SAP Material Number for an ATLAS Model#
    private static String getSAPMaterialNum(String ModelNum){
      Product__c prd;
      for( Product__c p : [select Id, SAP_Material_Number__c from Product__c where SKU__c = :ModelNum and sub_type__c <> 'Service' and Status__c = 'Active' limit 1]) {
      	prd = p;	
      } 
      if(prd != null) {
      	  return prd.SAP_Material_Number__c;	
      	}
      return null;	
    }

    public Pagereference redirectToCase() {
        String csId = Apexpages.currentPage().getParameters().get('caseId');
        if(csId <> null) {
            return new Pagereference('/' + csId);
        }
        return null;
    }
    // T-241576 :- Develop WebService Callout to SAP
    public void webServiceCallout(Case selectedCases,Staging_SAP_Outbound__c sap) {

        List<SonyComMiddlewareSfdcToSap_New.data> sfdcDatas = new List<SonyComMiddlewareSfdcToSap_New.data>();
        sfdcDatas.add(webServiceCalloutData(selectedCases,sap));
        SonyComMiddlewareSfdcToSap_New.SFDCTOSAPSOAP11BindingQSPort sfdcToSap = new SonyComMiddlewareSfdcToSap_New.SFDCTOSAPSOAP11BindingQSPort();
        SonyComMiddlewareSfdcToSap_New.Output_details_element outputElement = sfdcToSap.process(sfdcDatas);
        System.debug('outputElement.Status_msg -----  '+outputElement );
        if(outputElement != null) {
            if(outputElement.Status_msg != null && (outputElement.Status_msg.equals('File created Successfully !') || outputElement.Status_msg.contains('Success'))) {
                sap.Status__c = 'Success';  
                sap.Processed_Date_Time__c = Datetime.now();
            }else if(outputElement.errormsg != null) {
                SonyComMiddlewareSfdcToSap_New.errortype errormsg = outputElement.errormsg;
                sap.Status__c = 'Failure';  
                sap.Error_Message__c = errormsg.ErrorCode +' '+errormsg.ErrorMessage+' '+errormsg.Description;  
            }
        }

    }

    // Create data record based on Staging_SAP_Outbound__c ,Case
    public SonyComMiddlewareSfdcToSap_New.data  webServiceCalloutData(Case selectedCases,Staging_SAP_Outbound__c sap) {

        SonyComMiddlewareSfdcToSap_New.data sfdcDatas = new SonyComMiddlewareSfdcToSap_New.data();  
        sfdcDatas.Case_Id_xC = sap.Case_Id__c;
        sfdcDatas.Case_Status_xc = sap.Case_Status__c;
        sfdcDatas.Case_Ship_To_First_Name_xc = sap.Case_Ship_To_First_Name__c;
        sfdcDatas.Case_Ship_To_Last_Name_xc = sap.Case_Ship_To_Last_Name__c;
        sfdcDatas.Case_Ship_To_Address_1_xc = sap.Case_Ship_To_Address_1__c;
        sfdcDatas.Case_Ship_To_Address_2_xc = sap.Case_Ship_To_Address_2__c;
        sfdcDatas.Case_Ship_To_City_xc = sap.Case_Ship_To_City__c;
        sfdcDatas.Case_Ship_To_State_xc = sap.Case_Ship_To_State__c;
        sfdcDatas.Case_Ship_To_Zip_Code_xc = sap.Case_Ship_To_Zip_Code__c;
        sfdcDatas.Case_Ship_To_Country_xc = sap.Case_Ship_To_Country__c;
        sfdcDatas.Consumer_Phone_xc = sap.Consumer_Phone__c;
        sfdcDatas.Consumer_Email_xc = sap.Consumer_Email__c;
        sfdcDatas.Case_Owner_xc = sap.Case_Owner__c;
        sfdcDatas.Asset_Inbound_Model_Number_xc = sap.Asset_Inbound_Model_Number__c;
        sfdcDatas.Asset_Inbound_Product_xc = sap.Asset_Inbound_Product__c;
        sfdcDatas.Case_Feed_xc = sap.Case_Feed__c;
        sfdcDatas.Case_Unit_Received_Date_xc = sap.Case_Unit_Received_Date__c;
        sfdcDatas.Asset_Outbound_Model_Number_xc = sap.Asset_Outbound_Model_Number__c;
        sfdcDatas.Asset_Outbound_Serial_Number_xc = sap.Asset_Outbound_Serial_Number__c;
        sfdcDatas.Asset_Outbound_Purchase_Date_xc = sap.Asset_Outbound_Purchase_Date__c;
        sfdcDatas.Asset_Inbound_In_Warranty_xc = sap.Asset_Inbound_In_Warranty__c;
        sfdcDatas.Asset_Inbound_Serial_Number_xc = sap.Asset_Inbound_Serial_Number__c;
        sfdcDatas.Source = 'SFDC';
        return sfdcDatas;
    }
    
    //LM 05/14 : added for formatting date to MMDDYYYY format
     private static String formatDateMMDDYYYY(Date dt) {
        if(dt == null) return '';
        String month = dt.month() < 10 ? '0' + dt.month() : '' + dt.month();
        String day = dt.day() < 10 ? '0' + dt.day() : '' + dt.day();
        return '' + month  + day + dt.year();
    }

}