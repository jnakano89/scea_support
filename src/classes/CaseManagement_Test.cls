/****************************************************************************************************************************************************************************************
Created :   Appirio
        :   Aaron Briggs : 06/26/2014 : SF-849 : Add LATAM Web-to-Case and Email-to-Case test scenarios
        :   Leena Mandadapu : 02/17/2015 : Updated the class due to deployment issues- Atlas Release 3.04
        :   Leena Mandadapu : 04/23/2015 : SMS-835 : Updated to add Blocked Address test logic - Atlas Release 3.07
*****************************************************************************************************************************************************************************************/
@isTest(seeAllData=true)
global class CaseManagement_Test {

    static testMethod void myUnitTest() {
        //Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HTTPMockCyberSource());
        list<Address__c> addList = TestClassUtility.createAddress(1, false);
        insert addList;
       
        list<Product__c> productList = TestClassUtility.createProduct(1, true);
       
        //LM 02/17/2015 : Commented the below code and added new Test asset creation method due to SOQL limits issue
        /*list<Asset__c> assetList = TestClassUtility.createAssets(1, true);
        assetList[0].PPP_Product__c = null;
        assetList[0].Product__c = productList[0].Id;
        update assetList[0];*/
        
        //LM 02/17/2015 : Added below Test Asset creation code
        Asset__c asset = new Asset__c();
        asset.Serial_Number__c = '123TEST';
		asset.Purchase_Date__c = Date.today();
		asset.Model_Number__c = 'CUH-1001A';
		asset.PPP_Product__c = null;
        asset.Product__c = productList[0].Id;
        insert asset;
       
        list<Contact> cntList = TestClassUtility.createContact(2, false);
        cntList[0].FirstName = 'Test Name';
        cntlist[0].Email = 'test@test.com';
        cntlist[0].Preferred_Language__c = 'Eng';
        cntlist[0].Bill_To__c = addList[0].Id;
        insert cntlist;
        
        
         //code added by preetu
    
      String RecTypeId= [select Id from RecordType where (Name='Person Account') and (SobjectType='Account')].Id; 
      PSN_Account__C psnAcc1 = [select Id,consumer__r.AccountId,Name from PSN_Account__c where Name='xgamertomx'];
  
  
      Account acc = new Account();
      acc.LastName = 'test';
      acc.FirstName = 'ftest';
      acc.Phone = '12345';
      //acc.PSN_Account__c = psnAcc1.Id;
      acc.RecordTypeID=RecTypeId;
      insert acc;
      
     Contact repContact = [SELECT id,FirstName, LastName
     FROM Contact
     WHERE isPersonAccount = true AND AccountId = :acc.id];
       
        PSN_Account__c psnAcc = new PSN_Account__c();
        psnAcc.Consumer__c = repContact.id;
        psnAcc.MDM_Account_ID__c = '13456';
        psnAcc.Status__c = 'Inactive';
        psnAcc.Name = 'TestPsn1111';
        insert psnAcc;
       
        psnAcc.Status__c = 'Active';
       
        update psnAcc;
        
        //LM 02/17/2015: moved Test.startTest method before the case DML statements to avoid/reset governor limits.
        Test.startTest();
       
        Case cs = TestClassUtility.createCase('New', 'TEst', false);
        cs.ContactId = cntList[0].Id;
        cs.RecordTypeId =  GeneralUtiltyClass.RT_PURCHASE_ID;
        cs.Sub_Area__c = 'NEw';
        cs.Status = 'New';
        //cs.Asset__c = assetList[0].id;
        cs.Asset__c = asset.Id;
        insert cs;
           
        //LM 04/08/2014 : Added for LATAM WEB-TO-CASE JIRA# SF-200
        Case csLatam = TestClassUtility.createCase('Open', 'Web', false);
        csLatam.RecordTypeId =  GeneralUtiltyClass.RT_LATAM_WEB_CASE_ID;
        csLatam.Web_Source__c = 'LATAM WEB CASE';
        csLatam.Web_Last_Name__c = 'Latam LN';
        csLatam.Web_first_Name__C = 'Latam FN';
        csLatam.Web_Country__c = 'Mexico';
        insert csLatam;
       
        //LM 05/14/2014 : Added for SF-517 to test update triggers
        csLatam.Web_Country__c = 'Chile';
        update csLatam;
                     
        list<Order__c> ordrList = TestClassUtility.creatOrder(cntList[0].Id, 3, false);
        list<Account>personAccountList = TestClassUtility.createPersonAccount(1, true);
        list<contact> lstcontact = [select id from contact where accountid in:personAccountList limit 1];
        
        for(Order__c o : ordrList){
            o.Case__c = cs.Id;
            o.Consumer__c = lstcontact[0].Id;
        }
        insert ordrList;
       
        Payment__c payment = new Payment__c();
        payment.Payment_Status__c = 'Charged';
        payment.Order__c = ordrList[0].Id;
        insert payment;
       
        ordrList[0].Payment__c = payment.Id;
        //ordrlist[0].Asset__c = assetList[0].Id;
        ordrlist[0].Asset__c = asset.Id;
        update ordrList[0];
      
        cs.Order__c = ordrList[0].Id;
        cs.SCEA_Product__c = productList[0].id;
        cs.Order__c = ordrList[0].Id;
        cs.Status = 'Payment Complete';
        update cs;
       
        Test.stopTest();
    }

    static testMethod void myUnitTest2() {
        //Test.startTest();
        
        list<Product__c> productList = TestClassUtility.createProduct(1, true);
        
        //LM 02/17/2015 : Commented the below code and added new Test asset creation method due to SOQL limits issue
       /* list<Asset__c> assetList = TestClassUtility.createAssets(1, true);
        assetList[0].PPP_Product__c = null;
        assetList[0].Product__c = productList[0].Id;
        update assetList[0];*/
        
        //LM 04/23/2015 : added for Retailer block address
        Blocked_Address__c blockAddr = new Blocked_Address__c();
        blockAddr.Address_Line_1__c = '123test';
        blockAddr.Address_Line_2__c = 'blank';
        blockAddr.Postal_Code__c = '123456';
        blockAddr.Status__c = 'Blocked';
        insert blockAddr;
        
        
        //LM 02/17/2015 : Added below Test Asset creation code
        Asset__c asset = new Asset__c();
        asset.Serial_Number__c = '123TESTING';
		asset.Purchase_Date__c = Date.today();
		asset.Model_Number__c = 'CUH-1001A';
		asset.PPP_Product__c = null;
        asset.Product__c = productList[0].Id;
        insert asset;
       
        list<Contact> cntList = TestClassUtility.createContact(2, true);
        list<Order__c> ordrList = TestClassUtility.creatOrder(cntList[0].Id, 3, false);
        
        Product__c prod = TestClassUtility.createProduct(1, false)[0];
        prod.Sub_Type__c = 'AD';
        prod.Product_Type__c = 'AD';
        insert prod;
        
                  
        Case cs = TestClassUtility.createCase('New', 'TEst', false);
        cs.ContactId = cntList[0].Id;
        cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Refund').getRecordTypeId();
        cs.Sub_Area__c = 'NEw';
        insert cs;
         
        cs.SCEA_Product__c = productList[0].id;
        cs.Order__c = ordrList[0].Id;
        cs.Refund_Amount__c = 20;
        cs.Refund_Reason__c = 'test';
        cs.Status = 'Cancel Ready';
        cs.Refund_Type__c = 'test';
        update cs;
       
        Case csNew = TestClassUtility.createCase('New', 'TEst', false);
        csNew.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Grief Report').getRecordTypeId();
        csNew.Offender__c = 'TestPsn1111';
        insert csNew;
       
        //LM 05/14/2014 : Added for SF-517 to test update triggers
        csNew.Status = 'Case Cancelled';
        update csNew;
       
        Case cs1 = TestClassUtility.createCase('New', 'TEst', false);
        cs1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
        cs1.ContactId = cntlist[0].id;
        insert cs1;
        
         //LM 02/17/2015: moved Test.startTest method before the case DML statements to avoid/reset governor limits.
        Test.startTest();
           
        Case cs2 = TestClassUtility.createCase('New', 'TEst', false);
        cs2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('PS Network').getRecordTypeId();
        cs2.ContactId = cntlist[0].id;
        insert cs2;
        //LM 05/14/2014 : Added for SF-517 to test update triggers
        cs2.Status = 'Case Cancelled';
        update cs2;
       
        //PSN_Account__c for case should populated by related PSN_Account__c related to contact.
        //system.assertNotEquals(null, [select PSN_Account__c from case where id=:cs2.id].get(0).PSN_Account__c );
       
        /*assetList[0].PPP_Status__c = 'AD';
        assetList[0].PPP_Product__c = prod.Id;*/
        asset.PPP_Status__c = 'AD';
        asset.PPP_Product__c = prod.Id;
        update asset;
       
        cs1.Status = 'Ship Complete';
        cs1.Accidental_Damage__c = true;
        //cs1.Asset__c = assetList[0].Id;
        cs1.Asset__c = asset.Id;
        update cs1;
       
        Case cs3 = TestClassUtility.createCase('New', 'TEstGeneral', false);
        cs3.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('General').getRecordTypeId();
        cs3.ContactId = cntList[0].id;
        cs3.OwnerId = Userinfo.getUserId();
        insert cs3;
        
        //LM 05/14/2014 : Added for SF-517 to test update triggers
        cs3.Status = 'Case Cancelled';
        update cs3;
       
        cntList[0].email = 'test@email.com';
        cntList[0].FirstName = 'fName';
        update cntList[0];
       
        //Mockset for this class, doen't  works from triggers.
        Test.setMock(HttpCalloutMock.class, new HTTPMockSurvey());
        cs3.Status = 'Closed';
        CaseManagement.updateStagingSurveyObject(new list<Case>{cs3});
        test.stopTest();
    }
    
    global class HTTPMockSurvey implements HttpCalloutMock  {
        global HTTPResponse respond(HTTPRequest req) {
            //System.assertEquals('https://ebctest.cybersource.com/ebctest/Query', req.getEndpoint());
            //System.assertEquals('POST', req.getMethod());
            HttpResponse res = new HttpResponse();
            //res.setHeader('Content-Type', 'application/json');
          
            res.setBody('Success');
            res.setStatusCode(200);
            return res;
        }
    }

    global class HTTPMockCyberSource implements HttpCalloutMock  {
        global HTTPResponse respond(HTTPRequest req) {
            //System.assertEquals('https://ebctest.cybersource.com/ebctest/Query', req.getEndpoint());
            //System.assertEquals('POST', req.getMethod());
            HttpResponse res = new HttpResponse();
            //res.setHeader('Content-Type', 'application/json');
            String body = getBody();
            res.setBody(body);
            res.setStatusCode(200);
            return res;
        }

        String getBody() {
            String body = '<Report xmlns="https://ebctest.cybersource.com/ebctest/reports/dtd/tdr_1_9.dtd" Name="Transaction Detail" Version="1.9" MerchantID="scea_esptest" ReportStartDate="2013-09-18 12:35:23.902-08:00" ReportEndDate="2013-09-18 12:35:23.902-08:00">' + 
            '<Requests>' + 
            '<Request MerchantReferenceNumber="OrderDev - 000300" RequestDate="2013-09-17T06:34:51-08:00" RequestID="3794248915050178147626" SubscriptionID="" Source="Secure Acceptance Web/Mobile" TransactionReferenceNumber="5067937032">' + 
            '<ShipTo>' + 
            '<FirstName> SABRINA</FirstName>' + 
            '<LastName> CONSUMER</LastName>' + 
            '<Address1> 123 B St</Address1>' + 
            '<City>San Mateo</City>' + 
            '<State>CA</State>' + 
            '<Zip>94402</Zip>' + 
            '<Email> martha@appirio.com</Email>' + 
            '<Country> US</Country>' + 
            '<Phone/>' + 
            '</ShipTo>' + 
            '<PaymentMethod>' + 
            '<Card>' + 
            '<AccountSuffix> 1111</AccountSuffix>' + 
            '<ExpirationMonth> 01</ExpirationMonth>' + 
            '<ExpirationYear> 2015</ExpirationYear>' + 
            '<CardType> Visa</CardType>' + 
            '</Card>' + 
            '</PaymentMethod>' + 
            '<LineItems>' + 
            '<LineItem Number="0">' + 
            '<FulfillmentType/>' + 
            '<Quantity> 1</Quantity>' + 
            '<UnitPrice> 59.99</UnitPrice>' + 
            '<TaxAmount> 0.00</TaxAmount>' + 
            '<ProductCode> default</ProductCode>' + 
            '</LineItem>' + 
            '</LineItems>' + 
            '<ApplicationReplies>' + 
            '<ApplicationReply Name="ics_auth">' + 
            '<RCode> 1</RCode>' + 
            '<RFlag> SOK</RFlag>' + 
            '<RMsg> Request was processed successfully.</RMsg>' + 
            '</ApplicationReply>' + 
            '<ApplicationReply Name="ics_bill">' + 
            '<RCode> 1</RCode>' + 
            '<RFlag> SOK</RFlag>' + 
            '<RMsg> Request was processed successfully.</RMsg>' + 
            '</ApplicationReply>' + 
            '</ApplicationReplies>' + 
            '<PaymentData>' + 
            '<PaymentRequestID> 3794248915050178147626</PaymentRequestID>' + 
            '<PaymentProcessor> smartfdc</PaymentProcessor>' + 
            '<Amount> 59.99</Amount>' + 
            '<CurrencyCode> USD</CurrencyCode>' + 
            '<TotalTaxAmount> 0.00</TotalTaxAmount>' + 
            '<AuthorizationCode> 123456</AuthorizationCode>' + 
            '<AVSResult>  YYY</AVSResult>' + 
            '<AVSResultMapped> Y</AVSResultMapped>' + 
            '<EventType> TRANSMITTED</EventType>' + 
            '<RequestedAmount> 59.99</RequestedAmount>' + 
            '<RequestedAmountCurrencyCode> USD</RequestedAmountCurrencyCode>' + 
            '</PaymentData>' + 
            '</Request>' + 
            '</Requests>' + 
            '</Report>' ;
            
            return body; 
        }
    }

    @isTest
    static void testSetWalkIn() {
      try {
          User usr = TestClassUtility.createUser('Svc Tech Agent');
            
          Case cs = TestClassUtility.createCase('New', 'TEst', false);
          cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service').getRecordTypeId();
          cs.Product__c = 'PS4';
          cs.Issue1__c = 'Button';
          
          System.runAs(usr) {
            insert cs;
          }
          
          System.assertEquals(true, [SELECT Walk_In_Flag__c, ID FROM Case WHERE ID =: cs.Id].Walk_In_Flag__c);
      }catch(Exception ex) {
        System.debug('Profile Does not exist---->' + ex.getMessage());
      }
    }
    
    @isTest
    static void testPopulateABSServiceCenter() {
        //code added by preetu
    
    String RecTypeId= [select Id from RecordType where (Name='Person Account') and (SobjectType='Account')].Id; 
      PSN_Account__C psnAcc1 = [select Id,consumer__r.AccountId,Name from PSN_Account__c where Name='xgamertomx'];
  
  
      Account acc = new Account();
      acc.LastName = 'test';
      acc.FirstName = 'ftest';
      acc.Phone = '12345';
      //acc.PSN_Account__c = psnAcc1.Id;
      acc.RecordTypeID=RecTypeId;
      insert acc;
      
     Contact repContact = [SELECT id,FirstName, LastName
     FROM Contact
     WHERE isPersonAccount = true AND AccountId = :acc.id];
    
    
    
    
    
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HTTPMockCyberSource());
        list<Address__c> addList = TestClassUtility.createAddress(1, false);
        addList.get(0).State__c = 'CA';
        insert addList;
       
        Location__c location = new Location__c(Name='Test Location');
        insert location;
       
        ASB_Mapping__c asbMapping = new ASB_Mapping__c(Name='Califonia' , State__c = 'CA' , Location_Id__c = location.id);
        insert asbMapping;
        list<Contact> cntList = TestClassUtility.createContact(2, false);
        list<Product__c> productList = TestClassUtility.createProduct(1, true);
        cntList[0].FirstName = 'Test Name';
        cntlist[0].Email = 'test@test.com';
        cntlist[0].Preferred_Language__c = 'Eng';
        cntlist[0].Bill_To__c = addList[0].Id;
        insert cntlist;
       
        PSN_Account__c psnAcc = new PSN_Account__c();
        //psnAcc.Consumer__c = cntList[0].Id;
        psnAcc.Consumer__c = repContact.id;
        psnAcc.MDM_Account_ID__c = '13456';
        psnAcc.Status__c = 'Inactive';
        psnAcc.Name = 'TestPsn1111';
        insert psnAcc;
       
        psnAcc.Status__c = 'Active';
       
        update psnAcc;
       
        Case cs = TestClassUtility.createCase('New', 'TEst', false);
        cs.ContactId = cntList[0].Id;
        cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
        cs.Sub_Area__c = 'NEw';
        cs.Status = 'New';
        cs.Ship_To__c = addList.get(0).id;
        insert cs;
        List<Case> resultCases = [select id, ASB_Service_Center__c , Ship_To__c from Case where id = : cs.id];
        System.assertEquals(resultCases.get(0).Ship_To__c , addList.get(0).id); 
        System.assertEquals(resultCases.get(0).ASB_Service_Center__c , location.id);              
        Test.stopTest();
    }
    
    static testMethod void serviceCaseFromHardwareCaseTest() {
        list<Case> caselistToInsert = new list<Case>();
        list<Case> caselistToUpdate = new list<Case>();
        list<Location__c> loclist = new list<Location__c>();
        
        //LM 11/07/2014: added for PYB deployment as this test class failed due to Phone number validation rule on Hardware/Networking cases
        list<Contact> cntList = TestClassUtility.createContact(2, true);
                       
        Product__c prod = TestClassUtility.createProduct(1, false)[0];
        prod.Sub_Type__c = 'Hardware';
        prod.Status__c = 'Active';
        insert prod;
        
        Asset__c ast = TestClassUtility.createAssets(1, false)[0];
        ast.Model_Number__c = prod.SKU__c;
        ast.Product__c = prod.Id;
        insert ast;
        
        Case hwCase = TestClassUtility.createCase('Open', '', false);
        //LM 11/07/2014 : Added consumer reference for Phone number validation
        hwCase.ContactId = cntList[0].Id;
        hwCase.RecordTypeId = GeneralUtiltyClass.RT_HARDWARE_NETWORKING_ID;
        hwCase.Product__c = GeneralUtiltyClass.PRODUCT_PERIPHERAL;
        hwCase.Asset__c = ast.Id;
        caselistToInsert.add(hwCase);
                
        Location__c loc = TestClassUtility.createLocation(false);
        loc.Location_ID__c = Label.Service_Location_LRC;
        loclist.add(loc);
        
        Location__c loc2 = TestClassUtility.createLocation(false);
        loc2.Location_ID__c = Label.Service_Location_SCEA;
        loclist.add(loc2);
        
        insert loclist;
        
        Case testCase = TestClassUtility.createCase('Open', '', false);
        testCase.RecordTypeId = GeneralUtiltyClass.RT_SERVICE_ID;
        caselistToInsert.add(testCase);
        
        Case testCase2 = TestClassUtility.createCase('Open', '', false);
        testCase2.RecordTypeId = GeneralUtiltyClass.RT_SERVICE_ID;
        caselistToInsert.add(testCase2);
        
        insert caselistToInsert;
        
        hwCase.Status = 'Service';
        caselistToUpdate.add(hwCase);
        
        testCase.ASB_Type__c = 'Green Label';       
        caselistToUpdate.add(testCase);
        
        testCase2.ASB_Type__c = 'Alleged Incidents';        
        caselistToUpdate.add(testCase2);
        
        update caselistToUpdate;
        
        Case serviceCase = [select Id from Case where Parent_Case__c = :hwCase.Id];
        System.assertNotEquals(serviceCase.Id, null, 'Service Case should be created for H/W Case.');
    }
    
    //AB - 06/26/2014 - Positive Test Condition for LATAM Email-to-Case
    static testMethod void latamEmailToCase(){
        Test.startTest();

        Case testCase1 = new Case();
        testCase1.Origin = 'Web';
        testCase1.Status = 'New';
        testCase1.Case_Type__c = 'LATAM WEB CASE';
        testCase1.RecordTypeID = GeneralUtiltyClass.RT_LATAM_WEB_CASE_ID;
        testCase1.Description = 'Test Description!';
        testCase1.SuppliedEmail = 'test@playstation.sony.com';
        testCase1.SuppliedName = '';
        testCase1.Web_First_Name__c = '';
        testCase1.Web_Last_Name__c = '';
        insert testCase1;
        
        Test.stopTest();
        
        Case insertedCase1 = [SELECT id FROM Case WHERE ID =: testCase1.Id];
        System.assertEquals(testCase1.id, insertedCase1.id, 'Failure : Record Not Inserted');
    }
    
    //AB - 06/26/2014 - Positive Test Condition for LATAM Web-to-Case
    static testMethod void latamWebToCase(){
        Test.startTest();
        
        Case testCase2 = new Case();
        testCase2.Origin = 'Web';
        testCase2.Status = 'New';
        testCase2.Case_Type__c = 'LATAM WEB CASE';
        testCase2.RecordTypeID = GeneralUtiltyClass.RT_LATAM_WEB_CASE_ID;
        testCase2.Description = 'Test Description!';
        testCase2.SuppliedEmail = 'test@playstation.sony.com';
        testCase2.SuppliedName = '';
        testCase2.Web_First_Name__c = 'Test';
        testCase2.Web_Last_Name__c = 'Monkey';
        testCase2.Web_Country__c = 'Mexico';
        testCase2.Product__c = 'PS4';
        testCase2.Sub_Area__c = 'How To/General Question';
        testCase2.Issue1__c = 'Peripheral';
        insert testCase2;
        
        Test.stopTest();
        
        Case insertedCase2 = [SELECT id FROM Case WHERE ID =: testCase2.Id];
        System.assertEquals(testCase2.id, insertedCase2.id, 'Failure : Record Not Inserted');
    }
    
    //LM 11/07/2014 - Positive Test conditions for Staging Claims records
    static testMethod void StagingClaimsforAssurantTest() {
        list<Case> caselistToInsert = new list<Case>();
        list<Case> caselistToUpdate = new list<Case>();
        list<Location__c> loclist = new list<Location__c>();
        list<Contact> cntList = TestClassUtility.createContact(2, true);
                       
        Product__c prod = TestClassUtility.createProduct(1, false)[0];
        prod.Sub_Type__c = 'Hardware';
        prod.Status__c = 'Active';
        prod.Product_Type__c = 'PS4Test';
        insert prod;
        
        Product__c ESPprod = TestClassUtility.createProduct(1, false)[0];
        ESPprod.Sub_Type__c = 'ESP';
        ESPprod.Status__c = 'Active';
        insert ESPprod;
        
        Asset__c ast = TestClassUtility.createAssets(1, false)[0];
        ast.Model_Number__c = prod.SKU__c;
        ast.Product__c = prod.Id;
        ast.Purchase_Date__c = Date.today();
        ast.PPP_Product__c = ESPprod.Id;
        ast.PPP_Status__c = 'Active';
        ast.PPP_Purchase_Date__c = Date.today();
        insert ast;
        
        Order__c ord = TestClassUtility.creatOrder(cntList[0].Id, 1, false)[0];
        ord.Asset__c = ast.Id;
        insert ord;
        
        Asset_ShippingCost_InvoiceAmount__c costMap = new Asset_ShippingCost_InvoiceAmount__c();
        costMap.Invoice_Amount__c = 20.00;
        costMap.Shipping_Cost__c = 6.00;
        costMap.Name = 'PS4Test';
        insert costMap;
        
        Case ServiceCase = TestClassUtility.createCase('Open', '', false);
        ServiceCase.ContactId = cntList[0].Id;
        ServiceCase.RecordTypeId = GeneralUtiltyClass.RT_SERVICE_ID;
        ServiceCase.Product__c = 'PS4';
        ServiceCase.Sub_Area__c = 'Test';
        ServiceCase.Asset__c = ast.Id;
        ServiceCase.Fee_Type__c = 'PlayStation Protection Plan';
        caselistToInsert.add(ServiceCase);
                
        Location__c loc = TestClassUtility.createLocation(false);
        loc.Location_ID__c = Label.Service_Location_LRC;
        loclist.add(loc);
        
        Location__c loc2 = TestClassUtility.createLocation(false);
        loc2.Location_ID__c = Label.Service_Location_SCEA;
        loclist.add(loc2);
        
    	Staging_PPP_Outbound__c createStagingPPPoutbound = new Staging_PPP_Outbound__c();
		createStagingPPPoutbound.Unit_Model_Number__c = 'SONY';
		createStagingPPPoutbound.Unit_Serial_Number__c = 'Test';
		createStagingPPPoutbound.Order_Record_Type__c = 'A';
		createStagingPPPoutbound.Order_Dealer__c = 'SPSN';
		createStagingPPPoutbound.PPP_Currency_Code__c = 'US';
		createStagingPPPoutbound.Processed_Date_Time__c = null;
		createStagingPPPoutbound.Order_Number__c = ord.External_Order_Number__c;
		createStagingPPPoutbound.Order__c = ord.Id;
		insert createStagingPPPoutbound;
        
     
        insert loclist; 
        
        Test.startTest();  
        insert caselistToInsert;
        
        ServiceCase.Status = 'Ship Complete';
        caselistToUpdate.add(ServiceCase);

        update caselistToUpdate;
        Test.stopTest();
    } 
    
    //LM 11/07/2014 - Positive Test conditions for Staging Claims records - Outbound Staging records not exists in Assurant Outbound Object
    static testMethod void StagingClaimsforAssurantTest2() {
        list<Case> caselistToInsert = new list<Case>();
        list<Case> caselistToUpdate = new list<Case>();
        list<Location__c> loclist = new list<Location__c>();
        list<Contact> cntList = TestClassUtility.createContact(2, true);
                       
        Product__c prod = TestClassUtility.createProduct(1, false)[0];
        prod.Sub_Type__c = 'Hardware';
        prod.Status__c = 'Active';
        prod.Product_Type__c = 'PS4Test';
        insert prod;
        
        Product__c ESPprod = TestClassUtility.createProduct(1, false)[0];
        ESPprod.Sub_Type__c = 'ESP';
        ESPprod.Status__c = 'Active';
        insert ESPprod;
        
        Asset__c ast = TestClassUtility.createAssets(1, false)[0];
        ast.Model_Number__c = prod.SKU__c;
        ast.Product__c = prod.Id;
        ast.Purchase_Date__c = Date.today();
        ast.PPP_Product__c = ESPprod.Id;
        ast.PPP_Status__c = 'Active';
        ast.PPP_Purchase_Date__c = Date.today();
        insert ast;
        
        Order__c ord = TestClassUtility.creatOrder(cntList[0].Id, 1, false)[0];
        ord.Asset__c = ast.Id;
        insert ord;
        
        Asset_ShippingCost_InvoiceAmount__c costMap = new Asset_ShippingCost_InvoiceAmount__c();
        costMap.Invoice_Amount__c = 20.00;
        costMap.Shipping_Cost__c = 6.00;
        costMap.Name = 'PS4Test';
        insert costMap;
        
        Case ServiceCase = TestClassUtility.createCase('Open', '', false);
        ServiceCase.ContactId = cntList[0].Id;
        ServiceCase.RecordTypeId = GeneralUtiltyClass.RT_SERVICE_ID;
        ServiceCase.Product__c = 'PS4';
        ServiceCase.Sub_Area__c = 'Test';
        ServiceCase.Asset__c = ast.Id;
        ServiceCase.Fee_Type__c = 'PlayStation Protection Plan';
        caselistToInsert.add(ServiceCase);
                
        Location__c loc = TestClassUtility.createLocation(false);
        loc.Location_ID__c = Label.Service_Location_LRC;
        loclist.add(loc);
        
        Location__c loc2 = TestClassUtility.createLocation(false);
        loc2.Location_ID__c = Label.Service_Location_SCEA;
        loclist.add(loc2);

        insert loclist;   
        
        Test.startTest();
        insert caselistToInsert;
        
        ServiceCase.Status = 'Ship Complete';
        caselistToUpdate.add(ServiceCase);

        update caselistToUpdate;
        Test.stopTest();
    } 
}