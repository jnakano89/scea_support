/***********************************************************************************************************
Created : 09/08/2014 : Leena Mandadapu : Jira# SMS-654 - WebPPP Revamp project


************************************************************************************************************/
public class PYBCancellationResponse {
	
    public Boolean ErrStatus {get;set;}
    public String ErrorCode {get;set;}
    public String ErrorMsg {get;set;}
    public String PPPCancelDate {get;set;}
    public decimal RefundAmount {get;set;}
}