public with sharing class StagingAddressManagement {


    public static void beforeInsert(List<Staging_Address__c> addressList){
    } 

    public static void beforeUpdate(List<Staging_Address__c> addressList, Map<Id, Staging_Address__c> oldAddressMap){
	}   

    public static void afterInsert(List<Staging_Address__c> addressList){
    	
    	OSB__c osbSettings = OSB__c.getOrgDefaults();
    	
    	if (osbSettings.Create_Address_Enabled__c){
			callWebService(addressList);
    	}
    } 

    public static void afterUpdate(List<Staging_Address__c> addressList, Map<Id, Staging_Address__c> oldAddressMap){
	}   
 
    public static void callWebService(List<Staging_Address__c> addressList){
    	set<Id> stAddIdSet = new set<Id>();
    	for(Staging_Address__c sAdd : addressList){
    		stAddIdSet.add(sAdd.id);
    	}
    	if(!stAddIdSet.isEmpty()){
    		insertAddressList(stAddIdSet);
    	}
    }
    
    @future(callout=true)
	public static void insertAddressList(set<Id> stAddIdSet){
		 ConsumerSearchResult conResult = new ConsumerSearchResult();
		 Sony_MiddlewareUpdateInsertAddress2.ListOfContact  sonyAddressType = new  Sony_MiddlewareUpdateInsertAddress2.ListOfContact();
		 list<Sony_MiddlewareUpdateInsertAddress2.Contact> contactMDMList = new list<Sony_MiddlewareUpdateInsertAddress2.Contact>();
		 list<Staging_Address__c> stagingAddressList = new list<Staging_Address__c>();
		 map<Id, String> addressIdMdmIdMap = new map<Id, String>();
		 for(Staging_Address__c sAdd : [select Address_1__c, Address_2__c, Address_3__c, City__c, Country_Code__c, Errors__c,MDM_Address_ID__c,
										Province_Code__c, Zip_Code__c, MDM_Contact_ID__c, Processed_Date_Time__c, Address__c, Address_Type__c 
										from Staging_Address__c
										where Id IN :stAddIdSet]) {
			stagingAddressList.add(sAdd);
		 } 
		 
		 for(Staging_Address__c add :stagingAddressList){
		 		list<Sony_MiddlewareUpdateInsertAddress2.Address> addressList 	   = new  list<Sony_MiddlewareUpdateInsertAddress2.Address>();
		 		
		 		Sony_MiddlewareUpdateInsertAddress2.Address addMdm = new Sony_MiddlewareUpdateInsertAddress2.Address();
		 		
		 		addMdm.AddressId = add.MDM_Address_ID__c;
		 		addMdm.City = add.City__c;
		 		addMdm.Country = add.Country_Code__c;
		 		addMdm.PostalCode = add.Zip_Code__c;
		 		addMdm.State = add.Province_Code__c;
		 		addMdm.StreetAddress = add.Address_1__c;
		 		addMdm.StreetAddress2 = add.Address_2__c;
		 		addMdm.StreetAddress3 = add.Address_3__c;
		 		addMdm.ContactAddressType = add.Address_Type__c;
		 		addressList.add(addMdm);
		 		
		 		Sony_MiddlewareUpdateInsertAddress2.ListOfAddress  ListOfAddress_mdm = new Sony_MiddlewareUpdateInsertAddress2.ListOfAddress();
		 		ListOfAddress_mdm.Address = addressList;
		 		
		 		Sony_MiddlewareUpdateInsertAddress2.Contact conMdm = new Sony_MiddlewareUpdateInsertAddress2.Contact();
		 		conMdm.MDMRowId = add.MDM_Contact_ID__c;
		 		conMdm.ListOfAddress = ListOfAddress_mdm;
		 		
		 		contactMDMList.add(conMdm);
		 		
		 }
		  system.debug('contactMDMList>>>>>> '+contactMDMList); 
		 if(!contactMDMList.isEmpty()){
		 	Sony_MiddlewareUpdateInsertAddress2.ListOfContact listOfContact_mdm = new Sony_MiddlewareUpdateInsertAddress2.ListOfContact();
		 	listOfContact_mdm.Contact = contactMDMList;
		 	conResult = ConsumerServiceUtility.createAddress(listOfContact_mdm);
		 }
		system.debug('conResult>>>>>> '+conResult);
		//list<Staging_Address__c> stgAddresstList = [select id,Processed_Date_Time__c,Errors__c from Staging_Address__c where id in:stAddIdSet];
		for(Staging_Address__c sAdd :stagingAddressList){
			if(conResult.ErrorMessage != null){
				sAdd.Errors__c = conResult.ErrorMessage;
			}
			else{
				sAdd.Processed_Date_Time__c = Datetime.now();
				sAdd.MDM_Address_ID__c = getAddressId(conResult);
				addressIdMdmIdMap.put(sAdd.Address__c, sAdd.MDM_Address_ID__c);
			}
    	}
    	if(!stagingAddressList.isEmpty()){
    		update stagingAddressList;
    	}
    	
    	list<Address__c> updatedAddressList = getUpdatedAddressList(addressIdMdmIdMap);
    	if(!updatedAddressList.isEmpty()){
    		update updatedAddressList;
    	}
	}
	
	public static list<Address__c> getUpdatedAddressList(map<Id, String> addressIdMdmIdMap) {
	  list<Address__c> updatedAddressList = new list<Address__c>();
	  for(Address__c add : [select MDM_ID__c from Address__c where Id IN :addressIdMdmIdMap.keySet()]) {
	  	add.MDM_ID__c = addressIdMdmIdMap.get(add.Id);
	  	updatedAddressList.add(add);
	  }
	  return updatedAddressList;	
	}
	
	private static String getAddressId(ConsumerSearchResult conResult) {
		if(conResult.listOfContactForUpdateInsertAddress <> null) {
			for(Sony_MiddlewareUpdateInsertAddress2.Contact result : conResult.listOfContactForUpdateInsertAddress) {
			  if(result.ListOfAddress <> null && result.ListOfAddress.Address <> null) {
			    for(Sony_MiddlewareUpdateInsertAddress2.Address address : result.ListOfAddress.Address) {
			  	  return address.AddressId;
			    }
			  }	
			}
		}
		return null;
	}
}