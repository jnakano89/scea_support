/******************************************************************************
Class         : AssurantInboundProcessBatchable
Description   : This class will retrieve a list of Staging_Siebel_PPP_Inbound__c (inbound) daily at 5:30 PM PST to pickup unprocessed records from 
    Staging_PPP_Inbound__c object and update the relevant SFDC base objects based on the mapping as mentioned in the folowing worksheet and the 
    corresponding logic and data validation below:[T-166694]
Developed by  : Ranjeet Singh (JDC)
Date          : Jan 06, 2014
 ******************************************************************************/
global class AssurantInboundProcessBatchable implements Database.Batchable<Sobject>, AssurantInboundProcessScheduler.ISchedule{
    public static string ORDER_STATUS_ASSURANT_ENROLLED = 'Assurant Enrolled';
    public static string ORDER_STATUS_ASSURANT_CANCELLED = 'Assurant Cancelled';
    public static string CASE_STATUS_REFUND_COMPLETE = 'Refund Complete';
    
    public static string claimQueryTest = '';
    public static string claimQuery = 'Select s.Zip_Code_of_Sale__c, s.Vendor_Number_Name__c, s.Unit_Warranty_Duration__c, s.Unit_Serial_Number__c, s.Unit_Price__c, '+
            's.Unit_OEM_Warranty__c, s.Unit_Model_Number__c, s.SystemModstamp, s.State_of_Sale__c, s.Shipping_Cost__c, s.Purchase_Date__c, s.Proof_of_Purchase_Date__c, '+
            's.Processed_Date_Time__c, s.PPP_Tax__c, s.PPP_Start_Date__c, s.PPP_Service_Level__c, s.PPP_SKU__c, s.PPP_Refund_to_SCEA__c, s.PPP_Refund_by_Dealer__c, '+
            's.PPP_Purchase_Date__c, s.PPP_List_Price__c, s.PPP_Dealer_Cost__c, s.PPP_Currency_Code__c, s.PPP_Contract_Number__c, s.PPP_Claim_Reason__c, '+
            's.PPP_Cancel_Date__c, s.PPP_Cancel_After_30_Days__c, s.PPP_Assurant_End_Date__c, s.OwnerId, s.Order__c, s.Order_Record_Type__c, s.Order_Number__c, '+
            's.Order_Dealer__c, s.Name, s.Manufacture_Code__c, s.Loss_Date__c, s.LastModifiedDate, s.LastModifiedById, s.IsDeleted, s.Invoice_Number__c, s.Invoice_Date__c, '+
            's.Invoice_Amount__c, s.Invalid_Record__c, s.Id, s.Error_Message__c, s.CreatedDate, s.CreatedById, s.Country_of_Sale__c, s.Consumer_Phone_Number_2__c, '+
            's.Consumer_Phone_Number_1__c, s.Consumer_Last_Name__c, s.Consumer_First_Name__c, s.Consumer_Email_Address__c, s.Consumer_Area_Code_2__c, s.Consumer_Area_Code_1__c, '+
            's.Billing_Zip_Postal_Code__c, s.Billing_State__c, s.Billing_Country__c, s.Billing_City__c, s.Billing_Address_2__c, s.Billing_Address_1__c '+
            'From Staging_PPP_Inbound__c s where s.Processed_Date_Time__c=null AND (s.Order_Record_Type__c=\'A\' or( s.Order_Record_Type__c=\'C\')) ';

    global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = claimQuery;
		if(Test.isRunningTest()){
			query = claimQuery + claimQueryTest;
		}
		system.debug('*************Database Query : '+query+' >> '+Database.Query(query));
		return Database.getQueryLocator(query);
	}    

    global void execute(SchedulableContext SC){
        DataBase.executeBatch(this, 5);
    }

    //Members updated from helper methods.
    map<id, Asset__c> assetMap  = new map<id, Asset__c>();
    map<Id, Order__c> orderMap  = new map<Id, Order__c>();
    map<Id, Case> caseMap       = new map<id, Case>();
    map<Id, Payment__c> PaymentMap      = new map<id, Payment__c>();
    map<id, Order_Line__c>orderLineMap = new map<id, Order_Line__c>();

    //Do the business process.
    global void execute(Database.BatchableContext BC, List<sObject> batchList) {        
        try{
            //map<String, Staging_PPP_Inbound__c> assetInboundMap   = new map<String, Staging_PPP_Inbound__c>();
            map<String, Staging_PPP_Inbound__c> orderNumbers = new map<String, Staging_PPP_Inbound__c>();
            list<Staging_PPP_Inbound__c> inboundList = new list<Staging_PPP_Inbound__c>();
            set<Staging_PPP_Inbound__c> inboundInvalidList = new set<Staging_PPP_Inbound__c>();
            set<Staging_PPP_Inbound__c> inboundValidList = new set<Staging_PPP_Inbound__c>();

            //Validated data at staging records
            for( Staging_PPP_Inbound__c inboundPPP: (list<Staging_PPP_Inbound__c>)batchList){
                inboundPPP.Processed_Date_Time__c = DateTime.Now();
                String errorMsg = ValidateInboundStage(inboundPPP);
                if('Success'.equalsIgnoreCase(errorMsg)){
                	system.debug('**********errorMsg::'+errorMsg);
                    orderNumbers.put(inboundPPP.Order_Number__c, inboundPPP);
                }else{
                    setInboundPPPError(inboundInvalidList, inboundPPP, errorMsg);
                }
            }
            system.debug('*****:::'+orderNumbers.keySet() + '>>>>'+inboundInvalidList);
            /*
                Check if the corresponding Order Number exists in SFDC. If the Order is not found in SFDC then do not process the records and log proper error message in an 
                Error tracking field.
             */
            if(orderNumbers.size()>0){
                Set<String>orderNumberFoundAtSfdc = new Set<String>();
                for(Order__c order :[select Id,Name, Case__c, Case__r.Refund_Amount__c, Refund_Amount__c, Order_Status__c, Order_total__c, External_Order_Number__c, Asset__c, 
                                     Payment__r.Payment_Status__c, Case__r.Status, Order_Type__c, Payment__c, Asset__r.Purchase_Date__c, Asset__r.PPP_Purchase_date__c,
                                     Asset__r.PPP_Status__c, Consumer__c, Consumer__r.Email, Asset__r.PPP_Last_Update_Date__c, Asset__r.ESP_Cancellation_Date__c,Refund_to_SCEA_Amount__c,ESP_Refunded_by_Dealer__c, 
                                     (Select  List_Price__c, Ship_Cost__c, Tax__c, Total__c From Order_Lines__r) 
                                     from Order__c where External_Order_Number__c in:orderNumbers.keySet()]){
                    if(orderNumbers.containsKey( order.External_Order_Number__c )){
                        //Process the staging data for SFDC Order.
                        try{
                            String errorMsg = BusinessLogicProcess(order, orderNumbers.get( order.External_Order_Number__c ));
                            if(!'Success'.equalsIgnoreCase(errorMsg)){
                            	system.debug('**********errorMsg::'+errorMsg);
                                setInboundPPPError(inboundInvalidList, orderNumbers.get(order.External_Order_Number__c), errorMsg);
                            }else{
                                //Add Order list for update at SFDC .
                                orderMap.put(order.id, order);
                            }
                        }catch(Exception ex){
                            setInboundPPPError(inboundInvalidList, orderNumbers.get(order.External_Order_Number__c), ex.getMessage());
                        }
                        orderNumberFoundAtSfdc.add(order.External_Order_Number__c );
                    }
                }
                //If Order not found for staging object, Mark invalid staging objects.
                for(String inboundPPPKey : orderNumbers.keySet()){
                    if(!orderNumberFoundAtSfdc.contains(inboundPPPKey)){
                        Staging_PPP_Inbound__c inboundPPP = orderNumbers.get(inboundPPPKey);
                        setInboundPPPError(inboundInvalidList, inboundPPP, 'Order is not found in SFDC.');
                        system.debug('*****Not Found:::'+inboundPPP);
                    }
                }
                system.debug('***********Asset Update::'+assetMap.values());
                if(!assetMap.isEmpty()) update assetMap.values();
                /*if(!caseMap.isEmpty()) {
                    update caseMap.values();
                    //Need to manully send the Callout and we can't call from trigger
                    List<Staging_Survey__c>Survey = [Select Id, Processed_Date_Time__c, Errors__c, Case__c from Staging_Survey__c where Case__c in: caseMap.values() ];
                    if(Survey.size()>0){
                        StagingSurveyManagement.sendSurvey(Survey);
                    }

                }*/
                if(!PaymentMap.IsEmpty()) update PaymentMap.values();
                if(!orderLineMap.isEmpty())update orderLineMap.values();

                Database.UpsertResult[] srList = Database.upsert(orderMap.values(), false);
                system.debug('***********srList::'+srList);
                // Iterate through each returned result
                Set<Order__c>processedOrders = new Set<Order__c>();
                for (Database.UpsertResult sr : srList) {               
                    Order__c orderObj = orderMap.get(sr.getId());
                    Staging_PPP_Inbound__c inboundPPP = orderNumbers.get(orderObj.External_Order_Number__c);
                    if (!sr.isSuccess()) {                  
                        String errorText = '';
                        for(Database.Error err : sr.getErrors()) {
                            errorText += err.getMessage()+'\n\n';
                        }
                        setInboundPPPError(inboundInvalidList, inboundPPP, errorText);
                    }else{
                        //set staging object for processed date, //For failed/succ status any fields at staging.
                        setInboundPPPSuccess(inboundValidList, inboundPPP);                 
                        //Create email list to be sentout with templates
                        processedOrders.add(orderObj);
                    }
                }
            }
            //After successfully processing all the unprocessed records, SFDC process will need to update the Processed Date / Time field in the staging object with the current server Date / Time to prevent reprocessing.
            inboundValidList.AddALL(inboundInvalidList);
            if(inboundValidList.size()>0){
                upsert new List<Staging_PPP_Inbound__c>(inboundValidList);
            }
        }catch(Exception ex) {
            system.debug('******Stack -->'+ex+' >>>>> ');
            ExceptionHandler.logException(ex);
            Apex_Log__c al = new Apex_Log__c();
            al.Message__c = ex.getMessage();

            al.Exception_Cause__c = BC.getJobId();
            al.Class_Name__c = 'Assurance Inbound Claim';
            insert al;

            System.debug('**********Error Occured while running batch*********' + ex.getMessage());           
        }
    }

    //Mark the Staging objects for the sucessfully processed records.
    void setInboundPPPSuccess(Set<Staging_PPP_Inbound__c> inboundValidList, Staging_PPP_Inbound__c inboundPPP){
        inboundPPP.Invalid_Record__c = false;
        inboundPPP.Error_Message__c = null;
        inboundPPP.Processed_Date_Time__c = DateTime.Now();
        inboundValidList.add(inboundPPP);
    }

    //Mark the Staging objects for the failed records.
    public static void setInboundPPPError(Set<Staging_PPP_Inbound__c> inboundInvalidList, Staging_PPP_Inbound__c inboundPPP, String errorMsg){
        inboundPPP.Invalid_Record__c = true;
        inboundPPP.Error_Message__c = errorMsg;
        inboundPPP.Processed_Date_Time__c = DateTime.Now();
        inboundInvalidList.add(inboundPPP);
    }


    Asset__c getAssetById(Id AssetId, Order__c order){
        if(AssetId!=null){
            Asset__c asset = assetMap.get(AssetId);
            if(asset==null){
                asset = new Asset__c(id= AssetId, Purchase_Date__c =order.Asset__r.Purchase_Date__c);
                assetMap.put(asset.id, asset);
            }
            return assetMap.get(AssetId);
        }
        return null;
    }
    /*
    Case getCaseById(Id CaseId){
        if(CaseId!=null){
            Case caseObj = caseMap.get(CaseId);
            if(caseObj==null){
                caseObj = new Case(id= CaseId);
                caseMap.put(caseObj.id, caseObj);
            }
            return caseMap.get(CaseId);
        }
        return  null;
    }*/

    Payment__c getPaymentById(Id PaymentId){
        if(PaymentId!=null){
            Payment__c PaymentObj = PaymentMap.get(PaymentId);
            if(PaymentObj==null){
                PaymentObj = new Payment__c(id= PaymentId);
                PaymentMap.put(PaymentObj.id, PaymentObj);
            }
            return PaymentMap.get(PaymentId);
        }
        return null;
    }

    /*Order_Line__c getorderLineById(Id orderLineId){
        if(orderLineId!=null){
            Order_Line__c orderLineObj = orderLineMap.get(orderLineId);
            if(orderLineObj==null){
                orderLineObj = new Order_Line__c(id= orderLineId);
                orderLineMap.put(orderLineObj.id, orderLineObj);
            }
            return orderLineMap.get(orderLineId);
        }
        return null;
    }*/


    //Perform the BusinessLogicProcess from Staging object to SFDC objects.
    String BusinessLogicProcess(Order__c order, Staging_PPP_Inbound__c inboundPPP){
        //For Enrollment Confirmation records (Record Type = ‘A’) :
        system.debug('**********************<<>>:'+order.id);
        if(inboundPPP.Order_Record_Type__c=='A'){
            //Update the PPP Status [PPP_Status__c] on the SCEA Asset object to ‘Active’.
            //Asset__c asset = new Asset__c(id=order.Asset__c);
            if(order.Asset__c!=null){
                Asset__c asset = getAssetById(order.Asset__c, order);
                if(asset!=null){
                	
                	Order.Order_Status__c='Assurant Enrolled';
                    asset.PPP_Status__c= 'Active';
                    asset.PPP_Contract_Number__c= inboundPPP.PPP_Contract_Number__c;

                    asset.PPP_Last_Update_Date__c = datetime.Now().date();
                    if(inboundPPP.PPP_Purchase_Date__c!=null){

                        asset.PPP_Purchase_Date__c= ValidationRulesUtility.formatDateMMDDYYY(inboundPPP.PPP_Purchase_Date__c);//'Cancelled';
                    } 

                }
            }else{
                return 'Unable to find Asset for Order';
            }
        }
        //For Cancellation Confirmation records (Record Type = ‘C’):
        if(inboundPPP.Order_Record_Type__c=='C'){
            String status = '';
            String objectMissing = '';

            //Full Refund (Cancel with in 30 days from PPP purchase):
            //if(String.isEmpty(inboundPPP.PPP_Cancel_After_30_Days__c)){               
           Order.Order_Status__c='Assurant Cancelled';
            if(order.Asset__c == null){
                objectMissing= 'Asset';
            }
            /*if(order.Case__c==null){
                    objectMissing = objectMissing + (!String.isEmpty(objectMissing)?', ':'')+' Case';
                } */
            if(order.Payment__c==null){
                objectMissing = objectMissing + (!String.isEmpty(objectMissing)?', ':'')+' Payment';
            }

            if(!String.isEmpty(objectMissing)){
                return '\r\n' +objectMissing+ ' needs to be associated with the Order for Cancellation.';                    
            }else{
            	Integer daysbetweenCancel = 0;
            	if(inboundPPP.PPP_Cancel_Date__c!=null && inboundPPP.PPP_Purchase_Date__c!=null){
            		
            		Date Date_PPPCancellation=ValidationRulesUtility.formatDateMMDDYYY(inboundPPP.PPP_Cancel_Date__c);
            		Date Date_PPP_PurcahseDate=ValidationRulesUtility.formatDateMMDDYYY(inboundPPP.PPP_Purchase_Date__c);
            		
            		daysbetweenCancel =Date_PPP_PurcahseDate.daysbetween(Date_PPPCancellation);
            		
            		System.debug('>>>>>>>>>>+daysbetweenCancel<<<<<<<<<<<< ' +daysbetweenCancel);
            	}
                //Full Refund (Cancel  ON or After 30 days from PPP Purchase):
            	if(daysbetweenCancel<30){                	
                   	setFullRefund(order, inboundPPP);
                }else{
                    //Partial Refund (Cancel  after 30 days from PPP Purchase):

                    //if(order.Order_Lines__r!=null && order.Order_Lines__r.size()>0){              
                    //PSN sale (Order Type [Order_Type__c]  = PSN):
    
                    //if('PSN'.equalsIgnoreCase(order.Order_Type__c)){
                    //  status = 'PSN Refund Ready';
                    //}
                    //CS agent sale (Order Type [Order_Type__c] = ESP):
                    //if( 'ESP'.equalsIgnoreCase(order.Order_Type__c )){
                    //  status = 'Refund Complete';
                    //}
                    status = 'Assurant Cancelled';
                    if(!String.IsEmpty(status)){
                        if(order.Asset__c == null){
                            objectMissing= 'Asset';
                        }
                        /*if(order.Case__c==null){
                            objectMissing = objectMissing + (!String.isEmpty(objectMissing)?', ':'')+' Case';
                        }*/
                        if(order.Payment__c==null){
                            objectMissing = objectMissing + (!String.isEmpty(objectMissing)?', ':'')+' Payment';
                        }
                        /*if(order.Order_Lines__r==null && order.Order_Lines__r.size()==0){
                            objectMissing = objectMissing + (!String.isEmpty(objectMissing)?', ':'')+' "Order Line"';
                        } */
                        if(!String.isEmpty(objectMissing)){
                            return '\r\n' +objectMissing+ ' needs to be associated with the Order for Partial-Cancelation.';                    
                        }
                        setPartialRefund(order, inboundPPP, status);
                    }
                }
            }
        }

        return 'Success';
    }

    global void finish(Database.BatchableContext BC) {
    }

    
    //Full Refund (Cancel with in 30 days from PPP purchase):
    void setFullRefund(Order__c order, Staging_PPP_Inbound__c inboundPPP){

        Order.Order_Status__c='Assurant Cancelled';
        Order.ESP_Cancel_After_30_Days__c=False;
        //Need to just set order.Refund_Amount__c = order.Order_total__c.
        //?order.Refund_Amount__c = order.Order_total__c;

        //Set (Refund case) status = Refund Complete
        /*if(order.Case__c!=null){
            Case caseObj = getCaseById(order.case__c);
            if(caseObj!=null){
                caseObj.Status = 'Refund Complete';
            } 
        } */
        setAsset(order, inboundPPP);

        if(order.Payment__c!=null){
            Payment__c PaymentObj = getPaymentById(order.Payment__c);
            if(PaymentObj!=null && (order.Order_Type__c=='PSN')){
                PaymentObj.Payment_Status__c = 'PSN Ready for Refund ';
            }

            if(PaymentObj!=null && (order.Order_Type__c=='ESP')){
                PaymentObj.Payment_Status__c = 'Ready for Refund ';
            }
        }

    }

    //Partial refund, for Canceled after 30days.
    void setPartialRefund(Order__c order, Staging_PPP_Inbound__c inboundPPP, String Order_Status){
        system.debug('***************order.Order_Lines__r>>'+order.Order_Lines__r);
        if(order.Order_Lines__r!=null && order.Order_Lines__r.size()>0){
            /*

            //Amount to Refund to SCEA, Staging_PPP_Inbound__c.PPP_Refund_to_SCEA__c
            order.Order_Lines__r[0].Refund_to_SCEA_Amount__c = Decimal.valueOf(inboundPPP.PPP_Refund_to_SCEA__c);

            // Update Order status to Refund Complete (from Awaiting Refund status)  -->For ESP
            //Update Order status  to PSN Refund Ready (from Awaiting Refund status). -->For PSN

             */
            order.Order_Status__c = Order_Status;  //?Confirm
            Order.ESP_Cancel_After_30_Days__c=True;
            setAsset(order, inboundPPP);
            /*if(order.Case__c!=null){
                Case caseObj = getCaseById(order.case__c);
                if(caseObj!=null){
                    //Set (Refund case) status = Refund Complete
                    caseObj.Status = 'Refund Complete';

                    //Set (Refund case) Refund Amount to PPP Refund by Dealer
                    if(inboundPPP.PPP_Refund_by_Dealer__c!=null){
                        caseObj.Refund_Amount__c = Decimal.valueOf(inboundPPP.PPP_Refund_by_Dealer__c);
                    }else{
                        caseObj.Refund_Amount__c=null;
                    }
                }
            } */
            //Set (Payment) status = PSN/Ready for Refund
            if(order.Payment__c!=null){
                Payment__c PaymentObj = getPaymentById(order.Payment__c);
                if(PaymentObj!=null && (order.Order_Type__c=='PSN')){
                    PaymentObj.Payment_Status__c = 'Refunded ';
                }

                if(PaymentObj!=null && (order.Order_Type__c=='ESP')){
                    PaymentObj.Payment_Status__c = 'Refunded ';
                }
            }
            //Set (Order) ESP_Refunded_by_Dealer__c to PPP Refund by Dealer OR
            if(inboundPPP.PPP_Refund_by_Dealer__c !=null){
                order.ESP_Refunded_by_Dealer__c  = Decimal.valueOf(inboundPPP.PPP_Refund_by_Dealer__c);
            }else{
                order.ESP_Refunded_by_Dealer__c  = null;
            }

            //Set (Order) Refund_to_SCEA_Amount__ to PPP_Refund_to_SCEA__c
            if(inboundPPP.PPP_Refund_to_SCEA__c !=null){
                order.Refund_to_SCEA_Amount__c = Decimal.valueOf(inboundPPP.PPP_Refund_to_SCEA__c);
            }else{
                order.Refund_to_SCEA_Amount__c = null;
            }


            //Set (Order Line) ESP_Refunded_by_Dealer__c to PPP Refund by Dealer OR
            //Amount to Refund to Consumer, Staging_PPP_Inbound__c.PPP_Refund_by_Dealer__c
            /*if(order.Order_Lines__r!=null && order.Order_Lines__r.size()>0){
                Order_Line__c orderLine = getorderLineById(order.Order_Lines__r[0].id);
                if(orderLine==null){
                    if(inboundPPP.PPP_Refund_by_Dealer__c!=null){
                        orderLine.ESP_Refunded_by_Dealer__c = Decimal.valueOf(inboundPPP.PPP_Refund_by_Dealer__c);
                    }else{
                        orderLine.ESP_Refunded_by_Dealer__c = null;
                    }
                }
            } */
        }
    }

    void setAsset(Order__c order, Staging_PPP_Inbound__c inboundPPP){
        //Set (asset) PPP Status = Cancelled
        system.debug('********asset.ESP_Cancellation_Date__c:'+inboundPPP.PPP_Cancel_Date__c);
        if(order.Asset__c!=null){
            Asset__c asset = getAssetById(order.Asset__c, order);
            if(asset!=null){
                asset.PPP_Status__c = 'Cancelled';
                //Set (asset) ESP_Cancellation_Date__c  to Assurant cancel date ?
                system.debug('********asset.ESP_Cancellation_Date__c:'+asset.ESP_Cancellation_Date__c+' <><> '+inboundPPP.PPP_Cancel_Date__c);
                if(inboundPPP.PPP_Cancel_Date__c!=null){
                    asset.ESP_Cancellation_Date__c = ValidationRulesUtility.formatDateMMDDYYY(inboundPPP.PPP_Cancel_Date__c);//'Cancelled';
                }
                //Set (asset) PPP Last Update Date = current date
                asset.PPP_Last_Update_Date__c = datetime.Now().date();

                //Updated For T-166694.
                system.debug('********:ASSET'+asset);
                asset.PPP_Contract_Number__c = inboundPPP.PPP_Contract_Number__c;
                asset.PPP_Last_Update_Date__c = DateTime.now().date();
                system.debug('********:asset.Purchase_Date__c'+asset.Purchase_Date__c);
                if(inboundPPP.Purchase_Date__c!=null){
                	//I-100915 - Plan End Date = Unit Purchase Date + OEM (12 Month) + Plan Duration (24 Month)
                    asset.PPP_End_Date__c = ValidationRulesUtility.formatDateMMDDYYY(inboundPPP.Purchase_Date__c).addMonths(36);
                    system.debug('********:PPP_End_Date__c'+asset.PPP_End_Date__c);
                }
            }
        }
    }

    //Validate the staging inbound fields.
    static String ValidateInboundStage(Staging_PPP_Inbound__c inboundPPP){
        String errMsg = '';
        String tmpMsg = '';
        try{
            //if(String.IsEmpty(inboundPPP.Order_Number__c)){
            //  errMsg = 'Invalid Empty Order Number.';         
            //}     
            //Staging_PPP_Inbound__c.Billing_State__c

            //Less than or equal to 75 characters, Not Null
            errMsg = buildErrorText(errMsg,
                    ValidationRulesUtility.validateCharctersOnlySerialMaxLength(true, inboundPPP.Order_Number__c, 'Unique Identifier [Order Number]', 75));

            //Equal to 1 character
            errMsg = buildErrorText(errMsg, 
                    ValidationRulesUtility.validateCharctersFixLength(false, inboundPPP.Order_Record_Type__c, 'Record Type', 1));

            if(!String.isEmpty(inboundPPP.Order_Record_Type__c)){
                if('C'.equalsIgnoreCase( inboundPPP.Order_Record_Type__c)){
                    //Equal to 8 numbers
                    errMsg = buildErrorText(errMsg,
                            ValidationRulesUtility.validateNumericFixLength(false, inboundPPP.PPP_Cancel_Date__c, 'ESP Cancellation Date', 8));

                    //Equal to 1 character
                    errMsg = buildErrorText(errMsg,
                            ValidationRulesUtility.validateCharctersFixLength(false, inboundPPP.PPP_Cancel_After_30_Days__c, 'ESP Cancell After 30 Days', 1));

					//Partial Refund (Cancel  after 30 days from PPP Purchase):
					if(inboundPPP.PPP_Cancel_Date__c!=null && inboundPPP.PPP_Purchase_Date__c!=null){
						Date Purchase_Date = ValidationRulesUtility.formatDateMMDDYYY(inboundPPP.PPP_Purchase_Date__c);
						Date Cancel_Date = ValidationRulesUtility.formatDateMMDDYYY(inboundPPP.PPP_Cancel_Date__c);
						if(Purchase_Date!=null && Cancel_Date!=null && Purchase_Date.daysbetween(Cancel_Date)>=30){							
								//Less than or equal to 9 characters
								errMsg = buildErrorText(errMsg,
										ValidationRulesUtility.validateNumericMaxLength(false, inboundPPP.PPP_Refund_by_Dealer__c, 'ESP Refund to Consumer', 9));
							
						}
					}                    
                }
            }
            /*
        //Equal to 4 characters
        errMsg = buildErrorText(errMsg, 
            ValidationRulesUtility.validateCharctersFixLength(false, inboundPPP.Order_Dealer__c, 'Dealer', 4));

        //Equal to 8 numbers, Not Null
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateNumericFixLength(true, inboundPPP.PPP_Purchase_Date__c, 'ESP Purchase Date',8));

        //Less than or equal to 10 characters
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateCharctersOnlyMaxLength(inboundPPP.PPP_SKU__c, 'ESP SKU', 10));

        //Less than or equal to 9 characters
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateCharctersOnlyMaxLength(false, inboundPPP.PPP_List_Price__c, 'ESP List Price', 9));

        //Less than or equal to 9 characters
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateCharctersOnlyMaxLength(false, inboundPPP.PPP_Dealer_Cost__c, 'ESP Dealer Cost', 9));

        //Less than or equal to 9 characters
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateCharctersOnlyMaxLength(false, inboundPPP.PPP_Tax__c, 'ESP Tax', 9));//Check should be Number?

        //Equal to 8 numbers, Not Null
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateNumericFixLength(true, inboundPPP.Proof_Of_Purchase_Date__c, 'Unit Proof of Purchase Date', 8));

        //Equal to 'SONY'
        if(inboundPPP.Manufacture_Code__c==null || !'SONY'.equalsIgnoreCase(inboundPPP.Manufacture_Code__c)){
            errMsg = buildErrorText(errMsg,
                '\'Unit Manufacture Code\' must be \'SONY\'');
        }

        //Less than or equal to 20 characters, Not Null
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateCharctersOnlyMaxLength(true, inboundPPP.Unit_Model_Number__c, 'Unit Model Number', 20));

        //Less than or equal to 7 characters, Not Null
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateCharctersOnlyMaxLength(true, inboundPPP.Unit_Price__c, 'Unit Price', 7));

        //Less than or equal to 12 characters, Not Null
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateCharctersOnlyMaxLength(true, inboundPPP.Unit_Serial_Number__c, 'Unit Serial Number', 12));

        //Less than or equal to 4 characters, Not Null
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateCharctersOnlyMaxLength(true, inboundPPP.Unit_OEM_Warranty__c, 'OEM Warranty Duration', 4));

        //Equal to 8 numbers, Not Null
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateNumericFixLength(true, inboundPPP.PPP_Start_Date__c, 'ESP Start Date', 8));
             */
            /*
        //Less than or equal to 15 characters, Not Null, Letters Only
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateLettersOnlyMaxLength(true, inboundPPP.Consumer_Last_Name__c, 'Consumer Last Name', 15));

        //Less than or equal to 15 characters, Not Null, Letters Only
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateLettersOnlyMaxLength(true, inboundPPP.Consumer_First_Name__c, 'Consumer First Name', 15));

        //Less than or equal to 30 characters, Not Null
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateCharctersOnlyMaxLength(true, inboundPPP.Billing_Address_1__c, 'Billing Address 1', 30));

        //Less than or equal to 30 characters, Not Null
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateCharctersOnlyMaxLength(true, inboundPPP.Billing_Address_2__c, 'Billing Address 2', 30));

        //Less than or equal to 20 characters, Not Null, Letters Only
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateLettersOnlyMaxLength(true, inboundPPP.Billing_City__c, 'Billing City', 20));

        //Equal to 2 characters, Letters Only, Not Null
        errMsg = buildErrorText(errMsg,
                ValidationRulesUtility.validateLettersLength(true, inboundPPP.Billing_State__c, 'State of Sale[Billing State]', 2));

        //Less than or equal to 6 characters, Not Null, Space Removed
        String billing_Zip = inboundPPP.Billing_Zip_Postal_Code__c;
        if(inboundPPP.Billing_Zip_Postal_Code__c != null && inboundPPP.Billing_Zip_Postal_Code__c.trim() != ''){
            billing_Zip = billing_Zip.replaceAll(' ', '');
        }
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateCharctersOnlyMaxLength(true, billing_Zip, 'Zip Code of Sale[Billing Zip Code]', 6));

        //Equal to 2 characters, Letters Only, Not Null
        errMsg = buildErrorText(errMsg,
            ValidationRulesUtility.validateLettersLength(false, inboundPPP.Billing_Country__c, 'Country of Sale[Billing Country]', 2));
             */

            /*
        //Less than or equal to 9 characters
        errMsg = buildErrorText(errMsg,
                ValidationRulesUtility.validateCharctersOnlyMaxLength(false, inboundPPP.PPP_Refund_to_SCEA__c, 'ESP Refund to SCEA', 9));

        //Less than or equal to 9 characters
        errMsg = buildErrorText(errMsg,
                ValidationRulesUtility.validateCharctersOnlyMaxLength(false, inboundPPP.PPP_Contract_Number__c, 'ESP Contract Number', 9));

        //Equal to 8 numbers
        errMsg = buildErrorText(errMsg,
                ValidationRulesUtility.validateNumericFixLength(false, inboundPPP.PPP_Assurant_End_Date__c, 'ESP Expire Date', 8));
             */
            /*
        //Equal to 3 numbers, Not Null
        errMsg = buildErrorText(errMsg,
                ValidationRulesUtility.validateNumericFixLength(true, inboundPPP.Consumer_Area_Code_1__c, 'Consumer Area Code 1', 3));

        //Equal to 7 numbers, Not Null
        errMsg = buildErrorText(errMsg,
                ValidationRulesUtility.validateNumericFixLength(true, inboundPPP.Consumer_Phone_Number_1__c, 'Consumer Phone Number 1', 7));

        //Equal to 3 numbers
        errMsg = buildErrorText(errMsg,
                ValidationRulesUtility.validateNumericFixLength(false, inboundPPP.Consumer_Area_Code_2__c, 'Consumer Area Code 2', 3));

        //Equal to 7 numbers
        errMsg = buildErrorText(errMsg,
                ValidationRulesUtility.validateNumericFixLength(false, inboundPPP.Consumer_Phone_Number_2__c, 'Consumer Phone Number 2', 7));

        //Less than or equal to 40 characters
        errMsg = buildErrorText(errMsg,
                ValidationRulesUtility.validateCharctersOnlyMaxLength(false, inboundPPP.Consumer_Email_Address__c, 'Consumer Email Address', 40));
             */
        }catch(Exception Ex){
            system.debug('*************Error Validation:'+Ex);
            errMsg = Ex.getMessage();
        }
        if(String.IsEmpty(errMsg)){
            errMsg = 'Success';
        }
        system.debug('****************>>::'+errMsg);
        return errMsg;
    }

    //Build the validation error text.
    static String buildErrorText(String errBuilder, String currentErr){
        if(!'Success'.equalsIgnoreCase(currentErr)){
            errBuilder = errBuilder+ ' \r\n ' +currentErr;
        }
        return errBuilder;
    }

}