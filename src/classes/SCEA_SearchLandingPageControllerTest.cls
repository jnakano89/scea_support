/*
    @Author: Venkat Reddy
    @Version: 1.0
    @Description: Test class for SCEA_SearchLandingPageController
*/
@isTest
public class SCEA_SearchLandingPageControllerTest{
    private static testMethod void constructorTestMethod(){
        Test.setCurrentPage(Page.SCEA_SearchResults);
        ApexPages.currentPage().getParameters().put('lang','en_US');
        SCEA_SearchLandingPageController con = new SCEA_SearchLandingPageController();
        system.assert(con.selectedLanguage != null);
    }
    
    private static testMethod void searchForArticlesTestMethod(){
        Test.setCurrentPage(Page.SCEA_SearchResults);
        SCEA_SearchLandingPageController con = new SCEA_SearchLandingPageController();
        con.searchedString = 'Test';
        PageReference retPageRef = con.searchForArticles();
        system.assert(retPageRef != null);
    }
    
    private static testMethod void fetchCategoriesForProductTestMethod(){
        Test.setCurrentPage(Page.SCEA_SearchResults);
        SCEA_SearchLandingPageController con = new SCEA_SearchLandingPageController();
        con.fetchCategoriesForProduct();
        system.assertEquals(con.isProdutSelectedFlag,TRUE);
    }
    
    private static testMethod void fetchCategoriesForAccountTestMethod(){
        Test.setCurrentPage(Page.SCEA_SearchResults);
        SCEA_SearchLandingPageController con = new SCEA_SearchLandingPageController();
        con.fetchCategoriesForAccount();
        system.assertEquals(con.isAccountSelectedFlag,TRUE);
    }
    
    private static testMethod void fetchCategoriesForAllTopicsTestMethod(){
        Test.setCurrentPage(Page.SCEA_SearchResults);
        SCEA_SearchLandingPageController con = new SCEA_SearchLandingPageController();
        con.fetchCategoriesForAllTopics();
        system.assertEquals(con.isAllTopicsSelectedFlag,TRUE);
    }
    
    private static testMethod void fetchArticlesBasedOnSubCategoryTestMethodForSystem(){
        Test.setCurrentPage(Page.SCEA_SearchResults);
        SCEA_SearchLandingPageController con = new SCEA_SearchLandingPageController();
        con.selectedProduct = 'PlayStation_4__c';
        con.selectedProductCategory = 'PlayStation_Store__c';
        con.fetchArticlesBasedOnSubCategory();
        system.assertEquals(con.showArticlesFlag,TRUE);
        Pagereference retPageRef = con.showMoreArticles();
        system.assert(retPageref != null);
    }
    
    private static testMethod void fetchArticlesBasedOnSubCategoryTestMethodForAccount(){
        Test.setCurrentPage(Page.SCEA_SearchResults);
        SCEA_SearchLandingPageController con = new SCEA_SearchLandingPageController();
        con.selectedProduct = null;
        con.selectedAccountCategory = 'PlayStation_Store__c';
        con.fetchArticlesBasedOnSubCategory();
        system.assertEquals(con.showArticlesFlag,TRUE);
        Pagereference retPageRef = con.showMoreArticles();
        system.assert(retPageref != null);
    }
    
    private static testMethod void fetchArticlesBasedOnSubCategoryTestMethodForAllTopics(){
        Test.setCurrentPage(Page.SCEA_SearchResults);
        SCEA_SearchLandingPageController con = new SCEA_SearchLandingPageController();
        con.selectedProduct = null;
        con.selectedAccountCategory = null;
        con.selectedAllTopicCategory = 'PlayStation_Store__c';
        con.fetchArticlesBasedOnSubCategory();
        system.assertEquals(con.showArticlesFlag,TRUE);
        Pagereference retPageRef = con.showMoreArticles();
        system.assert(retPageref != null);
    }
    
    private static testMethod void initializeValuesFormySystemTestMethod(){
        SCEA_SearchLandingPageController con = new SCEA_SearchLandingPageController();
        con.initializeValuesFormySystem();
        system.assertEquals(con.isProdutSelectedFlag,FALSE);
        system.assertEquals(con.isAccountSelectedFlag,FALSE);
        system.assertEquals(con.isAllTopicsSelectedFlag,FALSE);
    }
    
    private static testMethod void fetchFeaturedArticlesTestMethod(){
        KC_Article__kav kcArticle = new KC_Article__kav();
        kcArticle.language = 'en_US';
        kcArticle.Title = 'Test KC Article';
        kcArticle.UrlName = 'Test-KC-Article';
        insert kcArticle;
        
        kcArticle = [SELECT KnowledgeArticleId, Language, PublishStatus, ArticleNumber FROM KC_Article__kav WHERE Id = :kcArticle.Id];
        system.debug(kcArticle.Language+'---'+kcArticle.PublishStatus);
        Test.startTest();
        KbManagement.PublishingService.publishArticle(kcArticle.KnowledgeArticleId, true);
        
        kcArticle = [SELECT KnowledgeArticleId, Language, PublishStatus, ArticleNumber FROM KC_Article__kav WHERE Id = :kcArticle.Id];
        system.debug(kcArticle.Language+'---'+kcArticle.PublishStatus);
        Feature_Article__c featureArticle = new Feature_Article__c();
        featureArticle.Name = 'Test Feature Article';
        featureArticle.Article_Id__c = kcArticle.ArticleNumber;
        featureArticle.Image_URL__c = 'TestImageURL';
        insert featureArticle;
        SCEA_SearchLandingPageController con = new SCEA_SearchLandingPageController();
        con.fetchFeaturedArticles();
        system.assert(con.featureArticleResult != null);
    }
    
    private static testMethod void searchForErrorCodeTestMethod(){
        Error_Code_PKB__c errorCode = new Error_Code_PKB__c();
        errorCode.Error_Code__c = 'TESTERRCODE';
        errorCode.Description__c = 'Test Description';
        errorCode.Action__c = 'Test Action';
        errorCode.Language__c = 'en_US';
        insert errorCode;
        SCEA_SearchLandingPageController con = new SCEA_SearchLandingPageController();
        con.errorCodeString = 'TESTERRCODE';
        con.searchForErrorCode();
        system.assert(con.errorCodeCS != null);
    }
    
    private static testMethod void searchForErrorCodeTestMethodForAllTopics(){
        Error_Code_PKB__c errorCode = new Error_Code_PKB__c();
        errorCode.Error_Code__c = 'TESTERRCODE';
        errorCode.Description__c = 'Test Description';
        errorCode.Action__c = 'Test Action';
        errorCode.Language__c = 'en_US';
        insert errorCode;
        SCEA_SearchLandingPageController con = new SCEA_SearchLandingPageController();
        con.errorCodeString = null;
        con.errorCodeAllTopicString = 'TESTERRCODE';
        con.searchForErrorCode();
        system.assert(con.errorCodeCS != null);
    }
}