/******************************************************************************
Class         : ConsumerServiceUtility
Description   : A webservice Class which calls different Consumer services.
Developed by  : Urminder Vohra(JDC)
Date          : August 27, 2013
Task          : T-176715                 
******************************************************************************/
public class ConsumerServiceUtility {

    public static String stripNonNumbers(String phone){
        
        String newPhone;
        
        newPhone=phone.replaceAll('[^\\d]', '');
        
        return newPhone;
        
        
    }

    
  /*This method will call Consumer Lookup service to get List of Accounts*/
  
  public static ConsumerSearchResult getConsumers(String phone, String email, String psnOnlineId) {

    System.debug('>>>>>>>>>ConsumerSearchResult<<<<<<<<<<<<');
    System.debug('>>>>>>>>>phone='+phone);
    System.debug('>>>>>>>>>email='+email);
    System.debug('>>>>>>>>>psnOnlineId='+psnOnlineId);
    

    String emailSearch;
    String psnOnlineIdSearch;
    String phoneSearch;
    

    if (email == null){
        emailSearch = null; 
    }else{
        emailSearch = email.toLowerCase();
    }
    
    if(psnOnlineId == null){
        psnOnlineIdSearch = null;
    }else{
        psnOnlineIdSearch = psnOnlineId.toLowerCase();
    }

    if(phone == null){
        phoneSearch = null;
    }else{
        phoneSearch = stripNonNumbers(phone);
    }
    

    Sony_MiddlewareConsumerdata3.PSConsumerLookup_Output_element response;
    ConsumerSearchResult searchResult = new ConsumerSearchResult();
    Sony_MiddlewareConsumerdata3.ConsumerLookupQSPort serviceReq = new Sony_MiddlewareConsumerdata3.ConsumerLookupQSPort();
    
   // try{
        
        System.debug('Invoking OSB PSConsumerLookup ---------- START');
        
        response = serviceReq.PSConsumerLookup(emailSearch, phoneSearch, null, psnOnlineIdSearch, Label.SFDC_SOURCE_ID, null, null, null);
    
        System.debug('Invoking OSB PSConsumerLookup ---------- END');
    
   // }catch(Exception e){
    // System.debug('eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee:'+e);
    //    IntegrationAlert.addException('Lookup Consumer', 'email='+emailSearch + ', phone='+phoneSearch + ', PSN Online ID='+psnOnlineIdSearch, e);    
    //}
    
    if(response <> null) {

      if(response.ErrorMessage <> null) {
        System.debug('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
        searchResult.ErrorMessage = response.ErrorMessage.ErrorMessage;
        searchResult.ErrorCode = response.ErrorMessage.ErrorCode;
        searchResult.ErrorDescription = response.ErrorMessage.Description;
        System.debug('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
        
        System.debug('>>>>ErrorMessage:'+searchResult.ErrorMessage);
        System.debug('>>>>ErrorDescription:'+searchResult.ErrorDescription);
        System.debug('>>>>ErrorCode:'+searchResult.ErrorCode);                  
        
      } else if(response.ListOfAccount <> null && response.ListOfAccount.Account <> null) {

        searchResult.ListOfAccounts = new list<Sony_MiddlewareConsumerdata3.Account_mdm>();

        for(Sony_MiddlewareConsumerdata3.Account_mdm acc : response.ListOfAccount.Account) {
            searchResult.ListOfAccounts.add(acc);
        }//end-for

      }

    }

    return searchResult;
  }
  
  /*This service will update Consumer with passed parameterts as list of contacts
    A contact will required email, phone and mdm contact Id.
  */
  
  public static ConsumerSearchResult updateConsumer(list<WrapperContacts> wrapCntList) {
    ConsumerSearchResult result = new ConsumerSearchResult();
    Sony_MiddlewareUpdateconsumer_Final.PSUpdateConsumer_Output_element response;
    Sony_MiddlewareUpdateconsumer_Final.UpdateConsumerQSPort serviceReq = new Sony_MiddlewareUpdateconsumer_Final.UpdateConsumerQSPort();
    list<Sony_MiddlewareUpdateconsumer_Final.Contact> cntList = new list<Sony_MiddlewareUpdateconsumer_Final.Contact>();
    system.debug('wrapCntList>>>>> '+wrapCntList);
    for(WrapperContacts cnt : wrapCntList) {
     system.debug('::cnt:::'+cnt);  
      Sony_MiddlewareUpdateconsumer_Final.Contact c = new Sony_MiddlewareUpdateconsumer_Final.Contact();
      c.MDMRowId = cnt.mdmContactId;
      c.RMPhone = cnt.phone;
      c.SiebelEmail = cnt.email;
      cntList.add(c);
    }
    Sony_MiddlewareUpdateconsumer_Final.ListOfContact ListOfContact = new Sony_MiddlewareUpdateconsumer_Final.ListOfContact();
    ListOfContact.Contact = cntList;

    try{
        response = serviceReq.PSUpdateConsumer(ListOfContact, Label.SFDC_SOURCE_ID, null, null, null);
    }catch (Exception e){
        IntegrationAlert.addException('Update Consumer', e);    
    }

    result.Status = response.Status;
    if(response.ErrorMessage <> null) {
      result.ErrorCode = response.ErrorMessage.ErrorCode;
      result.ErrorMessage = response.ErrorMessage.ErrorMessage;
      result.ErrorDescription = response.ErrorMessage.Description;  
    } 
    return result;
  }
  
  public class WrapperContacts {
    public String email;
    public String mdmContactId;
    public String phone;
    
    public WrapperContacts(String mdmContactId, String email, String phone) {
      this.mdmContactId = mdmContactId;
      this.email = email;
      this.phone = phone;   
    }
  }
  
  /*This method will call Refresh Consumer Service*/
  public static ConsumerSearchResult refreshConsumer(String mdmContactRowId) {
    return ConsumerServiceUtility.getConsumer(mdmContactRowId,null,null,null);
  }
  
  public static ConsumerSearchResult getConsumer(String mdmContactRowId,String Email,String PhoneNo,String Handle) {
    ConsumerSearchResult result = new ConsumerSearchResult(); 
    Sony_MiddlewareConsumerRefresh_Final.PSConsumerRefresh_Output_element response;
    Sony_MiddlewareConsumerRefresh_Final.ConsumerRefreshQSPort serviceReq = new Sony_MiddlewareConsumerRefresh_Final.ConsumerRefreshQSPort();
    
    //try{
    response = serviceReq.PSConsumerRefresh(MDMContactRowId, null, null, null, null, Label.SFDC_SOURCE_ID, null, null, null);
    //} catch (Exception e){
    //    IntegrationAlert.addException('Refresh Consumer', 'MDM Contact ID='+MDMContactRowId, e);
    //}
    
    if(response.ErrorMessage <> null) {
      result.ErrorCode = response.ErrorMessage.ErrorCode;
      result.ErrorMessage = response.ErrorMessage.ErrorMessage;
      result.ErrorDescription = response.ErrorMessage.Description;  
    } else if(response.ListOfContact <> null && response.ListOfContact.Contact <> null) {
        result.listOfContacts = new list<Sony_MiddlewareConsumerRefresh_Final.Contact_mdm>();
        for(Sony_MiddlewareConsumerRefresh_Final.Contact_mdm cnt : response.ListOfContact.Contact) {
          result.listOfContacts.add(cnt);   
        }
    }
    return result;
  }
  

  public static ConsumerSearchResult createAddress(Sony_MiddlewareUpdateInsertAddress2.ListOfContact listOfContact) {
        system.debug('listOfContact>>>> '+listOfContact);
    ConsumerSearchResult result = new ConsumerSearchResult();
    Sony_MiddlewareUpdateInsertAddress2.PSUpdateInsertAddress_Output_element response;
    Sony_MiddlewareUpdateInsertAddress2.UpdateInsertAddressQSPort serviceReq = new Sony_MiddlewareUpdateInsertAddress2.UpdateInsertAddressQSPort();
    
    try{
    response = serviceReq.PSUpdateInsertAddress(listOfContact, Label.SFDC_SOURCE_ID, null, null, null);
    }catch (Exception e){
        IntegrationAlert.addException('Create Address', e); 
    }
    
    if(response <> null && response.ErrorMessage <> null) {
      result.ErrorCode = response.ErrorMessage.ErrorCode;
      result.ErrorMessage = response.ErrorMessage.ErrorMessage;
      result.ErrorDescription = response.ErrorMessage.Description;  
    } else if(response <> null && response.ListOfContact <> null && response.ListOfContact.Contact <> null) {
        result.listOfContactForUpdateInsertAddress = new list<Sony_MiddlewareUpdateInsertAddress2.Contact>();
        for(Sony_MiddlewareUpdateInsertAddress2.Contact cnt : response.ListOfContact.Contact) {
          result.listOfContactForUpdateInsertAddress.add(cnt);  
        }
    }
    return result; 
  }
  
 
  
  public static ConsumerSearchResult updateAsset(Sony_MiddlewareUpdateAccountAsset.ListOfAccount_mdm listOfAccount) {
    ConsumerSearchResult result = new ConsumerSearchResult();
    Sony_MiddlewareUpdateAccountAsset.PSUpdateAcntAsset_Output_element response;
    Sony_MiddlewareUpdateAccountAsset.UpdateAcntAssetQSPort serviceReq = new Sony_MiddlewareUpdateAccountAsset.UpdateAcntAssetQSPort();
    
    try{
        response = serviceReq.PSUpdateAcntAsset(listOfAccount, Label.SFDC_SOURCE_ID, null, null, null);
    }catch (Exception e){
        IntegrationAlert.addException('Update Asset', e);   
    }
    
    if(response.ErrorMessage <> null) {
      result.ErrorCode = response.ErrorMessage.ErrorCode;
      result.ErrorMessage = response.ErrorMessage.ErrorMessage;
      result.ErrorDescription = response.ErrorMessage.Description;  
    } else if(response.ListOfAccount.Account <> null) {
        result.listOfAccountForUpdateAccountAsset = new list<Sony_MiddlewareUpdateAccountAsset.Account_mdm>();
        for(Sony_MiddlewareUpdateAccountAsset.Account_mdm acc : response.ListOfAccount.Account) {
            result.listOfAccountForUpdateAccountAsset.add(acc);
        }
    }
    return result; 
  } 
}