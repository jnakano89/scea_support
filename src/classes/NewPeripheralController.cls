//**************************************************************************/
// (c) 2013 Appirio, Inc.
//
// Description : Controller Class For VF PAge NewPeripheral.
//                  
// Original Jan 29, 2014  : Naresh Kr Ojha (Appirio) Created for task T-242742
// ***************************************************************************/
public with sharing class NewPeripheralController {
  public list<Product__c> peripheralProductList {get;set;}

  public string caseId;

  public string title{get;set;}
  public string subTitle{get;set;}
  public String productType {get;set;}
  public Case selectedCase{get;set;}
  
    public NewPeripheralController () {
      caseId = Apexpages.currentPage().getParameters().get('caseId');
      populateCase (caseId);
      subTitle = 'Select '+ productType;
        populatePeripherals();
    }
    
   public void populatePeripherals () {
    peripheralProductList = new List<Product__c>();
        String query = 'SELECT ID, SKU__c, Product_Type__c, Name FROM Product__c ';
        String whereClause = '';
        String orderBy = ' Order By SKU__c';
    
    if(caseId != null && caseId != ''){
      whereClause = 'where ';
      
      for(Product_Filter_Mapping__c pfm : Product_Filter_Mapping__c.getAll().values()) {
        if(pfm.Name == productType) {
            whereClause += pfm.Filter_Criteria__c;
        }
      } 
     /*
      if(productTywhereClausepe == 'Peripheral') {  
         = 'Where Product_Type__c = \'1st Party Peripheral\' AND Status__c = \'Active\' ';
      }
      else if (productType == 'Promotion') {
        whereClause = 'Where Product_Type__c = \'1st Party Peripheral\' AND (Sub_Type__c = \'Hardware\' OR Sub_Type__c = \'Software\') AND Promo_Product__c = true  AND Status__c = \'Active\'';
      } 
      for(Product__c p : [SELECT ID, SKU__c, Product_Type__c, Name 
                                    FROM Product__c
                                    WHERE Product_Type__c = '1st Party Peripheral'
                                            AND Status__c = 'Active'
                                            order by SKU__c]){
            peripheralProductList.add(p);
       }*/
       for(sObject obj : Database.query(query+whereClause+orderBy)) {
         peripheralProductList.add((Product__c)obj);
       } 
    }
    }

  public Pagereference returnToRecord() {
    Pagereference pg = new ApexPages.StandardController(selectedCase).view();
    pg.setRedirect(true); 
    return pg;
  }

  private void populateCase(String caseId) {
    for(Case c : [select Product__c, Asset__c, caseNumber, ContactId
                    from Case
                    where Id = :caseId]) {
      title = 'Case #:'+c.CaseNumber;
      productType = c.Product__c;
      selectedCase = c;                 
    }
  }
  
  public Pagereference selectPeripheral () {
    Product__c currentProduct = new Product__c();
    String productId = ApexPages.currentPage().getParameters().get('ctRadio');
    
    Asset__c newAsset = new Asset__c();
    if(SelectedCase == null) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Valid case is must be used in parameter.'));
      return null;
    }
    if(productId == null || productId == '') {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select a product.'));
      return null;
    } else {
        for (Product__c p : [SELECT ID, SKU__c, Product_Type__c, Name , Description__c
                                             FROM Product__c
                                             WHERE ID =: productId LIMIT 1]) {
            currentProduct = p;
        }
    }
    
        newAsset.Model_Number__c = currentProduct.SKU__c;
        newAsset.Product__c =currentProduct.Id;
        newAsset.Coverage_Type__c = null;
        newAsset.Asset_Status__c = 'Active';
        newAsset.Brand_Name__c = currentProduct.Name;
        newAsset.Description__c = currentProduct.Description__c;
        if(selectedCase.Product__c == GeneralUtiltyClass.PRODUCT_PROMOTIONAL) {
            newAsset.Serial_Number__c = 'PRO-ASSETNUMBER';
        } else {
            newAsset.Serial_Number__c = 'PER-ASSETNUMBER';
        }

    
        newAsset.Purchase_Date__c = date.today();
        insert newAsset;
        // Added this junction as per chat with Dan
        Consumer_Asset__c ca = new Consumer_Asset__c();
        ca.Asset__c = newAsset.Id;
        ca.Consumer__c = selectedCase.ContactId;
        insert ca;
        
        selectedCase.SCEA_Product__c = currentProduct.ID;
        selectedCase.Asset__c = newAsset.ID;
        
        If (selectedCase!=null && '3D Display (Monitor)'.equalsIgnoreCase(selectedCase.Product__c)){
            selectedCase.Fee_Type__c = 'Out Of Warranty';
        }
        update selectedCase;
    
    return returnToRecord();
  }     
}