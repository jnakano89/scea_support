/*************************************************************************************************************************************************************************
Developed By : Appirio

Updates:
        04/16/2014 - Leena Mandadapu : JIRA# SF-383 - Updated to use the External Case Number (if available) in the Staging Service outbound record creation.
        05/28/2014 - Aaron Briggs : SF-966 - Definte Additional Static Record Type Variable - Case_Feedback
        03/04/2015 : Aaron Briggs : SMS-448 : Adjusted isPromoValid Method to allow Service Replacement Purchases
**************************************************************************************************************************************************************************/
global without sharing class GeneralUtiltyClass {
    public static final String MTCFUL = 'MTC FUL';
    public static final String MTCFWFUL = 'MTC-FW FUL';
    public static final String FIRST_PARTY_PERIPHERAL = '1st Party Peripheral';
    public static final String FIRST_PARTY_SOFTWARE = '1st Party Software';
    public static final String PEROMOTIONAL = 'Promotional';
    public static final String ASSET_MODEL_NUMBER = 'CECH-ZED1U';
    
    public static final String SHIP_COMPLETE = 'Ship Complete';
    public static final String SERVICE_READY_STATUS = 'Ready for Service';
    
    //Preetu made changes on 3/4/2015
    public static final String SERVICE_REPLACEMENT = 'Service Replacement';
    public static final String ASB_TYPE_NO_BOX_REQUIRED = 'No Box Required';
    
    public static final String REFUND_AMOUNT_ERR_MSG = Label.REFUND_AMOUNT_ERR_MSG;
    
    public static final String FEE_TYPE_PLAY_STATION_PROTECTION_PLAN = 'PlayStation Protection Plan';
    public static final String ASB_TYPE_GREEN_LABEL = 'Green Label';
    
    public static final String INVALID_PROFILE_MESSAGE = 'User not authorized for this action.';
    public static final String PRODUCT_PERIPHERAL = 'All Peripherals';
    public static final String PRODUCT_PROMOTIONAL = 'Promotion';
    public static final String FEE_TYPE_GOODWILL = 'GOODWILL';
    
    public static final Set<String> ALL_PERIPHERAL_VALUES  = new Set<String>{'All Peripherals', 'PS3 Peripheral','PS4 Peripheral', 'PS2 Peripheral', 'PSP / VITA Peripheral'};
    public static Set<String> workflowActionValues = new Set<String>{'ASB - Shipped', 'No Box Required'};
    public static Set<String> caseResolutions = new Set<String>{'Service','Display Service'};
    public static Set<String> caseStatusValues = new Set<String>{'Auth Complete', 'Open', 'Check Open'};
    public static Set<String> productTypes = new Set<String>{'PS3Slim', 'PS3', 'PSP', 'PSPGO','VITA','1st Party Peripheral', 'PS4'};
    
    public static final String RT_SERVICE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service').getRecordTypeId();
    public static final String RT_REFUND_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Refund').getRecordTypeId();
    public static final String RT_PURCHASE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
    public static final String RT_HARDWARE_NETWORKING_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Hardware/Networking').getRecordTypeId();
    public static final String RT_PS_NETWORK_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('PS Network').getRecordTypeId();
    public static final String RT_GENERAL_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('General').getRecordTypeId();
    public static final String RT_VOUCHER_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Voucher').getRecordTypeId();
    public static final String RT_GRIEF_REPORT_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Grief Report').getRecordTypeId();
    public static final String RT_LATAM_WEB_CASE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('LATAM Web Case').getRecordTypeId();
    // AB : 05/28/2014 : Define Record Type Case_Feedback
    public static final String RT_CASE_FEEDBACK_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Feedback').getRecordTypeId();
    
    public static Staging_Service_Outbound__c createStagingServiceOutbound(Case cs, Contact cnt, Product__c prod, Asset__c ast, Address__c shipTo, Location__c returnAddress){
      Staging_Service_Outbound__c sso = new Staging_Service_Outbound__c();
      if(cs <> null) {
        //LM 04/16/2014 : added External_Case_Number__c if condition    
        sso.Case_Number__c = cs.External_Case_Number__c <> null ? cs.External_Case_Number__c : cs.CaseNumber;
        sso.Case_Fee_Type__c = cs.Fee_Type__c;
      }
      if(cnt <> null) {      
          sso.Contact_First_Name__c = cnt.FirstName;
          sso.Contact_Last_Name__c = cnt.LastName;
          sso.Contact_Email__c = cnt.Email;
          sso.Contact_Phone__c = cnt.Phone;
      }
      if(shipTo <> null) {      
          sso.Ship_To_Address_Line_1__c = shipTo.Address_Line_1__c;
          sso.Ship_To_Address_Line_2__c = shipTo.Address_Line_2__c;
          sso.Ship_To_Address_City__c = shipTo.City__c;
          sso.Ship_To_Address_State__c = shipTo.State__c;
          sso.Ship_To_Address_Postal_Code__c = shipTo.Postal_Code__c;
          sso.Ship_To_Address_Country__c = shipTo.Country__c;
      }
      if(prod <> null) {        
          sso.Product_Type__c = prod.Product_Type__c;      
          sso.Product_SKU__c = prod.SKU__c;
          if(returnAddress == null){
            sso.Product_Description__c = prod.Description__c;
          }
      }
      if(ast <> null) {
        sso.Asset_Serial_Number__c = ast.Serial_Number__c;
      }
      if(returnAddress <> null){
        sso.Return_Location__c = returnAddress.Name;
        sso.Return_Address_Line_1__c = returnAddress.Address_Line_1__c;
        sso.Return_Address_Line_2__c = returnAddress.Address_Line_2__c;
        sso.Return_State__c = returnAddress.State__c;
        sso.Return_City__c = returnAddress.City__c;
        sso.Return_Postal_Code__c = returnAddress.Postal_Code__c;
      }
      return sso;
    }
    
    public static boolean isServiceOrderValid(Case newCase, Case oldCase){
      Boolean isValid = false;
      if(newCase.RecordTypeId == RT_SERVICE_ID && newCase.Status <> oldCase.Status && newCase.Status == SERVICE_READY_STATUS
            && newCase.Bill_To__c <> null && newCase.Ship_To__c <> null){
        isValid = true;
      }
      return isValid;
    }
    
    public static boolean isASBValid(Case newCase, Case oldCase){
      Boolean isValid = false;
      if((newCase.Status <> oldCase.Status || newCase.ASB_Type__c <> oldCase.ASB_Type__c) && newCase.RecordTypeId == RT_SERVICE_ID 
                && newCase.Status == SERVICE_READY_STATUS && newCase.ASB_Type__c <> ASB_TYPE_NO_BOX_REQUIRED
                && newCase.Bill_To__c <> null && newCase.Ship_To__c <> null  && newCase.Asset__r.product__r.sub_type__c == 'Hardware'){
        
        isValid = true;
      }
      return isValid;
    }
    
    public static boolean isPromoValid(Case newCase, Case oldCase){
      Boolean isValid = false;
      /*if((newCase.Status <> oldCase.Status || newCase.Product__c <> oldCase.Product__c) && newCase.RecordTypeId == RT_SERVICE_ID 
                && newCase.Status == SERVICE_READY_STATUS && newCase.Product__c == GeneralUtiltyClass.PRODUCT_PROMOTIONAL
                && newCase.Bill_To__c <> null && newCase.Ship_To__c <> null){*/
      //AB : 03/04/15 : Add Service Replacement Status and Sub Area Check
      if((newCase.Status <> oldCase.Status || newCase.Product__c <> oldCase.Product__c) && newCase.RecordTypeId == RT_SERVICE_ID 
                && ((newCase.Status == SERVICE_READY_STATUS && newCase.Product__c == GeneralUtiltyClass.PRODUCT_PROMOTIONAL) || newCase.Status == SERVICE_REPLACEMENT)
                && newCase.Bill_To__c <> null && newCase.Ship_To__c <> null){          
        
        isValid = true;
      }
      return isValid;
    }
    
    public static list<Case> getCaseDetails(Set<Id> caseIds) {
        list<Case> detailedCaseList = new list<Case>();
        //LM 04/16/2014 : Added External_Case_Number__c field to SOQL 
        for(Case cs : [select Bill_To__c, SCEA_Product__r.Product_Type__c, Ship_To__c, Owner.Name, ASB_Outbound_Carrier__c, ASB_Outbound_Tracking_Number__c,
                        Approved_Reason__c, SCEA_Product__r.SKU__c, Contact.FirstName, Contact.LastName, Contact.Email, Contact.Phone, 
                        Ship_To__r.Address_Line_1__c, Ship_To__r.Address_Line_2__c, Ship_To__r.City__c, Ship_To__r.State__c, Ship_To__r.Postal_Code__c, Resolution__c, 
                        Status, Asset__r.Model_Number__c, CaseNumber, Fee_Type__c, SCEA_Product__r.Description__c, Asset__r.Serial_Number__c, Ship_To__r.Country__c, 
                        ASB_Service_Center__r.Name, ASB_Service_Center__r.Address_Line_1__c, ASB_Service_Center__r.Address_Line_2__c,ASB_Service_Center__r.State__c,
                        Service_Location__r.Name, Service_Location__r.Address_Line_1__c, Service_Location__r.Address_Line_2__c,Service_Location__r.State__c,
                        Service_Location__r.City__c,Service_Location__r.Country__c, Service_Location__r.Postal_Code__c,Product__c,
                        Asset__r.Product__r.Product_Type__c, Asset__r.Product__r.sub_type__c, Asset__r.Product__r.SKU__c, Asset__r.Product__r.Description__c,
                        ASB_Service_Center__r.City__c,ASB_Service_Center__r.Country__c, ASB_Service_Center__r.Postal_Code__c, Trade_In__c, ASB_Type__c,RecordTypeId,
                        Inbound_Tracking_Number__c,Inbound_Carrier__c, ASB_Resend_Count__c, External_Case_Number__c
                        from Case where Id IN : caseIds] ) {
            detailedCaseList.add(cs);                   
        } 
        return detailedCaseList;
    }
    
    public static boolean isValidPromoOrder(Order__c newOrder , Order__c oldOrder, Case cs){
      Boolean isValid = false;
      if( (newOrder.Order_Status__c <> oldOrder.Order_Status__c || newOrder.Order_Type__c <> oldOrder.Order_Type__c ) &&
          (newOrder.Order_Status__c == 'Open' || newOrder.Order_Status__c == null) &&
          (cs.SCEA_Product__r.Product_Type__c == FIRST_PARTY_PERIPHERAL ||  cs.SCEA_Product__r.Product_Type__c == FIRST_PARTY_SOFTWARE) && 
           newOrder.Order_Type__c == PEROMOTIONAL) {
        isValid = true;          
      }
      return isValid;   
    }
    
    
    webservice static String validatePromoCase(String caseId) {
        String result; 
        Set<String> invalidProfiles = new Set<String>();
        
        for(Invalid_Profiles_For_Promotion__c vp : Invalid_Profiles_For_Promotion__c.getAll().values()) {
            invalidProfiles.add(vp.Name);
        }
        for(Profile p : [select Name from Profile Where Id = :Userinfo.getProfileId()]) {
            if(invalidProfiles.contains(p.Name)) {
                return INVALID_PROFILE_MESSAGE;
            }
        }
        Case oldCase;
        for(Case cs : [select  Id, ContactId,  /*Product__c, Issue1__c, Sub_Area__c,*/ Ship_To__c, Origin, Console_Model_Sent__c,
                         Console_Model_Received__c, Console_Serial_received__c, Console_Serial_Sent__c, Status
                        from Case where Id = :caseId]) {
          oldCase = cs;                 
        }
        Case promoCase = oldCase.clone();
        promoCase.RecordTypeId = GeneralUtiltyClass.RT_SERVICE_ID;
        promoCase.Status = 'Open';
        promoCase.Parent_Case__c = oldCase.Id;
        promoCase.parentId = oldCase.Id;
        promoCase.Product__c = GeneralUtiltyClass.PRODUCT_PROMOTIONAL;
        promoCase.Fee_Type__c = GeneralUtiltyClass.FEE_TYPE_GOODWILL;
        promoCase.ASB_Type__c = ASB_TYPE_NO_BOX_REQUIRED;
        promoCase.Product__c = PRODUCT_PROMOTIONAL;
        if(oldCase.Status != 'Received') {
            promoCase.Console_Model_Received__c = null;
            promoCase.Console_Serial_received__c = null;
        }
        try {
            insert promoCase;
            return 'Successfully created Promotional Service case.';
        } catch(Exception ex) {
            return ex.getMessage();
        }
    }
    
    public static Account getNewConsumer(String firstName, String lastName, String email) {
        String personAccountRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        
        Account newConsumer = new Account();
        newConsumer.RecordTypeId = personAccountRecId;
        newConsumer.FirstName = firstName;
        newConsumer.lastName = lastName;
        newConsumer.PersonEmail = email;
        
        return newConsumer;
    }
    
    public static Address__c getNewAddress(String addressLine1, String AddressLine2, String addressLine3, String city, String state, String country, String zipCode, String addressType) {
        Address__c address = new Address__c();
        address.Address_Line_1__c = addressLine1;
        address.Address_Line_2__c = addressLine2;
        address.Address_Line_3__c = addressLine3;
        address.City__c = city;
        address.State__c = state;
        address.Country__c = country;
        address.Postal_Code__c = zipCode;
        address.Address_Type__c = addressType;
        address.Status__c = 'Active';
        return address;
    }
    
    public static PSN_Account__c getNewPSNAccount(String name, String psnAccId, String signInId) {
        PSN_Account__c psnAcc = new PSN_Account__c();
        psnAcc.Name = name;
        psnAcc.PSN_Account_ID__c = psnAccId;
        psnAcc.PSN_Sign_In_ID__c = signInId;
        psnAcc.Status__c = 'Active';
        return psnAcc;
    }
    
    global class CustomException extends Exception {}
}