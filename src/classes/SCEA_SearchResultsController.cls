/*
    @Author: Venkat Reddy
    @Version: 1.0
    @Description: controller for complete search results page functionality
*/
public class SCEA_SearchResultsController{
    //property to store all the articles which can be used further to show more results
    private List<List<sObject>> searchList;
    //property to hold articles which can be showed n number of records in page
    public list<ResultSet> searchedResultList {get;set;}
    //property to hold search string
    public String searchedString {get;set;}
    //property to hold category in guided browse functionality
    public String guideString {get;set;}
    //property to hold product in guided browse functionality
    public String guideProdString {get;set;}
    //property to hold error code record based on error code search string
    public Error_Code_PKB__c errorCodeCS{get;set;}
    //property to hold products which can be showed in left side filter area
    public List<DataCategoryWrapper> productDataCategoryList{set;get;}
    //property to hold categories which can be showed in left side filter area
    public List<DataCategoryWrapper> topicDataCategoryList{set;get;}
    //property to hold promoted search result articles
    public list<ResultSet> promotedSearchedResultList {get;set;}
    
    //to hold lang parameter value
    public String selectedLanguage{set;get;}
    
    static String decisionTreeArticleKeyPrefix = Decision_Tree__kav.sObjectType.getDescribe().getKeyPrefix();
    static String kcArticleKeyPrefix = KC_Article__kav.sObjectType.getDescribe().getKeyPrefix();
    //property to hold no of records to be shown in page for each time
    private final Integer recordsPerPage = Integer.valueOf(Label.Search_Results_Records_Per_Page);
    //property to hold current page number
    public Integer pageNumber {set;get;}
    public Integer totalRecordCount{set;get;}
    static Integer articleSummaryTrimLength = Integer.valueOf(Label.Article_Summary_Trim_Length);
    Integer searchTextMinimumLength = Integer.valueOf(Label.Search_Text_Minimum_Length);
    
    public SCEA_SearchResultsController() {
        //to accees lang parameter value from URL
        //incase no lang value in URL then make it en_US which is English as default
        if(ApexPages.currentPage().getParameters().get('lang') != null)
            selectedLanguage = ApexPages.currentPage().getParameters().get('lang');
        
        system.debug('--- Site.getBaseURL() ---'+Site.getBaseURL());
        if(Site.getBaseURL() != null && (selectedLanguage == null || selectedLanguage == '')){
            for(SCEA_LanguageSpecificSites__c langSite : SCEA_LanguageSpecificSites__c.getAll().values()){
                if(Site.getBaseURL().containsIgnoreCase(langSite.URL_Identifier_Keyword__c)){
                    selectedLanguage = langSite.Name;
                    break;
                }
            }
        }
        
        if(selectedLanguage == null || selectedLanguage == '')
            selectedLanguage = 'en_US';
        system.debug('--- selectedLanguage ---'+selectedLanguage);
        
        pageNumber = 0;
        totalRecordCount = 0;
        //get the value of search string from query string parameters if existed
        searchedString = ApexPages.currentPage().getParameters().get('selStr');
        //get the value of category from query string parameters if existed
        guideString = ApexPages.currentPage().getParameters().get('guideStr');
        //get the value of product from query string parameters if existed
        guideProdString = ApexPages.currentPage().getParameters().get('guideProdStr');
        searchedResultList = new list<ResultSet>();
        //process for searchstring
        if(searchedString != null && searchedString.length()>searchTextMinimumLength){
            //SOSL text should be more then one charecter
            //check if the search string is error code or not
            //List<Error_Codes__c> errorCodeCSList = Error_Codes__c.getAll().values();
            List<Error_Code_PKB__c> errorCodeCSList = [select Id, Name, Error_Code__c, Description__c, Action__c from Error_Code_PKB__c where Language__c=:selectedLanguage];
            if(errorCodeCSList != null && !errorCodeCSList.isEmpty()){
                //if matches with error code then return error code which is a single record to show in search results
                for(Error_Code_PKB__c errorCodeObj : errorCodeCSList){
                    if(errorCodeObj.Error_Code__c.containsIgnoreCase(searchedString)){
                        errorCodeCS = errorCodeObj;
                        break;
                    }
                }
            }
            //if no error code found with the given search string
            if(errorCodeCS == null){
                searchedString = searchedString+'*';
                //SOSL operation to retrive records of all the article types with the given search string
                String dynamicSOSLQuery = 'FIND :searchedString IN ALL FIELDS RETURNING ';
                dynamicSOSLQuery += 'Decision_Tree__kav (Id, ArticleNumber,Title,Summary,ArticleType, UrlName where PublishStatus=\'Online\' AND Language=:selectedLanguage),';
                dynamicSOSLQuery += 'KC_Article__kav (Id, ArticleNumber, Title, Summary,ArticleType, UrlName where PublishStatus=\'Online\' AND Language=:selectedLanguage)';
                searchList = search.query(dynamicSOSLQuery);
                //Adding diffrent object's records in sobject list
                for(List<SObject> o:searchList){
                    totalRecordCount += o.size();
                }
                
                for(List<SObject> o:searchList){
                    for(SObject s:o){
                        searchedResultList.add(new SCEA_SearchResultsController.ResultSet(s,selectedLanguage));
                        //if no of records in list is equals to records per page then break it nd show in search results
                        if(searchedResultList.size() == recordsPerPage){
                            break;
                        }
                    }
                    //if no of records in list is equals to records per page then break it nd show in search results
                    if(searchedResultList.size() == recordsPerPage){
                        break;
                    }
                }
                //logic to fetch promoted search articles with the given string query
                promotedSearchedResultList = new List<ResultSet>();
                List<sObject> promotedSearchList = new List<sObject>();
                String promotedSearchString = '%'+ApexPages.currentPage().getParameters().get('selStr')+'%';
                Integer promotedSearchResultsLimit = Integer.valueOf(Label.Promoted_Search_Results_Count_To_Display);
                Set<Id> kavIdSet = new Set<Id>();
                
                for(SearchPromotionRule spRule : [select Id, PromotedEntityId, Query from SearchPromotionRule where Query like :promotedSearchString LIMIT :promotedSearchResultsLimit]){
                    kavIdSet.add(spRule.PromotedEntityId);
                }
                
                //query on Decision Tree article based on promoted search article id
                String soqlQuery = 'select Id, ArticleNumber, Title, summary, ArticleType, UrlName from Decision_Tree__kav where Id in:kavIdSet AND PublishStatus=\'Online\' AND Language=:selectedLanguage ORDER BY ArticleNumber DESC LIMIT '+promotedSearchResultsLimit;
                List<sObject> sobjectList = Database.query(soqlQuery);
                promotedSearchList.addAll(sobjectList);
                if(promotedSearchList.size() < promotedSearchResultsLimit){
                    //query on KC article based on promoted search article id
                    soqlQuery = 'select Id, ArticleNumber, Title, summary,ArticleType, UrlName from KC_Article__kav where Id in:kavIdSet AND PublishStatus=\'Online\' AND Language=:selectedLanguage ORDER BY ArticleNumber DESC LIMIT '+(promotedSearchResultsLimit-promotedSearchList.size());
                    sobjectList = Database.query(soqlQuery);
                    promotedSearchList.addAll(sobjectList);
                }
                for(sObject sObj :  promotedSearchList){
                    promotedSearchedResultList.add(new SCEA_SearchResultsController.ResultSet(sObj,selectedLanguage));
                }
            }
        }
        //if search string is not provided then check the process for category/product based articles
        //process for category/product based articles w/o search string
        //as there is no searhc string to use SOSL, use SOQL on each and every artile type to fetch all the articles based on category/product
        else if(guideString != null && guideString != ''){
            searchList = new List<List<sObject>>();
            String soqlQuery = '';
            List<sObject> sobjectList = new List<sObject>();
            //query on Decision Tree article based on category and/or product
            soqlQuery = 'select Id, ArticleNumber,Title,summary,ArticleType, UrlName from Decision_Tree__kav where PublishStatus=\'Online\' AND Language=:selectedLanguage with data category Topic__c AT '+guideString;
            if(guideProdString != null && guideProdString != '')
                soqlQuery += ' AND Products__c AT '+guideProdString;
            sobjectList = Database.query(soqlQuery);
            searchList.add(sobjectList);
            //query on KC article based on category and/or product
            soqlQuery = 'select Id, ArticleNumber,Title,summary,ArticleType, UrlName from KC_Article__kav where PublishStatus=\'Online\' AND Language=:selectedLanguage with data category Topic__c AT '+guideString;
            if(guideProdString != null && guideProdString != '')
                soqlQuery += ' AND Products__c AT '+guideProdString;
            sobjectList = Database.query(soqlQuery);
            searchList.add(sobjectList);
            //Adding diffrent object's records in sobject list
            for(List<SObject> o:searchList){
                totalRecordCount += o.size();
            }
            
            for(List<SObject> o:searchList){
                for(SObject s:o){
                    searchedResultList.add(new SCEA_SearchResultsController.ResultSet(s,selectedLanguage));
                    if(searchedResultList.size() == recordsPerPage){
                        break;
                    }
                }
                if(searchedResultList.size() == recordsPerPage){
                    break;
                }
            }
        }
        //to prepare articles related data categories
        processResultSetToIdentifyDataCategory(searchedResultList);
        fetchDataCategories();
    }
    //method to showcase next set of results to show in search results page on clicking of Show More button
    public void showMoreResults(){
        searchedResultList = new List<ResultSet>();
        //get the current page number
        Integer processedRecordsCount = (pageNumber*recordsPerPage)+recordsPerPage;
        pageNumber++;
        Integer tempCounter = 0;
        system.debug(processedRecordsCount+'---'+pageNumber);
        for(List<SObject> o:searchList){
            for(SObject s:o){
                system.debug(tempCounter+'---'+processedRecordsCount);
                tempCounter++;
                //bypass/ignore the records that shown in search results page
                if(tempCounter<=processedRecordsCount){
                    continue;
                }
                //if no of records in list is equals to records per page then break it and show in search results
                else{
                    searchedResultList.add(new SCEA_SearchResultsController.ResultSet(s,selectedLanguage));
                    if(searchedResultList.size() == recordsPerPage){
                        break;
                    }
                }
            }
            //if no of records in list is equals to records per page then break it and show in search results
            if(searchedResultList.size() == recordsPerPage){
                break;
            }
        }
        //to prepare articles related data categories
        processResultSetToIdentifyDataCategory(searchedResultList);
    }
    //method to fetch categories and its associated child categories of Product & Topic data categories
    //and then prepare a list with wrappers to show in left side filter of search results page
    public void fetchDataCategories(){
        productDataCategoryList = new List<DataCategoryWrapper>();
        topicDataCategoryList = new List<DataCategoryWrapper>();
        //prepare the object with Products data category of KnowledgeArticle
        DataCategoryGroupSobjectTypePair pair1 = new DataCategoryGroupSobjectTypePair();
        pair1.setSobject('KnowledgeArticleVersion');
        pair1.setDataCategoryGroupName('Products');
        //prepare the object with Topic/Category data category of KnowledgeArticle
        DataCategoryGroupSobjectTypePair pair2 = new DataCategoryGroupSobjectTypePair();
        pair2.setSobject('KnowledgeArticleVersion');
        pair2.setDataCategoryGroupName('Topic');
        
        DataCategoryGroupSobjectTypePair[] pairs = new DataCategoryGroupSobjectTypePair[] {pair1,pair2};
            
        //Describe Call
        List<DescribeDataCategoryGroupStructureResult> describeCategoryResult = Schema.describeDataCategoryGroupStructures(pairs,false);
        for(DescribeDataCategoryGroupStructureResult singleResult : describeCategoryResult){            
            for(DataCategory topCategories : singleResult.getTopCategories()){
                //process all the child categories of all categories and preapre wrapper list based upon category like either Product or Topic/Category
                for(DataCategory childCategories : topCategories.getChildCategories()){
                //Getting the name of the category
                system.debug('category---'+childCategories.getName());
                //Getting the name of label
                system.debug('category---'+childCategories.getLabel());
                    DataCategoryWrapper dataCatWrapper = new DataCategoryWrapper();
                    dataCatWrapper.isSelected = false;
                    dataCatWrapper.dataCategoryGroupName = singleResult.getName();
                    dataCatWrapper.dataCategoryName = childCategories.getName();
                    dataCatWrapper.dataCategoryLabel = childCategories.getLabel();
                    //for guided broser functionality, if any product or topic/category provided then make sure that 
                    //respective checkbox is checked automatically
                    if(guideString != null && guideString.equalsIgnoreCase(dataCatWrapper.dataCategoryName+'__c'))
                        dataCatWrapper.isSelected = true;
                    if(guideProdString != null && guideProdString.equalsIgnoreCase(dataCatWrapper.dataCategoryName+'__c'))
                        dataCatWrapper.isSelected = true;
                    //if data category is of type Products then add it to respective wrapper list
                    if(singleResult.getName().equalsIgnoreCase('Products')){
                        productDataCategoryList.add(dataCatWrapper);
                    }
                    //if data category is of type Topic then add it to respective wrapper list
                    else if(singleResult.getName().equalsIgnoreCase('Topic')){
                        topicDataCategoryList.add(dataCatWrapper);
                    }
                }
            }
        }
    }
    
    public void showfilterWiseResults() {
        
        //if(searchedString != null && searchedString.length()>2){
            searchedResultList = new list<ResultSet>();
            String selectedTopicCategories = '';
            String selectedProductCategories = '';
            pageNumber = 0;
        
            for(DataCategoryWrapper categoryWrapper : topicDataCategoryList){
                if(categoryWrapper.isSelected){
                    if(selectedTopicCategories == '')
                        selectedTopicCategories = categoryWrapper.dataCategoryName+'__c';
                    else
                        selectedTopicCategories += ','+categoryWrapper.dataCategoryName+'__c';
                }
            }
            
            for(DataCategoryWrapper categoryWrapper : productDataCategoryList){
                if(categoryWrapper.isSelected){
                    if(selectedProductCategories == '')
                        selectedProductCategories = categoryWrapper.dataCategoryName+'__c';
                    else
                        selectedProductCategories += ','+categoryWrapper.dataCategoryName+'__c';
                }
            }
            
            if(selectedTopicCategories != '')
                selectedTopicCategories = '('+selectedTopicCategories+')';
                
            if(selectedProductCategories != '')
                selectedProductCategories = '('+selectedProductCategories +')';
                
            String publishStatus = 'Online';
            String languageStr = 'en_US';
            
            system.debug('--- selectedTopicCategories ---'+selectedTopicCategories);
            system.debug('--- selectedProductCategories ---'+selectedProductCategories);
            
            if(searchedString != null && searchedString.length()>searchTextMinimumLength){
                if(selectedTopicCategories == '' && selectedProductCategories == ''){
                    String dynamicSOSLQuery = 'FIND :searchedString IN ALL FIELDS RETURNING ';
                    dynamicSOSLQuery += 'Decision_Tree__kav (Id, ArticleNumber,Title,Summary,ArticleType, UrlName where PublishStatus=\'Online\' AND Language=:selectedLanguage),';
                    dynamicSOSLQuery += 'KC_Article__kav (Id, ArticleNumber, Title, Summary,ArticleType, UrlName where PublishStatus=\'Online\' AND Language=:selectedLanguage)';
                    searchList = search.query(dynamicSOSLQuery);
                }else{
                    String dynamicSOSLQuery = '';
                    dynamicSOSLQuery += 'FIND :searchedString IN ALL FIELDS RETURNING ';
                    dynamicSOSLQuery += 'Decision_Tree__kav (Id, ArticleNumber,Title,Summary,ArticleType, UrlName where PublishStatus=:publishStatus AND Language=:selectedLanguage),';
                    dynamicSOSLQuery += 'KC_Article__kav (Id, ArticleNumber,Title,Summary,ArticleType, UrlName where PublishStatus=:publishStatus AND Language=:selectedLanguage) ';
                    
                    if(selectedTopicCategories != null && selectedTopicCategories != '')
                        dynamicSOSLQuery += 'WITH DATA CATEGORY Topic__c AT '+selectedTopicCategories;
                    
                    if(selectedProductCategories != null && selectedProductCategories != ''){
                        if(selectedTopicCategories != null && selectedTopicCategories != '')
                            dynamicSOSLQuery += ' AND Products__c AT '+selectedProductCategories;
                        else
                            dynamicSOSLQuery += ' WITH DATA CATEGORY Products__c AT '+selectedProductCategories;
                    }
                    system.debug('--- dynamicSOSLQuery ---'+dynamicSOSLQuery);
                    searchList = search.query(dynamicSOSLQuery);
                }
            }else if(guideString != null && guideString != ''){
                searchList = new List<List<sObject>>();
                if(selectedTopicCategories == '' && selectedProductCategories == ''){
                    String soqlQuery = '';
                    List<sObject> sobjectList = new List<sObject>();
                    soqlQuery = 'select Id, ArticleNumber,Title,summar,ArticleType, UrlNamey from Decision_Tree__kav where PublishStatus=\'Online\' AND Language=:selectedLanguage with data category Topic__c AT '+guideString;
                    if(guideProdString != null && guideProdString != '')
                        soqlQuery += ' AND Products__c AT '+guideProdString;
                    system.debug('--- soqlQuery ---'+soqlQuery);
                    sobjectList = Database.query(soqlQuery);
                    searchList.add(sobjectList);
                    
                    soqlQuery = 'select Id, ArticleNumber,Title,summary,ArticleType, UrlName from KC_Article__kav where PublishStatus=\'Online\' AND Language=:selectedLanguage with data category Topic__c AT '+guideString;
                    if(guideProdString != null && guideProdString != '')
                        soqlQuery += ' AND Products__c AT '+guideProdString;
                    system.debug('--- soqlQuery ---'+soqlQuery);
                    sobjectList = Database.query(soqlQuery);
                    searchList.add(sobjectList);
                    
                    for(DataCategoryWrapper categoryWrapper : topicDataCategoryList){
                        if(guideString.equalsIgnoreCase(categoryWrapper.dataCategoryName+'__c')){
                            categoryWrapper.isSelected = true;
                            break;
                        }
                    }
                    
                    for(DataCategoryWrapper categoryWrapper : productDataCategoryList){
                        if(guideProdString.equalsIgnoreCase(categoryWrapper.dataCategoryName+'__c')){
                            categoryWrapper.isSelected = true;
                            break;
                        }
                    }
                }else{
                    String soqlQuery = '';
                    List<sObject> sobjectList = new List<sObject>();
                    
                    soqlQuery = 'select Id, ArticleNumber, Title, Summary, ArticleType, UrlName from Decision_Tree__kav where PublishStatus=\'Online\' AND Language=:selectedLanguage ';
                    
                    if(selectedTopicCategories != null && selectedTopicCategories != '')
                        soqlQuery += 'with data category Topic__c AT '+selectedTopicCategories;
                        
                    if(selectedProductCategories != null && selectedProductCategories != ''){
                        if(selectedTopicCategories != null && selectedTopicCategories != '')
                            soqlQuery += ' AND Products__c AT '+selectedProductCategories;
                        else
                            soqlQuery += ' WITH DATA CATEGORY Products__c AT '+selectedProductCategories;
                    }
                    system.debug('--- soqlQuery ---'+soqlQuery);
                    sobjectList = Database.query(soqlQuery);
                    searchList.add(sobjectList);
                    
                    soqlQuery = 'select Id, ArticleNumber, Title, Summary, ArticleType, UrlName from KC_Article__kav where PublishStatus=\'Online\' AND Language=:selectedLanguage ';
                    
                    if(selectedTopicCategories != null && selectedTopicCategories != '')
                        soqlQuery += 'with data category Topic__c AT '+selectedTopicCategories;
                        
                    if(selectedProductCategories != null && selectedProductCategories != ''){
                        if(selectedTopicCategories != null && selectedTopicCategories != '')
                            soqlQuery += ' AND Products__c AT '+selectedProductCategories;
                        else
                            soqlQuery += ' WITH DATA CATEGORY Products__c AT '+selectedProductCategories;
                    }
                    system.debug('--- soqlQuery ---'+soqlQuery);
                    sobjectList = Database.query(soqlQuery);
                    searchList.add(sobjectList);
                }
            }
            totalRecordCount = 0;
            for(List<SObject> o:searchList){
                totalRecordCount += o.size();
            }
            
            //Adding diffrent object's records in sobject list
            for(List<SObject> o:searchList){
                for(SObject s:o){
                    searchedResultList.add(new SCEA_SearchResultsController.ResultSet(s,selectedLanguage));
                    if(searchedResultList.size() == recordsPerPage){
                        break;
                    }
                }
                if(searchedResultList.size() == recordsPerPage){
                    break;
                }
            }
            //to prepare articles related data categories
            processResultSetToIdentifyDataCategory(searchedResultList);
        //}
    }
    
    private void processResultSetToIdentifyDataCategory(List<ResultSet> searchedResultList){
        if(searchedResultList != null && !searchedResultList.isEmpty()){
            Set<Id> articleIdSet = new Set<Id>();
            for(ResultSet rs : searchedResultList){
                articleIdSet.add(rs.Id);
            }
            
            List<sObject> articleTypeDataCatergorySelectionList = new List<sObject>();
            
            List<sObject> decisiontreeDCList = [select ParentId, DataCategoryGroupName, DataCategoryName from Decision_Tree__DataCategorySelection where ParentId in :articleIdSet];
            articleTypeDataCatergorySelectionList.addAll(decisiontreeDCList);
            
            List<sObject> kcArticleDCList = [select ParentId, DataCategoryGroupName, DataCategoryName from KC_Article__DataCategorySelection where ParentId in :articleIdSet];
            articleTypeDataCatergorySelectionList.addAll(kcArticleDCList);
            
            Map<String,String> articleWiseDataCategoryMap = new Map<String,String>();
            for(sObject obj : articleTypeDataCatergorySelectionList){
                if(!articleWiseDataCategoryMap.containsKey(String.valueOf(obj.get('ParentId'))))
                    articleWiseDataCategoryMap.put(String.valueOf(obj.get('ParentId')),String.valueOf(obj.get('DataCategoryName')));
                String existingDCName = articleWiseDataCategoryMap.get(String.valueOf(obj.get('ParentId')));
                if(!existingDCName.contains(String.valueOf(obj.get('DataCategoryName'))))
                    existingDCName += ';'+String.valueOf(obj.get('DataCategoryName'));
                articleWiseDataCategoryMap.put(String.valueOf(obj.get('ParentId')),existingDCName);
            }
            
            for(ResultSet rs : searchedResultList){
                if(articleWiseDataCategoryMap != null && articleWiseDataCategoryMap.containsKey(rs.Id+'')){
                    rs.associatedDataCategories = articleWiseDataCategoryMap.get(rs.Id);
                }
            }
        }
    }
    
    public class DataCategoryWrapper{
        public Boolean isSelected{get;set;}
        public String dataCategoryGroupName{get;set;}
        public String dataCategoryName{set;get;}
        public String dataCategoryLabel{set;get;}
    }
    
    /*Record Wrapper*/
    public class ResultSet{
        public String Id {get;set;} 
        public String ArticleNumber{get;set;}
        public String Title{get;set;}
        public String sObjectName {get;set;}
        public String overviewSummary{set;get;}
        public String associatedDataCategories{set;get;}
        public String articleDetailURL{set;get;}
        
        public ResultSet(sObject s, String tempSelectedLanguage){
            this.Id = s.Id;
            this.ArticleNumber = s.get('ArticleNumber')+'';
            this.Title = s.get('Title')+'';
            
            if(String.valueOf(s.Id).startsWith(decisionTreeArticleKeyPrefix)){
                this.overviewSummary = s.get('Summary')+'';
                this.sObjectName = 'Decison Tree';        
            }else if(String.valueOf(s.Id).startsWith(kcArticleKeyPrefix)){
                this.overviewSummary = s.get('Summary')+'';
                this.sObjectName = 'KC Article';        
            }
            
            if(this.overviewSummary.length()>articleSummaryTrimLength)
                this.overviewSummary = this.overviewSummary.substring(0,(articleSummaryTrimLength-2))+'...';
            
            this.overviewSummary = this.overviewSummary.replaceAll('<br/>','\n');
            this.overviewSummary = this.overviewSummary.replaceAll('<br />','\n');
            String htmlTagPattern = '<.*?>';
            Pattern patternTemp = Pattern.compile(htmlTagPattern);
            Matcher matcherTemp = patternTemp.matcher(this.overviewSummary);
            this.overviewSummary = matcherTemp.replaceAll('');
            
            if(Site.getBaseURL() != null){
                articleDetailURL = Site.getBaseURL()+'/articles/'+tempSelectedLanguage+'/'+String.valueOf(s.get('ArticleType')).replace('__kav','')+'/'+String.valueOf(s.get('UrlName'));
            }
        }     
    }
}