/**********************************************************************************************************************************
(c) 2013 Appirio, Inc.

Description : Test class for AssetManagement.
                  
Original August 26, 2013  : KapiL Choudhary(JDC) Created for the Task T-175201
Updated :August 30, 2013  : KapiL Choudhary(JDC) Updated for the Task T-179024
			Sep    20 2013   Urminder(JDC) Updated with TestMockUp
			
          02/18/2015 Leena Mandadapu : Jira# SMS-1041 - Re-activate Canadian PPP Dealer Ids - Added test for PPP cancellation process
*************************************************************************************************************************************/

@isTest
global class AssetManagement_Test {

    static testMethod void assetManagementUnitTest() {
    	Test.startTest();
        Test.setMock(WebServiceMock.class, new WebServiceMockUpdateAsset());
       
    	list<Asset__c> assetList = TestClassUtility.createAssets(2, true);
    	
    	if(!assetList.isEmpty()){
    		assetList[0].PlayStation_Plus_Subscription_Start_Date__c = Date.today().addDays(2);
    		assetList[0].PPP_Status__c = 'updated';
    		update assetList;
    		
    		// Must Creates a recod in Staging_Asset__c with MDM_ID__c Id.
        	system.assertEquals(1, [select id from Staging_Asset__c where MDM_Asset_ID__c =: assetList[0].MDM_ID__c].size());
        	Test.stopTest();	
    	}
    }
    
    static testMethod void assetPPPCancellationprocessTest(){
        Test.startTest();
        //create Warranty product record	
        Product__c prod = TestClassUtility.createProduct(1, false)[0];
        prod.List_Price__c = 100;
        prod.Product_Type__c = 'ESP';
        insert prod;
                     
        //create asset record
        Asset__c ast = TestClassUtility.createAssets(1, false)[0];
		ast.PPP_Product__c = prod.Id;
		ast.PPP_Purchase_Date__c = Date.today().addDays(15);
		ast.Purchase_Date__c = Date.today();
		ast.PPP_Status__c = 'Active';
		insert ast;
		
		//Update asset PPP status = Pending Cancel
		ast.PPP_Status__c = 'Pending Cancel';
		Update ast;
		
		test.StopTest();
		
    }
    global class WebServiceMockUpdateAsset implements WebServiceMock {
	   global void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,	
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	      		Sony_MiddlewareUpdateAccountAsset.PSUpdateAcntAsset_Output_element responseElm = 
	      		  	new Sony_MiddlewareUpdateAccountAsset.PSUpdateAcntAsset_Output_element();
	       		
	       		
	       		    Sony_MiddlewareUpdateAccountAsset.Asset_mdm  ast = new Sony_MiddlewareUpdateAccountAsset.Asset_mdm();
					ast.AssetRowId = '1';
					ast.SerialNumber = '1234';
					ast.ProductName = 'testing';
					
					//creating list of assets
					
					Sony_MiddlewareUpdateAccountAsset.ListOfAsset_mdm   last = new Sony_MiddlewareUpdateAccountAsset.ListOfAsset_mdm ();
					last.Asset = new list<Sony_MiddlewareUpdateAccountAsset.Asset_mdm>{ast};
					
					//associating assets with account.
					
					Sony_MiddlewareUpdateAccountAsset.Account_mdm   acc = new Sony_MiddlewareUpdateAccountAsset.Account_mdm ();
					acc.MDMAccountRowId = '1234567';
					acc.ListOfAsset = last;
					
					//creating list of list of accounts
					
					Sony_MiddlewareUpdateAccountAsset.ListOfAccount_mdm    lacc = new Sony_MiddlewareUpdateAccountAsset.ListOfAccount_mdm  ();
					lacc.Account  = new list<Sony_MiddlewareUpdateAccountAsset.Account_mdm>{acc};
				  
				  responseElm.ListOfAccount = lacc;
				       		 
	       		  response.put('response_x', responseElm); 
	   		   }
    }
}