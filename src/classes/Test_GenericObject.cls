/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers. */
@isTest(seeAllData=true)
private class Test_GenericObject {

	public static final String DATE_VALUE = 'DateValue';
    static testMethod void OrderObjectTest() {
        
        //For GenericAttributedObj code-coverage
        GenericObject.GenericAttributedObj getObj = new GenericObject.GenericAttributedObj();
        getObj.attributes =new map<string, string>();
        getObj.attributes.put('BODErrorDescription','!!');
        getObj.attributes.put('BODErrorCode','@@');
        getObj.attributes.put('NounFailureErrorDescription','@@');
        getObj.attributes.put(DATE_VALUE,'2014-02-17 23:59:59');        
        Date convertedDate = getObj.formatDate(DATE_VALUE);
        System.assert(convertedDate!=null);
        String convertedstring = getObj.formatDateAsString(DATE_VALUE);
        System.assert(convertedstring!=null);
        System.assert(getObj.fields!=null);
        getObj.setVal(DATE_VALUE, convertedDate);        
        System.assert(getObj.getVal(DATE_VALUE)!=null);
        
        GenericObject.TTDTaxObject ttdTax = new GenericObject.TTDTaxObject();
        ttdTax.tax = 23.3;
        
        GenericObject.OrderObject orderObject = new GenericObject.OrderObject();
        System.assert(orderObject.lineItems!=null);
        
        GenericObject.OrderLine orderLine = new GenericObject.OrderLine();
        System.assert(orderLine.Charges!=null);
        getObj.attributes.clear();
        String jsonString = '{"ID": "SGML", "SortAs": "SGML", "GlossTerm": "Standard Generalized Markup Language", "Acronym": "SGML", "Abbrev": "ISO 8879:1986"}';
        getObj.initJson(jsonString);
        System.assert(getObj.attributes.size()>0);
        
    }
    
}