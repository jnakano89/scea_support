/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers. 
 */
@isTest
private class DescribeUtility_Test {

    static testMethod void myUnitTest() {
       
        Test.startTest();     
         Account acc = TestClassUtility.createAccount(false);
         insert acc;
         System.assert(DescribeUtility.getsObjectById(new List<Id>{acc.Id}) != null);
         System.assert(DescribeUtility.getFieldsForsObject('Account') != null);
        Test.stopTest();
    }
}