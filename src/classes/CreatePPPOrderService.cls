/*
Updated By 		: Ranjeet Singh(Appirio)
Updated Date	: 12/30/2013
Task				: T-166688
Description 	:
Salesforce [Appirio] will develop a web service to allow the creation of a PlayStation Protection Plan Order and associated objects (Asset, Contact, etc.) within Salesforce 
	environment to complete a web PPP purchase, and update CyberSource Secure Acceptance configuration to reflect the correct PPP merchant details.
IT [WebPPP] will consume a Salesforce web service to complete the PlayStation Protection Plan purchase flow, update the list of PPP eligible products, 
	update the defined PPP eligible SKU values, and update the purchase flow to take advantage of the CyberSource Secure Acceptance web service mechanism 
	already developed by Appirio.

SFDC - Web Service Input

	System Type [Asset__c.Product_Genre__c]
	Model Number [Asset__c.Model_Number__c]
	Serial Number [Asset__c.Serial_Number__c]
	Proof of Purchase Date [Asset__c.Purchase_Date__c]
	PPP SKU [Asset__c.PPP_Product_SKU__c]
	Billing Address 1 [Address__c.Address_Line_1__c]
	Billing Address 2 [Address__c.Address_Line_2__c] (*Optional)
	Billing City [Address__c.City__c]
	Billing State [Address__c.State__c]
	Billing Zip Code [Address__c.Postal_Code__c]
	Billing Country [Address__c.Country__c]
	Payment Transaction Number[Payment__c.Transaction_Number__c]
	
Success Response:
	Order Number [External_Order_Number__c]
	Note: Orders created from the WebPPP site should return an auto-generated number with a PS- prefix (i.e. PS-1234567).

Failure Response:
	Web Service Message: Unable to Complete Transaction

Email Alert
	Recipients: scea_rhq_cs_tech_team@playstation.sony.com
	Subject: WebPPP Failure
	Body: Unable to complete WebPPP process due to {insert action/object or connection failure}. {Insert complete web service URL {i.e. http://webserver/fname/###/lname/###/…}

SFDC - Web Service Logic
	Create pertinent Salesforce objects.
	Submit to Assurant for Underwriting [Staging_PPP_Outbound__c]
	Notify Consumer [Email Template]: Protection Plan Welcome Email
	

*/
global class CreatePPPOrderService {

	global class WebServiceInput{
		webservice String systemType{get;set;}
		webservice String modelNumber{get;set;}
		webservice String serialNumber{get;set;}
		webservice Date proofOfPurchaseDate{get;set;}
		webservice String pppSKU {get;set;}
		webservice String firstName {get;set;}
		webservice String lastName {get;set;}
		webservice String phone {get;set;}
		webservice String email {get;set;}
		/*webservice String billingAddress1 {get;set;}
		webservice String billingAddress2 {get;set;}
		webservice String billingCity {get;set;}
		webservice String billingState {get;set;}
		webservice String billingZipCode {get;set;}
		webservice String billingCountry {get;set;} */
		//webservice String paymentTransactionNumber {get;set;} 

		// Created by Charu

		webservice string Order_Number;			// *(Order.External_Order_Number__c)
		webservice Date Target_Date;			// *(Order.Order_Date__c)
	}
	
	global class WebServiceResponse{
		//webservice String orderNumber{get;set;}
		
		webservice string status;
		webservice WebServiceFailureResponse failResponse{get;set;}
	}
	
	global class WebServiceFailureResponse{
		webservice String message{get;set;}
		webservice String detailMessage{get;set;}
		//webservice EmailAlert emailAlert{get;set;}
	}
	/*
	global class EmailAlert{
		webservice String recepients{get;set;}
		webservice String subject{get;set;}
		webservice Blob body{get;set;}
	}*/
	
	webservice static WebServiceResponse processPPPFlow(WebServiceInput request){
		
		System.Debug('>>>>>>>>> WebPPP Request<<<<<<<<<<<<<<  ' +request);
		
		WebServiceResponse response = new WebServiceResponse();
		response.status = 'Failed';
		String errorDetails = '';
		Savepoint sp = Database.setSavepoint();
		try{
			if(String.isEmpty(System.Label.Email_For_Sony_Integration_Error)){
				CyberSourceUtility.failureEmailAlert(response, request, 'Unable to Complete Transaction. Please configure the email address at label \'Email_For_Sony_Integration_Error\'.', null);
				return response;
			}else{
				if(request!=null){
					
					//List<Address__c>addresses = new List<Address__c>();
					
					//Contact contact = sObjectUtility.getContact( request.firstName, request.lastName, request.phone, request.email);
					
					Account AccConsumer = null;
					try{
						AccConsumer = sObjectUtility.getAccConsumer( request.firstName, request.lastName, request.phone, request.email);
					}catch (Exception e){
						response.status = 'Failed';
						CyberSourceUtility.failureEmailAlert(response, request, 'Unable to find or create Consumer 1.', e);
						return response;
					}      
					if(AccConsumer==null || AccConsumer.id ==null){
						response.status = 'Failed';
						CyberSourceUtility.failureEmailAlert(response, request, 'Unable to find or create Consumer 2.'+AccConsumer, null);
						return response;
					}

					AccConsumer= [Select Id, ispersonaccount, PersoncontactId from Account where Id=:AccConsumer.Id][0]; 
					if(AccConsumer.PersonContactId == null){
						response.status = 'Failed';
						CyberSourceUtility.failureEmailAlert(response, request, 'Unable to find Consumer for Account-Id:'+AccConsumer.id, null);
						return response;
					}
					
					Asset__c asset = null;
					try{
						asset = sObjectUtility.getAsset(request, AccConsumer);
					}catch (Exception e){
						response.status = 'Failed';
						CyberSourceUtility.failureEmailAlert(response, request, 'Unable to create asset/Asset-Consumer.', e);
						return response;
					}  
						
					if(asset==null || asset.id==null){
						response.status = 'Failed';
						CyberSourceUtility.failureEmailAlert(response, request, 'Unable to create asset.', null);
						return response;
					}
					//Asset__c asset;
					
					// Changed by CM 
					Order__c order = null;
					try{
						order = sObjectUtility.getOrder(request, null, asset, 'WebPPP', AccConsumer);
					} catch (Exception e){
						response.status = 'Failed';
						CyberSourceUtility.failureEmailAlert(response, request, 'Unable to create Order.', e);
						return response;
					}
					if(order==null || order.id == null){
						response.status = 'Failed';
						CyberSourceUtility.failureEmailAlert(response, request, 'Unable to create Order.', null);
						return response;
						
					}					
					order.Order_Status__c = 'Open';
					update order;
					//Staging_PPP_Outbound__c stgOutBound = sObjectUtility.getStaging_PPP_Outbound(request, order);
					response.status = 'Success';
					//After APEX DML,  callout not allowed so needs to be done on different thread/Future,
					actionOnCyberSource(order.id, Asset.id, AccConsumer.Id, AccConsumer.PersonContactId, asset.Product__c, request.Order_Number, JSON.serialize(request)); 
					return response;
				}
				CyberSourceUtility.failureEmailAlert(response, request, '', null);
			}
		}catch(Exception Ex){
			Database.rollback(sp);
			CyberSourceUtility.failureEmailAlert(response, request, '', Ex);
		}
		return response;
	}
	
	/*
		Actions needs to be performed after CyberSource success.

		Create Payment__c SFDC sObject from input request.
			webservice string serviceCost;			// [Payment.Amount]
			webservice string paymentToken;			// [Payment__c.Token__c] 
			webservice string consumerAccount;		// [Payment__c.Primary_Account_Number__c]
			webservice string transactionNumber;	// [Payment__c.Transaction_Number__c]
	*/
  	@future (callout=true)
	static void actionOnCyberSource(Id OrderId, Id AssetId, Id ConsumerId, Id PersonContactId, Id ProductId, String orderNo, String responseJSON){
		
		String errorText = 'Unable to Complete Transaction. Cybersource API call failed.';
		CreatePPPOrderService.WebServiceInput request = (CreatePPPOrderService.WebServiceInput)JSON.deserialize(responseJSON, CreatePPPOrderService.WebServiceInput.class) ;
		
		try{
		
		     Payment__c payment = sObjectUtility.getPaymentFromCybersource_New(orderNo, request.Target_Date, OrderId, ProductId,'PPP');
		     
		
		if(payment!=null && 'Charged'.equalsIgnoreCase(payment.Payment_Status__c)){
			/*  commented out per Task TC-247202	
				CyberSourceUtility.sendSuccessEmail(PersonContactId, OrderId, 'CS153_PPP_Welcome');
			 */	
			
			Address__c billAdr = sObjectUtility.getAddress(PersonContactId, payment.Billing_Address__c, '', payment.Billing_City__c, payment.Billing_State__c, payment.Billing_Country__c, 
					payment.Billing_Zip__c, 'Billing');
		  
		    //Address__c billAdr;
			upsert billAdr;

			// UPDATE CONSUMER INFO
			Account Consumer = new Account (id=ConsumerId);
			consumer.Bill_To__pc=billAdr.id;
			update consumer;
			
			Account acc = [select id, Ship_To__pc from Account where id=:ConsumerId];
			/*Consumer_Asset__c ca = new Consumer_Asset__c();
			
			ca.Asset__c=AssetId;
			ca.Consumer__c=PersonContactId;
			
			upsert ca; */
			
			/*Contact contact = new Contact(id = ContactId);
			contact.Bill_To__c = billAdr.id;
			update contact; 
			
			
			system.debug('***********contact>>'+contact); */
			
			
			
			
			Order__c order = new Order__C(id=OrderId);
			order.Bill_To__c = billAdr.id;
			order.Ship_To__c = acc.Ship_To__pc;
		    order.Consumer__c = PersonContactId;
			update order;
			system.debug('***********>>'+order);
			
			
			payment.Purchase_Type__c='PPP';
			update payment;
			
		}else{
			//Send error email.
			if(payment!=null){
				errorText = errorText + '\n\nCybersource Payment Status :'+payment.Payment_Status__c;
			}
			//errorText
			system.debug('***********>>ERROR.');
			CyberSourceUtility.failureEmailAlert(null, request, errorText, null);
		}
		}catch(Exception ex){
			system.debug('***********>>ex.'+Ex);
			CyberSourceUtility.failureEmailAlert(null, request, errorText, ex);			
		}
	}	

}