/******************************************************************************
Class         : EditAddressController_Test
Description   : Test class for EditAddressController
Developed by  : Poonam Varyani
Date          : August 7, 2013              
******************************************************************************/
@isTest
private class CreateCaseController_Test {

    static testMethod void createCaseTest() {
        //create asset
        List<Asset__c> assetList = new List<Asset__c>();
        assetList = TestClassUtility.createAssets(1, true);
        system.debug('---assetList ---' + assetList);
        //create account
       String rtId;
    	for(RecordType rt : [select Id from RecordType where SobjectType = 'Account']) {
    		rtId = rt.Id;
    		break;
    	}
    	
    	Account account = new Account();
    	account.Name = 'test contact';
    	account.RecordTypeId = rtId;
    	insert account;
    	
        
        //create contact
        Contact contact = new Contact();
        contact.LastName = 'lname';
        contact.AccountId = account.Id;
        insert contact;
         Test.startTest();
        PageReference pg = Page.CreateCase; 
        Test.setCurrentPage(pg); 
        
        CreateCaseController ctrl = new CreateCaseController();
        Pagereference pgRef1 = ctrl.purchaseProtectionPlan();
        system.assertEquals(null, pgRef1);
        
        ApexPages.currentPage().getParameters().put('contactId', contact.Id);
        ApexPages.currentPage().getParameters().put('assetId', assetList[0].Id);
        ApexPages.currentPage().getParameters().put('type', 'Purchase');
       
        
        CreateCaseController crtCaseObj = new CreateCaseController();
        Pagereference pgRef = crtCaseObj.purchaseProtectionPlan();
        
        //check new case created for the above inserted contact
        system.assertEquals(1, [select Id From Case where ContactId = : contact.Id].size());
        
        Test.stopTest();
    }
}