/* ============================================================
 * This code is part of Richard Vanhook's submission to the 
 * Cloudspokes Geolocation Toolkit challenge.
 *
 * This software is provided "AS IS," and you, its user, 
 * assume all risks when using it. 
 * ============================================================
 */
@IsTest(seeAllData=false)
private class GeocodeSearchNearbyDemoControllerTest {

    private static testmethod void testLookup_WithOneResults(){
        try {
            setupGlobalVariables();
            List<Address__c>  addressList = TestClassUtility.createAddress(1, false);  
            addressList[0].Coordinates__Latitude__s = 40.9d;
            addressList[0].Coordinates__Longitude__s = -90.9d;
            insert addressList;
            
            Location__c location = TestClassUtility.createLocation(true);
            
            list<Contact> cntList = TestClassUtility.createContact(1, false);
            cntList[0].Email = 'user@test.com';
    		insert cntList;
            
            Case cs = TestClassUtility.createCase('New', 'Test', false);
            cs.ContactId = cntList[0].Id;
		    cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
		    cs.Sub_Area__c = 'New';
		    cs.Status = 'New';
		    cs.Ship_To__c = addressList[0].Id;
		    insert cs;
		    
		    Apexpages.currentPage().getParameters().put('caseId', cs.Id);
		    Apexpages.currentPage().getParameters().put('ctRadio', location.Id);		    
            
            GeocodeSearchNearbyDemoController controller = new GeocodeSearchNearbyDemoController();
            System.assert(controller.addressInput!=null);
            System.assertEquals(25,controller.distance);
            System.assertEquals('mi',controller.unit);
            System.assertEquals('Coordinates__Latitude__s',controller.latitudeFieldName);
            System.assertEquals('Coordinates__Longitude__s',controller.longitudeFieldName);
            System.assertEquals('Location__c',controller.objectName);
            System.assertEquals(null,controller.results);
            System.assertNotEquals(null,controller.paginator);
            System.assertEquals(false,controller.searchExecuted);
                   
            final GeoPoint expected = new GeoPoint(41.0d, -91.0d);
            al.HttpUtils.pushTest(buildBasicResponseWithCoordinates(''+expected.latitude,''+expected.longitude));
            controller.addressInput = 'doesn\'t really matter what goes in here during unit test, just not blank';
            controller.distance = 20;
    
            System.assertEquals(null,controller.doSearchNearby());
    		System.assert(controller.gotoCase() != null);
    		Test.startTest();
    		System.assert(controller.sendEmail() == null);
    		Test.stopTest();
            System.assertNotEquals(null,controller.results);
            controller.handlePageChange(null);
            
            System.assert(controller.results.size() >= 0);
            System.assert(controller.distanceOptions.size() > 0);
            System.assert(controller.unitOptions.size() > 0);
        }
        catch(System.QueryException qe) {
            // ------FORCEXPERTS-------------
            // There is a possibility that the expected fields
            // do not exist on the org, so ignore the exception
            // and move on
            System.assert(false, qe.getMessage());
            // ------FORCEXPERTS-------------
        }
        catch(Exception e) {
            if(e.getMessage().contains('System.SObjectException: Invalid field')) {
                System.assert(false, e.getMessage());
            }
        }        
    }

    private static String buildBasicResponseWithCoordinates(String lat, String lng){
        return   '{"query":{"latitude":' 
         + lat 
         + ',"longitude":' 
         + lng 
         + ',"address":"San Francisco"}}';
    }
    
    // ------FORCEXPERTS-------------
    // Removed traces of Simple Geo Seervice from the method
    // ------FORCEXPERTS-------------
    private static void setupGlobalVariables(){
        final Map<String,String> theVariables = new Map<String,String>{
            GlobalVariable.KEY_USE_GOOGLE_GEOCODING_API   => 'false'
        };
        for(String key : theVariables.keySet()){
            GlobalVariableTestUtils.ensureExists(new GlobalVariable__c(name=key,Value__c=theVariables.get(key)));
        }
    }

}