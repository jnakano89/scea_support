@isTest
global class RefreshAssetsController_Test {

    static testMethod void myUnitTest() {
        
		Account acc = TestClassUtility.createAccount(true);
		Apexpages.currentPage().getParameters().put('accountId', acc.Id);
		
		Contact cnt = TestClassUtility.createContact(1, false)[0];
		cnt.AccountId = acc.Id;
		insert cnt;
		
		Asset__c ast = TestClassUtility.createAssets(1, false)[0];
		ast.Siras_Purchase_Date__c = date.today();
		ast.Serial_Number__c = '213123';
		ast.Retailer__c = 'RET01';
		ast.Retailer_ID__c = '1122';
		ast.Release_Date__c = date.today();
		ast.Purchase_Date__c = date.today();
		ast.Model_Number__c = 'MOD121';
		insert ast; 
		
		Account personAcc = [select PersonContactId from Account where Id = :acc.Id];
		
		Consumer_Asset__c ca = new Consumer_Asset__c();
		ca.Asset__c = ast.Id;
		ca.Consumer__c = cnt.Id;
		insert ca;
		
		Integration__c integration = TestClassUtility.createIntegrationRec(true); 
		
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new HTTPMockCyberSource());
		
		RefreshAssetsController ctrl = new RefreshAssetsController();
		ctrl.updateAssetRecords();
		
		Asset__c currentAsset = [select Warranty_Details__c from Asset__c where Id = :ast.Id];
		system.assertNotEquals(currentAsset.Warranty_Details__c, null, 'Warranty detail should be udpated');
		
		Test.stopTest();
    }
    global class HTTPMockCyberSource implements HttpCalloutMock  {
	   global HTTPResponse respond(HTTPRequest req) {
      		System.assertEquals('https://qarouter.siras.com/edes/', req.getEndpoint());
	        System.assertEquals('POST', req.getMethod());
	        HttpResponse res = new HttpResponse();
	        //res.setHeader('Content-Type', 'application/json');
	        String body = getResponse();
	        res.setBody(body);
	        res.setStatusCode(200);
	        return res;
	  }
	  global String getResponse(){
	  	String response = '<?xml version=\'1.0\'?>';
	
		response +=			'			<Serial-Number-Inquiry-Response version=\'3.0\'>';
		response +=			'					<Response-Header>';
		response +=			'					   <Response-Status>100</Response-Status>';
		response +=			'					   <Response-Status-Description>Request Processed</Response-Status-Description>';
		response +=			'					   <Partner-ID>scea-crm-edes2</Partner-ID>';
		response +=			'					   <Response-Email-Address Type="" />';
		response +=			'					   <Confirmation-Type>2</Confirmation-Type>';
		response +=			'					   <Transmission-ID />';
		response +=			'					   <Location-ID />';
		response +=			'					   <Creation-Date>20130809</Creation-Date>';
		response +=			'					   <Creation-Time>110301</Creation-Time>';
		response +=			'					   <Creation-Time-Zone>GMT-07:00</Creation-Time-Zone>';
		response +=			'					</Response-Header>';
							
		response +=			'					<Product-Information>';
		response +=			'					   <Reason-Code />';
		response +=			'					   <Reason-Description />';
		response +=			'					   <SiRAS-Inquiry-ID>7980682</SiRAS-Inquiry-ID>';
		response +=			'					   <UPC />';
		response +=			'					   <Serial-Number>1232145425</Serial-Number>';
		response +=			'					   <Item-Description />';
		response +=			'					   <Item-Number />';
		response +=			'					   <Brand-Name>SONY PLAYSTATION</Brand-Name>';
		response +=			'					   <Brand-ID>9</Brand-ID>';
		response +=			'					   <Sold-By-Retailer />';
		response +=			'					   <Sold-By-Retailer-ID />';
		response +=			'					   <Sold-Date />';
		response +=			'					   <Manufacturer-Warranty-Determination>12</Manufacturer-Warranty-Determination>';
		response +=			'					   <Manufacturer-Warranty-Parts-Determination>12</Manufacturer-Warranty-Parts-Determination>';
		response +=			'				   <Manufacturer-Warranty-Labor-Determination>12</Manufacturer-Warranty-Labor-Determination>';
		response +=			'				</Product-Information>';
							
		response +=			'					<Response-Trailer>';
		response +=			'					   <Expected-Count>1</Expected-Count>';
		response +=			'					   <Actual-Count>1</Actual-Count>';
		response +=			'					</Response-Trailer>';
		response +=			'					</Serial-Number-Inquiry-Response>';
	  	
	  	
	  	return response;
	  }
    }
}