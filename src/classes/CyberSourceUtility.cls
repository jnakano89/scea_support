/******************************************************************************
Class         : CyberSourceUtility
Description   : Class for utility methods for Cyber Source
Developed by  : Urminder Vohra(JDC)
Date          : Sep 4, 2013

Updates       :
******************************************************************************/
public class CyberSourceUtility {
  public static String MERCHANT_ID;
  public static String TYPE;
  public Static String SUB_TYPE;
  public Static String VERSION_NUMBER;
  public Static String END_POINT;
  public Static String USERNAME;
  public Static String PASSWORD;
  
  public static String RMA_USERNAME;
  public static String RMA_PASSWORD;
  public static String RMA_MERCHANT_ID;
  
  public static final Set<String> DATA_ERROR = new Set<String>{'DMISSINGFIELD','DINVALIDDATA','DINVALIDCARD'};
  public static final Set<String> SYSTEM_ERROR = new Set<String>{'ESYSTEM','ETIMEOUT'};
  public static final String DCALL = 'DCALL';
  public static final String DAVSNO = 'DAVSNO';
  public static final String DCV = 'DCV';
  
  static {
  
  	Cybersource__c settings = Cybersource__c.getOrgDefaults();
  	
  	MERCHANT_ID = settings.Merchant_ID__c;
  	USERNAME = settings.Username__c;
  	PASSWORD = settings.Password__c;
  	
  	RMA_MERCHANT_ID = settings.RMA_Merchant__c;
  	RMA_PASSWORD = settings.RMA_Password__c;
  	RMA_USERNAME = settings.RMA_Username__c;
  	
  	END_POINT = settings.Payment_Response_URL__c;
    VERSION_NUMBER = settings.Payment_Response_Version_Number__c;
    TYPE = settings.Payment_Response_Type__c;
    SUB_TYPE = settings.Payment_Response_Sub_Type__c; 	
  }

  public static TaxWrapper taxCalculation(String caseId, decimal unitPrice) {
	  	Case cs = [select CaseNumber, Contact.FirstName, Contact.LastName, ship_To__r.Address_Line_1__c, ship_To__r.Address_line_2__c, Ship_To__r.City__c, Ship_To__r.Country__c, 
	  							SCEA_Product__r.List_Price__c, SCEA_Product__r.Name, SCEA_Product__r.SKU__c, ship_To__r.X2_Letter_Country_Code__c,
	  							Ship_To__r.State__c, Walk_In_Flag__c, Ship_To__r.Postal_Code__c from Case where Id = :caseId];
	  		
	  		return taxCalculation(cs, unitPrice);
  }
  
  public static TaxWrapper taxCalculation(Case objCase, decimal unitPrice){
	  TaxWrapper txtWpr = new TaxWrapper();
	  Cybersource__c cyberSource = [Select RMA_Merchant__c From Cybersource__c LIMIT 1];
	  String merchantId = '';
	  if(cyberSource != null) {
	        merchantId = cyberSource.RMA_Merchant__c;
	  }
      // Call 'tax Calculation' web service 
      GenericWSHandler handler = new GenericWSHandler('TaxCalculation');

      System.debug('============Case Object =======' + objCase);
      GenericRequestBuilder obj = new GenericRequestBuilder.defGenericRequestBuilder(new map<String, object> {'merchantId' => merchantId, 'caseObj' => objCase, 'unitPrice' => unitPrice});
      //creditObj.RefOrderNumber
      String taxResponse = handler.callStartSession(obj);
      if(!String.isEmpty(taxResponse)){
          Dom.Document document = new Dom.Document();
          document.load(taxResponse);
          String placementOrderObj = DocumentMappingUtilities.executeObjectMap(document, 'TaxCalculation');
          system.debug('************placementOrderObj'+placementOrderObj);
          if(!String.IsEmpty(taxResponse)){
              
        	  txtWpr = (TaxWrapper) JSON.deserialize(placementOrderObj,TaxWrapper.class);
              system.debug('*******resultObj ::'+txtWpr);
          }
      }
      //GenericObject.GenericAttributedObj
	  return txtWpr;
  }

  public static Payment__c getTransaction_New(String merchantReferenceNumber, String targetDate, String requestID, String paymentType) {

	Payment__c payment; 

	
	try{
 
	  	String body = getBody_New(merchantReferenceNumber, targetDate, requestID, paymentType);
	  	System.debug('=====body========' + body);
	  	// Sending HTTP Request to get Response.
	  	HttpRequest req = new HttpRequest();
	    HttpResponse res = new HttpResponse();
	    Http http = new Http();
	    req.setEndpoint(END_POINT);
	    Blob headerValue;
	    if(paymentType == 'RMA') {
	    	headerValue = Blob.valueOf(RMA_USERNAME + ':' + RMA_PASSWORD);
	    	system.debug('***********RMA_USERNAME:  '+RMA_USERNAME);
	    	system.debug('***********RMA_PASSWORD:  '+RMA_PASSWORD);
	    } else {
	    	headerValue = Blob.valueOf(USERNAME + ':' + PASSWORD);
	    	system.debug('***********USERNAME:  '+  USERNAME);
	    	system.debug('***********PASSWORD:  '+ PASSWORD);
	    }
		
		String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
		req.setHeader('Authorization', authorizationHeader);
	    req.setMethod('POST');
	    req.setBody(body);
	    
	    system.debug('************Payment Response REQ:  '+req);
	    
	    system.debug('************Payment Response REQ BODY:  '+req.getBody());
	    
	    system.debug('************Payment Response REQ ENDPOINT:  '+req.getEndpoint());
	    
	    system.debug('************Payment Response REQ ToString:  '+req.toString());
	    system.debug('************Payment Response REQ Header:  '+req.getHeader('Authorization'));
	    //system.debug('************Payment Response REQ Header Decoded:  '+encodingutil.base64Decode(req.getHeader('Authorization')).toString());
	    
	    req.setTimeout(120000);
	    
	    
	    
	    res = http.send(req);
	    
	    system.debug('************Payment Response RES:  '+res);
	    
	    System.debug('==response body == ' + res.getBody());
		
		payment = doParse(res.getBody());
	
	}catch(Exception e){
	
		IntegrationAlert.addException('Get CyberSource Transaction Details', 'Reference Number='+merchantReferenceNumber, e);
	
	}
    
    
    return payment;
  }

  private static Payment__c doParse(String dataString) {
  	//Response rspns = new Response();
  	Payment__c payment = new Payment__c();
  	DOM.Document doc = new DOM.Document();
  	doc.load(dataString);
  	DOM.XMLNode root = doc.getRootElement();
  	String RequestID = '';
  	Boolean isSettled = false;
  	boolean isFirstRecord = false;
  	// Iteration over XML Nodes
  	for (Dom.XMLNode requests: root.getChildElements()) {
  	  for (Dom.XMLNode req: requests.getChildElements()) {
  	   payment.Transaction_Number__c = req.getAttributeValue('TransactionReferenceNumber','');
  	   payment.Transaction_Date__c = parseDate(req.getAttributeValue('RequestDate',''));
  	   payment.Token__c = req.getAttributeValue('SubscriptionID','');
  	   RequestID = req.getAttributeValue('RequestID','');
  	   for (Dom.XMLNode child: req.getChildElements()) {
  	   	if(child.getName().trim() == 'BillTo') {
  	   	  for (Dom.XMLNode innerChild: child.getChildElements()) {
  	   	  	if(innerChild.getName().trim() == 'FirstName') payment.Billing_First_Name__c = innerChild.getText().trim();
  	   	  	if(innerChild.getName().trim() == 'LastName') payment.Billing_Last_Name__c = innerChild.getText().trim();
  	   	  	if(innerChild.getName().trim() == 'Address1') payment.Billing_Address__c = innerChild.getText().trim();
  	   	  	if(innerChild.getName().trim() == 'Address2') payment.Billing_Address__c  += ' ' + innerChild.getText().trim();
  	   	  	if(innerChild.getName().trim() == 'City') 	payment.Billing_City__c = innerChild.getText().trim();
  	   	  	if(innerChild.getName().trim() == 'State') payment.Billing_State__c = innerChild.getText().trim();
  	   	  	if(innerChild.getName().trim() == 'Zip') payment.Billing_Zip__c = innerChild.getText().trim();
  	   	  	if(innerChild.getName().trim() == 'Email') payment.Billing_Email__c= innerChild.getText().trim();
  	   	  	if(innerChild.getName().trim() == 'Country') payment.Billing_Country__c= innerChild.getText().trim();
  	   	  	//if(innerChild.getName().trim() == 'Phone') rspns.billTo.phone = innerChild.getText().trim();
  	   	  }   	
  	   	} else if(child.getName().trim() == 'PaymentData') {
  	   	   for (Dom.XMLNode innerChild: child.getChildElements()) {
  	   	   	 //if(innerChild.getName().trim() == 'PaymentRequestID') rspns.paymentData.PaymentRequestID = innerChild.getText().trim();
  	   	   	 //if(innerChild.getName().trim() == 'PaymentProcessor') rspns.paymentData.PaymentProcessor = innerChild.getText().trim();
  	   	   	 //if(innerChild.getName().trim() == 'Amount') rspns.paymentData.Amount = innerChild.getText().trim();
  	   	   	 //if(innerChild.getName().trim() == 'CurrencyCode') rspns.paymentData.CurrencyCode = innerChild.getText().trim();
  	   	   	 //if(innerChild.getName().trim() == 'TotalTaxAmount') rspns.paymentData.TotalTaxAmount = innerChild.getText().trim();
  	   	   	 if(innerChild.getName().trim() == 'AuthorizationCode') payment.Auth_Code__c = innerChild.getText().trim();
  	   	   	 if(innerChild.getName().trim() == 'AVSResult') payment.AVS__c = innerChild.getText().trim();
  	   	   	 //if(innerChild.getName().trim() == 'AVSResultMapped') rspns.paymentData.AVSResultMapped = innerChild.getText().trim();
  	   	   	 if(innerChild.getName().trim() == 'RequestedAmount') payment.Amount__c = Double.valueOf(innerChild.getText().trim());
  	   	   	 if(innerChild.getName().trim() == 'RequestedAmountCurrencyCode') payment.Currency_Code__c = innerChild.getText().trim();
  	   	   }	
  	   	} else if(child.getName().trim() == 'PaymentMethod'){
  	   	  	for (Dom.XMLNode innerChild: child.getChildElements()) {
  	   	  	  if(innerChild.getName().trim() == 'Card'){
  	   	  	    for (Dom.XMLNode grandChild: innerchild.getChildElements()) {
  	   	  	  	 if(grandChild.getName().trim() == 'AccountSuffix') payment.Credit_Card_Number__c = grandChild.getText().trim();
  	   	  	  	 //if(grandChild.getName().trim() == 'ExpirationMonth') payment.Expiry_Month__c = grandChild.getText().trim();
  	   	  	  	 //if(grandChild.getName().trim() == 'ExpirationYear') payment.Expiry_Year__c = grandChild.getText().trim();
  	   	  	  	 if(grandChild.getName().trim() == 'CardType') payment.Payment_Type__c = grandChild.getText().trim();	 
  	   	  	    }
  	   	  	  }	
  	   	  	}
  	   	} else if(child.getName().trim() == 'ApplicationReplies') {
  	   		for (Dom.XMLNode innerChild: child.getChildElements()) {
  	   		  //ApplicationReply ar = new ApplicationReply();
  	   		  if(innerChild.getName().trim() == 'ApplicationReply') {
  	   		  	
  	   		  	 if(innerChild.getAttributeValue('Name','') == 'ics_auth') {
                  //isFirstRecord = true;
                }
                if(innerChild.getAttributeValue('Name','') == 'ics_bill') {
                  isSettled = true;
                  isFirstRecord = true;
                }
                
                
  	   		    for (Dom.XMLNode grandChild: innerchild.getChildElements()) {
  	   		      if(grandChild.getName().trim() == 'RCode') payment.Cybersource_Response_Code__c = Double.valueOf(grandChild.getText().trim() == '' ? '0' : grandChild.getText().trim() );
  	   		      if(grandChild.getName().trim() == 'RFlag') payment.Cybersource_Response_Flag__c = grandChild.getText().trim();
  	   		      if(grandChild.getName().trim() == 'RMsg') payment.CyberSource_Response__c = grandChild.getText().trim();
  	   		    }
  	   		    //rspns.applicationReplies.add(ar);   		
  	   		  }	
  	   		  if(isFirstRecord) break;
  	   		}    	
  	   	}
  	   }
  	   
  	   if(isSettled && payment.Token__c != null && payment.Token__c != '' && RequestID != null && RequestID != '' && RequestID	!= payment.Token__c){
  	     isSettled = true;
  	   } else {
  	   	isSettled = false;
  	   }
  	   if(isFirstRecord) break;
  	  }	
  	  
  	  if(isFirstRecord) break;
  	}
  	if(payment.Cybersource_Response_Code__c == 1) {
  		payment.Payment_Status__c = 'Charged';
  		payment.Authorization_Date__c = Date.today();
  	} else if(DATA_ERROR.contains(payment.Cybersource_Response_Flag__c)){
  		payment.Payment_Status__c = 'Data Error';
  	} else if(SYSTEM_ERROR.contains(payment.Cybersource_Response_Flag__c)) {
  		payment.Payment_Status__c = 'System Error';
  	} else if(payment.Payment_Status__c == DCALL) {
  		payment.Payment_Status__c = 'Need Verbal Authorization';
  	} else if(payment.Payment_Status__c == DAVSNO) {
  		payment.Payment_Status__c = 'AVS Failed';
  	} else if(payment.Payment_Status__c == DCV) {
  		payment.Payment_Status__c = 'Card Verification Declined';
  	} else {
  		payment.Payment_Status__c = 'Declined';
  	}
  	
  	if(isSettled && payment.Payment_Status__c == 'Charged'){
  		payment.Payment_Status__c = 'Settled';
  	}
  	
  	payment.Payment_Method__c = 'Credit Card';
  	payment.Name_on_Card__c = payment.Billing_First_Name__c + ' ' + payment.Billing_Last_Name__c;
  	payment.Credit_Card_Type__c = payment.Payment_Type__c;
  	
  	System.Debug('>>>>>>> Payment  >>>>>>>' +Payment);
  	
  	return payment;
  }
  
  private static String getBody(String merchantReferenceNumber, String targetDate, String requestID) {
  	String body = '';
  	body += 'merchantID=' + MERCHANT_ID + '&';
  	body += 'type=' + TYPE + '&';
  	body += 'subtype=' + SUB_TYPE + '&' ; 
  	body += 'requestID=' + requestID + '&' ; 
  	body += 'merchantReferenceNumber=' + merchantReferenceNumber + '&' ; 
  	body += 'targetDate=' + targetDate + '&' ; 
  	body += 'versionNumber=' + VERSION_NUMBER;
  	return body;
  }
  
  private static String getBody_New(String merchantReferenceNumber, String targetDate, String requestID, String paymentType) {
  	String body = '';
  	if(paymentType == 'RMA') {
  		body += 'merchantID=' + RMA_MERCHANT_ID + '&';
  	} else {
  		body += 'merchantID=' + MERCHANT_ID + '&';
  	} 
  	body += 'type=' + TYPE + '&';
  	body += 'subtype=' + SUB_TYPE + '&' ; 
  	body += 'requestID=' + requestID + '&' ; 
  	body += 'merchantReferenceNumber=' + merchantReferenceNumber + '&' ; 
  	body += 'targetDate=' + targetDate + '&' ; 
  	body += 'versionNumber=' + VERSION_NUMBER;
  	return body;
  }
  
  public static Date parseDate(String dateStr) {
  	try{
  		list<String> formattedDate = dateStr.split('T')[0].split('-');
  		if(formattedDate.size() == 3) { 
  		  Date dt = Date.parse(formattedDate[1] + '/' + formattedDate[2] + '/' + formattedDate[0]);
  		  return dt;
  		}
  	} catch(Exception ex) {
  		System.debug('=========INVALID DATE TIME== : ' + ex.getMessage());
  	}
   return null;
  }
  
  public static Set<String> getNexusStates() {
  	Set<String> nexusStates = new Set<String>();
  	for(Tax_Nexus__c tn : Tax_Nexus__c.getAll().values()) {
  		if(tn.State_Code__c <> null) {
  			nexusStates.add(tn.State_Code__c);
  		}
  	}
  	return nexusStates;
  }
  
  public static String getNexusStatesString() {
  	String states = '';
  	for(String tn : getNexusStates()) {
  		states += tn + ',';
  	}
  	states = states.indexOf(',') == -1 ? states : states.substring(0,states.lastIndexOf(','));
  	return states;
  }
  
  public class TaxWrapper {
  	public String created{get;set;}
  	public String merchantReferenceCode{get;set;}
  	public String requestID{get;set;}
  	public String decision{get;set;}
  	public String reasonCode{get;set;}
  	public String requestToken{get;set;}
  	public String taxReply{get;set;}
  	public String grandTotalAmount{get;set;}
  	public String totalCityTaxAmount{get;set;} 
  	public String city{get;set;} 
  	public String totalCountyTaxAmount{get;set;} 
  	public String totalDistrictTaxAmount{get;set;} 
  	public String totalStateTaxAmount{get;set;} 
  	public String state{get;set;} 
  	public String totalTaxAmount{get;set;} 
  	public String postalCode{get;set;} 
  	public String cityTaxAmount{get;set;} 
  	public String countyTaxAmount{get;set;}
  	public String districtTaxAmount{get;set;} 
  	public String stateTaxAmount{get;set;} 
  	public String totalItemTaxAmount{get;set;}
  	
  	public TaxWrapper() {}
  }
	/*
	 *	Failure Response: Email Alert
	 */
  public static void  failureEmailAlert(String subject, String body) {
		if(!String.isEmpty(body)) {
			// Create dummy contact and after mail send delete it
			//Contact con = new Contact(FirstName = 'Test',LastName = 'Test',Email = 'scea_rhq_cs_tech_team@playstation.sony.com');
			try{
				system.debug('***Failure Email :Start');
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
				mail.setSubject(subject);
				mail.setPlainTextBody(body);
				mail.setToAddresses(new String[]{System.Label.Email_For_Sony_Integration_Error});
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
				system.debug('***Failure Email :Sent');
			}
			catch(Exception Ex){
	
			}
			//Database.rollback(sp);  	
		}
	}

	//Send email for CyberSource success.
	public static void sendSuccessEmail(Id ContactId,Id whatId, String emailTemplateName){
	    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	    mail.setTargetObjectId(ContactId);
	    //mail.setSenderDisplayName('SCEA');
	    EmailTemplate et=[Select id from EmailTemplate where DeveloperName=:emailTemplateName];
	    mail.setTemplateId(et.id);
	    mail.setWhatId(whatId);	    
	    Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});   
	} 

	/*
	 *	Failure Response: Email Alert
	 */
	public static void  failureEmailAlert(WebRMAServiceRequestProcess.ServiceRequestCreateCaseResponse response, WebRMAServiceRequestProcess.ServiceRequestInput request, String errorDetail) {
		failureEmailAlert(response, request, errorDetail, null);
	}
	public static void  failureEmailAlert(WebRMAServiceRequestProcess.ServiceRequestCreateCaseResponse response, WebRMAServiceRequestProcess.ServiceRequestInput request, String errorDetail, Exception Ex) {
		if(response==null){
			response = new WebRMAServiceRequestProcess.ServiceRequestCreateCaseResponse();
		}
		response.failure = new WebRMAServiceRequestProcess.ServiceRequestFailureResponse();
		if(String.isEmpty(errorDetail)){
			response.failure.message = 'Unable to complete WebRMA process/transaction ';
		}else{
			response.failure.message = errorDetail;
		}
		errorDetail = response.failure.message;
		if(Ex!=null){
			errorDetail = errorDetail + ' due to'+ '\n\nError Details :'+Ex.getMessage()+'\n\n StackTrace :'+ex.getStackTraceString();
		}
		// Failure Email Alert			
		errorDetail = errorDetail + '\n\n  Web Service API :WebRMAServiceRequestProcess.createServiceRequestCase\n'+
		'Input Data :'+((request!=null)?JSON.serialize(request):'Blank');
		//API URL :'+System.label.URL_For_WebRMA_API+
		response.failure.detailMessage = errorDetail;

		
		//Log Error-Message
		ExceptionHandler.logException(response.failure.detailMessage, '', '', '', '');
		//Send Failure email message
		if(!String.isEmpty(response.failure.detailMessage)) {
			CyberSourceUtility.failureEmailAlert('WebRMA Failure ', response.failure.detailMessage);
		}
		
	}	

	///Next -->
	public static void  failureEmailAlert(CreatePPPOrderService.WebServiceResponse response, CreatePPPOrderService.WebServiceInput request, String errorDetail) {
		failureEmailAlert(response, request, errorDetail, null);
	}
	public static void  failureEmailAlert(CreatePPPOrderService.WebServiceResponse response, CreatePPPOrderService.WebServiceInput request, String errorDetail, Exception Ex) {
		if(response==null){
			response = new CreatePPPOrderService.WebServiceResponse();
		}
		response.status = 'Failed';
		response.failResponse = new CreatePPPOrderService.WebServiceFailureResponse();
		if(String.isEmpty(errorDetail)){
			response.failResponse.message = 'Unable to complete WebPPP process/transaction ';
		}else{
			response.failResponse.message = errorDetail;
		}
		errorDetail = response.failResponse.message;
		if(Ex!=null){
			errorDetail = errorDetail + ' due to'+ '\n\nError Details :'+Ex.getMessage()+'\n\n StackTrace :'+ex.getStackTraceString();
		}
		// Failure Email Alert			
		errorDetail = errorDetail + '\n\n  Web Service API :CreatePPPOrderService.processPPPFlow\n'+
		'Input Data :'+((request!=null)?JSON.serialize(request):'Blank');
		//API URL :'+System.label.URL_For_WebRMA_API+
		response.failResponse.detailMessage = errorDetail;

		
		//Log Error-Message
		ExceptionHandler.logException(response.failResponse.detailMessage, '', '', '', '');
		//Send Failure email message
		if(!String.isEmpty(response.failResponse.detailMessage)) {
			CyberSourceUtility.failureEmailAlert('WebPPP Failure', response.failResponse.detailMessage);
		}
		
	}	

}