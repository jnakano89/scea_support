/*******************************************************************
Name  : TestSmartContactSearchExtention
Author: Appirio Offshore (Urminder Vohra)
Date  : July 19, 2011
 
*************************************************************************/
@isTest
private class TestSmartContactSearchExtention {

    static testMethod void myUnitTest() {
        ApexPages.StandardController sc;
        createData();
        Apexpages.currentPage().getParameters().put('language','English');
        SmartContactSearchExtention controller = new SmartContactSearchExtention(sc);
        controller.contactFirstNameToSearch = 'Test';
        controller.contactLastNameToSearch = 'TestLast';
       
        controller.performSearch();
        
        System.assertEquals(55, controller.searchCount);
        System.assertEquals(1,controller.showingFrom);
        System.assertEquals(20,controller.showingTo);
        System.assertEquals(3,controller.totalPage);
        System.assertEquals(true,controller.hasNext);
        System.assertEquals(false,controller.hasPrevious);
        
        controller.nextContactPage();
        System.assertEquals(21,controller.showingFrom);
        System.assertEquals(40,controller.showingTo);
        System.assertEquals(3,controller.totalPage);
        System.assertEquals(true,controller.hasNext);
        System.assertEquals(true,controller.hasPrevious);
        
        controller.previousContactPage();
        
        controller.requestedPage = '3';
        controller.requestedContactPage();
        System.assertEquals(41,controller.showingFrom);
        System.assertEquals(55,controller.showingTo);
        System.assertEquals(3,controller.totalPage);
        System.assertEquals(false,controller.hasNext);
        System.assertEquals(true,controller.hasPrevious);
        
        controller.sortData();
        controller.cancel();
        
        controller.requestedPage = '5';
        controller.requestedContactPage();
        controller.isInConsole = false;
        controller.selectedContactId = controller.contacts[0].Id;
        controller.redirectToContact();
        controller.createNewcontact();
        SmartContactSearchExtention ctrl = new SmartContactSearchExtention();
    }
    static void CreateData() {
      
      String rtId;
    	for(RecordType rt : [select Id from RecordType where SobjectType = 'Account' AND IsPersonType = true]) {
    		rtId = rt.Id;
    		break;
    	}
    	
        Account acc = new Account();
           acc.LastName = 'German Person' ;
           acc.BillingCountry = 'Germany';
           //acc.Region__c = 'Europe';
           //acc.Sub_Region__c = 'Germany';
           //acc.CurrencyIsoCode = 'EUR';  
       acc.RecordTypeId = rtId;
       insert acc;   
        
       list<Contact> cntList = new list<Contact>();
       for(Integer i=0;i<55;i++) {
           Contact cnt = new Contact();
           //cnt.AccountID = acc.id;
           cnt.LastName='TestLast'+ i;
           cnt.FirstName='Test'+  i;
           cnt.MailingCountry = 'United States';
           cnt.MailingState = 'CA';
           cnt.email = 'TestLast@appiro.com';
           cnt.Phone = '123123123';
           cnt.Preferred_Language__c = 'Eng';
           cntList.add(cnt);
       }
       
       insert cntList;
    }
}