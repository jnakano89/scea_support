public with sharing class UtilityGenericWebCallout {
    public static String getOAuthToken() {        
        if(currentUser!=null){
          return currentUser.OAuthAccessToken__c;
        }
        return null;
    }  
    
    public static User currentUser{
      get{
        if(currentUser == null){
          List<User> users = DescribeUtility.getsObjectById(new List<Id>{UserInfo.getUserId()});
          if(users.size()>0){
            currentUser = users[0];
          }else{
            return null;
          }
         }
         return currentUser;
      }
      private set{
        currentUser = value;
      }
    }    
    
     public static Web_Call_Setting__c createWebCallSetting(boolean isInsert, String name){
      Web_Call_Setting__c setting = new Web_Call_Setting__c(Name=name);
      if(isInsert){
        insert setting;
      }
      return setting;
    }
    public static Web_Call_Setting_key__c createWebCallSettingKey(boolean isInsert, map<string, object>keyval){
      Web_Call_Setting_key__c objSett = new Web_Call_Setting_key__c(Name='!!Order Refund!!');
      for(String key: keyval.keyset()){
        objSett.put(key, keyval.get(key));
      }
      if(isInsert){
        insert objSett;
      }
      return objSett;
    } 
    
    
    public static boolean IsCreditCardExpire(String expireYear, String expireMonth) {
				  Integer month = Integer.valueOf(expireMonth);
				  Integer year = Integer.valueOf(expireYear);
				  
				  Date currentDate = Date.today();
				  				  
				  if( year < currentDate.year() ) {
				  		return true;
				  }
				  
				  if((year == currentDate.year()) && (month < currentDate.month())) {
				  		return true;
				  }
				  
				  return false;
	}
    
}