/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class test_ExceptionHandler {

    static testMethod void testExceptionHandlerStandard() {
    	
        try {
        	   system.Apexbaseclass baseclass = new System.Apexbaseclass();
        	   Account a = new Account();
           insert a;
        }
        catch (Exception ex)
        {
        	   system.assertNotEquals(ex.getTypeName(), '');
        	   ExceptionHandler.logException(ex);
        }
    }
    
    static testMethod void testExceptionHandlerCustom() {
    	   try { 
            String s = '';
            for (integer i = 1; i <= 33000; i++)
            {
            	   s += 'A';
            }
            testException testex = new testException();
            testex.setMessage(s);
            throw testex;
        }
        catch (Exception ex)
        {
            system.assertNotEquals(ex.getMessage(), '');
            ExceptionHandler.logException(ex);
        }
    }
    static testMethod void testFieldLengthOverflow() {
        List<String> fieldnames = new List<String>();
        String fieldname = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        for (integer i = 1; i <= 2000; i++)
        {
        	   fieldnames.add(fieldname);
        }
       system.assertNotEquals(ExceptionHandler.getFieldNames(fieldnames), '');
    }
    static testMethod void testOriginationMethods() {
    	   String stacktrace = 'Class.TestClass: Test';
    	   system.assertEquals(ExceptionHandler.getClassOrigination(stacktrace), 'Class.TestClass');
    	   stacktrace = 'Trigger.TestTrigger: Test';
    	   system.assertEquals(ExceptionHandler.getTriggerOrigination(stacktrace), 'Trigger.TestTrigger');
    	   stacktrace = '';
    	   system.assertEquals(ExceptionHandler.getClassOrigination(stacktrace), null);
    	   system.assertEquals(ExceptionHandler.getTriggerOrigination(stacktrace), null);
    	   
    }
    
    public class testException extends Exception { }
}