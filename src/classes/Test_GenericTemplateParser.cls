/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 */
@isTest(seeAllData=true)
private class Test_GenericTemplateParser {

    static testMethod void testTemplateParser() {
        
         string sampleMail= 'Contact Name is {!Contact.LastName}, User  Name is  {!User.FirstName}{!User.LastName}';
        // Prepare data
        User user = [select FirstName, LastName from user limit 1];
        
        map<string, sObject>mapObject_Name_Id = new map<string, sObject>();
        
        Test.startTest();
        GenericTemplateParser tempParser= new GenericTemplateParser(sampleMail);
        //Verify tokens size
        System.assert(tempParser.tokens.size() == 3);        
        mapObject_Name_Id.put('User',user); 
        String  temp= tempParser.getParsedContent(mapObject_Name_Id);
        //Verify the parsed template        
        Test.stopTest();               
    }
}