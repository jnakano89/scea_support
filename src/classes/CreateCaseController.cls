//**************************************************************************/
// (c) 2013 Appirio, Inc.
//
// Description: Controller Class For VF Page Create Case.
//
// Original August 27,2013: Poonam Varyani(JDC)
///**************************************************************************/
public without sharing class CreateCaseController {
    
     String contactId;
     String assetId;
     String caseType;
    
    //constructor
    public CreateCaseController(){
        contactId = Apexpages.currentPage().getParameters().get('contactId');
        assetId = Apexpages.currentPage().getParameters().get('assetId');
        caseType = Apexpages.currentPage().getParameters().get('type');
    }
   
    //method to create case with record type = Purchase
    public Pagereference purchaseProtectionPlan(){
        //create case
        Case caseNew = new Case();
        caseNew.Asset__c = assetId;
        caseNew.ContactId = contactId;
        System.debug('>>>>>>>>>>caseType='+caseType);
        if(caseType <> null && caseType != ''){
        	caseNew.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(caseType).getRecordTypeId();
        } else {
        	Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Invalid Case Type is Used in parameter'));
        	return null;
        }
        
        try{
        	insert caseNew;
        	
        	if( caseNew != null) {
            	PageReference casePage = new ApexPages.StandardController(caseNew).view();
            	
            	/*
            	PageReference casePage = null; 
            
                String appId;
                String appName = Label.Service_Console_Name;
                
                for(AppMenuItem app : [select Id from AppMenuItem where Name = :appName]) {
                    appId = app.Id;
                }//end-for
                
                casePage = new PageReference('/console?tsid='+ appId);
                casePage.setAnchor('%2F' + caseNew.Id);
         		*/

            	
            	casePage.setRedirect(true);
                return casePage;
        	}
        }
        catch(Exception Ex){
    			system.debug('Exception occurred during case creation');
    		}
        
        return null;
    }    
}