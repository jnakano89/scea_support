/**
 * Name     : DefHttpCallout_Test
 * Usage    : Unit test class for DefHttpCallout class
 *            
 *  
 * Created By      : Igor Androsov
 * Modified By     : 
 * Modified Date   : October 11, 2012
 * 
 * (c) 2012 Appirio, Inc.
 */
@isTest
private class DefHttpCallout_Test {

    public static testMethod void testDefHttpCallout() {
    	Test.startTest();
        DefHttpCallout dht = new DefHttpCallout();
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://www.salesforce.com');
        req.setMethod('GET');
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('<test>Hello response</test>'));
        DefHttpResponse resp = (DefHttpResponse)dht.send(req);
        
        //DefHttpResponse code -coverage
        String body = resp.getBody();
        String[] keys = resp.getHeaderKeys();
        
        String st = resp.getStatus();
        Integer ist = resp.getStatusCode();
        
        String str = resp.toStrings();
        
        resp.getBodyDocument();
        resp.getBodyDocument();
        resp.getHeader('test');
        //resp.getXmlStreamReader();
      Test.stopTest();  
    }
}