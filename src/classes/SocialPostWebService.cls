/***************************************************************************************************************************************************************************
Developed By : Salesforce Professional Services

Updates:
        11/19/2014 : Aaron Briggs : SMS-725 : Updated Case Creation Logic to Set Record Type to Social and Owner to Social Support Queue
        12/01/2014 : Leena Mandadapu : SMS-725 : Added code to setup Social_content__c field on Case
***************************************************************************************************************************************************************************/
global class SocialPostWebService {
	//AB : 11/19/14 : Globally Instatiate Record Type and Group IDs
	public static final String RT_CASE_SOCIAL_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Social').getRecordTypeId();
	public static final Group OWNER_SOCIAL = [SELECT Id FROM Group WHERE Name = 'Social Support' AND Type = 'Queue' ][0];
	
	//constructor
	public SocialPostWebService(){}
		    
    webService static String createSocialPostCases(List<String> recordList) {
    	String str = createSocialCases(recordList);
        return str;
    }
    
    webService static String createSocialPostCase(String recordId) {    	         
        String str = createSocialCase(recordId);
        return str;
    }
    
    public static String createSocialCases(List<String> recordList){
    	List<List<string>> resultList = new List<List<string>>();
        List<string> idMap = new List<string>();
        for(String sp1 : recordList){        	
        	List<SocialPost> spList = [select OwnerId, Handle, MessageType, PostTags, ParentId, PersonaId, Name, ReplyToId, Content from SocialPost 
        		where Id =:sp1 order by LastModifiedDate ASC Limit 1];        	
        	if(spList.size() > 0){        	        		
        		String tempId = createCaseFromSocial(spList.get(0));
        		List<string> tempList = new List<string>{spList.get(0).Name, tempId};
        		resultList.add(tempList);        	
        		idMap.add(tempId);
        	}                                	
        }
        system.debug('============================> resultList: ' + resultList);
        system.debug('============================> idMap: ' + idMap);
        return createMessage(resultList, idMap);     
    }
    
    public static String createSocialCase(String recordId){    
    	List<List<string>> resultList = new List<List<string>>();
   		List<string> idMap = new List<string>();	
    	List<SocialPost> spList = [select OwnerId, Handle, MessageType, PostTags, ParentId, PersonaId, Name, ReplyToId, Content from SocialPost 
        	where Id =:recordId order by LastModifiedDate ASC Limit 1];        	
        if(spList.size() > 0){        	        	
        	String tempId = createCaseFromSocial(spList.get(0));
        	List<string> tempList = new List<string>{spList.get(0).Name, tempId};
        	resultList.add(tempList);        	
        	idMap.add(tempId);
        }                        
        return createMessage(resultList, idMap);
    }
    
    private static String createMessage(List<List<string>> messageList, List<string> caseIdMap){
    	List<Case> caseNumberList = [select caseNumber, Id from case where id in : caseIdMap];
    	String message1 = '';
    	if(caseNumberList.size()>0){
    		message1 = message1 + 'Case Creation Result: \n' ;
    		for(Case tempCase : caseNumberList){
    			for(Integer j = 0; j < messageList.size(); j++){
    				if(messageList.get(j).get(1).equalsIgnoreCase(tempCase.Id)){    					
    					message1 = message1 + 'Social Post Name: ' + messageList.get(j).get(0) +'\nCase Number : ' + tempCase.CaseNumber + '\n\n' ;    					
    					messageList.remove(j);
    				}
    			}    			
    		}
    	}
    	if(messageList.size()>0){
    		message1 = message1 + 'Criteria Not Satisfied For: \n' ;
    		for(List<string> tempStr : messageList){
    			message1 = message1 + 'Social Post Name: ' + tempStr.get(0) + '\n';
    		}
    	}
    	return message1;
    }
    
    private static String createCaseFromSocial(SocialPost sp){
    	Case newCase = new Case();
    	SonyInboundSocialPostHandlerImpl sisphi = new SonyInboundSocialPostHandlerImpl();

		List<SocialPersona> sPersonaList = new List<SocialPersona>();

		String refContactId;
		String refAccountId;

        system.debug('<<<<<<<<<<<<<<Social Post>>>>>>>>>>>>'+sp);
		if(String.isNotBlank(sp.PersonaId)){
			sPersonaList = [Select id, parentId, ExternalId, Provider, Name, RealName from SocialPersona where id = : sp.PersonaId order by LastModifiedDate ASC Limit 1];
			if(sPersonaList.size() > 0){
				//set reply id
				if(String.isNotBlank(sp.ReplyToId)){
					List <SocialPost> posts = [SELECT Id, ParentId, IsOutbound, PersonaId FROM SocialPost WHERE id = : sp.replyToId LIMIT 1];
					sp.ReplyTo = posts.get(0);
				}else{
					sp.ReplyTo = new SocialPost();
				}
				newCase = sisphi.buildParentCaseDefault(sp, sPersonaList.get(0));
				//AB : 11/19/14 : Set Case RecordType and OwnerId to Globally Instatiated Values
				newCase.RecordTypeId = RT_CASE_SOCIAL_ID;
				newCase.OwnerId = OWNER_SOCIAL.ID;	
				//LM : 12/01/2014 : set Case Social_Content__c field value to Social post Content 	
				system.debug('<<<<<<<Social Post Content>>>>>>>>>'+sp.Content);
				newCase.Social_Content__c = sp.Content;						
				newCase.Origin = 'Social Post';
				newCase.Social_Post_Tag__c=SocialPostManagement.getCategoriesFromPostTag(sp.PostTags);
				Boolean isContactType = SocialPostManagement.checkParentId(sPersonaList.get(0).parentId);
				if(isContactType != null){
					if(isContactType){
						refAccountId = SocialPostManagement.checkAndCreateAccount(sPersonaList.get(0).parentId);															
						refContactId = SocialPostManagement.checkAndCreateContact(refAccountId);																																																									
					}else{																														
						refContactId = SocialPostManagement.checkAndCreateContact(sPersonaList.get(0).parentId);
						refAccountId = SocialPostManagement.checkAndCreateAccount(refContactId);																											
					}													
				}else{
					//nothing available create both would not be a case
				}																										
				if(String.isNotBlank(refAccountId)){
					newCase.AccountId = refAccountId;
				}
				if(String.isNotBlank(refContactId)){																											
					newCase.ContactId = refContactId;
				}												
			}																						
		}
		try{							
			//insert newCase;
			system.debug('<<<<<<<<<<<<<<<<New Case to Insert - SocialPostWebService>>>>>>>>>>'+newCase);
            upsert newCase;	  															  																				
			sp.ParentId = newCase.id;
			update sp;
			SocialPersona spUpdate = sPersonaList.get(0);
			spUpdate.ParentId = refContactId;
			update spUpdate;
			return newCase.Id; 												
		}catch(Exception ex){
			System.debug('======ERROR Occured during insertSocialPostCase====' + ex.getMessage());
			return ex.getMessage();				
		}																										

    	return 'Criteria Not Satisfied';		    			
    }       	
}