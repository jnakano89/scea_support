/******************************************************************************
Class         : UpdateWarrantyScheduler
Description   : This class will retrieve a list of Staging_Siebel_PPP_Inbound__c (inbound) records where Processed_Date_Time__c is null (record hasn't been processed) yet.
				For each inbound record, other records must be updated.[T-188693]
Developed by  : Urminder Vohra (JDC)
Date          : Sep 25, 2013              
******************************************************************************/

global class UpdateWarrantyScheduler implements Database.Batchable<Sobject>, Schedulable {
	
	public static string ORDER_STATUS_ASSURANT_ENROLLED = 'Assurant Enrolled';
	public static string ORDER_STATUS_ASSURANT_CANCELLED = 'Assurant Cancelled';
	public static string CASE_STATUS_REFUND_COMPLETE = 'Refund Complete';
	
	global void execute(SchedulableContext sc) {
		UpdateWarrantyScheduler batch = new UpdateWarrantyScheduler();
		Database.executeBatch(batch);
	}
	  
	global Database.QueryLocator start(Database.BatchableContext BC) {
      return Database.getQueryLocator('Select Asset_Number__c, Order_Number__c, Processed_Date_Time__c, Asset_Status__c, Comments__c, ' + 
      									' Contract_Number__c, Warranty_Last_Update_Date__c, PPP_Start_Date__c,PPP_End_Date__c, ' +
      									' Refund_Amount__c, Order_Status__c '  + 
      									' from Staging_Siebel_PPP_Inbound__c ' + 
      									' where Processed_Date_Time__c = null');
 	}

 	global void execute(Database.BatchableContext BC, List<sObject> batchList) {
    	
        map<String, Staging_Siebel_PPP_Inbound__c> assetInboundMap	= 
        	new map<String, Staging_Siebel_PPP_Inbound__c>();
        	
        map<String, Staging_Siebel_PPP_Inbound__c> orderInboundMap	= 
        	new map<String, Staging_Siebel_PPP_Inbound__c>();

		map<String, Id> orderIdMap = new map<String, Id>();
        map<Id, Case> orderCaseMap = new map<Id, Case> ();
        
        list<Asset__c> assetList 	= new list<Asset__c>();
        list<Order__c> orderList 	= new list<Order__c>();
        list<Case> caseList 		= new list<Case>();
        
         
		list<Staging_Siebel_PPP_Inbound__c> inboundList = (list<Staging_Siebel_PPP_Inbound__c>)batchList;
        
        //creating mapping of Staging record to child records.
    	
    	
    	for(Staging_Siebel_PPP_Inbound__c inbound : inboundList){
    	 
    	  if(inbound.Asset_Number__c <> null && inbound.Asset_Number__c <> '') {
    	  	assetInboundMap.put(inbound.Asset_Number__c, inbound);
    	  }
    	  
    	  if(inbound.Order_Number__c <> null && inbound.Order_Number__c <> '') {
    	  	orderInboundMap.put(inbound.Order_Number__c, inbound);
    	  }	
    	  
    	}
    	
        if(!assetInboundMap.isEmpty()) { 
        	
          for(Asset__c ast : [select Name, PPP_Contract_Number__c, PPP_Last_Update_Date__c,
          							Asset_Status__c, PPP_Start_Date__c, PPP_End_Date__c   
          						from Asset__c 
          						where Name IN : assetInboundMap.keySet()]) {
          	
          	Staging_Siebel_PPP_Inbound__c inbound = assetInboundMap.get(ast.Name);
          	
          	boolean isUpdated = false;
          	
          	//Asset Status - if this field is not null, update PPP Status field on Asset record with new value.
          	
          	if(inbound.Order_Status__c != ORDER_STATUS_ASSURANT_CANCELLED){
          		
	          	if(inbound.Asset_Status__c <> null && inbound.Asset_Status__c != ast.Asset_Status__c) {
	          	  ast.Asset_Status__c = inbound.Asset_Status__c;
	          	  isUpdated = true;
	          	}
          	}
          	
          	//Contract Number - if this field is not null, update PPP Contract Number field on Asset record with new value.
          	if(inbound.Contract_Number__c <> null && inbound.Contract_Number__c <> ast.PPP_Contract_Number__c) {
          	  ast.PPP_Contract_Number__c = inbound.Contract_Number__c;
          	  isUpdated = true;
          	}
          	
          	//Warranty Last Update Date- if this field is not null, update PPP Last Update Date field on Asset record with new value.
          	if(inbound.Warranty_Last_Update_Date__c <> null && inbound.Warranty_Last_Update_Date__c <> ast.PPP_Last_Update_Date__c) {
          	  ast.PPP_Last_Update_Date__c = inbound.Warranty_Last_Update_Date__c;
          	  isUpdated = true;
          	}

          	if(inbound.PPP_Start_Date__c <> null && inbound.PPP_Start_Date__c <> ast.PPP_Start_Date__c) {
          	  ast.PPP_Start_Date__c = inbound.PPP_Start_Date__c;
          	  isUpdated = true;
          	}

          	if(inbound.PPP_End_Date__c <> null && inbound.PPP_End_Date__c <> ast.PPP_End_Date__c) {
          	  ast.PPP_End_Date__c = inbound.PPP_End_Date__c;
          	  isUpdated = true;
          	}

          	if(isUpdated) {
          	  assetList.add(ast);
          	}
          
          }
        }

        
        if(!orderInboundMap.isEmpty()) {
        	
          for(Order__c ordr : [select Name, Order_Status__c, Refund_Amount__c, Order_Comments__c     
          						from Order__c 
          						where Name IN : orderInboundMap.keySet()]) {
          							
          	Staging_Siebel_PPP_Inbound__c inbound = orderInboundMap.get(ordr.Name);
          	
          	orderIdMap.put(ordr.Name, ordr.Id);
          	
          	boolean isUpdated = false;
          	
          	//Order Status  - if this field is not null, update Order Status field on order record with new value.
          	if(inbound.Order_Status__c <> null && inbound.Order_Status__c <> ordr.Order_Status__c) {
          		ordr.Order_Status__c = inbound.Order_Status__c;
          		isUpdated = true;
          	}

          	//Refund Amount - if this field is not null, update Refund Amount field on case record with new value.
          	if(inbound.Refund_Amount__c <> null && inbound.Refund_Amount__c <> ordr.Refund_Amount__c) {
          		ordr.Refund_Amount__c = inbound.Refund_Amount__c;
          		isUpdated = true;
          	}

          	//Comments - if this field is not null, add a new comment to the Case record.
          	if(inbound.Comments__c <> null && inbound.Comments__c.trim() <> '<br>') {
          		ordr.Order_Comments__c = inbound.Comments__c;
          		isUpdated = true;
          	}
 
          	if(isUpdated) {
          	  orderList.add(ordr);
          	}
          }//end-for
          
        }//end-if
 

        if(!orderIdMap.isEmpty()) {
        	
          for(Case c : [Select Status, Order__c, Id 
          						From Case  
          						where Order__c IN : orderIdMap.values()]) {
  		
  				orderCaseMap.put(c.Order__c, c);					
          							
          }//end-for					
          
          
          for(String orderName : orderIdMap.keySet()){
          	
          	Id orderId = orderIdMap.get(orderName);
          	Case currentCase = 	orderCaseMap.get(orderId);
          	Staging_Siebel_PPP_Inbound__c inbound = orderInboundMap.get(orderName);
          	
          	boolean isUpdated = false;
          	
          	if(inbound.Order_Status__c == ORDER_STATUS_ASSURANT_CANCELLED){
          		currentCase.Status = CASE_STATUS_REFUND_COMPLETE;
          		isUpdated = true;
          	}
 
          	if(isUpdated) {
          	  caseList.add(currentCase);
          	}//end-if
          	
          }//end-for
          
        }//end-if

        
        if(!assetList.isEmpty()) {
        	update assetlist;
        }
        
        if(!orderList.isEmpty()) {
        	update orderList;
        }
        
        if(!caseList.isEmpty()) {
        	update caseList;
        }
        
    	for(Staging_Siebel_PPP_Inbound__c inbound : inboundList){
    		
    		inbound.Processed_Date_Time__c = system.now();	
        
        } 
        
        update inboundList;   
    	
    }

 	global void finish(Database.BatchableContext BC) {
 	}
}