/******************************************************************************
Class         : RefreshConsumerController_Test
Description   : Test class for RefreshConsumerController
Developed by  : Urminder Vohra(JDC)
Date          : Sep 3, 2013

Updates       :
******************************************************************************/
@isTest
global class RefreshConsumerController_Test {
    static Contact cnt;
    static final String MDM_ID = '1-2-3-4';
    static testMethod void myUnitTest() {
      createData();
      Test.startTest();
      Test.setMock(WebServiceMock.class, new WebServiceMockRefreshConsumer());
      
      Apexpages.currentPage().getParameters().put('mdmContactId',MDM_ID);
      RefreshConsumerController ctrl = new RefreshConsumerController();
      
      Sony_MiddlewareConsumerRefresh_Final.Account_mdm acc_mdm = new Sony_MiddlewareConsumerRefresh_Final.Account_mdm();
      acc_mdm.AccountStatus = 'Test';
      acc_mdm.AccountSuspendDate = '1/2/2013'; 
      acc_mdm.AccountSuspendReason = 'Test Reason';
      acc_mdm.CreatedDate = '9/2/2013';
      acc_mdm.MDMAccountRowId = '1-1-3-3';
      
      
      Sony_MiddlewareConsumerRefresh_Final.Address_mdm add_mdm = new Sony_MiddlewareConsumerRefresh_Final.Address_mdm();
      add_mdm.City = 'City';
      add_mdm.State = 'State';
      add_mdm.Country = 'Country';
      
      Sony_MiddlewareConsumerRefresh_Final.ListOfAddress_mdm ladd_mdm = new Sony_MiddlewareConsumerRefresh_Final.ListOfAddress_mdm();
      ladd_mdm.Address = new list<Sony_MiddlewareConsumerRefresh_Final.Address_mdm>{add_mdm}; 
      
      Sony_MiddlewareConsumerRefresh_Final.Contact_mdm cnt_mdm = new Sony_MiddlewareConsumerRefresh_Final.Contact_mdm();
      cnt_mdm.BirthDate = '1/1/2013';
      cnt_mdm.CRMPhone = '111111';
      cnt_mdm.FirstName = 'FirstName';
      cnt_mdm.LastName= 'LastName';
      cnt_mdm.MDMRowId = '1-2-3-4';
      cnt_mdm.PSNSignInId = 'psn@test.com';
      cnt_mdm.Account = new list<Sony_MiddlewareConsumerRefresh_Final.Account_mdm>{acc_mdm};
      cnt_mdm.ListOfAddress = ladd_mdm;
      
      
      
      ctrl.searchResultMember.listOfContacts = new list<Sony_MiddlewareConsumerRefresh_Final.Contact_mdm>();
      ctrl.searchResultMember.listOfContacts.add(cnt_mdm);   
      
      ctrl.refreshConsumer();
      ctrl.createAssetRecords();
      
      Address__c cntAddress;
      for(Address__c add : [select Id from Address__c where Consumer__c = :cnt.Id]) {
        cntAddress = add;
      }
      if(cntAddress <> null){
          System.assert(cntAddress <> null , 'Address should be inserted for Contact'); 
          
          PSN_Account__c psnAcc = [select MDM_Account_ID__c from PSN_Account__c where Consumer__c = :cnt.Id];
          System.assertEquals(psnAcc.MDM_Account_ID__c,  acc_mdm.MDMAccountRowId , 'PSN_Account Should be inserted for Contact');
      }
      Test.stopTest();       
    }
    
    static void createData() {
        OSB__c osbSettings = new OSB__c();
        osbSettings.Consumer_Refresh_Enabled__c = true;
        insert osbSettings;
        
        cnt = new Contact();
        cnt.LastName = 'lname';
        cnt.MDM_ID__c = MDM_ID;
        cnt.MDM_Account_ID__c = MDM_ID;
        insert cnt;
    }
    global class WebServiceMockCreateAddress implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,    
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
                Sony_MiddlewareUpdateInsertAddress2.PSUpdateInsertAddress_Output_element responseElm = 
                    new Sony_MiddlewareUpdateInsertAddress2.PSUpdateInsertAddress_Output_element();
                
                
                    Sony_MiddlewareUpdateInsertAddress2.Address add =  new Sony_MiddlewareUpdateInsertAddress2.Address();
                    add.city='test';
                    add.state='test';
                    
                    //prepare list of addresses
                    
                    Sony_MiddlewareUpdateInsertAddress2.ListOfAddress ladd = new Sony_MiddlewareUpdateInsertAddress2.ListOfAddress();
                    ladd.Address = new list<Sony_MiddlewareUpdateInsertAddress2.Address>{add};
                    
                    //Assing that address to contact
                    
                    Sony_MiddlewareUpdateInsertAddress2.Contact cont = new Sony_MiddlewareUpdateInsertAddress2.Contact();
                    cont.MDMRowId = '1-4EB-1';
                    cont.ListOfAddress = ladd;
                    
                    //Create list of contact
                    
                    Sony_MiddlewareUpdateInsertAddress2.ListOfContact lcnt = new Sony_MiddlewareUpdateInsertAddress2.ListOfContact();
                    lcnt.Contact = new list<Sony_MiddlewareUpdateInsertAddress2.Contact>{cont};
                  
                  responseElm.ListOfContact = lcnt;
                             
                  response.put('response_x', responseElm); 
               }
    }
     global class WebServiceMockRefreshConsumer implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,    
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
                Sony_MiddlewareConsumerRefresh_Final.PSConsumerRefresh_Output_element responseElm = 
                    new Sony_MiddlewareConsumerRefresh_Final.PSConsumerRefresh_Output_element();
                
                
                  Sony_MiddlewareConsumerRefresh_Final.Account_mdm acc_mdm = new Sony_MiddlewareConsumerRefresh_Final.Account_mdm();
                  acc_mdm.AccountStatus = 'Test';
                  acc_mdm.AccountSuspendDate = '1/2/2013'; 
                  acc_mdm.AccountSuspendReason = 'Test Reason';
                  acc_mdm.CreatedDate = '9/2/2013';
                  acc_mdm.MDMAccountRowId = '1-1-3-3';
                  
                  //code added by preetu
                  acc_mdm.PSPlusSubscriber = 'true';
                  acc_mdm.PSPlusStartDate = '2014-01-20 00:00:00';
                  acc_mdm.PSPlusStackedEndDate = '2014-01-20 00:00:00';
                  acc_mdm.PSPlusAutoRenewalFlag = 'True';
                  
                  Sony_MiddlewareConsumerRefresh_Final.Address_mdm add_mdm = new Sony_MiddlewareConsumerRefresh_Final.Address_mdm();
                  add_mdm.City = 'City';
                  add_mdm.State = 'State';
                  add_mdm.Country = 'Country';
                  
                  Sony_MiddlewareConsumerRefresh_Final.ListOfAddress_mdm ladd_mdm = new Sony_MiddlewareConsumerRefresh_Final.ListOfAddress_mdm();
                  ladd_mdm.Address = new list<Sony_MiddlewareConsumerRefresh_Final.Address_mdm>{add_mdm}; 
                  
                  Sony_MiddlewareConsumerRefresh_Final.Contact_mdm cnt_mdm = new Sony_MiddlewareConsumerRefresh_Final.Contact_mdm();
                  cnt_mdm.BirthDate = '1/1/2013';
                  cnt_mdm.CRMPhone = '111111';
                  cnt_mdm.FirstName = 'FirstName';
                  cnt_mdm.LastName= 'LastName';
                  cnt_mdm.MDMRowId = '1-2-3-4';
                  cnt_mdm.PSNSignInId = 'psn@test.com';
                  cnt_mdm.Account = new list<Sony_MiddlewareConsumerRefresh_Final.Account_mdm>{acc_mdm};
                  cnt_mdm.ListOfAddress = ladd_mdm;
                  
                  Sony_MiddlewareConsumerRefresh_Final.ListOfContact_mdm lCnt = new Sony_MiddlewareConsumerRefresh_Final.ListOfContact_mdm();
                  lCnt.Contact = new list<Sony_MiddlewareConsumerRefresh_Final.Contact_mdm>{cnt_mdm};
                  
                  responseElm.ListOfContact = lCnt;
                             
                  response.put('response_x', responseElm); 
               }
    }
}