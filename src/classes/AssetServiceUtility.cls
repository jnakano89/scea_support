/**
08/27/2013  : Urminder : created this class.
Description : This is a utility class which calls all the Asset related Services.
**/ 	

public class AssetServiceUtility {
  /*This method is used to get list of assets from service Account Assets*/
  public static AssetSearchResult getAssets(String mdmAccountId) {
  	
  	system.debug('>>>>>>>>>>AssetServiceUtility: mdmAccountId>>>>> ' + mdmAccountId);
  	
  	Sony_MiddlewareAssetdata.PSConsumerAccountAssets_Output_element response;
  	
  	AssetSearchResult searchResult = new AssetSearchResult();
  	

  	Sony_MiddlewareAssetdata.ConsumerAccountAssetsQSPort serviceReq = new Sony_MiddlewareAssetdata.ConsumerAccountAssetsQSPort();

  	//try{
  		            
  		response = serviceReq.PSConsumerAccountAssets(mdmAccountId, Label.SFDC_SOURCE_ID, null, null, null);

  	//}catch(Exception e){
            	
    //	IntegrationAlert.addException('Get Consumer Assets', 'MDM Account ID='+mdmAccountId, e);
            	
	//}
	  	
  	
  	if(response <> null) {
  		
  	  if(response.ErrorMessage <> null) {
  	  	
  	    searchResult.ErrorMessage = response.ErrorMessage.ErrorMessage;
  	    searchResult.ErrorCode = response.ErrorMessage.ErrorCode;
  	    searchResult.ErrorDescription = response.ErrorMessage.Description;
  	  
  	  } else if (response.ListOfAccount.Account <> null) {
  	  	
  	  	searchResult.listOfAssests = new list<Sony_MiddlewareAssetdata.Asset>();

  	  	for(Sony_MiddlewareAssetdata.Account acc : response.ListOfAccount.Account) {

	  	  if (acc.ListOfAsset.Asset <> null) {	 
	  	  	
	  	  	 for(Sony_MiddlewareAssetdata.Asset ast : acc.ListOfAsset.Asset) {
	  	  	 	
	  	  	  searchResult.listOfAssests.add(ast);

	  	  	  system.debug('>>>>>> MDMAssetRowId= ' + ast.MDMAssetRowId); 	

	  	  	 }
	  	  }
  	  	}
  	  }
  	}
  	return searchResult;
  }
}