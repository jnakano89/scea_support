/*
    @Author: Venkat Reddy
    @Version: 1.0
    @Description: Auto complete and search funtionality for the Articles
*/
global class SCEA_SearchLandingPageController{
    /*Global variables*/
    public String searchedRecordId { get; set; }
    //public static list<ResultSet> searchedRecord {get;set;}
    //public list<ResultSet> searchedResultList {get;set;}
    public String searchedString {get;set;}
    //properties for guided browse category
    public List<Category__c> categoryList{set;get;}
    public Integer product_buckets_len{set;get;}
    public Integer account_buckets_len{set;get;}
    public Integer topics_buckets_len{set;get;}
    public String selectedProduct{set;get;}
    public String selectedProductCategory{set;get;}
    public String selectedAccountCategory{set;get;}
    public String selectedAllTopicCategory{set;get;}
    public List<KnowledgeArticleVersion> articleList{set;get;}
    public List<ResultSet> guidedBrowseResultsList{set;get;}
    public Boolean showArticlesFlag{set;get;}
    public Boolean isProdutSelectedFlag{set;get;}
    public Boolean isAccountSelectedFlag{set;get;}
    public Boolean isAllTopicsSelectedFlag{set;get;}
    public Boolean showMoreButton{set;get;}
    public KnowledgeArticleVersion featureArticle{set;get;}
    public FeatureResultSet featureArticleResult{set;get;}
    //property for error code
    public Error_Code_PKB__c errorCodeCS{get;set;}
    public String errorCodeString{get;set;}
    public String errorCodeAllTopicString{get;set;}
    private List<Category__c> allCategoryList;
    private Integer browseGuideTopicsPageNumber = 0;
    public Boolean showMoreBrowseGuideTopicsFlag{set;get;}
    
    //to hold lang parameter value
    public String selectedLanguage{set;get;}
    
    static String decisionTreeArticleKeyPrefix = Decision_Tree__kav.sObjectType.getDescribe().getKeyPrefix();
    static String kcArticleKeyPrefix = KC_Article__kav.sObjectType.getDescribe().getKeyPrefix();
    
    Integer pageRecordSize = Integer.valueOf(Label.Browse_Guide_Search_Results_Per_Page)+1;
    Integer searchTextMinimumLength = Integer.valueOf(Label.Search_Text_Minimum_Length);
    static Integer articleSummaryTrimLength = Integer.valueOf(Label.Article_Summary_Trim_Length);
    Integer browseGuideTopicLimit = Integer.valueOf(Label.Browse_Guide_Topics_Limit);
    
    global SCEA_SearchLandingPageController(){
        //to accees lang parameter value from URL
        //incase no lang value in URL then make it en_US which is English as default
        if(ApexPages.currentPage().getParameters().get('lang') != null)
            selectedLanguage = ApexPages.currentPage().getParameters().get('lang');
        
        system.debug('--- Site.getBaseURL() ---'+Site.getBaseURL());
        if(Site.getBaseURL() != null && (selectedLanguage == null || selectedLanguage == '')){
            for(SCEA_LanguageSpecificSites__c langSite : SCEA_LanguageSpecificSites__c.getAll().values()){
                if(Site.getBaseURL().containsIgnoreCase(langSite.URL_Identifier_Keyword__c)){
                    selectedLanguage = langSite.Name;
                    break;
                }
            }
        }
        
        if(selectedLanguage == null || selectedLanguage == '')
            selectedLanguage = 'en_US';
        system.debug('--- selectedLanguage ---'+selectedLanguage);
        fetchFeaturedArticles();
    }
    
    global PageReference searchForArticles() {
        //searchedResultList = new list<ResultSet>();
        PageReference pageRef = null;
        if(searchedString != null && searchedString.length()>searchTextMinimumLength){
            pageRef = Page.SCEA_SearchResults;
            pageRef.getParameters().put('selStr',searchedString);
            pageRef.getParameters().put('lang',selectedLanguage);
            pageRef.setRedirect(true);
            return pageRef;
        }
        return pageRef;
    }
    
    public void fetchCategoriesBasedOnSelection(String selStr){
        //List<Category__c> tempCategoryList = new List<Category__c>();
        categoryList = new List<Category__c>();
        allCategoryList = new List<Category__c>();
        showMoreBrowseGuideTopicsFlag = false;
        browseGuideTopicsPageNumber = 0;
        if(selStr != null && selStr.equalsIgnoreCase('Product')){
            allCategoryList = [select Name, Display_Order__c, Category_API_Name__c, es_Name__c, pt_BR_Name__c from Category__c where Display_in_Product__c=true order by Display_Order__c asc ];
            product_buckets_len = allCategoryList.size();
        }else if(selStr != null && selStr.equalsIgnoreCase('Account')){
            allCategoryList = [select Name, Display_Order__c, Category_API_Name__c, es_Name__c, pt_BR_Name__c from Category__c where Display_in_Account__c=true order by Display_Order__c asc ];
            account_buckets_len = allCategoryList.size();
        }else if(selStr != null && selStr.equalsIgnoreCase('All Topics')){
            allCategoryList = [select Name, Display_Order__c, Category_API_Name__c, es_Name__c, pt_BR_Name__c from Category__c order by Display_Order__c asc];
            topics_buckets_len = allCategoryList.size();
        }
        if(allCategoryList.size()>browseGuideTopicLimit){
            showMoreBrowseGuideTopicsFlag = true;
            browseGuideTopicsPageNumber++;
            for(Category__c cat : allCategoryList){
                categoryList.add(cat);
                if(categoryList.size() == browseGuideTopicLimit){
                    break;
                }
            }
        }else{
            categoryList.addAll(allCategoryList);
        }
    }
    
    public void showMoreBrowseGuideTopics(){
        categoryList.clear();
        categoryList.addAll(allCategoryList);
        showMoreBrowseGuideTopicsFlag = false;
    }
    
    public void fetchCategoriesForProduct(){
        fetchCategoriesBasedOnSelection('Product');
        showArticlesFlag = false;
        isProdutSelectedFlag = true;
        isAccountSelectedFlag = false;
        showMoreButton = false;
        guidedBrowseResultsList = new List<ResultSet>();
        selectedAccountCategory = null;
        isAllTopicsSelectedFlag = false;
        selectedAllTopicCategory = null;
        errorCodeCS = null;
        errorCodeString = null;
        errorCodeAllTopicString = null;
    }
    
    public void fetchCategoriesForAccount(){
        fetchCategoriesBasedOnSelection('Account');
        showArticlesFlag = false;
        isProdutSelectedFlag = false;
        isAccountSelectedFlag = true;
        showMoreButton = false;
        guidedBrowseResultsList = new List<ResultSet>();
        selectedProduct = null;
        selectedProductCategory = null;
        isAllTopicsSelectedFlag = false;
        selectedAllTopicCategory = null;
        errorCodeCS = null;
        errorCodeString = null;
        errorCodeAllTopicString = null;
    }
    
    public void fetchCategoriesForAllTopics(){
        fetchCategoriesBasedOnSelection('All Topics');
        showArticlesFlag = false;
        isProdutSelectedFlag = false;
        isAccountSelectedFlag = false;
        showMoreButton = false;
        guidedBrowseResultsList = new List<ResultSet>();
        selectedProduct = null;
        selectedProductCategory = null;
        isAllTopicsSelectedFlag = true;
        selectedAccountCategory = null;
        errorCodeCS = null;
        errorCodeString = null;
        errorCodeAllTopicString = null;
    }
    
    public void fetchArticlesBasedOnSubCategory(){
        showArticlesFlag = true;
        system.debug('---'+selectedProduct+'---'+selectedProductCategory+'---'+selectedAccountCategory);
        guidedBrowseResultsList = new List<ResultSet>();
        List<sObject> searchList = new List<sObject>();
        if(selectedProduct != null && selectedProductCategory != null){
            //query on Decision Tree article based on category and/or product
            String soqlQuery = 'select Id, ArticleNumber, Title, summary, ArticleType, UrlName from Decision_Tree__kav where PublishStatus=\'Online\' AND Language=:selectedLanguage with data category Topic__c AT '+selectedProductCategory+' AND Products__c AT '+selectedProduct+' ORDER BY ArticleNumber DESC LIMIT '+pageRecordSize;
            List<sObject> sobjectList = Database.query(soqlQuery);
            searchList.addAll(sobjectList);
            
            if(searchList.size() < pageRecordSize){
                //query on KC article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, summary, ArticleType, UrlName from KC_Article__kav where PublishStatus=\'Online\' AND Language=:selectedLanguage with data category Topic__c AT '+selectedProductCategory+' AND Products__c AT '+selectedProduct+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
        }else if(selectedProduct == null && selectedAccountCategory != null){
            //query on Decision Tree article based on category and/or product
            String soqlQuery = 'select Id, ArticleNumber, Title, summary, ArticleType, UrlName from Decision_Tree__kav where PublishStatus=\'Online\' AND Language=:selectedLanguage with data category Topic__c AT '+selectedAccountCategory+' ORDER BY ArticleNumber DESC LIMIT '+pageRecordSize;
            List<sObject> sobjectList = Database.query(soqlQuery);
            searchList.addAll(sobjectList);
            
            if(searchList.size() < pageRecordSize){
                //query on KC article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, summary, ArticleType, UrlName from KC_Article__kav where PublishStatus=\'Online\' AND Language=:selectedLanguage with data category Topic__c AT '+selectedAccountCategory+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
        }else if(selectedProduct == null && selectedAccountCategory == null && selectedAllTopicCategory != null){
            //query on Decision Tree article based on category and/or product
            String soqlQuery = 'select Id, ArticleNumber, Title, summary, ArticleType, UrlName from Decision_Tree__kav where PublishStatus=\'Online\' AND Language=:selectedLanguage with data category Topic__c AT '+selectedAllTopicCategory+' ORDER BY ArticleNumber DESC LIMIT '+pageRecordSize;
            List<sObject> sobjectList = Database.query(soqlQuery);
            searchList.addAll(sobjectList);
            
            if(searchList.size() < pageRecordSize){
                //query on KC article based on category and/or product
                soqlQuery = 'select Id, ArticleNumber, Title, summary, ArticleType, UrlName from KC_Article__kav where PublishStatus=\'Online\' AND Language=:selectedLanguage with data category Topic__c AT '+selectedAllTopicCategory+' ORDER BY ArticleNumber DESC LIMIT '+(pageRecordSize-searchList.size());
                sobjectList = Database.query(soqlQuery);
                searchList.addAll(sobjectList);
            }
        }
        
        if(searchList != null && !searchList.isEmpty() && searchList.size() == pageRecordSize){
            searchList.remove(searchList.size()-1);
            showMoreButton = true;
        }else
            showMoreButton = false;
        
        for(SObject s : searchList){
            guidedBrowseResultsList.add(new SCEA_SearchLandingPageController.ResultSet(s,selectedLanguage) );
        }
    }
    
    public void initializeValuesFormySystem(){
        isProdutSelectedFlag = false;
        isAccountSelectedFlag = false;
        categoryList = new List<Category__c>();
        showArticlesFlag = false;
        showMoreButton = false;
        guidedBrowseResultsList = new List<ResultSet>();
        isAllTopicsSelectedFlag = false;
        errorCodeCS = null;
        errorCodeString = '';
        errorCodeAllTopicString = '';
    }
    
    public PageReference showMoreArticles(){
        PageReference pageRef = Page.scea_searchresults;
        if(selectedProduct != null && selectedProductCategory != null){
            pageRef.getParameters().put('guideStr',selectedProductCategory);
            pageRef.getParameters().put('guideProdStr',selectedProduct);
            pageRef.getParameters().put('lang',selectedLanguage);
        }else if(selectedProduct == null && selectedAccountCategory != null){
            pageRef.getParameters().put('guideStr',selectedAccountCategory);
            pageRef.getParameters().put('lang',selectedLanguage);
        }else if(selectedProduct == null && selectedAccountCategory == null && selectedAllTopicCategory != null){
            pageRef.getParameters().put('guideStr',selectedAllTopicCategory);
            pageRef.getParameters().put('lang',selectedLanguage);
        }
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public void fetchFeaturedArticles(){
        List<Feature_Article__c> featureArticleCSList = Feature_Article__c.getAll().values();
        if(featureArticleCSList != null && !featureArticleCSList.isEmpty() && featureArticleCSList.get(0).Article_Id__c != null && featureArticleCSList.get(0).Article_Id__c != ''){
            String articleNumber = featureArticleCSList.get(0).Article_Id__c;
            String featureArticleSOQL = 'select Id, Title, ArticleType from KnowledgeArticleVersion where ArticleNumber=:articleNumber AND Language=:selectedLanguage AND PublishStatus=\'Online\'';
            List<KnowledgeArticleVersion> kavList = Database.query(featureArticleSOQL);
            if(kavList != null && !kavList.isEmpty()){
                featureArticle = kavList.get(0);
                String featureArticleResultSOQL = '';
                String articleType = featureArticle.ArticleType;
                featureArticleResultSOQL = 'select Id, ArticleNumber, Title, ArticleType, UrlName from '+articleType+' where ArticleNumber=:articleNumber AND PublishStatus=\'Online\' AND Language=:selectedLanguage limit 1';
                List<sObject> objList = Database.query(featureArticleResultSOQL);
                if(objList != null && !objList.isEmpty()){
                    featureArticleResult = new FeatureResultSet(objList.get(0),selectedLanguage);
                }
            }
        }
    }
    
    public void searchForErrorCode(){
        errorCodeCS = null;
        
        List<Error_Code_PKB__c> errorCodeCSList = [select Id, Name, Error_Code__c, Description__c, Action__c from Error_Code_PKB__c where Language__c=:selectedLanguage];
        if(errorCodeCSList != null && !errorCodeCSList.isEmpty() && errorCodeString != null && errorCodeString != ''){
            //if matches with error code then return error code which is a single record to show in search results
            for(Error_Code_PKB__c errorCodeObj : errorCodeCSList){
                if(errorCodeObj.Error_Code__c.containsIgnoreCase(errorCodeString)){
                    errorCodeCS = errorCodeObj;
                    break;
                }
            }
        }else if(errorCodeCSList != null && !errorCodeCSList.isEmpty() && errorCodeAllTopicString != null && errorCodeAllTopicString != ''){
            //if matches with error code then return error code which is a single record to show in search results
            for(Error_Code_PKB__c errorCodeObj : errorCodeCSList){
                if(errorCodeObj.Error_Code__c.containsIgnoreCase(errorCodeAllTopicString)){
                    errorCodeCS = errorCodeObj;
                    break;
                }
            }
        }
        //to add error code search string to the search history with details
        if(errorCodeCS != null){
            Error_Code_Search_History__c ecSearchHistory = new Error_Code_Search_History__c();
            ecSearchHistory.Error_Code__c = errorCodeCS.Id;
            if(errorCodeString != null && errorCodeString != '')
                ecSearchHistory.Search_Source__c = 'My System';
            else if(errorCodeAllTopicString != null && errorCodeAllTopicString != '')
                ecSearchHistory.Search_Source__c = 'All Topics';
                
            try{
                insert ecSearchHistory;
            }catch(Exception e){
                
            }
        }
    }
    
    /*Record Wrapper*/
    global class ResultSet{
        public String Id {get;set;} 
        public String ArticleNumber{get;set;}
        public String Title{get;set;}
        public String sObjectName {get;set;}
        public String overviewSummary{set;get;}
        public String articleDetailTitle{set;get;}
        public String articleDetailURL{set;get;}
        
        public ResultSet(sObject s, String tempSelectedLanguage){
            this.Id = s.Id;
            this.ArticleNumber = s.get('ArticleNumber')+'';
            this.Title = s.get('Title')+'';
            
            if(String.valueOf(s.Id).startsWith(decisionTreeArticleKeyPrefix)){
                this.overviewSummary = s.get('Summary')+'';
                this.sObjectName = 'Decison Tree';        
            }else if(String.valueOf(s.Id).startsWith(kcArticleKeyPrefix)){
                this.overviewSummary = s.get('Summary')+'';
                this.sObjectName = 'KC Article';        
            }
            
            if(this.overviewSummary.length()>articleSummaryTrimLength)
                this.overviewSummary = this.overviewSummary.substring(0,(articleSummaryTrimLength-2))+'...';
            this.overviewSummary = this.overviewSummary.replaceAll('<br/>','\n');
            this.overviewSummary = this.overviewSummary.replaceAll('<br />','\n');
            String htmlTagPattern = '<.*?>';
            Pattern patternTemp = Pattern.compile(htmlTagPattern);
            Matcher matcherTemp = patternTemp.matcher(this.overviewSummary);
            this.overviewSummary = matcherTemp.replaceAll('');
            if(Site.getBaseURL() != null){
                articleDetailTitle = EncodingUtil.urlDecode( String.valueOf(s.get('UrlName')), 'ISO-8859-1'); 
                articleDetailURL = Site.getBaseURL()+'/articles/'+tempSelectedLanguage+'/'+String.valueOf(s.get('ArticleType')).replace('__kav','')+'/';
              
            }
        }     
    }
    
    /*Record Wrapper*/
    global class FeatureResultSet{
        public String Id {get;set;} 
        public String ArticleNumber{get;set;}
        public String Title{get;set;}
        public String articleDetailURL{set;get;}
        
        public FeatureResultSet(sObject s, String tempSelectedLanguage){
            this.Id = s.Id;
            this.ArticleNumber = s.get('ArticleNumber')+'';
            this.Title = s.get('Title')+'';
            
            if(Site.getBaseURL() != null){
                articleDetailURL = Site.getBaseURL()+'/articles/'+tempSelectedLanguage+'/'+String.valueOf(s.get('ArticleType')).replace('__kav','')+'/'+String.valueOf(s.get('UrlName'));
            }
        }     
    }
}