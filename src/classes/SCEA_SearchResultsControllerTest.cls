/*
    @Author: Venkat Reddy
    @Version: 1.0
    @Description: Test class for SCEA_SearchResultsController
*/
@isTest
public class SCEA_SearchResultsControllerTest{
    private static testMethod void constructorTestMethod(){
        Test.setCurrentPage(Page.SCEA_SearchResults);
        ApexPages.currentPage().getParameters().put('lang','en_US');
        ApexPages.currentPage().getParameters().put('selStr','Test');
        KC_Article__kav kcArticle = new KC_Article__kav();
        kcArticle.language = 'en_US';
        kcArticle.Title = 'Test KC Article';
        kcArticle.UrlName = 'Test-KC-Article';
        insert kcArticle;
        
        kcArticle = [SELECT KnowledgeArticleId, Language, PublishStatus, ArticleNumber FROM KC_Article__kav WHERE Id = :kcArticle.Id];
        system.debug(kcArticle.Language+'---'+kcArticle.PublishStatus);
        Test.startTest();
        KbManagement.PublishingService.publishArticle(kcArticle.KnowledgeArticleId, true);
        
        kcArticle = [SELECT KnowledgeArticleId, Language, PublishStatus, ArticleNumber FROM KC_Article__kav WHERE Id = :kcArticle.Id];
        system.debug(kcArticle.Language+'---'+kcArticle.PublishStatus);
        
        KnowledgeArticleVersion kaversion = [select Id from KnowledgeArticleVersion where ArticleNumber=:kcArticle.ArticleNumber AND Language='en_US' AND PublishStatus='Online'];
        
        SearchPromotionRule promotedArticle = new SearchPromotionRule();
        promotedArticle.PromotedEntityId = kaversion.Id;
        promotedArticle.Query = 'Test';
        insert promotedArticle;
        
        Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = kcArticle.Id+'';
        Test.setFixedSearchResults(fixedSearchResults);
        SCEA_SearchResultsController con = new SCEA_SearchResultsController();
        Test.stopTest();
        con.showMoreResults();
        system.assertEquals(con.searchedResultList.size(),0);
        
        con.topicDataCategoryList.get(0).isSelected = true;
        con.productDataCategoryList.get(0).isSelected = true;
        con.showfilterWiseResults();
        system.assertEquals(con.searchedResultList.size(),1);
        
        con.topicDataCategoryList.get(0).isSelected = false;
        con.productDataCategoryList.get(0).isSelected = false;
        con.showfilterWiseResults();
        system.assertEquals(con.searchedResultList.size(),1);
        
    }
    
    private static testMethod void constructorTestMethodForErrorCode(){
        Test.setCurrentPage(Page.SCEA_SearchResults);
        ApexPages.currentPage().getParameters().put('lang','en_US');
        ApexPages.currentPage().getParameters().put('selStr','TESTERRCODE');
        KC_Article__kav kcArticle = new KC_Article__kav();
        kcArticle.language = 'en_US';
        kcArticle.Title = 'Test KC Article';
        kcArticle.UrlName = 'Test-KC-Article';
        insert kcArticle;
        
        Error_Code_PKB__c errorCode = new Error_Code_PKB__c();
        errorCode.Error_Code__c = 'TESTERRCODE';
        errorCode.Description__c = 'Test Description';
        errorCode.Action__c = 'Test Action';
        insert errorCode;
        Test.startTest();
        SCEA_SearchResultsController con = new SCEA_SearchResultsController();
        Test.stopTest();        
    }
    
    private static testMethod void constructorTestMethodForGuideBrowse(){
        Test.setCurrentPage(Page.SCEA_SearchResults);
        ApexPages.currentPage().getParameters().put('lang','en_US');
        ApexPages.currentPage().getParameters().put('guideStr','PlayStation_Store__c');
        ApexPages.currentPage().getParameters().put('guideProdStr','PlayStation_4__c');
        KC_Article__kav kcArticle = new KC_Article__kav();
        kcArticle.language = 'en_US';
        kcArticle.Title = 'Test KC Article';
        kcArticle.UrlName = 'Test-KC-Article';
        insert kcArticle;
        Test.startTest();
        SCEA_SearchResultsController con = new SCEA_SearchResultsController();
        Test.stopTest();        
    }
}