/****************************************************************************************
Class         : SurveyUtility
Description   : This is utlity class for send Survey [T-186690].
Created Date  : Sep 17, 2013
Developed by  : Urminder Vohra (JDC)
Updates       :
              : Aaron Briggs - 06/04/2014 - SF-614 - Update to Support Origin Parameter
****************************************************************************************/
public class SurveyUtility {
  
    public static String END_POINT; 
    public static String SECURITY_STR;
  
    static {
        Integration__c settings = Integration__c.getOrgDefaults();
        END_POINT = settings <> null ? settings.Survey_End_Point__c : '';
        SECURITY_STR = settings <> null ? settings.Survey_Security_String__c : '';
    }
  
    @future(callout=true)
    public static void sendSurvey (Id caseId, Id surveyId){
        sendSurveyBO(caseId, surveyId);
    }

    public static void sendSurveyBO (Id caseId, Id surveyId){
        Case selectedCase;
        String companyName;

        // abriggs - 06/04/2014 - SF-614 - Add Origin Field
        for(Case cs : [select ContactId, Contact.Preferred_Language__c, Contact.LATAM_Country__c,  
                    Contact.FirstName, Contact.lastName, Contact.Phone_Unformatted__c, Contact.Email,
  					Contact.Bill_To__c, Contact.Bill_To__r.City__c, 
  					Contact.Bill_To__r.State__c, Contact.Bill_To__r.Country__c, Contact.Bill_To__r.Postal_Code__c,
  					Bill_To__r.City__c, Bill_To__r.State__c,  
  					Bill_To__r.Country__c,  Bill_To__r.Postal_Code__c, RecordType.Name, Sub_Area__c,
  					Bill_To__c, Owner.Name, OwnerId, CaseNumber, Service_Location__r.Location_Id__c,
  					Resolution__c, SCEA_Product__r.Name, SCEA_Product__c,  
  					Product__c, Owner.Username, 
  					Status, Origin, Case_Type__c
  					from Case where Id = :caseId]) {
            selectedCase = cs;
            System.debug('-------------------------> Selected Case Fields: ' + selectedCase);
        }

        //updating END POINT with parameters.
        if(selectedCase <> null){
            String response = 'Success';

            if(!Test.isRunningTest()){
                END_POINT = getContentEndPointString(selectedCase, END_POINT);

	  	        // Sending HTTP Request to get Response.
	  	        HttpRequest req = new HttpRequest();
                HttpResponse res = new HttpResponse();
                Http http = new Http();
                req.setEndpoint(END_POINT);
                req.setMethod('POST');
                res = http.send(req);
                System.debug('-------------------------> Endpoint: ' + END_POINT);
                System.debug('-------------------------> Response: ' + res);
                System.debug('-------------------------> Response Body: ' + res.getBody());
                response = res.getBody();
  		    }
            updateSurveyRecord(response, surveyId);
        }
    }

    private static void updateSurveyRecord(String response, Id surveyId){
	  	list<Staging_Survey__c> surveyList = new list<Staging_Survey__c>();
  	
        for(Staging_Survey__c survey : [select Processed_Date_Time__c, Errors__c 
  										from Staging_Survey__c 
  										where Id = : surveyId]) {
            if(response == 'Success') {
                survey.Processed_Date_Time__c = DateTime.now();
            } else {
                survey.Errors__c = 'Survey was not Successful ' + response;
            }
  	         surveyList.add(survey);
        }
        update surveyList;
    }
  
    private static String getContentEndPointString(Case cs, String endPointString){
        String companyName = getCompanyName(cs.OwnerId);
        
        system.debug('-------------------------> Case: ' + cs);
        system.debug('-------------------------> Endpoint String: ' + endPointString);
        endPointString += 'sec_string/' + SECURITY_STR + '/';
        endPointString += 'service_request/' + cs.CaseNumber + '/';

        if(cs.ContactId <> null){
            if(cs.Contact.FirstName <> null){endPointString += 'first_name/' + EncodingUtil.urlEncode(cs.Contact.FirstName, 'UTF-8') + '/';}
            if(cs.Contact.LastName <> null){endPointString += 'last_name/' + EncodingUtil.urlEncode(cs.Contact.LastName, 'UTF-8') + '/';}
            if(cs.Contact.Email <> null){endPointString += 'email/' + cs.Contact.Email + '/';}
            if(cs.Contact.Phone_Unformatted__c <> null){endPointString += 'ph_home/' + EncodingUtil.urlEncode(cs.Contact.Phone_Unformatted__c, 'UTF-8') + '/';}
            if(cs.Contact.Preferred_Language__c <> null){endPointString += 'language/' + cs.Contact.Preferred_Language__c + '/';}
        }

        if(cs.Contact.Preferred_Language__c == 'Spanish' && getCountryCode(cs.Contact.LATAM_Country__c) <> null) {
            endPointString += 'country/' + EncodingUtil.urlEncode(getCountryCode(cs.Contact.LATAM_Country__c), 'UTF-8') + '/';
        }
        else if(cs.Bill_To__c <> null){
            if(cs.Bill_To__r.City__c!=null){endPointString += 'city/' + EncodingUtil.urlEncode(cs.Bill_To__r.City__c, 'UTF-8') + '/';}
            if(cs.Bill_To__r.State__c!=null){endPointString += 'state/' + EncodingUtil.urlEncode(cs.Bill_To__r.State__c, 'UTF-8') + '/';}
            if(cs.Bill_To__r.Country__c!=null){endPointString += 'country/' + EncodingUtil.urlEncode(cs.Bill_To__r.Country__c, 'UTF-8') + '/';}
            if(cs.Bill_To__r.Postal_Code__c!=null){endPointString += 'postal_code/' + EncodingUtil.urlEncode(cs.Bill_To__r.Postal_Code__c, 'UTF-8') + '/';}
        }
        else if(cs.ContactId <> null && cs.Contact.Bill_To__c <> null){
            if(cs.Contact.Bill_To__r.City__c!=null){endPointString += 'city/' + EncodingUtil.urlEncode( cs.Contact.Bill_To__r.City__c, 'UTF-8')  + '/';}
            if(cs.Contact.Bill_To__r.State__c!=null){endPointString += 'state/' + EncodingUtil.urlEncode(cs.Contact.Bill_To__r.State__c, 'UTF-8') + '/';}
            if(cs.Contact.Bill_To__r.Country__c!=null){endPointString += 'country/' + EncodingUtil.urlEncode(cs.Contact.Bill_To__r.Country__c, 'UTF-8') + '/';}  
            if(cs.Contact.Bill_To__r.Postal_Code__c!=null){endPointString += 'postal_code/' + EncodingUtil.urlEncode(cs.Contact.Bill_To__r.Postal_Code__c, 'UTF-8') + '/';}
        }
        else{endPointString += 'country/' + EncodingUtil.urlEncode('USA', 'UTF-8') + '/';}
  	
        if(cs.Product__c <> null){
            if(cs.RecordType.Name=='Voucher'){endPointString += 'product/' + EncodingUtil.urlEncode('No Value', 'UTF-8') + '/';}
            else{
                if(cs.Product__c!=null){endPointString += 'product/' + EncodingUtil.urlEncode(cs.Product__c, 'UTF-8') + '/';}
  		    }
        }
  	
        if(companyName <> null){endPointString += 'site/' + EncodingUtil.urlEncode(companyName, 'UTF-8') + '/';}
        if(cs.Status <> null){endPointString += 'resolution/' + EncodingUtil.urlEncode(cs.Status, 'UTF-8') + '/';
        }
        if(cs.RecordType.Name=='Voucher'){endPointString += 'diagnosis/' + EncodingUtil.urlEncode('No Value', 'UTF-8') + '/';}
        else{
	  	    if(cs.Sub_Area__c <> null){endPointString += 'diagnosis/' + EncodingUtil.urlEncode(cs.Sub_Area__c, 'UTF-8') + '/';}
            else if(cs.Case_Type__c <> null){endPointString += 'diagnosis/' + EncodingUtil.urlEncode(cs.Case_Type__c, 'UTF-8') + '/';}
        }

        endPointString += 'category/' + EncodingUtil.urlEncode(cs.RecordType.Name, 'UTF-8') + '/';
  	
        if(cs.RecordTypeId == GeneralUtiltyClass.RT_SERVICE_ID) {endPointString += 'agent_name/' + cs.Service_Location__r.Location_Id__c + '/';}
        else{
            if(cs.Owner.Username <> null){endPointString += 'agent_name/' + cs.Owner.Username + '/';}
        }
        // abriggs - 06/04/2014 - SF-614 - Add Origin Field to Endpoint
        system.debug('-------------------------> Case Origin: ' + cs.Origin);
        endPointString += 'origin/' + EncodingUtil.urlEncode(cs.Origin, 'UTF-8');

        system.debug('-------------------------> Endpoint String: ' + endPointString);
        return endPointString; 
    }
  
    private static String getCompanyName(String userId) {
        String companyName;

        for(User u :[select CompanyName from User where Id = :userId]) {
            companyName = u.CompanyName;	
        }

        return companyName;
    }

    private static String getLanguageCode(String language) {
        map<String, Language__c> langSettingMap = Language__c.getAll();
        Language__c lang = langSettingMap.get(language);

        if(lang <> null) {
            return lang.X3_Letter_Code__c;
        } else {
            return null;
        }
    }

    private static String getCountryCode(String countryName) {
        // Urminder : Changed as per Issue I-99062
        return countryName;
        map<String, Country__c> countryMap = Country__c.getAll();
        Country__c country = countryMap.get(countryName);

        if(country <> null) {
            return country.X3_Letter_Code__c;
        } else {
            return null;
        }
    }

}