/**********************************************************************************************************************************************************************
// Developed By : Appirio
//          August 21,2013 : KapiL Choudhary(JDC) : Update Code to Add logic for function "updateMDM" [T-166679].
//          August 21,2013 : KapiL Choudhary(JDC) : Update Code  Add function "updateFields" [T-179733].
//          01/13/2014 : Harish Khatri(JDC) : T-235683:-Create Staging_PPP_Outbound__c record for Refund Process
//          01/15/2014 : Harish Khatri(JDC) : T-237745 :Change Logic on PPP Start Date and PPP End Date on Asset

   Updates :
            04/17/2014 Leena Mandadapu : JIRA# SF-584 - PPP Start/End Date calculation logic based on Proof of Purchase Date.
            05/19/2014 Leena Mandadapu : Jira# SF-683 - Added Asset Model Number Validation logic
            07/01/2014 Leena Mandadapu : Jira# SF-1026 - Updated a few statements as they are throwing errors during Test class validations.
            10/24/2014 Leena Mandadapu : Jira# SMS-654 - Added PPP record lock logic for PYB orders
            02/18/2015 Leena Mandadapu : Jira# SMS-1041 - Re-activate Canadian PPP Dealer Ids
************************************************************************************************************************************************************************/
public with sharing class AssetManagement {

    public Static Boolean isExchangeAsset = false;
    public Static Boolean ProcessPPPdates = true;


    public static void afterInsert(List<Asset__c> assetList){

    }

    public static void beforeInsert(List<Asset__c> assetList){
        updateAssetCoverageType(assetList, null);
        //LM 05/20/2014 : commented below method and updated the method arguments
        //updateFields(assetList);
        //Argument 2 : true means validate the model number on new Assets
        updateFields(assetList, true);
    }  

    public static void beforeUpdate(List<Asset__c> assetList, Map<Id, Asset__c> oldAssetMap){
        //updateAssetCoverageType(assetList, oldAssetMap);
        //LM 05/20/2014 : commented below method and updated the method arguments
        //updateFields(assetList);
        //Argument 2 : false means do not validate model number on existing Assets
        updateFields(assetList, false);
        updateCancellationDate(assetList, oldAssetMap);
    }   

    public static void afterUpdate(List<Asset__c> assetList, Map<Id, Asset__c> oldAssetMap){

        updateStagingObject(assetList, oldAssetMap);
        updateStagingPPPOutboundObject(assetList, oldAssetMap); 
        updateESPOrderRecordType(assetList, oldAssetMap);
    }   
    /*
     * create Staging_PPP_Outbound__c from Asset
     */

    public static void updateESPOrderRecordType(List<Asset__c> assetList, Map<Id, Asset__c> oldAssetMap){
        Set<Id> AssetIds = new Set<Id>();
        List<Order__c> UpdateOrderList = new List<Order__c>();

        for(Asset__c asset :assetList){
            if( asset.PPP_Status__c != null &&

                    oldAssetMap.get(asset.id).PPP_Status__c != asset.PPP_Status__c && 
                    asset.PPP_Status__c.equals('Pending Cancel')){


                AssetIds.add(Asset.Id);
            }
        }

        if(AssetIds.Size()>0){

            for (Order__c o : [Select ESP_Record_Type__c from Order__c where Asset__c IN :AssetIds]){
                o.ESP_Record_Type__c='C';
                UpdateOrderList.add(o);
            }
        }

        if(UpdateOrderList.size()>0){
            try{
                Orderhelper.processOutbound=false;
                database.update(UpdateOrderList);
            }
            catch(Exception e){
                System.Debug('>>>>>>>> Exception in Updating ESP Order Record TYPE  >>>>>>>>>>' +e);
            }
        }
    }

    public static void updateStagingPPPOutboundObject(List<Asset__c> assetList, Map<Id, Asset__c> oldAssetMap){
        list<Staging_PPP_Outbound__c> staOutboundList = new list<Staging_PPP_Outbound__c>();    
        list<String> assetIds = new list<String>();
        Map<String ,Order__c> mapIdToOrder = new Map<String,Order__c>();
        Map<String, decimal> assurantMatrixMap = new Map<String, decimal>();
        Set<Id> orderIds = new Set<Id>();
        //LM 06/17/2014 : Changed the Set type to String as Dealer Ids are not SFDC ID type
        //Set<Id> dealerIds = new Set<Id>();
        Set<String> dealerIds = new Set<String>();
        Set<String> sfdcSkuValues = new Set<String>(); 
        Map<Id, Order_Line__c> orderLineMap = new Map<Id, Order_Line__c>();

        for(Asset__c asset :assetList){
            if( asset.PPP_Status__c != null &&
                    oldAssetMap.get(asset.id).PPP_Status__c != asset.PPP_Status__c && 
                    asset.PPP_Status__c.equals('Pending Cancel')){
                assetIds.add(asset.Id);
                sfdcSkuValues.add(asset.PPP_Product_SKU__c);
            }   
        }

        for (Order__c o : [select Total_Tax__c, Order_Type__c, Payment__r.Payment_Status__c, Asset__c,
                           Bill_To__r.Address_Line_1__c, Bill_To__r.Address_Line_2__c, Bill_To__r.City__c, Bill_To__r.State__c, Bill_To__r.Country__c, Ship_To__r.Country__c,
                           Bill_To__r.Postal_Code__c, Consumer__r.Email, Consumer__r.FirstName, Consumer__r.lastName, Consumer__r.Phone_Unformatted__c,
                           Consumer__r.Alt_Phone_Unformatted__c, Case__r.CaseNumber, Case__r.Invoice_Date__c, Case__r.Invoice_Amount__c,
                           ESP_Record_Type__c, ESP_Dealer__c, Name, External_Order_Number__c, Payment_Status__c, ESP_Cancel_After_30_Days__c,
                           Case__c, Case__r.Hot_Topics__c, Case__r.Vendor_Number_Name__c,Order_Total__c,Case__r.CreatedDate,ESP_Refunded_by_Dealer__c,Order_Origin_Source__c,
                           (Select Order__c From Order_Lines__r)  
                           from Order__c where Asset__c IN : assetIds and Order_Type__c = 'ESP' order by LastModifiedDate DESC]){   

            // if(o.Payment__r.Payment_Status__c == 'Charged' || o.Payment__r.Payment_Status__c == 'Refunded') {
            if(!mapIdToOrder.containsKey(o.Asset__c))
                mapIdToOrder.put(o.Asset__c,o); 
            // }    
        }

        for(Order__c o : mapIdToOrder.values()) {
            orderIds.add(o.Id);
            dealerIds.add(o.ESP_Dealer__c); 
            System.debug(' o.Order_Lines__r '+o.Order_Lines__r);
            if(o.Order_Lines__r.size() > 0)
                orderLineMap.put(o.id, o.Order_Lines__r[0]);
        }
        /* for(Order_Line__c oli : [select Order__c, ESP_Cancel_After_30_Days__c, ESP_Refunded_by_Dealer__c 
                                        from Order_Line__c 
                                        where Order__c IN : orderIds]) {
            orderLineMap.put(oli.Order__c, oli);
        }*/
        System.debug(orderIds+' -- orderLineMap -- '+orderLineMap);
        //LM 06/17/2014 : Added SFDC_SKU__c, Dealer_Id__c to SOQL
        for(Assurant_Matrix__c am : [select Dealer_Cost__c, Assurant_SKU__c, SFDC_SKU__c, Dealer_Id__c
                                     from Assurant_Matrix__c 
                                     where SFDC_SKU__c = : sfdcSkuValues
                                     and Dealer_Id__c = : dealerIds]) {
            assurantMatrixMap.put(am.SFDC_SKU__c + '-' + am.Dealer_Id__c, am.Dealer_Cost__c);
        }
        system.debug('************************assetList::'+assetList);
        for(Asset__c asset :assetList){
            system.debug('************************asset.PPP_Status__c::'+asset.PPP_Status__c+' <> '+oldAssetMap.get(asset.id).PPP_Status__c+' mapIdToOrder '+mapIdToOrder );
            if(asset.PPP_Status__c != null && oldAssetMap.get(asset.id).PPP_Status__c != asset.PPP_Status__c && 
                    asset.PPP_Status__c.equals('Pending Cancel')){
                if(mapIdToOrder.containsKey(asset.id)) {    
                    Staging_PPP_Outbound__c stagingPPPOutbound = getStagingOutboundRecord(asset,mapIdToOrder.get(asset.id),assurantMatrixMap,orderLineMap);

                    //stagingPPPOutbound.
                    system.debug('************************asset.PPP_Purchase_Date__c::'+asset.Purchase_Date__c + ' :: '+ asset.PPP_Purchase_Date__c);
                    if(asset.Purchase_Date__c <> null && asset.PPP_Purchase_Date__c <> null) {

                        //if(asset.PPP_Purchase_Date__c.daysBetween(asset.Purchase_Date__c) > 30) {
                        if(asset.Purchase_Date__c.daysBetween(asset.PPP_Purchase_Date__c) > 30) {
                            //orderDealerMap.put(ordr.Id, 'MPOS');
                            if((mapIdToOrder.get(asset.id)).Bill_To__r.Country__c == 'US' || mapIdToOrder.get(asset.id).Bill_To__r.Country__c == 'USA') {
                                if(mapIdToOrder.get(asset.id).Order_Origin_Source__c == 'P') {
                                    stagingPPPOutbound.Order_Dealer__c = 'SCEO';
                                } else {
                                    stagingPPPOutbound.Order_Dealer__c = 'SCEB';
                                }
                            } else if(mapIdToOrder.get(asset.id).Bill_To__r.Country__c.contains('CA') || mapIdToOrder.get(asset.id).Bill_To__r.Country__c.contains('Ca')|| mapIdToOrder.get(asset.id).Bill_To__r.Country__c.contains('ca')) {
                                if(mapIdToOrder.get(asset.id).Order_Origin_Source__c == 'P') {
                                   stagingPPPOutbound.Order_Dealer__c = 'SCECM';     
                                } else {
                                	//LM 02/18/2015: Corrected the dealer Id
                                    stagingPPPOutbound.Order_Dealer__c = 'SPSCM';
                                }
                            }
                        } else {
                            //orderDealerMap.put(ordr.Id, 'POS');
                            if(mapIdToOrder.get(asset.id).Bill_To__r.Country__c == 'US' || mapIdToOrder.get(asset.id).Bill_To__r.Country__c == 'USA') {
                                if(mapIdToOrder.get(asset.id).Order_Origin_Source__c == 'P') {
                                    stagingPPPOutbound.Order_Dealer__c = 'SPSN';
                                } else {
                                    stagingPPPOutbound.Order_Dealer__c = 'SCEA';
                                }
                            } else if(mapIdToOrder.get(asset.id).Bill_To__r.Country__c.contains('CA') || mapIdToOrder.get(asset.id).Bill_To__r.Country__c.contains('Ca')|| mapIdToOrder.get(asset.id).Bill_To__r.Country__c.contains('ca')) {
                                if(mapIdToOrder.get(asset.id).Order_Origin_Source__c == 'P') {
                                    stagingPPPOutbound.Order_Dealer__c = 'SCEC';
                                } else {
                                	//LM 02/18/2015: Corrected the dealer Id
                                    stagingPPPOutbound.Order_Dealer__c = 'SPSC';
                                }
                            }
                        }
                    }
                    staOutboundList.add(stagingPPPOutbound);
                }       
            }
        }

        System.debug('staOutboundList>>. '+staOutboundList);
        if(!staOutboundList.isEmpty()){
            try{
                insert staOutboundList;
                //upsert staOutboundList Unique_Key__c;
                OrderHelper.processOutbound = false;
            }
            catch(Exception Ex){
                system.debug(':::::::::::::::::: After Update:::Exception Occurred:::::::::::::::::'+Ex);
            }
        }
        System.debug('staOutboundList>>.222 '+staOutboundList);
    }
    
    public static void updateStagingObject(List<Asset__c> assetList, Map<Id, Asset__c> oldAssetMap){

        list<Staging_Asset__c> staAssetList = new list<Staging_Asset__c>();

        for(Asset__c asset :assetList){

            // MDM can be called only for assets with MDM Account ID 
            // Call MDM only when the asset has PPP info
            if(oldAssetMap.containsKey(asset.id) 
                    && asset.MDM_Account_ID__c != null 
                    && asset.MDM_Account_ID__c != ''){

                if(oldAssetMap.get(asset.id).PPP_Purchase_Date__c != asset.PPP_Purchase_Date__c
                        || oldAssetMap.get(asset.id).PPP_Start_Date__c != asset.PPP_Start_Date__c
                        || oldAssetMap.get(asset.id).PPP_End_Date__c != asset.PPP_End_Date__c
                        || oldAssetMap.get(asset.id).PPP_Status__c != asset.PPP_Status__c
                        || oldAssetMap.get(asset.id).PPP_Product__c != asset.PPP_Product__c){

                    Staging_Asset__c staAsset = getStagingRecord(asset);
                    staAssetList.add(staAsset);
                }//end-if
            }//end-if
        }//end-for

        if(!staAssetList.isEmpty()){
            try{
                insert staAssetList;
            }
            catch(Exception Ex){
                system.debug(':::::::::::::::::: After Update:::Exception Occurred:::::::::::::::::'+Ex);
            }
        }
    }

    //LM 05/20/2014 : added the second argument to the method for model number validation
    public static void updateFields(List<Asset__c> assetList, boolean isNewAsset){

        Set<String> modelNumbers = new Set<String>();
        Map<String, Id> mapModelNumberToProductID = new Map<String, Id>();
        Map<String, decimal> mapProductIDToDuration = new Map<String, decimal>();
        Map<String, Product__c> mapPPPProduct = new Map<String, Product__c>(); 
        Set<Id>blankModleNoForPrd = new Set<Id>();
        //LM 05/19/2014 : Added for model number validation
        map<String, String> productSKUlist = new map<String, string>();
        for (Product__c p : [select Id, sku__c from Product__c where status__c = 'Active' AND (sub_type__c = 'Hardware' OR sub_type__c = 'Peripherals' OR sub_type__c = 'Software')]) {
            productSKUlist.put(p.sku__c, p.Id);
        }
        
        for(Asset__c asset : assetList){
            boolean isProductAdded = false;
            //LM 05/20/2014 : updated below if statement for model number validation
            //if(asset.Model_Number__c != null){
            system.debug('<<<<<<<<isNewAsset>>>>>>>>'+ isNewAsset+'>>>>>>>>>>>>');
            system.debug('<<<<<<<<productSKUlist>>>>>>>>'+ productSKUlist+'>>>>>>>>>>>>');
            system.debug('<<<<<<<<contains>>>>>>>>'+ productSKUlist.ContainsKey(asset.Model_Number__c)+'>>>>>>>>>>>>');
            if(asset.Model_Number__c != null){
              if(isNewAsset && productSKUlist != null && productSKUlist.size()>0 && !productSKUlist.ContainsKey(asset.Model_Number__c)) {
               asset.Model_Number__c.addError('Invalid Model Number. Please select from the available list.');
              }
              else {
                modelNumbers.add(asset.Model_Number__c); 
                modelNumbers.add(asset.PPP_Product_SKU__c); 
                                        
                if(asset.Model_Number__c.contains(' ')){
                    modelNumbers.add(asset.Model_Number__c.substringBefore(' '));
                    System.debug('>>>>>modelNumbers='+modelNumbers);
                    isProductAdded =true;
                }                
              } //else
            } //model num if 

            if(!isProductAdded ||( asset.Model_Number__c!=null && !modelNumbers.contains(asset.Model_Number__c))){
                blankModleNoForPrd.add(asset.PPP_Product__c);
            }
        }

        if (modelNumbers.size()>0 || blankModleNoForPrd.size() > 0){
            // As per dicussion with DAN 
            /**normally there is only ONE SKU for hardware, peripheral, or software. 
                So, to find the asset product name, select for these 3 subtypes, and match the SKU. 
                That should produce the correct result. IN the case I showed, 
                it will not matter - the business owner will change the product table to correct the duplication***/
            for(Product__c product :[SELECT Id,SKU__c,Duration__c, Sub_Type__c 
                                     FROM Product__c where Status__c = 'Active' AND (SKU__c in:modelNumbers OR (ID IN : blankModleNoForPrd AND (Product_Type__c = 'ESP' OR Product_Type__c = 'ESP_CANADA')))]){
                if(product.Sub_Type__c == 'Hardware' || product.Sub_Type__c == 'Peripherals' || product.Sub_Type__c == 'Software') {
                    if(product.SKU__c!=null){   
                        mapModelNumberToProductID.put(product.SKU__c.toUpperCase(), product.Id);
                    }//end-if
                    
                }else if (product.sub_type__c != null && product.Sub_Type__c.contains('ESP') || product.Sub_Type__c.contains('AD') ){
                    mapProductIDToDuration.put(product.Id, product.Duration__c);
                    mapPPPProduct.put(product.Sku__c, product);}
            }
        }//end-if

        System.Debug('>>>>>>> +mapPPPProduct <<<<<<<<< '+mapPPPProduct);
        for(Asset__c asset : assetList){
            
            //LM 09-11-2014 : Added for PYB Orders. Agents/any other process should not update PPP dates for PYB orders
            system.debug('<<<<<<Asset PPP Record Lock>>>>>>>>'+asset.PPP_Record_Lock__c);
            ProcessPPPdates = asset.PPP_Record_Lock__c ? false : true;

            if(asset.Model_Number__c != null){

               if(mapModelNumberToProductID.get(asset.Model_Number__c.toUpperCase())<>null){
                asset.Product__c = mapModelNumberToProductID.get(asset.Model_Number__c.toUpperCase());
                }
                
                if(asset.Product__c==null && asset.Model_Number__c.contains(' ') && mapModelNumberToProductID.get(asset.Model_Number__c.substringBefore(' '))<>null){
                    asset.Product__c = mapModelNumberToProductID.get(asset.Model_Number__c.substringBefore(' '));

                }
             //LM 09-11-2014 : Added for PYB. Do not change Coverage type option for PYB PPP orders
             system.debug('<<<<<<ProcessPPPdates>>>>>>>>'+ProcessPPPdates);
             if(ProcessPPPdates) {
             asset.Coverage_Type__c=mapPPPProduct.get(asset.PPP_PRODUCT_SKU__c)<>null?mapPPPProduct.get(asset.PPP_PRODUCT_SKU__c).Sub_Type__c:'Standard';
             }

            }//end-if
            //T-237745 : Change Logic on PPP Start Date and PPP End Date on Asset
            decimal duration;
            System.Debug('>>>>>>> asset.PPP_Product__c  + ProcessPPPdates<<<<<<<<< ' +asset.PPP_Product__c+' '+ProcessPPPdates);
            
            //LM 09-11-2014: Added ProcessPPPdates condition. Do not recalculate PPP dates for PYB orders.            
            if(asset.PPP_Product__c <> null && ProcessPPPdates==true) {
                
                System.Debug('>>>>>>>+mapProductIDToDuration.get(asset.PPP_Product__c)<<<<<<<< ' +mapProductIDToDuration.get(asset.PPP_Product__c));
                
                if(mapProductIDToDuration.containsKey(asset.PPP_Product__c) && mapProductIDToDuration.get(asset.PPP_Product__c) != null)
                    duration = mapProductIDToDuration.get(asset.PPP_Product__c) ;

                if(asset.Coverage_Type__c != null && asset.Coverage_Type__c == 'AD') {

                    asset.PPP_Start_Date__c = asset.PPP_Purchase_Date__c;
                    if(duration != null){
                    
                        // Demo FeedBack
                         //asset.PPP_End_Date__c = asset.PPP_Start_Date__c == null ? null : asset.PPP_Start_Date__c.addMonths((Integer)duration+ 12);
                        
                        //LM 04/17/2014 : Updated logic to calc PPP End Date based on Proof Of Purchase Dt
                        // asset.PPP_End_Date__c = asset.Purchase_Date__c == null ? null : asset.Purchase_Date__c.addMonths((Integer)duration+ 12);
                        asset.PPP_End_Date__c = asset.Proof_of_Purchase_Date__c == null ? null : asset.Proof_of_Purchase_Date__c.addMonths((Integer)duration+ 12);
                        
                    }
                } else {
                    //If asset.Coverage_Type__c <> 'AD'
                    //LM 04/17/2014 : Updated logic to calc PPP Start Date based on Proof Of Purchase Dt
                    //asset.PPP_Start_Date__c = asset.Purchase_Date__c != null ? asset.Purchase_Date__c.addMonths( 12) : asset.Purchase_Date__c;
                    asset.PPP_Start_Date__c = asset.Proof_of_Purchase_Date__c != null ? asset.Proof_of_Purchase_Date__c.addMonths( 12) : asset.Proof_of_Purchase_Date__c;
                    if(duration != null )   
                        asset.PPP_End_Date__c = asset.PPP_Start_Date__c == null ? null : asset.PPP_Start_Date__c.addMonths( (Integer)duration);
                }
            }
            system.debug('*************asset >>'+asset);
        }    

    }

    public static void updateAssetCoverageType(list<Asset__c> newList, Map<id, Asset__c> oldMap) {
        Set<Id> productIds = new Set<Id>();
        map<Id, String> productTypeMap = new map<Id, String>();
        for(Asset__c ast : newList) {
            Asset__c oldAst = oldMap == null ? null : oldMap.get(ast.Id);
            if(oldAst == null || oldAst.PPP_Product__c <> ast.PPP_Product__c) {
                productIds.add(ast.PPP_Product__c);
            }
        }

        for(Product__c prod : [select Sub_Type__c from Product__c where Id IN : productIds]) {
            productTypeMap.put(prod.Id, prod.Sub_Type__c);
        }

        if (AssetManagement.isExchangeAsset != true) {
            for(Asset__c ast : newList) {
                String productType = ast.PPP_Product__c <> null ? productTypeMap.get(ast.PPP_Product__c) : null;
                if(productType <> null && productType == 'AD') {
                    ast.Coverage_Type__c = 'AD';
                } else if(productType <> null) {
                    ast.Coverage_Type__c = 'ESP';
                } 
            }
        }
    }

    public static Staging_Asset__c getStagingRecord(Asset__c asset) {

        Staging_Asset__c staAsset = new Staging_Asset__c();
        staAsset.Asset__c  = asset.Id;
        staAsset.MDM_Account_ID__c  = asset.MDM_Account_ID__c;
        staAsset.PPP_Start_Date__c = asset.PPP_Start_Date__c;
        staAsset.PPP_End_Date__c = asset.PPP_End_Date__c;
        staAsset.PPP_Purchase_Date__c = asset.PPP_Purchase_Date__c;
        staAsset.PPP_Product__c = asset.PPP_Product__c;
        staAsset.Product__c = asset.Product__c;

        if(asset.MDM_ID__c==null){
            staAsset.MDM_Asset_ID__c  = 'NEW-RECORD';   
        }else{
            staAsset.MDM_Asset_ID__c  = asset.MDM_ID__c;
        }

        return staAsset;        
    }

    public static void UpdateCancellationDate(list<Asset__c> newList, Map<id, Asset__c> oldMap){


        for (Asset__c asset: newList){
            if( asset.PPP_Status__c != null &&
                    oldMap.get(asset.id).PPP_Status__c != asset.PPP_Status__c && 
                    asset.PPP_Status__c.equals('Pending Cancel')){

                asset.ESP_Cancellation_Date__c=date.today();

            }
        }
    }

    public static Staging_PPP_Outbound__c getStagingOutboundRecord(Asset__c asset,Order__c o,
            Map<String, decimal> assurantMatrixMap,Map<Id, Order_Line__c> orderLineMap ) {
        System.debug(o.Id+'orderLineMap.get(o.Id) -- '+orderLineMap.get(o.Id));
        Order_Line__c oli = orderLineMap.get(o.Id);
        Staging_PPP_Outbound__c staOutbound = new Staging_PPP_Outbound__c();
        staOutbound.PPP_Contract_Number__c = asset.PPP_Contract_Number__c;
        staOutbound.PPP_Purchase_Date__c = formatDate(asset.PPP_Purchase_Date__c);
        staOutbound.Purchase_Date__c =  formatDate(asset.Purchase_Date__c);
        staOutbound.Proof_of_Purchase_Date__c =  formatDate(asset.Proof_of_Purchase_Date__c);
        staOutbound.PPP_Start_Date__c = formatDate(asset.PPP_Start_Date__c); 
        staOutbound.PPP_SKU__c = asset.PPP_Product_SKU__c;   
        staOutbound.Unit_Model_Number__c = asset.Model_Number__c; 
        staOutbound.Unit_Price__c = String.valueOf(asset.Price__c == null ? '0' : ''+asset.Price__c);
        staOutbound.Unit_Serial_Number__c = asset.Serial_Number__c; 
        staOutbound.PPP_Claim_Reason__c = asset.PPP_Claim_Reason__c;
        staOutbound.Order_Number__c = o.External_Order_Number__c;
        staOutbound.Billing_Address_1__c = o.Bill_To__r.Address_Line_1__c;
        staOutbound.Billing_Address_2__c = o.Bill_To__r.Address_Line_2__c;
        staOutbound.Billing_City__c = o.Bill_To__r.City__c;
        staOutbound.Billing_State__c = o.Bill_To__r.State__c;
        staOutbound.State_of_Sale__c = o.Bill_To__r.State__c;
        staOutbound.Billing_Country__c = o.Bill_To__r.Country__c;
        staOutbound.Country_of_Sale__c =  o.Bill_To__r.Country__c;
        staOutbound.Billing_Zip_Postal_Code__c = o.Bill_To__r.Postal_Code__c;
        staOutbound.Zip_Code_of_Sale__c = o.Bill_To__r.Postal_Code__c;

        staOutbound.Consumer_First_Name__c = o.Consumer__r.FirstName;
        staOutbound.Consumer_Last_Name__c = o.Consumer__r.LastName;
        staOutbound.Consumer_Email_Address__c = o.Consumer__r.Email;
        staOutbound.Order__c = o.Id;
        if(o.Consumer__r.Alt_Phone_Unformatted__c <> null) {
            staOutbound.Consumer_Area_Code_2__c = o.Consumer__r.Alt_Phone_Unformatted__c.length() > 3 ? o.Consumer__r.Alt_Phone_Unformatted__c.subString(0,3) : '';
            staOutbound.Consumer_Phone_Number_2__c = o.Consumer__r.Alt_Phone_Unformatted__c.length() > 10 ? o.Consumer__r.Alt_Phone_Unformatted__c.subString(3,10) : '';
        }


        if(o.Consumer__r.Phone_Unformatted__c<> null) { 

            staOutbound.Consumer_Area_Code_1__c = o.Consumer__r.Phone_Unformatted__c.length() > 3 ? o.Consumer__r.Phone_Unformatted__c.subString(0,3) : '';
            staOutbound.Consumer_Phone_Number_1__c = o.Consumer__r.Phone_Unformatted__c.length() >= 10 ? o.Consumer__r.Phone_Unformatted__c.subString(3,10) : '';
        }

        staOutbound.Order_Dealer__c = o.ESP_Dealer__c; 
        staOutbound.Order_Record_Type__c = 'C';


        Integer daysbetweenCancel = 0;
        if(Asset.ESP_Cancellation_Date__c!=null && Asset.PPP_Purchase_Date__c!=null){



            daysbetweenCancel =Asset.PPP_Purchase_Date__c.daysbetween(Asset.ESP_Cancellation_Date__c);

            System.debug('>>>>>>>>>>+daysbetweenCancel<<<<<<<<<<<< ' +daysbetweenCancel);
        }
        //Full Refund (Cancel  ON or After 30 days from PPP Purchase):
        if(daysbetweenCancel>30){  



            staOutbound.PPP_Cancel_After_30_Days__c = daysbetweenCancel>30 ? 'R':'';

        }

        //staOutbound.PPP_Cancel_After_30_Days__c = o<> null ? String.valueOf(o.ESP_Cancel_After_30_Days__c) : '';
        staOutbound.PPP_Tax__c = oli <> null ? String.valueOf(o.Total_Tax__c) : '';
        staOutbound.PPP_Refund_by_Dealer__c = o<> null ? String.valueOf(o.ESP_Refunded_by_Dealer__c) : '';

        if(Asset.ESP_Cancellation_Date__c <> null){
            staOutbound.PPP_Cancel_Date__c= formatdate(Asset.ESP_Cancellation_Date__c);
        }
        if(o.Case__c != null){
            staOutbound.Loss_Date__c = o.Case__c <> null ? '' : formatDate(o.Case__r.CreatedDate.date());
            staOutbound.Invoice_Number__c = o.Case__r.CaseNumber; 
            staOutbound.Invoice_Date__c = formatDate(o.Case__r.Invoice_Date__c);
            staOutbound.Invoice_Amount__c = String.valueOf(o.Case__r.Invoice_Amount__c);
        }
        decimal dealerCost = assurantMatrixMap.get(staOutbound.PPP_SKU__c + '-' + staOutbound.Order_Dealer__c);
        staOutbound.PPP_Dealer_Cost__c = dealerCost == null ? '' : String.valueOf(dealerCost);
        staOutbound.PPP_List_Price__c = String.valueOf(o.Order_Total__c);
        staOutbound.Unique_Key__c= staOutbound.Order_Record_Type__c + asset.Serial_Number__c;

        System.debug('>>>>>>>>>asset.ESP_Cancellation_Date__c<<<<<<<<)' +asset.ESP_Cancellation_Date__c);

        staOutbound.PPP_Cancel_Date__c= formatDate(asset.ESP_Cancellation_Date__c);

        System.debug('>>>>> formatDate((asset.ESP_Cancellation_Date__c)<<<<<<<<<<<)' + formatDate((asset.ESP_Cancellation_Date__c)));
        return staOutbound;     
    }  

    private static String formatDate(Date dt) {
        if(dt == null) return '';
        String month = dt.month() < 10 ? '0' + dt.month() : '' + dt.month();
        String day = dt.day() < 10 ? '0' + dt.day() : '' + dt.day();
        return '' + month  + day + dt.year();
    }       


}