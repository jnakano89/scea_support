//10/22/2013 : Urminder(JDC) : created this test class.
@isTest
global class LinkConsumerController_Test {

    static testMethod void myUnitTest() { 
       Test.startTest();
       Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
       
       ApexPages.currentPage().getParameters().put('email', 'Test@testid.com');
       ApexPages.currentPage().getParameters().put('ctRadio', '2-2-2-2');
       LinkConsumerController ctrl = new LinkConsumerController();
       ctrl.callGetConsumers();
       ctrl.consumerPhoneToSearch = '1234';
       ctrl.consumerEmailToSearch = 'test@email.com.test';
       ctrl.consumerPSNOnlineIdToSearch = 'Test@testid.com';
       
       ctrl.gotoAccount();
       ctrl.searchConsumerByFilter();
       
       ctrl.consumerPhoneToSearch = '1234';
       ctrl.consumerEmailToSearch = '';
       ctrl.consumerPSNOnlineIdToSearch = 'Test@testid.com';
       ctrl.searchConsumerByFilter();
       
       ctrl.consumerPhoneToSearch = '';
       ctrl.consumerEmailToSearch = '';
       ctrl.consumerPSNOnlineIdToSearch = '';
       ctrl.searchConsumerByFilter();
       
       Test.stopTest();
    }
    static testMethod void myUnitTest2() {
       
        
        Test.startTest();
        String rtId;
        for(RecordType rt : [select Id from RecordType where SobjectType = 'Account']) {
            rtId = rt.Id;
            break;
        }
        Account account = new Account();
        account.Name = 'test contact';
        account.RecordTypeId = rtId;
        insert account;
        
       Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
       ApexPages.currentPage().getParameters().put('accountId', account.Id);
       ApexPages.currentPage().getParameters().put('email', 'Test@testid.com');
       ApexPages.currentPage().getParameters().put('ctRadio', '2-2-2-2');
       LinkConsumerController ctrl = new LinkConsumerController();
       try{
       ctrl.fetchConsumer(null, null, 'Test@testid.com');
       } catch(Exception ex){}
        
       Test.stopTest();
       ctrl.selectConsumer();
       //ctrl.refereshConsumer();
       ctrl.searchConsumer();
    }
    static testMethod void myUnitTest3() {
       Test.startTest();
       Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
       ApexPages.currentPage().getParameters().put('ctRadio', '2-2-2-2');
       ApexPages.currentPage().getParameters().put('ANI', '1234');
       LinkConsumerController ctrl = new LinkConsumerController();
       list<contact> cntList=TestClassUtility.createContact(1, true) ;     
       String rtId;
       for(RecordType rt : [select Id from RecordType where SobjectType = 'Account']) {
            rtId = rt.Id;
            break;
        }
        Account account = new Account();
        account.Name = 'test contact';
        account.RecordTypeId = rtId;
        insert account;
        Contact contact = new Contact();
        contact.LastName = 'lname';
        contact.AccountId = account.Id;
        contact.MDM_ID__c = '2-2-2-2';
        insert contact;
        
       
       
       //ctrl.selectConsumer();
       ctrl.selectedcontact= cntList[0];
        Sony_MiddlewareConsumerdata3.Address_mdm address = new Sony_MiddlewareConsumerdata3.Address_mdm();
        address.AddressId = '1-1-1-1';
        address.City = 'City';
        address.ContactAddressType = 'Test';
        address.Country = 'US';
        address.PostalCode = '123456';
        address.State = 'LA';
        address.StreetAddress = 'AddressLine1';
        address.StreetAddress2 = 'AddressLine2';
        address.StreetAddress3 = 'AddressLine3';
        
        Sony_MiddlewareConsumerdata3.ListOfAddress_mdm listOfAddress = new Sony_MiddlewareConsumerdata3.ListOfAddress_mdm();
        listOfAddress.address = new list<Sony_MiddlewareConsumerdata3.Address_mdm>{address};
        
        Sony_MiddlewareConsumerdata3.Contact_mdm cnt = new Sony_MiddlewareConsumerdata3.Contact_mdm();
        cnt.ListOfAddress = listOfAddress;
        cnt.MDMRowId = '2-2-2-2';
        cnt.PSNSignInId = 'Test@testid.com';
        cnt.LastName = 'Test Contact';
        cnt.CRMPhone = '1234567890';
        
        Sony_MiddlewareConsumerdata3.ListOfContact_mdm lCnt = new Sony_MiddlewareConsumerdata3.ListOfContact_mdm();
        lCnt.Contact = new list<Sony_MiddlewareConsumerdata3.Contact_mdm>{cnt};
        
        Sony_MiddlewareConsumerdata3.Account_mdm acc = new Sony_MiddlewareConsumerdata3.Account_mdm();
        acc.ListOfContact = lCnt;
        acc.MDMAccountRowId = '3-3-3-3';
        acc.AccountStatus = 'Active';
        acc.AccountSuspendDate = String.valueOf(date.today());
        //Sony_MiddlewareConsumerdata3.ListOfAccount_mdm lAcc = new Sony_MiddlewareConsumerdata3.ListOfAccount_mdm();
        //lAcc.Account = new list<Sony_MiddlewareConsumerdata3.Account_mdm>{acc};
        
        ConsumerSearchResult searchResult = new ConsumerSearchResult();
        searchResult.listOfAccounts = new list<Sony_MiddlewareConsumerdata3.Account_mdm>{acc};
        ctrl.searchResult = searchResult;
        ctrl.selectConsumer();
        test.stopTest();
        ctrl.refreshConsumer();
        ctrl.resetSearchStatus();
        ctrl.createAssetRecords();
        ApexPages.StandardController controller;
        LinkConsumerController ctrl2 = new LinkConsumerController(controller);
        //Test.stopTest();
    }
    global class WebServiceMockImpl implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,    
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
                Sony_MiddlewareConsumerdata3.PSConsumerLookup_Output_element responseElm = 
                    new Sony_MiddlewareConsumerdata3.PSConsumerLookup_Output_element();
                
                
                Sony_MiddlewareConsumerdata3.Address_mdm address = new Sony_MiddlewareConsumerdata3.Address_mdm();
                address.AddressId = '1-1-1-1';
                address.City = 'City';
                address.ContactAddressType = 'Test';
                address.Country = 'US';
                address.PostalCode = '123456';
                address.State = 'LA';
                address.StreetAddress = 'AddressLine1';
                address.StreetAddress2 = 'AddressLine2';
                address.StreetAddress3 = 'AddressLine3';
                
                Sony_MiddlewareConsumerdata3.ListOfAddress_mdm listOfAddress = new Sony_MiddlewareConsumerdata3.ListOfAddress_mdm();
                listOfAddress.address = new list<Sony_MiddlewareConsumerdata3.Address_mdm>{address};
                
                Sony_MiddlewareConsumerdata3.Contact_mdm cnt = new Sony_MiddlewareConsumerdata3.Contact_mdm();
                cnt.ListOfAddress = listOfAddress;
                cnt.MDMRowId = '2-2-2-2';
                cnt.PSNSignInId = 'Test@testid.com';
                cnt.LastName = 'Test Contact';
                cnt.OnlineId = 'Test@testid.com';
                
                Sony_MiddlewareConsumerdata3.ListOfContact_mdm lCnt = new Sony_MiddlewareConsumerdata3.ListOfContact_mdm();
                lCnt.Contact = new list<Sony_MiddlewareConsumerdata3.Contact_mdm>{cnt};
                
                Sony_MiddlewareConsumerdata3.Account_mdm acc = new Sony_MiddlewareConsumerdata3.Account_mdm();
                acc.ListOfContact = lCnt;
                acc.MDMAccountRowId = '3-3-3-3';
                acc.AccountStatus = 'Active';
                
                Sony_MiddlewareConsumerdata3.ListOfAccount_mdm lAcc = new Sony_MiddlewareConsumerdata3.ListOfAccount_mdm();
                lAcc.Account = new list<Sony_MiddlewareConsumerdata3.Account_mdm>{acc};
                
                responseElm.ListOfAccount = lAcc;
                             
                response.put('response_x', responseElm); 
               }
    }
    
     global class WebServiceMockConsumerRefresh implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,    
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
                Sony_MiddlewareConsumerRefresh_Final.PSConsumerRefresh_Output_element responseElm = 
                    new Sony_MiddlewareConsumerRefresh_Final.PSConsumerRefresh_Output_element();
                
                
                Sony_MiddlewareConsumerRefresh_Final.Address_mdm address = new Sony_MiddlewareConsumerRefresh_Final.Address_mdm();
                address.AddressId = '1-1-1-1';
                address.City = 'City';
                address.ContactAddressType = 'Test';
                address.Country = 'US';
                address.PostalCode = '123456';
                address.State = 'LA';
                address.StreetAddress = 'AddressLine1';
                address.StreetAddress2 = 'AddressLine2';
                address.StreetAddress3 = 'AddressLine3';
                
                Sony_MiddlewareConsumerRefresh_Final.ListOfAddress_mdm listOfAddress = new Sony_MiddlewareConsumerRefresh_Final.ListOfAddress_mdm();
                listOfAddress.address = new list<Sony_MiddlewareConsumerRefresh_Final.Address_mdm>{address};
                
                Sony_MiddlewareConsumerRefresh_Final.Account_mdm acc = new Sony_MiddlewareConsumerRefresh_Final.Account_mdm();
                
                acc.MDMAccountRowId = '3-3-3-3';
                acc.AccountStatus = 'Active';
                
                //code added by preetu
                  acc.PSPlusSubscriber = 'true';
                  acc.PSPlusStartDate = '2014-01-20 00:00:00';
                  acc.PSPlusStackedEndDate = '2014-01-20 00:00:00';
                  acc.PSPlusAutoRenewalFlag = 'True';
                
                //Sony_MiddlewareConsumerRefresh_Final.ListOfAccount_mdm lAcc = new Sony_MiddlewareConsumerRefresh_Final.ListOfAccount_mdm();
                //lAcc.Account = new list<Sony_MiddlewareConsumerRefresh_Final.Account_mdm>{acc};
                
                Sony_MiddlewareConsumerRefresh_Final.Contact_mdm cnt = new Sony_MiddlewareConsumerRefresh_Final.Contact_mdm();
                cnt.ListOfAddress = listOfAddress;
                cnt.MDMRowId = '2-2-2-2';
                cnt.PSNSignInId = 'Test@testid.com';
                cnt.LastName = 'Test Contact';
                cnt.Account = new list<Sony_MiddlewareConsumerRefresh_Final.Account_mdm>{acc};
                
                Sony_MiddlewareConsumerRefresh_Final.ListOfContact_mdm lCnt = new Sony_MiddlewareConsumerRefresh_Final.ListOfContact_mdm();
                lCnt.Contact = new list<Sony_MiddlewareConsumerRefresh_Final.Contact_mdm>{cnt};
                
                responseElm.listOfContact = lCnt;
                             
                response.put('response_x', responseElm); 
               }
    }
    
}