@isTest
private class UpdateWarrantyScheduler_Test {

    static testMethod void myUnitTest() {
       
        List<Asset__c> astList  = TestClassUtility.createAssets(10, true);
        List<Order__c> ordrList = TestClassUtility.creatOrder(null, 1, true);
        
        String astName;
        String ordrName;
        
        for(Asset__c ast : [select name from asset__c where id = :astList[0].Id]) {
        	astName = ast.name;
        }
        
        for(Order__c ordr : [select name from Order__c where id = :ordrList[0].Id]) {
        	ordrName = ordr.name;
        }
       
       Case testcase = new Case();
       testcase.Status = 'Active';
       testcase.received_date__c = date.today();
       testcase.shipped_date__c = date.today().addDays(1);
       testcase.outbound_tracking_number__c = '101';
       testcase.outbound_carrier__c = 'TYest1';
       testCase.Offender__c = 'Test';
       testCase.Order__c = ordrList[0].Id;
       testCase.Product__c = 'PS4';
       testCase.Sub_Area__c = 'Account Help';
       insert testCase;
       
       Staging_Siebel_PPP_Inbound__c inbound = new Staging_Siebel_PPP_Inbound__c();
       inbound.Asset_Number__c = astName;
       inbound.Asset_Status__c = 'New';
       inbound.Contract_Number__c = '123123';
       inbound.PPP_Start_Date__c = date.today();
       inbound.PPP_End_Date__c = date.today().addDays(1);
       inbound.Order_Number__c = ordrName;
       inbound.Refund_Amount__c = 10;
       inbound.Comments__c = 'Test';
       inbound.Order_Status__c = 'Updated';
       insert inbound;
           	
    	Test.startTest();
    	
    	UpdateWarrantyScheduler batch = new UpdateWarrantyScheduler();
		Database.executeBatch(batch, 1);
			
		Test.stopTest();
    }
}