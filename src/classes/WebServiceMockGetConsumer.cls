/******************************************************************************************************************************************************
 CREATED BY : Leena Mandadapu 03/25/2015 : Jira# SMS-1080 - Web console Deactivation Requests
********************************************************************************************************************************************************/
global class WebServiceMockGetConsumer implements WebServiceMock {
       
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,    
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
               Sony_MiddlewareConsumerdata3.PSConsumerLookup_Output_element responseElm = 
                    new Sony_MiddlewareConsumerdata3.PSConsumerLookup_Output_element();

                Sony_MiddlewareConsumerdata3.Address_mdm address = new Sony_MiddlewareConsumerdata3.Address_mdm();
                address.AddressId = '12345';
                address.City = 'TestCity';
                address.ContactAddressType = 'Billing';
                address.Country = 'TestUS';
                address.PostalCode = '123456';
                address.State = 'TestState';
                address.StreetAddress = 'Test AddressLine1';
                address.StreetAddress2 = 'Test AddressLine2';
                address.StreetAddress3 = 'Test AddressLine3';
                
                Sony_MiddlewareConsumerdata3.ListOfAddress_mdm listOfAddress = new Sony_MiddlewareConsumerdata3.ListOfAddress_mdm();
                listOfAddress.address = new list<Sony_MiddlewareConsumerdata3.Address_mdm>{address};
                
                Sony_MiddlewareConsumerdata3.Contact_mdm cnt = new Sony_MiddlewareConsumerdata3.Contact_mdm();
                cnt.ListOfAddress = listOfAddress;
                cnt.MDMRowId = '123321';
                cnt.PSNSignInId = 'ApexTest@test.com';
                cnt.LastName = 'Apex LastName';
                cnt.PSNEmail = 'Test@test.com';
                cnt.FirstName = 'Apex FirstName';
                cnt.PSNPhone = '1234567890';
                cnt.sex='Test';
                cnt.PSNSignInId='test';
                cnt.OnlineId='ApexTest';
                
                
                Sony_MiddlewareConsumerdata3.ListOfContact_mdm lCnt = new Sony_MiddlewareConsumerdata3.ListOfContact_mdm();
                lCnt.Contact = new list<Sony_MiddlewareConsumerdata3.Contact_mdm>{cnt};
                
                Sony_MiddlewareConsumerdata3.Account_mdm acc = new Sony_MiddlewareConsumerdata3.Account_mdm();
                acc.ListOfContact = lCnt;
                acc.MDMAccountRowId = '99999';
                acc.AccountStatus = 'Active';
                acc.PSPlusStartDate = '2014-01-30 12:12:00';
                acc.PSPlusStackedEndDate = '2015-01-31 12:12:00';
                acc.PSPlusAutoRenewalFlag = 'True';
                acc.PSPlusAccountCountry = 'USA';
                acc.AccountSuspendDate ='';
                acc.CreatedDate = '2014-01-30';
                acc.PSNAccountId = '3-3-3';
                acc.PSPlusAccountCountry = 'USA';
                
                
                Sony_MiddlewareConsumerdata3.ListOfAccount_mdm lAcc = new Sony_MiddlewareConsumerdata3.ListOfAccount_mdm();
                lAcc.Account = new list<Sony_MiddlewareConsumerdata3.Account_mdm>{acc};
                
                responseElm.ListOfAccount = lAcc;
                             
                response.put('response_x', responseElm); 
               }
 }