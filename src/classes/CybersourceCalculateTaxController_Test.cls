@isTest(seeAllData = true)
global class CybersourceCalculateTaxController_Test {
	static testmethod void myUnitTest() {
		Tax_Nexus__c tn = new Tax_Nexus__c();
		tn.State_Code__c = 'TS';
		tn.Name = 'TS';
		insert tn;
		
		Case cs = TestClassUtility.createCase('open', '', false);
		
		Product__c prod = TestClassUtility.createProduct(1,false)[0];
		prod.List_Price__c = 100;
		prod.Exchange_Country__c = 'US';
		insert prod;
		
		Address__c shipTo = TestClassUtility.createAddress(1, true)[0];
		
		Asset__c ast = TestClassUtility.createAssets(1, false)[0];
		//AB - 05/23/2014 - Removed product reference to prevent model validation failure
		//ast.Model_Number__c = prod.SKU__c;
		ast.Model_Number__c = 'CUH-1001A';
		insert ast;
		
		Asset__c ast2 = [select Product__c,Product_SKU__c from Asset__c where Id = :ast.id];
		
		cs.Fee_Type__c = 'Out Of Warranty';
		cs.Ship_To__c = shipTo.Id;
		cs.Asset__c = ast.Id;
		insert cs;
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new HTTPMockCyberSourceTax());
		ApexPages.StandardController sc = new ApexPages.StandardController(cs);
		CybersourceCalculateTaxController ctrl = new CybersourceCalculateTaxController(sc);
		ctrl.udpateTaxInCase();
		ApexPages.currentPage().getParameters().put('proceedToPayment','true');
		ctrl.validateRedirect();
		Test.stopTest();

	}
	
	//LM 07/23/2014: Added for PPP State Tax
	static testmethod void myUnitTestPPPTax() {
		Tax_Nexus__c tn = new Tax_Nexus__c();
		tn.State_Code__c = 'TC';
		tn.Name = 'TC';
		tn.PPP_Tax__c = true;
		tn.Service_Tax__c = false;
		insert tn;
		
		Case cs = TestClassUtility.createCase('open', '', false);
		cs.RecordTypeId =  GeneralUtiltyClass.RT_PURCHASE_ID;
		//insert cs;
		
		
		Product__c prod = TestClassUtility.createProduct(1,false)[0];
		prod.List_Price__c = 59.99;
		prod.Exchange_Country__c = 'US';
		insert prod;
		
		Address__c shipTo = TestClassUtility.createAddress(1, false)[0];
		shipTo.State__c = 'TC';
		insert shipTo;
		
		Asset__c ast = TestClassUtility.createAssets(1, false)[0];
		//AB - 05/23/2014 - Removed product reference to prevent model validation failure
		//ast.Model_Number__c = prod.SKU__c;
		ast.Model_Number__c = 'CUH-1001A';
		insert ast;
		
		Asset__c ast2 = [select Product__c,Product_SKU__c from Asset__c where Id = :ast.id];
		
		cs.Ship_To__c = shipTo.Id;
		cs.Asset__c = ast.Id;
		cs.SCEA_Product__c = prod.Id;
		insert cs;
		
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new HTTPMockCyberSourceTax());
		ApexPages.StandardController sc = new ApexPages.StandardController(cs);
		CybersourceCalculateTaxController ctrl = new CybersourceCalculateTaxController(sc);
		ctrl.udpateTaxInCase();
		ApexPages.currentPage().getParameters().put('proceedToPayment','true');
		ctrl.validateRedirect();
		Test.stopTest();
	}	
	
	 global class HTTPMockCyberSourceTax implements HttpCalloutMock  {
	   global HTTPResponse respond(HTTPRequest req) {
	        HttpResponse res = new HttpResponse();
	        //res.setHeader('Content-Type', 'application/json');
	        String body = getBody();
	        res.setBody(body);
	        res.setStatusCode(200);
	        return res;
	   }
	   String getBody() {
	   	String body = '<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Header><wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><wsu:Timestamp xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="Timestamp-1148515799"><wsu:Created>2013-08-21T00:07:47.324Z</wsu:Created></wsu:Timestamp></wsse:Security></soap:Header><soap:Body><c:replyMessage xmlns:c="urn:schemas-cybersource-com:transaction-data-1.88"><c:merchantReferenceCode>203440404</c:merchantReferenceCode><c:requestID>3770436672150176056428</c:requestID><c:decision>ACCEPT</c:decision><c:reasonCode>100</c:reasonCode><c:requestToken>AhijLwSRmMqj4WHCaYjYIJ+sNTPKWOqrTKCGTSTKuj0ld/QAwjix</c:requestToken><c:taxReply><c:reasonCode>100</c:reasonCode><c:grandTotalAmount>109.25</c:grandTotalAmount><c:totalCityTaxAmount>1.00</c:totalCityTaxAmount><c:city>SAN MATEO</c:city><c:totalCountyTaxAmount>0.00</c:totalCountyTaxAmount><c:totalDistrictTaxAmount>1.75</c:totalDistrictTaxAmount><c:totalStateTaxAmount>6.50</c:totalStateTaxAmount><c:state>CA</c:state><c:totalTaxAmount>9.25</c:totalTaxAmount><c:postalCode>94404</c:postalCode><c:item id="0"><c:cityTaxAmount>1.00</c:cityTaxAmount><c:countyTaxAmount>0.00</c:countyTaxAmount><c:districtTaxAmount>1.75</c:districtTaxAmount><c:stateTaxAmount>6.50</c:stateTaxAmount><c:totalTaxAmount>9.25</c:totalTaxAmount></c:item></c:taxReply></c:replyMessage></soap:Body></soap:Envelope>' ;
		return body; 
	   }
    }
}