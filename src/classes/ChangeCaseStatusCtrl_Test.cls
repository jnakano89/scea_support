//8/17/2013 : Urminder : created this test class
//05/20/2014 : Agetz : edited to match edits to ChangeCaseStatusCtrl SF-198
@isTest
private class ChangeCaseStatusCtrl_Test {
    static Case testcase;
    static testMethod void myUnitTest() {
      createData();
      ApexPages.Standardcontroller stdCtrl = new ApexPages.Standardcontroller(testCase);
      ChangeCaseStatusCtrl ctrl  = new ChangeCaseStatusCtrl(stdCtrl);
      ctrl.inputText = 'Status of this Case is updated';
      //AG 05/20/2014 ctrl.cs.Status = 'Updated';
      ctrl.save();
      
      Case cs = [select Status from Case where Id = : testCase.Id];
      //AG 05/20/2014 System.assertEquals(cs.Status,'Updated','Case Status should be updated');
      
      FeedItem fi = [select Id, Body from FeedItem where ParentId = :cs.Id];
      System.assertNotEquals(fi.Id,null,'New Feed should be created'); 
      
      CaseComment com = [select Id, CommentBody from CaseComment where ParentId = :cs.Id];//AG 05/20/2014
      System.assertNotEquals(com.Id,null,'New Case Comment should be created');//AG 05/20/2014
      
      //for Coverage
      ApexPages.Standardcontroller stdCtrl2 = new ApexPages.Standardcontroller(new Case());
      ChangeCaseStatusCtrl ctrl2  = new ChangeCaseStatusCtrl(stdCtrl2);
      ctrl2.save();
    }
    static void createData() {
     Contact cnct = new Contact();
     cnct.LastName = 'test';
     cnct.FirstName = 'ftest';
     cnct.Phone = '12345';
     insert cnct;
     
     testcase = new Case();
     testcase.Status = 'Open';
     testcase.received_date__c = date.today();
     testcase.shipped_date__c = date.today().addDays(1);
     testcase.outbound_tracking_number__c = '101';
     testcase.outbound_carrier__c = 'TYest1';
     testCase.Origin = 'Social';
     testCase.ContactId = cnct.Id;
     testCase.Offender__c = 'Test';
     testCase.Product__c = 'PS4';
     testCase.Sub_Area__c = 'Account Help';
     insert testCase;
     
    }
}