@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    public String responseBody {get; set;}
    
    global MockHttpResponseGenerator(String responseBody)
    {
        this.responseBody = responseBody;
    }
    global HTTPResponse respond(HTTPRequest req) {  
        // Create a fake response
        HttpResponse res = new HttpResponse();
        //res.setHeader('Content-Type', 'application/json');
        res.setBody(responseBody);
        res.setStatusCode(200);
        return res;
    }
    static testMethod void testMockHttpResponseGenerator(){
        MockHttpResponseGenerator ctrl = new MockHttpResponseGenerator(' test Body ');
        ctrl.respond(null);        
    }   
}