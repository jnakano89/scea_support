/******************************************************************************
Class         : CaseService
Description   : A webservice class to create a  new CaseInfo record
Developed by  : Vinod Kumar (Appirio offshore)
Date          : August 3, 2013
Task		  : T-166691                 
******************************************************************************/
global class CaseService {

	 webservice static CaseInfo getCase(String caseNumber){

		/*
		
			Input:
			Service Request Number: The numeric portion of the consumers’ service request number.
		
			Output:
			Service Request Number: Current service request unique identifier.
			Service Request Status: Current status of the service request.
			Date Received: Date the inbound package was received.
			Date Shipped: Date that the outbound package was shipped.
			Tracking Number: Package tracking number.
			Carrier: The package shippers name.
 
		
		*/	

	 	CaseInfo objCaseInfo;

	 	for(Case c :[Select Status, Shipped_Date__c, Received_Date__c, External_SR_Number__c, Outbound_Tracking_Number__c, 
	 							Outbound_Carrier__c, Id, CaseNumber 
	 							From Case 
	 							where (CaseNumber =:caseNumber OR External_SR_Number__c  =:caseNumber) limit 1]){
	 								
 
	 	 	// create CaseInfo object 
		 	objCaseInfo = new CaseInfo(c);
	 	
	 	}

	 	return objCaseInfo;
	 }
	 
	 // Class structure for the CaseInfo 
	  
	 global class CaseInfo{ 
	 	
		webservice date receivedDate{get;set;} 
		webservice date shippedDate{get;set;} 
		webservice string carrier{get;set;} 
		webservice string trackingNumber{get;set;} 
		webservice string status{get;set;} 
		webservice string caseNumber{get;set;} 
		
		public CaseInfo(Case selectedCase){
			
			this.receivedDate = selectedCase.received_date__c<>null?selectedCase.received_date__c:null;
			this.shippedDate = selectedCase.shipped_date__c<> null?selectedCase.shipped_date__c:null;
			this.carrier = selectedCase.outbound_carrier__c<> null?selectedCase.outbound_carrier__c:'' ;
			this.trackingNumber = selectedCase.outbound_tracking_number__c<>null?selectedCase.outbound_tracking_number__c:'';
			this.status = selectedCase.status<>null?selectedCase.status:'';
			this.caseNumber = selectedCase.CaseNumber<>null?selectedCase.CaseNumber:'';
		}
		
	}
}