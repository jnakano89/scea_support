public with sharing class ExceptionHandler {
	
	public static void logException(Exception  ex){		
		logException(ex, GenericOAuth.webServiceEnum.None);
	}
	
	public static void logException(Exception  ex, GenericOAuth.webServiceEnum TypeName){
		Savepoint sp = Database.setSavepoint();

		SysException__c e = new SysException__c(); 
		LIST<DMLSysException__c> dmlexceptions = new LIST<DMLSysException__c>();
		String message = getTruncatedString(ex.getMessage());
		String stacktrace = getTruncatedString(ex.getStackTraceString());

		e.Line_Number__c = ex.getLineNumber();
		e.Message__c = message;
		e.Stack_Trace__c = stacktrace;
		if(TypeName!=GenericOAuth.webServiceEnum.None && GenericOAuth.webServiceEnum.None!=null){
			e.Type__c = TypeName.name();
		}else{
			e.Type__c = ex.getTypeName();
		}
		e.Class_Origination__c = getClassOrigination(stacktrace);
		e.Trigger_Origination__c = getTriggerOrigination(stacktrace);
		insert e; // insert new SysException__c record

		if (ex.getTypeName() == 'System.DmlException' || ex.getTypeName() == 'System.EmailException')
			// if Exception Type = DMLException or EmailException log all DMLExceptions being reported
		{
			DMLSysException__c dmlex;
			for (integer i = 0; i < ex.getNumDml(); i++)
			{
				dmlex = new DMLSysException__c();
				dmlex.DML_Field_Names__c = getFieldNames(ex.getDmlFieldNames(i));
				dmlex.DML_ID__c = ex.getDmlId(i);
				dmlex.DML_Index__c = ex.getDmlIndex(i);
				dmlex.DML_Message__c = getTruncatedSTring(ex.getDmlMessage(i));
				dmlex.DML_Type__c = ex.getDmlType(i).name();
				dmlex.System_Exception__c = e.Id;
				dmlexceptions.add(dmlex);
			}
			insert dmlexceptions;
		}
	}

	public static void logException(String message, String stacktrace, GenericOAuth.webServiceEnum TypeName, String ClassOrigination, String TriggerOrigination)
	{
		logException(message, stacktrace, TypeName.name(), ClassOrigination, TriggerOrigination);
	}	

	public static void logException(String message, String stacktrace, String TypeName, String ClassOrigination, String TriggerOrigination)
	{
		Savepoint sp = Database.setSavepoint();

		SysException__c e = new SysException__c(); 
		//e.Line_Number__c = LineNumber;
		e.Message__c = message;
		e.Stack_Trace__c = stacktrace;
		e.Type__c = TypeName;
		e.Class_Origination__c = ClassOrigination;
		e.Trigger_Origination__c = TriggerOrigination;
		insert e; // insert new SysException__c record
	}


	public static String getTruncatedString(String s)
	{
		s = String.escapeSingleQuotes(s);
		if (s.length() < 32768) return s;
		else return s.substring(0, 32767);
	}

	public static String getFieldNames(LIST<String> strings)
	{
		String fieldnames = '';
		integer i = 0;

		for (String fieldname : strings)
		{
			if ((fieldnames.length() + fieldname.length() + 2) < 32767)
			{
				fieldnames += fieldname;
				if (i <= strings.size() - 1) fieldnames += ', '; // append a comma if the field name is NOT the last element
				i++;
			}
			else break;
		}
		return fieldnames;
	}

	public static String getClassOrigination(String stacktrace)
	{
		integer indexStart = stacktrace.indexOf('Class.', 0);
		integer indexEnd = stacktrace.indexOf(':', 0);
		if ((indexStart < indexEnd) && (indexStart != -1 && indexEnd != -1))
		{
			return stacktrace.substring(indexStart, indexEnd);
		}
		else return null;
	}

	public static String getTriggerOrigination(String stacktrace)
	{
		integer indexStart = stacktrace.indexOf('Trigger.', 0);
		integer indexEnd = stacktrace.indexOf(':', 0);
		if ((indexStart < indexEnd) && (indexStart != -1 && indexEnd != -1))
		{
			return stacktrace.substring(indexStart, indexEnd);
		}
		else return null;
	}
}