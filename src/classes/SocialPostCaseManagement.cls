/******************************************************************************************************************************************************************
CREATED BY: SALESFORCE PROFESSIONAL SERVICES TEAM

UPDATED BY:
            11/20/2014  : Leena Mandadapu : Jira# SMS-642 - Added debug statements.
            12/01/2014  : Leena Mandadapu : SMS-725 : Added code to setup Social_content__c field on Case - Updated code to set correct OwnerId and RecordTypeId
            02/25/2015  : Leena Mandadapu : SMS-813 : Added logic to handle null pointer exception when the Topic Profile Name = null.
            02/25/2015  : Leena Mandadapu : SMS-813 : Updated Test Twitter handle to @robotpod.
*********************************************************************************************************************************************************************/
public with sharing class SocialPostCaseManagement {

    public static void beforeInsert(List < SocialPost > spList) {
      system.debug('#####################################################################################');
      system.debug('<<<<<<<<<<Inside SocialPostCaseManagement - beforeInsert>>>>>>>>>');
      system.debug('<<<<<<<<<<Incoming Social Post>>>>>>>>>'+spList);
      Case newCase = new Case();
        //LM Added
        String RT_CASE_SOCIAL_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Social').getRecordTypeId();
      Group OWNER_SOCIAL = [SELECT Id FROM Group WHERE Name = 'Social Support' AND Type = 'Queue' ][0];
        SonyInboundSocialPostHandlerImpl sisphi = new SonyInboundSocialPostHandlerImpl();
        List < User > userList = new List < User > ();
        list < RecordType > rtList = new list < RecordType > ();
        list < SocialPersona > sPersonaList = new list < SocialPersona > ();
        list < Account > accList = new List < Account > ();
        Boolean createCaseBool = false;
        Boolean someTemp = false;
        for (SocialPost sp: spList) {
            userList = [Select name from User where id = : sp.OwnerId order by LastModifiedDate ASC Limit 1];
            rtList = [Select id from RecordType where sobjecttype = 'Case'
                and name = 'Social'
                order by LastModifiedDate ASC Limit 1
            ];
        system.debug('<<<<<<<<<<<<User List>>>>>>>>>>>>>>>'+userList);
        system.debug('<<<<<<<<<<<<Social RecordType list>>>>>>>>>>>>>>>'+rtList); 
        //LM 02-25-2015 : Set Topic Profile to Blank if it is null in the Social post raw data.
        system.debug('<<<<<<<<<<<<Social Post Topic Profile Name>>>>>>>>>>>>>>>'+sp.TopicProfileName); 
        if(sp.TopicProfileName == null)
             sp.TopicProfileName = '';	//else no need to update the value   
             
            if (!userList.isEmpty()) {
                if (userList.get(0).name.equalsIgnoreCase('SCS Integrator')) {
                //if (userList.get(0).name.equalsIgnoreCase('CTI Integrator')) {
                    if (String.isNotBlank(sp.Handle) &&
                        String.isNotBlank(sp.MessageType)) {
                        createCaseBool = true;
                    }
                    system.debug('<<<<<<<<<Create Case Boolean>>>>>>>>>>'+createCaseBool);
                    if (createCaseBool) {
                      //LM 12/02/2014: commented below if loop and added new if condition to process test tweets same way as production @askplaystation tweets.
                      //if (sp.TopicProfileName.equalsIgnoreCase('@AskPlaystation')){
                        if (sp.TopicProfileName.equalsIgnoreCase('@AskPlaystation') || sp.TopicProfileName.equalsIgnoreCase('@robotpod')) {
                            if (!sp.Handle.containsIgnoreCase('PlayStation')) {
                                if (!sp.MessageType.equalsIgnoreCase('retweet')) {
                                    String refContactId;
                                    String refAccountId;
                                    if (String.isNotBlank(sp.PersonaId)) {
                                        sPersonaList = [Select id, parentId, ExternalId, Provider, Name, RealName
                                            from SocialPersona where id = : sp.PersonaId order by LastModifiedDate ASC Limit 1
                                        ];
                                        system.debug('<<<<<<<<Social Persona List>>>>>>>>>>>>'+sPersonaList);
                                        if (sPersonaList.size() > 0) {
                                            newCase = sisphi.buildParentCaseDefault(sp, sPersonaList.get(0));
                                            system.debug('<<<<<<<<<<New Case Object>>>>>>>'+newCase);
                                            //LM 12/01/2014 : Updated to set correct ownerId, recordTypeId. Added code to setup Social_Content__c field on case
                                            newCase.RecordTypeId = RT_CASE_SOCIAL_ID;
                                            newCase.OwnerId = OWNER_SOCIAL.ID;
                                            newCase.Social_Content__c = sp.Content;
                                            newCase.Origin = 'Social Post';
                                            newCase.Social_Post_Tag__c = getCategoriesFromPostTag(sp.PostTags);
                                            Boolean isContactType = checkParentId(sPersonaList.get(0).parentId);
                                            system.debug('<<<<<<<<<<<<<<isContactType>>>>>>>>>>>>>'+isContactType);
                                            if (isContactType != null) {
                                                if (isContactType) {
                                                    refAccountId = checkAndCreateAccount(sPersonaList.get(0).parentId);
                                                    refContactId = checkAndCreateContact(refAccountId);
                                                } else {
                                                    refContactId = checkAndCreateContact(sPersonaList.get(0).parentId);
                                                    refAccountId = checkAndCreateAccount(refContactId);
                                                }
                                            } else {
                                                //nothing available create both would not be a case
                                            }
                                            system.debug('<<<<<<<<<<<<<<<<<<<Account Reference>>>>>>>>>>>>>>'+refAccountId);
                                            system.debug('<<<<<<<<<<<<<<<<<<<Contact Reference>>>>>>>>>>>>>>'+refContactId);
                                            if (String.isNotBlank(refAccountId)) {
                                                newCase.AccountId = refAccountId;
                                            }
                                            if (String.isNotBlank(refContactId)) {
                                                newCase.ContactId = refContactId;
                                            }
                                        }
                                    }
                                    try {
                                        //insert newCase;
                                        upsert newCase;
                                        sp.ParentId = newCase.id;
                                        SocialPersona spUpdate = sPersonaList.get(0);
                                        spUpdate.ParentId = refContactId;
                                        update spUpdate;
                                    } catch (Exception ex) {
                                        System.debug('======ERROR Occured during insertSocialPostCase====' + ex.getMessage());
                                    }
                                }
                            }
                        } else {
                            Boolean createCaseRef = false;
                            system.debug('<<<<<<<<<<<<<<<<<<Social Post PostTag>>>>>>>>>>>>>'+sp.PostTags);
                            system.debug('<<<<<<<<<<<<<<<<<<Social Post PostPriority>>>>>>>>>>>>>'+sp.PostPriority);
                            if (String.isNotBlank(sp.PostTags)) {
                                if (sp.PostTags.containsIgnoreCase('Help') ||
                                    sp.PostTags.containsIgnoreCase('Complaint') ||
                                    sp.PostTags.containsIgnoreCase('Question') ||
                                    sp.PostTags.containsIgnoreCase('Compliment')) {
                                    createCaseRef = true;
                                }
                            }
                            if (String.isNotBlank(sp.PostPriority)) {
                                if (sp.PostPriority.equalsIgnoreCase('High') ||
                                    sp.PostPriority.equalsIgnoreCase('Medium')) {
                                    createCaseRef = true;
                                }
                            }
                            system.debug('<<<<<<<<<<<<<createCaseRef>>>>>>>>>>>>>>>'+createCaseRef);
                            if (createCaseRef) {
                                if (!sp.Handle.containsIgnoreCase('PlayStation')) {
                                    if (!sp.MessageType.equalsIgnoreCase('retweet')) {
                                      system.debug('-----------------------------------Into Playstation-----------------------------------------');
                                        String refContactId;
                                        String refAccountId;
                                        if (String.isNotBlank(sp.PersonaId)) {
                                            sPersonaList = [Select id, parentId, ExternalId, Provider, Name, RealName
                                                from SocialPersona where id = : sp.PersonaId order by LastModifiedDate ASC Limit 1
                                            ];
                                            if (sPersonaList.size() > 0) {
                                              system.debug('-----------------------------------Before Case Call-----------------------------------------');
                                                newCase = sisphi.buildParentCaseDefault(sp, sPersonaList.get(0));
                                                system.debug('-----------------------------------After Case Call'+ newCase +'-----------------------------------------');
                                                //LM 12/01/2014 : Updated to set correct ownerId, recordTypeId. Added code to setup Social_Content__c field on case
                                                newCase.RecordTypeId = RT_CASE_SOCIAL_ID;
                                                newCase.OwnerId = OWNER_SOCIAL.ID;
                                                newCase.Social_Content__c = sp.Content;
                                                newCase.Origin = 'Social Post';
                                                newCase.Social_Post_Tag__c = getCategoriesFromPostTag(sp.PostTags);
                                                Boolean isContactType = checkParentId(sPersonaList.get(0).parentId);
                                                if (isContactType != null) {
                                                    if (isContactType) {
                                                        refAccountId = checkAndCreateAccount(sPersonaList.get(0).parentId);
                                                        refContactId = checkAndCreateContact(refAccountId);
                                                    } else {
                                                        refContactId = checkAndCreateContact(sPersonaList.get(0).parentId);
                                                        refAccountId = checkAndCreateAccount(refContactId);
                                                    }
                                                } else {
                                                    //nothing available create both would not be a case
                                                }
                                                if (String.isNotBlank(refAccountId)) {
                                                    newCase.AccountId = refAccountId;
                                                }
                                                if (String.isNotBlank(refContactId)) {
                                                    newCase.ContactId = refContactId;
                                                }
                                            }
                                        }
                                        try {
                                            //insert newCase;
                                            upsert newCase;
                                            sp.ParentId = newCase.id;
                                            SocialPersona spUpdate = sPersonaList.get(0);
                                            spUpdate.ParentId = refContactId;
                                            update spUpdate;
                                        } catch (Exception ex) {
                                            System.debug('======ERROR Occured during insertSocialPostCase====' + ex.getMessage());
                                        }
                                        system.debug('#####################################################################################');
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }
    }

    public static String getCategoriesFromPostTag(String postTags) {
      system.debug('<<<<<<Post Tags>>>>>>>>'+postTags);
        if (String.isNotBlank(postTags)) {
            List < String > ptList = postTags.split(',');
            system.debug('<<<<<<<Post Tags List>>>>>>>'+ptList);
            String ptFinal = '';
            if (ptList.size() > 0) {
                for (String str: ptList) {
                    ptFinal = ptFinal + str + ' ';
                }
            }
            system.debug('<<<<<<Post Tags to return>>>>>>>>'+ptFinal);
            //LM 02/25/2015 : Added below logic to fix the Social_post_tag__c field length issue on case. Max field length for social_post_tag__c on case is 255 chars
            ptFinal = ptFinal != null ? (ptFinal.length() < 255 ? ptFinal : ptFinal.substring(0,254)) : null;  
            return ptFinal;
        }
        return null;
    }

    public static Boolean checkParentId(String parentIdRef) {
        system.debug('<<<<<<<<<<ParentId Reference>>>>>>>>'+parentIdRef);
        Boolean isContact = false;
        Integer accList1Count = [Select count() from Account where Id = : parentIdRef];
        Integer conList1Count = [Select count() from Contact where Id = : parentIdRef];
        system.debug('<<<<<<<<<<Account records count>>>>>>>>'+accList1Count);
        system.debug('<<<<<<<<<<Contact records count>>>>>>>>'+conList1Count);
        if (accList1Count > 0) {
            isContact = false;
            return isContact;
        }
        if (conList1Count > 0) {
            isContact = true;
            return isContact;
        }
        if (conList1Count == 0 && accList1Count == 0) {
            System.debug('There was no Contact or Account on the Social Post, Major error.');
        }
        return null;
    }

    public static String checkAndCreateAccount(String parentIdRef1) {
      system.debug('<<<<<<<<<<Parent Id Reference>>>>>>>>'+parentIdRef1);
        Integer conList1Count = [Select count() from Contact where Id = : parentIdRef1];
        system.debug('<<<<<<<<<Contact count>>>>>>>>'+conList1Count);
        Account acc = new Account();
        list < Contact > conList1 = new List < Contact > ();
        String personAccountRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        system.debug('<<<<<<<<<<Person Account Record Type Id>>>>>>>>>>>>>>>>'+personAccountRecId);
        if (conList1Count > 0) {
            conList1 = [Select id, FirstName, LastName, AccountId from Contact where Id = : parentIdRef1 order by LastModifiedDate ASC Limit 1];
            system.debug('<<<<<<<<<<<Contact List>>>>>>>>>>>>'+conList1);
            system.debug('<<<<<<<<<<<Contact Account Id>>>>>>'+conList1.get(0).AccountId);
            Contact con1 = new Contact();
            if (String.isNotBlank(conList1.get(0).AccountId)) {
                return conList1.get(0).AccountId;
            } else {
                acc.FirstName = conList1.get(0).FirstName;
                acc.LastName = conList1.get(0).LastName;
                acc.RecordTypeId = personAccountRecId;
                con1 = conList1.get(0);
                try {
                    insert acc;
                    con1.AccountId = acc.Id;
                    return acc.Id;
                } catch (Exception ex) {
                    System.debug('======ERROR Occured during insertSocialPostCase in Account Creation====' + ex.getMessage());
                }
            }
        }
        return null;
    }

    public static String checkAndCreateContact(String parentIdRef2) {
      system.debug('<<<<<<<<<<Parent Id Reference>>>>>>>>'+parentIdRef2);
        Integer accList1Count = [Select count() from Account where Id = : parentIdRef2];
        system.debug('<<<<<<<<<Account count>>>>>>>>'+accList1Count);
        Contact con = new Contact();
        list < Account > accList1 = new List < Account > ();
        if (accList1Count > 0) {
            accList1 = [Select id, FirstName, LastName, PersonContactId from Account where Id = : parentIdRef2 order by LastModifiedDate ASC Limit 1];
            system.debug('<<<<<<<<<<<Account List>>>>>>>>>>>>'+accList1);
            system.debug('<<<<<<<<<<<Account Id>>>>>>>>>>>>>>'+accList1.get(0).PersonContactId);
            if (String.isNotBlank(accList1.get(0).PersonContactId)) {
                return accList1.get(0).PersonContactId;
            } else {
                con.FirstName = accList1.get(0).FirstName;
                con.LastName = accList1.get(0).LastName;
                con.AccountId = parentIdRef2;
                try {
                    insert con;
                    return con.Id;
                } catch (Exception ex) {
                    System.debug('======ERROR Occured during insertSocialPostCase in Contact Creation====' + ex.getMessage());
                }
            }
        }
        return null;
    }

}