/*********************************************************************************
 Author       :   Dipika (JDC Developer)
 Created Date :   12 Aug,2013
 Task         :   T-170654
*********************************************************************************/
@isTest
global class Test_WebServiceUtility implements HttpCalloutMock{
	
	
	global HttpResponse respond(HTTPRequest req){
	    HttpResponse res = new HttpResponse();
	    res.setStatus('OK');
	    res.setStatusCode(200);
	    res.setBody(getResponse());
	    return res;
  }
 
  static void insertCustomSetting() {
  	Integration__c setting = new Integration__c();
  	setting.Siras_Partner_ID__c = 'scea-crm-edes2';
  	setting.Siras_Endpoint__c = 'https://playstation--pro.custhelp.com/cc/siebel/survey_trigger/';
  	setting.Siras_Partner_Passkey__c = 'G5yd3J6G';
  	insert setting;
  }
  
  static  testMethod void testGetPop(){
  	insertCustomSetting();
  	SirasUtility web =new SirasUtility();
  	RestRequest req = new RestRequest(); 
  	RestResponse res = new RestResponse();
  	String fullURL = 'https://router.siras.com/edes/';
  	req.requestURI = fullURL;
  	req.httpMethod = 'POST';
  	req.requestBody = Blob.valueOf(web.requestBody('1222'));
  	RestContext.request = req;
  	RestContext.response = res; 
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new Test_WebServiceUtility());
    SirasResult sr = web.getPOP('1232145425');
    System.debug(sr);
    system.assert(sr!=null);
    Test.stopTest();
  }
  
  static  testMethod void testGetPop2(){
  	insertCustomSetting();
  	SirasUtility web =new SirasUtility();
  	RestRequest req = new RestRequest(); 
  	RestResponse res = new RestResponse();
  	String fullURL = 'https://router.siras.com/edes/';
  	req.requestURI = fullURL;
  	req.httpMethod = 'POST';
  	req.requestBody = Blob.valueOf(web.requestBody('1222'));
  	RestContext.request = req;
  	RestContext.response = res; 
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new Test_WebServiceUtility());
    SirasResult sr = web.getPOP('CG835494586');
    System.debug(sr);
    system.assert(sr!=null);
    Test.stopTest();
  }
  
    
  global String getResponse(){
  	String response = '<?xml version=\'1.0\'?>';

	response +=			'			<Serial-Number-Inquiry-Response version=\'3.0\'>';
	response +=			'					<Response-Header>';
	response +=			'					   <Response-Status>100</Response-Status>';
	response +=			'					   <Response-Status-Description>Request Processed</Response-Status-Description>';
	response +=			'					   <Partner-ID>scea-crm-edes2</Partner-ID>';
	response +=			'					   <Response-Email-Address Type="" />';
	response +=			'					   <Confirmation-Type>2</Confirmation-Type>';
	response +=			'					   <Transmission-ID />';
	response +=			'					   <Location-ID />';
	response +=			'					   <Creation-Date>20130809</Creation-Date>';
	response +=			'					   <Creation-Time>110301</Creation-Time>';
	response +=			'					   <Creation-Time-Zone>GMT-07:00</Creation-Time-Zone>';
	response +=			'					</Response-Header>';
						
	response +=			'					<Product-Information>';
	response +=			'					   <Reason-Code />';
	response +=			'					   <Reason-Description />';
	response +=			'					   <SiRAS-Inquiry-ID>7980682</SiRAS-Inquiry-ID>';
	response +=			'					   <UPC />';
	response +=			'					   <Serial-Number>1232145425</Serial-Number>';
	response +=			'					   <Item-Description />';
	response +=			'					   <Item-Number />';
	response +=			'					   <Brand-Name>SONY PLAYSTATION</Brand-Name>';
	response +=			'					   <Brand-ID>9</Brand-ID>';
	response +=			'					   <Sold-By-Retailer />';
	response +=			'					   <Sold-By-Retailer-ID />';
	response +=			'					   <Sold-Date>' + date.today() + '</Sold-Date>';
	response +=			'					   <Manufacturer-Warranty-Determination>12</Manufacturer-Warranty-Determination>';
	response +=			'					   <Manufacturer-Warranty-Parts-Determination>12</Manufacturer-Warranty-Parts-Determination>';
	response +=			'				   <Manufacturer-Warranty-Labor-Determination>12</Manufacturer-Warranty-Labor-Determination>';
	response +=			'				</Product-Information>';
						
	response +=			'					<Response-Trailer>';
	response +=			'					   <Expected-Count>1</Expected-Count>';
	response +=			'					   <Actual-Count>1</Actual-Count>';
	response +=			'					</Response-Trailer>';
	response +=			'					</Serial-Number-Inquiry-Response>';
  	
  	
  	return response;
  }
}