/************************************************************************************************************************************************************************
//Class         : PSNInboundScheduler
//Description   : Scheduler class to process records from Staging_PPP_PSN_Inbound__c record.
//Created Date  : Jan 31, 2014
//Developed by  : Urminder Vohra (JDC)
//Updates       :
// LM 3/17/2014 : JIRA # SF-179 - Commented Address Search code.
                  JIRA # SF-144 - Removed Product Status = Active criteria to pull all products as PSN is still sending Old VITA plans. 

// LM 3/20/2014 : JIRA # SF-273, SF-278 - Added a statement to setup Order Date on the Base object to PPP Order transaction date in the Staging object.
                  Also commented a line (Asset_Creation_Date__c) and added a new line (Asset_Purchase_Date__c)as the line was referencing to a wrong field 
                  in the condition.
                  
   LM 3/28/2014  : JIRA # SF-308 - Fix for Asset Purchase Date, Product Model#s, trigger records to Assurant outbound. 
   LM 4/14/2014  : JIRA # SF-408 - PSN Inbound Schedule job issues - Added CPU time debug statements 
   LM 6/02/2014  : JIRA # SF-878 - PSN Batch failure due to specifying the Id value in an Insert operation for Assets.
   LM 7/1/2014   : JIRA # SF-1026 - PSN Discounted Price Changes. Updated Payment Total to use the inbound price value instead of getting it from Product.
   LM 7/1/2014   : JIRA # SF-1117 - PSN Process not updating existing Asset with the PPP information. Added/Updated code to populate PPP information on new/existing asset
   LM 9/3/2014	 : JIRA # SMS-251 - PSN PPP New Data elements from Data warehouse. 
//*****************************************************************************************************************************************************************************/

global class PSNInboundScheduler implements Database.Batchable<sObject>, Schedulable{
  public static Set<String> PSN_SKUSet = sObjectUtility.getPSN_SKUSet(); 
   
  global void execute(SchedulableContext SC) { // Start Batch
    System.debug('Total Number of CPU usage time (in ms) allowed in this Apex code context: ' +  Limits.getLimitCpuTime());  
    Database.executeBatch(new PSNInboundScheduler(), 5);
    System.debug('Amount of CPU time (in ms) used so far: Execute Batch ' + Limits.getCpuTime());
  }
  
  global Database.QueryLocator start(Database.BatchableContext BC){
    String qry = 'SELECT Id, OwnerId, IsDeleted, Name, Order_Item_Id__c, Order_Customer_Account_ID__c, Order_Line_Total__c, Order_Line_Sku__c, Model_Number__c, Serial_Number__c, ' + 
            ' Asset_Purchase_Date__c, Currency_Code__c, Order_Line_Quantity__c, Transaction_ID__c, Transaction_Type__c, Subscription_Flag__c, ' +
            ' Original_Transaction_ID__c, PSN_Account_ID__c, PSN_Account_Sign_In_ID__c, Contact_Email__c, Contact_First_Name__c, Contact_Last_Name__c,' +
            ' Address_Address_1__c, Address_Address_2__c, Address_Address_3__c, Address_City__c, Address_State__c, Address_Zip_Code__c, Address_Country__c,' +
            ' Asset_Creation_Date__c, Asset_Console_ID__c, PSN_Account_Name__c, Processed_Date_Time__c, Processed_Status__c, Processed_Error_Text__c ' +
            ' from Staging_PPP_PSN_Inbound__c where Processed_Date_Time__c = null';
            //and Transaction_ID__c=\'6492164896\''; //and Transaction_ID__c=\'6603800250\'
    return Database.getQueryLocator(qry);       
  }
  
  global void execute(Database.BatchableContext BC, List<sObject> scope){
    //Map to hold all scope records and for further filterization
    Map<String, Staging_PPP_PSN_Inbound__c> tranID_InboundMap = new Map<String, Staging_PPP_PSN_Inbound__c>();
    //List to hold existing inbound records and invalid records. 
    List<Staging_PPP_PSN_Inbound__c> rejectedRecords = new List<Staging_PPP_PSN_Inbound__c>();
    List<PSNInboundWrapper> inboundWrapperList = new List<PSNInboundWrapper>();
    Set<String> transactionIDs = new Set<String>();
    Set<String> psn_sku_set = new Set<String>();
    Set<String> LUCID_SKU_set = new Set<String>();
    Set<String> emailSet = new Set<String>();
    //LM 3/14 - Commented as it is no longer used in the code.
    //Set<String> addressLine1Set = new Set<String>();
    Set<String> psnAccountIdSet = new Set<String>();
    Set<String> serialNumberSet = new Set<String>();
    //LM 3/14 - Commented as it is no longer used in the code.
    //map<String, Address__c> addressMap = new map<String, Address__c>();
    map<String, list<PSN_SKU_Mapping__c>> psnSKUMap = new map<String, list<PSN_SKU_Mapping__c>>();
    map<String, Product__c> productMap = new map<String, Product__c>();
    map<String, Account> contactMap = new map<String, Account>();
    map<String, Asset__c> existingAssetsMap = new map<String, Asset__c>();
    //LM 08/18 - Added to restrict duplicate Contact-Asset records
    map<String, Consumer_Asset__c> existingConAssetMap = new map<String, Consumer_Asset__c>(); 
    set<Id> existingconAssetset = new set<Id>();
    map<String, PSN_Account__c> psnAccountMap = new map<String, PSN_Account__c>();       
    decimal planduration;
    
    for(Staging_PPP_PSN_Inbound__c inbound : (List<Staging_PPP_PSN_Inbound__c>)scope) {
      if (inbound.Order_Item_Id__c <> null) {
        transactionIDs.add(inbound.Order_Item_Id__c);
      }
      if (!tranID_InboundMap.containsKey(inbound.Order_Item_Id__c)) {
        tranID_InboundMap.put(inbound.Order_Item_Id__c, inbound);
      }
      if(inbound.Order_Item_Id__c <>  null) {
        psn_sku_set.add(inbound.Order_Line_Sku__c);
      }
      if(inbound.Contact_Email__c <> null) {
        emailSet.add(inbound.Contact_Email__c);
      }
      //LM 3/14 - Commented this code as the logic is no more looking for existing addresses
      /*if(inbound.Address_Address_1__c <> null) {
        addressLine1Set.add(inbound.Address_Address_1__c);
      } */
      if(inbound.PSN_Account_ID__c <> null) {
        psnAccountIdSet.add(inbound.PSN_Account_ID__c);
      }
      if(inbound.Asset_Console_ID__c <> null) {
        serialNumberSet.add(inbound.Asset_Console_ID__c);
      }
    }
    System.debug('Amount of CPU time (in ms) used so far: Initializing Lists/Maps ' + Limits.getCpuTime()); 

    // Mapping to get LUCID SKU from PSN SKU Mapping Object 
    for(PSN_SKU_Mapping__c psm : [select LUCID_SKU__c, PSN_SKU__c, Country_Code__c from PSN_SKU_Mapping__c where PSN_SKU__c IN : psn_sku_set]) {
      if(!psnSKUMap.containsKey(psm.PSN_SKU__c)) {
        psnSKUMap.put(psm.PSN_SKU__c, new list<PSN_SKU_Mapping__c>());
      }
      psnSKUMap.get(psm.PSN_SKU__c).add(psm);
      LUCID_SKU_set.add(psm.LUCID_SKU__c); 
    }
    System.debug('Amount of CPU time (in ms) used so far: Mapping to get Lucid SKU ' + Limits.getCpuTime());  
    System.debug('========psnSKUMap==================' + psnSKUMap);
    //Get product Information using LUCID_SKU_set
    // LM 3/17 - Commented the following line and created new line with out "Status = Active" criteria.
    //for(Product__c prod : [select Name, Duration__c, Sub_Type__c, SKU__c, List_Price__c from Product__c where SKU__c IN : LUCID_SKU_set AND Status__c = 'Active']) {
    for(Product__c prod : [select Name, Duration__c, Sub_Type__c, SKU__c, List_Price__c, Genre__c from Product__c where SKU__c IN : LUCID_SKU_set]) {  
      productMap.put(prod.SKU__c, prod);
    } 
    System.debug('Amount of CPU time (in ms) used so far: Product Object Query ' + Limits.getCpuTime());   
    System.debug('========productMap==================' + productMap);
    // Check for existing Contacts
    for(Account cnt : [select Name, FirstName, LastName, personEmail, PersonContactId from Account where personEmail IN : emailSet]) {
      contactMap.put(cnt.personEmail, cnt);
    }
    System.debug('Amount of CPU time (in ms) used so far: Account Object Query' + Limits.getCpuTime());    
    // check for existing Address Records
    //LM 3/14 - Commented the following code block.
    /*for(Address__c add : [select Id, Address_Line_1__c from Address__c where Address_Line_1__c IN : addressLine1Set]) {
      addressMap.put(add.Address_Line_1__c, add);
    }*/
    //LM 08/20: Updated search based on "PSN_Account_ID__c" instead of Account_ID__c as this is not populated in getNewPSNAccount method
    for(PSN_Account__c psnAcc : [select Id, PSN_Account_ID__c, Account_ID__c, Consumer__c from PSN_Account__c where PSN_Account_ID__c IN : psnAccountIdSet]) {
      psnAccountMap.put(psnAcc.PSN_Account_ID__c, psnAcc);
    }
    System.debug('Amount of CPU time (in ms) used so far: PSN Query' + Limits.getCpuTime());
    //LM 03/25/2014 : Added PPP_Purchase_Date__c to the SOQL
    //LM 06/17/2014 : Added asset fields to the SOQL
    for(Asset__c ast : [select Id, Console_ID__c, Purchase_Date__c, PPP_Purchase_Date__c, Model_Number__c, Serial_Number__c, Asset_Status__c, PPP_Status__c, PPP_Product__c, Coverage_Type__c,
                        PPP_Start_Date__c, PPP_End_Date__c, PPP_Refund_Eligible__c, PPP_Last_Update_Date__c
                        from Asset__c where Console_ID__c IN : serialNumberSet]) {
      existingAssetsMap.put(ast.Console_ID__c, ast);
      //LM: 08/27: Added to store Consumer-Asset records
      existingconAssetset.add(ast.Id);
    }
    //LM 08/19 : added to avoid duplicate consumer asset records creation
    for(Consumer_Asset__c conasset : [select Id, Asset__c, Consumer__c from Consumer_Asset__c where Asset__c in :existingconAssetset]){
    	existingConAssetMap.put(conasset.Asset__c+'-'+conasset.Consumer__c, conasset);
    }
    System.debug('Amount of CPU time (in ms) used so far: Asset Query ' + Limits.getCpuTime());  
    //Checking for existing record with Status and Date time (processed)
    for (Staging_PPP_PSN_Inbound__c inbound : [SELECT ID, Order_Item_Id__c FROM Staging_PPP_PSN_Inbound__c 
                                         WHERE Order_Item_Id__c IN: transactionIDs 
                                                 AND Processed_Status__c = 'Success' 
                                                 AND Processed_Date_Time__c <> null]) {
      if (tranID_InboundMap.containsKey(inbound.Order_Item_Id__c)) {
        //Getting existing inbound record to update date time and status to failure.
        Staging_PPP_PSN_Inbound__c existingInbound = tranID_InboundMap.get(inbound.Order_Item_Id__c);
        //Removed from map that contains all the scope records.
        tranID_InboundMap.remove(inbound.Order_Item_Id__c);
        
        existingInbound.Processed_Date_Time__c = Datetime.now();
        existingInbound.Processed_Status__c = 'Duplicate';
        existingInbound.Processed_Error_Text__c = 'Record with same Order Item Id has been successfully processed previously.';
        rejectedRecords.add(existingInbound);
        System.debug('Amount of CPU time (in ms) used so far:Finding Duplicate records ' + Limits.getCpuTime());  
      }
    }
    //Validating inbound records.
    for (Staging_PPP_PSN_Inbound__c inbound : tranID_InboundMap.values()) {
      Staging_PPP_PSN_Inbound__c validateRecord = validate(inbound);
      if (validateRecord.Processed_Status__c == 'Failure') {
        Staging_PPP_PSN_Inbound__c existingInbound = new Staging_PPP_PSN_Inbound__c();
        //Getting existing inbound record to update date time and status to failure.
        existingInbound = tranID_InboundMap.get(inbound.Order_Item_Id__c);
        //Removed from map that contains all the scope records.
        tranID_InboundMap.remove(inbound.Order_Item_Id__c);
        existingInbound.Processed_Date_Time__c = Datetime.now();
        existingInbound.Processed_Status__c = 'Failure';
        existingInbound.Processed_Error_Text__c = validateRecord.Processed_Error_Text__c;
        rejectedRecords.add(existingInbound);
        System.debug('Amount of CPU time (in ms) used so far: Inbound record validation rejections' + Limits.getCpuTime());  
      } else {
        // Record is approved for Processing
        PSNInboundWrapper inboundWrap = new PSNInboundWrapper();
        //Staging_PPP_PSN_Inbound__c.
        inboundWrap.consumer = GeneralUtiltyClass.getNewConsumer(inbound.Contact_First_Name__c, inbound.Contact_Last_Name__c, inbound.Contact_Email__c);
        System.debug('Amount of CPU time (in ms) used so far: Get New Consumer method' + Limits.getCpuTime()); 
        if(contactMap.containsKey(inbound.Contact_Email__c)) {
          inboundWrap.consumer.Id = contactMap.get(inbound.Contact_Email__c).Id;
        }
        //LM 3/14 - Commented the following code lines
        /*if(addressMap.containsKey(inbound.Address_Address_1__c)) {
        //  inboundWrap.listOfAddress.add(addressMap.get(inbound.Address_Address_1__c));
        } else { */ 
        //LM 08/13
        inbound.Address_Address_1__c = (inbound.Address_Address_1__c == '' || inbound.Address_Address_1__c == NULL) ? 'BLANK' : inbound.Address_Address_1__c;
        Address__c newAddress = GeneralUtiltyClass.getNewAddress(inbound.Address_Address_1__c, inbound.Address_Address_2__c, inbound.Address_Address_3__c, 
                                          inbound.Address_City__c, inbound.Address_State__c, inbound.Address_Country__c, inbound.Address_Zip_Code__c, 'Billing');
        System.debug('Amount of CPU time (in ms) used so far: get New Address Method ' + Limits.getCpuTime());  
        inboundWrap.listOfAddress.add(newAddress);
          
        // Creating new shipping type  address
        Address__c newAddress2 = newAddress.clone();
        newAddress2.Address_Type__c = 'Shipping';
        inboundWrap.listOfAddress.add(newAddress2);
        inboundWrap.isNewAddress = true;
        //} //LM 3/14 - commented End Else statement 
        if(!psnAccountMap.containsKey(inbound.PSN_Account_ID__c)) {
          inboundWrap.psnAccount = GeneralUtiltyClass.getNewPSNAccount(inbound.PSN_Account_Name__c, inbound.PSN_Account_ID__c, inbound.PSN_Account_Sign_In_ID__c);
          System.debug('Amount of CPU time (in ms) used so far:get New PSN Account Method ' + Limits.getCpuTime());  
        } else {
          inboundWrap.psnAccount = psnAccountMap.get(inbound.PSN_Account_ID__c);
        }
        String productSKU;
        Product__c product;
        System.Debug('<<<<<<<<<<<<< PSN SKU Map >>>>>>>> ' +psnSKUMap);
        System.Debug('<<<<<<<<<<<<< inbound.Order_Line_Sku__c >>>>>>>> ' +inbound.Order_Line_Sku__c);
   
        if(psnSKUMap.containsKey(inbound.Order_Line_Sku__c)) {
          for(PSN_SKU_Mapping__c psm : psnSKUMap.get(inbound.Order_Line_Sku__c)) {
            inbound.Address_Country__c = inbound.Address_Country__c == 'US' ? 'USA' : inbound.Address_Country__c;
            inbound.Address_Country__c = inbound.Address_Country__c == 'CA' ? 'CANADA' : inbound.Address_Country__c;
            if(inbound.Address_Country__c <> null && inbound.Address_Country__c.toUpperCase() == psm.Country_Code__c) {
              productSKU = psm.LUCID_SKU__c;
            }
        }
        System.debug('Amount of CPU time (in ms) used so far: Updating Country code for PSN SKU mapping ' + Limits.getCpuTime());  
        }
        System.debug('=======productSKU==========' + productSKU);
        if(productSKU <> null) {
          product = productMap.get(productSKU);
          inboundWrap.product = product;
        } 
        System.debug('=======product==========' + product);
        System.debug('=======existingAssetsMap==========' + existingAssetsMap);
        
        Asset__c ast;
        //LM 06/16/2014: Changed the IF condition and moved all the Asset fields setup code to a different If block 
        system.debug('<<<<<Test1>>>'+existingAssetsMap.containsKey(inbound.Asset_Console_ID__c));
        system.debug('<<<<<Test2>>>'+product);
        if(!existingAssetsMap.containsKey(inbound.Asset_Console_ID__c) && product <> null ) {
          ast = new Asset__c(); }    
          //LM 03/25/2014 - Derive Model Number based on PPP SKU - added below if condition
          //ast.Model_Number__c = 'PSN PPP';  
        else {
          ast = existingAssetsMap.get(inbound.Asset_Console_ID__c);
          system.debug('<<<<<Esiting asset>>>>>'+ast);
        } 
        //LM 06/16/2014 : Added code to populate PPP information on Asset
        if(ast <> null) {
        //LM 08/13 updated below statement 	
        system.debug('<<<<<Inside Asset Creation/Update>>>>>'+ast);
        ast.Model_Number__c = getModelNumber(Inbound.Model_Number__c,product.Genre__c) <> null ? getModelNumber(Inbound.Model_Number__c,product.Genre__c) : (ast.Model_Number__c <> null ? ast.Model_Number__c : null);                
        ast.Serial_Number__c = inbound.Serial_Number__c <> null ? inbound.Serial_Number__c : (ast.Serial_Number__c <> null ? ast.Serial_Number__c : inbound.Name);
        system.debug('<<<<<ast Serial Number>>>>'+ast.Serial_Number__c);
        ast.Purchase_Date__c = inbound.Asset_Creation_Date__c <> null ? Date.parse(inbound.Asset_Creation_Date__c.split(' ')[0].replace('-','/')) : (ast.Purchase_Date__c <> null ? ast.Purchase_Date__c : null) ;       
        ast.Asset_Status__c = ast.Asset_Status__c == null ? 'Active' : ast.Asset_Status__c;
        ast.PPP_Status__c = 'Pending Confirm';
        ast.PPP_Purchase_Date__c = inbound.Asset_Purchase_Date__c <> null ? Date.parse(inbound.Asset_Purchase_Date__c.split(' ')[0].replace('-','/')) : null ;   
        ast.PPP_Product__c = product <> null ? product.Id : null;
        ast.Coverage_Type__c = product <> null ? product.Sub_Type__c : null;          
        planduration = product <> null ? product.Duration__c : null;
        if(ast.Coverage_Type__c != null && ast.Coverage_Type__c == 'AD') {
                      ast.PPP_Start_Date__c = ast.PPP_Purchase_Date__c;
                      if(planduration != null){
                        ast.PPP_End_Date__c = ast.Purchase_Date__c == null ? null : ast.Purchase_Date__c.addMonths((Integer)planduration + 12);
                      }
        }else {
                      ast.PPP_Start_Date__c = ast.Purchase_Date__c <> null ? ast.Purchase_Date__c.addMonths(12) : ast.Purchase_Date__c;
                      if(planduration != null)  {
                        ast.PPP_End_Date__c = ast.PPP_Start_Date__c == null ? null : ast.PPP_Start_Date__c.addMonths((Integer)planduration);
                      }
              }    
        ast.PPP_Refund_Eligible__c = true;
        ast.PPP_Last_Update_Date__c = ast.PPP_Purchase_Date__c;
        ast.Console_ID__c = ast.Console_ID__c <> null ? ast.Console_ID__c : inbound.Asset_Console_ID__c;     
        inboundWrap.asset = ast;
        } else {   
          Staging_PPP_PSN_Inbound__c existingInbound = tranID_InboundMap.get(inbound.Order_Item_Id__c);
          //Removed from map that contains all the scope records.
          tranID_InboundMap.remove(inbound.Order_Item_Id__c);
          
          existingInbound.Processed_Date_Time__c = Datetime.now();
          existingInbound.Processed_Status__c = 'Failure';
          existingInbound.Processed_Error_Text__c = 'Asset creation or Update failed';
          rejectedRecords.add(existingInbound);
          continue;
        }
        
        Order__c ordr = new Order__c();
        ordr.Order_Type__c = 'ESP';
        //LM 3/20 - Added for JIRA SF-278, SF-273
        ordr.Order_Date__c = inbound.Asset_Purchase_Date__c <> null ? Date.parse(inbound.Asset_Purchase_Date__c.split(' ')[0].replace('-','/')) : null ;
        ordr.Order_Origin_Source__c = 'P';
        ordr.Order_Status__c = 'Pending';
        ordr.ESP_Record_Type__c = 'A';
        ordr.External_Id__c = 'P-'+inbound.Order_Item_Id__c;
        //update staging record on successfull transactions
        inbound.Processed_Status__c = 'Success';    
        inboundWrap.ordr = ordr;
        inboundWrap.inbound = inbound;
        inboundWrapperList.add(inboundWrap);
      }
    }
    System.debug('=======rejectedRecords==========' + rejectedRecords.size());
    System.debug('=======tranID_InboundMap==========' + tranID_InboundMap);
    if(!rejectedRecords.isEmpty()) update rejectedRecords;
    if(!tranID_InboundMap.isEmpty()) update tranID_InboundMap.values();
    
    list<Account> consumerList = new list<Account>();
    list<Asset__c> assetListToInsert = new list<Asset__c>();
    for(PSNInboundWrapper wrapInbound : inboundWrapperList) {
      consumerList.add(wrapInbound.consumer);
      if(wrapInbound.asset <> null)
      assetListToInsert.add(wrapInbound.asset);
    }
    System.debug('=======consumerList==========' + consumerList.size());
    System.debug('=======assetListToInsert==========' + assetListToInsert.size());
    System.debug('=======assetListToInsert2==========' + assetListToInsert);
    if(!consumerList.isEmpty()) upsert consumerList;
    System.debug('Amount of CPU time (in ms) used so far: Upsert ConsumerList ' + Limits.getCpuTime());  
    if(!assetListToInsert.isEmpty())
    // LM 06/02/2014 - commented the Insert Asset list statement and added Upsert operation.
    // insert assetListToInsert;
    upsert assetListToInsert;
    System.debug('Amount of CPU time (in ms) used so far: Insert Assets ' + Limits.getCpuTime());  
    
    map<Id, Account> personAccountMap = new map<Id, Account>([select Id, personContactId from Account where Id IN : consumerList]); 
    list<Address__c> addressListToInsert = new list<Address__c>();
    list<Consumer_Asset__c> caListToInsert = new list<Consumer_Asset__c>();
    list<PSN_Account__c> psnAccList = new list<PSN_Account__c>(); 
    Consumer_Asset__c ca;
    for(PSNInboundWrapper wrapInbound : inboundWrapperList) { 
      if(wrapInbound.isNewAddress) {
        for(Address__c add : wrapInbound.listOfAddress) {
          add.Consumer__c = personAccountMap.get(wrapInbound.consumer.Id).personContactId;
          addressListToInsert.add(add);
        }
      }  
    //LM 08/20 If Asset exists then 
    system.debug('<<<<<<<<<<<<<existingconAssetset>>>>>>>>>'+existingConAssetMap);
    system.debug('<<<<<<<<<<<<<exits>>>>>>>>>'+existingConAssetMap.containsKey(wrapInbound.asset.Id+'-'+wrapInbound.consumer.Id));
    system.debug('<<<<<<<<wrapInbound.asst.Id>>>>>>>'+wrapInbound.asset.Id);
    system.debug('<<<<<<<<wrapInbound.consumer.Id>>>>>>>'+wrapInbound.consumer.Id);
    system.debug('<<<<<<<personAccountMap.get(wrapInbound.consumer.Id).personContactId>>>>>>>'+personAccountMap.get(wrapInbound.consumer.Id).personContactId);
    
    if(existingConAssetMap == null) {
      ca = new Consumer_Asset__c(Asset__c = wrapInbound.asset.Id);
      system.debug('<<<<<<<<create New Consumer Asset>>>>>>>>'+ ca);
      if(wrapInbound.consumer.personContactId == null) {
        ca.Consumer__c = personAccountMap.get(wrapInbound.consumer.Id).personContactId;
      } else {
        ca.Consumer__c = wrapInbound.consumer.personContactId;
      }
      caListToInsert.add(ca);  
    } else if(existingConAssetMap <> null && !(existingConAssetMap.containsKey(wrapInbound.asset.Id+'-'+personAccountMap.get(wrapInbound.consumer.Id).personContactId))){
      ca = new Consumer_Asset__c(Asset__c = wrapInbound.asset.Id);
      system.debug('<<<<<<<<create New Consumer Asset>>>>>>>>'+ ca);
      if(wrapInbound.consumer.personContactId == null) {
        ca.Consumer__c = personAccountMap.get(wrapInbound.consumer.Id).personContactId;
       // wrapInbound.psnAccount.Consumer__c = personAccountMap.get(wrapInbound.consumer.Id).personContactId;
      } else {
        ca.Consumer__c = wrapInbound.consumer.personContactId;
       // wrapInbound.psnAccount.Consumer__c = wrapInbound.consumer.personContactId;
      }
      caListToInsert.add(ca);  
     }    
      
      //Link PSN Account to Consumer.
    system.debug('<<<<<<<<<<<<<psnAccountMap>>>>>>>>>'+psnAccountMap);
    if(wrapInbound.consumer.personContactId == null) {
        wrapInbound.psnAccount.Consumer__c = personAccountMap.get(wrapInbound.consumer.Id).personContactId;
     } else {
        wrapInbound.psnAccount.Consumer__c = wrapInbound.consumer.personContactId;
     }
      psnAccList.add(wrapInbound.psnAccount);  
    }//for
    
    System.debug('=======caListToInsert==========' + caListToInsert.size());
    System.debug(addressListToInsert + '=======addressListToInsert==========' + addressListToInsert.size());
    System.debug(addressListToInsert + '=======PSNListToInsert==========' + psnAccList.size());
    if(!addressListToInsert.isEmpty()) insert addressListToInsert;
    System.debug('Amount of CPU time (in ms) used so far: Insert Address ' + Limits.getCpuTime());  
    if(!caListToInsert.isEmpty()) insert caListToInsert;
    System.debug('Amount of CPU time (in ms) used so far: Insert Person Account ' + Limits.getCpuTime());  
    
    if(!psnAccList.isEmpty()) upsert psnAccList;
    System.debug('Amount of CPU time (in ms) used so far: Insert PSN Account ' + Limits.getCpuTime());  
    
    list<Order__c> orderList = new list<Order__c>();
    
    for(PSNInboundWrapper wrap : inboundWrapperList) {
      if(wrap.listOfAddress.size() > 0) {
        wrap.ordr.Bill_To__c = wrap.listOfAddress[0].Id;
      }
      if(wrap.consumer.personContactId == null) {
        wrap.ordr.Consumer__c = personAccountMap.get(wrap.consumer.Id).personContactId;
      } else {
        wrap.ordr.Consumer__c = wrap.consumer.PersonContactId;
      }
      wrap.ordr.Asset__c = wrap.asset.Id;
      orderList.add(wrap.ordr);
    }
    System.debug('=======orderList==========' + orderList.size());
    if(!orderList.isEmpty()) insert orderList;
    System.debug('Amount of CPU time (in ms) used so far: Insert Order' + Limits.getCpuTime());  
    
    list<Order_Line__c> orderLineList = new list<Order_Line__c>();
    list<Payment__c> paymentList = new list<Payment__c>();
    list<Account> accListToupdate = new list<Account>();
    for(PSNInboundWrapper wrap : inboundWrapperList) {
        Order_Line__c oli = new Order_Line__c();
        oli.Order__c = wrap.ordr.Id;
        oli.Quantity__c = '1';
        oli.SKU__c = wrap.Product <> null ? wrap.Product.SKU__c : null;
        //LM 08/20
        oli.Product_name__c = wrap.Product<> null ? wrap.Product.Name : null;
        oli.Total__c = wrap.inbound.Order_Line_Total__c;
        oli.Line_Number__c = '1';
        orderLineList.add(oli);

        Payment__c payment = new Payment__c();
        payment.Name_on_Card__c = wrap.consumer.Name; 
        payment.Transaction_Date__c = wrap.asset.PPP_Purchase_Date__c;
        payment.Transaction_Type__c = 'enrollment';
        payment.Payment_Status__c = 'Charged';
        //LM 06/16/2014 - commented below line and added modified statement
        //payment.Amount__c = wrap.Product <> null ? wrap.Product.List_Price__c : null;
        payment.Amount__c = wrap.inbound.Order_Line_Total__c;
        payment.Order__c = wrap.ordr.Id;
        payment.Purchase_Type__c = 'PPP';
        paymentList.add(payment);
        
        Account acc = wrap.consumer;
        if(wrap.listOfAddress.size() > 0) {
          acc.Bill_To__pc = wrap.listOfAddress[0].Id;
          accListToupdate.add(acc); 
        }
    }
    System.debug('=========orderLineList=============' + orderLineList.size()); 
    System.debug('=========Orderhelper.processOutbound Order=============' + Orderhelper.processOutbound);
    if(!orderLineList.isEmpty()) insert orderLineList;
    System.debug('Amount of CPU time (in ms) used so far: Insert Order Line' + Limits.getCpuTime());  
    if(!paymentList.isEmpty()) {  insert paymentList;}
    System.debug('Amount of CPU time (in ms) used so far: Insert Payment ' + Limits.getCpuTime());  
    if(!accListToupdate.isEmpty()) update accListToupdate;
    System.debug('Amount of CPU time (in ms) used so far: Update Account ' + Limits.getCpuTime());  
  }

  global void finish(Database.BatchableContext BC){
    System.debug('Amount of CPU time (in ms) used so far: Finish Batch job ' + Limits.getCpuTime());  
  }

  Staging_PPP_PSN_Inbound__c validate(Staging_PPP_PSN_Inbound__c inbound){
      inbound.Processed_Date_Time__c = DateTime.now();
      inbound.Processed_Error_Text__c = '';

      if(!PSN_SKUSet.contains(inbound.Order_Line_Sku__c)) {
         inbound.Processed_Status__c = 'Failure';
         inbound.Processed_Error_Text__c += 'Invalid PSN SKU \n'; 
      }
      if(inbound.Currency_Code__c <> 'USD' && inbound.Currency_Code__c <> 'CAD'  ) {
        inbound.Processed_Status__c = 'Failure';
        inbound.Processed_Error_Text__c += 'Invalid Currency Code \n';
      }
      if(inbound.Transaction_Type__c <> 2) {
        inbound.Processed_Status__c = 'Failure';
        inbound.Processed_Error_Text__c += 'Invalid Transaction Type \n';
      }
      //LM 08/13 commented
      /*
      if(inbound.Address_Address_1__c == null || inbound.Address_Address_1__c.trim() == '') {
        inbound.Processed_Status__c = 'Failure';
        inbound.Processed_Error_Text__c += 'Address Line 1 cannot be blank. \n';
      } */
      
      if(inbound.Address_City__c == null || inbound.Address_City__c.trim() == '') {
        inbound.Processed_Status__c = 'Failure';
        inbound.Processed_Error_Text__c += 'Address City cannot be blank. \n';
      }
      if(inbound.Address_Country__c == null || inbound.Address_Country__c.trim() == '') {
        inbound.Processed_Status__c = 'Failure';
        inbound.Processed_Error_Text__c += 'Address Country cannot be blank. \n';
      }
      if(inbound.Address_State__c == null || inbound.Address_State__c.trim() == '') {
        inbound.Processed_Status__c = 'Failure';
        inbound.Processed_Error_Text__c += 'Address State cannot be blank. \n';
      }
      if(inbound.Address_Zip_Code__c == null || inbound.Address_Zip_Code__c.trim() == '') {
        inbound.Processed_Status__c = 'Failure';
        inbound.Processed_Error_Text__c += 'Address Zip Code cannot be blank. \n';
      }
      if(inbound.Asset_Console_ID__c == null || inbound.Asset_Console_ID__c.trim() == '') {
        inbound.Processed_Status__c = 'Failure';
        inbound.Processed_Error_Text__c += 'Asset Console Id cannot be blank. \n';
      }
      if(inbound.Contact_Email__c == null || inbound.Contact_Email__c.trim() == '') {
        inbound.Processed_Status__c = 'Failure';
        inbound.Processed_Error_Text__c += 'Consumer Email cannot be blank. \n';
      }    
      if(inbound.Contact_First_Name__c == null || inbound.Contact_First_Name__c.trim() == '') {
        inbound.Processed_Status__c = 'Failure';
        inbound.Processed_Error_Text__c += 'Consumer First Name cannot be blank. \n';
      }     
      if(inbound.Contact_Last_Name__c == null || inbound.Contact_Last_Name__c.trim() == '') {
        inbound.Processed_Status__c = 'Failure';
        inbound.Processed_Error_Text__c += 'Consumer Last Name cannot be blank. \n';
      } 
      //LM 3/18 - Commented this code as it is a duplicate validation check
      /*if(inbound.Contact_Last_Name__c == null || inbound.Contact_Last_Name__c.trim() == '') {
        inbound.Processed_Status__c = 'Failure';
        inbound.Processed_Error_Text__c += 'Consumer Last Name cannot be blank. \n';
      }*/ 
      if(inbound.Order_Line_Quantity__c <> 1) {
        inbound.Processed_Status__c = 'Failure';
        inbound.Processed_Error_Text__c += 'Invalid Order Line Quantity. \n';
      }
      //LM 08/14
      Date PPPPurchaseDt = Date.parse(inbound.Asset_Purchase_Date__c.split(' ')[0].replace('-','/'));
      Date UnitpurchaseDt = Date.parse(inbound.Asset_Creation_Date__c.split(' ')[0].replace('-','/'));
      system.debug('>>>>>>>>PPP Purchase Date' + PPPPurchaseDt);
      system.debug('>>>>>>>> Unit Purchase Date' + UnitpurchaseDt);
      if(Date.parse(inbound.Asset_Purchase_Date__c.split(' ')[0].replace('-','/')) < Date.parse(inbound.Asset_Creation_Date__c.split(' ')[0].replace('-','/'))) {
      	inbound.Processed_status__c = 'Failure';
      	inbound.Processed_Error_Text__c += 'Unit Purchase Date greater then PPP Purchase date. \n';
      }
      
      Integer daysdiff = (Date.parse(inbound.Asset_Creation_Date__c.split(' ')[0].replace('-','/'))).daysBetween(Date.parse(inbound.Asset_Purchase_Date__c.split(' ')[0].replace('-','/')));
      system.debug('>>>>>>>>>>>>>>>difference between unit purchase date and PPP purchase date'+ daysdiff);
      if((Date.parse(inbound.Asset_Creation_Date__c.split(' ')[0].replace('-','/'))).daysBetween(Date.parse(inbound.Asset_Purchase_Date__c.split(' ')[0].replace('-','/'))) > 365) {
      	inbound.Processed_status__c = 'Failure';
      	inbound.Processed_Error_Text__c += 'PPP Purchase date is over 365 days from unit purchase date. \n';
      }
      
      system.debug('getModelNumber(inbound.Model_Number__c, null)>>>>>>>>>>>>>>'+ getModelNumber(inbound.Model_Number__c, null));
      system.debug('Inbound Model Number>>>>>>>>>>>>'+inbound.Model_Number__c);
           
      if(inbound.Model_Number__c <> null && getModelNumber(inbound.Model_Number__c, null) == null) {
      	 inbound.Processed_Status__c = 'Failure';
         inbound.Processed_Error_Text__c += 'Invalid Model Number \n';
      }
      System.debug('>>>>>>>>>>+inbound.Processed_Error_Text__c <<<<<<<< ' +inbound.Processed_Error_Text__c );
      System.debug('Amount of CPU time (in ms) used so far: Record validations ' + Limits.getCpuTime());   
      return inbound;
  }
  
  private Static String getModelNumber(String PSNModelNumber, String prdgenre) {
  	 	
  	if(PSNModelNumber == '' || PSNModelNumber == null){
  		 if (prdgenre == 'PS4') {
             return 'PS4NA';
          }else if(prdgenre == 'PS3') {
             return 'PS3NA';  
          }else if(prdgenre == 'Vita') {
             return 'VITANA';  
          } else {
          	return null;
          }
  	 } else {
  	  Product__c prd;
      for( Product__c p : [select Id, SKU__c from Product__c where PSN_Model_Number__c = :PSNModelNumber and sub_type__c = 'Hardware' and Status__c = 'Active' limit 1]) {
      	prd = p;	
      } 
      if(prd != null) {
      	  return prd.SKU__c;	
      }
      return null;			  		
    }
  }  
  
  public class PSNInboundWrapper {
    public Asset__c asset;
    public Order__c ordr;
    public PSN_Account__c psnAccount;
    public list<Address__c> listOfAddress;
    public Account consumer;
    public Staging_PPP_PSN_Inbound__c inbound;
    public Product__c product;
    public boolean isNewAddress;
    
    public PSNInboundWrapper () {
      listOfAddress = new list<Address__c>();
      isNewAddress = false;
      asset = new Asset__c();
      ordr = new Order__c();
      psnAccount = new PSN_Account__c();
      consumer = new Account();
      product = new Product__c();
    } 
  }
}