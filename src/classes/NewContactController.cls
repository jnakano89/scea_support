/************************************************************************************************************************************************************************
// (c) 2013 Appirio, Inc.
//
// Description : Controller Class For VF Page NewContact.
//                  
// Original August 13, 2013  : KapiL Choudhary(JDC) Created for the Task T-171732
   Updated :
            11/17/2014 : Leena Mandadapu : Jira# SMS-864 : Green buttons not appearing on CTI popped Consumer detail page - Added page redirect code to refresh the page
**************************************************************************************************************************************************************************/
public class NewContactController {
    
    //Public Property
    public Contact newContact {get;set;}
    private Account newAccount{get;set;}
    public boolean isInConsole{get;set;}
    private Id personAccountRecId;
    
    //Constructor
    public newContactController(){
        
        newAccount = new Account();
        newContact = new Contact();
        
        String firstNameParam = ApexPages.currentPage().getParameters().get('FirstName');
        String lastNameParam  = ApexPages.currentPage().getParameters().get('LastName');
        String phoneParam     = ApexPages.currentPage().getParameters().get('Phone');
        String emailParam     = ApexPages.currentPage().getParameters().get('Email');
        String languageParam     = ApexPages.currentPage().getParameters().get('Language');
        
                
        if(firstNameParam != null && firstNameParam != ''){
            newContact.FirstName = firstNameParam;
        }
        if(lastNameParam != null && lastNameParam != ''){
            newContact.LastName  = lastNameParam;
        }
        //Change to make phone field unrequired for all cases: Preetu 10/17/2014
        /*if(phoneParam != null && phoneParam != ''){
            newContact.Phone = phoneParam;
        }*/
        if(emailParam != null && emailParam != ''){
            newContact.email     = emailParam;
        }
        if(languageParam != null && languageParam != ''){
            newContact.Preferred_Language__c     = languageParam;
        }

    }
    
    // Here we are inserting Account instead of contact because this account is type of "Person Account".
    // A person account creates a contact itself having the same accountId.
    public Pagereference saveContact(){
        
        //Change to make phone field unrequired for all cases: Preetu 10/17/2014
        /*if(newContact.Phone == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Phone is required.'));
            return null;
        }*/
        
        for(RecordType rT: [SELECT Name, SobjectType,IsPersonType 
                                    FROM RecordType 
                                    WHERE SobjectType='Account' AND IsPersonType=True]){
         personAccountRecId = rT.id;
        }
        
        if(personAccountRecId != null){
            newAccount.RecordTypeId = personAccountRecId;
        }
        
        try{
            newAccount.FirstName   =  newContact.FirstName;
            newAccount.LastName    =  newContact.LastName;
            newAccount.Phone       =  newContact.Phone; 
            newAccount.PersonEmail =  newContact.email;
            newAccount.Preferred_Language__pc = newContact.Preferred_Language__c;
            newAccount.LATAM_Country__pc = newContact.LATAM_Country__c;
            
            insert newAccount;
            
            //list<Account> personAccountList = [select PersonContactId from Account where ID=:newAccount.id];          
            //PageReference contactPage = new ApexPages.StandardController(new Contact(Id = personAccountList[0].PersonContactId)).view();
            //contactPage.setRedirect(true);
            //return contactPage;

            String appId;
            String appName = Label.Service_Console_Name;
            
            for(AppMenuItem app : [select Id from AppMenuItem where Name = :appName]) {
                appId = app.Id;
            }//end-for
            


            //PageReference newPersonAccountPage = new ApexPages.StandardController(newAccount).view();
            //newPersonAccountPage.setRedirect(true);
            //return newPersonAccountPage;

            if(isInConsole) {
              //LM 11/17/2014: Commented the below code for Green Button fix
              //PageReference pg = new PageReference('/' + newAccount.Id);
              //LM 11/17/2014: Added the below 2 lines for Green button fix
              PageReference  pg = new PageReference('/apex/ConsumerSearchRedirect'); 
              pg.getParameters().put('contactId',newAccount.Id);  
              pg.setRedirect(true);
              return pg;    
            } else {
              PageReference pg = new PageReference('/console?tsid=' + appId);
              pg.setAnchor('%2F' + newAccount.Id);
              pg.setRedirect(true);
              return pg;
            }



        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
        return null;
    }
    
    public Pagereference cancelContact(){
        Schema.DescribeSObjectResult R = Contact.SObjectType.getDescribe();
         return new Pagereference('/'+ R.getKeyPrefix()+'/o');
         
    }
    
   
    
    public PageReference cancel(){
        PageReference pg = new Pagereference('/console');
        return pg;  
    }      
}