@isTest
private class SELInboundScheduler_Test {

    static testMethod void myUnitTest() {
    	Product__c prod = TestClassUtility.createProduct(1, false)[0];
    	prod.Duration__c = 24;
    	insert prod;
    	
    	
    	SEL_Logic__c sl = new SEL_Logic__c();
    	sl.SEL_SKU__c = 'PPPSKU00';
    	sl.SCEA_SKU__c = prod.Sku__c;
    	sl.SEL_Model_Number__c = 'PS4******';
    	sl.Product_Type__c = 'PSPSEL';
    	insert sl;
    	
    	
        Staging_PPP_SEL_Inbound__c inbound = new Staging_PPP_SEL_Inbound__c();
        inbound.Address_Address_1__c = 'New address 1';
        inbound.Address_Address_2__c = 'New Address 2';
        inbound.Address_City__c = 'City';
        inbound.Address_Country__c = 'USA';
        inbound.Address_State__c = 'FL';
        inbound.Address_Zip_Code__c = '32835';
        inbound.Asset_PPP_Cancel_Date__c = '20130228';
        inbound.Asset_PPP_Contract_Number__c = 'C123';
        inbound.Asset_PPP_Purchase_Date__c = '20130228';
        inbound.Asset_PPP_SKU__c = 'PPPSKU00';
        inbound.Asset_Proof_Of_Purchase_Date__c = '20130228';
        inbound.Product_PPP_List_Price__c = '100';
        inbound.Order_Dealer_Id__c = '0004933';
        inbound.Asset_Unit_Price__c = '10';
        inbound.Order_LnNum__c = '1';
        inbound.Asset_Unit_Model_Number__c = 'PS410034';
        inbound.Order_Prod_Code__c = 'PS410034';
        inbound.Order_QuantSold__c = '1';
        inbound.Order_RepTag1__c = '76633483';
        inbound.Order_Sale_Type__c = 'A';
        inbound.Order_RepTag2__c = '76633483';
        inbound.Order_Record_Type__c = 'L';
        inbound.Consumer_First_Name__c = 'Test Consumer';
        inbound.Consumer_Last_Name__c = 'Test Last Name';
        inbound.Consumer_Phone1__c = '7597444444';
        inbound.Consumer_Language__c = 'ENG';
        inbound.Order_Manufacture_Code__c = 'SONY';
        insert inbound;
        
        Database.executeBatch(new SELInboundScheduler());
        
    }
    
     static testMethod void myUnitTest2() {
    	Product__c prod = TestClassUtility.createProduct(1, false)[0];
    	//prod.Duration__c = 24;// this cause exception while processing batch.
    	insert prod;
    	
    	
    	SEL_Logic__c sl = new SEL_Logic__c();
    	sl.SEL_SKU__c = 'PPPSKU00';
    	sl.SCEA_SKU__c = prod.Sku__c;
    	sl.SEL_Model_Number__c = 'PS4******';
    	sl.Product_Type__c = 'PSPSEL';
    	insert sl;
    	
    	
        Staging_PPP_SEL_Inbound__c inbound = new Staging_PPP_SEL_Inbound__c();
        inbound.Address_Address_1__c = 'New address 1';
        inbound.Address_Address_2__c = 'New Address 2';
        inbound.Address_City__c = 'City';
        inbound.Address_Country__c = 'USA';
        inbound.Address_State__c = 'FL';
        inbound.Address_Zip_Code__c = '32835';
        inbound.Asset_PPP_Cancel_Date__c = '20130228';
        inbound.Asset_PPP_Contract_Number__c = 'C123';
        inbound.Asset_PPP_Purchase_Date__c = '20130228';
        inbound.Asset_PPP_SKU__c = 'PPPSKU00';
        inbound.Asset_Proof_Of_Purchase_Date__c = '20130228';
        inbound.Product_PPP_List_Price__c = '100';
        inbound.Order_Dealer_Id__c = '0004933';
        inbound.Asset_Unit_Price__c = '10';
        inbound.Order_LnNum__c = '1';
        inbound.Asset_Unit_Model_Number__c = 'PS410034';
        inbound.Order_Prod_Code__c = 'PS410034';
        inbound.Order_QuantSold__c = '1';
        inbound.Order_RepTag1__c = '76633483';
        inbound.Order_Sale_Type__c = 'A';
        inbound.Order_RepTag2__c = '76633483';
        inbound.Order_Record_Type__c = 'L';
        inbound.Consumer_First_Name__c = 'Test Consumer';
        inbound.Consumer_Last_Name__c = 'Test Last Name';
        inbound.Consumer_Phone1__c = '7597444444';
        inbound.Consumer_Language__c = 'ENG';
        inbound.Order_Manufacture_Code__c = 'SONY';
        insert inbound;
        
        Database.executeBatch(new SELInboundScheduler());
        
    }
}