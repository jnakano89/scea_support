/******************************************************************************
Class         : ChangeCaseStatusCtrl
Description   : This is controlle class for Change Case Status VF page which is added at 
                                case actions to update status.
Developed by  : Urminder Vohra (JDC)

Updates       :
01/06/2014    : Urminder : updated to add check for service type cases not to show description in that case(T-226066) 
05/16/2014    : Andy Getz (AG) : included code to create new Case Comment with each chatter post SF-198
06/05/2014    : Aaron Briggs : SF-1065 : Check to prevent an empty string from being saved                   
******************************************************************************/
public with sharing class ChangeCaseStatusCtrl {
  Case cs;
  public String inputText{get;set;}
  public boolean isServiceRecord{get;set;}
  public boolean hasErrMsg{get{return Apexpages.hasMessages();}set;}
  public ChangeCaseStatusCtrl(Apexpages.StandardController ctrl) {
        cs = (Case)ctrl.getRecord();
        //for(Case c : [select Record_Type_Name__c from Case where Id = :cs.Id]) {
        //      isServiceRecord = c.Record_Type_Name__c == 'Service';
        //}
  }
   
  public void save() {
        //Adding a Text post
        /* Commenting out case save AG 05/20/14
        try {
          update cs;
        } catch(Exception ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
                return;
        }
        */
        
        // AB - 06/05/2014 - Added condition to look for entries with one or multiple spaces   
        if(inputText <> null && inputText <> '' && inputText.remove(' ')  <> '') {
          FeedItem post = new FeedItem();
          post.ParentId = cs.Id; 
          post.Body = inputText;
          insert post;
          // new code AG 05-13-2014
          CaseComment com = new CaseComment();
          com.ParentId = cs.Id;
          com.CommentBody = inputText;
          insert com;
          // End new code AG 05-13-2014
        }        
  }
}