/******************************************************************************
Class         : RetryFailedSettlementsBatch
Description   : Develop code to Retry processing failed Settlements
Developed by  : Harish Khatri (JDC)
Date          : 01/22/2014
Task		  : T-221027           
******************************************************************************/
global class RetryFailedSettlementsBatch  implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts{

	global void execute(SchedulableContext SC) { // Start Batch
		Database.executeBatch(new RetryFailedSettlementsBatch(), 5);
	}

	global final String PAYMENT_FAILED = 'AVS Failed';
	Set<Decimal> integrationError = new Set<Decimal>{150,207,236};
	global final Date dateToValidate = Date.today().addDays(-1);

	global String ACCESS_KEY_STR;
	global String ALGORITHM_NAME;
	global String LOCALE_STR;
	global String PROFILE_ID_STR;
	global String SECRET_KEY;
	global String PAYMENT_REQUEST_URL;

	public static string PAYMENT_STATUS_CHARGED    = 'Charged';        

	global Database.QueryLocator start(Database.BatchableContext BC){
		//String qry = 'select Id from Payment__c where Payment_Status__c = ' + PAYMENT_FAILED + ' AND createdDate > :dateToValidate';

		String qry = 'Select p.Token__c,p.Order__r.Case__c,p.Order__r.Case__r.SIRAS_Date__c, p.Order__r.order_date__c,Purchase_Type__c,'+
		'  p.Order__r.Name,p.Order__c, p.Id From Payment__c p '+
		' where Cybersource_Response_Code__c IN :integrationError  AND createdDate > :dateToValidate';
		return Database.getQueryLocator(qry);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope){


		for(Payment__c payment : (list<Payment__c>) scope) {
			try {
				System.debug('payment --- '+payment);
				String transaction_type = 'sale';
				String paymentToken = payment.Token__c;
				Date orderDate = payment.order__r.order_date__c;
				String year = String.valueOf(orderDate.year());
				String targetDate = year;               
				String month = String.valueOf(orderDate.month()).length() == 2 ? '' + orderDate.month() : '0' + orderDate.month();
				targetDate += month;
				String day = String.valueOf(orderDate.day()).length() == 2 ? '' + orderDate.day() : '0' + orderDate.day();
				targetDate += day;

				Payment__c toBeUpdatePayment = CyberSourcePaymentController.insertPaymentRecord_New(payment.Order__r.Name,targetDate,payment.Order__c, payment.Id,  transaction_type,  paymentToken, true, payment.Purchase_Type__c);

				if(toBeUpdatePayment==null){

					System.debug('>>>>>>>>>>>>PAYMENT IS NULL ===');
					IntegrationAlert.addException('Settlement fails automated process Payment transaction null', 'Unable to retrieve payment for existing order.  Please contact your supervisor.', null);
					//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to retrieve payment for existing order.  Please contact your supervisor.'));
					//valid=false;

				}else{

					if(payment.Order__r.Case__c!=null){
						Case cs = new Case(Id = payment.Order__r.Case__c);
						if(toBeUpdatePayment <> null && toBeUpdatePayment.Id <> null && toBeUpdatePayment.Payment_Status__c == PAYMENT_STATUS_CHARGED) {
							if(payment.Order__r.Case__r.SIRAS_Date__c == null) {
								cs.Status = CaseManagement.CASE_STATUS_ASSIGNED_CORPORATE_TEAM_T3;
							} else {
								cs.Status = CaseManagement.CASE_STATUS_PAYMENT_COMPLETE;
							}
						}
						update cs;
					}
				}
			} catch(Exception ex) {
				IntegrationAlert.addException('Settlement fails automated process', 'In case the Settlement fails to process due to any error, build an automated process to retry all the failed transactions', ex);	
			}
		}
		/*
    	Cybersource__c settings = Cybersource__c.getOrgDefaults();

        if(settings <> null) {
            ACCESS_KEY_STR  = settings.Access_Key__c <> null ? settings.Access_Key__c : '';
            ALGORITHM_NAME = settings.Algorithm_Name__c <> null ? settings.Algorithm_Name__c : ''; 
            LOCALE_STR = settings.Locale__c <> null ? settings.Locale__c : ''; 
            PROFILE_ID_STR = settings.Profile_ID__c <> null ? settings.Profile_ID__c : '';
            SECRET_KEY = Label.CyberSource_Secret_Key; 
            PAYMENT_REQUEST_URL = settings.Payment_Request_URL__c;
        }

    	for(Payment__c payment : (list<Payment__c>) scope) {

    		//String body = getBody(merchantReferenceNumber, targetDate, requestID);
    		HttpRequest req = new HttpRequest();
		    HttpResponse res = new HttpResponse();
		    Http http = new Http();
		    req.setEndpoint(PAYMENT_REQUEST_URL);
			//Blob headerValue = Blob.valueOf('djotwani:Appir10Test');
			//String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
			//req.setHeader('Authorization', authorizationHeader);
		    req.setMethod('POST');
		    req.setBody('Dummy Body');
		    res = http.send(req);
		    System.debug('==response==' + res.getBody());
    	}*/
	}

	global void finish(Database.BatchableContext BC){

	}
}