@isTest
global class StagingContactManagement_Test {

    static testMethod void myUnitTest() {
    	Test.startTest();
      	Test.setMock(WebServiceMock.class, new WebServiceMockUpdateConsumer());
    	OSB__c settings = new OSB__c();
    	settings.Update_Consumer_Enabled__c = true;
    	insert settings;
    	
    	list<Contact> cntList = TestClassUtility.createContact(2, true);
        Staging_Contact__c stagingCnt = new Staging_Contact__c();
        stagingCnt.Contact__c = cntList[0].Id;
        stagingCnt.MDM_Contact_ID__c = '1-1';
        stagingCnt.Phone__c = '12334';
        insert stagingCnt;
        Test.stopTest();
    }
     global class WebServiceMockUpdateConsumer implements WebServiceMock {
	   global void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,	
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	      		Sony_MiddlewareUpdateconsumer_Final.PSUpdateConsumer_Output_element responseElm = 
	      		  	new Sony_MiddlewareUpdateconsumer_Final.PSUpdateConsumer_Output_element();
	       		
	       		
				responseElm.Message = 'Updated SucessFully';
			    responseElm.Status = 'Sucess'; 
	       		response.put('response_x', responseElm); 
	   		   }
    }
}