public abstract class GenericObject {
    public map<string,object> fields {
        get{
            if(fields==null){
                fields = new map<string,object>();
            }
            return fields;
        }
        set{
            fields = value;
        }
    }
    
    public String formatDateAsString(String val)
    {
    	if (attributes.containsKey(val)) return Date.valueOf(attributes.get(val)).format();
    	else return null;
    }
    
    public Date formatDate(String val)
    {
    	if (attributes.containsKey(val)) return Date.valueOf(attributes.get(val));
    	else return null;
    }
    
    public map<string,string> attributes {get;set;}
    //an Id field for object to identify.
    public String objId{get;set;}
    
    public GenericObject()
    {       
        attributes = new map<string,string>();
    }
    
    public object getVal(String fieldName){
            try{
                return fields.get(fieldName);
            }
      catch (Exception ex){
        ExceptionHandler.logException(ex);
        return null;   
      }
    }
    public void setVal(String fieldName, object value){
        fields.put(fieldName, value);
    }
    
  public void initJson(String objectDef){
    JSONParser parser = JSON.createParser(objectDef);
    String fieldName = '';
    String fieldValue = '';
    while (parser.nextToken() != null) {             
      if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
        if(!String.isEmpty(parser.getText())){
          if(!String.isEmpty(fieldName)){
            this.attributes.put(fieldName, fieldValue);
          }
          fieldName = parser.getText();
          fieldValue = '';
          system.debug('!!!'+fieldName);              
        }
      }else{
        String value = parser.getText();
        if(!String.isEmpty(value)){
         if(String.isEmpty(fieldValue)){
           fieldValue += ' '+fieldValue;
         }
         fieldValue += value;
        }
      }
    }
    if(this.attributes.get(fieldName)==null){
      this.attributes.put(fieldName, fieldValue);
    } 
  }
    
    
  public class TTDTaxObject
  {
    public Decimal tax;
  }
    
    // To hold any Attributed map object.
	public class GenericAttributedObj extends GenericObject {
		public GenericAttributedObj(){}
	} 

  public class OrderObject extends GenericObject{
        
        public OrderLine[] lineItems {get;set;}
        public GenericObject[] primaryParty {get;set;}
        public GenericObject[] shipToParty {get;set;}
        public GenericObject[] billToParty {get;set;}
        
        public OrderObject()
        {               
            lineItems = new OrderLine[]{};
            primaryParty = new OrderObject[]{};
            billToParty = new OrderObject[]{};
            shipToParty = new OrderObject[]{};
            system.debug('******fields?::'+fields);
        }
        
    }       
  
    public class OrderLine extends GenericObject{    	
      public GenericObject[] Charges{get;set;}
      
      public OrderLine()
      {
          Charges = new OrderLine[]{};
      }
    }
}