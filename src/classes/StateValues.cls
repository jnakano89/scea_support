public class StateValues{
public Boolean haserror{get;set;}
public String selectedCountry2{get;set;}
Public string oldStatus='Offline';
 public statevalues(){
 haserror = false;
 
 }
 
public void store_Variable(){
  haserror = true;
 integer t = 0;
 t = t+1;
 oldStatus ='Offline';
 System.debug('__________-------'+t);
}
public List<SelectOption> getStatusOptions() {
        List<SelectOption> StatusOptions= new List<SelectOption>();
        //StatusOptions.add(new SelectOption('','-None-'));
        StatusOptions.add(new SelectOption('Break','Break'));
        StatusOptions.add(new SelectOption('Billable Training','Billable Training'));
        StatusOptions.add(new SelectOption('Manager Approved','Manager Approved'));
        StatusOptions.add(new SelectOption('Admin','Admin'));
        StatusOptions.add(new SelectOption('Meal','Meal'));                
 
        return StatusOptions;
    }
    public PageReference save(){
        
        Id AgentInfo = UserInfo.GetUserId();
        Tracking_Agent_Session_Time__c  TrackAgSessionAvail = New Tracking_Agent_Session_Time__c ();
        Integer TrackSessionCount = [Select count() from Tracking_Agent_Session_Time__c  where AgentId__c =: UserInfo.getUserId()];
        Datetime CurrentTime =  System.now(); 
                
        if(TrackSessionCount > 0){
            TrackAgSessionAvail = [Select Admin__c,Break__c,Meal__c,BillableTraining__c,ManagerApproved__C,AgentId__c,StatusOptionType__c,Id,ManagerApprovedTime__c from Tracking_Agent_Session_Time__c  where AgentId__c =: UserInfo.getUserId() order by CreatedDate desc Limit 1];
            TrackAgSessionAvail.StatusOptionType__c = SelectedCountry2;
            TrackAgSessionAvail.ManagerApprovedTime__c = CurrentTime;           
            update TrackAgSessionAvail;
        }
        else{
            Tracking_Agent_Session_Time__c TrackingAgentSession = New Tracking_Agent_Session_Time__c();
            TrackingAgentSession.AgentId__c = UserInfo.GetUserId();
            TrackingAgentSession.StatusOptionType__c = SelectedCountry2;
            TrackingAgentSession.ManagerApprovedTime__c = CurrentTime;              
            insert TrackingAgentSession;
        }
        return (new Pagereference('javascript:window.close()'));
    }
    public void UpdateRecord(){
       /* String CurrentStatus = apexpages.currentpage().getparameters().get('CurrentStatus');
        String OldStatus = apexpages.currentpage().getparameters().get('OldStatusVal');
        String TimeBefore = apexpages.currentpage().getparameters().get('TimeGoingAway');
        String Timenow = apexpages.currentpage().getparameters().get('TimeComingOnline');
        System.debug('----+++++++-------'+CurrentStatus  + '_______'+ OldStatus +'============'+ TimeBefore + '---------' + Timenow + ' ---------'+ UserInfo.GetUserId());
        List<String> NewOne = New List<String>();
        NewOne = Timenow.split(':',5);
        integer HoursNow = integer.valueof(NewOne[0]);
        integer MinutesNow = integer.valueof(NewOne[1]);
        integer SecondsNow = integer.valueof(NewOne[2]);
        
        List<String> OldOne = New List<String>();
        OldOne = TimeBefore.split(':',5);
        integer HoursOld = integer.valueof(OldOne[0]);
        integer MinutesOld = integer.valueof(OldOne[1]);
        integer SecondsOld = integer.valueof(OldOne[2]);
        
        Integer TotalTimeInsecs = ((Hoursold - HoursNow)* 3600) + ((MinutesNow- MinutesOld)*60) + (SecondsNow - SecondsOld);
        System.debug('-------------Total Time '+ TotalTimeInsecs );*/
        
        Datetime CurrentTimeNew =  System.now();
        Integer TotalTimeInsecs = 0;
        
        Tracking_Agent_Session_Time__c  TrackAgSession = New Tracking_Agent_Session_Time__c ();
        Integer countTrackAgSession = [Select count() from Tracking_Agent_Session_Time__c  where AgentId__c =: UserInfo.getUserId()];
        if(countTrackAgSession > 0){
                    TrackAgSession = [Select Admin__c,Break__c,Meal__c,BillableTraining__c,ManagerApproved__C,AgentId__c,StatusOptionType__c,Id,ManagerApprovedTime__c from Tracking_Agent_Session_Time__c  where AgentId__c =: UserInfo.getUserId() order by CreatedDate DESC Limit 1];
            TotalTimeInsecs = (CurrentTimeNew.getTime() - TrackAgSession.ManagerApprovedTime__c.getTime()).intValue()/1000;
        
            System.debug('-------------*****Total Time1 '+ TotalTimeInsecs );
        
            if(TrackAgSession.StatusOptionType__c == 'Manager Approved'){
                TrackAgSession.ManagerApproved__C = TrackAgSession.ManagerApproved__C + TotalTimeInsecs;                                  
            }                        
            if(TrackAgSession.StatusOptionType__c == 'Billable Training'){
                TrackAgSession.BillableTraining__c = TrackAgSession.BillableTraining__c + TotalTimeInsecs;            
            }
            if(TrackAgSession.StatusOptionType__c == 'Admin'){
                TrackAgSession.Admin__c = TrackAgSession.Admin__c + TotalTimeInsecs;            
            }
            if(TrackAgSession.StatusOptionType__c == 'Break'){
                TrackAgSession.Break__c = TrackAgSession.Break__c + TotalTimeInsecs;            
            }
            if(TrackAgSession.StatusOptionType__c == 'Meal'){
                TrackAgSession.Meal__c = TrackAgSession.Meal__c + TotalTimeInsecs;            
            }
            TrackAgSession.StatusOptionType__c = '';
            TrackAgSession.ManagerApprovedTime__c = null;
            if(TrackAgSession != null){
                Update TrackAgSession;
            } 
        }
          
    }
}