public with sharing class CyberSourcePaymentCompleteController {

	public String caseId{get;set;}
	public Case selectedCase{get;set;}

	
	public CyberSourcePaymentCompleteController() {
    
		caseId = Apexpages.currentPage().getParameters().get('caseId');

	}		

	public PageReference complete() {
    	
    	//return new PageReference('/' + caseId);
	
	

            
            PageReference newPage  = new PageReference('/console');
            newPage.setAnchor('%2F' + caseId);
		 	newPage.setRedirect(true);
			return newPage;
	}
	
}