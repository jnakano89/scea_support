/******************************************************************************
Class         : NewAssetController_Test
Description   : Test class for NewAssetController
Developed by  : Poonam Varyani
Date          : August 28, 2013

Updates:
9/30/2013 : Urminder(JDC) : added Mock Test.
              
******************************************************************************/
@isTest
global class NewAssetController_Test {

    static testMethod void newAssetTest() {
    	//create intrgartion rec
    	Integration__c integration = TestClassUtility.createIntegrationRec(true); 
    	Siras_Warranty_Code__c warrantyCodes = new Siras_Warranty_Code__c();
    	warrantyCodes.Name = '12';
    	warrantyCodes.Description__c = 'Test Warranty Code';
    	insert warrantyCodes;
    	//create account
    	String rtId;
    	for(RecordType rt : [select Id from RecordType where SobjectType = 'Account']) {
    		rtId = rt.Id;
    		break;
    	}
    	
        Account account = new Account();
        account.Name = 'test contact';
        account.RecordTypeId = rtId;
        insert account;
    	
    	Contact contact = new Contact();
    	contact.LastName = 'lname';
    	contact.AccountId = account.Id;
    	contact.MDM_Account_ID__c = '1-2-3';
    	insert contact;
    	List<Asset__c> assetsList = TestClassUtility.createAssets(1, true);
    	
    	Case testcase = new Case();
	    testcase.Status = 'Active';
	    testcase.received_date__c = date.today();
	    testcase.shipped_date__c = date.today().addDays(1);
	    testcase.outbound_tracking_number__c = '101';
	    testcase.outbound_carrier__c = 'TYest1';
    	testcase.Offender__c = 'Test';
        testCase.Product__c = 'PS4';
        testCase.Sub_Area__c = 'Account Help';
    	insert testCase;
    	
    	PageReference pg = Page.NewAsset; 
    	Test.setCurrentPage(pg); 
    	ApexPages.currentPage().getParameters().put('SerialNumber', '123456');
    	ApexPages.currentPage().getParameters().put('accountId', account.Id);
    	
    	ApexPages.currentPage().getParameters().put('caseId', testCase.Id);
    	Apexpages.currentPage().getParameters().put('oldAssetId', assetsList[0].Id);
    	Apexpages.currentPage().getParameters().put('exchange', '1');
    	Test.startTest();
    	Test.setMock(HttpCalloutMock.class, new HTTPMockCyberSource());
    	NewAssetController assetObj = new NewAssetController();
    	
    	assetObj.saveAsset();
    	
    	//AB - 05/21/2014 - Hard-coded model number to pass validation check.
    	//assetObj.newAsset.Model_Number__c = '123';
    	assetObj.newAsset.Model_Number__c = 'CUH-1001A';
    	assetObj.saveAsset();
    	assetObj.newAsset.Serial_Number__c = null;
    	assetObj.saveAsset();
    	
    	Case updatedCase = [select Asset__c from Case where Id = :testCase.Id];
    	System.assert(updatedCase.Asset__c <> null, 'Asset should be updated');
    	assetObj.cancelAsset();
    	ApexPages.currentPage().getParameters().put('caseId', null);
    	NewAssetController assetObj2 = new NewAssetController();
    	assetObj2.cancelAsset();
    	Test.stopTest();
    }
    
    
    static testMethod void newNewAssetTest() {
    	//create intrgartion rec
    	Integration__c integration = TestClassUtility.createIntegrationRec(true); 
    	Siras_Warranty_Code__c warrantyCodes = new Siras_Warranty_Code__c();
    	warrantyCodes.Name = '12';
    	warrantyCodes.Description__c = 'Test Warranty Code';
    	insert warrantyCodes;
    	//create account
    	String rtId;
    	for(RecordType rt : [select Id from RecordType where SobjectType = 'Account']) {
    		rtId = rt.Id;
    		break;
    	}
    	
        Account account = new Account();
        account.Name = 'test contact';
        account.RecordTypeId = rtId;
        insert account;
    	
    	Contact contact = new Contact();
    	contact.LastName = 'lname';
    	contact.AccountId = account.Id;
    	contact.MDM_Account_ID__c = '1-2-3';
    	insert contact;
    	List<Asset__c> assetsList = TestClassUtility.createAssets(1, true);
    	
    	Case testcase = new Case();
	    testcase.Status = 'Active';
	    testcase.received_date__c = date.today();
	    testcase.shipped_date__c = date.today().addDays(1);
	    testcase.outbound_tracking_number__c = '101';
	    testcase.outbound_carrier__c = 'TYest1';
    	testcase.Offender__c = 'Test';
        testCase.Product__c = 'PS4';
        testCase.Sub_Area__c = 'Account Help';
    	insert testCase;
    	
    	PageReference pg = Page.NewAsset; 
    	Test.setCurrentPage(pg); 
    	ApexPages.currentPage().getParameters().put('SerialNumber', '123456');
    	ApexPages.currentPage().getParameters().put('accountId', account.Id);
    	
    	ApexPages.currentPage().getParameters().put('caseId', testCase.Id);
    	
    	Test.startTest();
    	Test.setMock(HttpCalloutMock.class, new HTTPMockCyberSource());
    	NewAssetController assetObj = new NewAssetController();
    	
    	assetObj.saveAsset();
    	
    	//AB - 05/21/2014 - Hard-coded model number to pass validation check.
        //assetObj.newAsset.Model_Number__c = '123';
        assetObj.newAsset.Model_Number__c = 'CUH-1001A';
    	assetObj.saveAsset();
    	assetObj.newAsset.Serial_Number__c = null;
    	assetObj.saveAsset();
    	
    	Case updatedCase = [select Asset__c from Case where Id = :testCase.Id];
    	System.assert(updatedCase.Asset__c <> null, 'Asset should be updated');
    	assetObj.cancelAsset();
    	ApexPages.currentPage().getParameters().put('caseId', null);
    	NewAssetController assetObj2 = new NewAssetController();
    	assetObj2.cancelAsset();
    	Test.stopTest();
    }
    
     global class HTTPMockCyberSource implements HttpCalloutMock  {
	   global HTTPResponse respond(HTTPRequest req) {
      		System.assertEquals('https://qarouter.siras.com/edes/', req.getEndpoint());
	        System.assertEquals('POST', req.getMethod());
	        HttpResponse res = new HttpResponse();
	        //res.setHeader('Content-Type', 'application/json');
	        String body = getResponse();
	        res.setBody(body);
	        res.setStatusCode(200);
	        return res;
	  }
	  global String getResponse(){
	  	String response = '<?xml version=\'1.0\'?>';
	
		response +=			'			<Serial-Number-Inquiry-Response version=\'3.0\'>';
		response +=			'					<Response-Header>';
		response +=			'					   <Response-Status>100</Response-Status>';
		response +=			'					   <Response-Status-Description>Request Processed</Response-Status-Description>';
		response +=			'					   <Partner-ID>scea-crm-edes2</Partner-ID>';
		response +=			'					   <Response-Email-Address Type="" />';
		response +=			'					   <Confirmation-Type>2</Confirmation-Type>';
		response +=			'					   <Transmission-ID />';
		response +=			'					   <Location-ID />';
		response +=			'					   <Creation-Date>20130809</Creation-Date>';
		response +=			'					   <Creation-Time>110301</Creation-Time>';
		response +=			'					   <Creation-Time-Zone>GMT-07:00</Creation-Time-Zone>';
		response +=			'					</Response-Header>';
							
		response +=			'					<Product-Information>';
		response +=			'					   <Reason-Code />';
		response +=			'					   <Reason-Description />';
		response +=			'					   <SiRAS-Inquiry-ID>7980682</SiRAS-Inquiry-ID>';
		response +=			'					   <UPC />';
		response +=			'					   <Serial-Number>1232145425</Serial-Number>';
		response +=			'					   <Item-Description />';
		response +=			'					   <Item-Number />';
		response +=			'					   <Brand-Name>SONY PLAYSTATION</Brand-Name>';
		response +=			'					   <Brand-ID>9</Brand-ID>';
		response +=			'					   <Sold-By-Retailer />';
		response +=			'					   <Sold-By-Retailer-ID />';
		response +=			'					   <Sold-Date />';
		response +=			'					   <Manufacturer-Warranty-Determination>12</Manufacturer-Warranty-Determination>';
		response +=			'					   <Manufacturer-Warranty-Parts-Determination>12</Manufacturer-Warranty-Parts-Determination>';
		response +=			'				   <Manufacturer-Warranty-Labor-Determination>12</Manufacturer-Warranty-Labor-Determination>';
		response +=			'				</Product-Information>';
							
		response +=			'					<Response-Trailer>';
		response +=			'					   <Expected-Count>1</Expected-Count>';
		response +=			'					   <Actual-Count>1</Actual-Count>';
		response +=			'					</Response-Trailer>';
		response +=			'					</Serial-Number-Inquiry-Response>';
	  	
	  	
	  	return response;
	  }
    }
}