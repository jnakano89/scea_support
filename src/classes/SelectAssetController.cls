//8/26/2013 : Urminder : created this class.
public with sharing class SelectAssetController {
  
  public list<Asset__c> assetList{get;set;}
  
  public string caseId;
  public string accountId;
        
  public string title{get;set;}
  public string subTitle{get;set;}
  public String selectedAssetId{get;set;}
  public Case selectedCase{get;set;}
  public String productType{get;set;}
  public boolean showNewProductButton{get;set;}
  
  public SelectAssetController() {
    assetList = new list<Asset__c>();
    subTitle = 'Select Asset';
    showNewProductButton = false;
    caseId = Apexpages.currentPage().getParameters().get('caseId');
    
    String contactId = getAssociatedContactId(caseId);
    if(contactId <> null) {
      assetList = getAssetList(contactId);  
    }
   /*
   Changed: Charudatta Mandhare
   Date: 12/019/2012
   Comments: Changed due to feedback from demo.  User should be able to create new assets.
             Also removed message.
   
   */
   /*
   Noopur :- Moved this code to SmartAssetSearch For the Exchange Functionality
   */
   // if(assetList.size() > 0 && isExchange != '0'){
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'ASSET ALREADY IN USE.'));
        //disableNew = true;
   // }
   
   for(Product_Filter_Mapping__c pfm : Product_Filter_Mapping__c.getAll().values()) {
      	if(pfm.Name == selectedCase.Product__c) {
      		showNewProductButton = true;
      	}
      } 
  }
  
   public Pagereference createAsset() {
    
    if(caseId != null && caseId != ''){
        
        
        for(Case cs: [select AccountId from Case where Id =:caseId]){
            accountId = cs.AccountId;
        }
        
        System.debug('>>>>>>>>>>>createAsset<<<<<<<<<<<');      
        System.debug('>>>>>>>caseId='+caseId);  
        System.debug('>>>>>>>accountId='+accountId);    
                
        
        if(accountId != null && accountId != ''){
            Pagereference smartAssetSearch = new PageReference('/apex/SmartAssetSearch?retId='+caseId+'&accountId='+accountId+'&caseId='+caseId);
            return smartAssetSearch;
        }
    }
    return null;
   }
  public Pagereference selectAsset() {
    String assetId = ApexPages.currentPage().getParameters().get('ctRadio');
    
    if(assetId == null) {
        
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select an asset'));
      return null;
    }
    
    return updateObject(assetId);
  }
  
  public Pagereference clearAsset() {
    return updateObject(null);
  }
  
  private Pagereference updateObject(String assetId) {
    
    selectedCase.Asset__c = assetId;
    selectedCase.SCEA_Product__c = null;
    
    try{
        update selectedCase;
    }catch(Exception ex) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,ex.getMessage()));
    }
    return returnToRecord();
  }
  
  public Pagereference returnToRecord() {
    Pagereference pg = new ApexPages.StandardController(selectedCase).view();
    pg.setRedirect(true); 
    return pg;
  }

  private list<Asset__c> getAssetList(String cntId) {
    list<Asset__c> aList = new list<Asset__c>();
    Set<Id> assetIds = new Set<Id>();
    
    for(Consumer_Asset__c ca : [select Asset__c from 
                                    Consumer_Asset__c
                                    where Consumer__c = :cntId]) {
        assetIds.add(ca.Asset__c);
    }
    
    for(Asset__c a : [select Serial_Number__c, Model_Number__c, Purchase_Date__c, 
                            Registration_Date__c,Warranty_Details__c, Retailer__c,
                            Description__c, Siras_Purchase_Date__c, Warranty_Available__c   
                        from Asset__c
                        where Id IN : assetIds]) {
        aList.add(a);               
    } 
    return aList;
  }
  
  private String getAssociatedContactId(String caseId) {
    String cntId;
    for(Case c : [select ContactId, Contact.Name, Asset__c, caseNumber, Product__c
                    from Case
                    where Id = :caseId]) {
      cntId = c.ContactId;
      title = c.Contact.Name;
      selectedAssetId = c.Asset__c;
      productType = c.Product__c;
      selectedCase = c;                 
    }
    return cntId;
  }

   public Pagereference newPeripheral () {
    
    if(caseId != null && caseId != ''){
        
        Pagereference smartAssetSearch = new PageReference('/apex/NewPeripheral?retId='+caseId+'&caseId='+caseId);
        smartAssetSearch.setRedirect(true);
        return smartAssetSearch;
    }
    return null;
 	}
 
  
  
}