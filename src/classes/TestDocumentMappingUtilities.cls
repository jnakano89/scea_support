@isTest
private class TestDocumentMappingUtilities {

    private static String sampleXML = 
        '<ShowSalesOrder revision=\"8.0\" xmlns=\"http://www.maskedurl.com\">' +
            '<DataArea>' + 
                '<Show confirm=\"Never\" xmlns:NS1=\"http://www.maskedurl.com\" xmlns:NS2=\"http://www.maskedurl.com\" xmlns:MyNamespace=\"http://www.MyNamespace.com\" xmlns:NS3=\"http://www.maskedurl.com\" xmlns:NS4=\"http://www.maskedurl.com\" xmlns:NS5=\"http://www.maskedurl.com\" xmlns:NS6=\"http://www.maskedurl.com\" />' + 
                '<SalesOrder xmlns:NS1=\"http://www.maskedurl.com\" xmlns:NS2=\"http://www.maskedurl.com\" xmlns:MyNamespace=\"http://www.MyNamespace.com\" xmlns:NS3=\"http://www.maskedurl.com\" xmlns:NS4=\"http://www.maskedurl.com\" xmlns:NS5=\"http://www.maskedurl.com\" xmlns:NS6=\"http://www.maskedurl.com\">' + 
                    '<Header>' + 
                        '<RecordIds>' + 
                            '<RecordId>' +
                                '<Id>Test-12345</Id>' +
                            '</RecordId>' + 
                        '</RecordIds>' +
                        '<TotalAmount currency=\"USD\">95.75</TotalAmount>' + 
                        '<PaymentTerms>' + 
                            '<UserArea>' + 
                                '<MyNamespace:PaymentMethod>' +  
                                    '<MyNamespace:MethodId>Credit Card</MyNamespace:MethodId>' + 
                                    '<MyNamespace:CCInfo>' + 
                                        '<MyNamespace:AccountType>Visa</MyNamespace:AccountType>' + 
                                        '<MyNamespace:AccountName>Deadbeat Lebowski</MyNamespace:AccountName>' +  
                                        '<MyNamespace:ExpMonth>08</MyNamespace:ExpMonth>' + 
                                        '<MyNamespace:ExpYear>2013</MyNamespace:ExpYear>' + 
                                    '</MyNamespace:CCInfo>' +
                                '</MyNamespace:PaymentMethod>' + 
                            '</UserArea>' + 
                        '</PaymentTerms>' +
                    '</Header>' + 
                    '<Line>' + 
                        '<LineNumber>1</LineNumber>' + 
                        '<OrderItem>' + 
                            '<Description>Sham-WOW</Description>' + 
                        '</OrderItem>' + 
                        '<OrderQuantity uom=\"Each\">1</OrderQuantity>' + 
                        '<UnitPrice>' + 
                            '<Amount currency=\"USD\">49.95</Amount>' + 
                            '<PerQuantity uom=\"Each\">1</PerQuantity>' + 
                        '</UnitPrice>' + 
                        '<Priority>Standard</Priority>' + 
                        '<Tax>' + 
                            '<TaxAmount currency=\"USD\">3.25</TaxAmount>' + 
                            '<PercentQuantity uom=\"Each\">6.5</PercentQuantity>' +
                        '</Tax>' + 
                        '<ShipDate>2013-08-02T00:00:00-08:00</ShipDate>' +
                    '</Line>' + 
                    '<Line>' + 
                        '<LineNumber>2</LineNumber>' + 
                        '<OrderItem>' +
                            '<Description>Slap Chop</Description>' + 
                        '</OrderItem>' + 
                        '<OrderQuantity uom=\"Each\">1</OrderQuantity>' + 
                        '<UnitPrice>' + 
                            '<Amount currency=\"USD\">39.95</Amount>' + 
                            '<PerQuantity uom=\"Each\">1</PerQuantity>' + 
                        '</UnitPrice>' + 
                        '<Priority>Standard</Priority>' + 
                        '<Tax>' + 
                            '<TaxAmount currency=\"USD\">2.6</TaxAmount>' + 
                            '<PercentQuantity uom=\"Each\">6.5</PercentQuantity>' + 
                        '</Tax>' + 
                        '<ShipDate>2009-10-10T00:00:00-08:00</ShipDate>' +
                    '</Line>' + 
                '</SalesOrder>' + 
            '</DataArea>' + 
        '</ShowSalesOrder>';

    /*
    Demonstrates how to use the DocumentMappingUtilities class by converting an XML Document 
    to a JSON and then casting the JSON to the target class. Contains all the Object Map
    and Attribute Map records required to demonstrate how an Object Map works
    */
    static testMethod void testObjectMapUsingMapStyleCollection() {
        
        //Insert Object Map
        Object_Map__c objectMap = createObjectMap('ShowSalesOrder to MOrder', 'ShowSalesOrder/DataArea/SalesOrder', 'MOrder', true, null, null);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/PaymentTerms/UserArea/PaymentMethod/CCInfo/ExpYear', 'expiry_Year', 'Integer', objectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/PaymentTerms/UserArea/PaymentMethod/CCInfo/AccountName', 'bill_To_Name', 'String', objectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/TotalAmount', 'grand_Total', 'Decimal', objectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/RecordIds/RecordId/Id', 'order_Number', 'String', objectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/PaymentTerms/UserArea/PaymentMethod/CCInfo/AccountType', 'credit_Card_Type', 'String', objectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/PaymentTerms/UserArea/PaymentMethod/CCInfo/ExpMonth', 'expiry_Month', 'Integer', objectMap.Id);
        
        //Insert second Object Map and associate to first
        Object_Map__c childObjectMap = createObjectMap('Line to MOrderLineItem', 'ShowSalesOrder/DataArea/SalesOrder/Line', 'lineItems', true, 'List', objectMap.Id);
        addObjectMapKey(childObjectMap, createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/OrderItem/Description', 'product', 'String', childObjectMap.Id));
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/UnitPrice/Amount@currency', 'sales_Currency', 'String', childObjectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/OrderQuantity', 'quantity', 'Integer', childObjectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/ShipDate', 'ship_date', 'Datetime', childObjectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/UnitPrice/Amount', 'sales_Price', 'Decimal', childObjectMap.Id);

        //Insert third Object Map and associate to second
        Object_Map__c grandchildObjectMap = createObjectMap('Part to MPart', 'ShowSalesOrder/DataArea/SalesOrder/Line/Part', 'parts', true, 'Map', childObjectMap.Id);
        addObjectMapKey(grandchildObjectMap, createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/Part/Name', 'name', 'String', grandchildObjectMap.Id));
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/Part/PartNumber', 'part_Number', 'Integer', grandchildObjectMap.Id);
        
        //Insert fourth Object Map and associate to first
        childObjectMap = createObjectMap('Address to MAddress', 'ShowSalesOrder/DataArea/SalesOrder/Address', 'addresses', true, 'Map', objectMap.Id);
        addObjectMapKey(childObjectMap, createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/Address/AddressType', 'address_Type', 'String', childObjectMap.Id));
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/Address/Street', 'street', 'String', childObjectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/Address/City', 'city', 'String', childObjectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/Address/County', 'county', 'String', childObjectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/Address/Country', 'country', 'String', childObjectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/Address/PostCode', 'postcode', 'String', childObjectMap.Id);

        Test.StartTest();
        GenericObject.OrderObject tt = new GenericObject.OrderObject();
        tt.primaryParty = new List<GenericObject.OrderObject>();
        //tt.lineItems = new List<GenericObject.OrderLine>();
        //System.debug('******jsonStr tt...' +JSON.serialize(tt));
      
        //Load Response into a Document object
        Dom.Document document = new Dom.Document();
        document.load(sampleXML);   
       
        String jsonStr = DocumentMappingUtilities.executeObjectMap(document, 'ShowSalesOrder to MOrder');
        System.debug('******jsonStr...' +jsonStr);
        GenericObject.OrderObject testOrder = (GenericObject.OrderObject)  JSON.deserialize(jsonStr, GenericObject.OrderObject.class);
        Test.StopTest();
    }
    
    static testMethod void testObjectMapUsingListStyleCollection() {
        
        //Insert Object Map
        Object_Map__c objectMap = createObjectMap('ShowSalesOrder to MOrder', 'ShowSalesOrder/DataArea/SalesOrder', 'MOrder', false, null, null);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/PaymentTerms/UserArea/PaymentMethod/CCInfo/ExpYear', 'expiry_Year', 'Integer', objectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/PaymentTerms/UserArea/PaymentMethod/CCInfo/AccountName', 'bill_To_Name', 'String', objectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/TotalAmount', 'grand_Total', 'Decimal', objectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/RecordIds/RecordId/Id', 'order_Number', 'String', objectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/PaymentTerms/UserArea/PaymentMethod/CCInfo/AccountType', 'credit_Card_Type', 'String', objectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/PaymentTerms/UserArea/PaymentMethod/CCInfo/ExpMonth', 'expiry_Month', 'Integer', objectMap.Id);
        
        //Insert second Object Map and associate to first
        Object_Map__c childObjectMap = createObjectMap('Line to MOrderLineItem', 'ShowSalesOrder/DataArea/SalesOrder/Line', 'listLineItems', false, 'List', objectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/OrderItem/Description', 'product', 'String', childObjectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/UnitPrice/Amount@currency', 'sales_Currency', 'String', childObjectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/OrderQuantity', 'quantity', 'Integer', childObjectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/ShipDate', 'ship_date', 'Datetime', childObjectMap.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Line/UnitPrice/Amount', 'sales_Price', 'Decimal', childObjectMap.Id);

        Test.StartTest();
        
        //Load Response into a Document object
        Dom.Document document = new Dom.Document();
        document.load(sampleXML);   
        
        String jsonStr = DocumentMappingUtilities.executeObjectMap(document, 'ShowSalesOrder to MOrder');
        //GenericObject.OrderObject testOrder = (GenericObject.OrderObject)JSON.deserialize(jsonStr, GenericObject.OrderObject.class);
        GenericObject.OrderObject testOrder = (GenericObject.OrderObject) JSON.deserialize(jsonStr, GenericObject.OrderObject.class);
        //System.Debug(JSON.serialize(testOrder)); 
              
        Test.StopTest();
    }
    
    static testMethod void testNoObjectMapFound() {

        Test.StartTest();
        
        //Load Response into a Document object
        Dom.Document document = new Dom.Document();
        document.load(sampleXML);  
        
        try {
            String jsonStr = DocumentMappingUtilities.executeObjectMap(document, 'Non-existent object map');
            GenericObject.OrderObject testOrder = (GenericObject.OrderObject)JSON.deserialize(jsonStr, GenericObject.OrderObject.class);
            System.Debug(JSON.serialize(testOrder));
        }
        catch (Exception e) {
            //Do Nothing
        }
              
        Test.StopTest();

    }
    
    static testMethod void testDuplicateObjectMap() {

        //Insert Object Map
        Object_Map__c objectMap1 = createObjectMap('ShowSalesOrder to MOrder', 'ShowSalesOrder/DataArea/SalesOrder', 'MOrder', false, null, null);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/PaymentTerms/UserArea/PaymentMethod/CCInfo/ExpYear', 'expiry_Year', 'Integer', objectMap1.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/PaymentTerms/UserArea/PaymentMethod/CCInfo/AccountName', 'bill_To_Name', 'String', objectMap1.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/TotalAmount', 'grand_Total', 'Decimal', objectMap1.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/RecordIds/RecordId/Id', 'order_Number', 'String', objectMap1.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/PaymentTerms/UserArea/PaymentMethod/CCInfo/AccountType', 'credit_Card_Type', 'String', objectMap1.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/PaymentTerms/UserArea/PaymentMethod/CCInfo/ExpMonth', 'expiry_Month', 'Integer', objectMap1.Id);

        //Insert Object Map
        Object_Map__c objectMap2 = createObjectMap('ShowSalesOrder to MOrder', 'ShowSalesOrder/DataArea/SalesOrder', 'MOrder', false, null, null);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/PaymentTerms/UserArea/PaymentMethod/CCInfo/ExpYear', 'expiry_Year', 'Integer', objectMap2.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/PaymentTerms/UserArea/PaymentMethod/CCInfo/AccountName', 'bill_To_Name', 'String', objectMap2.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/TotalAmount', 'grand_Total', 'Decimal', objectMap2.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/RecordIds/RecordId/Id', 'order_Number', 'String', objectMap2.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/PaymentTerms/UserArea/PaymentMethod/CCInfo/AccountType', 'credit_Card_Type', 'String', objectMap2.Id);
        createObjectAttributeMap('ShowSalesOrder/DataArea/SalesOrder/Header/PaymentTerms/UserArea/PaymentMethod/CCInfo/ExpMonth', 'expiry_Month', 'Integer', objectMap2.Id);

        Test.StartTest();
        
        //Load Response into a Document object
        Dom.Document document = new Dom.Document();
        document.load(sampleXML);   
        
        try {
            String jsonStr = DocumentMappingUtilities.executeObjectMap(document, 'ShowSalesOrder to MOrder');
            GenericObject.OrderObject testOrder = (GenericObject.OrderObject)JSON.deserialize(jsonStr, GenericObject.OrderObject.class);
            System.Debug(JSON.serialize(testOrder));
        }
        catch (Exception e) {
            //Do Nothing
        }
              
        Test.StopTest();

    }
    
    private static testMethod void testObjects() {
        
        Test.StartTest();
        
        GenericObject.OrderObject order = new GenericObject.OrderObject();
        order.attributes.put('bill_To_Name', 'Test Name'); 
        order.attributes.put('credit_Card_Type', 'Visa');
        order.attributes.put('expiry_Month',  '9');
        order.attributes.put('expiry_Year',  '2012');
        order.attributes.put('grand_Total',  '10.2');
        order.attributes.put('order_Number',  '12345');
        //order.attributes.put('bill_To_Name', order.bill_To_Name);
        
       /* GenericObject.OrderLine orderLineItem = new GenericObject.OrderLine();
        //orderLineItem.product = 'Test Product';
        orderLineItem.attributes.put('quantity', '1');
        //orderLineItem.sales_Currency = 'USD';
        orderLineItem.attributes.put('sales_Price', '10.2');
        orderLineItem.attributes.put('sales_Tax', '1.1');
        //orderLineItem.ship_date = Datetime.newInstance(2008, 12, 1, 12, 30, 2);
        //order.lineItems.put(orderLineItem.product, orderLineItem);
        */
        Test.StopTest();
    }
    
    private static Object_Map__c createObjectMap(String name, String source, String target, Boolean createAttributesMap, String targetCollectionType, Id parentObjectMapId) {
        
        Object_Map__c objectMap = new Object_Map__c(
            Name=name, 
            Source__c=source, 
            Target__c=target, 
            Create_Attributes_Map__c=createAttributesMap, 
            Target_Collection_Type__c=targetCollectionType, 
            Parent_Object_Map__c=parentObjectMapId);
            
        insert objectMap;
        
        return objectMap;
    } 
    
    private static Object_Attribute_Map__c createObjectAttributeMap(String source, String target, String targetDataType, Id parentObjectMapId) {
        DocumentMappingUtilities.getSampleDocument();
        Object_Attribute_Map__c objectAttributeMap = new Object_Attribute_Map__c(
            Source__c=source,
            Target__c=target,
            Target_Data_Type__c=targetDataType,
            Parent_Object_Map__c=parentObjectMapId);
        
        insert objectAttributeMap;
        
        return objectAttributeMap;
    }
    
    private static void addObjectMapKey(Object_Map__c objectMap, Object_Attribute_Map__c objectAttributeMap) {
        
        objectMap.Object_Map_Key__c = objectAttributeMap.Id;
        
        update objectMap;
    }
    
    private static void testException() {
    	   Test.startTest();
    	  //Load Response into a Document object
        Dom.Document document = new Dom.Document();
        document.load('');   
       
        String jsonStr = DocumentMappingUtilities.executeObjectMap(document, '');  
        Test.stopTest();  	
    	
    }
}