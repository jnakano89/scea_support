//Generated by wsdl2apex

public class Sony_MiddlewareConsumerdata3 {
    public class ErrorType {
        public String ErrorCode;
        public String ErrorMessage;
        public String Description;
        private String[] ErrorCode_type_info = new String[]{'ErrorCode','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] ErrorMessage_type_info = new String[]{'ErrorMessage','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] Description_type_info = new String[]{'Description','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/consumerdata/','true','false'};
        private String[] field_order_type_info = new String[]{'ErrorCode','ErrorMessage','Description'};
    }
    public class Contact_mdm {
        public String MDMRowId;
        public String BirthDate;
        public String PSNEmail;
        public String SiebelEmail;
        public String FirstName;
        public String PSNPhone;
        public String LastName;
        public String sex;
        public String PersonUId;
        public String PreferredLanguageCode;
        public String PSNSignInId;
        public String CRMPhone;
        public String OnlineId;
        public Sony_MiddlewareConsumerdata3.ListOfAddress_mdm ListOfAddress;
        private String[] MDMRowId_type_info = new String[]{'MDMRowId','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] BirthDate_type_info = new String[]{'BirthDate','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] PSNEmail_type_info = new String[]{'PSNEmail','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] SiebelEmail_type_info = new String[]{'SiebelEmail','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] FirstName_type_info = new String[]{'FirstName','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] PSNPhone_type_info = new String[]{'PSNPhone','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] LastName_type_info = new String[]{'LastName','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] sex_type_info = new String[]{'sex','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] PersonUId_type_info = new String[]{'PersonUId','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] PreferredLanguageCode_type_info = new String[]{'PreferredLanguageCode','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] PSNSignInId_type_info = new String[]{'PSNSignInId','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] CRMPhone_type_info = new String[]{'CRMPhone','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] OnlineId_type_info = new String[]{'OnlineId','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] ListOfAddress_type_info = new String[]{'ListOfAddress','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/consumerdata/','true','false'};
        private String[] field_order_type_info = new String[]{'MDMRowId','BirthDate','PSNEmail','SiebelEmail','FirstName','PSNPhone','LastName','sex','PersonUId','PreferredLanguageCode','PSNSignInId','CRMPhone','OnlineId','ListOfAddress'};
    }
    public class ListOfAccount_mdm {
        public Sony_MiddlewareConsumerdata3.Account_mdm[] Account;
        private String[] Account_type_info = new String[]{'Account','http://xmlns.sony.com/middleware/consumerdata/',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/consumerdata/','true','false'};
        private String[] field_order_type_info = new String[]{'Account'};
    }
    public class Account_mdm {
        public String MDMAccountRowId;
        public String CreatedDate;
        public String PSNAccountId;
        public String PSNMasterAccountId;
        public String AccountStatus;
        public String PSNHandle;
        public String AccountSuspendReason;
        public String AccountSuspendDate;
        public String AccountUnsuspendDate;
        public String PSPlusSubscriber;
        public String PSPlusStartDate;
        public String PSPlusStackedEndDate;
        public String PSPlusAutoRenewalFlag;
        public String PSPlusAccountCountry;
        public Sony_MiddlewareConsumerdata3.ListOfContact_mdm ListOfContact;
        private String[] MDMAccountRowId_type_info = new String[]{'MDMAccountRowId','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] CreatedDate_type_info = new String[]{'CreatedDate','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] PSNAccountId_type_info = new String[]{'PSNAccountId','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] PSNMasterAccountId_type_info = new String[]{'PSNMasterAccountId','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] AccountStatus_type_info = new String[]{'AccountStatus','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] PSNHandle_type_info = new String[]{'PSNHandle','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] AccountSuspendReason_type_info = new String[]{'AccountSuspendReason','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] AccountSuspendDate_type_info = new String[]{'AccountSuspendDate','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] AccountUnsuspendDate_type_info = new String[]{'AccountUnsuspendDate','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] PSPlusSubscriber_type_info = new String[]{'PSPlusSubscriber','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] PSPlusStartDate_type_info = new String[]{'PSPlusStartDate','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] PSPlusStackedEndDate_type_info = new String[]{'PSPlusStackedEndDate','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] PSPlusAutoRenewalFlag_type_info = new String[]{'PSPlusAutoRenewalFlag','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] PSPlusAccountCountry_type_info = new String[]{'PSPlusAccountCountry','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] ListOfContact_type_info = new String[]{'ListOfContact','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/consumerdata/','true','false'};
        private String[] field_order_type_info = new String[]{'MDMAccountRowId','CreatedDate','PSNAccountId','PSNMasterAccountId','AccountStatus','PSNHandle','AccountSuspendReason','AccountSuspendDate','AccountUnsuspendDate','PSPlusSubscriber','PSPlusStartDate','PSPlusStackedEndDate','PSPlusAutoRenewalFlag','PSPlusAccountCountry','ListOfContact'};
    }
    public class PSConsumerLookup_Output_element {
        public Sony_MiddlewareConsumerdata3.ListOfAccount_mdm ListOfAccount;
        public Sony_MiddlewareConsumerdata3.ErrorType ErrorMessage;
        private String[] ListOfAccount_type_info = new String[]{'ListOfAccount','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] ErrorMessage_type_info = new String[]{'ErrorMessage','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/consumerdata/','true','false'};
        private String[] field_order_type_info = new String[]{'ListOfAccount','ErrorMessage'};
    }
    public class ListOfContact_mdm {
        public Sony_MiddlewareConsumerdata3.Contact_mdm[] Contact;
        private String[] Contact_type_info = new String[]{'Contact','http://xmlns.sony.com/middleware/consumerdata/',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/consumerdata/','true','false'};
        private String[] field_order_type_info = new String[]{'Contact'};
    }
    public class ListOfContact_mdmTopElmt {
        public Sony_MiddlewareConsumerdata3.ListOfContact_mdm ListOfContact;
        private String[] ListOfContact_type_info = new String[]{'ListOfContact','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/consumerdata/','true','false'};
        private String[] field_order_type_info = new String[]{'ListOfContact'};
    }
    public class ListOfAddress_mdm {
        public Sony_MiddlewareConsumerdata3.Address_mdm[] Address;
        private String[] Address_type_info = new String[]{'Address','http://xmlns.sony.com/middleware/consumerdata/',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/consumerdata/','true','false'};
        private String[] field_order_type_info = new String[]{'Address'};
    }
    public class PSConsumerLookup_Input_element {
        public String Email;
        public String PhoneNo;
        public String Handle;
        public String OnlineId;
        public String Source;
        public String TextAttribute1;
        public String TextAttribute2;
        public String TextAttribute3;
        private String[] Email_type_info = new String[]{'Email','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] PhoneNo_type_info = new String[]{'PhoneNo','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] Handle_type_info = new String[]{'Handle','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] OnlineId_type_info = new String[]{'OnlineId','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] Source_type_info = new String[]{'Source','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] TextAttribute1_type_info = new String[]{'TextAttribute1','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] TextAttribute2_type_info = new String[]{'TextAttribute2','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] TextAttribute3_type_info = new String[]{'TextAttribute3','http://xmlns.sony.com/middleware/consumerdata/',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/consumerdata/','true','false'};
        private String[] field_order_type_info = new String[]{'Email','PhoneNo','Handle','OnlineId','Source','TextAttribute1','TextAttribute2','TextAttribute3'};
    }
    public class Address_mdm {
        public String AddressId;
        public String City;
        public String ContactAddressType;
        public String Country;
        public String PostalCode;
        public String State;
        public String StreetAddress;
        public String StreetAddress2;
        public String StreetAddress3;
        private String[] AddressId_type_info = new String[]{'AddressId','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] City_type_info = new String[]{'City','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] ContactAddressType_type_info = new String[]{'ContactAddressType','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] Country_type_info = new String[]{'Country','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] PostalCode_type_info = new String[]{'PostalCode','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] State_type_info = new String[]{'State','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] StreetAddress_type_info = new String[]{'StreetAddress','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] StreetAddress2_type_info = new String[]{'StreetAddress2','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] StreetAddress3_type_info = new String[]{'StreetAddress3','http://xmlns.sony.com/middleware/consumerdata/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlns.sony.com/middleware/consumerdata/','true','false'};
        private String[] field_order_type_info = new String[]{'AddressId','City','ContactAddressType','Country','PostalCode','State','StreetAddress','StreetAddress2','StreetAddress3'};
    }
    public class ConsumerLookupQSPort {
        OSB__c settings = OSB__c.getOrgDefaults();
        public String endpoint_x = settings <> null ? settings.URL_for_Consumer_Lookup__c : '' ;
        //public String endpoint_x = 'https://devosb.scea.com:443/SONYConsumerLookup/ProxyService/PSConsumerLookup';
        //public String endpoint_x = 'https://osb.scea.com:443/SONYConsumerRefresh/ProxyService/PSConsumerRefresh';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x =20000;
        private String[] ns_map_type_info = new String[]{'http://xmlns.sony.com/middleware/consumerdata/', 'Sony_MiddlewareConsumerdata3'};
        public Sony_MiddlewareConsumerdata3.PSConsumerLookup_Output_element PSConsumerLookup(String Email,String PhoneNo,String Handle,String OnlineId,String Source,String TextAttribute1,String TextAttribute2,String TextAttribute3) {
            Sony_MiddlewareConsumerdata3.PSConsumerLookup_Input_element request_x = new Sony_MiddlewareConsumerdata3.PSConsumerLookup_Input_element();
            request_x.Email = Email;
            request_x.PhoneNo = PhoneNo;
            request_x.Handle = Handle;
            request_x.OnlineId = OnlineId;
            request_x.Source = Source;
            request_x.TextAttribute1 = TextAttribute1;
            request_x.TextAttribute2 = TextAttribute2;
            request_x.TextAttribute3 = TextAttribute3;
            Sony_MiddlewareConsumerdata3.PSConsumerLookup_Output_element response_x;
            Map<String, Sony_MiddlewareConsumerdata3.PSConsumerLookup_Output_element> response_map_x = new Map<String, Sony_MiddlewareConsumerdata3.PSConsumerLookup_Output_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'document/http://xmlns.sony.com/middleware/consumerdata/:PSConsumerLookup',
              'http://xmlns.sony.com/middleware/consumerdata/',
              'PSConsumerLookup_Input',
              'http://xmlns.sony.com/middleware/consumerdata/',
              'PSConsumerLookup_Output',
              'Sony_MiddlewareConsumerdata3.PSConsumerLookup_Output_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}