/******************************************************************************
Class         : Siebel_CaseTrigger_Test
Description   : Test class for Siebel_CaseTrigger Trigger and its management class Siebel_CaseTrigger_AC
Developed by  : Rahul Mittal
Date          : March 25, 2013              
******************************************************************************/
@isTest
private class Siebel_CaseTrigger_Test {

    //Method to test case's Current_Case_Owner field on insert or update of case
    static testMethod void testCurrentOwnerOnCaseInsertUpdate() {
        //create test user record
		List<User> users = Siebel_TestUtility_AC.CreateUsers('System Administrator',2,null);
		for(User u : users){
			u.CompanyName = 'SCEA';
		}
		try{insert users;}catch(Exception e){System.assert(false,' Failed while inserting test users.  ' + e.getMessage() ); }
		List<Case> testCaseRecs;
		
		//Testing when user is current user
		/*
		System.runAs(users.get(0)){
	        Test.startTest();
	        //Create test case records
	        testCaseRecs = Siebel_TestUtility_AC.createCases(2, true);
	        List<Case> insertedCases = [Select Id, OwnerId, Current_Case_Owner__c From Case Where Id IN : testCaseRecs];
	        //system.assert(insertedCases.get(0).Current_Case_Owner__c == insertedCases.get(0).OwnerId, 'Failed while checking Current Case Owner');
	        //system.assert(insertedCases.get(1).Current_Case_Owner__c == insertedCases.get(1).OwnerId, 'Failed while checking Current Case Owner');
	        system.debug(insertedCases.get(1).Current_Case_Owner__c +'=='+ insertedCases.get(1).OwnerId+ 'Failed while checking Current Case Owner'); 
	        //update case owner id
	        testCaseRecs.get(1).OwnerId = users.get(1).Id;
	        update testCaseRecs;
	        insertedCases = [Select Id, OwnerId, Current_Case_Owner__c From Case Where Id IN : testCaseRecs];
	        system.assert(insertedCases.get(0).OwnerId == UserInfo.getUserId(), 'Failed while checking Case OwnerId');
	        system.assert(insertedCases.get(0).Current_Case_Owner__c == UserInfo.getUserId(), 'Failed while checking Current Case Owner');
	        system.assert(insertedCases.get(1).OwnerId == users.get(1).Id, 'Failed while checking Case OwnerId');
	        system.assert(insertedCases.get(1).Current_Case_Owner__c == users.get(1).Id, 'Failed while checking Current Case Owner');
			Test.stopTest();	
		}
		*/
    }
}