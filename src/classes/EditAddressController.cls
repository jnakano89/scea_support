/*******************************************************************************************************************************
 (c) 2013 Appirio, Inc.
    Description: Controller Class For VF Page NewAddress and EditAddress.
    Original August 07,2013: Poonam Varyani(JDC)
    August 17,2013 : KapiL Choudhary(JDC) : Update Code for new page NewAddress [T-173743]
 
 Updates:  
   04/21/2015 : Leena Mandadapu : SMS-835 : Retailer Address Block functionality.
*********************************************************************************************************************************/
public without sharing class EditAddressController {
        
        private ApexPages.StandardController stdController;
        
        public Address__c address {get; set;}

    public  string contactId{get;set;}
    public  string caseId{get;set;}
    public boolean isSuccess{get;set;}
        Set<String> validateZipCodeCountries = new Set<String>{'USA', 'Canada'};
        public EditAddressController (ApexPages.StandardController controller){
                
                System.debug('>>>>>>>>>EditAddressController.StandardController<<<<<<<<<' );

                caseId = Apexpages.currentPage().getParameters().get('caseId');
                contactId = Apexpages.currentPage().getParameters().get('contactId');

                System.debug('>>>>>>>>>contactId='+contactId);
                System.debug('>>>>>>>>>caseId='+caseId);

                address = (Address__c) controller.getRecord();

                System.debug('>>>>>>>>>address='+address);

           //the new address case will have no Id
           //LM 04/21/2015: Added Block_Address__c field to SOQL
           if(address.Id != null){ 
                
                        address = [Select Consumer__c, Address_Type__c, 
                               Address_Line_1__c, Address_Line_2__c, 
                               Address_Line_3__c, Country__c, Postal_Code__c, 
                               State__c, City__c, Name, Description__c,Status__c, Block_Address__c
                               from Address__c 
                               where Id=: address.Id];
           }else{
                
                        if(address.consumer__c==null && contactId!=null){
                                address.Consumer__c=contactId;
                        }
                
           }
        }

  public EditAddressController() {
        
        
        System.debug('>>>>>>>>>EditAddressController<<<<<<<<<' );
        System.debug('>>>>>>>>>contactId='+contactId);
        System.debug('>>>>>>>>>caseId='+caseId);
        caseId = Apexpages.currentPage().getParameters().get('caseId');
        contactId = Apexpages.currentPage().getParameters().get('contactId');
        
        address = new Address__c(Consumer__c=contactId);        
 
  }

        public boolean validateZipCode(){
                
          boolean valid = true;
          
          if(address <> null && validateZipCodeCountries.contains(address.Country__c)) {
                
                Integer zipCodeRecordCount = [select count() from Zip_Code__c where Zip_Code__c = :address.Postal_Code__c];
                
                if(zipCodeRecordCount == 0) {
                        if(!'Success'.equalsIgnoreCase(ValidationRulesUtility.isValidCanadaZipCode('Zip Code', address.Postal_Code__c))){
                                valid = false;
                        }                       
                }//end-if 
          }//end-if
                
          return valid; 
        }

        
        //method to search the sate/city from zip code object based on the 
        public void searchPostalCode(){
                
           String query = '';       
           if(address.Postal_Code__c != '' && address.Country__c != null) {
                
                   String countryStr = address.Country__c+'%';
                   String postalCode = address.Postal_Code__c.trim();
                   
               List<Zip_Code__c> zipCodeList = [Select State__c, City__c From Zip_Code__c Where Name =: postalCode AND Country__c like :countryStr limit 1];
                                                   
            if(zipCodeList.size() == 0 || zipCodeList == null){
                address.State__c = '';
                address.City__c = '';
            }else {
                address.State__c = zipCodeList.get(0).State__c;
                address.City__c = zipCodeList.get(0).City__c;
            }               
           }
        }
        

        //method to save
        public pageReference save(){
                
          isSuccess = true; 
          if(!validateZipCode()){
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a valid zip/postal code.'));
              isSuccess = false;
          }
                
          if(address <> null && address.City__c == null){
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'City is required.'));
              isSuccess = false;
          }

          if(address <> null && address.Address_Line_1__c == null){
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Address Line 1 is required.'));
              isSuccess = false;
          }
                
          if(!isSuccess){
              return null;
          }                               
                
        try{     
           //update the address field : 
            upsert address;
        }
        catch(exception e){         
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            isSuccess = false;
            return null;
        }

        /*PageReference returnPage;*/
             
        if(caseId != null && caseId != ''){             
            Case updateCase  = new Case(id=caseId, Bill_To__c = address.Id);        
            try{
                update updateCase;
            }
            catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                isSuccess = false;
                return null;
            }                                   
            /*returnPage = new ApexPages.StandardController(updateCase).view();*/         
         }/*else{
                        
                returnPage = new ApexPages.StandardController(new Contact(Id = address.Consumer__c)).view();
                }
                
                returnPage.setRedirect(true);

                return returnPage;*/
        return null;
    } 
    
        public pageReference redirectBack(){

                PageReference returnPage;
                
                if(caseId != null && caseId != ''){
                        returnPage = new ApexPages.StandardController(new case(id=caseId)).view();
                }else{
                returnPage = new ApexPages.StandardController(new Contact(Id = address.Consumer__c)).view();
                }
                returnPage.setRedirect(true);
                return returnPage;
    }    
        
}