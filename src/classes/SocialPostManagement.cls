/******************************************************************************************************************************************************************
CREATED BY: SALESFORCE PROFESSIONAL SERVICES TEAM

UPDATED BY:
           12/02/2014  : Leena Mandadapu : SMS-725 : Added code to setup Social_content__c field on Case - Updated code to set correct OwnerId and RecordTypeId
           02/25/2015  : Leena Mandadapu : SMS-813 : Added logic to handle null pointer exception when the Topic Profile Name = null.
           02/25/2015  : Leena Mandadapu : SMS-813 : Updated Test Twitter handle to @robotpod.
*********************************************************************************************************************************************************************/
public with sharing class SocialPostManagement {
    
    public static void beforeInsert(List<SocialPost> spList){
        Case newCase = new Case();
        List<User> userList = new List<User>();
        list<RecordType> rtList = new list<RecordType>();
        list<SocialPersona> sPersonaList = new list<SocialPersona>();
        list<Account> accList = new List<Account>();
        Boolean createCaseBool = false; 
        Boolean someTemp = false;
        //LM Added
        String RT_CASE_SOCIAL_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Social').getRecordTypeId();
	    Group OWNER_SOCIAL = [SELECT Id FROM Group WHERE Name = 'Social Support' AND Type = 'Queue' ][0];
	    
        for(SocialPost sp : spList){
            userList = [Select  name from User  where id = :sp.OwnerId  order by LastModifiedDate ASC Limit 1];
            rtList = [Select  id from RecordType  where sobjecttype = 'Case' and name = 'Social' order by LastModifiedDate ASC Limit 1];
            
             //LM 02-25-2015 : Set Topic Profile to Blank if it is null in the Social post raw data.
        system.debug('<<<<<<<<<<<<Social Post Topic Profile Name>>>>>>>>>>>>>>>'+sp.TopicProfileName); 
        if(sp.TopicProfileName == null)
             sp.TopicProfileName = '';	//else no need to update the value
             
            if(!userList.isEmpty()){
                if(userList.get(0).name.equalsIgnoreCase('SCS Integrator')){                                        
                    if(String.isNotBlank(sp.Handle)&&
                        String.isNotBlank(sp.MessageType)){
                        createCaseBool = true;
                    }
                    if(createCaseBool){
                        //LM 12/02/2014: commented below if loop and added new if condition to process test tweets same way as production @askplaystation tweets.
                    	//if(sp.TopicProfileName.equalsIgnoreCase('@AskPlaystation')){ 
                        if (sp.TopicProfileName.equalsIgnoreCase('@AskPlaystation') || sp.TopicProfileName.equalsIgnoreCase('@robotpod')) {	                      
                                    if(!sp.Handle.containsIgnoreCase('PlayStation')){
                                        if(!sp.MessageType.equalsIgnoreCase('retweet')){
                                            //LM 12/01/2014 : Updated to set correct ownerId, recordTypeId. Added code to setup Social_Content__c field on case
                                            newCase.RecordTypeId = RT_CASE_SOCIAL_ID;
                                            newCase.OwnerId = OWNER_SOCIAL.ID;
                                            newCase.Social_Content__c = sp.Content;
                                            newCase.Origin = 'Social Post';
                                            newCase.Social_Post_Tag__c=getCategoriesFromPostTag(sp.PostTags);
                                            //newCase.Status = 'Transfer to Chat';  
                                            //newCase.Subject = sp.PostTags;
                                            if(!rtList.isEmpty()){
                                                //newCase.RecordTypeId = rtList.get(0).id;                              
                                            }
                                            String refContactId;
                                            String refAccountId;
                                            if(String.isNotBlank(sp.PersonaId)){
                                                sPersonaList = [Select  id, parentId from SocialPersona  where id =:sp.PersonaId order by LastModifiedDate ASC Limit 1];                                                
                                                if(sPersonaList.size() > 0){
                                                    Boolean isContactType = checkParentId(sPersonaList.get(0).parentId);                                                    
                                                    if(isContactType != null){                                                      
                                                        if(isContactType){                                                          
                                                            refAccountId = checkAndCreateAccount(sPersonaList.get(0).parentId);                                                         
                                                            refContactId = checkAndCreateContact(refAccountId); 
                                                            //refContactId = sPersonaList.get(0).parentId;                                                                                                                                                                  
                                                        }else{                                                                                                                      
                                                            refContactId = checkAndCreateContact(sPersonaList.get(0).parentId);
                                                            refAccountId = checkAndCreateAccount(refContactId); 
                                                            //refAccountId = sPersonaList.get(0).parentId;                                              
                                                        }                                                   
                                                    }else{
                                                        //nothing available create both would not be a case
                                                    }                                                                                                   
                                                    if(String.isNotBlank(refAccountId)){                                                                                                                
                                                        newCase.AccountId = refAccountId;
                                                    }
                                                    if(String.isNotBlank(refContactId)){                                                                                                                
                                                        newCase.ContactId = refContactId;
                                                    }                                               
                                                }                                                                                       
                                            }
                                            try{
                                                insert newCase;                 
                                                sp.ParentId = newCase.id;                                               
                                                SocialPersona spUpdate = sPersonaList.get(0);
                                                spUpdate.ParentId = refContactId;
                                                update spUpdate;                                                
                                            }catch(Exception ex){
                                                System.debug('======ERROR Occured during insertSocialPostCase====' + ex.getMessage());              
                                            }                                                                                                                                                                                                       
                                        }
                                    }                                                   
                        }else{
                            Boolean createCaseRef = false;
                            if(String.isNotBlank(sp.PostTags)){
                                if(sp.PostTags.containsIgnoreCase('Help')||
                                    sp.PostTags.containsIgnoreCase('Complaint')||
                                    sp.PostTags.containsIgnoreCase('Question')||
                                    sp.PostTags.containsIgnoreCase('Compliment')){
                                    createCaseRef = true;
                                }
                            }
                            if(String.isNotBlank(sp.PostPriority)){
                                if(sp.PostPriority.equalsIgnoreCase('High')||
                                    sp.PostPriority.equalsIgnoreCase('Medium')){
                                    createCaseRef = true;
                                }
                            }
                            
                            if(createCaseRef){                                                                    
                                    if(!sp.Handle.containsIgnoreCase('PlayStation')){
                                        if(!sp.MessageType.equalsIgnoreCase('retweet')){
                                            //LM 12/01/2014 : Updated to set correct ownerId, recordTypeId. Added code to setup Social_Content__c field on case
                                            newCase.RecordTypeId = RT_CASE_SOCIAL_ID;
                                            newCase.OwnerId = OWNER_SOCIAL.ID;
                                            newCase.Social_Content__c = sp.Content;
                                            newCase.Origin = 'Social Post';
                                            newCase.Social_Post_Tag__c=getCategoriesFromPostTag(sp.PostTags);
                                            //newCase.Status = 'Transfer to Chat';  
                                            //newCase.Subject = sp.PostTags;
                                            if(!rtList.isEmpty()){
                                                //newCase.RecordTypeId = rtList.get(0).id;                              
                                            }
                                            String refContactId;
                                            String refAccountId;
                                            if(String.isNotBlank(sp.PersonaId)){
                                                sPersonaList = [Select  id, parentId from SocialPersona  where id =:sp.PersonaId order by LastModifiedDate ASC Limit 1];                                                
                                                if(sPersonaList.size() > 0){
                                                    Boolean isContactType = checkParentId(sPersonaList.get(0).parentId);                                                    
                                                    if(isContactType != null){                                                      
                                                        if(isContactType){                                                          
                                                            refAccountId = checkAndCreateAccount(sPersonaList.get(0).parentId);                                                         
                                                            refContactId = checkAndCreateContact(refAccountId);                 
                                                            //refContactId = sPersonaList.get(0).parentId;                                                                                                                                                  
                                                        }else{                                                                                                                      
                                                            refContactId = checkAndCreateContact(sPersonaList.get(0).parentId);
                                                            refAccountId = checkAndCreateAccount(refContactId);         
                                                            //refAccountId = sPersonaList.get(0).parentId;                                      
                                                        }                                                   
                                                    }else{
                                                        //nothing available create both would not be a case
                                                    }                                                                                                       
                                                    if(String.isNotBlank(refAccountId)){                                                                                                        
                                                        newCase.AccountId = refAccountId;
                                                    }
                                                    if(String.isNotBlank(refContactId)){                                                                                                            
                                                        newCase.ContactId = refContactId;
                                                    }                                               
                                                }                                                                                       
                                            }
                                            try{
                                                insert newCase;                                                                                                                     
                                                sp.ParentId = newCase.id;
                                                SocialPersona spUpdate = sPersonaList.get(0);
                                                spUpdate.ParentId = refContactId;
                                                update spUpdate;                                                
                                            }catch(Exception ex){
                                                System.debug('======ERROR Occured during insertSocialPostCase====' + ex.getMessage());              
                                            }                                                                                                                                                                                                       
                                        }
                                    }
                                //}
                            }
                            
                        }                                               
                    }
                }               
            }                       
        }                                   
    }
    
    public static String getCategoriesFromPostTag(String postTags){
        if(String.isNotBlank(postTags)){
            List<String> ptList = postTags.split(',');
            String ptFinal = '';
            if(ptList.size()>0){
                for (String str : ptList){
                    ptFinal= ptFinal+str+' ';            
                }           
            }           
            system.debug('<<<<<<Post Tags to return>>>>>>>>'+ptFinal);
            //LM 02/25/2015 : Added below logic to fix the Social_post_tag__c field length issue on case. Max field length for social_post_tag__c on case is 255 chars
            ptFinal = ptFinal != null ? (ptFinal.length() < 255 ? ptFinal : ptFinal.substring(0,254)) : null;  
            return ptFinal;         
        }   
        return null;    
    }
        
    public static Boolean checkParentId(String parentIdRef){       
        Boolean isContact = false;
        Integer accList1Count = [Select count() from Account where Id =: parentIdRef ];
        Integer conList1Count = [Select count() from Contact where Id =: parentIdRef ];
        if(accList1Count > 0){          
            isContact = false;
            return  isContact;      
        }
        if(conList1Count > 0){          
            isContact = true;
            return  isContact;
        }
        if(conList1Count == 0 && accList1Count == 0){           
            System.debug('There was no Contact or Account on the Social Post, Major error.');                   
        }                       
        return null;
    }
    
    public static String checkAndCreateAccount(String parentIdRef1){       
        Integer conList1Count = [Select count() from Contact where Id =: parentIdRef1 ];
        Account acc = new Account(); 
        list<Contact> conList1 = new List<Contact>();
        String personAccountRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        if(conList1Count > 0){          
            conList1 = [Select id, FirstName, LastName, AccountId from Contact where Id =: parentIdRef1 order by LastModifiedDate ASC Limit 1];
            Contact con1 = new Contact();
            if(String.isNotBlank(conList1.get(0).AccountId)){               
                return conList1.get(0).AccountId;
            }else{              
                acc.FirstName = conList1.get(0).FirstName;
                acc.LastName = conList1.get(0).LastName;
                acc.RecordTypeId = personAccountRecId;              
                con1 = conList1.get(0);
                try{                    
                    insert acc;  
                    con1.AccountId = acc.Id;                    
                    //update conList1.get(0);
                    //update con1;                                  
                    return acc.Id;                                                  
                }catch(Exception ex){
                    System.debug('======ERROR Occured during insertSocialPostCase in Account Creation====' + ex.getMessage());              
                }                   
            }           
        }
        return null;    
    }   
    
    public static String checkAndCreateContact(String parentIdRef2){       
        Integer accList1Count = [Select count() from Account where Id =: parentIdRef2 ];
        Contact con = new Contact(); 
        list<Account> accList1 = new List<Account>();       
        if(accList1Count > 0){
            accList1 = [Select id, FirstName, LastName, PersonContactId from Account where Id =: parentIdRef2 order by LastModifiedDate ASC Limit 1];
            if(String.isNotBlank(accList1.get(0).PersonContactId)){
                return accList1.get(0).PersonContactId;
            }else{
                con.FirstName = accList1.get(0).FirstName;
                con.LastName = accList1.get(0).LastName;
                con.AccountId = parentIdRef2;
                try{
                    insert con;                 
                    return con.Id;                                                  
                }catch(Exception ex){
                    System.debug('======ERROR Occured during insertSocialPostCase in Contact Creation====' + ex.getMessage());              
                }                   
            }           
        }
        return null;                
    }  
      
}