public with sharing class EmailMessageManagement {

	public static string PLAYSTATION_SONY_COM = 'playstation.sony.com';
	
    public static void beforeInsert(List<EmailMessage> emailMessageList){
	
		validate(emailMessageList);	
		   
    } 
 

    public static void validate(List<EmailMessage> emailMessageList){
    	
    	
    	for(EmailMessage email : emailMessageList){
    		
    		if(email.Incoming == false 
    			&& email.FromAddress != null
    			&& email.FromAddress.containsIgnoreCase(PLAYSTATION_SONY_COM) == false){
    		
    			email.FromAddress.addError(label.Invalid_From_Email);
    					
			}//end-if
    		
    		
    	}//end-for	
    	 
    }

}