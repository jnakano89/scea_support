/**
  CREATED BY:
  10/28/2014 Leena Mandadapu : Jira# SMS-654 - WebPPP Revamp project
  
  UPDATED BY:
  
 */
@isTest(SeeAllData=true)
private class WebPPPOrdersService_Test {
    
    static Account acc;
    static Address__c addr;
    static Order__c ord;
    static Asset__c ast;
    static Consumer_Asset__c conast;
    static Order_Line__c ordline;

    static testMethod void myPositiveUnitTest() {
        Test.startTest();
        createData();
        //Valid New Consumer request
        WebPPPOrdersService.WebServiceResponse response = WebPPPOrdersService.WebPPPOrderProcess(getValidRequest_NewConsumer());
        //valid Existing Consumer request
        WebPPPOrdersService.WebServiceResponse response1 = WebPPPOrdersService.WebPPPOrderProcess(getValidRequest_ExistingConsumer());
        
        //Update Request
        WebPPPOrdersService.WebServiceResponse response2 = WebPPPOrdersService.WebPPPOrderUpdateProcess(getValidRequest_ToUpdate());
        Test.stopTest();
    }
    
    static testMethod void myNegativeUnitTest() {
        Test.startTest();
        createData();
        //Blank Purchase request
        WebPPPOrdersService.WebServiceResponse response = WebPPPOrdersService.WebPPPOrderProcess(getinValidRequest_Blank());
        //Purchase Request with one of the required values missing
        WebPPPOrdersService.WebServiceResponse response1 = WebPPPOrdersService.WebPPPOrderProcess(getinValidRequest_missingREQValues());
        //Purchase Request with Invalid Adress
        WebPPPOrdersService.WebServiceResponse response11 = WebPPPOrdersService.WebPPPOrderProcess(getinValidRequest_inValidAddressValues());
        //Purchase Request with invalid Asset info
        WebPPPOrdersService.WebServiceResponse response12 = WebPPPOrdersService.WebPPPOrderProcess(getinValidRequest_inValidAssetValues());
        //Purchase Request with Invalid Order info
        WebPPPOrdersService.WebServiceResponse response13 = WebPPPOrdersService.WebPPPOrderProcess(getinValidRequest_inValidOrderValues());
        
        //Update Requests
        //Update process - Missing Order Number
        WebPPPOrdersService.WebServiceResponse response2 = WebPPPOrdersService.WebPPPOrderUpdateProcess(getinValidRequest_ToUpdate_MissingOrdNum());
        //Update process - Order number doesn't exist in the system
        WebPPPOrdersService.WebServiceResponse response3 = WebPPPOrdersService.WebPPPOrderUpdateProcess(getinValidRequest_ToUpdate_OrdNumNotExist());
        //Update process - Blank request
        WebPPPOrdersService.WebServiceResponse response4 = WebPPPOrdersService.WebPPPOrderUpdateProcess(getinValidRequest_ToUpdate_Blank());
        Test.stopTest();
    }
    
    //Valid Single Pay Purchase New Consumer request
    static WebPPPOrdersService.WebServiceInput getValidRequest_NewConsumer(){
        WebPPPOrdersService.WebServiceInput request = new WebPPPOrdersService.WebServiceInput();
        request.OrderNumber = 'PYB-11223344';
        request.FirstName = 'TestFN';
        request.LastName = 'TestLN';
        request.Email = 'Test@test.com';
        request.Phonenum = '1234567890';
        request.ShippingAddress1 = '123 Main St';
        request.ShippingAddress2 = 'test';
        request.ShippingCity = 'Fremont';
        request.ShippingState = 'CA';
        request.ShippingZipCode = '94538';
        request.ShippingCountry = 'USA';
        request.ModelType = 'PS4';
        request.PPPSKU = 'VITA36MADM';
        request.ProofOfPurchaseDate = '09/15/2014';
        request.PPPPurchaseDate = '09/20/2014';
        request.PaymentType  = 'SinglePay';
        request.ContractNumber  = '123AXYZ123';
        request.ContractStartDt  = '09/20/2014';
        request.ContractEndDt  = '09/15/2017';
        request.BaseAmount = '59.99';
        request.TaxAmount = '0.23';
        request.DealerId  = 'SCEAP';
        request.PPPPlanType = 'AD';
        request.BillingAddress1 = '123 Main St';
        request.BillingAddress2 = 'test';
        request.BillingCity = 'Fremont';
        request.BillingState = 'CA';
        request.BillingZipCode = '94538';
        request.BillingCountry = 'USA';     
        
        return request;
    }  
    
    //Valid Single Pay Purchase Existing Consumer request
    static WebPPPOrdersService.WebServiceInput getValidRequest_ExistingConsumer(){
        WebPPPOrdersService.WebServiceInput request = new WebPPPOrdersService.WebServiceInput();
        request.OrderNumber = 'PYB-11223355';
        request.FirstName = acc.FirstName;
        request.LastName = acc.LastName;
        request.Email = acc.PersonEmail;
        request.Phonenum = acc.Phone;
        request.ShippingAddress1 = addr.Address_Line_1__c;
        request.ShippingAddress2 = addr.Address_Line_2__c;
        request.ShippingCity = addr.City__c;
        request.ShippingState = addr.State__c;
        request.ShippingZipCode = addr.Postal_Code__c;
        request.ShippingCountry = addr.Country__c;
        request.ModelType = 'PS4';
        request.PPPSKU = 'VITA36MADM';
        request.ProofOfPurchaseDate = '09/15/2014';
        request.PPPPurchaseDate = '09/20/2014';
        request.PaymentType  = 'SinglePay';
        request.ContractNumber  = '123AXYZ124';
        request.ContractStartDt  = '09/20/2014';
        request.ContractEndDt  = '09/15/2017';
        request.BaseAmount = '59.99';
        request.TaxAmount = '0.23';
        request.DealerId  = 'SCEAP';
        request.PPPPlanType = 'AD';
        request.BillingAddress1 = addr.Address_Line_1__c;
        request.BillingAddress2 = addr.Address_Line_2__c;
        request.BillingCity = addr.City__c;
        request.BillingState = addr.State__c;
        request.BillingZipCode = addr.Postal_Code__c;
        request.BillingCountry = addr.Country__c;       
        
        return request;
    } 
    
     //InValid Single Pay - Blank request
    static WebPPPOrdersService.WebServiceInput getinValidRequest_Blank(){
        WebPPPOrdersService.WebServiceInput request = new WebPPPOrdersService.WebServiceInput();
        request = null;
        
        return request;
    } 
    
    //InValid Single Pay - Missing Required values request
    static WebPPPOrdersService.WebServiceInput getinValidRequest_missingREQValues(){
        WebPPPOrdersService.WebServiceInput request = new WebPPPOrdersService.WebServiceInput();
        request.OrderNumber = ''; //Order required
        request.FirstName = ''; //FN required
        request.LastName = ''; //LN required
        request.Email = ''; //Email required
        request.Phonenum = acc.Phone;
        request.ShippingAddress1 = addr.Address_Line_1__c;
        request.ShippingAddress2 = addr.Address_Line_2__c;
        request.ShippingCity = addr.City__c;
        request.ShippingState = addr.State__c;
        request.ShippingZipCode = addr.Postal_Code__c;
        request.ShippingCountry = addr.Country__c;
        request.ModelType = 'PS4';
        request.PPPSKU = 'VITA36MADM';
        request.ProofOfPurchaseDate = '09/15/2014';
        request.PPPPurchaseDate = '09/20/2014';
        request.PaymentType  = 'SinglePay';
        request.ContractNumber  = '123AXYZ124';
        request.ContractStartDt  = '09/20/2014';
        request.ContractEndDt  = '09/15/2017';
        request.BaseAmount = '59.99';
        request.TaxAmount = '0.23';
        request.DealerId  = 'SCEAP';
        request.PPPPlanType = 'AD';
        request.BillingAddress1 = addr.Address_Line_1__c;
        request.BillingAddress2 = addr.Address_Line_2__c;
        request.BillingCity = addr.City__c;
        request.BillingState = addr.State__c;
        request.BillingZipCode = addr.Postal_Code__c;
        request.BillingCountry = addr.Country__c;       
        
        return request;
    }
    
    //InValid Single Pay - Invalid Address
    static WebPPPOrdersService.WebServiceInput getinValidRequest_inValidAddressValues(){
        WebPPPOrdersService.WebServiceInput request = new WebPPPOrdersService.WebServiceInput();
        request.OrderNumber = 'PYB-1122331122'; //Order required
        request.FirstName = '123'; //FN required
        request.LastName = '456'; //LN required
        request.Email = 'test@yahoo.com'; //Email required
        request.Phonenum = '1231231234';
        request.ShippingAddress1 = 'Test St';
        request.ShippingAddress2 = '';
        request.ShippingCity = 'Test';
        request.ShippingState = 'Test';
        request.ShippingZipCode = 'Test';
        request.ShippingCountry = 'Test';
        request.ModelType = 'PS4NA';
        request.PPPSKU = 'VITATEST';
        request.ProofOfPurchaseDate = '09/15/2014';
        request.PPPPurchaseDate = '09/20/2014';
        request.PaymentType  = 'SinglePay';
        request.ContractNumber  = '123AXYZ124';
        request.ContractStartDt  = '09/20/2014';
        request.ContractEndDt  = '09/15/2017';
        request.BaseAmount = '59.99';
        request.TaxAmount = '0.23';
        request.DealerId  = 'SCEAP';
        request.PPPPlanType = 'AD';
        request.BillingAddress1 = 'Test St';
        request.BillingAddress2 = '';
        request.BillingCity = 'Test';
        request.BillingState = 'test';
        request.BillingZipCode = 'Test';
        request.BillingCountry = 'test';        
        
        return request;
    } 
    
    //InValid Single Pay - Invalid Asset data
    static WebPPPOrdersService.WebServiceInput getinValidRequest_inValidAssetValues(){
        WebPPPOrdersService.WebServiceInput request = new WebPPPOrdersService.WebServiceInput();
        request.OrderNumber = 'PYB-1122332122'; //Order required
        request.FirstName = '123'; //FN required
        request.LastName = '456'; //LN required
        request.Email = 'test@yahoo.com'; //Email required
        request.Phonenum = '1231231234';
        request.ShippingAddress1 = 'Test St';
        request.ShippingAddress2 = '';
        request.ShippingCity = 'Fremont';
        request.ShippingState = 'CA';
        request.ShippingZipCode = '94538';
        request.ShippingCountry = 'USA';
        request.ModelType = 'PS4NA';
        request.PPPSKU = 'VITATEST';
        request.ProofOfPurchaseDate = '09/15/2014';
        request.PPPPurchaseDate = '09/20/2014';
        request.PaymentType  = 'SinglePay';
        request.ContractNumber  = '123AXYZ124';
        request.ContractStartDt  = '09/20/2014';
        request.ContractEndDt  = '09/15/2017';
        request.BaseAmount = '59.99';
        request.TaxAmount = '0.23';
        request.DealerId  = 'SCEAP';
        request.PPPPlanType = 'AD';
        request.BillingAddress1 = 'Test St';
        request.BillingAddress2 = '';
        request.BillingCity = 'Fremont';
        request.BillingState = 'CA';
        request.BillingZipCode = '94538';
        request.BillingCountry = 'USA';     
        
        return request;
    }
    
    //InValid Single Pay - Invalid Order data
    static WebPPPOrdersService.WebServiceInput getinValidRequest_inValidOrderValues(){
        WebPPPOrdersService.WebServiceInput request = new WebPPPOrdersService.WebServiceInput();
        request.OrderNumber = 'PYB-11222122'; //Order required
        request.FirstName = 'Test'; //FN required
        request.LastName = 'Test'; //LN required
        request.Email = 'test@yahoo.com'; //Email required
        request.Phonenum = '1231231234';
        request.ShippingAddress1 = 'Test St';
        request.ShippingAddress2 = '';
        request.ShippingCity = 'Fremont';
        request.ShippingState = 'CA';
        request.ShippingZipCode = '94538';
        request.ShippingCountry = 'USA';
        request.ModelType = 'PS4';
        request.PPPSKU = 'VITA36MADM';
        request.ProofOfPurchaseDate = '09/15/2014';
        request.PPPPurchaseDate = '09/20/2014';
        request.PaymentType  = 'Single';
        request.ContractNumber  = '123AXYZ124';
        request.ContractStartDt  = '09/20/2014';
        request.ContractEndDt  = '09/15/2017';
        request.BaseAmount = 'Test';
        request.TaxAmount = '0.23';
        request.DealerId  = 'SCEAP';
        request.PPPPlanType = 'AD';
        request.BillingAddress1 = 'Test St';
        request.BillingAddress2 = '';
        request.BillingCity = 'Fremont';
        request.BillingState = 'CA';
        request.BillingZipCode = '94538';
        request.BillingCountry = 'USA';     
        
        return request;
    }      
    
    //Valid Update process request
    static WebPPPOrdersService.WebServiceInput getValidRequest_ToUpdate(){
        WebPPPOrdersService.WebServiceInput request = new WebPPPOrdersService.WebServiceInput();
        request.OrderNumber = 'PYB-11223344';
        request.FirstName = '';
        request.LastName = '';
        request.Email = 'Test@test1.com';
        request.Phonenum = '1234567894';
        request.ShippingAddress1 = '123 Main';
        request.ShippingAddress2 = 'test';
        request.ShippingCity = 'Fremont';
        request.ShippingState = 'CA';
        request.ShippingZipCode = '94538';
        request.ShippingCountry = 'USA';
        request.ModelType = '';
        request.PPPSKU = '';
        request.ProofOfPurchaseDate = '';
        request.PPPPurchaseDate = '';
        request.PaymentType  = '';
        request.ContractNumber  = '';
        request.ContractStartDt  = '';
        request.ContractEndDt  = '';
        request.BaseAmount = '';
        request.TaxAmount = '';
        request.DealerId  = '';
        request.PPPPlanType = '';
        request.BillingAddress1 = '123 Main';
        request.BillingAddress2 = 'test';
        request.BillingCity = 'Fremont';
        request.BillingState = 'CA';
        request.BillingZipCode = '94538';
        request.BillingCountry = 'USA'; 
        request.SerialNumber = 'MB112212121';
        request.ModelNumber = 'CUH-1001A';
        request.BillingStatus = 'Suspended';
        request.POPattachmentURL = 'Test.com';
        
        return request;
    }  
    
    //invalid Update process request - Order Number not exist in the system
    static WebPPPOrdersService.WebServiceInput getinValidRequest_ToUpdate_OrdNumNotExist(){
        WebPPPOrdersService.WebServiceInput request = new WebPPPOrdersService.WebServiceInput();
        request.OrderNumber = 'PYB-1231231213';
        request.FirstName = '';
        request.LastName = '';
        request.Email = 'Test@test1.com';
        request.Phonenum = '1234567894';
        request.ShippingAddress1 = '123 Main';
        request.ShippingAddress2 = 'test';
        request.ShippingCity = 'Fremont';
        request.ShippingState = 'CA';
        request.ShippingZipCode = '94538';
        request.ShippingCountry = 'USA';
        request.ModelType = '';
        request.PPPSKU = '';
        request.ProofOfPurchaseDate = '';
        request.PPPPurchaseDate = '';
        request.PaymentType  = '';
        request.ContractNumber  = '';
        request.ContractStartDt  = '';
        request.ContractEndDt  = '';
        request.BaseAmount = '';
        request.TaxAmount = '';
        request.DealerId  = '';
        request.PPPPlanType = '';
        request.BillingAddress1 = '123 Main';
        request.BillingAddress2 = 'test';
        request.BillingCity = 'Fremont';
        request.BillingState = 'CA';
        request.BillingZipCode = '94538';
        request.BillingCountry = 'USA'; 
        request.SerialNumber = 'MB112212121';
        request.ModelNumber = 'CUH-1001A';
        request.BillingStatus = 'Suspended';
        request.POPattachmentURL = 'Test.com';
        
        return request;
    }  
    //invalid Update process request - Blank Order Number
    static WebPPPOrdersService.WebServiceInput getinValidRequest_ToUpdate_MissingOrdNum(){
        WebPPPOrdersService.WebServiceInput request = new WebPPPOrdersService.WebServiceInput();
        request.OrderNumber = '';
        request.FirstName = '';
        request.LastName = '';
        request.Email = 'Test@test1.com';
        request.Phonenum = '1234567894';
        request.ShippingAddress1 = '123 Main';
        request.ShippingAddress2 = 'test';
        request.ShippingCity = 'Fremont';
        request.ShippingState = 'CA';
        request.ShippingZipCode = '94538';
        request.ShippingCountry = 'USA';
        request.ModelType = '';
        request.PPPSKU = '';
        request.ProofOfPurchaseDate = '';
        request.PPPPurchaseDate = '';
        request.PaymentType  = '';
        request.ContractNumber  = '';
        request.ContractStartDt  = '';
        request.ContractEndDt  = '';
        request.BaseAmount = '';
        request.TaxAmount = '';
        request.DealerId  = '';
        request.PPPPlanType = '';
        request.BillingAddress1 = '123 Main';
        request.BillingAddress2 = 'test';
        request.BillingCity = 'Fremont';
        request.BillingState = 'CA';
        request.BillingZipCode = '94538';
        request.BillingCountry = 'USA'; 
        request.SerialNumber = 'MB112212121';
        request.ModelNumber = 'CUH-1001A';
        request.BillingStatus = 'Suspended';
        request.POPattachmentURL = 'Test.com';
        
        return request;
    }  
    
    //Invalid Update process request - blank request
    static WebPPPOrdersService.WebServiceInput getinValidRequest_ToUpdate_Blank(){
        WebPPPOrdersService.WebServiceInput request = new WebPPPOrdersService.WebServiceInput();
        request = null;
        
        return request;
    }  
    
    //Creating Sample Data
    static void createData() {
       acc = TestClassUtility.createPersonAccount(1, true)[0];
       addr = TestClassUtility.createAddress(1, true)[0];       
       
    }    
           
}