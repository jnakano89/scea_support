//7/17/2013 : UV : created this class
//8/12/2013 : UV : udated this class for account.

// Updated :
//			August 19,2013 : KapiL Choudhary(JDC) : Update Code to Add Status__c and Description fields in Address__c queries [T-173743].
public class SelectAddressController {
	
  public list<Address__c> addressList{get;set;}
  
  public string orderId;
  public string contactId;
  public string caseId;
  public string title{get;set;}
  public string subTitle{get;set;}
  public String selectedAddressId{get;set;}
  public Order__c selectedOrder;
  public Contact selectedContact;
  public Case selectedCase;
  public String fieldToUpdate; 
    
  boolean isBillTo;
  boolean isShipTo;
  boolean isOrder;
  boolean isContact;
  boolean isCase;
  
  
  final string BILL_TO = 'BillTo';
  final string SHIP_TO = 'ShipTo';
  final string BILLING  = 'Billing';
  final string SHIPPING  = 'Shipping';
 
       
  public SelectAddressController() {
  	
  	addressList = new list<Address__c>();
  	isBillTo = false;
  	isShipTo = false;
  	isOrder = false;
  	isContact = false;
  	isCase = false;
  	
  	orderId = Apexpages.currentPage().getParameters().get('orderId');
  	contactId = Apexpages.currentPage().getParameters().get('contactId');
  	caseId = Apexpages.currentPage().getParameters().get('caseId');
  	
  	fieldToUpdate = Apexpages.currentPage().getParameters().get('Field');

 
  	
  	if(fieldToUpdate == BILL_TO) {
  	  isBillTo = true;
  	  subTitle = 'Select Bill To Address';	
  	} else if(fieldToUpdate == SHIP_TO) {
	  isShipTo = true;
	  subTitle = 'Select Ship To Address';
  	}

  	if(orderId <> null && orderId <> '') {
  	  isOrder = true;
  	} else if(contactId <> null && contactId <> '') {
	  isContact = true;
  	} else if(caseId <> null && caseId <> '') {
	  isCase = true;
  	}

  	
  	addressList = getAddressDetails();
  	
  }
  
  private list<Address__c> getAddressDetails() {
  	
  	list<Address__c> addressList = new list<Address__c>();
  	
  	if(isOrder) {
  	  
  	  selectedOrder = [select Id, Consumer__c, Bill_To__c, Ship_To__c, Consumer__r.Name 
  			  from Order__c
  			  where Id = :orderId limit 1];
  			  
  	  title = selectedOrder.Consumer__r.Name;
  	  
  	  if(isBillTo) {
  	  	selectedAddressId = selectedOrder.Bill_To__c;
  	  } else if(isShipTo) {
  	  	selectedAddressId = selectedOrder.Ship_To__c;
  	  }		  
  	  
  	} else if(isContact){
  		
  		selectedContact = [select Id, Bill_To__c, Ship_To__c, Name
  				from Contact 
  				where Id = :contactId or AccountId =:contactId limit 1];
  	   
  	   	contactId=selectedContact.Id;
  	   	
  	  	title = selectedContact.Name;
  	  
  		  if(isBillTo) {
	  	    selectedAddressId = selectedContact.Bill_To__c;
	  	  } else if(isShipTo) {
	  	    selectedAddressId = selectedContact.Ship_To__c;
	  	  }
  	  
  	} else if(isCase){
  		
  		selectedCase = [select Id,CaseNumber,AccountId, Bill_To__c, Ship_To__c  
  							from Case 
  							where Id = :caseId limit 1];
  							
  	    list<Contact> contactList = [select id from Contact where AccountId =:selectedCase.AccountId limit 1];
  	    
  	    if(!contactList.isEmpty()){
  	    	selectedContact = contactList[0];
  	    	contactId=selectedContact.Id;
  	    }
  	   	
  	   	
  	  	title = selectedCase.CaseNumber;
  	  
  		  if(isBillTo) {
	  	    selectedAddressId = selectedCase.Bill_To__c;
	  	  } else if(isShipTo) {
	  	    selectedAddressId = selectedCase.Ship_To__c;
	  	  }
  	  
  	}
  	
  	if(selectedOrder <> null) {
  		
  	  for(Address__c address :[select Id, City__c, Address_Line_1__c, Address_Line_2__c, Address_Line_3__c,
  	  							State__c, Postal_Code__c, Country__c, Address_Type__c, Description__c
  	  							from  Address__c
  	  							where  Status__c != 'Inactive' and Consumer__c = :selectedOrder.Consumer__c ]) {
  	    addressList.add(address);								
  	  }
  	} else if(selectedContact <> null ){
  		
  	  for(Address__c address :[select Id, City__c, Address_Line_1__c, Address_Line_2__c, Address_Line_3__c, 
  	  							State__c, Postal_Code__c, Country__c, Address_Type__c, Description__c
  	  							from  Address__c
  	  							where Status__c != 'Inactive' and Consumer__c = :selectedContact.Id ]) {
  	    addressList.add(address);								
  	  }
  	  
  	}
  	
  	return addressList; 
  } 
  
  
   
  public Pagereference returnToRecord() {
  	
	Pagereference returnPage = null;

  	if(isOrder) {
		returnPage = new ApexPages.StandardController(selectedOrder).view();
 
  	} 
  	else if(isContact){
 		returnPage = new ApexPages.StandardController(selectedContact).view();
 
  	} 
  	else if(isCase){
  		
 		returnPage = new ApexPages.StandardController(selectedCase).view();
		
  	} 
  	
	returnPage.setRedirect(true);
	return returnPage;
  }
            
 
  
  public Pagereference selectAddress() {
  
  	String addressId = ApexPages.currentPage().getParameters().get('ctRadio');
  	
  	if(addressId == null) {
  		
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select an address'));
      return null;
    }
    
    return updateObject(addressId);
  }
  
  public Pagereference clearAddress() {
  	
  	return updateObject(null);
  
  }
  
  private Pagereference updateObject(String addressId) {
  	
  	if(isOrder) {
  		
  	  if(isShipTo) {
  	    selectedOrder.Ship_To__c = addressId;
 	  } else if(isBillTo) {
 	    selectedOrder.Bill_To__c = addressId;
      }
      try{
	    update selectedOrder;
      } catch(Exception ex) {
    	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, ex.getMessage()));
    	return null;
      }
  	  
    } 
    else if(isContact) {
    	
  	  if(isShipTo) {
  	    selectedContact.Ship_To__c = addressId;
 	  } else if(isBillTo) {
 	    selectedContact.Bill_To__c = addressId;
      }
      try{
	    update selectedContact;
      } catch(Exception ex) {
    	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, ex.getMessage()));
    	return null;
      }
    }
    else if(isCase) {
    	
  	  if(isShipTo) {
  	    selectedCase.Ship_To__c = addressId;
 	  } else if(isBillTo) {
 	    selectedCase.Bill_To__c = addressId;
      }
      try{
	    update selectedCase;
      } catch(Exception ex) {
    	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, ex.getMessage()));
    	return null;
      }
    }
  return returnToRecord();
}

   public Pagereference createAddress() {

		System.debug('>>>>>>>>>>>SelectAddressController.createAddress<<<<<<<<<<<');   	
		System.debug('>>>>>>>caseId='+caseId);	
		System.debug('>>>>>>>contactId='+contactId);

	

   	if(caseId != null && contactId!=null){

		Pagereference NewAddressPage = new PageReference('/apex/NewAddress?contactId='+contactId+'&caseId='+caseId);
		return NewAddressPage;
 
   	}else if(contactId!=null){

		Pagereference NewAddressPage = new PageReference('/apex/NewAddress?contactId='+contactId);
		return NewAddressPage;   		
   		
   	} 

	return null;
   }
    
}