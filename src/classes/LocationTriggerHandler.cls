///******************************************************************************
//Class         : LocationTriggerHandler
//Description   : This is handler class for Location Trigger
//Created Date  : 11/26/2013
//Developed by  : Urminder Vohra (JDC)
//Updates       :
//******************************************************************************/
public without sharing class LocationTriggerHandler {
    
    public static void afterInsert(list<Location__c> newLocationList) {
        /*Set<Id> locIds = new Set<Id>();
        for(Location__c loc : newLocationList) {
            locIds.add(loc.Id);
        }
        
        //LocationTriggerHandler.updateLocationCordinates(locIds);*/
    }
    
    public static void beforeUpdate(list<Location__c> newLocationList, Map<Id, Location__c> oldMap) {
        Set<Id> locIds = new Set<Id>();
        for(Location__c loc : newLocationList) {
            Location__c oldLoc = oldMap.get(loc.Id);
            if(oldLoc.Address_Line_1__c <> loc.Address_Line_1__c || oldLoc.Address_Line_2__c <> loc.Address_Line_2__c
                    || oldLoc.City__c <> loc.City__c || oldLoc.Country__c <> loc.Country__c
                    || oldLoc.State__c <> loc.State__c || oldLoc.Postal_Code__c <> loc.Postal_Code__c) {
                loc.Mapping_Status__c = 'New Address';
            }
        }
    }
    //@future(callout = true)
    public static void updateLocationCordinates(Set<Id> locationIds) {
        /*list<Location__c> newLocationList = new list<Location__c>();
        list<Location__c> locationToUpdate= new list<Location__c>();
        list<Apex_Log__c> errList = new list<Apex_Log__c>();
        
        for(Location__c loc : [select Address_Line_1__c, Address_Line_2__c, City__c, State__c, Postal_Code__c, Country__c
                                from Location__c where Id IN :locationIds]) {
            newLocationList.add(loc);
        }       
        
        for(Location__c loc : newLocationList) {
            String address = '';
            if(loc.Address_Line_1__c <> '') {
                address += loc.Address_Line_1__c + ',';
            }
            if(loc.Address_Line_2__c <> '') {
                address += loc.Address_Line_2__c + ',';
            }
            if(loc.City__c <> '') {
                address += loc.City__c + ',';
            }
            if(loc.State__c <> '') {
                address += loc.State__c + ',';
            }
            if(loc.Postal_Code__c <> '') {
                address += loc.Postal_Code__c + ',';
            }
            if(loc.Country__c <> '') {
                address += loc.Country__c + ',';
            }
            address = address.substring(0, address.length()-1);
            address = EncodingUtil.urlEncode(address, 'UTF-8'); 
            
            // build callout
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint('http://maps.googleapis.com/maps/api/geocode/json?address='+address+'&sensor=false');
            req.setMethod('GET');
            req.setTimeout(60000);
            
            try{
            // callout
            HttpResponse res = h.send(req);
 
            // parse coordinates from response
            JSONParser parser = JSON.createParser(res.getBody());
            double lat = null;
            double lon = null;
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'location')){
                   parser.nextToken(); // object start
                   while (parser.nextToken() != JSONToken.END_OBJECT){
                       String txt = parser.getText();
                       parser.nextToken();
                       if (txt == 'lat')
                           lat = parser.getDoubleValue();
                       else if (txt == 'lng')
                           lon = parser.getDoubleValue();
                   }// END - while
                } else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'error_message')){
                    parser.nextToken();
                    Apex_Log__c errLog = new Apex_Log__c();
                    errLog.Class_name__c = 'LocationTriggerHandler';
                    errLog.Message__c =  parser.getText();
                    errLog.Method_Name__c = 'updateLocationCordinates';
                    errList.add(errLog);    
                }// END - if
            } // END - while
            System.debug('**************lat********' + lat);
            System.debug('**************RESPOONSE********' + res.getBody());
            // update coordinates if we get back
            if (lat != null){
                loc.Coordinates__Latitude__s = lat;
                loc.Coordinates__Longitude__s = lon;
                locationToUpdate.add(loc);
            }
 
        } catch (Exception ex) {
            System.debug('**************ERROR OCCURED********' + ex.getMessage());
            Apex_Log__c errLog = new Apex_Log__c();
            errLog.Class_name__c = 'LocationTriggerHandler';
            errLog.Exception_Message__c = ex.getMessage();
            errLog.Message__c = 'Error while updating locations cordinates';
            errLog.Method_Name__c = 'updateLocationCordinates';
            errList.add(errLog);
            
        }
        if(!locationToUpdate.isEmpty()) update locationToUpdate;
        if(!errList.isEmpty()) insert errList;
        
      }*/
    }
}