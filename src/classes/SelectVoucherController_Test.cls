/*****************************************************************************************************************************************************
Class           : SelectVoucherController_Test.cls
Description     : Test Class for SelectVoucherController.cls
Developer       : Appirio (Urminder)
Created Date    : 08/17/2013
Modified        : 03/24/2014 : Leena Mandadapu : SF-310 / SF-192 : Disable OOW and CyberSource Logic
                : 02/20/2015 : Aaron Briggs : SMS-1247 : Class Cleanup + New Methods testAdminOverride and
*****************************************************************************************************************************************************/
@isTest
private with sharing class SelectVoucherController_Test {
    static Case testcase;

    static testMethod void testAvailableVouchers() {
        createData();
        String profileName;
        ApexPages.Standardcontroller stdCtrl = new ApexPages.Standardcontroller(testCase);
     
        for(Profile p : [Select p.Name, p.Id From Profile p where Name != 'System Administrator' LIMIT 1]){
            profileName = p.name;
        }

        User u = CreateUser(profileName);

        System.runAs(u) {
	        SelectVoucherController ctrl = new SelectVoucherController(stdCtrl);
	        Voucher_Permission__c vp = new Voucher_Permission__c();
	        vp.Amount__c = 10.0;
	        vp.Profile__c = profileName;
	        vp.Voucher_Type__c = 'Accommodation Voucher';
	        insert vp;
	     
	        ctrl.availableVoucher.Type__c = 'Accommodation Voucher';
	        ctrl.availableVoucher.Select_Type__c = 'Accommodation Voucher';
	        ctrl.availableVoucher.Amount__c = 10.0;
	        ctrl.availableVoucher.Select_Country__c = 'US';
	        ctrl.voucherAmount = '10';
	        ctrl.findAvailableVoucher();
	     
	        Voucher__c v = [select Case__c,Status__c, Consumer__c from Voucher__c where Id = :ctrl.selectedVoucher.Id];
        }
    }
  
    static testMethod void testAdminOverride() {
    	createData();
    	String profileName;
    	ApexPages.StandardController stdCtrl = new ApexPages.StandardController(testCase);
    	
    	for(Profile p : [SELECT p.Name, p.Id FROM Profile p WHERE Name = 'System Administrator' LIMIT 1]){
    		profileName = p.name;
    	}
    	
    	User u = CreateUser(profileName);
    	
    	System.runAs(u) {
    		SelectVoucherController ctrl = new SelectVoucherController(stdCtrl);
    		ctrl.availableVoucher.Type__c = 'Accommodation Voucher';
            ctrl.availableVoucher.Select_Type__c = 'Accommodation Voucher';
            ctrl.availableVoucher.Amount__c = 10.0;
            ctrl.availableVoucher.Select_Country__c = 'US';
            ctrl.voucherAmount = '10';
            ctrl.findAvailableVoucher();
            
            Voucher__c v = [select Case__c,Status__c, Consumer__c from Voucher__c where Id = :ctrl.selectedVoucher.Id];
    	}
    }
    
    static testMethod void testNullPermissionAmount() {
        createData();
        String profileName;
        ApexPages.Standardcontroller stdCtrl = new ApexPages.Standardcontroller(testCase);
     
        for(Profile p : [Select p.Name, p.Id From Profile p where Name != 'System Administrator' LIMIT 1]){
            profileName = p.name;
        }

        User u = CreateUser(profileName);

        System.runAs(u) {
            SelectVoucherController ctrl = new SelectVoucherController(stdCtrl);
            Voucher_Permission__c vp = new Voucher_Permission__c();
            vp.Amount__c = null;
            vp.Profile__c = profileName;
            vp.Voucher_Type__c = 'Accommodation Voucher';
            insert vp;
         
            ctrl.availableVoucher.Type__c = 'Accommodation Voucher';
            ctrl.availableVoucher.Select_Type__c = 'Accommodation Voucher';
            ctrl.availableVoucher.Amount__c = 10.0;
            ctrl.availableVoucher.Select_Country__c = 'US';
            ctrl.voucherAmount = '10';
            ctrl.findAvailableVoucher();
         
            Voucher__c v = [select Case__c,Status__c, Consumer__c from Voucher__c where Id = :ctrl.selectedVoucher.Id];
        }
    }

    static void createData() {
        Contact cnct = new Contact();
        cnct.LastName = 'test';
        cnct.FirstName = 'ftest';
        cnct.Phone = '12345';
        insert cnct;

        testcase = new Case();
        testcase.Status = 'Open';
        testcase.received_date__c = date.today();
        testcase.shipped_date__c = date.today().addDays(1);
        testcase.outbound_tracking_number__c = '101';
        testcase.outbound_carrier__c = 'TYest1';
        testCase.Origin = 'Social';
        testCase.ContactId = cnct.Id;
        testCase.Offender__c = 'test';
        testCase.Product__c = 'PS4';
        testCase.Sub_Area__c = 'Account Help';
        insert testCase;
     
        Voucher__c v = new Voucher__c();
        v.Amount__c = 10.0;
        v.Country__c = 'US';
        v.Status__c = 'Available';
        v.Type__c = 'Accommodation Voucher';
        v.Voucher_Code__c = '123';
        //LM 3/24 - Added the following 2 statements
        v.Available_Date__c = date.today();
        v.Expiration_Date__c = date.today().addDays(20);
        insert v;
    }

    public static User CreateUser(string profileName) {
        List<Profile> profiles;
        if(profileName != null && profileName != '') 
            profiles = [Select id from Profile where Name = :profileName limit 1];

        User u1 = new User();
        u1.firstName = 'test' ;
        u1.LastName = 'test'; 
        u1.Alias = 'tst';
        u1.Email = 'test' +  '.test@netapp.com';  
        u1.UserName='test'+ Math.random().format() + string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','') + 'user1@gmail.com';

        if(profiles != null && profiles.size() > 0)
            u1.ProfileId = profiles.get(0).Id;

        u1.CommunityNickname = u1.firstname + '_' + +  Math.random().format() + u1.lastName;
        u1.EmailEncodingKey ='ISO-8859-1'; 
        u1.LanguageLocaleKey = 'en_US';
        u1.TimeZoneSidKey = 'America/Los_Angeles';
        u1.CompanyName = 'SCEA';
        u1.LocaleSidKey = 'en_US';
        String fedId =  Math.random().format() + Datetime.now().format()+'1234567890ABCDEFG';       
        u1.FederationIdentifier = fedId.substring(0,20);
        insert u1;      
        return u1;   
    }

}