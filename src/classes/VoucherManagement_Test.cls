//LM 3/17/2014 - Created for VoucherManagement Test.
//LM 3/24/2014 - Updated to add new Required fields to the Test Class - JIRA# SF-310, SF-192
@isTest
private class VoucherManagement_Test {

    static testMethod void myUnitTest() {
                
        Test.StartTest();
        
        //Create Voucher        
        Voucher__c v = New Voucher__c();
        v.Status__c = 'Available';
        //LM 3/24 - Added the following 4 lines of code
        v.Amount__c = 10.0;
        v.Voucher_Code__c = 'Test'+ Math.Floor(Math.Random()*100 + 1);
        v.Available_Date__c = date.today();
        v.Expiration_Date__c = date.today().addDays(20);
        insert v;        
        
       //create Consumer
        list<Contact> cnt = TestClassUtility.createContact(2, true);
        
        //Create Voucher Case               
       Case c = TestClassUtility.createCase('Opened', 'Test', false);
       c.ContactId = cnt[0].Id;
       c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Voucher').getRecordTypeId();             
       insert c;    
        
        //Update Voucher Status = Distirbuted and associate Case to voucher        
        v.Status__c = 'Distributed';
        v.Case__c = c.Id;
        
        update v;
        
        Case caselist = [select Id, Voucher_Type__c from Case where Id = : c.Id ];
        
        system.assertEquals(caselist.Voucher_Type__c, v.Type__c , 'Case Voucher Type should be equal to Voucher Type');
        
        Test.stopTest();
        
        
    }
}