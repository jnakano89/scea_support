/******************************************************************************
Class         : SelectPSNAccountController
Description   : Controller class for SelectPSNAccount [T-180177]
Developed by  : Urminder Vohra(JDC)
Date          : Sep 4, 2013

Updates       :
******************************************************************************/
public with sharing class SelectPSNAccountController {
  
  public list<PSN_Account__c> psnAccountList{get;set;}
  
  public string caseId;
  public string title{get;set;}
  public string subTitle{get;set;}
  public String selectedPsnAccId{get;set;}
  public Case selectedCase;
  
  public SelectPSNAccountController() {
  	psnAccountList = new list<PSN_Account__c>();
  	subTitle = 'Select PSN Account';
  	
  	caseId = Apexpages.currentPage().getParameters().get('caseId');
  	
  	String contactId = getAssociatedContactId(caseId);
  	if(contactId <> null) {
  	  psnAccountList = getPsnAccountList(contactId);	
  	}
  }
  
  public Pagereference selectPsnAccount() {
  	String psnAccId = ApexPages.currentPage().getParameters().get('ctRadio');
  	
  	if(psnAccId == null) {
  		
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select a PSN Acocunt'));
      return null;
    }
    
    return updateObject(psnAccId);
  }
  
  public Pagereference clearPsnAccount() {
  	return updateObject(null);
  }
  
  private Pagereference updateObject(String psnAccId) {
  	selectedCase.PSN_Account__c = psnAccId;
  	try{
  		update selectedCase;
  	}catch(Exception ex) {
  		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,ex.getMessage()));
  		return null;
  	}
  	return returnToRecord();
  }
  
  public Pagereference returnToRecord() {
  	Pagereference pg = new ApexPages.StandardController(selectedCase).view();
  	pg.setRedirect(true); 
  	return pg;
  }
  
  private list<PSN_Account__c> getPsnAccountList(String cntId) {
  	list<PSN_Account__c> paList = new list<PSN_Account__c>();
  	Set<Id> psnAccIds = new Set<Id>();
  	
  	for(PSN_Account__c pa : [select Name, First_Name__c, Last_Name__c, PSN_Sign_In_ID__c 
  								  from PSN_Account__c
  								  where Consumer__c = :cntId]) {
  		paList.add(pa);				
  	} 
  	return paList;
  }
  
  private String getAssociatedContactId(String caseId) {
  	String cntId;
  	for(Case c : [select ContactId, Contact.Name, PSN_Account__c
  					from Case
  					where Id = :caseId]) {
  	  cntId = c.ContactId;
  	  title = c.Contact.Name;
  	  selectedPsnAccId = c.PSN_Account__c;
  	  selectedCase = c;					
  	}
  	return cntId;
  }


 
  
  
}