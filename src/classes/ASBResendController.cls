// =============================================================================
// (c) 2013 Appirio, Inc.
//
// Batch class to process Staging_Service_Inbound__c
//
// Created :
// Jan 6, 2014  Shyam Sundar
// Updates :
// Mar 11, 2014 Leena Mandadapu - Added code to filter out PEC ASB requests going into Service Outbound Object
// Mar 26, 2014 Leena Mandadapu - Added ASB Resend Count logic
// Mar 27, 2014 Aaron Briggs - Added Inbound Tracking and Carrier Details to Task Creation and Clear Logic
// =============================================================================

public with sharing class ASBResendController {
    
    public PageReference doLoad() {
        String caseId = ApexPages.currentPage().getParameters().get('Id');
    
        if(caseId == null || caseId == '') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Case Id is not mentioned in URL.'));
            return null;
        }
        
        for(Case objCase : GeneralUtiltyClass.getCaseDetails(new Set<Id>{caseId})){
            
            // Create a savepoint for multiple transactions
            Savepoint sp = Database.setSavepoint();
                
            try
            {
              //LM 3/26 - Added ASB Resend count logic
              system.debug('<<<<<<<<<< ASB Resend Count' + objCase.ASB_Resend_Count__c + '>>>>>>>>>>>');
              if (objCase.ASB_Resend_Count__c != null)
                 { objCase.ASB_Resend_Count__c ++; }
              else 
                { objCase.ASB_Resend_Count__c = 1; }	 
                  update objCase;
                  
            	//LM - Added if condition to filter out PEC records
                //system.debug('<<<<<<<<<< ASB Resend Location' + objCase.ASB_Service_Center__r.Name + '>>>>>>>>>>>');
                if(objCase.ASB_Service_Center__r.Name != 'PEC' && objCase.ASB_Service_Center__r.Name != 'LRC')
                {
                    Staging_Service_Outbound__c outbound = GeneralUtiltyClass.createStagingServiceOutbound(objCase, objCase.Contact, objCase.Asset__r.Product__r, objCase.Asset__r, objCase.Ship_To__r, objCase.Service_Location__r);
                    outbound.Case_Resend_Flag__c = 'Y';
                    if(objCase.Fee_Type__c == GeneralUtiltyClass.FEE_TYPE_PLAY_STATION_PROTECTION_PLAN) {outbound.Case_Handling__c = 'E';}
                    if(objCase.ASB_Type__c == GeneralUtiltyClass.ASB_TYPE_GREEN_LABEL) {outbound.Case_Handling__c = 'S';}
                    
                    if(objCase.Ship_To__r.Country__c == 'CANADA')
                        {outbound.Batch_Type__c = 'MTC - ASB';} 
                    else
                        {outbound.Batch_Type__c = 'MTCFW - ASB';}
                    
                    insert outbound;
                } //end if statement.
                
                Task tsk = new Task();
                tsk.OwnerId = UserInfo.getUserId();
                tsk.WhatId = objCase.Id;
                tsk.Subject = 'ASB Resend - ' + objCase.ASB_Type__c;
                tsk.Priority = 'Normal';
                tsk.Status = 'Completed';
                tsk.WhoId = objCase.contactId;
                tsk.ActivityDate = System.today();
                //AB - Added Inbound Carrier / Tracking Number to the Task Details
                tsk.Description = 'ASB RESEND requested by ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + '.' + 
                            '\nPrevious Tracking Information' + 
                            '\nASB Outbound Carrier: ' + (objCase.ASB_Outbound_Carrier__c == null ? '' : objCase.ASB_Outbound_Carrier__c) +
                            '\nASB Outbound Tracking Number: '  + (objCase.ASB_Outbound_Tracking_Number__c == null ? '' : objCase.ASB_Outbound_Tracking_Number__c) +
                            '\nInbound Carrier: ' + (objCase.Inbound_Carrier__c == null ? '' : objCase.Inbound_Carrier__c) +
                            '\nInbound Tracking Number: ' + (objCase.Inbound_Tracking_Number__c == null ? '' : objCase.Inbound_Tracking_Number__c);
                insert tsk;
                
                //AB - Added Inbound Carrier / Tracking Number to the Clear Logic
                Update new Case(Id = caseId, ASB_Outbound_Carrier__c = null, ASB_Outbound_Tracking_Number__c = '', Inbound_Carrier__c = null, Inbound_Tracking_Number__c = '', Error_Message__c = 'ASB Resend has been sent.');
                
                system.debug(objCase);
                
                return new PageReference('/' + caseId);
            }
            catch(Exception ex){
                // Rollback to the previous state
                Database.rollback(sp);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error occurred in Resend ASB.'));
            }
        }
        
        
        return null;
    }
}