/**********************************************************************************************************************
Developed By : Appirio, Inc., Charudatta Mandhare(JDC)
Created      : 12/02/13
Description  : This class maintains all Validation Rules methods to validate integration data.
Modified     :
               12/30/2014 : Aaron Briggs : #11389220 : YYYY represents the week year; changed to yyyy for actual year
***********************************************************************************************************************/

public with sharing class ValidationRulesUtility {

	static Set<String> eligibleStates = new Set<String>();

	static {
		for(No_ESP_States__c es : No_ESP_States__c.getAll().values()) {
			eligibleStates.add(es.Code__c);
		}
	}
	///////////////////////////////////////////
	//		Start 	Validation For number.
	///////////////////////////////////////////
	public static String validateNumeric(String strValue, String strLabel) {
		return validateNumeric(true, strValue, strLabel);
	}

	public static String validateNumeric(boolean isRequired, String strValue, String strLabel) {
		// 	Numeric_Only
		if(strValue == null || strValue.trim() == ''){
			if(isRequired){
				return (strLabel + ' cannot be null');
			}else{
				return 'Success';
			}
		}
		if(!checkDecimalOnly(strValue)) return (strLabel + ' should be Numeric Only'); 
		// no Error
		return 'Success';
	}
	public static String validateNumericFixLength(boolean isRequired, String strValue, String strLabel, Integer numLength) {
		// 	Numeric_Only
		String msg = validateNumeric(isRequired, strValue, strLabel);
		if(!'Success'.equalsIgnoreCase(msg)){
			return msg;
		}
		if(strValue!=null){
			if(strValue.trim().length()!= numLength) return (strLabel + ' should be '+numLength+' digit long.');
		}
		// no Error
		return 'Success';
	}
	public static String validateNumericMaxLength(boolean isRequired, String strValue, String strLabel, Integer numLength) {
		// 	Numeric_Only
		String msg = validateNumeric(isRequired, strValue, strLabel);
		if(!'Success'.equalsIgnoreCase(msg)){
			return msg;
		}
		if(strValue!=null){
			if(strValue.trim().length()> numLength) return (strLabel + ' should be '+numLength+' digit long.');
		}
		// no Error
		return 'Success';
	}

	
	///////////////////////////////////////////
	//		END 	Validation For number.
	///////////////////////////////////////////


	///////////////////////////////////////////
	//		Start 	Validation For Charcters.
	///////////////////////////////////////////
	public static String validateCharcters(String strValue, String strLabel) {
		return validateCharcters(true, strValue, strLabel);
	}
	public static String validateCharcters(boolean isRequired, String strValue, String strLabel) {
		// 	Chars_Only
		if(strValue == null || strValue.trim() == ''){
			if(isRequired){			
				return (strLabel + ' cannot be null');
			}else{
				return 'Success';
			}
		}
		if(!checkCharctersOnly(strValue)) return (strLabel + ' should be Characters Only'); 

		// no Error
		return 'Success';
	}
	public static String validateCharctersFixLength(boolean isRequired, String strValue, String strLabel, Integer numLength) {
		// 	Chars_Only
		String msg = validateCharcters(isRequired, strValue, strLabel);
		if(!'Success'.equalsIgnoreCase(msg)){
			return msg;
		}
		if(strValue!=null){
			if(strValue.trim().length()!= numLength) return (strLabel + ' should be '+numLength+' Chars long.');
			return 'Success';
		}

		// no Error
		return 'Success';
	}

	public static String validateCharctersOnlyMaxLength(String strValue, String strLabel, Integer chrLength) {
		return validateCharctersOnlyMaxLength(true, strValue, strLabel, chrLength);
	}
	public static String validateCharctersOnlyMaxLength(boolean isRequired, String strValue, String strLabel, Integer numLength) {
		// 	Chars_Only
		String msg = validateCharcters(isRequired, strValue, strLabel);
		if(!'Success'.equalsIgnoreCase(msg)){
			return msg;
		}
		if(strValue!=null){
			if(strValue.trim().length()> numLength) return (strLabel + ' should be '+numLength+' Chars long.');
			return 'Success';
		}
		// no Error
		return 'Success';
	}
	public static String validateCharctersOnlySerialMaxLength(boolean isRequired, String strValue, String strLabel, Integer numLength) {
		// 	Chars_Only
		if(strValue == null || strValue.trim() == ''){
			if(isRequired){			
				return (strLabel + ' cannot be null');
			}else{
				return 'Success';
			}
		}
		String msg = (checkCharctersOnlySerial(strValue)) ?'Success' :(strLabel + ' should be Characters Only');
		if(!'Success'.equalsIgnoreCase(msg)){
			return msg;
		}
		if(strValue!=null){
			if(strValue.trim().length()> numLength) return (strLabel + ' should be '+numLength+' Chars long.');
			return 'Success';
		}
		// no Error
		return 'Success';
	}

	///////////////////////////////////////////
	//		END 	Validation For Charcters.
	///////////////////////////////////////////

	///////////////////////////////////////////
	//		END 	Validation For Letters.
	///////////////////////////////////////////

	public static boolean checkLettersOnly(String str) {
		if(!String.isEmpty(str)){
			str = str.replace(' ', '');
		}
		Pattern isnumbers = Pattern.Compile('^[A-Za-z]+$');
		Matcher postalMatch = isnumbers.matcher(str);

		if(postalMatch.Matches()){
			//has only chars
			return true;
		}
		return false;
	}

	public static String validateLettersLength(String strValue, String strLabel, Integer chrLength) {		
		String msg = validateLettersLength(true, strValue, strLabel, chrLength);
		if(!'Success'.equalsIgnoreCase(msg)){
			return msg;
		}
		if(strValue!=null){
			if(strValue.trim().length()!=chrLength) return (strLabel + ' should be '+chrLength+' letter long.');			
		}
		return 'Success';
	}

	public static String validateLettersLength(boolean isRequired, String strValue, String strLabel, Integer chrLength) {
		// 	Chars_Only
		if(isRequired){
			if(strValue == null || strValue.trim() == ''){
				return (strLabel + ' cannot be null');
			}else{
				return 'Success';
			}
		}	
		if(strValue != null ){
			if(!checkLettersOnly(strValue)) return (strLabel + ' should be Letters Only');
		}
		// no Error
		return 'Success';
	}

	public static String validateLettersOnlyMaxLength(String strValue, String strLabel, Integer chrLength) {
		return validateLettersOnlyMaxLength(true, strValue, strLabel, chrLength);
	}
	public static String validateLettersOnlyMaxLength(boolean isRequired, String strValue, String strLabel, Integer chrLength) {
		String msg = validateLettersLength(true, strValue, strLabel, chrLength);
		if(!'Success'.equalsIgnoreCase(msg)){
			return msg;
		}
		if(strValue!=null){
			if(strValue.trim().length()>chrLength) return (strLabel + ' should be '+chrLength+' letter long.');			
		}
		return 'Success';
	}


	///////////////////////////////////////////
	//		END 	Validation For Letters.
	///////////////////////////////////////////

	public static String validateCharctersOnlyLength(String strValue, String strLabel, Integer chrLength) {
		// 	Chars_Only
		if(strValue == null || strValue.trim() == '')  return (strLabel + ' cannot be null');
		if(!checkCharctersOnlyExt(strValue)) return (strLabel + ' should be Charcters Only');

		if(strValue.trim().length()!=chrLength) return (strLabel + ' should be '+chrLength+' charcter long.'); 

		// no Error
		return 'Success';
	}



	public static boolean checkDecimalOnly(String str) {
		if(!String.isEmpty(str)){
			Pattern isnumbers = Pattern.Compile('^[0-9]+(\\.[0-9]+)?$');
			Matcher postalMatch = isnumbers.matcher(str);
	
			if(postalMatch.Matches()){
				//has only numbers
				return true;
			}
		}
		return false;
	}

	public static boolean checkNumericOnly(String str) {
		if(!String.isEmpty(str)){
			Pattern isnumbers = Pattern.Compile('^[0-9]+$');
			Matcher postalMatch = isnumbers.matcher(str);
	
			if(postalMatch.Matches()){
				//has only numbers
				return true;
			}
		}
		return false;
	}

	public static boolean checkLettersOnlyExt(String str) {
		if(!String.isEmpty(str)){
			str = str.replace(' ', '');
		}
		Pattern isnumbers = Pattern.Compile('^[A-Za-z ]+$');
		Matcher postalMatch = isnumbers.matcher(str);

		if(postalMatch.Matches()){
			//has only chars
			return true;
		}
		return false;
	}

	public static boolean checkCharctersOnlySerial(String str) {
		Pattern isnumbers = Pattern.Compile('^[A-Za-z0-9-]+$');
		Matcher postalMatch = isnumbers.matcher(str);

		if(postalMatch.Matches()){
			//has only chars
			return true;
		}
		return false;
	}	

	public static boolean checkCharctersOnlyExt(String str) {
		Pattern isnumbers = Pattern.Compile('^[A-Za-z0-9-_\\., ]+$');
		Matcher postalMatch = isnumbers.matcher(str);

		if(postalMatch.Matches()){
			//has only chars
			return true;
		}
		return false;
	}	

	public static boolean checkCharctersOnly(String str) {
		Pattern isnumbers = Pattern.Compile('^[A-Za-z\\s+]*$');
		Matcher postalMatch = isnumbers.matcher(str);

		if(postalMatch.Matches()){
			//has only chars
			return true;
		}
		return false;
	}

	public static String valdiateNames(String strValue, String strLabel) {

		if(strValue == null || strValue.trim() == '')  return (strLabel + ' cannot be null');

		if(!checkNumericOnly(strValue)) return (strLabel + ' should be Numeric Only'); 
		Pattern regexPattern = Pattern.compile('^[\\p{L}\\. \'-]+$');
		Matcher regexMatcher = regexPattern.matcher(strValue);

		if (!regexMatcher.matches()) {
			return strLabel + ' should be a valid character';
		} else {
			return 'Success';
		}
	}
	public static String isValidCanadaZipCode(String strLabel, String strValue){
		if(strValue == null || strValue.trim() == '')  return (strLabel + ' cannot be null');

		Pattern regexPattern = Pattern.compile('^[A-Za-z][0-9][A-Za-z] [0-9][A-Za-z][0-9]$');
		Matcher regexMatcher = regexPattern.matcher(strValue);

		if (!regexMatcher.matches()) {
			return strLabel + ' should be a valid \''+strLabel+'\'.';
		} else {
			return 'Success';
		}		
	}
	
	//Validate Date format MMDDYYYY
	public static String validateDate(String strValue, String strLabel) { 


		if(strValue == null || strValue.trim() == '')  return (strLabel + ' cannot be null');
		System.debug('==============' + strLabel + '==========' + strValue);

		if(strValue.length() == 10 && strValue.contains('-')) return 'Success';

		Pattern regexPattern = Pattern.compile('(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])((19|20)\\d\\d)');
		Matcher regexMatcher = regexPattern.matcher(strValue);

		if (!regexMatcher.matches()) {
			return strLabel + ' should be a valid date.';
		} else {
			return 'Success';
		}
	}

	public static String dateAsString(Date orgDate){
		String StrDate;
		if(orgDate==null){
			return '';
		}
		
		System.Debug('>>>>>>>+orgDate<<<<<<' +orgDate);
		
		//AB : 12/30/14 : #11389220 : Format changed from YYYY to yyyy'
        StrDate= Datetime.newInstance(orgDate.year(), orgDate.month(), orgDate.day()).format('yyyyMM');
        System.Debug('========================> StrDate: ' + StrDate);
		
		String day = String.valueOf(orgDate.day()).length() == 2 ? '' + orgDate.day() : '0' + orgDate.day() ;
	    
	    StrDate+=day;
	    
	    System.Debug('>>>>>>>+StrDate<<<<<<' +StrDate);
	    return StrDate;
	}

	public static boolean isEligibleForEspPurchase(String stateName) {
		return eligibleStates.contains(stateName);
	}

	public static string ValidateConsumerAreaCode(String PhoneNumber){

		//Numeric_Only_Consumer_Area_Code

		// on Error
		return 'Error Message';

		// no Error
		return 'Success';
	}

	public static String ValidateZipcode(String PostalCode){

		// 	Numeric_Only_Billing_Zip_Code
		if(postalCode == null || postalCode.trim() == '')  return 'Zip/Postal Code cannot be null';
		
		if(isValidCanadaZipCode('Zip/Postal', PostalCode) == 'Success'){ return 'Success';}
		
		if(!checkNumericOnly(postalCode)) return 'Zip/Postal Code should be Valid'; 

		// no Error
		return 'Success';
	}

	public static String removeAllSpecialChars(String inputString) {
		String specialChars = '&A^A@A|A\\A?A/A>A.A<A,A~A`A!A$A#A%A^A*A(A)A-A_A=A+A{A}A[A]A\'A"';
		for(integer i=0; i<specialChars.split('A').size(); i++) {
			inputString = inputString.replace(specialChars.split('A')[i], '');	
		}

		return inputString;


	}
	
	public static Date parseDateTime(String dt){
		if(!String.isEmpty(dt) && dt.length() >= 10){
			Datetime objDt = parseDateTimeMMDDYYY(dt);
			if(objDt!=null){
				return objDt.date();
			}
			//Integer  dtStr = integer.valueOf(dt.subString(0,2));
			//Integer  mnth = integer.valueOf(dt.subString(3,5));
			//Integer  year = integer.valueOf(dt.subString(6,10));
			//return Date.newInstance(year, mnth, dtStr);
		}
		return null;
	}
	public static DateTime parseDateTimeMMDDYYY(String dt){
		System.debug('=============' +  dt.length());
		if(!String.isEmpty(dt) && dt.length() >7){//  1/1/2013 1:1:1
			//String datetext = '12/13/2013 8:45:48';
			List<String> parts = dt.split( ' ');
			if(parts!=null && parts.size()>0){
				List<String> datePart = parts[0].split('/');
				if(datePart!=null && datePart.Size()>2){
					String zCorrectFormat = datePart[2]+'-'+datePart[0]+'-'+datePart[1];
					Integer  dtStr = integer.valueOf(datePart[1]);
					Integer  mnth = integer.valueOf(datePart[0]);
					Integer  year = integer.valueOf(datePart[2]);

					Integer hr = 0;
					Integer min = 0;
					Integer sec = 0;
					
					if(parts.size()>1){
						if(!String.isEmpty(parts[1])){
							List<String>timeParts = parts[1].split(':');
							if(timeParts!=null && timeParts.size()==3){
								hr = integer.valueOf(timeParts[0]);
								min = integer.valueOf(timeParts[1]);
								sec = integer.valueOf(timeParts[2]);
							}
						}
					}
					//zCorrectFormat = zCorrectFormat  + ' '+timeFormat;
					//system.debug('************>>'+zCorrectFormat);
					//return Datetime.valueOfGmt(zCorrectFormat);
					return Datetime.newInstance(year,mnth, dtStr, hr, min, sec);
				}
			}
			//system.debug('******::'+dt.formatGmt('yyyy:MM:dd hh:mm:ss'));
			//return Datetime.newInstance(year, mnth, dtStr, hr, min, sec);
		}
		return null;
	}	
	/*
	public static DateTime parseDateTimeDDMMYYY(String dt){
		System.debug('=============' +  dt.length());
		if(!String.isEmpty(dt) && dt.length() >13){//  1/1/2013 1:1:1
			//String datetext = '12/13/2013 8:45:48';
			List<String> parts = dt.split( ' ');
			if(parts!=null && parts.size()>1){
				List<String> datePart = parts[0].split('/');
				if(datePart!=null && datePart.Size()>2){
					String zCorrectFormat = datePart[2]+'-'+datePart[0]+'-'+datePart[1]+' '+parts[1];
					//Get GTM
					Datetime correctFormat = Datetime.valueOf(zCorrectFormat);
					
					System.Debug('>>>>>>>>>+correctFormat<<<<<<<<<<' +correctFormat);
					return Datetime.newInstance(correctFormat.Year(),correctFormat.Month(), correctFormat.day(), correctFormat.Hour(), correctFormat.Minute(), correctFormat.second());
				}
			}
			//system.debug('******::'+dt.formatGmt('yyyy:MM:dd hh:mm:ss'));
			//return Datetime.newInstance(year, mnth, dtStr, hr, min, sec);
		}
		return null;
	}	
	*/
	public static DateTime parseDateTimeGMT(String dt){
		System.debug('=============' +  dt.length());
		if(dt <> null){
		
		//return datetime.valueOf(dt);
		System.debug('>>>>>>>>>>>>+parseDateTimeDDMMYYY(dt)<<<<<<<<<< ' +parseDateTimeMMDDYYY(dt));
		
		return parseDateTimeMMDDYYY(dt);
		
		}
		
		/*if(dt <> null && !String.isEmpty(dt) && dt.length() == 19){
			Integer  dtStr = integer.valueOf(dt.subString(0,2));
			Integer  mnth = integer.valueOf(dt.subString(3,5));
			Integer  year = integer.valueOf(dt.subString(6,10));
			Integer  hr = integer.valueOf(dt.subString(11,13));
			Integer  min = integer.valueOf(dt.subString(14,16));
			Integer  sec = integer.valueOf(dt.subString(17,19));
			return Datetime.newInstance(year, mnth, dtStr, hr, min, sec);
		} */
		return null;
	}
	
	public static Date formatDateMMDDYYY(String dtStr) {
		if(dtStr.length() == 8) {
    		String mnth = dtStr.substring(0,2);
    		String dt = dtStr.substring(2,4);
    		String year = dtStr.substring(4,8);
    		System.debug('======' + dt + '====' + mnth + '===' + year);
    		Date dat = Date.newInstance(Integer.valueOf(year), Integer.valueOf(mnth), Integer.valueOf(dt)); 
    		return dat;
    	}
		return null;
	}	

	public static Date formatDateDDMMYYY(String dtStr) {
		if(dtStr.length() == 8) {
    		String dt = dtStr.substring(0,2);
    		String mnth = dtStr.substring(2,4);
    		String year = dtStr.substring(4,8);
    		System.debug('======' + dt + '====' + mnth + '===' + year);
    		Date dat = Date.newInstance(Integer.valueOf(year), Integer.valueOf(mnth), Integer.valueOf(dt)); 
    		return dat;
    	}
		return null;
	}


	public static Date formatDate(String dtStr) {
		/*if(dtStr.length() == 8) {
    		String dt = dtStr.substring(0,2);
    		String mnth = dtStr.substring(2,4);
    		String year = dtStr.substring(4,8);
    		System.debug('======' + dt + '====' + mnth + '===' + year);
    		Date dat = Date.newInstance(Integer.valueOf(year), Integer.valueOf(mnth), Integer.valueOf(dt)); 
    		return dat;
    	} */

		// Incoming date format  20130228    28th feb 2013
		if(dtStr!=null && dtStr.length() == 8) {
			String year = dtStr.substring(0,4);
			String mnth = dtStr.substring(4,6);
			String dt = dtStr.substring(6,8);
			System.debug('======' + dt + '====' + mnth + '===' + year);	
			//system.debug(date.valueof(year+'-'+ mnth +'-'+dt).addmonths(24));
			Date dat = Date.newInstance(Integer.valueOf(year), Integer.valueOf(mnth), Integer.valueOf(dt)); 
			System.debug('======' + dat);
			return dat;
		} 
		return null;
	}

	public static boolean validateDateFormat(String dtStr) {
		if(dtStr.length() == 8) {
			String year = dtStr.substring(0,4);
			String mnth = dtStr.substring(4,6);
			String dt = dtStr.substring(6,8);
			System.debug('======' + dt + '====' + mnth + '===' + year);	
			try {
				Date.parse(mnth + '/' + dt + '/' + year);
				return true;
			} catch(Exception ex) {
				return false;
			}
		} 
		return false;
	}
	// Add more static methods for other validations.

}