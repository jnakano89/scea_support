//
public abstract class GenericRequestBuilder {

    public abstract String buildRequest(String template, map<String, object> params);

    public static String formatDateTime(DateTime d)
    {
        String dateVal = d.format('yyyy-MM-dd HH:mm:ss.SSSZ').replace(' ','T');
        return dateVal.left(26) + ':' + dateVal.right(2);
    }

     /*
        Request builder defGenericRequestBuilder
     */
    public class defGenericRequestBuilder extends GenericRequestBuilder {
        map<String, object> paramsM{get;set;}

        public defGenericRequestBuilder(map<String, object> params)
        {
            paramsM = params;
        }   
        public defGenericRequestBuilder()
        {
        }

        public override String buildRequest(String template, map<String, object> params)
        {
            try{
                if(paramsM!=null){
                    for(String key : paramsM.keySet()){
                        params.put(key, paramsM.get(key));
                    }
                }
                GenericTemplateParser parser = new GenericTemplateParser(template);
                system.debug('parsed string: ' +parser.getParsedContent(params));              
                String parsedContent = parser.getParsedContent(params);
                return parsedContent;
            }
            catch (Exception ex){
                ExceptionHandler.logException(ex);
                return '';
            }
        }
    }
/*    
    public class AccountLookupRequestBuilder extends GenericRequestBuilder {
        map<String, object> paramsM{get;set;}

        public AccountLookupRequestBuilder(map<String, object> params)
        {
            paramsM = params;
        }   
        public override String buildRequest(String template, map<String, object> params)
        {
            try{
                if(paramsM!=null){
                    for(String key : paramsM.keySet()){
                        params.put(key, paramsM.get(key));
                    }
                }
                //get template name
                String criteriaName = (String)params.get('searchtemplate');
                //get Template and create parser.
                GenericTemplateParser liparser = new GenericTemplateParser((String)params.get(criteriaName));
                String SearchCriteria = liparser.getParsedContent(params);
                params.put('XMLcontent-Criteria',SearchCriteria);

                GenericTemplateParser parser = new GenericTemplateParser(template);

                String parsedContent = parser.getParsedContent(params);
                return parsedContent;
            }
            catch (Exception ex){
                ExceptionHandler.logException(ex);
                return '';
            }
        }
    }*/

}