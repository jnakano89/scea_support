//**************************************************************************/
// (c) 2013 Appirio, Inc.
//
// Description : This class maintains all web service callout called from VF page ConsumerSearch.
//                  
// Original August 08, 2013  : KapiL Choudhary(JDC) Created for the Task T-169605
// Updated : August 13, 2013 : Urminder Moved static values to custom setting (Integeration)
// Updated : August 13, 2013 : Urminder code clean up clear code related to SIRAS.(already moved to SirasUtility)
//
// ***************************************************************************/
public class WebServiceUtility {
	
		
	public static List<ConsumerSearchResult> getConsumers(String emailAddress, String phoneNumber, String psnOnlineID){
			
		List<ConsumerSearchResult> consumerSearchResultList = new List<ConsumerSearchResult>();
		
		for(integer i=0;i<10;i++){
				ConsumerSearchResult cSearchResult= new ConsumerSearchResult();
				cSearchResult.firstName = 'FirstName '+string.valueof((i+1));
				cSearchResult.lastName =  'LastName ' +string.valueof((i+1));
				cSearchResult.mdmID =  'mdmID ' +string.valueof((i+1));
				cSearchResult.primaryPhoneNumber =  string.valueof(1111 * (i+1));
				consumerSearchResultList.add(cSearchResult);
		}
		
		return consumerSearchResultList;
	}
	
	public static List<AssetSearchResult> getAssets(String mdmAccountID){
		List<AssetSearchResult> AssetSearchResultList = new List<AssetSearchResult>();
		return AssetSearchResultList;
	}
	
	
	
	//This will represent 1 result from a web service call.
	public class ConsumerSearchResult{
		 public  String firstName{get;set;}
		 public  String LastName{get;set;}
		 public  String registeredEmailAddress{get;set;}
		 public  String supportEmailAddress {get;set;}
		 public  String primaryPhoneNumber{get;set;}
		 public  String alternatePhoneNumber{get;set;}
		 public  String psnOnlineID{get;set;}
		 public  String mdmID{get;set;}
		 public  String gender{get;set;}
		 public  String language{get;set;}
		 public  Date birthdate{get;set;}
		 public  Integer currentAge{get;set;}
	}
	
	//This will represent 1 result from a web service call.
	public class AssetSearchResult{
		public  String consoleKey{get;set;} 
		public  String consoleID{get;set;}  
		public  String consoleWarrantyDetails{get;set;}  
		public  String consoleDescription{get;set;}
		public  String consoleType{get;set;}
		public  String consoleModelNumber{get;set;}  
		public  String consoleSerialNumber{get;set;}  
		public  String consoleBarcode{get;set;}
		public  Date consolePurchaseDate{get;set;}
		public  Date consoleRegistrationDate{get;set;}
	}
	
	
}