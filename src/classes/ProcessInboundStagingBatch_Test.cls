/****************************************************************************************************************************************************************************************
Created :   Appirio
        :   Aaron Briggs : 06/26/2014 : SF-1074 : Add Inbound Charge Type parsing assert to confirm base object is not updated.
*****************************************************************************************************************************************************************************************/
@isTest
private class ProcessInboundStagingBatch_Test {

    static testMethod void myUnitTest() {
        list<Asset__c> astlist = TestClassUtility.createAssets(10, true);
        
        list<Case> caselist = new list<Case>();
        for(Integer i = 0; i < 10 ; i++) {
        	Case testCase = TestClassUtility.createCase('Open', '', false);
        	//AB - 06/26/2014 - Set Default Fee Type for Service Record 
        	testCase.Fee_Type__c = 'In Warranty';
        	testCase.External_SR_Number__c = 'TC00' + (i+1);
        	testCase.Asset__c = astlist[i].Id;
        	caselist.add(testCase);
        }
        
        insert caselist;
        
        Product__c prod = TestClassUtility.createProduct(1, true)[0];
        
        Staging_Service_Inbound__c inbound = new Staging_Service_Inbound__c();
        inbound.Asset_Serial_Number__c = '822822822';
        inbound.Product_SKU__c = prod.SKU__c;
        inbound.Case_Work_Order__c = 'C40256';
        inbound.Case_Number__c = 'TC001';
        inbound.Case_Walk_In_Flag__c = 'FALSE';
        inbound.Case_Received_Date__c = '12/12/2013 11:24:00';
        inbound.Case_Status__c = 'Unit-Received';
        insert inbound;
        
        Staging_Service_Inbound__c inbound2 = new Staging_Service_Inbound__c();
        inbound2.Case_Work_Order__c = 'C40253';
        inbound2.Case_Number__c = 'TC002';
        inbound2.Case_Ship_Method__c = 'Purolator';
        inbound2.Case_Outbound_Tracking_Number__c = '40253A';
        inbound2.Case_Box_Shipped_Date__c = '12/12/2013 8:45:47';
        inbound2.Case_Inbound_Tracking_Number__c = '40253B';
        inbound2.Case_Status__c = 'ASB-Shipped';
        insert inbound2;
        
        Staging_Service_Inbound__c inbound3 = new Staging_Service_Inbound__c();
        inbound3.Case_Work_Order__c = 'C40253';
        inbound3.Case_Number__c = 'TC003';
        inbound3.Case_Ship_Method__c = 'Purolator';
        inbound3.Case_Outbound_Tracking_Number__c = '40253A';
        inbound3.Case_Box_Shipped_Date__c = '12/12/2013 8:45:47';
        inbound3.Case_Inbound_Tracking_Number__c = '40253B';
        inbound3.Case_Status__c = 'ASB-DoubleBox';
        insert inbound3;
        
        Staging_Service_Inbound__c inbound4 = new Staging_Service_Inbound__c();
        inbound4.Case_Number__c = 'TC004';
        inbound4.Exchanged_SKU__c = prod.SKU__c;
        inbound4.Product_SKU__c = prod.SKU__c;
        inbound4.Case_Work_Order__c = 'M40021';
        inbound4.Case_Ship_Method__c = 'FedEx';
        inbound4.Case_Outbound_Tracking_Number__c = '24332412341';
        inbound4.Case_Box_Shipped_Date__c = '12/09/2013 15:16:00';
        inbound4.Case_Waybill__c = '89989998888';
        inbound4.Case_Walk_In_Flag__c = 'true';
        // W = In Warranty - inbound4.Case_Charge_Type__c = 'W';
        //AB - 06/26/2014 - Update Test Charge Type to confirm no change to base object
        inbound4.Case_Charge_Type__c = 'G';
        inbound4.Case_Date_Purchased__c = '12/02/2013 15:16:00';
        inbound4.Exchanged_Flag__c = 'TRUE';
        inbound4.Exchanged_Condition__c = 'NEW';
        inbound4.Case_Status__c = 'Unit-Shipped';
        insert inbound4;
        
        Staging_Service_Inbound__c inbound5 = new Staging_Service_Inbound__c();
        inbound5.Case_Number__c = 'TC005';
        inbound5.Exchanged_SKU__c = prod.SKU__c;
        inbound5.Product_SKU__c = prod.SKU__c;
        inbound5.Case_Work_Order__c = '5000';
        inbound5.Case_Ship_Method__c = 'FedEx';
        inbound5.Case_Outbound_Tracking_Number__c = '24332412341';
        inbound5.Case_Box_Shipped_Date__c = '12/12/2013 11:24:00';
        inbound5.Case_Status_Date__c = '12/12/2013 11:24:00';
        inbound5.Case_Received_Date__c = '12/12/2013 11:24:00';
        inbound5.Case_Status__c = 'Fulfillment-Shipped';
        insert inbound5;
        
        Staging_Service_Inbound__c inbound6 = new Staging_Service_Inbound__c();
        inbound6.Case_Number__c = 'TC006';
        inbound6.Exchanged_SKU__c = prod.SKU__c;
        inbound6.Product_SKU__c = prod.SKU__c;
        inbound6.Case_Work_Order__c = 'M40021';
        inbound6.Case_Ship_Method__c = 'UPS';
        inbound6.Case_Outbound_Tracking_Number__c = '001AQ';
        inbound6.Case_Box_Shipped_Date__c = '12/12/2013 11:24:00';
        inbound6.Case_Status_Date__c = '12/12/2013 11:24:00';
        inbound6.Case_Received_Date__c = '12/12/2013 11:24:00';
        inbound6.Case_Status__c = 'Unit-Completed';
        insert inbound6;
        
        //AB - 06/26/2014 - Assert Fee Type does not change when
        Case feeCaseOG = [SELECT Fee_Type__c FROM Case WHERE External_SR_Number__c =: inbound4.Case_Number__c];
        
        Test.startTest();
        Database.executeBatch(new ProcessInboundStagingBatch());
        Test.stopTest();
        
        Integer indx = 0 ;
        for(Staging_Service_Inbound__c inb : [select Processed_Status__c from Staging_Service_Inbound__c]) {
        	System.assertEquals('Success', inb.Processed_Status__c , indx++ + '--->All records should be processed successfully');
        }
        
        //AB - 06/26/2014 - Assert Fee Type does not change during Unit-Shipped Processing
        Case feeCase = [SELECT Fee_Type__c FROM Case WHERE External_SR_Number__c =: inbound4.Case_Number__c];
        System.assertEquals(feeCaseOG, feeCase, 'Failure : Fee Type Mismatch');
    }
}