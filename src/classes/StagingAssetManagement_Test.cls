@isTest
global class StagingAssetManagement_Test {

    static testMethod void myUnitTest() {
        Test.startTest();
    	Test.setMock(WebServiceMock.class, new WebServiceMockUpdateAsset());
    	
    	OSB__c setting = new OSB__c();
    	setting.Update_Asset_Enabled__c = true;
    	insert setting;
    	
    	list<Asset__c> astList = TestClassUtility.createAssets(2, true);
    	
    	Staging_Asset__c stagingAsset = new Staging_Asset__c();
    	stagingAsset.Asset__c = astList[0].Id;
    	insert stagingAsset;
    	
    	Test.stopTest();
    }
     global class WebServiceMockUpdateAsset implements WebServiceMock {
	   global void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,	
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	      		Sony_MiddlewareUpdateAccountAsset.PSUpdateAcntAsset_Output_element responseElm = 
	      		  	new Sony_MiddlewareUpdateAccountAsset.PSUpdateAcntAsset_Output_element();
	       		
	       		
	       		    Sony_MiddlewareUpdateAccountAsset.Asset_mdm  ast = new Sony_MiddlewareUpdateAccountAsset.Asset_mdm();
					ast.AssetRowId = '1';
					ast.SerialNumber = '1234';
					ast.ProductName = 'testing';
					
					//creating list of assets
					
					Sony_MiddlewareUpdateAccountAsset.ListOfAsset_mdm   last = new Sony_MiddlewareUpdateAccountAsset.ListOfAsset_mdm ();
					last.Asset = new list<Sony_MiddlewareUpdateAccountAsset.Asset_mdm>{ast};
					
					//associating assets with account.
					
					Sony_MiddlewareUpdateAccountAsset.Account_mdm   acc = new Sony_MiddlewareUpdateAccountAsset.Account_mdm ();
					acc.MDMAccountRowId = '1234567';
					acc.ListOfAsset = last;
					
					//creating list of list of accounts
					
					Sony_MiddlewareUpdateAccountAsset.ListOfAccount_mdm    lacc = new Sony_MiddlewareUpdateAccountAsset.ListOfAccount_mdm  ();
					lacc.Account  = new list<Sony_MiddlewareUpdateAccountAsset.Account_mdm>{acc};
				  
				  responseElm.ListOfAccount = lacc;
				       		 
	       		  response.put('response_x', responseElm); 
	   		   }
    }
    
}