/***********************************************************************************************************************************************************************
//Name  :  SmartContactSearchExtention
//Author:  Appirio Offshore (Urminder Vohra)
//Date  :  July 19, 2011
//Updated : August 13, 2013     : KapiL Choudhary(JDC) Updated for the Task T-171732
//          August 23, 2013     : Urminder(JDC) populate contactPhone from Url if parameters is passed.
//          September 9, 2013  : KapiL Choudhary(Jaipur-DC) Added Alt_Phone__c,Alt_E_mail__c field in search. Updated for the Task T-180490.
//          September 10, 2013  : Urminder(JDC) : added redirectToContact() [T-181516]

            11/06/2014 : Leena Mandadapu : Jira# SMS-864 : Green buttons not appearing on CTI popped Consumer detail page - Added page redirect code to refresh the page
//***********************************************************************************************************************************************************************/
public with sharing class SmartContactSearchExtention {

    
    //Search criteria fields
    public String contactFirstNameToSearch {set;get;}
    public String contactLastNameToSearch {set;get;}
    public String contactEmail {set;get;}
    public String contactPhone {set;get;}
    public String contactLanguage {set;get;}
        
    public integer searchCount{set; get;}
    public string searchStatus{set; get;}
    public string sortField{set;get;}
    private string previousSortField;
    private string sortOrder;
      
    public boolean isAsc{set; get;}
    public Integer showingFrom{get;set;}
    public Integer showingTo{get;set;}
    public string query ;
    
    public boolean showContactButton{set; get;}
    public boolean hasNext{get;set;}
    public boolean hasPrevious{get;set;}
    public String requestedPage {get;set;}
    
    public integer totalResults {set; get;}
    public Boolean isInConsole{get;set;}
    public Integer totalPage {set; get;}
    
    public string selectedContactId{set;get;}
    
    private static final Integer DEFAULT_RESULTS_PER_PAGE = 20;  
    private static final string SEARCH_TYPE = ' and ';
    private static final string DEFAULT_SORT_ORDER = ' ASC ';
    private static final string DEFAULT_SORT_FIELD = 'Name';
    
    public ApexPages.StandardSetController contactResults;
    
    //Constructor
    public SmartContactSearchExtention(ApexPages.StandardController controller) {
        resetSearchStatus();
    }

    //Constructor
    public SmartContactSearchExtention(){
        resetSearchStatus();
    }
    
    //set to default status of page
    public void resetSearchStatus(){
        //Reset Contact fields
        showContactButton = false;
        contacts = new List<Contact>();
        searchCount = 0;
        searchStatus = '';
        sortOrder = DEFAULT_SORT_ORDER;
        sortField = DEFAULT_SORT_FIELD;
        previousSortField = DEFAULT_SORT_FIELD;
        contactFirstNameToSearch = '';
        contactLastNameToSearch = '';
        contactEmail = '';
        contactPhone = ApexPages.CurrentPage().getParameters().get('phone') <> null ? ApexPages.CurrentPage().getParameters().get('phone') : '';
        contactEmail = ApexPages.CurrentPage().getParameters().get('email') <> null ? ApexPages.CurrentPage().getParameters().get('email') : '';
        contactLanguage = ApexPages.CurrentPage().getParameters().get('language') <> null ? ApexPages.CurrentPage().getParameters().get('language') : '';
        System.debug('>>>>>>>>contactPhone='+contactPhone);
        System.debug('>>>>>>>>contactEmail='+contactEmail);        
        System.debug('>>>>>>>>contactLanguage='+contactLanguage);        
        isAsc = true;
        hasPrevious = false;
        hasNext = false; 
       
    }
    
    public List<Contact> contacts {
        get{
            return contacts;
        }set;
    }
    
    public static String stripNonNumbers(String phone){
        
        String newPhone;
        
        newPhone=phone.replaceAll('[^\\d]', '');
        
        return newPhone;
        
        
    }  
    
    public PageReference cancel(){
        PageReference pg = new Pagereference('/console');
        return pg;  
    }
    
    public String findSearchCondition(String query, String cntFirstName, String cntLastName, String cntEmail, String cntPhone){
     
     if(cntFirstName != null && cntFirstName != ''){
          if(query.toUpperCase().contains('WHERE')){
            query += ' and FirstName like :cntFirstName ';
          }else{
            query += ' where FirstName like :cntFirstName ';
          }
      }
      if(cntLastName != null && cntLastName != ''){
          if(query.toUpperCase().contains('WHERE')){
            query += ' AND LastName like :cntLastName ';
          }else{
            query += ' where LastName like :cntLastName ';
          }
      }
      if(cntEmail != null && cntEmail != ''){
          if(query.toUpperCase().contains('WHERE')){
            query += ' and (Email like :cntEmail)';
                     
          }else{
            query += ' where (Email like :cntEmail)';
          }
      }
      if(cntPhone != null && cntPhone != ''){
        
        //cntPhone = stripNonNumbers(cntPhone);     
        
          if(query.toUpperCase().contains('WHERE')){
            query += ' AND (Phone_Unformatted__c like :cntPhone)';
          }else{
            query += ' where Phone_Unformatted__c like :cntPhone'; 
          }
      }
      system.debug('query======'+query );
      
    return query;
  }
  
  
    
    
    public void performSearch() {
        searchContact();
    }
    
    //method to search Contact and make list according to pagesize
    private void searchContact(){
        
        showContactButton = true;
        String cntFirstName = '%' + contactFirstNameToSearch.replace('*','').trim() + '%';
        String cntLastName = '%' + contactLastNameToSearch.replace('*','').trim() + '%';
        String cntEmail= '%' + contactEmail.replace('*','').trim() + '%';
        String cntPhone= '%' + contactPhone.replace('*','').trim() + '%';
        cntPhone=stripNonNumbers(cntPhone);
        
        System.debug('=========cntFirstName=' + cntFirstName);
        System.debug('=========cntLastName=' + cntLastName);
        System.debug('=========cntEmail=' + cntEmail);
        System.debug('=========cntPhone=' + cntPhone);
        
        
        query = 'Select Name, Phone, LastName, FirstName, Email, Phone_Unformatted__c From Contact';
    
        query = findSearchCondition(query, cntFirstName, cntLastName, cntEmail, cntPhone);
        
        System.debug('>>>>>>>> QUERY = ' + query);
        
        query += ' order by ' + sortField + sortOrder + ' nulls last'  ;
          
        try{
            contacts = new List<Contact>();
            contactResults = new ApexPages.StandardSetController(Database.query(query));
            contactResults.setPageSize(DEFAULT_RESULTS_PER_PAGE);
            contacts = contactResults.getRecords();
            searchCount = contactResults.getResultSize();
        }catch(Exception e){
            searchCount = 0;
        }  
        if (searchCount  == 0){
            searchStatus = 'No records to display.';
        }
        requestedPage = String.valueOf(contactResults.getPageNumber());
        showingFrom = 1;
        
        
        totalResults = 0;
        for (List<Sobject> recordBatch:Database.query(query))  {
             totalResults = totalResults + recordBatch.size();
         }
        totalPage = 0;
        totalPage = totalResults / contactResults.getPageSize() ; 
        if (totalPage * contactResults.getPageSize() < totalResults){
          totalPage++;
        }
        
        
        
        if(searchCount < contactResults.getPageSize()) {
            showingTo = searchCount;
        } else {
            showingTo = contactResults.getPageSize();
        }
        if(contactResults.getHasNext()) {
            hasNext = true;
        } else {
            hasNext = false;
        }
        hasPrevious = false;
    }
    
    
    public PageReference nextContactPage(){
        
        if(contactResults.getHasNext()) {
            contacts = new List<Contact>();
            contactResults.next();
            contacts = contactResults.getRecords();
            showingFrom = showingFrom + contactResults.getPageSize();
            showingTo =  showingTo + contacts.size();
            if(contactResults.getHasNext()) {
                hasNext = true;
            } else {
                hasNext = false;
            }
            hasPrevious = true; 
        }
        requestedPage = String.valueOf(contactResults.getPageNumber());
        return null;
    }
    
   
  
    public PageReference previousContactPage(){
        if(contactResults.getHasPrevious()) {
            showingTo =  showingTo - contacts.size();
            contacts = new List<Contact>();
            contactResults.previous();
            contacts = contactResults.getRecords();
            showingFrom = showingFrom - contactResults.getPageSize();
            hasNext = true;
            if(contactResults.getHasPrevious()) {
                hasPrevious = true;
            } else {
                hasPrevious = false;
            }
        }
        requestedPage = String.valueOf(contactResults.getPageNumber());  
        return null;
    }
    
   
  
    public PageReference requestedContactPage(){
        
        boolean check = pattern.matches('[0-9]+',requestedPage); 
        Integer pageNo = check? Integer.valueOf(requestedPage) : 0;
        if(pageNo == 0 || pageNo > totalPage){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Invalid page number.')); 
            return null;       
        }  
        contactResults.setPageNumber(pageNo);
        contacts = contactResults.getRecords();
        if(contactResults.getHasPrevious()) {
            hasPrevious = true;
         } else {
            hasPrevious = false;
         }
         if(contactResults.getHasNext()) {
            hasNext = true;
        } else {
            hasNext = false;
        }
        showingFrom  = (pageNo - 1) * contactResults.getPageSize() + 1;
       
        showingTo = showingFrom + contacts.size() - 1;
        if(showingTo > totalResults) {
            showingTo = totalResults;
        }
        return null;
    }
    
    
  
    //used to sort
    public void sortData(){
        if (previousSortField.equals(sortField)){
          isAsc = !isAsc;  
        }else{
            isAsc = true;
        }   
        sortOrder = isAsc ? ' ASC ' : ' DESC ';
        previousSortField = sortField;
        searchContact();
    }
    
    public PageReference redirectToContact() {
        
        String appId;
        String appName = Label.Service_Console_Name;
        
        System.debug('>>>>>>>>isInConsole='+isInConsole);
        
        for(AppMenuItem app : [select Id from AppMenuItem where Name = :appName]) {
            appId = app.Id;
        } 
        
        if(selectedContactId!=null && contactLanguage!=null && contactLanguage!=''){
            Contact selectedContact = new Contact(Id=selectedContactId);
            selectedContact.Preferred_Language__c = contactLanguage;
            update selectedContact;
        }
        
        system.debug('<<<<<<<<<<Is In Console>>>>>>>>'+isInConsole);
        
        if(isInConsole) {
          //PageReference pg = new PageReference('/' + selectedContactId);
          //pg.setRedirect(true);
          //return pg;  
          //LM 11/06/2014 : Added to fix Green Button issues
            PageReference contactPage = new PageReference('/apex/ConsumerSearchRedirect'); 
            contactPage.getParameters().put('contactId',selectedContactId);  
            contactPage.setRedirect(true); 
            return contactPage;  
        } else {
          PageReference pg = new PageReference('/console?tsid=' + appId);
          pg.setAnchor('%2F' + selectedContactId);
          //pg.setAnchor(selectedContactId.substring(0,15));
          pg.setRedirect(true);
          return pg;
        }
    }
    
    public PageReference createNewcontact() {
        
        PageReference returnPage = new PageReference('/apex/NewContact');
        
        if(contactFirstNameToSearch <> null && contactFirstNameToSearch <> '') {
          returnPage.getParameters().put('FirstName', contactFirstNameToSearch);
        }
        if(contactLastNameToSearch <> null && contactLastNameToSearch <> '') {
          returnPage.getParameters().put('LastName', contactLastNameToSearch);
        }
        if(contactPhone <> null && contactPhone <> '') {
          returnPage.getParameters().put('Phone', contactPhone);
        }
        if(contactEmail <> null && contactEmail <> '') {
          returnPage.getParameters().put('Email', contactEmail);
        }
        if(contactLanguage <> null && contactLanguage <> '') {
          returnPage.getParameters().put('Language', contactLanguage);
        }       
        
        return returnPage;
    }
    
   

}