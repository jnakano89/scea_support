/*
    @Author: Venkat Reddy
    @Version: 1.0
    @Description: Controller for complete article detail page functionality
*/
public class SCEA_ArticleDetailController{
    public ResultSet articleDetail{get;set;}
    //properties for Yes feedback
    public Boolean helpedMeFlag{set;get;}
    public Boolean answeredMyQuestionFlag{set;get;}
    public Boolean answeredMyQuestionButFlag{set;get;}
    public Boolean didntHaveInfoFlag{set;get;}
    public String feedbackComments{set;get;}
    //properties for Yes feedback
    public Boolean helpedMeNFlag{set;get;}
    public Boolean answeredMyQuestionNFlag{set;get;}
    public Boolean answeredMyQuestionButNFlag{set;get;}
    public Boolean didntHaveInfoNFlag{set;get;}
    public String noFeedbackComments{set;get;}
    //property to hold count of feedback type
    public Integer yesFeedbackCount{set;get;}
    public Integer noFeedbackCount{set;get;}
    //property to hold KnowledgeArticle record of respective article
    private KnowledgeArticleVersion kavOfArticle;
    //property to hold helpful articles
    public List<ResultSet> helpfulArticleList{set;get;}
    //property to hold values for Need More Help section
    public String selectedProduct{set;get;}
    public String selectedIssue{set;get;}
    //to hold lang parameter value
    public String selectedLanguage{set;get;}

    private Integer helpfulArticlesLimit = Integer.valueOf(Label.Helpful_Articles_Limit);
    static Integer articleSummaryTrimLength = Integer.valueOf(Label.Article_Summary_Trim_Length);

    static String decisionTreeArticleKeyPrefix = Decision_Tree__kav.sObjectType.getDescribe().getKeyPrefix();
    static String KCArticleKeyPrefix = KC_Article__kav.sObjectType.getDescribe().getKeyPrefix();
    /*
    public SCEA_ArticleDetailController(ApexPages.StandardController stdController){
        new SCEA_ArticleDetailController();
    }
    */
    public SCEA_ArticleDetailController(ApexPages.StandardController stdController){
        String articleId = stdController.getRecord().Id;//ApexPages.currentPage().getParameters().get('kavId');

        system.debug('--- articleId ---'+articleId+'---'+ApexPages.currentPage().getURL());
        //to accees lang parameter value from URL
        //incase no lang value in URL then make it en_US which is English as default
        if(ApexPages.currentPage().getParameters().get('lang') != null)
            selectedLanguage = ApexPages.currentPage().getParameters().get('lang');

        system.debug('--- Site.getBaseURL() ---'+Site.getBaseURL());
        if(Site.getBaseURL() != null && (selectedLanguage == null || selectedLanguage == '')){
            for(SCEA_LanguageSpecificSites__c langSite : SCEA_LanguageSpecificSites__c.getAll().values()){
                if(Site.getBaseURL().containsIgnoreCase(langSite.URL_Identifier_Keyword__c)){
                    selectedLanguage = langSite.Name;
                    break;
                }
            }
        }

        if(selectedLanguage == null || selectedLanguage == '')
            selectedLanguage = 'en_US';

        system.debug('--- selectedLanguage ---'+selectedLanguage);

        if(articleId != null){
            List<sObject> sObjList = new List<sObject>();
            List<sObject> articleTypeDataCatergorySelectionList = new List<sObject>();
            String dynamicQuery = 'select id from knowledgearticleversion where language=:selectedLanguage AND PublishStatus=\'Online\' AND KnowledgeArticleId=:articleId limit 1';
            articleId = Database.query(dynamicQuery).get(0).Id;

            if(articleId.startsWith(decisionTreeArticleKeyPrefix)){
                sObjList = [select Id, ArticleNumber, Title, Flow_Name__c, LastPublishedDate, ArticleType, UrlName, MasterVersionId from Decision_Tree__kav where Id=:articleId];
                if(sObjList != null && !sObjList.isEmpty()){
                    String masterArticleId = String.valueOf(sObjList[0].get('MasterVersionId'));
                    List<sObject> decisiontreeDCList = [select ParentId, DataCategoryGroupName, DataCategoryName from Decision_Tree__DataCategorySelection where ParentId=:masterArticleId];
                    articleTypeDataCatergorySelectionList.addAll(decisiontreeDCList);
                }
            }else if(articleId.startsWith(KCArticleKeyPrefix)){
                sObjList = [select Id, ArticleNumber, Title, Body__c , LastPublishedDate, ArticleType, UrlName, MasterVersionId from KC_Article__kav where Id=:articleId];
                if(sObjList != null && !sObjList.isEmpty()){
                    String masterArticleId = String.valueOf(sObjList[0].get('MasterVersionId'));
                    List<sObject> kcArticleDCList = [select ParentId, DataCategoryGroupName, DataCategoryName from KC_Article__DataCategorySelection where ParentId=:masterArticleId];
                    articleTypeDataCatergorySelectionList.addAll(kcArticleDCList);
                }
            }

            yesFeedbackCount = 0;
            noFeedbackCount = 0;

            if(sObjList != null && !sObjList.isEmpty()){
                articleDetail = new ResultSet(sObjList.get(0),selectedLanguage);
                List<Category__c> tempCategoryList = [select Channel__c, Category_API_Name__c from Category__c order by Display_Order__c asc];
                Map<String,String> categoryWiseChannelMap = new Map<String,String>();

                for(Category__c cat : tempCategoryList){
                    categoryWiseChannelMap.put(cat.Category_API_Name__c,cat.Channel__c);
                }

                String dataCategoryName = '';
                String channel = '';
                String categoriesForHelpfulArticles = '';

                for(sObject obj : articleTypeDataCatergorySelectionList){
                    if(dataCategoryName != '' && !dataCategoryName.contains(String.valueOf(obj.get('DataCategoryName')))){
                        dataCategoryName += ';'+String.valueOf(obj.get('DataCategoryName'));
                        categoriesForHelpfulArticles += ','+String.valueOf(obj.get('DataCategoryName'))+'__c';
                        if(categoryWiseChannelMap.containsKey(String.valueOf(obj.get('DataCategoryName'))+'__c'))
                            channel += ';'+categoryWiseChannelMap.get(String.valueOf(obj.get('DataCategoryName'))+'__c');
                        else
                            channel += ';'+'';
                    }
                    if(dataCategoryName == ''){
                        dataCategoryName = String.valueOf(obj.get('DataCategoryName'));
                        categoriesForHelpfulArticles = String.valueOf(obj.get('DataCategoryName'))+'__c';
                        if(categoryWiseChannelMap.containsKey(String.valueOf(obj.get('DataCategoryName'))+'__c'))
                            channel = categoryWiseChannelMap.get(String.valueOf(obj.get('DataCategoryName'))+'__c');
                        else
                            channel = '';
                    }
                }
                articleDetail.associatedDataCategories = dataCategoryName;
                articleDetail.associatedChannels = channel;
                system.debug('--- articleDetail.associatedDataCategories ---'+articleDetail.associatedDataCategories);
                system.debug('--- articleDetail.associatedChannels ---'+articleDetail.associatedChannels);

                kavOfArticle = [select Id, KnowledgeArticleId from KnowledgeArticleVersion where Id=:articleDetail.Id];
                //to fetch helpful articles
                helpfulArticleList = new List<ResultSet>();
                String searchedString = articleDetail.Title+'*';
                //SOSL operation to retrive records of all the article types with the given search string
                system.debug('--- categoriesForHelpfulArticles ---'+categoriesForHelpfulArticles);
                List<List<sObject>> searchList = null;
                String publishStatus = 'Online';
                String languageStr = 'en_US';
                String dynamicSOSLQuery = '';
                dynamicSOSLQuery += 'FIND :searchedString IN ALL FIELDS RETURNING ';
                dynamicSOSLQuery += 'Decision_Tree__kav (Id, ArticleNumber,Title,Summary,LastPublishedDate,Flow_Name__c,ArticleType, UrlName where PublishStatus=:publishStatus AND Language=:selectedLanguage),';
                dynamicSOSLQuery += 'KC_Article__kav (Id, ArticleNumber,Title,Summary,LastPublishedDate,Body__c,ArticleType, UrlName where PublishStatus=:publishStatus AND Language=:selectedLanguage) ';
                searchList = search.query(dynamicSOSLQuery);

                for(List<SObject> o:searchList){
                    for(SObject s:o){
                        system.debug('---'+articleDetail.Id+'---'+s.Id+'---'+kavOfArticle.KnowledgeArticleId);
                        if(articleDetail.ArticleNumber != s.get('ArticleNumber')+'')
                            helpfulArticleList.add(new ResultSet(s,selectedLanguage));
                        //if no of records in list is equals to records per page then break it nd show in search results
                        if(helpfulArticleList.size() == helpfulArticlesLimit){
                            break;
                        }
                    }
                    //if no of records in list is equals to records per page then break it nd show in search results
                    if(helpfulArticleList.size() == helpfulArticlesLimit){
                        break;
                    }
                }
            }else
                articleDetail = null;
        }else{
            articleDetail = null;
        }
    }

    public PageReference processYesFeedback(){
        PageReference pageRef = Page.SCEA_Article;
        SCEA_Article_Feedback__c articleFeedback = new SCEA_Article_Feedback__c();
        articleFeedback.Name = articleDetail.ArticleNumber;
        articleFeedback.Article_Id__c = articleDetail.Id;
        articleFeedback.Article_Title__c = articleDetail.Title;
        articleFeedback.Feedback_Type__c = 'Yes';
        articleFeedback.Feedback_Comments__c = feedbackComments;
        articleFeedback.Additional_Feedback_Information__c = '';
        if(helpedMeFlag != null && helpedMeFlag == true)
            articleFeedback.Additional_Feedback_Information__c += 'Helped me avoid contacting support'+';';
        if(answeredMyQuestionFlag != null && answeredMyQuestionFlag == true)
            articleFeedback.Additional_Feedback_Information__c += 'Answered my question'+';';
        if(answeredMyQuestionButFlag != null && answeredMyQuestionButFlag == true)
            articleFeedback.Additional_Feedback_Information__c += 'Answered my question, but don\'t like'+';';
        if(didntHaveInfoFlag != null && didntHaveInfoFlag == true)
            articleFeedback.Additional_Feedback_Information__c += 'Didn\'t have the information'+';';
        insert articleFeedback;
        yesFeedbackCount++;
        try{
            Vote kaVote = new Vote();
            kaVote.Type = 'Up';
            kaVote.ParentId = kavOfArticle.KnowledgeArticleId;
            insert kaVote;
        }catch(Exception e){
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,'You already had submitted feedback for this article'));
            return null;
        }
        pageRef.getParameters().put('kavTitle',articleDetail.Title);
        pageRef.getParameters().put('lang',selectedLanguage);
        return pageRef;
    }

    public PageReference processNoFeedback(){
        PageReference pageRef = Page.SCEA_Article;
        SCEA_Article_Feedback__c articleFeedback = new SCEA_Article_Feedback__c();
        articleFeedback.Name = articleDetail.ArticleNumber;
        articleFeedback.Article_Id__c = articleDetail.Id;
        articleFeedback.Article_Title__c = articleDetail.Title;
        articleFeedback.Feedback_Type__c = 'No';
        articleFeedback.Feedback_Comments__c = noFeedbackComments;
        articleFeedback.Additional_Feedback_Information__c = '';
        if(helpedMeNFlag != null && helpedMeNFlag == true)
            articleFeedback.Additional_Feedback_Information__c += 'Helped me avoid contacting support'+';';
        if(answeredMyQuestionNFlag != null && answeredMyQuestionNFlag == true)
            articleFeedback.Additional_Feedback_Information__c += 'Answered my question'+';';
        if(answeredMyQuestionButNFlag != null && answeredMyQuestionButNFlag == true)
            articleFeedback.Additional_Feedback_Information__c += 'Answered my question, but don\'t like'+';';
        if(didntHaveInfoNFlag != null && didntHaveInfoNFlag == true)
            articleFeedback.Additional_Feedback_Information__c += 'Didn\'t have the information'+';';
        insert articleFeedback;
        noFeedbackCount++;
        try{
            Vote kaVote = new Vote();
            kaVote.Type = 'Down';
            kaVote.ParentId = kavOfArticle.KnowledgeArticleId;
            insert kaVote;
        }catch(Exception e){
            return null;
        }
        pageRef.getParameters().put('kavTitle',articleDetail.Title);
        pageRef.getParameters().put('lang',selectedLanguage);
        return pageRef;
    }

    public List<SelectOption> getProductCategoryOptions(){
        List<SelectOption> returnList = new List<SelectOption>();
        system.debug('--- selectedLanguage ---'+selectedLanguage);

        if(selectedLanguage == null){
            if(ApexPages.currentPage().getParameters().get('lang') != null)
                selectedLanguage = ApexPages.currentPage().getParameters().get('lang');

            if(Site.getBaseURL() != null && (selectedLanguage == null || selectedLanguage == '')){
                for(SCEA_LanguageSpecificSites__c langSite : SCEA_LanguageSpecificSites__c.getAll().values()){
                    if(Site.getBaseURL().containsIgnoreCase(langSite.URL_Identifier_Keyword__c)){
                        selectedLanguage = langSite.Name;
                        break;
                    }
                }
            }

            if(selectedLanguage == null || selectedLanguage == '')
                selectedLanguage = 'en_US';
        }

        for(Category__c cat : [select Name, Category_API_Name__c, es_Name__c, pt_BR_Name__c from Category__c where Display_in_Product__c=true order by Display_Order__c asc]){
            if(selectedLanguage == 'en_US'){
                returnList.add(new SelectOption(cat.Category_API_Name__c,cat.Name));
            }else{
                returnList.add(new SelectOption(cat.Category_API_Name__c,String.valueOf(cat.get(selectedLanguage+'_Name__c'))));
            }
        }
        return returnList;
    }

     public PageReference searchAgainForArticles(){
        System.debug('searchAgainForArticles() entered');
        PageReference pageRef = Page.scea_searchresults;
        pageRef.getParameters().put('guideStr',selectedIssue);
        pageRef.getParameters().put('guideProdStr',selectedProduct);
        pageRef.getParameters().put('lang',selectedLanguage);
        pageRef.setRedirect(true);
        System.debug('searchAgainForArticles() returning: ' + pageRef.getUrl());
        return pageRef;
    }

    public class ResultSet{
        public String Id {get;set;}
        public String ArticleNumber{get;set;}
        public String Title{get;set;}
        public String sObjectName {get;set;}
        public String overviewSummary{set;get;}
        public String helpfulArticleOverviewSummary{set;get;}
        public String publishedDate{get;set;}
        public String associatedDataCategories{set;get;}
        public String associatedChannels{set;get;}
        public String dynamicFlowPage{set;get;}
        public String articleDetailURL{set;get;}

        public ResultSet(sObject s, String tempSelectedLanguage){
            this.Id = s.Id;
            this.ArticleNumber = s.get('ArticleNumber')+'';
            this.Title = s.get('Title')+'';
            this.publishedDate = Date.valueOf(s.get('LastPublishedDate')).format();
            this.dynamicFlowPage = 'SCEA_Landing';

            if(String.valueOf(s.Id).startsWith(decisionTreeArticleKeyPrefix)){
                this.overviewSummary = s.get('Flow_Name__c')+'';
                this.sObjectName = 'Decision Tree';
                String flowName = (String)s.get('Flow_Name__c');
                if(DecisionTreeArticleTypePageSettings__c.getAll().get(flowName) == null)
                {
                    this.dynamicFlowPage = null;
                }
                this.dynamicFlowPage =  ((DecisionTreeArticleTypePageSettings__c)DecisionTreeArticleTypePageSettings__c.getAll().get(flowName)).ExternalPageName__c;
            }else if(String.valueOf(s.Id).startsWith(KCArticleKeyPrefix)){
                this.overviewSummary = s.get('Body__c')+'';
                this.sObjectName = 'KC Article';
            }

            this.helpfulArticleOverviewSummary = this.overviewSummary;

            this.helpfulArticleOverviewSummary = this.helpfulArticleOverviewSummary.replaceAll('<br/>','\n');
            this.helpfulArticleOverviewSummary = this.helpfulArticleOverviewSummary.replaceAll('<br />','\n');
            String htmlTagPattern = '<.*?>';
            Pattern patternTemp = Pattern.compile(htmlTagPattern);
            Matcher matcherTemp = patternTemp.matcher(this.helpfulArticleOverviewSummary);
            this.helpfulArticleOverviewSummary = matcherTemp.replaceAll('');

            if(this.helpfulArticleOverviewSummary.length()>articleSummaryTrimLength)
                this.helpfulArticleOverviewSummary = this.helpfulArticleOverviewSummary.substring(0,(articleSummaryTrimLength-2))+'...';

            if(Site.getBaseURL() != null){
                articleDetailURL = Site.getBaseURL()+'/articles/'+tempSelectedLanguage+'/'+String.valueOf(s.get('ArticleType')).replace('__kav','')+'/'+String.valueOf(s.get('UrlName'));
            }
        }
    }
}