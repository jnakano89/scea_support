/**
 * 
 */
@isTest
private class Test_GeneralUtiltyClass {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        list<Asset__c> assets =  TestClassUtility.createAssets(1, true);
        
        list<Product__c> products = TestClassUtility.createProduct(1, false);
        products[0].Product_Type__c ='1st Party Peripheral';
        insert products;
      
        list<Contact> cntList = TestClassUtility.createContact(1, true);
        
        list<Address__c> addresses = TestClassUtility.createAddress(1, true);
        
        list<Order__c> orders = TestClassUtility.creatOrder(cntList[0].Id, 2, false);
        orders[0].Order_Status__c = 'Open';
        orders[0].Order_Type__c = 'Promotional';
        insert orders;
        
        Case caseTest = TestClassUtility.createCase('New', 'Test', false);
        caseTest.ContactId = cntList[0].Id;
       	caseTest.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service').getRecordTypeId();        
        insert caseTest;
        
        Case newCaseTest = TestClassUtility.createCase('Ready for Service', 'Test Case', false);
        newCaseTest.ContactId = cntList[0].Id;
       	newCaseTest.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service').getRecordTypeId();  
       	newCaseTest.Ship_To__c = addresses[0].Id;
       	newCaseTest.Bill_To__c = addresses[0].Id;
       	newCaseTest.SCEA_Product__r = products[0];
       	newCaseTest.Fee_Type__c = 'CR 90';
        insert newCaseTest;
        
        Staging_Service_Outbound__c sso = GeneralUtiltyClass.createStagingServiceOutbound(caseTest, cntList[0], products[0], assets[0], addresses[0], null);
        System.assert(sso != null);
        System.assert(GeneralUtiltyClass.isServiceOrderValid(newCaseTest, caseTest));
        
        System.assert(!GeneralUtiltyClass.isASBValid(newCaseTest, caseTest));
        
        System.assert(!GeneralUtiltyClass.isPromoValid(newCaseTest, caseTest));
        
        Set<Id> caseIds = new Set<Id>();
        caseIds.add(newCaseTest.Id);
        caseIds.add(caseTest.Id);
        System.assert(GeneralUtiltyClass.getCaseDetails(caseIds)!=null);
        System.assert(GeneralUtiltyClass.isValidPromoOrder(orders[0] , orders[1], newCaseTest));
        Test.startTest();
        String webRequestResult = GeneralUtiltyClass.validatePromoCase(newCaseTest.Id);
        System.assert(webRequestResult.equals('Successfully created Promotional Service case.'));
        Test.stopTest();
        
        System.assert(GeneralUtiltyClass.getNewConsumer('firstName', 'lastName', 'test@user.com')!=null);
        
        System.assert(GeneralUtiltyClass.getNewPSNAccount('name', 'testId', 'test')!=null);
        
    }
}