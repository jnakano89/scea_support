global class HTTPMockCyberSource implements HttpCalloutMock  {
     global HTTPResponse respond(HTTPRequest req) {
          //System.assertEquals('https://ebctest.cybersource.com/ebctest/Query', req.getEndpoint());
          //System.assertEquals('POST', req.getMethod());
          HttpResponse res = new HttpResponse();
          //res.setHeader('Content-Type', 'application/json');
          String body = getBody();
          res.setBody(body);
          res.setStatusCode(200);
          return res;
     }
     String getBody() {
      String body = '<Report xmlns="https://ebctest.cybersource.com/ebctest/reports/dtd/tdr_1_9.dtd" Name="Transaction Detail" Version="1.9" MerchantID="scea_esptest" ReportStartDate="2013-09-18 12:35:23.902-08:00" ReportEndDate="2013-09-18 12:35:23.902-08:00">' + 
    '<Requests>' + 
    '<Request MerchantReferenceNumber="OrderDev - 000300" RequestDate="2013-09-17T06:34:51-08:00" RequestID="3794248915050178147626" SubscriptionID="" Source="Secure Acceptance Web/Mobile" TransactionReferenceNumber="5067937032">' + 
    '<ShipTo>' + 
    '<FirstName> SABRINA</FirstName>' + 
    '<LastName> CONSUMER</LastName>' + 
    '<Address1> 123 B St</Address1>' + 
    '<City>San Mateo</City>' + 
    '<State>CA</State>' + 
    '<Zip>94402</Zip>' + 
    '<Email> martha@appirio.com</Email>' + 
    '<Country> US</Country>' + 
    '<Phone/>' + 
    '</ShipTo>' + 
    '<PaymentMethod>' + 
    '<Card>' + 
    '<AccountSuffix> 1111</AccountSuffix>' + 
    '<ExpirationMonth> 01</ExpirationMonth>' + 
    '<ExpirationYear> 2015</ExpirationYear>' + 
    '<CardType> Visa</CardType>' + 
    '</Card>' + 
    '</PaymentMethod>' + 
    '<LineItems>' + 
    '<LineItem Number="0">' + 
    '<FulfillmentType/>' + 
    '<Quantity> 1</Quantity>' + 
    '<UnitPrice> 59.99</UnitPrice>' + 
    '<TaxAmount> 0.00</TaxAmount>' + 
    '<ProductCode> default</ProductCode>' + 
    '</LineItem>' + 
    '</LineItems>' + 
    '<ApplicationReplies>' + 
    '<ApplicationReply Name="ics_auth">' + 
    '<RCode> 1</RCode>' + 
    '<RFlag> SOK</RFlag>' + 
    '<RMsg> Request was processed successfully.</RMsg>' + 
    '</ApplicationReply>' + 
    '<ApplicationReply Name="ics_bill">' + 
    '<RCode> 1</RCode>' + 
    '<RFlag> SOK</RFlag>' + 
    '<RMsg> Request was processed successfully.</RMsg>' + 
    '</ApplicationReply>' + 
    '</ApplicationReplies>' + 
    '<PaymentData>' + 
    '<PaymentRequestID> 3794248915050178147626</PaymentRequestID>' + 
    '<PaymentProcessor> smartfdc</PaymentProcessor>' + 
    '<Amount> 59.99</Amount>' + 
    '<CurrencyCode> USD</CurrencyCode>' + 
    '<TotalTaxAmount> 0.00</TotalTaxAmount>' + 
    '<AuthorizationCode> 123456</AuthorizationCode>' + 
    '<AVSResult>  YYY</AVSResult>' + 
    '<AVSResultMapped> Y</AVSResultMapped>' + 
    '<EventType> TRANSMITTED</EventType>' + 
    '<RequestedAmount> 59.99</RequestedAmount>' + 
    '<RequestedAmountCurrencyCode> USD</RequestedAmountCurrencyCode>' + 
    '</PaymentData>' + 
    '</Request>' + 
    '</Requests>' + 
    '</Report>' ;
    return body; 
     }
    }