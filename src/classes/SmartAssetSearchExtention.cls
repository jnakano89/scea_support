//**************************************************************************/
// (c) 2013 Appirio, Inc.
//
// Description :Controller Class For VF PAge SmartAssetSearch.
// Original August 16,2013: Poonam Varyani(JDC)
//
// Updated :
//          August    16,2013 : KapiL Choudhary(JDC) : Update Code for Link Asset to Contact [T-173077]
//          September 05,2013 : KapiL Choudhary(JDC) : Update Code for SelectAsset VF Page [T-180244]
///**************************************************************************/

public with sharing class SmartAssetSearchExtention {
    
    //Search criteria fields
    public String assetSerialNumber {set;get;}
    
    public integer searchCount{set; get;}
    public string searchStatus{set; get;}
    public string sortField{set;get;}
    private string previousSortField;
    private string sortOrder;
    public string accountIdParam{get;set;}
    public string selectedAsset {get;set;}
      
    public boolean isAsc{set; get;}
    public Integer showingFrom{get;set;}
    public Integer showingTo{get;set;}
    public string query ;
    
    public boolean showAssetButton{set; get;}
    public boolean hasNext{get;set;}
    public boolean hasPrevious{get;set;}
    public boolean isInConsole{get;set;}
    public String requestedPage {get;set;}
    
    public integer totalResults {set; get;}
    
    public Integer totalPage {set; get;}
    
    private static final Integer DEFAULT_RESULTS_PER_PAGE = 20;  
    private static final string SEARCH_TYPE = ' and ';
    private static final string DEFAULT_SORT_ORDER = ' ASC ';
    private static final string DEFAULT_SORT_FIELD = 'Name';
    
    public  string caseIdParam{get;set;}
    public  string retIdParam{get;set;}
    
    public String isExchange {get;set;}
    public String oldAssetParam {get;set;}
    
    public ApexPages.StandardSetController assetResults;
    
    //Constructor
    public SmartAssetSearchExtention(ApexPages.StandardController controller) {
        
        accountIdParam = ApexPages.currentPage().getParameters().get('accountId');
        caseIdParam    = ApexPages.currentPage().getParameters().get('caseId');
        retIdParam    = ApexPages.currentPage().getParameters().get('retId');
        isExchange = Apexpages.currentPage().getParameters().get('exchange');
        oldAssetParam = Apexpages.currentPage().getParameters().get('oldAssetId');
        resetSearchStatus();
    }

    //Constructor
    public SmartAssetSearchExtention(){
        
        resetSearchStatus();
    }
    
    //set to default status of page
    public void resetSearchStatus(){
        //Reset asset fields
        showAssetButton = false;
        assets = new List<Asset__c>();
        searchCount = 0;
        searchStatus = '';
        sortOrder = DEFAULT_SORT_ORDER;
        sortField = DEFAULT_SORT_FIELD;
        previousSortField = DEFAULT_SORT_FIELD;
        assetSerialNumber = '';
        isAsc = true;
        hasPrevious = false;
        hasNext = false; 
       
    }
    
    public List<Asset__c> assets {
        get{
            return assets;
        }set;
    }

    
    public String findSearchCondition(String query, String serialNumber){
 
     if(serialNumber != null && serialNumber != ''){
          if(query.toUpperCase().contains('WHERE')){
            query += ' and Serial_Number__c like :serialNumber ';
          }else{
            query += ' where Serial_Number__c like :serialNumber ';
          }
      }
      system.debug('query======'+query );
    return query;
  }
  
    // Insert a Record to link between Consumer(Contact) and Asset__c.
    private void insertJunctionRecord(Id contactId){
        list<Consumer_Asset__c> currentConnectionList = [select id from Consumer_Asset__c where Consumer__c =:contactId and Asset__c =:selectedAsset];
                
                if(currentConnectionList.isEmpty()){//No Record Exists for this consumer and this asset.
                      try{
                        Consumer_Asset__c conAsset = new Consumer_Asset__c(Consumer__c = contactId , Asset__c = selectedAsset);
                        insert conAsset;
                      }
                      catch(Exception ex){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
                      }
                }
    }
    
   public PageReference createConsumerAsset(){
     
      String appId;
      String appName = Label.Service_Console_Name;
      Asset__c newAsset;
      
      for(AppMenuItem app : [select Id from AppMenuItem where Name = :appName]) {
        appId = app.Id;
      } 
      
      try{
        //Asset exchange process
        if(isExchange == '1'){
            Case csObj = [select Id,Asset__c, Order__c from Case where Id = :caseIdParam];
            if(selectedAsset == csObj.Asset__c) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'This asset is already selected.'));
                return null;
             }
            Asset__c oldAsset = [Select Genre__c, Sub_Genre__c, Model_Number__c, Model_Type__c, Party_Type__c, Platform__c, Id,Purchase_Date__c, Product__c, PPP_Product__c, PPP_Purchase_Date__c, PPP_Contract_Number__c, PPP_Start_Date__c, 
                                                   Serial_Number__c, Asset_Status__c, PPP_End_Date__c,  PPP_Status__c, PPP_Claim_Reason__c, PPP_Last_Update_Date__c, PPP_Service_Level__c,PPP_Refund_Eligible__c,
                                                   Coverage_Type__c, Warranty_Details__c
                                From Asset__c 
                                Where Id = :csObj.Asset__c];
          
             Asset__c selectedAssetRecord = [Select Serial_Number__c, Genre__c, Sub_Genre__c, Model_Number__c, Model_Type__c, Party_Type__c, Platform__c, Id,Purchase_Date__c, Product__c, PPP_Product__c, PPP_Purchase_Date__c, PPP_Contract_Number__c, PPP_Start_Date__c, 
                                                   Asset_Status__c, PPP_End_Date__c,  PPP_Status__c, PPP_Claim_Reason__c, PPP_Last_Update_Date__c, PPP_Service_Level__c,
                                                   Coverage_Type__c, Warranty_Details__c
                                From Asset__c 
                                Where Id = :selectedAsset];
                                
                selectedAssetRecord.PPP_Status__c = 'Inactive';
                selectedAssetRecord.Asset_Status__c = 'Inactive';
             
             newAsset=selectedAssetRecord.clone(false, true);
             
             newAsset.Asset_Status__c='Active';
             newAsset.PPP_Claim_Reason__c = oldAsset.PPP_Claim_Reason__c;
             newAsset.PPP_Contract_Number__c = oldAsset.PPP_Contract_Number__c;
             newAsset.PPP_End_Date__c = oldAsset.PPP_End_Date__c;
             newAsset.PPP_Last_Update_Date__c = oldAsset.PPP_Last_Update_Date__c;
             newAsset.Product__c = oldAsset.Product__c;
             newAsset.PPP_Product__c = oldAsset.PPP_Product__c;            
             newAsset.PPP_Purchase_Date__c = oldAsset.PPP_Purchase_Date__c;
             newAsset.PPP_Service_Level__c = oldAsset.PPP_Service_Level__c;            
             newAsset.PPP_Start_Date__c = oldAsset.PPP_Start_Date__c;
             newAsset.PPP_Status__c = oldAsset.PPP_Status__c;
             newAsset.Purchase_Date__c = oldAsset.Purchase_Date__c;
             newAsset.Last_Repair_Date__c = Date.today();
             newAsset.PPP_Refund_Eligible__c=oldAsset.PPP_Refund_Eligible__c;
                     
                     
           /*Asset__c newAsset = new Asset__c(
                                            Id = selectedAsset,
                                            PPP_Claim_Reason__c = oldAsset.PPP_Claim_Reason__c,
                                            PPP_Contract_Number__c = oldAsset.PPP_Contract_Number__c,
                                            PPP_End_Date__c = oldAsset.PPP_End_Date__c,
                                            PPP_Last_Update_Date__c = oldAsset.PPP_Last_Update_Date__c,
                                            Product__c = oldAsset.Product__c,
                                            PPP_Product__c = oldAsset.PPP_Product__c,            
                                            PPP_Purchase_Date__c = oldAsset.PPP_Purchase_Date__c,
                                            PPP_Service_Level__c = oldAsset.PPP_Service_Level__c,            
                                            PPP_Start_Date__c = oldAsset.PPP_Start_Date__c,
                                            PPP_Status__c = oldAsset.PPP_Status__c,
                                            Purchase_Date__c = oldAsset.Purchase_Date__c,
                                            Last_Repair_Date__c = Date.today()); */
                oldAsset.PPP_Status__c = 'Inactive';
                oldAsset.Asset_Status__c = 'Inactive';
             
             List<Asset__c> updateAssetList = new List<Asset__c>{oldAsset, selectedAssetRecord};
             update updateAssetList;
             
             insert newAsset;
             
             list<Order__c> orderList = new list<order__c>();
             for(Order__c o : [select Asset__c from Order__c where Id = :csObj.Order__c]) {
                o.Asset__c = newAsset.Id;
                orderList.add(o);
             }
             
            if(orderList.isEmpty()) {
                    System.debug('>>>>>>>>>>>Old Asset Serial Number<<<<<<<<<<<<<<< ' + oldAsset.Serial_Number__c);
            
            for(Order__c o : [select Asset__c, Asset__r.Serial_Number__c, ESP_Record_Type__c, Asset__r.PPP_Status__c from Order__c where Asset__r.Serial_Number__c=:oldAsset.Serial_Number__c AND (ESP_Record_Type__c='A' OR ESP_Record_Type__c='U') AND Asset__r.PPP_Status__c='Inactive' limit 1]) {
                o.Asset__c = newAsset.Id;
                orderList.add(o);
             }
            
            }
            
            
             if(!orderList.isEmpty()) update orderList;
              
            
        }
        if(accountIdParam != null && accountIdParam !=''){
          PageReference returnPage ;
          list<Contact> contactList = [Select Id From Contact where AccountId=:accountIdParam];
          system.debug('contactList??? ' +contactList);
          if(!contactList.isEmpty()){
            insertJunctionRecord(contactList[0].Id);
          }
          if(caseIdParam != null && caseIdParam != ''){
            Case updateCase  = new Case(id=caseIdParam);
            if(isExchange == '1') {
            	updateCase.Asset__c = newAsset.Id;
            } else {
            	updateCase.Asset__c = selectedAsset;
            }
            try{
              update updateCase;
            } catch(Exception ex) {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            }
            //if(isInConsole) {
                returnPage= new ApexPages.StandardController(updateCase).view();
            /*} else {
                returnPage = new Pagereference('/console?tsid=' + appId);
                returnPage.setAnchor('%2F' + caseIdParam);  
            }*/
            
          } else {
            //if(isInConsole) {
                returnPage= new ApexPages.StandardController(new Account(id=accountIdParam)).view();
            /*} else {
                returnPage = new Pagereference('/console?tsid=' + appId);
                returnPage.setAnchor('%2F' + accountIdParam);
            }*/
          }
        returnPage.setRedirect(true);
        return returnPage;
      }
      return null;
    } catch(Exception ex) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
    }
    return null;
  
  }
    
    public void performSearch(){
        searchAsset();
    }
    
    //method to search Contact and make list according to pagesize
    private void searchAsset(){
        
        showAssetButton = true;
        String serialNumber = '%' + assetSerialNumber.replace('*','') + '%';
        query = 'Select Console_Type__c, Serial_Number__c, Purchase_Date__c, Registration_Date__c, Model_Number__c, Siras_Purchase_Date__c From Asset__c ';
        query = findSearchCondition(query, serialNumber);
        System.debug('QUERY+++++++++:' + query);
        query += ' order by ' + sortField + ' ' +  sortOrder + ' nulls last'  ;
          
        try{
            assets = new List<Asset__c>();
            assetResults = new ApexPages.StandardSetController(Database.query(query));
            assetResults.setPageSize(DEFAULT_RESULTS_PER_PAGE);
            assets = assetResults.getRecords();
            searchCount = assetResults.getResultSize();
        }catch(Exception e){
            searchCount = 0;
        }  
        if (searchCount  == 0){
            searchStatus = 'No records to display.';
        }
        else if(isExchange == '1') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'ASSET ALREADY IN USE.'));
            showAssetButton = false;
        }
        requestedPage = String.valueOf(assetResults.getPageNumber());
        showingFrom = 1;
        
        
        totalResults = 0;
        for (List<Sobject> recordBatch:Database.query(query))  {
             totalResults = totalResults + recordBatch.size();
         }
        totalPage = 0;
        totalPage = totalResults / assetResults.getPageSize() ; 
        if (totalPage * assetResults.getPageSize() < totalResults){
          totalPage++;
        }
        
        if(searchCount < assetResults.getPageSize()) {
            showingTo = searchCount;
        } else {
            showingTo = assetResults.getPageSize();
        }
        if(assetResults.getHasNext()) {
            hasNext = true;
        } else {
            hasNext = false;
        }
        hasPrevious = false;
    }
    
    
    public PageReference nextAssetPage(){
        
        if(assetResults.getHasNext()) {
            assets = new List<Asset__c>();
            assetResults.next();
            assets = assetResults.getRecords();
            showingFrom = showingFrom + assetResults.getPageSize();
            showingTo =  showingTo + assets.size();
            if(assetResults.getHasNext()) {
                hasNext = true;
            } else {
                hasNext = false;
            }
            hasPrevious = true; 
        }
        requestedPage = String.valueOf(assetResults.getPageNumber());
        return null;
    }
    
   
  
    public PageReference previousAssetPage(){
        if(assetResults.getHasPrevious()) {
            showingTo =  showingTo - assets.size();
            assets = new List<Asset__c>();
            assetResults.previous();
            assets = assetResults.getRecords();
            showingFrom = showingFrom - assetResults.getPageSize();
            hasNext = true;
            if(assetResults.getHasPrevious()) {
                hasPrevious = true;
            } else {
                hasPrevious = false;
            }
        }
        requestedPage = String.valueOf(assetResults.getPageNumber());  
        return null;
    }
    
   
  
    public PageReference requestedAssetPage(){
        
        boolean check = pattern.matches('[0-9]+',requestedPage); 
        Integer pageNo = check? Integer.valueOf(requestedPage) : 0;
        if(pageNo == 0 || pageNo > totalPage){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Invalid page number.')); 
            return null;       
        }  
        assetResults.setPageNumber(pageNo);
        assets = assetResults.getRecords();
        if(assetResults.getHasPrevious()) {
            hasPrevious = true;
         } else {
            hasPrevious = false;
         }
         if(assetResults.getHasNext()) {
            hasNext = true;
        } else {
            hasNext = false;
        }
        showingFrom  = (pageNo - 1) * assetResults.getPageSize() + 1;
       
        showingTo = showingFrom + assets.size() - 1;
        if(showingTo > totalResults) {
            showingTo = totalResults;
        }
        return null;
    }
    
    
  
    //used to sort
    public void sortData(){
        if (previousSortField.equals(sortField)){
          isAsc = !isAsc;  
        }else{
            isAsc = true;
        }   
        sortOrder = isAsc ? ' ASC ' : ' DESC ';
        previousSortField = sortField;
        searchAsset();
    }
    
    public PageReference cancel(){

        String appId;
        String appName = Label.Service_Console_Name;
            
        for(AppMenuItem app : [select Id from AppMenuItem where Name = :appName]) {
            appId = app.Id;
        }//end-for

        PageReference returnPage = new PageReference('/console?tsid='+ appId);
        
        if(caseIdParam != null && caseIdParam != ''){
            
            returnPage= new ApexPages.StandardController(new Case (Id=caseIdParam)).view();
            //returnPage.setAnchor('%2F' + caseIdParam);
        }
        else if(accountIdParam != null && accountIdParam != ''){
            returnPage= new ApexPages.StandardController(new Account (Id=accountIdParam)).view();
            //returnPage.setAnchor('%2F' + accountIdParam);
        }
        
        returnPage.setRedirect(true);
        return returnPage;



    }
}