//******************************************************************************************************************************************************
// CREATED BY : Leena Mandadapu 03/25/2015 : Jira# SMS-1080 - Web console Deactivation Requests
// EDITED BY: Joseph Lam 5/13/2015: Jira Ticket SMS-1515 - Brazil Console Deactivation Web Form
//********************************************************************************************************************************************************/
public with sharing class ConsoleDeactivationController {

    public Boolean requiredLabel { get; set; }
    public Boolean requiredFirstName {get;set;}
    public Boolean requiredLastName {get;set;}
    public Boolean requiredPSNOnlineId { get;set;}
    public Boolean requiredPSNSignInId {get;set;}
    public Boolean requiredSerialNumber {get;set;}
    public Boolean requiredModelType {get;set;}
    public Boolean requiredCountry {get;set;}
    public Boolean requiredEmail {get;set;}
    public Boolean duplicateRequest{get;set;}
    public Boolean invalidEmail{get;set;}
    public Boolean invalidPSNSignInId{get;set;}
    public Boolean invalidSerialNum{get;set;}
    public Boolean showAlert{get;set;}
    public Boolean generalError{get;set;}
    public Boolean MDMRecNotExists{get;set;}
    public Boolean isCDRequestSuccess{get;set;}
    public Boolean reqBlocked{get;set;}
    public Boolean ipBlocked{get;set;}
    public Boolean MDMRecordFound {get;set;}
    
    public Contact selectedContact {get;set;}
    public Account selectedAccount {get;set;}
    public Case selectedcase {get; set;}
    public Console_Deactivation_Requests__c logWebRequest {get;set;}

    public String language {get;set;}
    public String englishState {get;set;}
    public String SpanishState{get;set;}
    public String PortugueseState {get;set;}
    public String css {get;set;}
    public String FirstName {get;set;}
    public String LastName {get;set;}
    public String PersonEmail {get;set;}
    public String PSNOnlineId {get;set;}
    public String PSNSigninId {get;set;}
    public String SerialNumber {get;set;}
    public String SelectedModelType {get;set;}
    public String SelectedCountry {get;set;}
    public String ip {get;set;}
    public String url {get;set;}
    public String ipAddress{get;set;}
    public String SerialNumhelpTextVal{get;set;}
    
    
    //Case Record Type Id
    public static final String RT_PS_NETWORK_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('PS Network').getRecordTypeId();
       
 public ConsoleDeactivationController() {
             
    selectedAccount = new Account();
    selectedContact = new Contact();
    selectedcase = new Case();
        
    englishState = 'laON bol3';
    SpanishState= 'loff txt3';
    PortugueseState = 'loff txt3';
    css = 'CD.css';
    language = 'en';
    
    showAlert = false;
 }    
 
 public PageReference switchLanguage(){
    System.debug('<<<<<<<<<<<<<<<<< Language >>>>>>>>>>>>>>>>>>> ' + language);
    return null;
  }  
  
 public PageReference languageSwap(){
    
    if(ApexPages.currentPage().getParameters().get('language') == 'en') {
        englishState = 'laON bol3';
        SpanishState = 'loff txt3';
        PortugueseState = 'loff txt3';
        css = 'CD.css';
        language = 'en';
    } else if(ApexPages.currentPage().getParameters().get('language') == 'es') {
        englishState = 'loff txt3';
        SpanishState = 'laON bol3';
        PortugueseState = 'loff txt3';
        css = 'CD_ES.css';
        language = 'es';
    } else {
        englishState = 'loff txt3';
        SpanishState = 'loff txt3';
        PortugueseState = 'laON bol3';
        css = 'CD_ES.css';
        language = 'pt_BR';
    }
    return null;
 }  
  
  public List<SelectOption> getCountryValues(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('',''));
        //if(language == 'en') {
        options.add(new SelectOption('United States','United States'));
        options.add(new SelectOption('Canada','Canada'));
        //} else {
        options.add(new SelectOption('Argentina','Argentina'));
        options.add(new SelectOption('Bolivia','Bolivia'));
        options.add(new SelectOption('Brazil','Brazil'));
        options.add(new SelectOption('Chile','Chile'));
        options.add(new SelectOption('Colombia','Colombia'));
        options.add(new SelectOption('Costa Rica','Costa Rica'));
        options.add(new SelectOption('Ecuador','Ecuador'));
        options.add(new SelectOption('El Salvador','El Salvador'));
        options.add(new SelectOption('Guatemala','Guatemala'));
        options.add(new SelectOption('Honduras','Honduras'));
        options.add(new SelectOption('Mexico','Mexico'));
        options.add(new SelectOption('Nicaragua','Nicaragua'));
        options.add(new SelectOption('Panama','Panama'));
        options.add(new SelectOption('Paraguay','Paraguay'));
        options.add(new SelectOption('Peru','Peru'));
        options.add(new SelectOption('Uruguay','Uruguay'));
        options.add(new SelectOption('Venezuela','Venezuela'));
       // options.add(new SelectOption('United States','United States'));
       // options.add(new SelectOption('Canada','Canada'));
       //}
        
        system.debug('<<<<<<<<<<<<<<<<< getCountryValues options >>>>>>>>>>>>>>>>>>>' + options);
        return options;
  }
    
  public List<SelectOption> getModelTypeValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('',''));
        options.add(new SelectOption('PS4','PS4'));
        options.add(new SelectOption('PS3','PS3'));
        options.add(new SelectOption('VITA','VITA'));
        system.debug('<<<<<<<<<<<<<<<<< getModelTypeValuesoptions >>>>>>>>>>>>>>>>>>> ' + options);
        return options;
  }
  
  public PageReference getSerialNumHelpText() {
        if(SelectedModelType == 'PS4') {
            SerialNumhelpTextVal = Label.PS4_help_text_URL;
        } else if (SelectedModelType == 'PS3') {
            SerialNumhelpTextVal = Label.PS3_Help_Text_URL;
        } else if(SelectedModelType == 'VITA'){
            SerialNumhelpTextVal = Label.VITA_Help_Text_URL;
        } else {
            SerialNumhelpTextVal = Label.PS4_help_text_URL;
        }
        system.debug('<<<<<<<<<<<<<<<<< getSerialNumHelpText >>>>>>>>>>>>>>>>>>> ' + SerialNumhelpTextVal);
        return null;
  }
  
  public PageReference validateSession() {
      ipBlocked = false;
      system.debug('<<<<<<< Validate IP Address Call >>>>>>>> ');
      validateIP();
      if(ipBlocked) {
        reqBlocked = true; 
        showAlert = true; 
        system.debug('<<<<<<<<<<<<<Language>>>>>>>>>>>>>>>'+language);
        PageReference toerrorPage = new PageReference('/apex/ConsoleDeactivationsErrorPage');
        toerrorPage.getParameters().put('reqBlocked', 'true');
        toerrorPage.getParameters().put('language', language);   
        toerrorPage.setRedirect(true); 
        return toerrorPage;
      } //else no action required       
      return null;
  }
  
  public PageReference ValidateInputData() {
 
        // validate input data
        showAlert = false;
        requiredLabel = false;
        requiredFirstName = false;
        requiredLastName = false;
        requiredEmail = false;
        requiredPSNOnlineId = false;
        requiredPSNSignInId = false;
        requiredSerialNumber = false;
        requiredModelType = false;
        requiredCountry = false;
        duplicateRequest = false;
        invalidEmail = false;
        invalidPSNSignInId = false;
        invalidSerialNum = false;
        generalError = false;
        MDMRecNotExists = false;
        isCDRequestSuccess = false;
        reqBlocked = false;
        
        boolean dupRequest = false;
        system.debug('<<<<<<<<<<<<<In Input Validation Method>>>>>>>>>>>>>>>>');
        
        if(String.isBlank(FirstName)){
            requiredFirstName = true;
            requiredLabel = true;}
        if(String.isBlank(LastName)){
            requiredLastName = true;
            requiredLabel = true;}
        if(String.isBlank(PersonEmail)){
            requiredEmail = true;
            requiredLabel = true;}
        if(String.isBlank(PSNOnlineId)){
            requiredPSNOnlineId = true;
            requiredLabel = true;}
        if(String.isBlank(PSNSigninId)){
            requiredPSNSignInId = true;
            requiredLabel = true;}
        if(String.isBlank(SerialNumber)){
            requiredSerialNumber = true;
            requiredLabel = true;}
        if(String.isBlank(SelectedModelType)){
            requiredModelType = true;
            requiredLabel = true;}
        if(String.isBlank(SelectedCountry)){
            requiredCountry = true;
            requiredLabel = true;}
            
        if(requiredLabel == true){
                system.debug('<<<<<<<<<<<<<Required Label Value>>>>>>>>>>>>>>>>'+requiredLabel);
            return null;
        } 
            
         //Email Address form Validation
        if(!Pattern.matches('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$', PersonEmail)){
            invalidEmail = true;
            showAlert = true;
        } 
        
        //PSN Sing-In Id format validation
        if(!Pattern.matches('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$', PSNSigninId)){
            invalidPSNSignInId = true;
            showAlert = true;
        }
        
        //Serial Number Validation
        system.debug('<<<<<<<<<< Serial Number >>>>>>>>>>>>>>>'+SerialNumber);
        system.debug('<<<<<<<<<< Serial Number without White spaces >>>>>>>>>>>>>>>'+SerialNumber.deleteWhitespace().length() );
        system.debug('<<<<<<<<<< Serial Number Pattern Match >>>>>>>>>>>>>>>'+Pattern.matches('^([A-Za-z]{2})([0-9]{9})$', SerialNumber));
        if(SerialNumber <> null && (SerialNumber.deleteWhitespace().length() <> 11 ||
           (!Pattern.matches('^([A-Za-z]{2})([0-9]{9})$', SerialNumber)))) {
           invalidSerialNum = true;
           showAlert = true;    
        }
        
        
        if(invalidEmail == true || invalidPSNSignInId == true || invalidSerialNum == true) {
                return null;
        } else {
                //log the request in the Console Deactivation Log Object
                //logRequest();
            //Check for dupe submissions
            dupRequest = CheckforDuplicateRequest();    
            if(dupRequest) {
                logRequest();
                duplicateRequest = true;
                showAlert = true;
                logWebRequest.Status__c = 'Failed';
                logWebRequest.Error_Message__c = 'Duplicate Console Deactivation request for PSN Online Id ='+PSNOnlineId;
                update logWebRequest;
                //return null;
                system.debug('<<<<<<<<<<<<<Language>>>>>>>>>>>>>>>'+language);
                system.debug('<<<<<<<<<<<<<Duplicate Request Variable>>>>>>>>>>>>>>>'+duplicateRequest);
                system.debug('<<<<<<<<<<<<<Show Alert Variable>>>>>>>>>>>>>>>'+showAlert);
                PageReference toerrorPage = new PageReference('/apex/ConsoleDeactivationsErrorPage');
                toerrorPage.getParameters().put('duplicateRequest', 'true');
                toerrorPage.getParameters().put('language', language);  
                toerrorPage.setRedirect(true); 
                return toerrorPage;
            } else {
                  SearchMDMandprocessRequest();
                  system.debug('<<<<<<<<<<<<<MDMRecordFound>>>>>>>>>>>>>>>'+MDMRecordFound);
                  if(!MDMRecordFound){
                     if(generalError) {
                       system.debug('<<<<<<<<<<<<<Language>>>>>>>>>>>>>>>'+language);
                       PageReference toerrorPage = new PageReference('/apex/ConsoleDeactivationsErrorPage');
                       //toerrorPage.getParameters().put('reqBlocked', 'false');
                       //toerrorPage.getParameters().put('duplicateRequest', 'false');
                       toerrorPage.getParameters().put('language', language);   
                       toerrorPage.setRedirect(true); 
                       return toerrorPage;  
                     } else {
                       logRequest();
                       MDMRecNotExists = true;    
                       showAlert = true;
                       if(logWebRequest <> null) {
                         logWebRequest.Status__c = 'Failed';
                         logWebRequest.Error_Message__c = 'No MDM record found with PSNOnline ='+PSNOnlineId;      
                         update logWebRequest;
                       } //else no action needed
                       return null;
                     }        
                  } else {
                   MDMRecNotExists = false;
                   isCDRequestSuccess = true;
                   showAlert = true;
                   
                   system.debug('<<<<<<<<<<<<<Language>>>>>>>>>>>>>>>'+language);
                   PageReference toconfirmPage = new PageReference('/apex/ConsoleDeactivationConfirmationPage');
                   toconfirmPage.getParameters().put('language', language); 
                   toconfirmPage.setRedirect(true); 
                   return toconfirmPage;
                  }     
              }        
        }  
    }
    
  public void validateIP(){ 
          
      try{             
         url = Apexpages.currentPage().getUrl();    
         ip = ApexPages.currentPage().getHeaders().get('True-Client-IP');
       
         if (ip == '' || ip == null){
             ip = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
         }  
         if (ip == '' || ip == null){
          ip = ApexPages.currentPage().getHeaders().get('X-Forwarded-For');
         }
      
         system.debug('<<<<<<<<<<<<<< ip Address >>>>>>>>>>>>>' + ip);
         system.debug('<<<<<<<<<<<<<< URL Address >>>>>>>>>>>>>' + url);
      
         ipAddress = ip <> null ? ip : '';
         system.debug('<<<<<<<<<<<<<< ip Address for Case creation>>>>>>>>>>>>>' + ipAddress);
        
         List<Blocklist__c> Blocklist = new List<Blocklist__c>([SELECT Start_IP_Address__c FROM Blocklist__c WHERE Status__c = 'Blocked' AND Start_IP_Address__c =: ip limit 1]);
         system.debug('<<<<<<<<< Block List >>>>>>>>>>>>>>>>>>> ' + Blocklist);
         if (Blocklist <> null && Blocklist.size() > 0 && Blocklist[0].Start_IP_Address__c == ip){
             LogBlockedUser('IP Block',null,url, ip);
             ipBlocked = true;
         } 
      } catch(Exception e) {
        IntegrationAlert.addException('Console Deactivation validateIP address process failed for', 'email='+PersonEmail + ', PSN Online ID='+PSNOnlineId, e);
        generalError = true;
     } 
  }

  public void LogBlockedUser(String blockType, Id ConsumerId, String urlNow, String ipNow) {
         
     try {
         Blocked_User_Log__c bl = new Blocked_User_Log__c();
         if(ConsumerId <> null)
            bl.Blocked_User_Ref__c = ConsumerId;
         bl.Type_Of_Block__c = blockType;
         if(urlNow != null)
            bl.Block_URL_Source__c = urlNow.split('\\?')[0];
         bl.Blocked_User_IP__c = ipNow;
    
         insert bl;
     } catch(Exception e) {
        IntegrationAlert.addException('Console Deactivation Logging Blocked User process failed for', 'email='+PersonEmail + ', PSN Online ID='+PSNOnlineId, e);
        reqBlocked = true; //still have to block consumer even if logging failed due to system issues 
     } 
  }   
   
  public void logRequest(){
        
    try {
           Console_Deactivation_Requests__c ConDeactivationReq = new Console_Deactivation_Requests__c();
           ConDeactivationReq.Request_Form_Inputs__c = 'First Name  = '+FirstName+'\n' +
                                                    'Last Name  = '+LastName+'\n' +  
                                                    'Email  = '+PersonEmail+'\n' +
                                                    'PSN Online Id  = '+PSNOnlineId+'\n' +
                                                    'PSN sign-In Id  = '+PSNSigninId+'\n' +
                                                    'Serial Number  = '+SerialNumber+'\n' +
                                                    'Model Type  = '+SelectedModelType+'\n' +
                                                    'Country  = '+SelectedCountry+'\n' +
                                                    'IP Address =' +ipAddress+'\n' +
                                                    'Form Language =' + language; 
           ConDeactivationReq.Process_Tracking__c = 'Step logRequest';                                           
     
           insert ConDeactivationReq;
     
    for(Console_Deactivation_Requests__c req :[select Id, Status__c, Error_Message__c, Case_Reference__c, Process_Tracking__c from Console_Deactivation_Requests__c where Id = :ConDeactivationReq.Id]) {
         system.debug('<<<<<<<<<<< requery record to assign it to global variable >>>>>>>>>>>>>>>'+req);
        if(req <> null){
          logWebRequest = req;  
          system.debug('<<<<<<<<<<< Gobal variable logWebRequest >>>>>>>>>>>>>>>'+logWebRequest); 
        }
     }  
    } catch(Exception e) {
        IntegrationAlert.addException('Console Deactivation logRequest process failed for', 'email='+PersonEmail + ', PSN Online ID='+PSNOnlineId, e);
        generalError = true;
    }                                             
  }    
    
  //This method checks for Duplicate submissions based on PSN Online Id entered in the web form
  public boolean CheckforDuplicateRequest() {
                        
        //logWebRequest.Process_Tracking__c = 'Step: Entered CheckforDuplicateRequest Method';
        boolean dupeFound = false;
        
        //get the Date filter criteria from custom setting
        Date_Filter_Web_Console_Deactivations__c datefilterValSetting = Date_Filter_Web_Console_Deactivations__c.getOrgDefaults();
        decimal datefilterVal   = datefilterValSetting <> null ? datefilterValSetting.Date_Filter_Value__c : 0.00 ;
        system.debug('<<<<<<<<<<<Custom Settings Date filter Value>>>>>>>>>>>>>>>>'+datefilterVal);
        
        //calculate the Date time filter value based on custom setting value
        DateTime currentDateTime = system.now();
        DateTime filterCriteriaDate = datefilterVal <> null ? (currentDateTime - datefilterVal) : currentDateTime;
        system.debug('<<<<<<<<<<<Date Criteria value used to pull the Cases =>>>>>>>>>>>>>>>>'+filterCriteriaDate);
              
        //get all the Console Deactivation requests for past 24 hrs only
        system.debug('<<<<<<<<<<<Case creation date criteria>>>>>>>>>>>>>>>>'+currentDateTime);
        list <Case> consoleDeactivationCases = new list <Case>([SELECT Id, CaseNumber, Contact.FirstName, Contact.LastName, Description, CreatedDate, Sub_Area__c, Issue1__c, Status, Case_Age__c, PSN_Account__r.Name
                                     FROM Case WHERE 
                                     RecordTypeId =: RT_PS_NETWORK_ID AND
                                     Sub_Area__c =: 'Deactivate (Console)' AND
                                     Issue1__c != : 'Referred to Web' AND
                                     status != : 'Case Cancelled' AND
                                     CreatedDate >= :filterCriteriaDate AND
                                     PSN_Account__r.Name =: PSNOnlineId
                                     ]);
        system.debug('<<<<<<<<<<<Console Deactivation Cases>>>>>>>>>>>>>>>>'+consoleDeactivationCases);
        
        if(consoleDeactivationCases <> null && consoleDeactivationCases.size() > 0) {
                system.debug('<<<<<<<<<<<Console Deactivation Cases List Size>>>>>>>>>>>>>>>>'+consoleDeactivationCases.size());
                return (dupeFound = true);      
        } else {
                //logWebRequest.Process_Tracking__c = 'Step: Exit CheckforDuplicateRequest Method with dupeFound ='+dupeFound;
                return dupeFound; //No records found. so go ahead and create case. 
        }
        //logWebRequest.Process_Tracking__c = 'Step: Exit CheckforDuplicateRequest Method with dupeFound ='+dupeFound;
        return dupeFound;
  }

  public void SearchMDMandprocessRequest() {
        
    try {
    //logWebRequest.Process_Tracking__c = 'Step: Entered SearchMDMforConsumer Method';
    string MDMContactId;
    string MDMAccountId;
    Contact existingContact;
    Id personAccountRecTypeId;
    Sony_MiddlewareConsumerdata3.Account_mdm MDMAccount;
    Sony_MiddlewareConsumerdata3.Contact_mdm MDMContact;
    MDMRecordFound = false;
        
    //convert to all lower case for MDM to lookup
    string cleanPSNOnline = PSNOnlineId.toLowerCase();
    system.debug('<<<<<<<<<< Language parameter in Case>>>>>>>>>>' + language);
    string preferredlang =   (language <> null && language <> '') ? (language == 'en' ? 'English' : (language == 'es' ? 'Spanish' : 'Portuguese') ) : 'English';
    system.debug('<<<<<<<<<< Case Language to be stamped in the Description field =>>>>>>>>>>' + preferredlang);
                
    //MDM call to search for consumer by Email and PSNOnlineId
    ConsumerSearchResult searchResult = new ConsumerSearchResult();
    list<WrapperContacts> resultList = new list<WrapperContacts>();
    try{
      searchResult = ConsumerServiceUtility.getConsumers(null, null, cleanPSNOnline);
    } catch(Exception e) {
      IntegrationAlert.addException('Console Deactivation MDM Consumer Lookup exception', 'email='+PersonEmail + ', PSN Online ID='+PSNOnlineId, e); 
      generalError = true; //incase of any system issues, show general error       
    } 
    //MDM check errored. so call for consumer record creation in Atlas
    system.debug('<<<<<<<<<<<<<MDM Search Result>>>>>>>>>>>>>>'+searchResult);
    system.debug('<<<<<<<<<<<<<MDM Search Error Message>>>>>>>>>>>>>>'+searchResult.ErrorMessage);
    system.debug('<<<<<<<<<<<<<MDM Search Result List of Accounts>>>>>>>>>>>>>>'+searchResult.listOfAccounts);
    
    if(searchResult <> null && searchResult.ErrorMessage <> null) {
       System.debug('<<<<<< MDM Error Message >>>>>>>'+searchResult.ErrorMessage);       
       if(searchResult.ErrorCode == '20') { //No RECORD FOUND IN MDM
          MDMRecordFound = false;       
        } else { //All Other Exceptions/Errors in MDM response. This will take care of MDM downtimes with out impacting customers
         // generalError = true; //incase of any system issues, show general error    
        }       
    } 
    
    //MDM returned no results. so call for Consumer record creation in Atlas
    if(searchResult <> null && searchResult.listOfAccounts == null && searchResult.ErrorMessage == null) {
       //No MDM results so show error
       MDMRecordFound = false;
    }
    else if(searchResult <> null && searchResult.listOfAccounts <> null && searchResult.ErrorMessage == null ) { //perform logic of checking and creating accounts
        if(searchResult <> null && searchResult.listOfAccounts <> null) {
           MDMRecordFound = true;       
           system.debug('<<<<<<<<<<< MDM listOfAccounts >>>>>>>>>>>>> = '+searchResult.listOfAccounts);
           for(Sony_MiddlewareConsumerdata3.Account_mdm acc : searchResult.listOfAccounts) {
               MDMAccount = acc;
               system.debug('<<<<<<<< MDM listOfContacts >>>>>>>>>>>> = '+acc.ListOfContact.Contact);
               system.debug('<<<<<<<< MDM Account >>>>>>>>>>>> = '+MDMAccount);
               if(acc.ListOfContact.Contact <> null){        
                  for(Sony_MiddlewareConsumerdata3.Contact_mdm cnt : acc.ListOfContact.Contact) {
                          MDMContact = cnt;
                      WrapperContacts wrapCnt = new WrapperContacts();
                      wrapCnt.fName = cnt.FirstName;
                      wrapCnt.lName = cnt.LastName;
                      wrapCnt.psnSignInId = cnt.PSNSignInId;
                      wrapCnt.psnPhone = formatPhoneNumber(cnt.PSNPhone);
                      wrapCnt.crmPhone = formatPhoneNumber(cnt.CRMPhone);
                      wrapCnt.psnEmail = cnt.PSNEmail;
                      wrapCnt.siebelEmail = cnt.SiebelEmail;
                      wrapCnt.birthDate = cnt.BirthDate;
                      wrapCnt.mdmContactId = cnt.MDMRowId;
                      wrapCnt.PSNHandle = acc.PSNHandle;
                      wrapCnt.PreferredLanguageCode = preferredlang; //cnt.PreferredLanguageCode;
                      wrapCnt.sex = cnt.sex;
                      wrapCnt.PSPlusExpiration = getMyDateTime(acc.PSPlusStackedEndDate); 
                      wrapCnt.PSPlusSubscriber = acc.PSPlusSubscriber;
                      system.debug('<<<<<<< MDM Contacts cnt  >>>>'+cnt);
                      system.debug('<<<<<<< MDM Contact ListOfAddress  >>>>'+cnt.ListOfAddress.Address);
                      
                      if(cnt.ListOfAddress.Address <> null){   
                        for(Sony_MiddlewareConsumerdata3.Address_mdm add : cnt.ListOfAddress.Address) {
                            wrapCnt.addressList.add(add); 
                        }
                      }  
                     resultList.add(wrapCnt);
                     system.debug('<<<<<<<<<MDM Contact result list >>>>>>>>>>>' +resultList);
                  }
               }
           }
        } 
        
    //search of existing record using MDM ID and if not found in Atlas, go ahead and create one
    if(resultList <> null && resultList.size() > 0) {
        MDMContactId = resultList[0].mdmContactId;
        system.debug('<<<<<<<MDM Contact Id >>>>>>>>>'+MDMContactId);
        
        //search Atlas for contact by MDM ID
        if(MDMContactId <> null && MDMContactId <> '') {
           for(Contact cnt : [select Id, MDM_Account_ID__c,Preferred_Language__c from Contact where MDM_ID__c = :MDMContactId]) {
                existingContact = cnt;     
           }    
        }
        system.debug('<<<<<<<Existing Contact record >>>>>>>>>'+existingContact);
        //Search by MDM ID couldn't find a contact record in Atlas. so create person account
        if(existingContact == null) {
          //get Person Account record type Id
          for(RecordType peraccrecType: [SELECT Name, SobjectType,IsPersonType FROM RecordType 
                              WHERE SobjectType='Account' AND IsPersonType=True]){
              personAccountRecTypeId = peraccrecType.id;
            }
          
          //Person Account creation
          if(personAccountRecTypeId <> null){ 
                Account newConsumerPerson = new Account();
                newConsumerPerson.RecordTypeId = personAccountRecTypeId;
                newConsumerPerson.FirstName =resultList[0].fName;
                newConsumerPerson.LastName = resultList[0].lName;
               // newConsumerPerson.PersonBirthdate = '';
                newConsumerPerson.PersonEmail = resultList[0].siebelEmail;           
                newConsumerPerson.MDM_ID__pc = resultList[0].mdmContactId;
                newConsumerPerson.MDM_Account_ID__pc = searchResult.listOfAccounts[0].MDMAccountRowId;
                newConsumerPerson.Preferred_Language__pc = preferredlang; //resultList[0].PreferredLanguageCode <> null ? getLanguage(resultList[0].PreferredLanguageCode) : '';
                newConsumerPerson.Gender__pc = resultList[0].sex;
                newConsumerPerson.Phone = resultList[0].crmPhone;
                newConsumerPerson.LATAM_Country__pc = SelectedCountry;
                if(newConsumerPerson.Phone==null || newConsumerPerson.Phone.trim().length()==0){
                    newConsumerPerson.Phone = resultList[0].psnPhone;     
                }
                if(newConsumerPerson.PersonEmail==null || newConsumerPerson.PersonEmail.trim().length()==0){
                    newConsumerPerson.PersonEmail = resultList[0].psnEmail;     
                }               
                
                try { 
                  insert newConsumerPerson;
                } catch(Exception e) {
                  IntegrationAlert.addException('Console Deactivation MDM Account creation in Atlas failed ', 'email='+PersonEmail + ', PSN Online ID='+PSNOnlineId, e);
                  MDMRecordFound = true; //incase of any system issues we should allow the record creation and not show any errors in the web form      
                  //call for Atlas record creation
                  createCase(null);
                } 
                //query for the newly inserted Person Account for using it later down in the process
                for(Account peracct : [select id,PersonContactId from Account where ID=:newConsumerPerson.id]) {
                    selectedAccount = peracct;
                }
                
                //query for the newly inserted Contact and assign it to existingcontact for using it later down in the process
                for(Contact cnt : [select Id, MDM_Account_ID__c from Contact where MDM_ID__c = :MDMContactId]) {
                    existingContact = cnt;
                    selectedContact = cnt;
                }
                system.debug('<<<<<<<<<< Existing contact record after inserting the contact = >>>>>>>>>>>>'+existingContact);  
          }  
          
          //PSN Account record creation
          if(existingContact <> null && MDMContact <> null && MDMAccount <> null){
            system.debug('<<<<<<<<<< Call to Create PSN Account record with ContactId =' +existingContact.Id+ '---------AccountId='+searchResult.listOfAccounts[0].MDMAccountRowId);
            createPSNAccount(existingContact.Id, MDMContact, MDMAccount);
          } // else no action necessary

          //Create Case
          if(existingContact <> null){
            system.debug('<<<<<<<<<< Call to Create Case with ContactId = >>>>>>>>>>>>>>>>>' +existingContact.Id);
            createCase(existingContact.Id);
          }  
          //Create Case 
        } else { //record found in Atlas with the same MDM Id. Go ahead and create case and associate case to this contact.
                system.debug('<<<<<<<<<< Existing contact record after inserting the contact = >>>>>>>>>>>>'+existingContact);
                //Update Preferred Language for existing contact for email communication template language
                existingContact.Preferred_Language__c = preferredlang;
                update existingContact;
                system.debug('<<<<<<<<<< Existing contact record after updating the Preferred Lang = >>>>>>>>>>>>'+existingContact);
                createCase(existingContact.Id);     
        }
    } else { //something went wrong in the MDM result. It has Account but no contacts associated to it or something went wrong in the wrapper class assignment. A system issue.
       // MDMRecordFound = true; //incase of any system issues we should allow the record creation and not show any errors in the web form        
    }
   } 
    //logWebRequest.Process_Tracking__c = 'Step: Exit SearchMDMforConsumer Method';     
  } catch(Exception e) {
      IntegrationAlert.addException('Console Deactivation SearchMDMandprocessRequest Method failed for ', 'email='+PersonEmail + ', PSN Online ID='+PSNOnlineId, e);       
    }   
  } //End Method
  
  public void createPSNAccount(Id ConsumerId, Sony_MiddlewareConsumerdata3.Contact_mdm  MDMContact, Sony_MiddlewareConsumerdata3.Account_mdm  MDMAccount){
        
    try {
    PSN_Account__c PSNAccount;
    //find existing record
    for(PSN_Account__c psnacc : [select Id from PSN_Account__c where Consumer__c = :ConsumerId AND MDM_Account_ID__c = :MDMAccount.MDMAccountRowId]) {                       
       system.debug('<<<<<<<<<<< Existing PSN Account >>>>>>>>>>>>>>'+psnacc);
       PSNAccount = new PSN_Account__c(Id = psnacc.Id);
    }
    
    //No existing PSN Account, so create new record
    if(PSNAccount == null) {
       PSNAccount = new PSN_Account__c(Consumer__c = ConsumerId, MDM_Account_ID__c = MDMAccount.MDMAccountRowId);                              
       system.debug('<<<<<<<<<<< New PSN Account record >>>>>>>>>> '+PSNAccount);       
    }
       
    //setup other PSN attributes 
    PSNAccount.Account_ID__c =  MDMAccount.PSNAccountId;
    PSNAccount.Creation_Date__c = MDMAccount.CreatedDate;
    PSNAccount.PSN_Account_ID__c = MDMAccount.PSNAccountId;
    PSNAccount.Suspension_Date__c = MDMAccount.AccountSuspendDate <> null && MDMAccount.AccountSuspendDate <> '' ? Date.parse(MDMAccount.AccountSuspendDate.left(10)) : null;
    PSNAccount.Suspension_Reason__c = MDMAccount.AccountSuspendReason;
    PSNAccount.Unsuspended_Date__c = MDMAccount.AccountUnsuspendDate <> null && MDMAccount.AccountUnsuspendDate <> '' ? Date.parse(MDMAccount.AccountUnsuspendDate.left(10)) : null;
    PSNAccount.Status__c = MDMAccount.AccountStatus;
    PSNAccount.First_Name__c = MDMContact.FirstName;
    PSNAccount.Last_Name__c = MDMContact.LastName;
    PSNAccount.Language__c = getLanguage(MDMContact.PreferredLanguageCode);
    //PSNAccount.Date_of_Birth__c = parseDate(contact_mdm.BirthDate);
    PSNAccount.Gender__c = MDMContact.sex;
    PSNAccount.PSN_Sign_In_ID__c = MDMContact.PSNSignInId;
    PSNAccount.Name = MDMAccount.PSNHandle;
    PSNAccount.Email__c = MDMContact.PSNEmail;
    PSNAccount.Phone__c = MDMContact.PSNPhone;
    PSNAccount.PS_Plus_Indicator__c = Boolean.valueOf(MDMAccount.PSPlusSubscriber);
    PSNAccount.PS_Plus_Start_Date__c = getMyDateTime(MDMAccount.PSPlusStartDate);
    PSNAccount.PS_Plus_End_Date__c = getMyDateTime(MDMAccount.PSPlusStackedEndDate); 
    if(MDMAccount.PSPlusAutoRenewalFlag.equalsIgnoreCase('True')){
        PSNAccount.Auto_Renewal__c = 'On';
    } else if(MDMAccount.PSPlusAutoRenewalFlag.equalsIgnoreCase('False')){
        PSNAccount.Auto_Renewal__c = 'Off';
    }
    PSNAccount.PSN_Plus_Country__c = MDMAccount.PSPlusAccountCountry;
        
     
         system.debug('<<<<<<<<<<<<<<<<<<<<< PSN Account to Upsert >>>>>>>>>>>>>>>>>>>'+PSNAccount);      
         upsert PSNAccount; 
    } catch(Exception e) {
         IntegrationAlert.addException('Console Deactivation MDM PSN Account creation in Atlas failed for ', 'email='+PersonEmail + ', PSN Online ID='+PSNOnlineId, e); 
    } 
  }
 
 public void createCase(Id ConsumerId) {
        
    try {
        system.debug('<<<<<<<<<< Language parameter in Case>>>>>>>>>>' + language);
        string Case_lang =  (language <> null && language <> '') ? (language == 'en' ? 'English' : (language == 'es' ? 'Spanish' : 'Portuguese') ) : 'English';
        system.debug('<<<<<<<<<< Case Language to be stamped in the Description field =>>>>>>>>>>' + Case_lang);
        Case newCase;
        List<Group> ConsoleDeactivationQueueId = [Select Id From Group where Name = 'ConsoleDeactivations' and Type = 'Queue' limit 1];
    
        if(ConsumerId <> null) {
           newCase = new Case(ContactId=ConsumerId);
        } else {
           newCase = new Case();        
        }
        system.debug('<<<<<<<<<<<< Console Deactivation Case Record Type >>>>>>>>>>>>>>>>'+RT_PS_NETWORK_ID);
        system.debug('<<<<<<<<<<<<<< ip Address for Case creation>>>>>>>>>>>>>' + ipAddress);
        
                
        newCase.RecordTypeId = RT_PS_NETWORK_ID;
        newCase.Product__c = SelectedModelType;
        newCase.Sub_Area__c = 'Deactivate (Console)';
        newCase.Description = 'First Name  = '+FirstName+'\n' +
                                 'Last Name  = '+LastName+'\n' +  
                                 'Email  = '+PersonEmail+'\n' +
                                 'PSN Online Id  = '+PSNOnlineId+'\n' +
                                 'PSN sign-In Id  = '+PSNSigninId+'\n' +
                                 'Serial Number  = '+SerialNumber+'\n' +
                                 'Model Type  = '+SelectedModelType+'\n' +
                                 'Country  = '+SelectedCountry+'\n' +
                                 'IP Address =' +ipAddress+'\n'+
                                 'Form Language = '+Case_lang; 
        newCase.Status = 'Opened'; 
        newCase.SuppliedEmail = PersonEmail;
        newCase.Origin = 'Web';
        system.debug('<<<<<<<<<<<< Console Deactivation Queue >>>>>>>>>>>>>>>>'+ConsoleDeactivationQueueId);
        if(ConsoleDeactivationQueueId <> null && ConsoleDeactivationQueueId.size() > 0)                             
           newCase.OwnerId = ConsoleDeactivationQueueId[0].Id;
        
        insert newCase;
    } Catch (Exception e) {
         IntegrationAlert.addException('Console Deactivation New Case Insertion failed for', 'email='+PersonEmail + ', PSN Online ID='+PSNOnlineId, e);          
    }
 }   
    
  
  public Class WrapperContacts {
    public String mdmContactId{get;set;}
    public String fName{get;set;}
    public String lName{get;set;}
    public String PSNHandle{get;set;}
    public String psnSignInId{get;set;}
    public String psnPhone{get;set;}
    public String psnEmail{get;set;}
    public String siebelEmail{get;set;}
    public String crmPhone{get;set;}
    public String birthDate{get;set;}
    public String PreferredLanguageCode{get;set;}
    public String sex{get;set;}
    public list<Sony_MiddlewareConsumerdata3.Address_mdm> addressList{get;set;}
    public String PSPlusExpiration{get;set;}
    public String PSPlusSubscriber{get;set;}
    
    public WrapperContacts() {
        addressList = new list<Sony_MiddlewareConsumerdata3.Address_mdm>();
    }
  }
  
  private String formatPhoneNumber(string phoneNumber) {
        if(phoneNumber<> null && phoneNumber.length() == 10) {
           phoneNumber = '(' + phoneNumber.substring(0, 3) + ') ' + phoneNumber.substring(3, 6) + '-' + 
                          phoneNumber.substring(6, phoneNumber.length());
        }
        return phoneNumber;
  }
  
  public static String getMyDateTime(string strDt){ 
      if(strDt != null && strDt !=''){ 
         String[] DTSplitted = strDT.split(' ');
         System.debug('<<<<<< Date String list after split = >>>>>>>>>>>>>>>>'+DTSplitted);
         string year = DTSplitted.get(0).split('-').get(0);
         string month = DTSplitted.get(0).split('-').get(1);
         string day = DTSplitted.get(0).split('-').get(2);
         string stringDate = month + '/' + day + '/' + year;
         System.debug('<<<<<< Date String to return = >>>>>>>>>>>>>>>>'+stringDate);
         return stringDate;
      }
      return null;
  }
  
 private static String getLanguage(String languageCode) {
    System.debug('<<<<<< Language Code passed to this method = >>>>>>>>>>>>>>>>'+languageCode);    
    map<String, Language__c> langSettingMap = Language__c.getAll();
    System.debug('<<<<<< Language Custom setting records >>>>>>>'+langSettingMap);

    if(langSettingMap.containsKey(languageCode)){
        return langSettingMap.get(languageCode).Description__c;
    } 
    return languageCode;
  }
  

}