//8/8/2013 : Urminder : created this test class.
@isTest
private class CaseService_Test {
  
  static testMethod void myUnitTest() {
        
        Case testcase = new Case();
        testcase.Status = 'Open';
        testcase.received_date__c = date.today();
        testcase.shipped_date__c = date.today().addDays(1);
        testcase.outbound_tracking_number__c = '101';
        testcase.outbound_carrier__c = 'TYest1';
        testCase.Origin = 'Social';
        testCase.Offender__c = 'Test';
        testCase.Product__c = 'PS4';
        testCase.Sub_Area__c = 'Account Help';
        insert testCase;
        
        Case insertedCase = [select caseNumber from Case where id = :testCase.Id];
        
        // set up the request object
        CaseService.CaseInfo caseinfo = CaseService.getCase(insertedCase.caseNumber);
        System.assertEquals(caseinfo.caseNumber, insertedCase.caseNumber, 'Case number should Match');
        
        testCase.Status='New Status';
        update testCase;
    }
}