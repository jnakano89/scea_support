/***********************************************************************************************************
Created : 09/08/2014 : Leena Mandadapu : Jira# SMS-654 - WebPPP Revamp project


************************************************************************************************************/
public class PYBCancellationRequest {
    
    public static final String PYB_END_POINT;
    public static final String PYB_USER_NAME;
    public static final String PYB_PASSWORD;
    
    static {
    Integration__c settings = Integration__c.getOrgDefaults();
    PYB_END_POINT = settings <> null ? settings.PYB_End_Point_URL__c : '';
    PYB_USER_NAME = settings <> null ? settings.PYB_User_Name__c : '';
    PYB_PASSWORD = settings <> null ? settings.PYB_Password__c : ''; 
    
    system.debug('<<<<<<<END POINT>>>>>>>>>>>>'+PYB_END_POINT);
    system.debug('<<<<<<<PYB_USER_NAME>>>>>>>>'+PYB_USER_NAME);
    system.debug('<<<<<<<PYB_PASSWORD>>>>>>>>'+PYB_PASSWORD);
    }
    
    public PYBCancellationResponse CancelRequest(String OrderNumber, Boolean PPPClaimed){
        
        PYBCancellationResponse cancelresponse;
        Http h;
        HttpRequest req;
        HttpResponse res;
        String response;
        String request;
 
        try{
        h = new Http();
        req = new HttpRequest();
        req.setEndpoint(PYB_END_POINT);
        req.setMethod('POST');
        req.setTimeout(120000);
        req.setHeader('Content-Type', 'application/xml');
        system.debug('<<<<<<Cancellation Request Order Number>>>>>>>>'+OrderNumber);
        system.debug('<<<<<<Cancellation Request PPP Claim Flag>>>>>>'+PPPClaimed);
        request = requestBody(OrderNumber,PPPClaimed);
        system.debug('<<<<<<Request Body>>>>>>>>'+request);
        req.setBody(request);
        system.debug('<<<<<<Request>>>>>>>>'+req);
        res = h.send(req);
        response = res.getBody();
        system.debug('<<<<<response>>>> '+response);
        cancelresponse = PYBCancelResponse(response);
        system.debug('<<<<<<Parsed PYBCancellation Response>>>> '+cancelresponse);
        
        }catch(Exception e){
            IntegrationAlert.addException('PYB Cancellation: Order Number='+OrderNumber, e, request, response);
        }
        
        return cancelresponse;
    }
    
    public String requestBody(String OrderNumber, Boolean PPPClaimed){
        String reqbody;
        reqbody  = '<RESTPolicyCancel xmlns="http://www.protectyourbubble.com/2011/09">';
        reqbody += '<Cred>';
        reqbody += '<Password>'+PYB_PASSWORD+'</Password>';
        reqbody += '<User>'+PYB_USER_NAME+'</User>';
        reqbody += '</Cred>';
        reqbody += '<policyCancelItem>';
        reqbody += '<ContractNumber>' + OrderNumber + '</ContractNumber>';
        reqbody += '<CustomerHasClaimed>' + PPPClaimed + '</CustomerHasClaimed>';
        reqbody += '</policyCancelItem>';
        reqbody += '</RESTPolicyCancel>';
        
        system.debug('<<<<<<<<<<<<<Request Body>>>>>>>>>>>>>>>>>>>'+ reqbody);
        return reqbody;
    } 
    
    /*Cancel Response Format
    <PolicyCancelReply xmlns="http://www.protectyourbubble.com/2011/09" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
    <CancelDate>0001-01-01T00:00:00</CancelDate>
    <ContractNumber i:nil="true"/>
    <ErrorMessage xmlns:a="http://schemas.datacontract.org/2004/07/EaglePartnerData">
      <a:ErrStatus>true</a:ErrStatus>
      <a:ErrorCode>91</a:ErrorCode>
      <a:ErrorMsg>Error Cancelling PolicyThere was no endpoint listening at http://10.10.4.40:10059/web/services/ServiceContract that could accept the message.</a:ErrorMsg>
    </ErrorMessage>
    <PolicyStatus i:nil="true"/>
    <RefundAmount>0</RefundAmount>
    </PolicyCancelReply> 
   */
    
    private PYBCancellationResponse PYBCancelResponse(String response){
        DOM.Document doc = new DOM.Document();     
        try {
            doc.load(response);  
            system.debug('<<<<<<Respose in DOM Object>>>>>>>'+response); 
            system.debug('<<<<<<DOM>>>>>>>'+ doc);
            PYBCancellationResponse cancelRes = new PYBCancellationResponse(); 
            for(dom.XmlNode node : doc.getRootElement().getChildElements() ){
             system.debug('<<<<<XML Node>>>>>'+ node + '<<<<<>>>>>>>'+doc.getRootElement().getChildElements()+'<<<<<<>>>>>'+node.getName());	
             if(node.getName()=='CancelDate') {
                cancelRes.PPPCancelDate = node.getText();
                system.debug('<<<<CancelDate>>>>>>>'+cancelRes.PPPCancelDate);
             } 
             else if(node.getName()=='ErrorMessage') {
                  	 for(dom.XmlNode ErrResponse : node.getChildElements()) {
                       system.debug('<<<<<Error Response Node>>>>>>'+ErrResponse);
                       if(ErrResponse.getName() == 'ErrStatus') {
                          cancelRes.ErrStatus = Boolean.ValueOf(ErrResponse.getText());
                          system.debug('<<<<<<DOM Error Status in Response>>>>>'+cancelRes.ErrStatus);
                       }else if (ErrResponse.getName()=='ErrorCode') {
                          cancelRes.ErrorCode = ErrResponse.getText();
                          system.debug('<<<<<<DOM Error Code in Response>>>>>'+cancelRes.ErrorCode);
                       }else if (ErrResponse.getName()=='ErrorMsg') {
                          cancelRes.ErrorMsg = ErrResponse.getText(); 
                          system.debug('<<<<<<DOM Error Message in Response>>>>>'+cancelRes.ErrorMsg);
                       }		      
                     }          
             }
             else if(node.getName() == 'RefundAmount') {
             	cancelRes.RefundAmount = decimal.valueOf(node.getText());
             	system.debug('<<<<<<DOM Refund Amount in Response>>>>>'+cancelRes.RefundAmount);
             }           
            }
        system.debug('<<<<<<Parsed Cancellation Response>>>>>'+cancelRes);    
        return cancelRes;
        } catch(System.XMLException e) {
            system.debug(e.getMessage());
            IntegrationAlert.addException('PYB Cancellation Process', 'PYB Cancellation Response failed during response parsing>>>>>>'+response, e);
            return null;
        }
    }
}