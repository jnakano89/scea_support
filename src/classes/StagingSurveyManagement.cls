public with sharing class StagingSurveyManagement {
 

    public static void afterInsert(List<Staging_Survey__c> surveyList){
		sendSurvey(surveyList);	
    } 

    public static void afterUpdate(List<Staging_Survey__c> surveyList, Map<Id, Staging_Survey__c> oldSurveyMap){
		sendSurvey(surveyList);			
	}   
	
	public static void sendSurvey(List<Staging_Survey__c> surveyList){
      
		Set<Id> caseIds = new Set<Id>();

		for(Staging_Survey__c ss : surveyList) {
			
      		if(ss.Processed_Date_Time__c == null){
      			
	     		if(ss.Case__c <> null) {
	     			//We will be unable to do callouts form Trigger.
	     			if(system.trigger.isExecuting){
		     			//If we are already in Batch/Future context, we can't invoke another.
		     	  		if(System.isFuture() || System.isBatch()) { 
		     	  			//Will not be able to invoke Survey callouts..
							//Need to handle Invoke manually.
		     	  		}else{
		     	  			//Create callout in future method.
	     	  				SurveyUtility.sendSurvey(ss.Case__c, ss.Id);
		     	  		}
	     			}else{
	     				//Save future method, and run-survey from controller.
		     	  		SurveyUtility.sendSurveyBO(ss.Case__c, ss.Id);
		     	  	}
     	
	     		}//end-if
      
      		}
      	}//end-for
 	 
    }//end-method  
}