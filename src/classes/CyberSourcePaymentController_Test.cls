//***************************************************************************************************/
// (c) 2013 Appirio, Inc.
// Description : Test class for CyberSourcePaymentController.
// Original August 30, 2013  : Vinod Kumar(JDC)
// 
//UPDATES:
//  04/23/2015 : Leena Mandadapu : Updated to fix the Too Many SOQLs issue during the test run.
// **********************************************************************************************/

@isTest
private class CyberSourcePaymentController_Test {
    
    //LM 04/23/2015 : Added global variables for test data creation
    static List<Product__c> pdctLst;
    static Contact testContact;
    static Address__c  address;
    static Address__c  add;
    static Case testCase;
    static Case testCase2;
    
    static void createTestData(){
    	pdctLst = TestClassUtility.createProduct(1,true);
        //create test data
        testContact = TestClassUtility.createContact(1,  true)[0];
        
        address = new Address__c(); 
        address.Consumer__c = testContact.Id;
        address.Address_Line_1__c = '';
        address.City__c = 'TestCity';
        address.State__c = 'TS';
        address.Postal_Code__c = '12341';
        address.Country__c = 'USA';
        address.X2_Letter_Country_Code__c = 'US';
        insert address;
        
        testCase = TestClassUtility.createCase('Open', 'Social', false);
        testCase.ContactId = testContact.ID;
        testCase.Offender__c = 'Test';
        testCase.Bill_To__c = address.Id;
        testCase.Product__c = 'PS4';
        testCase.Sub_Area__c = 'Account Help';
        insert testCase;
        
        add = new Address__c(); 
        add.Consumer__c = testContact.Id;
        add.Address_Line_1__c = '';
        add.City__c = 'TestCity';
        add.State__c = 'TS';
        add.Postal_Code__c = '12341';
        add.Country__c = 'Canada';
        add.X2_Letter_Country_Code__c = 'CA';
        insert add;
        
        testCase2 = TestClassUtility.createCase('Open', 'Social', false);
        testCase2.ContactId = testContact.ID;
        testCase2.Offender__c = 'Test';
        testCase2.Bill_To__c = add.Id;
        testCase2.Product__c = 'PS4';
        testCase2.Sub_Area__c = 'Account Help';
        testCase2.SCEA_Product__c = pdctLst[0].id;
        insert testCase2;
    }

    static testMethod void myUnitTest() {
    	
    	createTestData();	
		//ApexPages.currentPage().getParameters().put('caseId', testCase2.Id);
		
		Cybersource__c testSetting = new Cybersource__c();
        testSetting.Access_Key__c = 'f77c9224ec5a3979bc838596eced6999';
  		testSetting.Algorithm_Name__c = 'hmacSHA256';
  		testSetting.Profile_ID__c = 'SFDC001';
  		testSetting.Locale__c = 'en';
  		testSetting.Number_of_Retries__c = 5;
  		testSetting.Payment_Response_Version_Number__c = '1.9'; 
        testSetting.Payment_Response_Type__c = 'transaction';
        testSetting.Payment_Response_Sub_Type__c = 'transactionDetail';
        testSetting.Payment_Response_URL__c =  'https://ebctest.cybersource.com/ebctest/Query';
  		insert testSetting;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HTTPMockCyberSource());
        
		PageReference pg = Page.CyberSourcePayment; 
        Test.setCurrentPage(pg);
        ApexPages.currentPage().getParameters().put('caseId', testCase.Id);     
        
        CyberSourcePaymentController objController = new CyberSourcePaymentController();
        objController.selectedCase = testCase;
        objController.showPaymentPage();
        objController.showDetails();
        objController.returnToCase();
        objController.cancel();
        objController.complete();
        objController.showDetails();
        order__c o = new order__c();
        
        o.Order_Status__c='new';
        insert o;
        
        order_line__c ol = new order_line__c();
        ol.List_Price__c= 12;
        ol.order__c = o.id;
        insert ol;
        
        ol.List_Price__c=10;
        update ol;
        
        ApexPages.currentPage().getParameters().put('caseId', testCase2.Id);
        ApexPages.currentPage().getParameters().put('TransactionType', 'authorization,create_payment_token');
        ApexPages.currentPage().getParameters().put('purchaseType', 'PPP');
        ApexPages.currentPage().getParameters().put('cashPayment', 'testCashPayment');
        ApexPages.currentPage().getParameters().put('Settle', 'testSettle');
        CyberSourcePaymentController objController2 = new CyberSourcePaymentController();
        objController2.showDetails();
        Test.stopTest();
        
        //Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HTTPMockCyberSource());
        ApexPages.currentPage().getParameters().put('PaymentToken', 'testPaymentToken');
        ApexPages.currentPage().getParameters().put('caseId', testCase2.Id);
        objController2 = new CyberSourcePaymentController();
        objController2.showDetails();
        objController2.returnToCase();
        
        //Test.stopTest();
    }
    
     static testMethod void CyberSourcePaymentCompleteTest() {
     	testContact = TestClassUtility.createContact(1,  true)[0];
     	add = new Address__c(); 
        add.Consumer__c = testContact.Id;
        add.Address_Line_1__c = '';
        add.City__c = 'TestCity';
        add.State__c = 'TS';
        add.Postal_Code__c = '12341';
        add.Country__c = 'Canada';
        add.X2_Letter_Country_Code__c = 'CA';
        insert add;
        
        testCase2 = TestClassUtility.createCase('Open', 'Social', false);
        testCase2.ContactId = testContact.ID;
        testCase2.Offender__c = 'Test';
        testCase2.Bill_To__c = add.Id;
        testCase2.Product__c = 'PS4';
        testCase2.Sub_Area__c = 'Account Help';
        insert testCase2;
        
        ApexPages.currentPage().getParameters().put('caseId', testCase2.Id);
        
     	CyberSourcePaymentCompleteController ctrl = new CyberSourcePaymentCompleteController();
     	ctrl.complete();
     	
     }
}