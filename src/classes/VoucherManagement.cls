public with sharing class VoucherManagement {
    public static void UpdateCaseAttributes(List<Voucher__c> voucherList){
        Set<Id> CaseIds = new Set<Id>();
        List<Case> UpdateCaseList = new List<Case>();
        for(Voucher__c v:voucherList){
            if(v.Status__c == 'Distributed' && v.Case__C != null ){
                CaseIds.add(v.Case__c);
                for (Case c : [Select Voucher_Type__c from Case where Id IN :CaseIds]){
                    c.Voucher_Type__c = v.Type__c;
                    UpdateCaseList.add(c);
                }   
            }
        }
        update updateCaseList;
    }  
}