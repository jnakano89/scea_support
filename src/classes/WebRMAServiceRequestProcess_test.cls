/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@IsTest(SeeAllData=true)
global class WebRMAServiceRequestProcess_test {

    static testMethod void createServiceRequest_Test() {
    	Test.startTest();
		Test.setMock(HttpCalloutMock.class, new HTTPMockCyberSource());  

    	WebRMAServiceRequestProcess.ServiceRequestInput request = new WebRMAServiceRequestProcess.ServiceRequestInput();

    	//Failed as External_SR_Number not provided
    	WebRMAServiceRequestProcess.createServiceRequestCase(request);
    	
    	//Failed as serviceCost not defined.
    	request.External_SR_Number = 'W-1212334223434';
    	WebRMAServiceRequestProcess.createServiceRequestCase(request);
    	
    	//Unable to create Consumer, request.firstName, request.lastName, request.phoneNumber, request.email
    	request.serviceCost = '10';
    	WebRMAServiceRequestProcess.createServiceRequestCase(request);

    	request=getSampleRequest();
    	request.External_SR_Number = 'W-1212334223434';
    	request.serviceCost = '10';
    	WebRMAServiceRequestProcess.ServiceRequestCreateCaseResponse res = WebRMAServiceRequestProcess.createServiceRequestCase(request);
    	
		List<Order__c> orders = [select id, Bill_To__c, Consumer__c, External_Order_Number__c, Case__c, Asset__r.Product__c from Order__c limit 1];
		if(orders.size()>0){
			List<Account>AccConsumers= [Select Id, ispersonaccount, PersoncontactId from Account where PersoncontactId=:orders[0].Consumer__c and ispersonaccount=true];
			if(AccConsumers.size()>0){						
				WebRMAServiceRequestProcess.actionOnCyberSource(orders[0].id, orders[0].Consumer__c, AccConsumers[0].id, orders[0].Asset__r.Product__c, 
						orders[0].External_Order_Number__c, orders[0].Case__c, JSON.serialize(request));
			}
		}
		Test.stopTest();
    	//Todo Asset for validate if all related recodrs created.
    }

    class HTTPMockCyberSource implements HttpCalloutMock  {
	   public HTTPResponse respond(HTTPRequest req) {
      		System.assertEquals('https://ebctest.cybersource.com/ebctest/Query', req.getEndpoint());
	        System.assertEquals('POST', req.getMethod());
	        HttpResponse res = new HttpResponse();
	        //res.setHeader('Content-Type', 'application/json');
	        res.setBody(paymentResponse);
	        res.setStatusCode(200);
	        return res;
	   }
    }
    
    
    /*static testMethod void searchExistingServiceCase_Test() {
    	WebRMAServiceRequestProcess.ServiceRequestResponse res = WebRMAServiceRequestProcess.searchExistingServiceCase('NotFoundKey');
    }*/

    static WebRMAServiceRequestProcess.ServiceRequestInput getSampleRequest(){
    	WebRMAServiceRequestProcess.ServiceRequestInput request = new WebRMAServiceRequestProcess.ServiceRequestInput();
		request.systemType = '3434';
		request.modelNumber = '2323232';
		request.serialNumber = '!!TestSerialNo!!';
		request.purchaseDate = DateTime.now().date();
		request.modelNumber  = 'TestSKU';
		request.firstName  = 'FirstName';
		request.lastName  = 'LastName Test';
		request.phoneNumber  = '234243432';
		request.email  = 'testEmail@SonyTest.com';
		
    	return request;
    }
    
    public static String paymentResponse = '<?xml version="1.0" encoding="UTF-8"?>'+
    		'<!DOCTYPE Report SYSTEM "https://ebctest.cybersource.com/ebctest/reports/dtd/tdr_1_9.dtd">'+
    '<Report xmlns="https://ebctest.cybersource.com/ebctest/reports/dtd/tdr_1_9.dtd" Name="Transaction Detail" Version="1.9" MerchantID="scea_test" ReportStartDate="2014-02-19 09:17:27.218-08:00" ReportEndDate="2014-02-19 09:17:27.218-08:00">'+
     ' <Requests>'+
     '   <Request MerchantReferenceNumber="CS-001992" RequestDate="2014-02-03T23:55:01-08:00" RequestID="3915005012270178147626" SubscriptionID="9997000078114572" Source="Secure Acceptance Web/Mobile" TransactionReferenceNumber="Y33YNR8JERT2">'+
     '     <BillTo>'+
     '       <FirstName>TRAVIS</FirstName>'+
     '       <LastName>BICKLE</LastName>'+
     '       <Address1>77 Massachusetts Ave</Address1>'+
     '       <City>Cambridge</City>'+
     '       <State>MA</State>'+
     '       <Zip>02139</Zip>'+
     '       <Email>ranjeet.singh@appirio.com</Email>'+
     '       <Country>US</Country>'+
     '       <Phone />'+
     '     </BillTo>'+
     '     <PaymentMethod>'+
     '       <Card>'+
     '         <AccountSuffix>1111</AccountSuffix>'+
     '         <ExpirationMonth>01</ExpirationMonth>'+
     '         <ExpirationYear>2016</ExpirationYear>'+
     '         <CardType>Visa</CardType>'+
     '       </Card>'+
     '     </PaymentMethod>'+
     '     <LineItems>'+
     '       <LineItem Number="0">'+
     '         <FulfillmentType />'+
     '         <Quantity>1</Quantity>'+
     '         <UnitPrice>436.99</UnitPrice>'+
     '         <TaxAmount>0.00</TaxAmount>'+
     '         <ProductCode>default</ProductCode>'+
     '       </LineItem>'+
     '     </LineItems>'+
     '     <ApplicationReplies>'+
     '       <ApplicationReply Name="ics_auth">'+
     '         <RCode>1</RCode>'+
     '         <RFlag>SOK</RFlag>'+
     '         <RMsg>Request was processed successfully.</RMsg>'+
     '       </ApplicationReply>'+
     '       <ApplicationReply Name="ics_pay_subscription_create">'+
     '         <RCode>1</RCode>'+
     '         <RFlag>SOK</RFlag>'+
     '         <RMsg>Request was processed successfully.</RMsg>'+
     '       </ApplicationReply>'+
     '     </ApplicationReplies>'+
     '     <PaymentData>'+
     '       <PaymentRequestID>3915005012270178147626</PaymentRequestID>'+
     '       <PaymentProcessor>fdiglobal</PaymentProcessor>'+
     '       <Amount>436.99</Amount>'+
     '       <CurrencyCode>USD</CurrencyCode>'+
     '       <TotalTaxAmount>0.00</TotalTaxAmount>'+
     '       <AuthorizationCode>831000</AuthorizationCode>'+
     '       <AVSResult>Y</AVSResult>'+
     '       <AVSResultMapped>Y</AVSResultMapped>'+
     '       <RequestedAmount>436.99</RequestedAmount>'+
     '       <RequestedAmountCurrencyCode>USD</RequestedAmountCurrencyCode>'+
     '     </PaymentData>'+
     '   </Request>'+
     ' </Requests>'+
     '</Report>';
}