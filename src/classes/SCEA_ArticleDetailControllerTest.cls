/*
    @Author: Venkat Reddy
    @Version: 1.0
    @Description: Test class for SCEA_ArticleDetailController
*/
@isTest
public class SCEA_ArticleDetailControllerTest{
    private static testMethod void constructorTestMethod(){
        KC_Article__kav kcArticle = new KC_Article__kav();
        kcArticle.language = 'en_US';
        kcArticle.Title = 'Test KC Article';
        kcArticle.UrlName = 'Test-KC-Article';
        insert kcArticle;
        
        kcArticle = [SELECT KnowledgeArticleId, Language, PublishStatus, ArticleNumber FROM KC_Article__kav WHERE Id = :kcArticle.Id];
        system.debug(kcArticle.Language+'---'+kcArticle.PublishStatus);
        Test.startTest();
        KbManagement.PublishingService.publishArticle(kcArticle.KnowledgeArticleId, true);
        
        kcArticle = [SELECT KnowledgeArticleId, Language, PublishStatus, ArticleNumber FROM KC_Article__kav WHERE Id = :kcArticle.Id];
        system.debug(kcArticle.Language+'---'+kcArticle.PublishStatus);
        
        KnowledgeArticle ka = [select Id from KnowledgeArticle where ArticleNumber=:kcArticle.ArticleNumber limit 1];
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(ka);
        SCEA_ArticleDetailController con = new SCEA_ArticleDetailController(stdController);
        
        con.feedbackComments = 'Test Feedback Comments';
        con.helpedMeFlag = true;
        con.answeredMyQuestionFlag = true;
        con.answeredMyQuestionButFlag = true;
        con.didntHaveInfoFlag = true;
        con.processYesFeedback();
    }
    
    private static testMethod void constructorTestMethodForDecisionTree(){
        DecisionTreeArticleTypePageSettings__c cs = new DecisionTreeArticleTypePageSettings__c();
        cs.ExternalPageName__c = 'ExternalPage';
        cs.InternalPageName__c = 'InternalPage';
        cs.Name = 'TestFlow';
        insert cs;
        
        Decision_Tree__kav dtArticle = new Decision_Tree__kav();
        dtArticle.language = 'en_US';
        dtArticle.Title = 'Test DT Article';
        dtArticle.UrlName = 'Test-DT-Article';
        dtArticle.Flow_Name__c = 'TestFlow';
        insert dtArticle;
        
        dtArticle = [SELECT KnowledgeArticleId, Language, PublishStatus, ArticleNumber FROM Decision_Tree__kav WHERE Id = :dtArticle.Id];
        system.debug(dtArticle.Language+'---'+dtArticle.PublishStatus);
        Test.startTest();
        KbManagement.PublishingService.publishArticle(dtArticle.KnowledgeArticleId, true);
        
        dtArticle = [SELECT KnowledgeArticleId, Language, PublishStatus, ArticleNumber FROM Decision_Tree__kav WHERE Id = :dtArticle.Id];
        system.debug(dtArticle.Language+'---'+dtArticle.PublishStatus);
        
        KnowledgeArticle ka = [select Id from KnowledgeArticle where ArticleNumber=:dtArticle.ArticleNumber limit 1];
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(ka);
        SCEA_ArticleDetailController con = new SCEA_ArticleDetailController(stdController);
        
        con.feedbackComments = 'Test Feedback Comments';
        con.helpedMeNFlag = true;
        con.answeredMyQuestionNFlag = true;
        con.answeredMyQuestionButNFlag = true;
        con.didntHaveInfoNFlag = true;
        con.processNoFeedback();
    }
    
    private static testMethod void getProductCategoryOptionsTestMethod(){
        Category__c c = new Category__c();
        c.Name = 'PlayStatoin Store';
        c.Category_API_Name__c = 'PlayStation_Store__c';
        c.es_Name__c = 'PlayStation Store';
        c.pt_BR_Name__c = 'PlayStation Store';
        c.Display_in_Product__c = true;
        c.Display_in_Account__c = false;
        c.Display_Order__c = 1;
        c.Channel__c = 'Chat';
        insert c;
        
        KC_Article__kav kcArticle = new KC_Article__kav();
        kcArticle.language = 'en_US';
        kcArticle.Title = 'Test KC Article';
        kcArticle.UrlName = 'Test-KC-Article';
        insert kcArticle;
        
        kcArticle = [SELECT KnowledgeArticleId, Language, PublishStatus, ArticleNumber FROM KC_Article__kav WHERE Id = :kcArticle.Id];
        system.debug(kcArticle.Language+'---'+kcArticle.PublishStatus);
        Test.startTest();
        KbManagement.PublishingService.publishArticle(kcArticle.KnowledgeArticleId, true);
        
        kcArticle = [SELECT KnowledgeArticleId, Language, PublishStatus, ArticleNumber FROM KC_Article__kav WHERE Id = :kcArticle.Id];
        system.debug(kcArticle.Language+'---'+kcArticle.PublishStatus);
        
        KnowledgeArticle ka = [select Id from KnowledgeArticle where ArticleNumber=:kcArticle.ArticleNumber limit 1];
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(ka);
        SCEA_ArticleDetailController con = new SCEA_ArticleDetailController(stdController);
        
        List<SelectOption> returnList = con.getProductCategoryOptions();
        system.assert(returnList.size()>0);
        
        PageReference returnPageRef = con.searchAgainForArticles();
        system.assert(returnPageRef != null);
    }
}