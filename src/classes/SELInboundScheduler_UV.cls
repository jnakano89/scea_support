global class SELInboundScheduler_UV  implements Database.Batchable<sObject>, Schedulable{

	global void execute(SchedulableContext SC) { // Start Batch
		Database.executeBatch(new SELInboundScheduler_UV(), 100);
	}

	global Database.QueryLocator start(Database.BatchableContext BC){
		String qry = 'Select  s.SeqPack__c, s.Product_PPP_List_Price__c, s.Processed_Date_Time__c, s.Phone2UseType__c, ' + 
				's.Phone2Type__c, s.Phone1UseType__c, s.Phone1Type__c, s.OwnerId, s.Order_Sale_Type__c, s.Order_RepTag2__c, s.Order_RepTag1__c, ' + 
				' s.Order_Record_Type__c, s.Order_QuantSold__c, s.Order_Prod_Code__c, s.Order_PartWarranty__c, s.Order_Manufacture_Code__c,' + 
				's.Order_LnNum__c, s.Order_Labor_Warranty__c, s.Order_File_Id__c, s.Order_Dealer_Id__c, s.Order_Client_Batch_Id__c,' +
				' s.Order_Action_Code__c, s.Name, s.MiddleInit__c, s.LastModifiedDate, s.LastModifiedById, s.Id, ' +
				' s.External_ID__c, s.Errors__c, s.CreatedDate, s.Consumer_Phone_2__c, s.Consumer_Phone1__c, ' +
				's.Consumer_Last_Name__c, s.Consumer_Language__c, s.Consumer_First_Name__c, s.Consumer_Email__c, s.ConsumerTitle__c,' +
				' s.ConsumerId__c, s.BusIndicator__c, s.BusDBA__c, s.Asset_Unit_Serial_Number__c, s.Asset_Unit_Price__c, ' +
				's.Asset_Unit_Model_Number__c, s.Asset_Proof_Of_Purchase_Date__c, s.Asset_PPP_SKU__c, s.Asset_PPP_Purchase_Date__c,' +
				' s.Asset_PPP_Contract_Number__c, s.Asset_PPP_Cancel_Date__c, s.Address_Zip_Code__c, s.Address_State__c, s.Address_Last_4__c,' +
				' s.Address_Country__c, s.Address_City__c, s.Address_Address_2__c, s.Address_Address_1__c From Staging_PPP_SEL_Inbound__c s where Processed_Date_Time__c = null';

		return Database.getQueryLocator(qry);	 		
	}
	 

	global void execute(Database.BatchableContext BC, List<sObject> scope){
		String personAccountRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
		
		list<SELInboundWrapper> wrapperList = new list<SELInboundWrapper>();
		list<Staging_PPP_SEL_Inbound__c> rejectedRecords = new list<Staging_PPP_SEL_Inbound__c>();
		list<Staging_PPP_SEL_Inbound__c> validRecords = new list<Staging_PPP_SEL_Inbound__c>();
		list<Staging_PPP_SEL_Inbound__c> validRecords2 = new list<Staging_PPP_SEL_Inbound__c>();
		
		Set<String> firstNameSet = new Set<String>();
		Set<String> lastNameSet = new Set<String>();
		Set<String> phoneSet = new Set<String>();
		Set<String> emailSet = new Set<String>();
		Set<String> serialNumberSet = new Set<String>();
		Set<String> addressline1Set = new Set<String>();
		Set<String> zipCodeSet = new Set<String>();
		
		map<String, String> modelNumberMap = new map<String, String>();
		map<String, String> addressMap = new map<String, String>();
		
		for(Staging_PPP_SEL_Inbound__c inbound : (list<Staging_PPP_SEL_Inbound__c>)scope) {
			inbound.Processed_Date_Time__c = system.now();
			if(!validate(rejectedRecords, inbound)){
				rejectedRecords.add(inbound);
				continue;
			}
			
			String uniqueKey = inbound.Order_Sale_Type__c + inbound.Asset_PPP_Contract_Number__c + inbound.Order_LnNum__c + inbound.Order_QuantSold__c;
			System.debug('========uniqueKey========' + uniqueKey);
			
			//Do not use unique key to ID asset - test only serial number to find any matching asset.(T-238943)
			
			serialNumberSet.add(inbound.Asset_Unit_Serial_Number__c);
			
			
			String modelNumber = inbound.Asset_Unit_Model_Number__c.length() >= 3 ? inbound.Asset_Unit_Model_Number__c.substring(0,3) : '';
			//modelNumber += '%';
			System.debug('========modelNumber========' + modelNumber);

			modelNumberMap.put(inbound.Asset_PPP_SKU__c, modelNumber);

			lastNameSet.add(inbound.Consumer_Last_Name__c);
			firstNameSet.add(inbound.Consumer_First_Name__c);
			
			if(inbound.Consumer_Phone1__c <> null && inbound.Consumer_Phone1__c <> '') {
				phoneSet.add(inbound.Consumer_Phone1__c);
			}
			
			if(inbound.Consumer_Email__c <> null && inbound.Consumer_Email__c <> '') {
				emailSet.add(inbound.Consumer_Email__c);
			}

			addressline1Set.add(inbound.Address_Address_1__c);
			zipCodeSet.add(inbound.Address_Zip_Code__c);

			validRecords.add(inbound);
		}
		try{
			Set<String> existingSerialNumberSet = new Set<String>();
			Set<String> skuValues = new Set<String>();
			Set<String> modelNumberSet = new Set<String>();
			map<String, Account> consumerMap = new map<String, Account>();
			
			for(Asset__c ast : [select Serial_Number__c from Asset__c where Serial_Number__c IN : serialNumberSet]) {
				existingSerialNumberSet.add(ast.Serial_Number__c);
			}

			Map<String, String> scea_sku_map = new map<String, String>();
			Map<String, Product__c> product_map = new map<String, Product__c>();

			System.debug('============modelNumberMap= Keys===========' + modelNumberMap.keySet());
			System.debug('============modelNumberMap===========' + modelNumberMAP);

			for(SEL_Logic__c sl : [select SCEA_SKU__c, SEL_SKU__c, SEL_Model_Number__c
			                       from SEL_Logic__c 
			                       where SEL_SKU__c IN : modelNumberMap.keySet()
			                       /* and SEL_Model_Number__c like IN : modelNumberSet*/]) {
				String modelNumber = modelNumberMap.get(sl.SEL_SKU__c);
				System.debug('>>>>>>>>>>>. modelNumber >>>>>>> ' + modelNumber);
				if(sl.SEL_Model_Number__c <> null && sl.SEL_Model_Number__c.startsWith(modelNumber)) {
					scea_sku_map.put(sl.SEL_SKU__c + modelNumber, sl.SCEA_SKU__c);
				}
			}

			//  Product__c prod: select Sub_type__c,SKU__c, Duration__c from Product__c  where SKU__c IN :scea_sku_map.values()


			for(Product__c prod : [select Sub_type__c,SKU__c, Duration__c from Product__c  where SKU__c IN :scea_sku_map.values()]) {
				product_map.put(prod.SKU__c, prod);
			}
			System.debug('============product_map===========' + product_map);

			for(Account cnt : [select id, FirstName, LastName, Phone, PersonEmail, Bill_to__pc, PersonContactId 
			                   from Account 
			                   where (PersonEmail IN :emailSet AND IsPersonAccount = true ) OR (FirstName IN: firstNameSet AND LastName IN : lastNameSet
			                		   AND Phone IN : phoneSet AND IsPersonAccount = true) OR (FirstName IN: firstNameSet AND LastName IN : lastNameSet)
			                		   ]) {
				consumerMap.put(getValidKey(new String[]{cnt.FirstName, cnt.lastName, cnt.Phone}), cnt);
				consumerMap.put(getValidKey(new String[]{cnt.PersonEmail}), cnt);
				consumerMap.put(getValidKey(new String[]{cnt.FirstName, cnt.lastName}), cnt);	
			}
			System.debug('============consumerMap===========' + consumerMap);
			for(Address__c add : [select Address_Line_1__c, Postal_Code__c  from Address__c where Address_Line_1__c IN : addressline1Set 
			                      AND Postal_Code__c IN : zipCodeSet]) {
				addressMap.put(getValidKey(new String[]{add.Address_Line_1__c , add.Postal_Code__c}), add.Id);
			}
			System.debug('============addressMap===========' + addressMap);
			
			map<String,String>accCtToAddr = new map<String,String>();
			
			
			for(Staging_PPP_SEL_Inbound__c inbound : validRecords) {
				String uniqueKey = inbound.Order_Sale_Type__c + inbound.Asset_PPP_Contract_Number__c + inbound.Order_LnNum__c + inbound.Order_QuantSold__c;
				inbound.Processed_Date_Time__c = System.now();
				if(existingSerialNumberSet.contains(inbound.Asset_Unit_Serial_Number__c)) {
					inbound.Errors__c += 'Asset exists - please review :'+inbound.Asset_Unit_Serial_Number__c+'. \n' ;
					rejectedRecords.add(inbound);
					continue;
				}
				System.debug('=========scea_sku_map=============' + scea_sku_map);	
				String modelNumber = inbound.Asset_Unit_Model_Number__c.length() >= 3 ? inbound.Asset_Unit_Model_Number__c.substring(0,3) : '';
				if(!scea_sku_map.containsKey(inbound.Asset_PPP_SKU__c + modelNumber)) {
					inbound.Errors__c += 'No Matching PPP SKU Found.\n' ;  
					rejectedRecords.add(inbound);
					continue;
				}
				
				SELInboundWrapper inboundWrap = new SELInboundWrapper();
				inboundWrap.key = uniqueKey;
				
				//get Existing Contact.
				String cntKey = '';
				if(inbound.Consumer_Email__c <> null) {
					cntKey = getValidKey(new String[]{inbound.Consumer_Email__c});
				} else if(inbound.Consumer_Phone1__c <> null) {
					cntKey = getValidKey(new String[]{inbound.consumer_first_name__c, inbound.consumer_Last_name__c, inbound.Consumer_Phone1__c});
				} else {
					cntKey = getValidKey(new String[]{inbound.consumer_first_name__c, inbound.consumer_Last_name__c});
				}
				inboundWrap.accConsumer = consumerMap.get(cntKey);
				if(inboundWrap.accConsumer == null){
					cntKey = getValidKey(new String[]{inbound.Consumer_Email__c}); 
					inboundWrap.accConsumer = consumerMap.get(cntKey);
				}
				System.debug('======================accConsumer===========' + inboundWrap.accConsumer);
				//use consumer e-mail  address to identify and match existing consumer. IF no email address, use concatenated first name, last name, and phone. If no match, create new consumer.
				try {
					if(inboundWrap.accConsumer == null) {
						//Create Account, so that related can be created
						inboundWrap.accConsumer = new Account();
						inboundWrap.accConsumer.RecordTypeId = personAccountRecId;
						inboundWrap.accConsumer.FirstName = inbound.Consumer_First_Name__c;
						inboundWrap.accConsumer.LastName = inbound.Consumer_Last_Name__c;
						inboundWrap.accConsumer.Phone = inbound.Consumer_Phone1__c;
						inboundWrap.accConsumer.PersonEmail = inbound.Consumer_Email__c;
						inboundWrap.isNewConsumer = true;
					} 
					//Address dups by Addr1 and PostalCode nit email?
					if(addressMap.get(getValidKey(new String[]{inbound.Address_Address_1__c, inbound.Address_Zip_Code__c})) != null){
						inboundWrap.address.Id = addressMap.get(getValidKey(new String[]{inbound.Address_Address_1__c + '-' + inbound.Address_Zip_Code__c}));
					} else {
						inboundWrap.isNewAddress = true;
					}
					inboundWrap.address.Address_Line_1__c=inbound.Address_Address_1__c;
					inboundWrap.address.Postal_Code__c= inbound.Address_Zip_Code__c;
					inboundWrap.address.Address_Line_2__c = inbound.Address_Address_2__c;
					inboundWrap.address.City__c = inbound.Address_City__c;
					inboundWrap.address.Country__c = inbound.Address_Country__c;
					inboundWrap.address.State__c = inbound.Address_State__c;
				} catch(Exception ex){
					system.debug('ERROR IN CONTACT CREATION********* ' + ex.getMessage()); 
				}
				// Inserting asset
				if(inbound.Asset_Unit_Serial_Number__c <> null && inbound.Asset_Unit_Serial_Number__c <> '' && !inbound.Asset_Unit_Serial_Number__c.startsWith('NA')) {
					inboundWrap.asset.Serial_Number__c = inbound.Asset_Unit_Serial_Number__c;
				} else {
					inboundWrap.asset.Serial_Number__c= inbound.Name;
					inbound.Asset_Unit_Serial_Number__c = inbound.Name;
				}
				inboundWrap.asset.PPP_Contract_Number__c= inbound.Asset_PPP_Contract_Number__c;
				inboundWrap.asset.Unique_Key__c = uniqueKey;

				inboundWrap.asset.Model_Number__c = inbound.Asset_Unit_Model_Number__c;
				inboundWrap.asset.Asset_Status__c = 'Active';
				String sku = scea_sku_map.get(inbound.Asset_PPP_SKU__c + modelNumber);

				System.debug(sku + '=========product_map.get(inbound.Asset_PPP_SKU__c)=============' + product_map.get(sku));

				Product__c prod = product_map.get(sku);
				
				//ContractPurchDate
				//inboundWrap.asset.PPP_Purchase_Date__c = ValidationRulesUtility.formatDate(inbound.Asset_PPP_Purchase_Date__c);
				boolean isInvalidDate = false;
				if(inbound.Asset_Proof_Of_Purchase_Date__c <> null && inbound.Asset_Proof_Of_Purchase_Date__c <> '0' && inbound.Asset_Proof_Of_Purchase_Date__c <> '00000000') {
					if(ValidationRulesUtility.validateDateFormat(inbound.Asset_Proof_Of_Purchase_Date__c)) {
						inboundWrap.asset.Purchase_Date__c = ValidationRulesUtility.formatDate(inbound.Asset_Proof_Of_Purchase_Date__c);
					} else {
						inbound.Errors__c += 'EquipPurchDate is not in valid format. \n';
						isInvalidDate = true;
					}
				} else {
					if(ValidationRulesUtility.validateDateFormat(inbound.Asset_PPP_Purchase_Date__c)) {
						inboundWrap.asset.Purchase_Date__c = ValidationRulesUtility.formatDate(inbound.Asset_PPP_Purchase_Date__c);
					} else {
						inbound.Errors__c += 'ContractPurchDate is not in valid format. \n';
					} 
				}
				
				

				if(prod <> null) {	
					if(prod.Sub_Type__c == 'AD') {
						inboundWrap.asset.Coverage_Type__c = 'AD';
						inboundWrap.asset.PPP_Start_Date__c = ValidationRulesUtility.formatDate(inbound.Asset_PPP_Purchase_Date__c);
						inboundWrap.asset.PPP_End_Date__c = inboundWrap.asset.PPP_Start_Date__c == null ? null : inboundWrap.asset.PPP_Start_Date__c.addMonths( (Integer)prod.Duration__c + 12);	
					} else {
						inboundWrap.asset.PPP_Start_Date__c  = ValidationRulesUtility.formatDate(inbound.Asset_Proof_Of_Purchase_Date__c).addMonths(12);
						inboundWrap.asset.PPP_End_Date__c = inboundWrap.asset.PPP_Start_Date__c == null ? null : inboundWrap.asset.PPP_Start_Date__c.addMonths((Integer)prod.Duration__c);
					}
					System.debug('>>>>>>>>>>>+ast.PPP_End_Date__c<<<<<<<<<<<<<' +inboundWrap.asset.PPP_End_Date__c);
					inboundWrap.asset.PPP_Product__c = prod.Id;
				}
				/*If it is Cancel Transaction – the set the ESP cancel date = Cancle date field value in the 
				  data file and Asset status = ‘Pending Cancel’
				  If it is Sale Transaction – the set Asset status = ‘Pending Confim’
				   'A' = Sale Transaction and 'C'=Cancel
				 */
				if(inbound.Order_Sale_Type__c == 'A') {
					inboundWrap.asset.PPP_Status__c = 'Pending Confirm';
				}  else if(inbound.Order_Sale_Type__c == 'C') {
					inboundWrap.asset.ESP_Cancellation_Date__c =  ValidationRulesUtility.formatDate(inbound.Asset_PPP_Cancel_Date__c);
					inboundWrap.asset.PPP_Status__c = 'Pending Cancel';
				}
				
				inboundWrap.ordr.Description__c = 'SEL ESP Sale';
				inboundWrap.ordr.Order_Date__c = Date.today();
				inboundWrap.ordr.Order_Type__c = 'ESP';
				inboundWrap.ordr.Order_Status__c = 'Pending';
				inboundWrap.ordr.ESP_Record_Type__c = inbound.Order_Sale_Type__c == 'A' ? 'S' : inbound.Order_Record_Type__c;
				inboundWrap.ordr.Order_Origin_Source__c='S';
				
				inboundWrap.inboundObj = inbound;
				
				wrapperList.add(inboundWrap);
				
				validRecords2.add(inbound);
			}
			System.debug('=========wrapperList=============' + wrapperList.size());

			//updating inbound records with processing date and failure reasons if any
			if(!rejectedRecords.isEmpty()) update rejectedRecords;
			if(!validRecords2.isEmpty()) update validRecords2;
			
			
			list<Account> consumerListToInsert = new list<Account>();
			list<Asset__c> assetListToInsert = new list<Asset__c>();
			for(SELInboundWrapper wrap : wrapperList) {
				if(wrap.isNewConsumer) {
					consumerListToInsert.add(wrap.accConsumer);
				}
				assetListToInsert.add(wrap.asset);
			}
			
			if(!consumerListToInsert.isEmpty()) insert consumerListToInsert;
			if(!assetListToInsert.isEmpty()) insert assetListToInsert;
			map<Id, Account> personAccountMap = new map<Id, Account>([select Id, personContactId from Account where Id IN : consumerListToInsert]); 
			
			
			list<Address__c> addressListToInsert = new list<Address__c>();
			list<Consumer_Asset__c> caListToInsert = new list<Consumer_Asset__c>();
			
			for(SELInboundWrapper wrap : wrapperList) {
				if(wrap.isNewAddress) {
					if(wrap.accConsumer.personContactId == null) {
						wrap.address.Consumer__c = personAccountMap.get(wrap.accConsumer.Id).personContactId;
					} else {
						wrap.address.Consumer__c = wrap.accConsumer.PersonContactId;
					}
					addressListToInsert.add(wrap.address);
				}
				Consumer_Asset__c ca = new Consumer_Asset__c();
				if(wrap.accConsumer.personContactId == null) {
					ca.Consumer__c = personAccountMap.get(wrap.accConsumer.Id).personContactId;
				} else {
					ca.Consumer__c = wrap.accConsumer.personContactId;
				}
				ca.Asset__c = wrap.asset.Id;
				caListToInsert.add(ca);
			}
			
			if(!addressListToInsert.isEmpty()) insert addressListToInsert;
			if(!caListToInsert.isEmpty()) insert caListToInsert;
			
			
			list<Order__c> orderList = new list<Order__c>();
			
			for(SELInboundWrapper wrap : wrapperList) {
				wrap.ordr.Bill_To__c = wrap.address.Id;
				if(wrap.accConsumer.personContactId == null) {
					wrap.ordr.Consumer__c = personAccountMap.get(wrap.accConsumer.Id).personContactId;
				} else {
					wrap.ordr.Consumer__c = wrap.accConsumer.PersonContactId;
				}
				wrap.ordr.Asset__c = wrap.asset.Id;
				orderList.add(wrap.ordr);
			}
			
			if(!orderList.isEmpty()) insert orderList;
			
			list<Order_Line__c> orderLineList = new list<Order_Line__c>();
			list<Payment__c> paymentList = new list<Payment__c>();
			list<Account> accListToupdate = new list<Account>();
			for(SELInboundWrapper wrap : wrapperList) {

				Order_Line__c oli = new Order_Line__c();
				oli.Order__c = wrap.ordr.Id;
				oli.Ship_Cost__c = wrap.inboundObj.Product_PPP_List_Price__c == null ? 0 : Decimal.valueOf(wrap.inboundObj.Product_PPP_List_Price__c);
				oli.List_Price__c = wrap.inboundObj.Product_PPP_List_Price__c == null ? 0 : Decimal.valueOf(wrap.inboundObj.Product_PPP_List_Price__c);
				oli.Quantity__c = wrap.inboundObj.Order_QuantSold__c;
				oli.Total__c = oli.List_Price__c * Integer.valueOf(oli.Quantity__c);
				oli.Line_Number__c = '1';
				orderLineList.add(oli);

				//Do not create Payment record. (T-237761)
				//SEL Create payment record for I/B orders (T-238939) 
				Payment__c payment = new Payment__c();
				payment.Amount__c = wrap.inboundObj.Product_PPP_List_Price__c == null ? 0 : Decimal.valueOf(wrap.inboundObj.Product_PPP_List_Price__c);
				payment.Authorization_Date__c = date.today();
				payment.Payment_Status__c = 'Charged';
				payment.Order__c = wrap.ordr.Id;
				paymentList.add(payment);
				
				Account acc = wrap.accConsumer;
				acc.Bill_To__pc = wrap.address.Id;
				accListToupdate.add(acc); 
			}
			System.debug('=========orderLineList=============' + orderLineList.size()); 
			if(!orderLineList.isEmpty()) insert orderLineList;
			if(!paymentList.isEmpty()) insert paymentList;
			if(!accListToupdate.isEmpty()) update accListToupdate;
			
			
			//Insert new Account, to generate Contact
			
		} catch(Exception ex) {
			Apex_Log__c al = new Apex_Log__c();
			al.Message__c = ex.getMessage();
			al.Exception_Cause__c = BC.getJobId();
			al.Class_Name__c = 'SELInboundScheduler_UV';
			insert al;

			System.debug('**********Error Occured while running batch*********' + ex.getMessage());
		}
	}

	global void finish(Database.BatchableContext BC){

		boolean isErrorOccurs = false;
		for(Apex_Log__c al : [select id from Apex_Log__c where Exception_Cause__c = :BC.getJobId()]) {
			isErrorOccurs = true;
		}	
		if(!isErrorOccurs) {	
			Integer recordsToBeProcessed = [select count() from Staging_PPP_SEL_Inbound__c s where Processed_Date_Time__c = null];

			if(recordsToBeProcessed > 0) {
				Datetime sysTime = System.now();
				String chron_exp = '';
				sysTime = sysTime.addSeconds(60);
				chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
				system.debug(chron_exp);
				SELInboundScheduler_UV m = new SELInboundScheduler_UV();
				// String sch = '0 0,30 * * * *';
				System.schedule('SEL Inbound Job ' + sysTime.getTime(), chron_exp, m);
			}	
		}
	}

	boolean validate(list<Staging_PPP_SEL_Inbound__c> rejectedRecords, Staging_PPP_SEL_Inbound__c inbound){
			// validates inboud record
			inbound.Errors__c = '';
			if(inbound.Asset_PPP_Contract_Number__c == null || inbound.Asset_PPP_Contract_Number__c.trim() == '') {
				inbound.Errors__c += 'Contract Number is Null. \n' ;
			}
			if(inbound.Order_LnNum__c == null || inbound.Order_LnNum__c.trim() == '') {
				inbound.Errors__c += 'LN Number is Null. \n' ;
			}

			if(inbound.Asset_PPP_SKU__c == null || inbound.Asset_PPP_SKU__c.trim() == '') {
				inbound.Errors__c += 'SKU is Null. \n' ;  
			} else if(inbound.Asset_Unit_Model_Number__c   == null || inbound.Asset_Unit_Model_Number__c.trim() == ''){
				inbound.Errors__c += 'Model Number is Null. \n' ; 
			}

			/* Record is rejected if any of these fields are blank
	            FstName, LastName, Addr1, City, Country, State, Zip
			 */
			if(inbound.Consumer_First_Name__c == null || inbound.Consumer_First_Name__c.trim() == '') {
				inbound.Errors__c += 'First Name is Null. \n' ; 
			}

			if(inbound.Consumer_Last_Name__c == null || inbound.Consumer_Last_Name__c.trim() == '') {
				inbound.Errors__c += 'Last Name is Null. \n' ; 
			}
			/*
			if(inbound.Consumer_Phone1__c == null || inbound.Consumer_Phone1__c.trim() == '') {
				inbound.Errors__c += 'Phone Number cannot be blank. \n' ; 
			}
			*/
			if(inbound.Address_Address_1__c == null || inbound.Address_Address_1__c.trim() == '') {
				inbound.Errors__c += 'Address Line 1 is Null. \n' ;
			}
			if(inbound.Address_City__c == null || inbound.Address_City__c.trim() == '') {
				inbound.Errors__c += 'City is Null. \n' ; 
				 
			}
			if(inbound.Address_State__c == null || inbound.Address_State__c.trim() == '') {
				inbound.Errors__c += 'State is Null. \n' ;
			}
			if(inbound.Address_Country__c == null || inbound.Address_Country__c.trim() == '') {
				inbound.Errors__c += 'Country is Null. \n' ;
			}
			if(inbound.Address_Zip_Code__c == null || inbound.Address_Zip_Code__c.trim() == '') {
				inbound.Errors__c += 'Zip Code is Null. \n' ;  
			}
			/*
			//UV : T-240642 : Add Validation: Serial Number Cannot be Blank
			if(inbound.Asset_Unit_Serial_Number__c   == null || inbound.Asset_Unit_Serial_Number__c.trim() == ''){
				inbound.Errors__c += 'Serial Number cannot be blank. \n' ; 
			}*/
			if(inbound.Asset_Proof_Of_Purchase_Date__c <> null && inbound.Asset_Proof_Of_Purchase_Date__c <> '0' 
				&& inbound.Asset_Proof_Of_Purchase_Date__c <> '00000000' && !ValidationRulesUtility.validateDateFormat(inbound.Asset_Proof_Of_Purchase_Date__c)) {
				inbound.Errors__c += 'EquipPurchDate is not in valid format. \n';
			}
			System.debug('-ValidationRulesUtility.validateDateFormat(inbound.Asset_PPP_Purchase_Date__c)===' + ValidationRulesUtility.validateDateFormat(inbound.Asset_PPP_Purchase_Date__c));
			if(!ValidationRulesUtility.validateDateFormat(inbound.Asset_PPP_Purchase_Date__c)) {
				inbound.Errors__c += 'ContractPurchDate is not in valid format. \n';
			}
			System.debug('======Errors__c=========' + inbound.Errors__c.length());
			if(inbound.Errors__c.length() > 1){
				return false;
				rejectedRecords.add(inbound);
			}
			return true;
		}
		
		static String getValidKey(List<string>values){
			String Result = ''; 
			for(String val : values){
				if(!String.isEmpty(val)){
					Result = Result+val;
				}
				Result = Result+'-';				
			}
			return Result;
		}
		
		public class SELInboundWrapper {
			public Address__c address;
			public Account accConsumer;
			public Asset__c asset;
			public Order__c ordr;
			public String key;
			public boolean isNewConsumer;
			public boolean isNewAddress;
			public Staging_PPP_SEL_Inbound__c inboundObj;
			public SELInboundWrapper() {
				address = new Address__c();
				accConsumer = new Account();
				asset = new Asset__c();
				ordr = new Order__c();
				isNewAddress = false;
				isNewConsumer = false;
			} 
			
		}

}