//**************************************************************************/
// (c) 2013 Appirio, Inc.
//
// Description : Test class for SelectAddressController.
//                  
// Original August 26, 2013  : KapiL Choudhary(JDC) Created for the Task T-175201
// Updated :
//
// ***************************************************************************/
@isTest
private class SelectAddressController_Test {

    static testMethod void selectAddressControllerUnitTest() {
        list<Address__c> addressList = TestClassUtility.createAddress(2, true);
        
        list<Order__c> orderList ;
        if(!addressList.isEmpty()){
        	orderList = TestClassUtility.creatOrder(addressList[0].Consumer__c , 1, true);
        	Test.startTest();
        	SelectAddressController sAddConNone = new SelectAddressController();
        	sAddConNone.selectAddress();
        	Test.stopTest();
        	
        	PageReference pageRef = Page.SelectAddress;
        	Test.setCurrentPageReference(pageRef);
        	
        	 // Add parameters to page URL
        	ApexPages.currentPage().getParameters().put('orderId', orderList[0].id);
        	ApexPages.currentPage().getParameters().put('Field', 'BillTo');
        	ApexPages.currentPage().getParameters().put('ctRadio', addressList[1].id);
        	
        	SelectAddressController sAddCon = new SelectAddressController();
        	sAddCon.selectAddress();
        	sAddCon.returnToRecord();
        	
        	// Add parameters to page URL
        	PageReference pageRef2 = Page.SelectAddress;
        	Test.setCurrentPageReference(pageRef2);
        	ApexPages.currentPage().getParameters().put('orderId', orderList[0].id);
        	ApexPages.currentPage().getParameters().put('Field', 'ShipTo');
        	ApexPages.currentPage().getParameters().put('ctRadio', addressList[1].id);
        
        	SelectAddressController sAddCon1 = new SelectAddressController();
        	sAddCon1.selectAddress();
        	sAddCon1.returnToRecord();
        }
    }
    static testMethod void selectAddressControllerUnitTest2() {    	
        
        list<Address__c> addressList = TestClassUtility.createAddress(2, true);
        
        list<Order__c> orderList ;
        if(!addressList.isEmpty()){
        	orderList = TestClassUtility.creatOrder(addressList[0].Consumer__c , 1, true);
        	Test.startTest();
        	PageReference pageRef3 = Page.SelectAddress;
        	Test.setCurrentPageReference(pageRef3);
        	ApexPages.currentPage().getParameters().put('contactId', addressList[0].Consumer__c);
        	ApexPages.currentPage().getParameters().put('Field', 'BillTo');
        	ApexPages.currentPage().getParameters().put('ctRadio', addressList[1].id);
        	
        	SelectAddressController sAddCon2 = new SelectAddressController();
        	sAddCon2.selectAddress();
        	sAddCon2.returnToRecord();
        	
        	 // Add parameters to page URL
        	PageReference pageRef4 = Page.SelectAddress;
        	Test.setCurrentPageReference(pageRef4);
        	ApexPages.currentPage().getParameters().put('contactId', addressList[0].Consumer__c);
        	ApexPages.currentPage().getParameters().put('Field', 'ShipTo');
        	
        	SelectAddressController sAddCon3 = new SelectAddressController();
        	sAddCon3.selectAddress();
        	sAddCon3.returnToRecord();
        	
        	 // Add parameters to page URL
        	Case testCase =  TestClassUtility.createCase('Open', 'Social', true);
        	Contact cnt = new Contact();
        	cnt.lastName='test';
        	insert cnt;
        	
        	testCase.ContactId = cnt.Id;
        	update testCase;
        	
        	PageReference pageRef5 = Page.SelectAddress;
        	Test.setCurrentPageReference(pageRef5);
        	ApexPages.currentPage().getParameters().put('caseId', testCase.ID);
        	ApexPages.currentPage().getParameters().put('Field', 'BillTo');
        	
        	SelectAddressController sAddCon4 = new SelectAddressController();
        	sAddCon4.selectAddress();
        	sAddCon4.returnToRecord();
        	
        	PageReference pageRef6 = Page.SelectAddress;
        	Test.setCurrentPageReference(pageRef6);
        	ApexPages.currentPage().getParameters().put('caseId', testCase.ID);
        	ApexPages.currentPage().getParameters().put('Field', 'ShipTo');
        	
        	SelectAddressController sAddCon5 = new SelectAddressController();
        	sAddCon5.selectAddress();
        	sAddCon5.returnToRecord();
        	sAddCon5.clearAddress();
        }
    }
}