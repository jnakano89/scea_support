/*
    @Author: Venkat Reddy
    @Version: 1.0
    @Description: Test class for SCEA_GlobalSearchController
*/
@isTest
public class SCEA_GlobalSearchControllerTest{
    private static testMethod void defaultConstructorTestMethod(){
        Test.setCurrentPage(Page.SCEA_Article);
        ApexPages.currentPage().getParameters().put('lang','en_US');
        SCEA_GlobalSearchController con = new SCEA_GlobalSearchController();
        system.assert(con.selectedLanguage != null);
    }
    
    private static testMethod void defaultConstructorTestMethodForURL(){
        Test.setCurrentPage(Page.SCEA_Article);
        SCEA_LanguageSpecificSites__c cs = new SCEA_LanguageSpecificSites__c();
        cs.Name = 'en_US';
        cs.URL_Identifier_Keyword__c= 'pkbamer';
        insert cs;
        SCEA_GlobalSearchController con = new SCEA_GlobalSearchController();
        system.assert(con.selectedLanguage != null);
    }
    
    private static testMethod void defaultConstructorTestMethodForDefaultLanguage(){
        Test.setCurrentPage(Page.SCEA_Article);
        SCEA_LanguageSpecificSites__c cs = new SCEA_LanguageSpecificSites__c();
        cs.Name = 'en_US';
        cs.URL_Identifier_Keyword__c= 'nokeywordfound';
        insert cs;
        SCEA_GlobalSearchController con = new SCEA_GlobalSearchController();
        system.assert(con.selectedLanguage != null);
    }
    
    private static testMethod void constructorWithStandardControllerAsArgumentTestMethod(){
        Test.setCurrentPage(Page.SCEA_Article);
        ApexPages.currentPage().getParameters().put('lang','en_US');
        ApexPages.StandardController stdController = new ApexPages.StandardController(new KC_Article__kav());
        SCEA_GlobalSearchController con = new SCEA_GlobalSearchController(stdController);
        system.assert(con.selectedLanguage != null);
    }
    
    private static testMethod void constructorWithStandardControllerAsArgumentTestMethodForURL(){
        Test.setCurrentPage(Page.SCEA_Article);
        SCEA_LanguageSpecificSites__c cs = new SCEA_LanguageSpecificSites__c();
        cs.Name = 'en_US';
        cs.URL_Identifier_Keyword__c= 'pkbamer';
        insert cs;
        ApexPages.StandardController stdController = new ApexPages.StandardController(new KC_Article__kav());
        SCEA_GlobalSearchController con = new SCEA_GlobalSearchController(stdController);
        system.assert(con.selectedLanguage != null);
    }
    
    private static testMethod void constructorWithStandardControllerAsArgumentTestMethodForDefaultLanguage(){
        Test.setCurrentPage(Page.SCEA_Article);
        ApexPages.StandardController stdController = new ApexPages.StandardController(new KC_Article__kav());
        SCEA_LanguageSpecificSites__c cs = new SCEA_LanguageSpecificSites__c();
        cs.Name = 'en_US';
        cs.URL_Identifier_Keyword__c= 'nokeywordfound';
        insert cs;
        SCEA_GlobalSearchController con = new SCEA_GlobalSearchController(stdController);
        system.assert(con.selectedLanguage != null);
    }
    
    private static testMethod void constructorWithSearchControllerAsArgumentTestMethod(){
        Test.setCurrentPage(Page.SCEA_Article);
        ApexPages.currentPage().getParameters().put('lang','en_US');
        SCEA_SearchResultsController searchCon = new SCEA_SearchResultsController();
        SCEA_GlobalSearchController con = new SCEA_GlobalSearchController(searchCon);
        system.assert(con.selectedLanguage != null);
    }
    
    private static testMethod void constructorWithSearchControllerAsArgumentTestMethodForURL(){
        Test.setCurrentPage(Page.SCEA_Article);
        SCEA_LanguageSpecificSites__c cs = new SCEA_LanguageSpecificSites__c();
        cs.Name = 'en_US';
        cs.URL_Identifier_Keyword__c= 'pkbamer';
        insert cs;
        SCEA_SearchResultsController searchCon = new SCEA_SearchResultsController();
        SCEA_GlobalSearchController con = new SCEA_GlobalSearchController(searchCon);
        system.assert(con.selectedLanguage != null);
    }
    
    private static testMethod void constructorWithSearchControllerAsArgumentTestMethodForDefaultLanguage(){
        Test.setCurrentPage(Page.SCEA_Article);
        SCEA_SearchResultsController searchCon = new SCEA_SearchResultsController();
        SCEA_LanguageSpecificSites__c cs = new SCEA_LanguageSpecificSites__c();
        cs.Name = 'en_US';
        cs.URL_Identifier_Keyword__c= 'nokeywordfound';
        insert cs;
        SCEA_GlobalSearchController con = new SCEA_GlobalSearchController(searchCon);
        system.assert(con.selectedLanguage != null);
    }
    
    private static testMethod void constructorWithDetailControllerAsArgumentTestMethod(){
        Test.setCurrentPage(Page.SCEA_Article);
        ApexPages.currentPage().getParameters().put('lang','en_US');
        ApexPages.StandardController stdController = new ApexPages.StandardController(new KC_Article__kav());
        SCEA_ArticleDetailController detailCon = new SCEA_ArticleDetailController(stdController);
        SCEA_GlobalSearchController con = new SCEA_GlobalSearchController(detailCon);
        system.assert(con.selectedLanguage != null);
    }
    
    private static testMethod void constructorWithDetailControllerAsArgumentTestMethodForURL(){
        Test.setCurrentPage(Page.SCEA_Article);
        SCEA_LanguageSpecificSites__c cs = new SCEA_LanguageSpecificSites__c();
        cs.Name = 'en_US';
        cs.URL_Identifier_Keyword__c= 'pkbamer';
        insert cs;
        ApexPages.StandardController stdController = new ApexPages.StandardController(new KC_Article__kav());
        SCEA_ArticleDetailController detailCon = new SCEA_ArticleDetailController(stdController);
        SCEA_GlobalSearchController con = new SCEA_GlobalSearchController(detailCon);
        system.assert(con.selectedLanguage != null);
    }
    
    private static testMethod void constructorWithDetailControllerAsArgumentTestMethodForDefaultLanguage(){
        Test.setCurrentPage(Page.SCEA_Article);
        ApexPages.StandardController stdController = new ApexPages.StandardController(new KC_Article__kav());
        SCEA_ArticleDetailController detailCon = new SCEA_ArticleDetailController(stdController);
        SCEA_LanguageSpecificSites__c cs = new SCEA_LanguageSpecificSites__c();
        cs.Name = 'en_US';
        cs.URL_Identifier_Keyword__c= 'nokeywordfound';
        insert cs;
        SCEA_GlobalSearchController con = new SCEA_GlobalSearchController(detailCon);
        system.assert(con.selectedLanguage != null);
    }
    
    private static testMethod void globalSearchTestMethod(){
        Test.setCurrentPage(Page.SCEA_Article);
        ApexPages.currentPage().getParameters().put('lang','en_US');
        SCEA_GlobalSearchController con = new SCEA_GlobalSearchController();
        con.globalSearchString = 'Test';
        PageReference returnPageRef = con.globalSearch();
        system.assert(returnPageRef != null);
        system.assert(returnPageRef.getUrl().containsIgnoreCase('SCEA_SearchResults'));
    }
}