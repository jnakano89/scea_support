/******************************************************************************
Class         : SelectPSNAccountController_Test
Description   : Test class for Controller class[T-180177]
Developed by  : Urminder Vohra(JDC)
Date          : Sep 4, 2013

Updates       :
******************************************************************************/
@isTest(seeAllData=true)
private class SelectPSNAccountController_Test {
   static case testcase;
   static PSN_Account__c psnAcc;
   static testMethod void myUnitTest() {
       Test.startTest();
       createData();
       Apexpages.currentPage().getParameters().put('caseId', testCase.Id);
       SelectPSNAccountController ctrl = new SelectPSNAccountController();
       
       ApexPages.currentPage().getParameters().put('ctRadio',psnAcc.Id);
       ctrl.selectPsnAccount();
       
       
       Case updatedCase = [select PSN_Account__c from Case where ID = :testCase.Id];
       System.assertEquals(updatedCase.PSN_Account__c, psnAcc.Id,'Case should be udpated with selected PSN Account');
       
       ctrl.clearPsnAccount();
       
       updatedCase = [select PSN_Account__c from Case where ID = :testCase.Id];
       System.assertEquals(updatedCase.PSN_Account__c, null,'Case should be udpated with null');
       
       
       Test.stopTest();
   }
    
   static void createData() {
   
     //code added by preetu
    
    String RecTypeId= [select Id from RecordType where (Name='Person Account') and (SobjectType='Account')].Id; 
      PSN_Account__C psnAcc1 = [select Id,consumer__r.AccountId,Name from PSN_Account__c where Name='xgamertomx'];
  
  
      Account acc = new Account();
      acc.LastName = 'test';
      acc.FirstName = 'ftest';
      acc.Phone = '12345';
      //acc.PSN_Account__c = psnAcc1.Id;
      acc.RecordTypeID=RecTypeId;
      insert acc;
      
     Contact repContact = [SELECT id,FirstName, LastName
     FROM Contact
     WHERE isPersonAccount = true AND AccountId = :acc.id];
     
     Contact cnct = new Contact();
     cnct.LastName = 'test';
     cnct.FirstName = 'ftest';
     cnct.Phone = '12345';
     insert cnct;
     
     testcase = new Case();
     testcase.Status = 'Open';
     testcase.received_date__c = date.today();
     testcase.shipped_date__c = date.today().addDays(1);
     testcase.outbound_tracking_number__c = '101';
     testcase.outbound_carrier__c = 'TYest1';
     testCase.Origin = 'Social';
     testCase.ContactId = cnct.Id;
     testCase.Offender__c = 'Test';
     testCase.Product__c = 'PS4';
     testCase.Sub_Area__c = 'Account Help';
     insert testCase;
     
     psnAcc = new PSN_Account__c();
     psnAcc.First_Name__c = 'First name';
     psnAcc.First_Name__c = 'Last name';
     psnAcc.Name = 'abcd';
     psnAcc.Consumer__c = repContact.id;
     insert psnAcc;
     
     
   }
}