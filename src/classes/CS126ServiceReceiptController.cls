public class CS126ServiceReceiptController {
  
  public Id caseId {get;set;}
  
  public Case caseObject { 
    get{
        if(caseObject == null && caseId != null){
            caseObject = [select Id,CaseNumber,ContactId,CreatedDate,Asset__r.Model_Number__c,Total_Tax__c,Total_Amount_to_Pay__c,Unit_Price__c 
                         from Case where Id = :caseId];
       }
        return caseObject;
    }
    set;
  }
  
  public Order__c orderObject { 
    get{
        if(orderObject == null && caseId != null){
            orderObject = [select Id from Order__c where Case__c = :caseId 
                          order By CreatedDate DESC limit 1];
            
       }
        return orderObject;
    }
    set;
  }
  
  public Order_Line__c orderLine {
  	 get{
        if(orderLine == null && orderObject != null){
            orderLine = [select Id,Quantity__c,List_Price__c,Tax__c,
                                Ship_Cost__c,Total__c,Order__r.Asset__r.Model_Number__c
                         from Order_Line__c where Order__c = :orderObject.Id];
       	}
        return orderLine;
    }
    set;
  }
  
  public Contact contactObject {
    get{
        if(contactObject == null && caseObject != null){
            contactObject = [select Id, FirstName,LastName 
                             From Contact
                             Where Id = :caseObject.ContactId];
       }
        return contactObject;
    }
    set;
  }
  
  public CS126ServiceReceiptController(){
  	
  }
}