@isTest (SeeAllData=true)
private class Test_Case_Email_pdf_Invoice_Controller { 
    
    static testMethod void myUnitTest() {

    list<Contact> cntList = TestClassUtility.createContact(2, false);
    insert cntlist;

    list<Order__c> ordrList = TestClassUtility.creatOrder(cntList[0].Id, 1, false);
    insert ordrList;
    
    //AB - 10/21/2014 - Add Asset to Address Null Pointer on updateParsedTemplate()
    list<Product__c> prdList = TestClassUtility.createProduct(1,true);
    list<Asset__c> asstList = TestClassUtility.createAssets(1,false);
    asstList[0].Product__c = prdList[0].Id;
    insert asstList;

    Address__c address = new Address__c(); 
    address.Consumer__c = cntList[0].Id;
    address.Address_Line_1__c = 'Test Line1';
    address.Address_Line_2__c = 'Test Line2';
    address.Address_Line_3__c = 'Test Line3';
    address.City__c           = 'San Mateo';
    address.Country__c        = 'US';
    address.State__c          = 'CA';
    address.Postal_Code__c    = '94404';
    address.Status__c         = 'Active';
    insert address;

    Case cs = TestClassUtility.createCase('New', 'Test', false);
    cs.ContactId = cntList[0].Id;
    cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service').getRecordTypeId();
    cs.Product__c = 'PS4';
    cs.Sub_Area__c = 'Console';
    cs.Status = 'Open';
    cs.Order__c = ordrList[0].id;
    cs.Asset__c = asstList[0].id;
    cs.Ship_To__c = address.id;
    cs.Bill_To__c = address.id;
    insert cs;

    EmailTemplate et;
    for(EmailTemplate e : [Select id, body, HtmlValue from EmailTemplate]){
        if(e.body != null){
            et = e;
            break;
        }
    }

    PageReference pPageReference = Page.Case_Email_pdf_Invoice;
    pPageReference.getParameters().put('templateId',et.id);
    Test.setCurrentPage(pPageReference);

    ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(cs);

    Case_Email_pdf_Invoice_Controller cont = new Case_Email_pdf_Invoice_Controller(sc);
    cont.updateParsedTemplate();

    //System.assertEquals(cont.parsedEmailTemplate, et.HtmlValue);

    }
}