// Class           : reCaptchaUtility
// Description     : Utility Class for usage of reCaptcha Validation
// Developer       : Briggs, Aaron
// Created Date    : 03/16/2015
// Modified        : 03/16/2015 : Aaron Briggs : SMS-1405 : Initial reCaptcha Development
// Modified        : 06/11/2015 : CM : SMS-1382 : Console Deactivation Form reCaptcha


global with sharing class reCaptchaUtility {

    private final static String SECRET_KEY = (reCaptcha__c.getInstance(UserInfo.getProfileId())).secretKey__c;
    private final static String PUBLIC_KEY = (reCaptcha__c.getInstance(UserInfo.getProfileId())).publicKey__c;
    private final static String END_POINT = 'https://www.google.com/recaptcha/api/siteverify';
    
    public reCaptchaUtility(StateValuesRemote controller) {}
    // Added 06/11/2015 : CM : SMS-1382 : Console Deactivation Form reCaptcha
    public reCaptchaUtility(ConsoleDeactivationController controller) {}
        
    public String publicKey {
        get { return PUBLIC_KEY; }
    }

    @RemoteAction
    global static boolean reCaptchaVerify(String pattern) {
        Boolean human = false;
        System.debug('==========================> pattern: ' + pattern);
        
        String success = '\"success\": true';
        System.debug('==========================> reCaptcha Verification Attempt');
        
        if(pattern == null) {
            System.debug('==========================> reCaptcha Verficiation with Empty Form');
            return null;
        }
        
        HttpResponse resp = makeRequest('secret=' + SECRET_KEY +
                                       '&response=' + pattern);

        String reCaptchaResponse = resp.getBody();
        System.debug('==========================> reCaptchaResponse: ' + reCaptchaResponse);
        System.debug('==========================> reCaptchaResponse.indexOf(success): ' + reCaptchaResponse.indexOf(success));
        
        if(reCaptchaResponse.indexOf(success) != -1) {
            human = true;
        }
        
        if(human) {
            System.debug('==========================> reCaptcha Successfully Verified');
            return true;
        } else {
            System.debug('==========================> reCaptcha Failed Verification');
            return false;
        }
    }
    
    global static HttpResponse makeRequest(string body) {
        HttpResponse response = null;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(END_POINT);
        req.setMethod('POST');
        req.setBody(body);
        
        try {
                Http http = new Http();
                response = http.send(req);
                System.debug('==========================> reCAPTCHA Response: ' + response);
                System.debug('==========================> reCAPTCHA Response: ' + response.getBody());
        } catch(System.Exception e) {
                //TBD Create error log entry
                System.debug('==========================> Error: ' + e);
        }
        
        return response;
    }

}