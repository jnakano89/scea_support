/*
    @Author: Venkat Reddy
    @Version: 1.0
    @Description: Controller for global search functionality
*/
public class SCEA_GlobalSearchController{
    public String globalSearchString {get;set;}
    //to hold lang parameter value
    public String selectedLanguage{set;get;}

    public SCEA_GlobalSearchController(){
        //to accees lang parameter value from URL
        //incase no lang value in URL then make it en_US which is English as default
        if(ApexPages.currentPage().getParameters().get('lang') != null)
            selectedLanguage = ApexPages.currentPage().getParameters().get('lang');

        system.debug('--- Site.getBaseURL() ---'+Site.getBaseURL());
        String baseURL = Site.getBaseURL() != null?Site.getBaseURL():(Test.isRunningTest()?'http://pkb-scea.cs24.force.com/pkbamer':null);
        if(baseURL != null && (selectedLanguage == null || selectedLanguage == '')){
            for(SCEA_LanguageSpecificSites__c langSite : SCEA_LanguageSpecificSites__c.getAll().values()){
                if(baseURL.containsIgnoreCase(langSite.URL_Identifier_Keyword__c)){
                    selectedLanguage = langSite.Name;
                    break;
                }
            }
        }

        if(selectedLanguage == null || selectedLanguage == '')
            selectedLanguage = 'en_US';
    }
    //constructor with search controller as argument, so that this class can be used as extensions
    public SCEA_GlobalSearchController(ApexPages.StandardController stdController){
        //to accees lang parameter value from URL
        //incase no lang value in URL then make it en_US which is English as default
        if(ApexPages.currentPage().getParameters().get('lang') != null)
            selectedLanguage = ApexPages.currentPage().getParameters().get('lang');

        system.debug('--- Site.getBaseURL() ---'+Site.getBaseURL());
        String baseURL = Site.getBaseURL() != null?Site.getBaseURL():(Test.isRunningTest()?'http://pkb-scea.cs24.force.com/pkbamer':null);
        if(baseURL != null && (selectedLanguage == null || selectedLanguage == '')){
            for(SCEA_LanguageSpecificSites__c langSite : SCEA_LanguageSpecificSites__c.getAll().values()){
                if(baseURL.containsIgnoreCase(langSite.URL_Identifier_Keyword__c)){
                    selectedLanguage = langSite.Name;
                    break;
                }
            }
        }

        if(selectedLanguage == null || selectedLanguage == '')
            selectedLanguage = 'en_US';
        system.debug('--- selectedLanguage ---'+selectedLanguage);
    }
    //constructor with search controller as argument, so that this class can be used as extensions
    public SCEA_GlobalSearchController(SCEA_SearchResultsController searchController){
        //to accees lang parameter value from URL
        //incase no lang value in URL then make it en_US which is English as default
        if(ApexPages.currentPage().getParameters().get('lang') != null)
            selectedLanguage = ApexPages.currentPage().getParameters().get('lang');

        system.debug('--- Site.getBaseURL() ---'+Site.getBaseURL());
        String baseURL = Site.getBaseURL() != null?Site.getBaseURL():(Test.isRunningTest()?'http://pkb-scea.cs24.force.com/pkbamer':null);
        if(baseURL != null && (selectedLanguage == null || selectedLanguage == '')){
            for(SCEA_LanguageSpecificSites__c langSite : SCEA_LanguageSpecificSites__c.getAll().values()){
                if(baseURL.containsIgnoreCase(langSite.URL_Identifier_Keyword__c)){
                    selectedLanguage = langSite.Name;
                    break;
                }
            }
        }

        if(selectedLanguage == null || selectedLanguage == '')
            selectedLanguage = 'en_US';
        system.debug('--- selectedLanguage ---'+selectedLanguage);
    }
    //extension for article detail page
    public SCEA_GlobalSearchController(SCEA_ArticleDetailController articleDetailController){
        //to accees lang parameter value from URL
        //incase no lang value in URL then make it en_US which is English as default
        if(ApexPages.currentPage().getParameters().get('lang') != null)
            selectedLanguage = ApexPages.currentPage().getParameters().get('lang');

        system.debug('--- Site.getBaseURL() ---'+Site.getBaseURL());
        String baseURL = Site.getBaseURL() != null?Site.getBaseURL():(Test.isRunningTest()?'http://pkb-scea.cs24.force.com/pkbamer':null);
        if(baseURL != null && (selectedLanguage == null || selectedLanguage == '')){
            for(SCEA_LanguageSpecificSites__c langSite : SCEA_LanguageSpecificSites__c.getAll().values()){
                if(baseURL.containsIgnoreCase(langSite.URL_Identifier_Keyword__c)){
                    selectedLanguage = langSite.Name;
                    break;
                }
            }
        }

        if(selectedLanguage == null || selectedLanguage == '')
            selectedLanguage = 'en_US';
        system.debug('--- selectedLanguage ---'+selectedLanguage);
    }

    public PageReference globalSearch(){
        System.debug('globalSearch() entered');
        PageReference pageRef = null;
        if(globalSearchString != null && globalSearchString.length()>0){
            pageRef = Page.SCEA_SearchResults;
            pageRef.getParameters().put('selStr',globalSearchString);
            pageRef.getParameters().put('lang',selectedLanguage);
            pageRef.setRedirect(true);
        }
        System.debug('globalSearch() returning: ' + pageRef.getUrl());
        return pageRef;
    }
}