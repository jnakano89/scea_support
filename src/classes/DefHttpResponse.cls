/**
  Default wrapper implementation over standard Apex HttpResponse class.
  Reference : http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_classes_restful_http_httpresponse.htm
  All contract methods of IHttpResponse are delegated to the wrapped Apex HttpResponse instance. 
*/
public with sharing virtual class DefHttpResponse implements IHttpResponse {
  public Httpresponse resp;
   
  public DefHttpResponse(HttpResponse resp) {
    this.resp = resp;
  }
   
  public String getBody() {
    return resp.getBody();
  }
   
  public Dom.Document getBodyDocument() {
    return resp.getBodyDocument();
  }
   
  public String getHeader(String key) {
    return resp.getHeader(key);
  }
   
  public String[] getHeaderKeys() {
    return resp.getHeaderKeys();
  }
   
  public String getStatus() {
    return resp.getStatus();
  }
   
  public Integer getStatusCode() {
    return resp.getStatusCode();
  }
   
  public Xmlstreamreader getXmlStreamReader() {
    return resp.getXmlStreamReader();
  }
   
  public String toStrings() {
    return resp.toString();
  }
}