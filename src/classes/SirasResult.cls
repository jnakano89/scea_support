/*********************************************************************************
 Author       :   Dipika (JDC Developer)
 Created Date :   12 Aug,2013
 Task         :   T-170654
*********************************************************************************/
public  class SirasResult {
	public String reasonCode{get;set;} 
    public String reasonDescription {get;set;} 
    public String sirasInquiryID {get;set;} 
    public String UPC {get;set;} 
    public String serialNumber {get;set;} 
    public String itemDescription {get;set;} 
    public String itemNumber {get;set;} 
    public String brandName {get;set;} 
    public String brandID {get;set;} 
    public String soldByRetailer {get;set;} 
    public String soldByRetailerID {get;set;} 
    public Date soldDate {get;set;} 
    public String manufacturerWarrantyDetermination {get;set;} 
    public String manufacturerWarrantyPartsDetermination {get;set;} 
    public String manufacturerWarrantyLaborDetermination {get;set;} 
}