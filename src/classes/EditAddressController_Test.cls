/******************************************************************************
Class         : EditAddressController_Test
Description   : Test class for EditAddressController
Developed by  : Poonam Varyani
Date          : August 7, 2013              
******************************************************************************/
@isTest
private class EditAddressController_Test {
    static Contact contact;
    
    static testMethod void testNewAddress() {
        //create data
        createTestData();
        
        //create Zip Code
    	Zip_Code__c zipCode = new Zip_Code__c();
        zipCode.Name = '12345';
        zipCode.City__c = 'CA';
        zipCode.Country__c = 'US';
        zipCode.State__c = 'CA';
        zipCode.Zip_Code__c = '12345';
        insert zipCode;
        //create address
        Address__c address = TestClassUtility.createAddress(1, false).get(0);
        address.Postal_Code__c = zipCode.Zip_Code__c;
        address.Country__c = 'US';
        
        Test.startTest();
        
         Case testcase = new Case();
	     testcase.Status = 'Open';
	     testcase.received_date__c = date.today();
	     testcase.shipped_date__c = date.today().addDays(1);
	     testcase.outbound_tracking_number__c = '101';
	     testcase.outbound_carrier__c = 'TYest1';
	     testCase.Origin = 'Social';
	     testCase.ContactId = contact.Id;
	     testCase.Offender__c = 'test';
         testCase.Product__c = 'PS4';
         testCase.Sub_Area__c = 'Account Help';
	     insert testCase;
	     
        Apexpages.currentPage().getParameters().put('caseId', testCase.Id);
        Apexpages.currentPage().getParameters().put('contactId', contact.Id);
        ApexPages.Standardcontroller con = new ApexPages.Standardcontroller(address);
        EditAddressController editAddObj = new EditAddressController(con);
        
        editAddObj.searchPostalCode();
        editAddObj.redirectBack();
        system.debug('address>>>>>>> '+address);
        insert address;
        
         //assert to check if city/state are same as from zip code object;
        String countryStr = address.Country__c+'%';
        List<Zip_Code__c> listZipCode = [Select State__c, City__c 
                                         From Zip_Code__c 
                                         Where Zip_Code__c =: address.Postal_Code__c 
                                         AND Country__c like :countryStr limit 1 ];
        List<Address__c> listAddress = [Select State__c, City__c, Postal_Code__c, 
                                        Country__c
                                        From Address__c
                                        Where Id =: address.Id];
                                        
        //system.assertEquals(listZipCode.get(0).State__c, listAddress.get(0).State__c); 
        
        //check for the changes in zip or country - update case
        EditAddressController editAddObj1 = new EditAddressController(con);
        listAddress.get(0).Postal_Code__c = '12347';
        listAddress.get(0).Country__c = 'CA';
        
        editAddObj1.searchPostalCode();
        
        update listAddress;
        
        //editAddObj.SaveAndNew();
        editAddObj.save();
        
        
        EditAddressController ctrl2 = new EditAddressController();
                                              	                                       
    }
    
    static testmethod void updateAddressTest(){
        //create data
        createTestData();
        
        //create address
        Address__c address = new Address__c(); 
        address.Consumer__c = contact.Id;
        address.Address_Line_1__c = 'address line 1';
        address.Postal_Code__c = '12345';
        address.Country__c = 'USA';
        
        Test.startTest();
        
        ApexPages.Standardcontroller con = new ApexPages.Standardcontroller(address);
        EditAddressController editAddObj = new EditAddressController(con);
        editAddObj.searchPostalCode();
        
        insert address;
        
        Test.stopTest(); 
    }
    
    private static void createTestData(){
    	
    	//create account
    	String rtId;
    	for(RecordType rt : [select Id from RecordType where SobjectType = 'Account']) {
    		rtId = rt.Id;
    		break;
    	}
    	
    	Account account = new Account();
    	account.Name = 'test contact';
    	account.RecordTypeId = rtId;
    	insert account;
    	
    	
    	//Contact
    	contact = new Contact();
    	contact.LastName = 'lname';
    	contact.AccountId = account.Id;
    	insert contact;
    	
    	//create zip codes
    	List<Zip_Code__c> zipCodeList = new List<Zip_Code__c>();
    	
    	for(integer i=1;i<4;i++){
            Zip_Code__c zipCode = new Zip_Code__c();
            zipCode.City__c = 'test'+i;
            zipCode.Country__c = 'USA';
            zipCode.State__c = 'TS';
            zipCode.Zip_Code__c = '1234'+i;
            zipCode.Name = '1234'+i;
            
            zipCodeList.add(zipCode);	
    	}
    	
    	insert zipCodeList;
    	
    }
     static testmethod void testSave(){
    	//
    	createTestData();
        Address__c address = new Address__c(); 
        address.Consumer__c = contact.Id;
        
        address.State__c = 'TS';
        address.Postal_Code__c = '1111';
        address.Country__c = 'USA';
        
        ApexPages.Standardcontroller con = new ApexPages.Standardcontroller(address);
        EditAddressController editAddObj = new EditAddressController(con);
        
        editAddObj.save();
        
        address.Address_Line_1__c = '';
        address.Postal_Code__c = '12341';
        address.City__c = 'TestCity';
        con = new ApexPages.Standardcontroller(address);
        editAddObj = new EditAddressController(con);
        
        editAddObj.save();
        
        Address__c address2 = new Address__c(); 
        address2.Consumer__c = contact.Id;
        address2.Address_Line_1__c = '';
        address2.City__c = '';
        address2.State__c = 'TS';
        address2.Postal_Code__c = '12341';
        address2.Country__c = 'USA';
        ApexPages.Standardcontroller con2 = new ApexPages.Standardcontroller(address2);
        EditAddressController editAddObj2 = new EditAddressController(con2);
        editAddObj2.save();
        //
      
    }
}