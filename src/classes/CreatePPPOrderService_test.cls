/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 
  Test Class for CreatePPPOrderService webservice.
 */ 
@isTest
private class CreatePPPOrderService_test {

    static testMethod void myUnitTest() {
        CreatePPPOrderService.WebServiceResponse response = CreatePPPOrderService.processPPPFlow(getSampleRequest());
        //todo - Verify Data and Assert validation
        system.debug('*******>>'+response);
    }
    static CreatePPPOrderService.WebServiceInput getSampleRequest(){
    	CreatePPPOrderService.WebServiceInput request = new CreatePPPOrderService.WebServiceInput();
    	/*request.billingAddress1 = 'teST Address2';
    	request.billingAddress2 = 'teST Address2';*/
		request.systemType = '3434';
		request.modelNumber = '2323232';
		request.serialNumber = '!!TestSerialNo!!';
		request.proofOfPurchaseDate = DateTime.now().date();
		request.pppSKU  = 'TestSKU';
		request.firstName  = 'FirstName';
		request.lastName  = 'LastName Test';
		request.phone  = '234243432';
		request.email  = 'testEmail@SonyTest.com';
		
    	return request;
    }
 
    
}