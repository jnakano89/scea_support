public interface IHttpResponse {
  String getBody();
   
  Dom.Document getBodyDocument();
   
  String getHeader(String key);
   
  String[] getHeaderKeys();
   
  String getStatus();
   
  Integer getStatusCode();
   
  Xmlstreamreader getXmlStreamReader();
   
  // have to name it toStrings() instead of toString(), as the later
  // is reserved by Apex
  String toStrings();
}