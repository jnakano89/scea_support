/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 //AssurantInboundProcessBatchable
 */
@isTest
private class AssurantInboundProcessScheduler_test {

	static List<Staging_PPP_Inbound__c> stagingPPPList = null;

	static testMethod void SchedulerTest() {
		createTestData();
		for(Staging_PPP_Inbound__c stg : stagingPPPList){
			stg.PPP_Claim_Reason__c =  'Test!!2!';
		}
		AssurantInboundProcessBatchable.claimQueryTest = ' and PPP_Claim_Reason__c like \'Test claim reason%\'';
		insert stagingPPPList;
		Test.startTest();
		String CRON_EXP = '0 0 0 * * ?';
		AssurantInboundProcessScheduler m = new AssurantInboundProcessScheduler(); 
		system.schedule('Test AssurantInboundClaimScheduler Job2', CRON_EXP, m); 
		//Todo -- Need to put Assert
		Test.stopTest();

	}

	static testMethod void BatchTestWithOutSFDCObject() {
		createTestData();
		for(Staging_PPP_Inbound__c stg : stagingPPPList){
			stg.PPP_Claim_Reason__c =  'Test!!2!';
		}
		AssurantInboundProcessBatchable.claimQueryTest = ' and PPP_Claim_Reason__c like \'Test!!2!%\'';
		insert stagingPPPList;
		Test.startTest();
		DataBase.executeBatch(new AssurantInboundProcessBatchable(), 5);
		//Todo -- Need to put Assert
		AssurantInboundProcessBatchable.setInboundPPPError(new Set<Staging_PPP_Inbound__c>(stagingPPPList), stagingPPPList[0], 'Coverage Error');
		Test.stopTest();

	}

	static testMethod void BatchTestWithSFDCObjectValiddata_Full() {
		list<Address__c> addList = TestClassUtility.createAddress(1, false);
		insert addList;
		list<Asset__c> assetList = TestClassUtility.createAssets(5, true);
		assetList[0].PPP_Product__c = null;
		update assetList[0];


		list<Contact> cntList = TestClassUtility.createContact(2, false);
		list<Product__c> productList = TestClassUtility.createProduct(1, true);
		cntList[0].FirstName = 'Test Name';
		cntlist[0].Email = 'test@test.com';
		cntlist[0].Preferred_Language__c = 'Eng';
		cntlist[0].Bill_To__c = addList[0].Id;
		insert cntlist;

		Case cs = TestClassUtility.createCase('New', 'TEst', false);
		cs.ContactId = cntList[0].Id;
		cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
		cs.Sub_Area__c = 'NEw';
		cs.Status = 'New';
		insert cs;

		list<Order__c> ordrList = TestClassUtility.creatOrder(cntList[0].Id, 5, false);
		list<Account>personAccountList = TestClassUtility.createPersonAccount(1, true);
		list<contact> lstcontact = [select id from contact where accountid in:personAccountList limit 1];
		Integer countRec = 0;
		for(Order__c o : ordrList){
			o.Asset__c = assetList[countRec].id;
			o.Case__c = cs.Id;
			o.Consumer__c =	lstcontact[0].Id;
			o.Order_Type__c = 'PSN';
			countRec++;
		}
		insert ordrList;

		Payment__c payment = new Payment__c();
		payment.Payment_Status__c = 'Charged';
		payment.Order__c = ordrList[0].Id;
		insert payment;

		ordrList[0].Payment__c = payment.Id;
		update ordrList[0];
		createTestData();
		Integer ordIndx = 0;
		map<id, Order__c> ordMap = new map<id, Order__c>([select id, Name, External_Order_Number__c from Order__c where Id in: ordrList]);
		for(Staging_PPP_Inbound__c stg : stagingPPPList){
			stg.Unit_Model_Number__c = '121';
			stg.Unit_Serial_Number__c = '121Ser';
			stg.PPP_Purchase_Date__c = datetime.Now().format('MMddYYYY');
			stg.Purchase_Date__c = datetime.Now().format('MMddYYYY');
			stg.Proof_Of_Purchase_Date__c = datetime.Now().format('MMddYYYY');
			stg.PPP_Start_Date__c = datetime.Now().format('MMddYYYY');
			stg.PPP_Cancel_Date__c= datetime.Now().format('MMddYYYY');
			stg.PPP_Purchase_Date__c = datetime.Now().format('MMddYYYY');
			stg.PPP_Refund_by_Dealer__c= '10';
			stg.Consumer_First_Name__c = 'Test Name';
			stg.Consumer_Last_Name__c = 'Test Name';
			stg.Billing_Address_1__c = 'Bill adr 1';
			stg.Billing_City__c = 'NYC';
			stg.Billing_State__c = 'CA';
			stg.Billing_Country__c = 'USA';
			stg.Order_Record_Type__c = 'C';
			stg.PPP_Claim_Reason__c =  'Test!!2!';
			stg.Order_Number__c = ordMap.get(ordrList[ordIndx].id).External_Order_Number__c;
		}
		AssurantInboundProcessBatchable.claimQueryTest = ' and s.PPP_Claim_Reason__c like \'Test!!2!%\'';
		insert stagingPPPList;
		system.debug('**************stagingPPPList::'+stagingPPPList[0].id +' '+[select id from Staging_PPP_Inbound__c where id in :stagingPPPList and Processed_Date_Time__c=null AND (Order_Record_Type__c='A' or( Order_Record_Type__c='C'))]);
		
		Test.startTest();
			DataBase.executeBatch(new AssurantInboundProcessBatchable(), 5);
		//Todo -- Need to put Assert
		Test.stopTest();
	}

	static testMethod void BatchTestWithSFDCObjectValiddata_Refund() {
		list<Address__c> addList = TestClassUtility.createAddress(1, false);
		insert addList;
		list<Asset__c> assetList = TestClassUtility.createAssets(5, true);
		assetList[0].PPP_Product__c = null;
		update assetList[0];


		list<Contact> cntList = TestClassUtility.createContact(2, false);
		list<Product__c> productList = TestClassUtility.createProduct(1, true);
		cntList[0].FirstName = 'Test Name';
		cntlist[0].Email = 'test@test.com';
		cntlist[0].Preferred_Language__c = 'Eng';
		cntlist[0].Bill_To__c = addList[0].Id;
		insert cntlist;

		Case cs = TestClassUtility.createCase('New', 'TEst', false);
		cs.ContactId = cntList[0].Id;
		cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
		cs.Sub_Area__c = 'NEw';
		cs.Status = 'New';
		insert cs;

		list<Order__c> ordrList = TestClassUtility.creatOrder(cntList[0].Id, 5, false);
		list<Account>personAccountList = TestClassUtility.createPersonAccount(1, true);
		list<contact> lstcontact = [select id from contact where accountid in:personAccountList limit 1];
		Integer countRec = 0;
		for(Order__c o : ordrList){
			o.Asset__c = assetList[countRec].id;
			o.Case__c = cs.Id;
			o.Consumer__c =	lstcontact[0].Id;
			o.Order_Type__c = 'PSN';
			countRec++;
		}
		insert ordrList;

		Payment__c payment = new Payment__c();
		payment.Payment_Status__c = 'Charged';
		payment.Order__c = ordrList[0].Id;
		insert payment;

		ordrList[0].Payment__c = payment.Id;
		update ordrList[0];
		createTestData();
		Integer ordIndx = 0;
		map<id, Order__c> ordMap = new map<id, Order__c>([select id, Name, External_Order_Number__c from Order__c where Id in: ordrList]);
		for(Staging_PPP_Inbound__c stg : stagingPPPList){
			stg.Unit_Model_Number__c = '121';
			stg.Unit_Serial_Number__c = '121Ser';
			stg.PPP_Purchase_Date__c = datetime.Now().format('MMddYYYY');
			stg.Purchase_Date__c = datetime.Now().format('MMddYYYY');
			stg.Proof_Of_Purchase_Date__c = datetime.Now().format('MMddYYYY');
			stg.PPP_Start_Date__c = datetime.Now().format('MMddYYYY');
			stg.PPP_Cancel_Date__c= datetime.Now().addMonths(2).format('MMddYYYY');
			stg.PPP_Purchase_Date__c = datetime.Now().format('MMddYYYY');
			stg.PPP_Refund_by_Dealer__c= '10';
			stg.Consumer_First_Name__c = 'Test Name';
			stg.Consumer_Last_Name__c = 'Test Name';
			stg.Billing_Address_1__c = 'Bill adr 1';
			stg.Billing_City__c = 'NYC';
			stg.Billing_State__c = 'CA';
			stg.Billing_Country__c = 'USA';
			stg.Order_Record_Type__c = 'C';
			stg.PPP_Claim_Reason__c =  'Test!!2!';
			stg.Order_Number__c = ordMap.get(ordrList[ordIndx].id).External_Order_Number__c;
		}
		AssurantInboundProcessBatchable.claimQueryTest = ' and s.PPP_Claim_Reason__c like \'Test!!2!%\'';
		insert stagingPPPList;
		system.debug('**************stagingPPPList::'+stagingPPPList[0].id +' '+[select id from Staging_PPP_Inbound__c where id in :stagingPPPList and Processed_Date_Time__c=null AND (Order_Record_Type__c='A' or( Order_Record_Type__c='C'))]);
		
		Test.startTest();
			DataBase.executeBatch(new AssurantInboundProcessBatchable(), 5);
		//Todo -- Need to put Assert
		Test.stopTest();
	}
	
	static testMethod void BatchTestWith_EnrollmentConfirmation() {
		list<Address__c> addList = TestClassUtility.createAddress(1, false);
		insert addList;
		list<Asset__c> assetList = TestClassUtility.createAssets(5, true);
		assetList[0].PPP_Product__c = null;
		update assetList[0];


		list<Contact> cntList = TestClassUtility.createContact(2, false);
		list<Product__c> productList = TestClassUtility.createProduct(1, true);
		cntList[0].FirstName = 'Test Name';
		cntlist[0].Email = 'test@test.com';
		cntlist[0].Preferred_Language__c = 'Eng';
		cntlist[0].Bill_To__c = addList[0].Id;
		insert cntlist;

		Case cs = TestClassUtility.createCase('New', 'TEst', false);
		cs.ContactId = cntList[0].Id;
		cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Purchase').getRecordTypeId();
		cs.Sub_Area__c = 'NEw';
		cs.Status = 'New';
		insert cs;

		list<Order__c> ordrList = TestClassUtility.creatOrder(cntList[0].Id, 5, false);
		list<Account>personAccountList = TestClassUtility.createPersonAccount(1, true);
		list<contact> lstcontact = [select id from contact where accountid in:personAccountList limit 1];
		List<Order_line__c> orderline = new List<Order_line__c>();
		Integer countRec = 0;
		for(Order__c o : ordrList){
			o.Asset__c = assetList[countRec].id;
			o.Case__c = cs.Id;
			o.Consumer__c =	lstcontact[0].Id;
			o.Order_Type__c = 'PSN';
			countRec++;
		}
		insert ordrList;

		Payment__c payment = new Payment__c();
		payment.Payment_Status__c = 'Charged';
		payment.Order__c = ordrList[0].Id;
		insert payment;
		
		for(Order__c ord :ordrList){
			orderline.add(TestClassUtility.createOrderLine(1, ord.id, null, false)[0]);
			ord.Payment__c = payment.Id;
		}
		insert orderline;
		//update ordrList[0];
		createTestData();
		Integer ordIndx = 0;
		map<id, Order__c> ordMap = new map<id, Order__c>([select id, Name, External_Order_Number__c from Order__c where Id in: ordrList]);
		for(Staging_PPP_Inbound__c stg : stagingPPPList){
			stg.Unit_Model_Number__c = '121';
			stg.Unit_Serial_Number__c = '121Ser';
			stg.PPP_Purchase_Date__c = datetime.Now().format('MMddYYYY');
			stg.Purchase_Date__c = datetime.Now().format('MMddYYYY');
			stg.Proof_Of_Purchase_Date__c = datetime.Now().format('MMddYYYY');
			stg.PPP_Start_Date__c = datetime.Now().format('MMddYYYY');
			stg.PPP_Cancel_Date__c= datetime.Now().addMonths(1).format('MMddYYYY');
			stg.PPP_Purchase_Date__c = datetime.Now().format('MMddYYYY');
			stg.PPP_Refund_by_Dealer__c= '10';
			stg.PPP_Refund_to_SCEA__c = '5';
			stg.Consumer_First_Name__c = 'Test Name';
			stg.Consumer_Last_Name__c = 'Test Name';
			stg.Billing_Address_1__c = 'Bill adr 1';
			stg.Billing_City__c = 'NYC';
			stg.Billing_State__c = 'CA';
			stg.Billing_Country__c = 'USA';
			stg.Order_Record_Type__c = 'A';
			stg.PPP_Claim_Reason__c =  'Test!!2!';
			stg.Order_Number__c = ordMap.get(ordrList[ordIndx].id).External_Order_Number__c;
		}
		AssurantInboundProcessBatchable.claimQueryTest = ' and s.PPP_Claim_Reason__c like \'Test!!2!%\'';
		insert stagingPPPList;
		system.debug('**************stagingPPPList::'+stagingPPPList[0].id +' '+[select id from Staging_PPP_Inbound__c where id in :stagingPPPList and Processed_Date_Time__c=null AND (Order_Record_Type__c='A' or( Order_Record_Type__c='C'))]);
		
		Test.startTest();
			DataBase.executeBatch(new AssurantInboundProcessBatchable(), 5);
		//Todo -- Need to put Assert
		Test.stopTest();
	}

	static void createTestData() {
		stagingPPPList = TestClassUtility.createStagingPPPInbound(5, false);
	}
}