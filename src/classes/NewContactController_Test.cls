//**************************************************************************/
// (c) 2013 Appirio, Inc.
//
// Description : Test class for NewContactController.
//                  
// Original August 30, 2013  : Vinod Kumar(JDC)
// 
//
// ***************************************************************************/
@isTest
private class NewContactController_Test {

    static testMethod void myUnitTest() {
        // create test data
        PageReference pg = Page.NewContact; 
        Test.setCurrentPage(pg); 
        string strFirstName= 'testFirstName'+ string.valueOf(math.random());
        string strLastName = 'testLastName'+ string.valueOf(math.random());
        string strPhone = '0123456';
        string strEmail = 'test@test.com' ;
        ApexPages.currentPage().getParameters().put('FirstName',strFirstName);
        ApexPages.currentPage().getParameters().put('LastName', strLastName);
        ApexPages.currentPage().getParameters().put('Phone', strPhone);
        ApexPages.currentPage().getParameters().put('Email', strEmail);
        ApexPages.currentPage().getParameters().put('Language', 'English');        
        NewContactController objController = new NewContactController();
        objController.isInConsole = false;
        objController.saveContact();
        list<Account> lstAccount = [select id from Account where FirstName =:strFirstName and LastName =: strLastName and PersonEmail =:strEmail];
        //system.assert(lstAccount.size() > 0);
        objController.cancelContact();
        objController.cancel();
    }
}