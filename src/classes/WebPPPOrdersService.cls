/***********************************************************************************************************
Created : 09/08/2014 : Leena Mandadapu : Jira# SMS-654 - WebPPP Revamp project


************************************************************************************************************/
global class WebPPPOrdersService {

    global class WebServiceInput{
        webservice String OrderNumber{get;set;}         //Order__c.External_ID__c
        webservice String FirstName {get;set;}          //Account.FirstName
        webservice String LastName {get;set;}           //Account.LastName
        webservice String Email {get;set;}              //Account.PersonEmail
        webservice String Phonenum {get;set;}           //Account.PersonHomePhone
        webservice String ShippingAddress1 {get;set;}   //Address__c.Address_Line_1__c
        webservice String ShippingAddress2 {get;set;}   //Address__c.Address_Line_2__c
        webservice String ShippingCity {get;set;}       //Address__c.City__c
        webservice String ShippingState {get;set;}      //Address__c.State__c
        webservice String ShippingZipCode {get;set;}    //Address__c.Postal_Code__c
        webservice String ShippingCountry {get;set;}    //Address__c.Country__c
        webservice String ModelType{get;set;}           //Derive generic Model numbers on Asset
        webservice String PPPSKU{get;set;}              //Asset__c.PPP_Product_SKU__c
        webservice String ProofOfPurchaseDate{get;set;} //Asset__c.Purchase_Date__c
        webservice String PPPPurchaseDate{get;set;}     //Asset__c.PPP_Purchase_Date__c
        webservice String PaymentType {get;set;}        //Asset__c.PPP_Payment_Type__c
        webservice String ContractNumber {get;set;}     //Asset__c.PPP_Contract_Number__c
        webservice String ContractStartDt {get;set;}    //Asset__c.PPP_Start_Date__c
        webservice String ContractEndDt {get;set;}      //Asset__c.PPP_End_Date__c
        webservice string BaseAmount {get;set;}         //Order_Line__c.List_Price__c
        webservice String TaxAmount {get;set;}          //Order_Line__c.Tax__c
        webservice String DealerId {get;set;}           //Order__c.ESP_Dealer__c
        webservice String PPPPlanType {get;set;}        //Asset__c.Coverage_Type__c
        webservice String BillingAddress1 {get;set;}    //Address__c.Address_Line_1__c
        webservice String BillingAddress2 {get;set;}    //Address__c.Address_Line_2__c
        webservice String BillingCity {get;set;}        //Address__c.City__c
        webservice String BillingState {get;set;}       //Address__c.State__c
        webservice String BillingZipCode {get;set;}     //Address__c.Postal_Code__c
        webservice String BillingCountry {get;set;}     //Address__c.Country__c
        webservice String SerialNumber {get;set;}       //Asset__c.Serial_Number__c
        webservice String ModelNumber {get;set;}        //Asset__c.Model_Number__c
        webservice String BillingStatus {get;set;}      //Asset__c.PPP_Status__c    
        webservice String POPattachmentURL {get;set;}   //Asset__c.PPP_POP_attachment__c
    }
    
    global class WebServiceResponse{        
        webservice string status;
        webservice WebServiceFailureResponse failResponse{get;set;}
    }
    
    global class WebServiceFailureResponse{
        webservice String message{get;set;}
    }
    
    public class myException extends Exception {}
    
    webservice static WebServiceResponse WebPPPOrderProcess(WebServiceInput request){
        System.Debug('<<<<<<<<<< WebPPP Request >>>>>>>>>>  ' +request);
        WebServiceResponse response = new WebServiceResponse();
        response.failResponse = new WebServiceFailureResponse();
        response.failResponse.message='';
        response.status = 'Failed';
        Savepoint sp = Database.setSavepoint();
        
        try{
            //check if the request is blank
            if(request!=null){
                  //Check for the required fields. If the required fields are blank then don't process the request and reject.
                 if(request.FirstName != null && request.LastName != null && request.OrderNumber != null && request.Email != null && request.Phonenum != null &&
                    request.ShippingAddress1 != null && request.ShippingCity != null && request.ShippingState != null && request.ShippingZipCode != null &&
                    request.ShippingCountry != null && request.PPPSKU != null && request.ContractNumber != null && request.PaymentType != null && request.DealerId != null) {   
                    //process account
                    Account AccConsumer = null;
                    try{
                     AccConsumer = IntegrationServicesUtility.getAccount(request);
                     if(AccConsumer == null){
                        throw new myException('Unable to get/create Account'); 
                     }
                     AccConsumer= [Select Id, ispersonaccount, PersoncontactId from Account where Id=:AccConsumer.Id][0];
                     if(AccConsumer.PersonContactId == null){
                        throw new myException('Unable to set Person Account relationship'); 
                     } 
                    }catch (Exception e){
                        Database.rollback(sp);
                        ExceptionHandler.logException(e);
                        response.status = 'Failed';
                        response.failResponse.message = 'Consumer creation failed';
                        IntegrationServicesUtility.logexception('Unable to find or create Account>>>>'+request, e, 'Purchase');
                        return response;
                    }      
                    //process Ship to Address
                    Address__c ShipAddr = null;
                    try{
                     ShipAddr = IntegrationServicesUtility.getAddress(AccConsumer.PersoncontactId, request.ShippingAddress1, request.ShippingAddress2, request.ShippingCity, request.ShippingState, request.ShippingCountry, 
                     request.ShippingZipCode, 'Shipping');
                     if(ShipAddr == null){
                      throw new myException('Shipping Address not created');    
                     }
                    }
                    catch (Exception e) {
                    Database.rollback(sp);
                    ExceptionHandler.logException(e);   
                    response.status = 'Failed';
                    response.failResponse.message = 'Shipping address creation failed';
                    IntegrationServicesUtility.logexception('Unable to create Shipping Address record>>>>>'+request, e, 'Purchase');
                    return response;        
                    }
                    //process Bill to Address
                    Address__c BillToAddr = null;
                    try{
                     BillToAddr = IntegrationServicesUtility.getAddress(AccConsumer.PersoncontactId, request.BillingAddress1, request.BillingAddress2, request.BillingCity, request.BillingState, request.BillingCountry, 
                     request.BillingZipCode, 'Billing');
                     if(BillToAddr == null){
                      throw new myException('Billing Address not created'); 
                     }
                    }
                    catch (Exception e) {
                    Database.rollback(sp);
                    ExceptionHandler.logException(e);   
                    response.status = 'Failed';
                    response.failResponse.message = 'Billing address creation failed';
                    IntegrationServicesUtility.logexception('Unable to create Billing Address record>>>>>>>'+request, e, 'Purchase');
                    return response;        
                    }
                    //process Asset 
                    Asset__c asset = null;
                     try{
                     asset = IntegrationServicesUtility.getAsset(request, AccConsumer);
                     if(asset == null){
                      throw new myException('Asset not created');   
                     }
                    }
                    catch (Exception e){
                    Database.rollback(sp);
                    ExceptionHandler.logException(e);
                    response.status = 'Failed'; 
                    response.failResponse.message = 'Asset creation failed';
                    IntegrationServicesUtility.logexception('Unable to create Asset/Asset-Consumer>>>>>>>'+request, e, 'Purchase');
                    return response;
                    }
                    //process Order
                    Order__c order = null;
                    try{
                     order = IntegrationServicesUtility.getOrder(request, asset, 'PS.COM', ShipAddr, BillToAddr, AccConsumer);
                     if(order == null){
                        throw new myException('Unable to create Order');
                     }
                    }
                    catch (Exception e){
                    Database.rollback(sp);
                    ExceptionHandler.logException(e);   
                    response.status = 'Failed';
                    response.failResponse.message = 'Unable to process request = '+request.OrderNumber;
                    IntegrationServicesUtility.logexception('Unable to process request = '+request.OrderNumber+'<<<>>>>'+request, e, 'Purchase');
                    return response;
                    }               
                    //order.Order_Status__c = 'Assurant Enrolled';
                    //update order;
                    
                    response.status = 'Success';
                    return response;
                }
            else {
                throw new myexception('Required fields in the request are blank');
            }
         }      
         else {throw new myexception('Request received is blank');}     
        }catch(Exception Ex){
            Database.rollback(sp);
            ExceptionHandler.logException(Ex);
            IntegrationServicesUtility.logexception('Unable to process the request>>>>>>'+request, Ex, 'Purchase');
            response.failResponse.message = 'Unable to process request';
        }
        System.Debug('<<<<<<<<<<<<< WebPPP Purchase process Response >>>>>>>>>>>>  ' +response);
        return response;
    }
    
 webservice static WebServiceResponse WebPPPOrderUpdateProcess(WebServiceInput request) {
        System.Debug('<<<<<<<<<<<<< WebPPP Update Request >>>>>>>>>>>>  ' +request);
        WebServiceResponse response = new WebServiceResponse();
        response.failResponse = new WebServiceFailureResponse();
        response.failResponse.message='';
        response.status = 'Failed';
        Boolean isUpdateSuccess = false;
        Savepoint sp = Database.setSavepoint();
        try{
         if(request != null){
            if(request.OrderNumber != null && request.OrderNumber != '') {
              isUpdateSuccess = IntegrationServicesUtility.UpdatePYBOrderProcess(request);  
              if(isUpdateSuccess){
                response.status = 'Success';
                return response;
              }
              else {
                throw new myexception('Unable to process updates'); 
              }
            } else {
             throw new myexception('Order Number in the request is blank');  
            }
         } else {
            throw new myexception('Request received is blank');
         }  
        } catch(Exception e) {
            Database.rollback(sp);
            ExceptionHandler.logException(e);
            IntegrationServicesUtility.logexception('Unable to process update request>>>>>>>'+request, e, 'Update');
            response.failResponse.message =  'Unable to process updates for order';
            return response;
        }
        System.Debug('<<<<<<<<<<<<< WebPPP Update Response >>>>>>>>>>>>  ' +response);
        return response;
 }
}