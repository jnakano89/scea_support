//**************************************************************************/
// (c) 2013 Appirio, Inc.
//
// Description :Utility class for other test classes so that they can reuse common functions.
//                  
// Original August 26, 2013  : KapiL Choudhary(JDC) Created for the Task T-175201
// Updated :
//         : 05/27/2014 : Aaron Briggs : Hard-code Model_Number__c to Pass Validation
//         : 07/01/2014 : Leena Mandadapu : Added Consumer-Asset creation method
// ***************************************************************************/

public  class TestClassUtility { 

	public static List<Asset__c> createAssets(integer assetCount,Boolean isInsert){
		List<Asset__c> listAsset = new List<Asset__c>();

		for(integer i=0;i<assetCount;i++){
			Asset__c asset = new Asset__c();
			asset.Serial_Number__c = '123'+i;
			//asset.Purchase_Date__c = Date.today().addDays(i-1);
			asset.Purchase_Date__c = Date.today();
			asset.Registration_Date__c = Date.today().addDays(i);
			asset.Console_Type__c = 'test'+i;
			
			//AB - 05/21/2014 - Hard-coded model number to pass validation check.
			//asset.Model_Number__c = '9999'+i;
			asset.Model_Number__c = 'CUH-1001A';
			asset.PlayStation_Plus_Subscription_Start_Date__c = Date.today();
			asset.MDM_ID__c = 'MDMID'+i;
			asset.MDM_Account_ID__c = 'MDMID'+i;
			listAsset.add(asset);
		}
		if(isInsert &&(!listAsset.isEmpty())){
			insert listAsset;
		}
		return listAsset;
	}

	public static List<Order__c> creatOrder(Id ContactId, integer orderCount,Boolean isInsert){
		List<Order__c> orderList = new List<Order__c>();

		for(integer i=0;i<orderCount;i++){
			Order__c orderItem = new Order__c(consumer__c = ContactId);
			orderList.add(orderItem);
		}

		if(!orderList.isEmpty() && isInsert){
			insert orderList;
		}
		return orderList;
	}
	
	//LM 06/17/2014 : Added this method
	public static List<Consumer_Asset__c> createConsumerAsset(Id ContactId, Id AssetId, integer orderCount,Boolean isInsert){
		List<Consumer_Asset__c> ConsumerAssetlst = new List<Consumer_Asset__c>();

		for(integer i=0;i<orderCount;i++){
			Consumer_Asset__c ConAst = new Consumer_Asset__c(consumer__c = ContactId, Asset__c = AssetId);
			ConsumerAssetlst.add(ConAst);
		}

		if(!ConsumerAssetlst.isEmpty() && isInsert){
			insert ConsumerAssetlst;
		}
		return ConsumerAssetlst;
	}

	public static List<Contact> createContact(integer contactCount,Boolean isInsert){
		List<Contact> contactList = new List<Contact>();

		for(integer i=0;i<contactCount;i++){
			//Contact
			Contact contact = new Contact();
			contact.LastName = 'lname'+i;
			contact.Phone = '1234567890';
			contact.Phone_Unformatted__c = '1234567890';
			contactList.add(contact);
		}
		if(!contactList.isEmpty() && isInsert){
			insert contactList;
		}
		return contactList;
	}
	
	//LM 08/25: PSN Account creation
	public static List<PSN_Account__c> createPSNAccount(integer PSNAccountcnt, Id accountId, Boolean isInsert){
		List<PSN_Account__c> PSNAccountList = new List<PSN_Account__c>();
   		
		Contact cnt = [SELECT id,FirstName, LastName FROM Contact WHERE isPersonAccount = true AND AccountId = :accountId];
        
		for(integer i=0;i<PSNAccountcnt;i++){
			//PSN Account
			PSN_Account__c PSNacc = new PSN_Account__c();
			PSNacc.PSN_Account_ID__c = 'PSNAccount'+i;
			PSNacc.PSN_Sign_In_ID__c = 'Test'+i+'@test.com';
			PSNacc.Name = 'PSNNAME'+i;
			PSNacc.Consumer__c = cnt.Id;
			PSNAccountList.add(PSNacc);
		}
	   if(isInsert && !PSNAccountList.isEmpty()){
		insert PSNAccountList;
	   }
		return PSNAccountList;
	}
	
	public static List<Address__c> createAddress(integer addressCount,Boolean isInsert){	
		//create account

		String rtId;
		for(RecordType rt : [select Id from RecordType where SobjectType = 'Account']) {
			rtId = rt.Id;
			break;
		}

		Account account = new Account();
		account.Name = 'test contact';
		account.RecordTypeId = rtId;
		insert account;

		//Contact
		list<Contact> cntList = new list<Contact>();
		for(integer i=0;i<addressCount;i++){
			Contact contact = new Contact();
			contact.LastName = 'lname';
			contact.AccountId = account.Id;
			contact.MDM_ID__c = '1-1-1';
			cntList.add(contact);
		}
		insert cntList;

		list<Address__c> addressList = new list<Address__c>();
		//create address
		for(integer i=0;i<addressCount;i++){
			Address__c address = new Address__c(); 
			address.Consumer__c = cntList[i].Id;
			address.Address_Line_1__c = 'Test Line1';
			address.Address_Line_2__c = 'Test Line2';
			address.Address_Line_3__c = 'Test Line3';
			address.City__c			  = 'ca';
			address.Country__c		  = 'US';
			address.State__c		  = 'TS';
			address.Postal_Code__c    = '123456';
			address.Status__c 		  = 'Active';

			addressList.add(address);
		}

		if(isInsert && (!addressList.isEmpty())){
			insert addressList;
		}

		return addressList;
	}
	public static Account createAccount(Boolean isInsert){	
		//create account
		String rtId;
		for(RecordType rt : [select Id from RecordType where SobjectType = 'Account']) {
			rtId = rt.Id;
			break;
		}
		Account account = new Account();
		account.Name = 'testcontact';
		account.RecordTypeId = rtId;
		if(isInsert ){
			insert account;
		}

		return account;
	}
	public static List<Account> createPersonAccount(integer accountCount,Boolean isInsert){	
		Id personAccountRecId;
		list<Account> personAccountList = new list<Account>();

		for(RecordType rT: [SELECT Name, SobjectType,IsPersonType FROM RecordType WHERE SobjectType='Account' AND IsPersonType=True]){
			personAccountRecId = rT.id;
		}

		for(integer i=0;i<accountCount;i++){
			Account newAccount = new Account();

			if(personAccountRecId != null){
				newAccount.RecordTypeId = personAccountRecId;
			}
			newAccount.FirstName   =  'Person FName'+i;
			newAccount.LastName    =  'Person LName'+i;
			newAccount.phone       =  '12345'+i; 
			newAccount.PersonOtherPhone = '32323232'+i;
			newAccount.PersonEmail =  'testPemail'+i+'@donotsend.com';
			newAccount.PersonMobilePhone =  '98989898';
			newAccount.Alt_E_mail__pc = 'testAemail'+i+'@donotsend.com';
			//newAccount.PSN_Sign_In_ID__pc = 'testpsn'+i+'@donotsend.com';
			newAccount.MDM_ID__c = 'MDMID'+i;

			personAccountList.add(newAccount);
		}
		try{
			if(isInsert && (!personAccountList.isEmpty())){
				insert personAccountList;
			}
		}
		catch(Exception ex){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
		}
		return personAccountList;
	}

	public static List<Zip_Code__c> createZipCode(integer zipCodeCount, Boolean isInsert){
		List<Zip_Code__c> zipCodeList = new List<Zip_Code__c>();

		for(integer i=0;i<zipCodeCount;i++){
			Zip_Code__c zipCode = new Zip_Code__c();
			zipCode.City__c = 'test'+i;
			zipCode.Country__c = 'US';
			zipCode.State__c = 'Test State';
			zipCode.Zip_Code__c = '12345'+i;

			zipCodeList.add(zipCode);   
		}
		try {
			if(isInsert && (!zipCodeList.isEmpty())){
				insert zipCodeList;
			}	   
		}catch(Exception e) {
			Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error occured : ' + e.getMessage()));
		}
		return zipCodeList;
	}

	public static Integration__c createIntegrationRec(Boolean isInsert){
		Integration__c integration = new Integration__c();
		integration.Siras_Partner_ID__c = 'scea-crm-edes2';
		integration.Siras_Endpoint__c = 'https://qarouter.siras.com/edes/';
		integration.Siras_Partner_Passkey__c = 'G5yd3J6G';

		if(isInsert) {
			insert integration;
		}
		return integration;
	}
	public static Case createCase(string strStatus, string strOrigin,Boolean isInsert){
		Case testcase = new Case();
		testcase.Status = strStatus;
		testcase.received_date__c = date.today();
		testcase.shipped_date__c = date.today().addDays(1);
		testcase.outbound_tracking_number__c = '101';
		testcase.outbound_carrier__c = 'TYest1';
		testCase.Origin = strOrigin;
		testCase.Offender__c = 'Test';
		testCase.Product__c = 'PS4';
		testCase.Sub_Area__c = 'Account Help';
		if(isInsert) {
			insert testCase;
		}
		return testCase;
	}
	public static list<Product__c> createProduct (integer accountCount,Boolean isInsert){
		list<Product__c> productList = new list<Product__c>();
		for(integer i=0;i<accountCount;i++){
			Product__c prod = new Product__c();
			prod.Description__c = 'Test Desc'+i;
			prod.Genre__c = 'Test Genre'+i;
			prod.Product_Type__c = 'NA'+i;
			prod.Sub_Type__c = 'Games';
			prod.SKU__c = 'SKU'+i;
			productList.add(prod);
		}
		if(isInsert){
			insert productList;
		}
		return productList;
	}
	public static list<Staging_PPP_Inbound__c> createStagingPPPInbound (integer recCount,Boolean isInsert){
		list<Staging_PPP_Inbound__c> stagList = new list<Staging_PPP_Inbound__c>();
		for(integer i=0;i<recCount;i++){
			Staging_PPP_Inbound__c obj = new Staging_PPP_Inbound__c();
			obj.Unit_Model_Number__c = 'SONY';
			obj.Unit_Serial_Number__c = 'Test'+i;
			obj.PPP_Service_Level__c = 'NA';
			obj.PPP_Contract_Number__c = 'Test'+i;
			obj.PPP_Claim_Reason__c = 'SKU'+i;
			obj.Order_Record_Type__c = 'L';
			obj.Order_Dealer__c = 'SPSN';
			obj.PPP_Currency_Code__c = 'US';
			obj.Processed_Date_Time__c = null;
			obj.Order_Number__c = 'Test SS  Number '+i;
			stagList.add(obj);
		}
		if(isInsert){
			insert stagList;
		}
		return stagList;
	}

	public static User createUser(String profileName) {
		User testUser = new User();
		testUser.Username = 'newuser1432111test@example.com';
		testUser.ProfileId = [SELECT ID FROM Profile WHERE Name =: profileName].ID;
		testUser.LastName = 'New User';
		testUser.Alias = 'nus010';
		testUser.Email = 'test@example.com';
		testUser.CompanyName = 'TestCompany001001';
		testUser.EmailEncodingKey ='ISO-8859-1';
		testUser.LanguageLocaleKey = 'en_US';
		testUser.TimeZoneSidKey ='America/Los_Angeles';
		testUser.LocaleSidKey = 'en_US';       
		testUser.Country = 'TestIndia';
		testUser.IsActive = true;

		insert testUser; 
		return testUser;
	}

	public static void insertCybersourceSettings() {
		Cybersource__c testSetting = new Cybersource__c();
		testSetting.Access_Key__c = 'f77c9224ec5a3979bc838596eced6999';
		testSetting.Algorithm_Name__c = 'hmacSHA256';
		testSetting.Profile_ID__c = 'SFDC001';
		testSetting.Locale__c = 'en';
		testSetting.Payment_Response_URL__c = 'https://ebctest.cybersource.com/ebctest/Query';
		testSetting.Merchant_ID__c = 'scea_esptest';
		testSetting.Username__c = 'djotwani';
		testSetting.Password__c = 'App1rioT3st';
		testSetting.Payment_Response_Version_Number__c = '1.9';
		testSetting.Payment_Response_Type__c = 'transaction';
		testSetting.Payment_Response_Sub_Type__c = 'transactionDetail';
		insert testSetting;
	}

	public static list<Order_Line__c> createOrderLine (integer orderLineCount,Id orderId,Id productId,Boolean isInsert){
		list<Order_Line__c> orderLineList = new list<Order_Line__c>();
		for(integer i=0;i<orderLineCount;i++){
			Order_Line__c orderLine = new Order_Line__c();
			orderLine.Order__c = orderId;
			orderLine.Product__c = productId;
			orderLine.Quantity__c = '1';
			orderLineList.add(orderLine);
		}
		if(isInsert){
			insert orderLineList;
		}
		return orderLineList;
	}
	
	public static Location__c createLocation(Boolean isInsert){
		Location__c location = new Location__c();
		location.Address_Line_1__c = 'Test Line1';
	    location.Address_Line_2__c = 'Test Line2';
	    location.Location_Type__c = 'Sony Store';
	    if(isInsert){
	     	insert location;
		 }
		 return location;
	}
}