// =============================================================================
// (c) 2013 Appirio, Inc.
//
// Scheduler class for batch class ProcessInboundStagingBatch
//
// Created :
// Jan 7, 2014  Shyam Sundar
// =============================================================================

global class ProcessInboundStagingScheduler implements Schedulable {
	
  //================================================================//
  //Custom Schedule Interface, that the class that needs to be scheduled must implement
  //instead of implementing the standard Out of Box SFDC Schedulable Class
  //================================================================//
  public Interface ISchedule { 
    void execute(SchedulableContext sc); 
  }
 
  //===============================================================//
  //Global method from Standard Salesforce Interface Schedulable
  //===============================================================//
  global void execute(SchedulableContext sc){
    //Invoke the Interface method Execute for the custom Class
    //Account Merge is a class, that is to invoked and implements the custom ISchedule interface
    Type targetType = Type.forName('ProcessInboundStagingBatch');   
    if(targetType != null) {  
      ISchedule obj = (ISchedule)targettype.NewInstance();     
      obj.execute(sc);
    }
  }
}