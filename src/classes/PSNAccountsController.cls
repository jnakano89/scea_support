// 8/14/2013 : Urminder : created this class.
public with sharing class PSNAccountsController {
  public String sObjId{get;set;}
  public boolean initializeComponent{get{ init();return true;}set;}
  public list<PSN_Account__c> psnAccList{get;set;}
  public PSNAccountsController() {
  	
  }
  public void init(){
    // This function is required to initialize with the assigned properties as constructor does not know the assigned properties
    String contactId;
    psnAccList = new list<PSN_Account__c>();	
    System.debug('_____parent_________' + sObjId);
    list<Case> csList = new list<Case>();
    list<Contact> cntList = new list<Contact>();
    
    csList = [select Id, ContactId from Case where Id = :sObjId];
    
    if(csList.isEmpty()) {
    	cntList = [select id from Contact where Id = :sObjId];
    	contactId = cntList[0].Id;
    } else{
    	contactId = csList[0].ContactId;
    }
    psnAccList = [select Name, PSN_Sign_In_ID__c, Status__c
    				from PSN_Account__c
    				where Consumer__c = :contactId];
  }
}