/*
    Class Name : GenericWSHandler
    Created By : Ranjeet Singh (JDC)
    Created Date : 27 Aug 2013
    Reason  :   Template parser utility.
*/

    public with sharing class GenericTemplateParser {
        public Set<String> tokens{get;set;}
        String templateBody{get;set;}
        map<string, objectFieldToken>objectFieldMap{get;set;}
        public GenericTemplateParser(String templateBodyP){
            templateBody = templateBodyP;
            tokens = new Set<String>();
            parseTemplate();
        }

        public static boolean IsGUIDToken(objectFieldToken tkn){
          if(tkn!=null && !String.IsEmpty(tkn.objectName)&& !String.IsEmpty(tkn.Field)){
            if(tkn.objectName.equalsIgnoreCase('system') && tkn.Field.equalsIgnoreCase('guid')){
              return true;
            }
          }
          return false;
        }
        public static boolean IsUSERToken(objectFieldToken tkn){
          if(tkn!=null && !String.IsEmpty(tkn.objectName)&& !String.IsEmpty(tkn.Field)){
            if(tkn.objectName.equalsIgnoreCase('$user')){
              return true;
            }
          }
          return false;
        }
        public static String getGlobalValues(String zTkn){
          if(!String.IsEmpty(zTkn)){            
            return getGlobalValues(new objectFieldToken(zTkn));
           }
           return '';
        }
        public static String getGlobalValues(objectFieldToken tkn){
          if(IsGUIDToken(tkn)){
            return StringUtil.getUUID();
          }else{
            if(IsUSERToken(tkn)){
              return String.ValueOf(UtilityGenericWebCallout.currentUser.get(tkn.Field));
            }
          }
          return '';
        }
        
        public String getParsedContent(map<String, object> Web_Call_Settings){
            try{
                String template = templateBody;
                if(Web_Call_Settings!=null){
                    for(String token : tokens){
                        String value;
                        objectFieldToken tkn = new objectFieldToken(token);
                        system.debug('******rel Val:0'+tkn);
                        if(tkn.objectName==null){
                            if (Web_Call_Settings.containsKey(tkn.tokenText))
                                value = String.ValueOf(Web_Call_Settings.get(tkn.tokenText));
                        }else{
                        		sObject Val = null;                        		
                        		system.debug('******rel Val:1'+tkn.objectNames+' >> '+Val);
                        		if(tkn.objectNames.size()>0){
	                        		for(String nestedObj : tkn.objectNames){
	                        			if(Val==null){
	                        				Val = (sObject)Web_Call_Settings.get(nestedObj);
	                        			}else{
	                        				system.debug('******rel Val:2'+nestedObj+' >> '+tkn.objectName);	                        				
	                            		Val = Val.getSObject(nestedObj);
	                            		system.debug('******rel Val:3'+nestedObj+' >> '+val);
	                        			}
	                        		}
                        		}
	                          if(Val!=null){
	                          			system.debug('******rel Val:4'+tkn.Field+' :: '+Val);
	                                value = String.valueOf(Val.get(tkn.Field));
	                                //tkn.tokenText
	                          }
                        }
                        if(value==null){
                            value = '';
                        }
                        if(!tkn.tokenText.startsWithIgnoreCase('XMLcontent-')){
                          value = value.replace('&','&amp;').replace('"','&quot;').replace('\'','&apos;').replace('<','&lt;').replace('>','&gt;');
                        }
                        template = template.replace('{!'+tkn.tokenText+'}', value);
                    }
                }
                return template;
            }
            catch (Exception ex){
        ExceptionHandler.logException(ex);
        return '';
      }
        }
        
        public void parseTemplate(){
            try{
                if(templateBody==null || templateBody.trim()==''){
                    return;
                }
                
                string parseTexttemplate = templateBody;
                while(parseTexttemplate!=null && parseTexttemplate.length()>0){
                    string zToken=null;
                    integer tokenStart= parseTexttemplate.indexOf('{!');
                    if(tokenStart!=-1){
                        integer tokenEnd = parseTexttemplate.indexOf('}', tokenStart);
                        if(tokenEnd!=-1){
                            //Extracted the token.
                            //zToken= parseTexttemplate.substring(tokenStart, tokenEnd+1).trim();
                            zToken= parseTexttemplate.substring(tokenStart+2, tokenEnd).trim();
                            if(zToken!=null){
                                tokens.Add(zToken);                         
                            }
                            if(zToken!=null && zToken!='' && parseTexttemplate.length()>(tokenEnd+3)){
                                parseTexttemplate=parseTexttemplate.substring(tokenEnd+1);
                            }else{
                                parseTexttemplate='';
                            }
                        }
                    }
                    if(zToken==null){   
                        parseTexttemplate= null;
                    }
                }
            }
            catch (Exception ex){
        ExceptionHandler.logException(ex);
      }
      
    }
    public class objectFieldToken{      
        public string tokenText{get;set;}
        public List<string> objectNames{get;set;}
        public string objectName{get;set;}
        public string Field{get;set;}
        public string tkn_key{
            get{
                if(objectName==null || objectName.trim()=='' || Field==null || Field.trim()==''){                   
                    return null;
                }               
                return (objectName+'.'+Field).toLowerCase().trim();
            }
        }
        public objectFieldToken(string token){
            addToken(token);                
        }
        public void addToken(string zToken){
            if(zToken!=null && zToken.trim()!=''){            
              integer tokenStart= zToken.indexOf('{!');
              if(tokenStart!=-1){
                  integer tokenEnd = zToken.indexOf('}', tokenStart);
                  if(tokenEnd!=-1){
                      //Extracted the token.
                      //zToken= parseTexttemplate.substring(tokenStart, tokenEnd+1).trim();
                      zToken= zToken.substring(tokenStart+2, tokenEnd).trim();   
                  }
              }            
            }
            if(zToken!=null && zToken.trim()!=''){
                tokenText= zToken;               
                List<String>lstObjectNames = zToken.split('\\.');
                if(lstObjectNames.size()>0){
                    Field= lstObjectNames[lstObjectNames.size()-1].trim();
                }
                system.debug('******rel Val:777> '+Field+' >> '+lstObjectNames+' <> '+tokenText);
                if(lstObjectNames.size()>1){
                		objectNames  = lstObjectNames;
                		lstObjectNames.remove(lstObjectNames.size()-1);
                    objectName = String.join(lstObjectNames, '.').trim();
                    system.debug('******rel Val:777> '+objectNames+' >> '+objectName);
                }
            }           
        }        
    }    
    
   /* @IsTest public static void Test_GenericTemplateParser() {
        string sampleMail= 'Dear Mr./Ms. {!Contact.LastName},'+
        'Thank you for bringing the matter at our restaurant to our attention. I would like to follow up with you directly on the telephone because at Chipotle we take this matter very seriously. Please provide me with your telephone number at your earliest convenience.'+
        'Sincerely,'+
        '{!User.FirstName}{!User.LastName}'+
        '{!User.FirstName}{!User.LastName} | {!User.Title}'+
        'Chipotle Mexican Grill';
        GenericTemplateParser tplProcer= new GenericTemplateParser(sampleMail);
        tplProcer.parseTemplate();
        List<User> otherUserList = [Select Id from User where Id <> :UserInfo.getUserId() and IsActive = true limit 1];
        User otherUser = null;
        if(otherUserList.size() == 0)
        {
            List<Profile> prof = [Select Id from Profile limit 1]; 
            otherUser = new User(FirstName='First Name _ Test', LastName='Last Name _ Test', Title='Title Test', Alias='testxyz1',emailencodingkey='UTF-8', languagelocalekey='en_US',timezonesidkey='America/Los_Angeles',  CommunityNickname='XYZTEST12301', Email='ranjeet.singh1@metacube.com', username='UnitTest123fake@testfake.com',localesidkey='en_US', ProfileId=prof.get(0).Id);
            insert otherUser;
        }else
        {
            otherUser = otherUserList.get(0);
        }
        Contact con = new Contact();
        con.LastName = 'contLastName';              
        insert con ;
        map<string, string>mapObject_Name_Id = new map<string, string>();
        mapObject_Name_Id.put('user', ' Id =\''+otherUser.id+'\''); 
        mapObject_Name_Id.put('contact', ' Id =\''+con.id+'\'');
        tplProcer.getParsedContent(mapObject_Name_Id);
    }*/
        
}