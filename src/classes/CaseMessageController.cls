public without sharing class CaseMessageController {
	
	public Case caseObj{get;set;}
	
	//---------------------------------------------------------------------------
    // Constructor and method
    //---------------------------------------------------------------------------
	public CaseMessageController(ApexPages.StandardController controller) {
        caseObj =  (Case)controller.getRecord(); 
        caseObj = getCaseData();
        
        
    }
    
	//---------------------------------------------------------------------------
    // Method to get the case records
    //---------------------------------------------------------------------------
    private Case getCaseData() {
    	Case caseDatas;
    	for(Case caseData : [Select Id,Error_Message__c From Case Where Id = :caseObj.Id]){
    		caseDatas = caseData;	
    	}
    	return caseDatas;	 
    }
    
    //---------------------------------------------------------------------------
    // Method to update Error Message
    //---------------------------------------------------------------------------
    public void doUpdate() {
    	if(caseObj.Error_Message__c != null && caseObj.Error_Message__c == 'ASB Resend has been sent.'){
        	update new Case(Id = caseObj.Id, Error_Message__c = '');
        }	 
    }
}