public with sharing class StagingContactManagement {
    
    public static void beforeInsert(List<Staging_Contact__c> contactList){
    } 

    public static void beforeUpdate(List<Staging_Contact__c> contactList, Map<Id, Staging_Contact__c> oldContactMap){
	}   

    public static void afterInsert(List<Staging_Contact__c> contactList){
    	
		OSB__c osbSettings = OSB__c.getOrgDefaults();
    	
    	if (osbSettings.Update_Consumer_Enabled__c){
			callWebService(contactList);
    	}
    } 

    public static void afterUpdate(List<Staging_Contact__c> contactList, Map<Id, Staging_Contact__c> oldContactMap){
	}   
 
    public static void callWebService(List<Staging_Contact__c> contactList){
    	set<Id> stConIdSet = new set<Id>();
    	for(Staging_Contact__c sCon : contactList){
    		
    		stConIdSet.add(sCon.id);
    	}
    	if(!stConIdSet.isEmpty()){
    		updateConsumerList(stConIdSet);
    	}
    }
@future(callout=true)

	private static void updateConsumerList(set<Id> stConIdSet){
		
		 ConsumerSearchResult conResult = new ConsumerSearchResult();
		 
		 list<ConsumerServiceUtility.WrapperContacts> csUtilWrpContacts = new  list<ConsumerServiceUtility.WrapperContacts>();
		 list<Staging_Contact__c> stagingContactList = new list<Staging_Contact__c>();
		 for(Staging_Contact__c sCon : [select MDM_Contact_ID__c, Email_Address__c, Phone__c
		 									from Staging_Contact__c
		 									where Id IN :stConIdSet])  {
		   	stagingContactList.add(sCon);
		 }
		 for(Staging_Contact__c sCon :stagingContactList){
		 	csUtilWrpContacts.add(new ConsumerServiceUtility.WrapperContacts(sCon.MDM_Contact_ID__c ,sCon.Email_Address__c, sCon.Phone__c));
		 }
		
		 if(!csUtilWrpContacts.isEmpty()){
		 	conResult = ConsumerServiceUtility.updateConsumer(csUtilWrpContacts);
		 }

		list<Staging_Contact__c>contactList = [select id,Processed_Date_Time__c,Errors__c from Staging_Contact__c where id in:stConIdSet];
		for(Staging_Contact__c sCon :contactList){
    			if(conResult.Status != null){
    				scon.Processed_Date_Time__c = Datetime.now();
    			}
    			else{
    				scon.Errors__c = conResult.ErrorMessage;
    			}
    	}
    	if(!contactList.isEmpty()){
    		update contactList;
    	}
	}
}