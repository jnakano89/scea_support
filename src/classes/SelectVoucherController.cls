/*****************************************************************************************************************************************************
Class           : SelectVoucherController
Description     : Custom controller class responsible for voucher issuing within agent console.
Developer       : Appirio
Created Date    : 
Modified        : 03/23/2015 : Aaron Briggs : SMS-1405 : Support Permissioning of New Voucher Type 'PlayStation Vue'
*****************************************************************************************************************************************************/
public without sharing class SelectVoucherController {
  
  public Voucher__c availableVoucher{get;set;}
  public Voucher__c selectedVoucher{get;set;}
  public boolean showResult{get;set;}
  Public String NO_MATCHES_FOUND{get;set;}
  public boolean resultFound{get;set;}
  public boolean hasErrMsg{get{return ApexPages.hasMessages();}set;}
  public list<SelectOption> countryList{get;set;}
  public String voucherAmount{get;set;}
  String caseId;
  
  final String ACCOMODATION_VOUCHER = 'Accommodation Voucher';
  final String PRICE_ADJUSTMENT_VOUCHER = 'Price Adjustment';
  //AB : 03/23/15 : Add new PlayStation Vue Voucher Type
  final String PS_VUE_VOUCHER = 'PlayStation Vue';
    
  public SelectVoucherController(ApexPages.StandardController ctrl) {
    availableVoucher = new Voucher__c();
    caseId = ctrl.getId();
    showResult = false;
    resultFound = false;
    voucherAmount = '';
    NO_MATCHES_FOUND = 'There are no vouchers available that meet the entered criteria. Please modify the criteria and search again or cancel';
  }
 
  public static String stripNonNumbers(String input) {
    String output;
    output=input.replaceAll('[$]', '');
    return output;
  }

  public void findAvailableVoucher() {
    
    showResult = true;
    resultFound = false;
    list<Voucher__c> voucherList = new list<Voucher__c>();
    List<Voucher_Permission__c> voucherPermissions = new list<Voucher_Permission__c>();
 
    String profileName;
    
    for(Profile p : [Select p.Name, p.Id From Profile p where Id =: Userinfo.getProfileId()]){
        profileName = p.name;
    }

    Double amount = 0.0;

    for(Voucher_Permission__c permission : [Select v.Voucher_Type__c, v.Name, v.Id, v.Amount__c, v.Profile__c 
                                                From Voucher_Permission__c v 
                                                        where v.Profile__c =: profileName
                                                                and v.Voucher_Type__c =: availableVoucher.Select_Type__c]){
        voucherPermissions.add(permission); 
    }

    String qry = 'select Voucher_Code__c, Name, Country__c, Type__c, Amount__c, Status__c, Expiration_Date__c ' + //AG: added Expiration Date 3.24.2104
                 ' from Voucher__c ' + 
                 ' where Status__c = \'Available\' AND Country__c = \'' + availableVoucher.Select_Country__c  + '\'' + 
                 ' AND Type__c = \'' + availableVoucher.Select_Type__c + '\'' ; 
                  
    //AB : 03/23/15 : Add new PlayStation Vue Voucher Type
    if(availableVoucher.Select_Type__c == ACCOMODATION_VOUCHER || availableVoucher.Select_Type__c == PRICE_ADJUSTMENT_VOUCHER || availableVoucher.Select_Type__c == PS_VUE_VOUCHER) {

        System.debug('>>>>>>>> voucherAmount ='+ voucherAmount );
        System.debug('>>>>>>>>stripNonNumbers(voucherAmount)='+stripNonNumbers(voucherAmount));
        amount = Double.valueOf(stripNonNumbers(voucherAmount));
        qry += ' AND Amount__c = :amount';
    }

    qry += ' order by Name desc '; 
    voucherList = Database.query(qry);

    for(Voucher__c v : voucherList) {
        
        if(profileName=='System Administrator'){
            selectedVoucher = v;
            resultFound = true;
            break;          
        }

        for(Voucher_Permission__c voucherPermission : voucherPermissions){
                 
            if(voucherPermission.Amount__c != null){ 
                
                if(voucherPermission.Amount__c == v.Amount__c){
            
                    selectedVoucher = v;
                    resultFound = true;
                    break;      
                }            
            }else{
                    selectedVoucher = v;
                    resultFound = true;
                    break;
            }
        }
    }

    System.debug('>>>>>>>>>>>>availableVoucher='+availableVoucher);
  }
  
  public void assignVoucher() {
    Case cs = [select ContactId , id 
                from Case 
                where Id = :caseId];
                
    selectedVoucher.Case__c = cs.Id;
    selectedVoucher.Consumer__c = cs.ContactId;
    selectedVoucher.Status__c = 'Distributed';
    update selectedVoucher;  
  }

}