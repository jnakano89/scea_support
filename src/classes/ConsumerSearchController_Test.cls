//9/15/2013 : Urminder(JDC) : created this test class.
@isTest
global class ConsumerSearchController_Test {

    static testMethod void myUnitTest() { 
       Test.startTest();
       Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
       
       ApexPages.currentPage().getParameters().put('ANI', '1234567890');
       ApexPages.currentPage().getParameters().put('mdiLanguage','English');
       ConsumerSearchController ctrl = new ConsumerSearchController();
       ctrl.selectConsumer();
       ctrl.callGetConsumers();
       
       ctrl.searchConsumerByFilter();
       
       ctrl.consumerPhoneToSearch = '1234567890';
       ctrl.consumerEmailToSearch = 'test@email.com.test';
       ctrl.consumerPSNOnlineIdToSearch = 'Test@testid.com';
       System.assertEquals(ctrl.resultList.size(), 1, 'Result List size should be match'); 
       ctrl.searchConsumerByFilter();
       ctrl.gotoContact();
       
       ctrl.consumerPhoneToSearch = '1234567890';
       ctrl.consumerEmailToSearch = null;
       ctrl.consumerPSNOnlineIdToSearch = null;
        ctrl.searchConsumerByFilter();
        ctrl.consumerPhoneToSearch = '1234567890';
       ctrl.consumerEmailToSearch = null;
       ctrl.consumerPSNOnlineIdToSearch = 'Test@testid.com';
        ctrl.searchConsumerByFilter();
        ctrl.consumerPhoneToSearch = null;
       ctrl.consumerEmailToSearch = 'test@email.com.test';
       ctrl.consumerPSNOnlineIdToSearch = 'Test@testid.com';
        ctrl.searchConsumerByFilter();
       ctrl.gotoContact();
       
       Test.stopTest();
    }
    static testMethod void myUnitTest2() {
       Test.startTest();
       Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
       ApexPages.currentPage().getParameters().put('ctRadio', '2-2-2-2');
       ApexPages.currentPage().getParameters().put('ANI', '1234');
       ConsumerSearchController ctrl = new ConsumerSearchController();
       try{
       ctrl.fetchConsumer(null, null, 'Test@testid.com');
       } catch(Exception ex){}
        
       Test.stopTest();
      
       ctrl.selectedMdmContactId = '2-2-2-2';
       ctrl.selectConsumer();
       //ctrl.refereshConsumer();
       
    }
    static testMethod void myUnitTest3() {
       Test.startTest();
       Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
       ApexPages.currentPage().getParameters().put('ctRadio', '2-2-2-2');
       ApexPages.currentPage().getParameters().put('ANI', '1234');
       ConsumerSearchController ctrl = new ConsumerSearchController();
       list<contact> cntList=TestClassUtility.createContact(1, true) ;     
       
       //ctrl.selectConsumer();
       ctrl.selectedcontact= cntList[0];
        Sony_MiddlewareConsumerdata3.Address_mdm address = new Sony_MiddlewareConsumerdata3.Address_mdm();
        address.AddressId = '1-1-1-1';
        address.City = 'City';
        address.ContactAddressType = 'Test';
        address.Country = 'US';
        address.PostalCode = '123456';
        address.State = 'LA';
        address.StreetAddress = 'AddressLine1';
        address.StreetAddress2 = 'AddressLine2';
        address.StreetAddress3 = 'AddressLine3';
        
        Sony_MiddlewareConsumerdata3.ListOfAddress_mdm listOfAddress = new Sony_MiddlewareConsumerdata3.ListOfAddress_mdm();
        listOfAddress.address = new list<Sony_MiddlewareConsumerdata3.Address_mdm>{address};
        
        Sony_MiddlewareConsumerdata3.Contact_mdm cnt = new Sony_MiddlewareConsumerdata3.Contact_mdm();
        cnt.ListOfAddress = listOfAddress;
        cnt.MDMRowId = '2-2-2-2';
        cnt.PSNSignInId = 'Test@testid.com';
        cnt.LastName = 'Test Contact';
        cnt.CRMPhone = '1234567890';
        
        Sony_MiddlewareConsumerdata3.ListOfContact_mdm lCnt = new Sony_MiddlewareConsumerdata3.ListOfContact_mdm();
        lCnt.Contact = new list<Sony_MiddlewareConsumerdata3.Contact_mdm>{cnt};
        
        Sony_MiddlewareConsumerdata3.Account_mdm acc = new Sony_MiddlewareConsumerdata3.Account_mdm();
        acc.ListOfContact = lCnt;
        acc.MDMAccountRowId = '3-3-3-3';
        acc.AccountStatus = 'Active';
        //code added by preetu : to increase code coverage
        acc.PSPlusSubscriber = 'true';
        acc.PSPlusStartDate = '2014-01-20 00:00:00';
        acc.PSPlusStackedEndDate = '2014-01-20 00:00:00';
        acc.PSPlusAutoRenewalFlag = 'True';
        //Sony_MiddlewareConsumerdata3.ListOfAccount_mdm lAcc = new Sony_MiddlewareConsumerdata3.ListOfAccount_mdm();
        //lAcc.Account = new list<Sony_MiddlewareConsumerdata3.Account_mdm>{acc};
        
        ConsumerSearchResult searchResult = new ConsumerSearchResult();
        searchResult.listOfAccounts = new list<Sony_MiddlewareConsumerdata3.Account_mdm>{acc};
        ctrl.searchResult = searchResult;
        ctrl.selectConsumer();
        test.stopTest();
        ctrl.refreshConsumer();
        ctrl.resetSearchStatus();
        ctrl.createAssetRecords();
        ApexPages.StandardController controller;
        ConsumerSearchController ctrl2 = new ConsumerSearchController(controller);
        //Test.stopTest();
    }
    global class WebServiceMockImpl implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,    
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
                Sony_MiddlewareConsumerdata3.PSConsumerLookup_Output_element responseElm = 
                    new Sony_MiddlewareConsumerdata3.PSConsumerLookup_Output_element();
                
                
                Sony_MiddlewareConsumerdata3.Address_mdm address = new Sony_MiddlewareConsumerdata3.Address_mdm();
                address.AddressId = '1-1-1-1';
                address.City = 'City';
                address.ContactAddressType = 'Test';
                address.Country = 'US';
                address.PostalCode = '123456';
                address.State = 'LA';
                address.StreetAddress = 'AddressLine1';
                address.StreetAddress2 = 'AddressLine2';
                address.StreetAddress3 = 'AddressLine3';
                
                Sony_MiddlewareConsumerdata3.ListOfAddress_mdm listOfAddress = new Sony_MiddlewareConsumerdata3.ListOfAddress_mdm();
                listOfAddress.address = new list<Sony_MiddlewareConsumerdata3.Address_mdm>{address};
                
                Sony_MiddlewareConsumerdata3.Contact_mdm cnt = new Sony_MiddlewareConsumerdata3.Contact_mdm();
                cnt.ListOfAddress = listOfAddress;
                cnt.MDMRowId = '2-2-2-2';
                cnt.PSNSignInId = 'Test@testid.com';
                cnt.LastName = 'Test Contact';
                cnt.OnlineId = 'Test@testid.com';
                
                Sony_MiddlewareConsumerdata3.ListOfContact_mdm lCnt = new Sony_MiddlewareConsumerdata3.ListOfContact_mdm();
                lCnt.Contact = new list<Sony_MiddlewareConsumerdata3.Contact_mdm>{cnt};
                
                Sony_MiddlewareConsumerdata3.Account_mdm acc = new Sony_MiddlewareConsumerdata3.Account_mdm();
                acc.ListOfContact = lCnt;
                acc.MDMAccountRowId = '3-3-3-3';
                acc.AccountStatus = 'Active';
                
                Sony_MiddlewareConsumerdata3.ListOfAccount_mdm lAcc = new Sony_MiddlewareConsumerdata3.ListOfAccount_mdm();
                lAcc.Account = new list<Sony_MiddlewareConsumerdata3.Account_mdm>{acc};
                
                responseElm.ListOfAccount = lAcc;
                             
                response.put('response_x', responseElm); 
               }
    }
    
     global class WebServiceMockConsumerRefresh implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,    
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
                Sony_MiddlewareConsumerRefresh_Final.PSConsumerRefresh_Output_element responseElm = 
                    new Sony_MiddlewareConsumerRefresh_Final.PSConsumerRefresh_Output_element();
                
                
                Sony_MiddlewareConsumerRefresh_Final.Address_mdm address = new Sony_MiddlewareConsumerRefresh_Final.Address_mdm();
                address.AddressId = '1-1-1-1';
                address.City = 'City';
                address.ContactAddressType = 'Test';
                address.Country = 'US';
                address.PostalCode = '123456';
                address.State = 'LA';
                address.StreetAddress = 'AddressLine1';
                address.StreetAddress2 = 'AddressLine2';
                address.StreetAddress3 = 'AddressLine3';
                
                Sony_MiddlewareConsumerRefresh_Final.ListOfAddress_mdm listOfAddress = new Sony_MiddlewareConsumerRefresh_Final.ListOfAddress_mdm();
                listOfAddress.address = new list<Sony_MiddlewareConsumerRefresh_Final.Address_mdm>{address};
                
                Sony_MiddlewareConsumerRefresh_Final.Account_mdm acc = new Sony_MiddlewareConsumerRefresh_Final.Account_mdm();
                
                acc.MDMAccountRowId = '3-3-3-3';
                acc.AccountStatus = 'Active';
                
                //Sony_MiddlewareConsumerRefresh_Final.ListOfAccount_mdm lAcc = new Sony_MiddlewareConsumerRefresh_Final.ListOfAccount_mdm();
                //lAcc.Account = new list<Sony_MiddlewareConsumerRefresh_Final.Account_mdm>{acc};
                
                Sony_MiddlewareConsumerRefresh_Final.Contact_mdm cnt = new Sony_MiddlewareConsumerRefresh_Final.Contact_mdm();
                cnt.ListOfAddress = listOfAddress;
                cnt.MDMRowId = '2-2-2-2';
                cnt.PSNSignInId = 'Test@testid.com';
                cnt.LastName = 'Test Contact';
                cnt.Account = new list<Sony_MiddlewareConsumerRefresh_Final.Account_mdm>{acc};
                
                Sony_MiddlewareConsumerRefresh_Final.ListOfContact_mdm lCnt = new Sony_MiddlewareConsumerRefresh_Final.ListOfContact_mdm();
                lCnt.Contact = new list<Sony_MiddlewareConsumerRefresh_Final.Contact_mdm>{cnt};
                
                responseElm.listOfContact = lCnt;
                             
                response.put('response_x', responseElm); 
               }
    }
    
}