/**
9/18/2013 : Urminder(JDC) : created this test class for ConsumerServiceUtility Class.
 */
@isTest
global class ConsumerServiceUtility_Test {

     static testMethod void getConsumersTest() {
       Test.startTest();
       Test.setMock(WebServiceMock.class, new WebServiceMockGetConsumer());
       
       ConsumerSearchResult result = ConsumerServiceUtility.getConsumers('123456', null, null);
       System.assertEquals(result.ListOfAccounts.size(), 1 , 'List Size should match');
       Test.stopTest();
    }
    static testMethod void getConsumersTest2() {
       Test.startTest();
       Test.setMock(WebServiceMock.class, new WebServiceMockGetConsumer());
       
       ConsumerSearchResult result = ConsumerServiceUtility.getConsumers(null, 'test@Email.com', 'test@email.com');
       //System.assertEquals(result.ListOfAccounts.size(), 1 , 'List Size should match');
       Test.stopTest();
    }
    static testMethod void getConsumersTest3() {
       Test.startTest();
       Test.setMock(WebServiceMock.class, new WebServiceMockGetConsumer2());
       ConsumerSearchResult result = ConsumerServiceUtility.getConsumers(null, 'test@Email.com', 'test@email.com');
       Test.stopTest();
    }
    static testmethod void updateConsumerTest() {
      	Test.startTest();
      	Test.setMock(WebServiceMock.class, new WebServiceMockUpdateConsumer());
      	ConsumerServiceUtility.WrapperContacts wrapCnt = new ConsumerServiceUtility.WrapperContacts('1-1-1-1', 'test@testEmail.com', '123123');
      	
      	ConsumerSearchResult result = ConsumerServiceUtility.updateConsumer(new list<ConsumerServiceUtility.WrapperContacts>{wrapCnt});
      	System.assertEquals(result.Status, 'Sucess','Status Should match');
      	Test.stopTest();
    }
    static testmethod void updateConsumerTest2() {
      	Test.startTest();
      	Test.setMock(WebServiceMock.class, new WebServiceMockUpdateConsumer2());
      	ConsumerServiceUtility.WrapperContacts wrapCnt = new ConsumerServiceUtility.WrapperContacts('1-1-1-1', 'test@testEmail.com', '123123');
      	
      	ConsumerSearchResult result = ConsumerServiceUtility.updateConsumer(new list<ConsumerServiceUtility.WrapperContacts>{wrapCnt});
      	Test.stopTest();
    }
    
    static testMethod void refreshConsumerTest() {
    	Test.startTest();
    	Test.setMock(WebServiceMock.class, new WebServiceMockRefreshConsumer());
    	ConsumerSearchResult result = ConsumerServiceUtility.refreshConsumer('1-2-3-4');
    	system.assertEquals(result.listOfContacts.size(), 1, 'Result Size should Match');
    	Test.stopTest();
    }
    
    static testMethod void createAddressTest() {
    	Test.startTest();
    	Test.setMock(WebServiceMock.class, new WebServiceMockCreateAddress());
    	
    	
    	Sony_MiddlewareUpdateInsertAddress2.Address add =  new Sony_MiddlewareUpdateInsertAddress2.Address();
		add.city='test';
		add.state='test';
		
		
		Sony_MiddlewareUpdateInsertAddress2.ListOfAddress ladd = new Sony_MiddlewareUpdateInsertAddress2.ListOfAddress();
		ladd.Address = new list<Sony_MiddlewareUpdateInsertAddress2.Address>{add};
		
		Sony_MiddlewareUpdateInsertAddress2.Contact cnt = new Sony_MiddlewareUpdateInsertAddress2.Contact();
		cnt.MDMRowId = '1-4EB-1';
		cnt.ListOfAddress = ladd;
		
		Sony_MiddlewareUpdateInsertAddress2.ListOfContact listOfContact = new Sony_MiddlewareUpdateInsertAddress2.ListOfContact();
		listOfContact.Contact = new list<Sony_MiddlewareUpdateInsertAddress2.Contact>{cnt};
    	
    	
    	ConsumerSearchResult result = ConsumerServiceUtility.createAddress(listOfContact);    	
    	system.assertEquals(result.listOfContactForUpdateInsertAddress.size(), 1, 'Result Size should Match');
    	
    	Test.stopTest();
    }
    
    
    static testMethod void updateAssetTest() {
    	Test.startTest();
    	Test.setMock(WebServiceMock.class, new WebServiceMockUpdateAsset());
    	
    	Sony_MiddlewareUpdateAccountAsset.Asset_mdm  ast = new Sony_MiddlewareUpdateAccountAsset.Asset_mdm();
		ast.AssetRowId = '1';
		ast.SerialNumber = '1234';
		ast.ProductName = 'testing';
		
		
		Sony_MiddlewareUpdateAccountAsset.ListOfAsset_mdm   last = new Sony_MiddlewareUpdateAccountAsset.ListOfAsset_mdm ();
		last.Asset = new list<Sony_MiddlewareUpdateAccountAsset.Asset_mdm>{ast};
		
		
		Sony_MiddlewareUpdateAccountAsset.Account_mdm   acc = new Sony_MiddlewareUpdateAccountAsset.Account_mdm ();
		acc.MDMAccountRowId = '1234567';
		acc.ListOfAsset = last;
		
		
		Sony_MiddlewareUpdateAccountAsset.ListOfAccount_mdm    lacc = new Sony_MiddlewareUpdateAccountAsset.ListOfAccount_mdm  ();
		lacc.Account  = new list<Sony_MiddlewareUpdateAccountAsset.Account_mdm>{acc};
    	
    	ConsumerSearchResult result = ConsumerServiceUtility.updateAsset(lacc);    	
    	system.assertEquals(result.listOfAccountForUpdateAccountAsset.size(), 1, 'Result Size should Match');
    	
    	Test.stopTest();    
    }
    global class WebServiceMockGetConsumer implements WebServiceMock {
	   global void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,	
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	      		Sony_MiddlewareConsumerdata3.PSConsumerLookup_Output_element responseElm = 
	      		  	new Sony_MiddlewareConsumerdata3.PSConsumerLookup_Output_element();
	       		
	       		
	       		Sony_MiddlewareConsumerdata3.Address_mdm address = new Sony_MiddlewareConsumerdata3.Address_mdm();
	       		address.AddressId = '1-1-1-1';
				address.City = 'City';
				address.ContactAddressType = 'Test';
				address.Country = 'US';
				address.PostalCode = '123456';
				address.State = 'LA';
				address.StreetAddress = 'AddressLine1';
				address.StreetAddress2 = 'AddressLine2';
				address.StreetAddress3 = 'AddressLine3';
				
				Sony_MiddlewareConsumerdata3.ListOfAddress_mdm listOfAddress = new Sony_MiddlewareConsumerdata3.ListOfAddress_mdm();
				listOfAddress.address = new list<Sony_MiddlewareConsumerdata3.Address_mdm>{address};
				
				Sony_MiddlewareConsumerdata3.Contact_mdm cnt = new Sony_MiddlewareConsumerdata3.Contact_mdm();
				cnt.ListOfAddress = listOfAddress;
				cnt.MDMRowId = '2-2-2-2';
				cnt.PSNSignInId = 'Test@testid.com';
				cnt.LastName = 'Test Contact';
				
				
				Sony_MiddlewareConsumerdata3.ListOfContact_mdm lCnt = new Sony_MiddlewareConsumerdata3.ListOfContact_mdm();
				lCnt.Contact = new list<Sony_MiddlewareConsumerdata3.Contact_mdm>{cnt};
				
				Sony_MiddlewareConsumerdata3.Account_mdm acc = new Sony_MiddlewareConsumerdata3.Account_mdm();
				acc.ListOfContact = lCnt;
				acc.MDMAccountRowId = '3-3-3-3';
				acc.AccountStatus = 'Active';
				
				Sony_MiddlewareConsumerdata3.ListOfAccount_mdm lAcc = new Sony_MiddlewareConsumerdata3.ListOfAccount_mdm();
				lAcc.Account = new list<Sony_MiddlewareConsumerdata3.Account_mdm>{acc};
				
				responseElm.ListOfAccount = lAcc;
				       		 
	       		response.put('response_x', responseElm); 
	   		   }
    }
    global class WebServiceMockGetConsumer2 implements WebServiceMock {
	   global void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,	
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	      		Sony_MiddlewareConsumerdata3.PSConsumerLookup_Output_element responseElm = 
	      		  	new Sony_MiddlewareConsumerdata3.PSConsumerLookup_Output_element();
	       		
	       		
	       		
				
				responseElm.ErrorMessage = new Sony_MiddlewareConsumerdata3.ErrorType();
				responseElm.ErrorMessage.ErrorCode = '10';
			    responseElm.ErrorMessage.ErrorMessage = 'Test Custom Error';
			    responseElm.ErrorMessage.Description = 'test desc';
				       		 
	       		response.put('response_x', responseElm); 
	   		   }
    }
    
    global class WebServiceMockUpdateConsumer implements WebServiceMock {
	   global void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,	
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	      		Sony_MiddlewareUpdateconsumer_Final.PSUpdateConsumer_Output_element responseElm = 
	      		  	new Sony_MiddlewareUpdateconsumer_Final.PSUpdateConsumer_Output_element();
	       		
	       		
				responseElm.Message = 'Updated SucessFully';
			    responseElm.Status = 'Sucess'; 
	       		response.put('response_x', responseElm); 
	   		   }
    }
    global class WebServiceMockUpdateConsumer2 implements WebServiceMock {
	   global void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,	
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	      		Sony_MiddlewareUpdateconsumer_Final.PSUpdateConsumer_Output_element responseElm = 
	      		  	new Sony_MiddlewareUpdateconsumer_Final.PSUpdateConsumer_Output_element();
	       		
	       		
				responseElm.Message = 'Updated SucessFully';
			    responseElm.Status = 'Sucess'; 
			    responseElm.ErrorMessage = new Sony_MiddlewareUpdateconsumer_Final.ErrorType();
			    responseElm.ErrorMessage.ErrorCode = '10';
			    responseElm.ErrorMessage.ErrorMessage = 'Test Custom Error';
			    responseElm.ErrorMessage.Description = 'test desc';
	       		response.put('response_x', responseElm); 
	   		   }
    }
    
    global class WebServiceMockRefreshConsumer implements WebServiceMock {
	   global void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,	
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	      		Sony_MiddlewareConsumerRefresh_Final.PSConsumerRefresh_Output_element responseElm = 
	      		  	new Sony_MiddlewareConsumerRefresh_Final.PSConsumerRefresh_Output_element();
	       		
	       		
	       		  Sony_MiddlewareConsumerRefresh_Final.Account_mdm acc_mdm = new Sony_MiddlewareConsumerRefresh_Final.Account_mdm();
				  acc_mdm.AccountStatus = 'Test';
				  acc_mdm.AccountSuspendDate = '1/2/2013'; 
				  acc_mdm.AccountSuspendReason = 'Test Reason';
				  acc_mdm.CreatedDate = '9/2/2013';
				  acc_mdm.MDMAccountRowId = '1-1-3-3';
			      
			      
			      Sony_MiddlewareConsumerRefresh_Final.Address_mdm add_mdm = new Sony_MiddlewareConsumerRefresh_Final.Address_mdm();
			      add_mdm.City = 'City';
			      add_mdm.State = 'State';
			      add_mdm.Country = 'Country';
			      
			      Sony_MiddlewareConsumerRefresh_Final.ListOfAddress_mdm ladd_mdm = new Sony_MiddlewareConsumerRefresh_Final.ListOfAddress_mdm();
			      ladd_mdm.Address = new list<Sony_MiddlewareConsumerRefresh_Final.Address_mdm>{add_mdm}; 
			      
			      Sony_MiddlewareConsumerRefresh_Final.Contact_mdm cnt_mdm = new Sony_MiddlewareConsumerRefresh_Final.Contact_mdm();
			      cnt_mdm.BirthDate = '1/1/2013';
			      cnt_mdm.CRMPhone = '111111';
			      cnt_mdm.FirstName = 'FirstName';
			      cnt_mdm.LastName= 'LastName';
				  cnt_mdm.MDMRowId = '1-2-3-4';
				  cnt_mdm.PSNSignInId = 'psn@test.com';
				  cnt_mdm.Account = new list<Sony_MiddlewareConsumerRefresh_Final.Account_mdm>{acc_mdm};
				  cnt_mdm.ListOfAddress = ladd_mdm;
				  
				  Sony_MiddlewareConsumerRefresh_Final.ListOfContact_mdm lCnt = new Sony_MiddlewareConsumerRefresh_Final.ListOfContact_mdm();
				  lCnt.Contact = new list<Sony_MiddlewareConsumerRefresh_Final.Contact_mdm>{cnt_mdm};
				  
				  responseElm.ListOfContact = lCnt;
				       		 
	       		  response.put('response_x', responseElm); 
	   		   }
    }
    
    global class WebServiceMockCreateAddress implements WebServiceMock {
	   global void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,	
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	      		Sony_MiddlewareUpdateInsertAddress2.PSUpdateInsertAddress_Output_element responseElm = 
	      		  	new Sony_MiddlewareUpdateInsertAddress2.PSUpdateInsertAddress_Output_element();
	       		
	       		
	       		    Sony_MiddlewareUpdateInsertAddress2.Address add =  new Sony_MiddlewareUpdateInsertAddress2.Address();
					add.city='test';
					add.state='test';
					
					//prepare list of addresses
					
					Sony_MiddlewareUpdateInsertAddress2.ListOfAddress ladd = new Sony_MiddlewareUpdateInsertAddress2.ListOfAddress();
					ladd.Address = new list<Sony_MiddlewareUpdateInsertAddress2.Address>{add};
					
					//Assing that address to contact
					
					Sony_MiddlewareUpdateInsertAddress2.Contact cnt = new Sony_MiddlewareUpdateInsertAddress2.Contact();
					cnt.MDMRowId = '1-4EB-1';
					cnt.ListOfAddress = ladd;
					
					//Create list of contact
					
					Sony_MiddlewareUpdateInsertAddress2.ListOfContact lcnt = new Sony_MiddlewareUpdateInsertAddress2.ListOfContact();
					lcnt.Contact = new list<Sony_MiddlewareUpdateInsertAddress2.Contact>{cnt};
				  
				  responseElm.ListOfContact = lcnt;
				       		 
	       		  response.put('response_x', responseElm); 
	   		   }
    }
    
    global class WebServiceMockUpdateAsset implements WebServiceMock {
	   global void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,	
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	      		Sony_MiddlewareUpdateAccountAsset.PSUpdateAcntAsset_Output_element responseElm = 
	      		  	new Sony_MiddlewareUpdateAccountAsset.PSUpdateAcntAsset_Output_element();
	       		
	       		
	       		    Sony_MiddlewareUpdateAccountAsset.Asset_mdm  ast = new Sony_MiddlewareUpdateAccountAsset.Asset_mdm();
					ast.AssetRowId = '1';
					ast.SerialNumber = '1234';
					ast.ProductName = 'testing';
					
					//creating list of assets
					
					Sony_MiddlewareUpdateAccountAsset.ListOfAsset_mdm   last = new Sony_MiddlewareUpdateAccountAsset.ListOfAsset_mdm ();
					last.Asset = new list<Sony_MiddlewareUpdateAccountAsset.Asset_mdm>{ast};
					
					//associating assets with account.
					
					Sony_MiddlewareUpdateAccountAsset.Account_mdm   acc = new Sony_MiddlewareUpdateAccountAsset.Account_mdm ();
					acc.MDMAccountRowId = '1234567';
					acc.ListOfAsset = last;
					
					//creating list of list of accounts
					
					Sony_MiddlewareUpdateAccountAsset.ListOfAccount_mdm    lacc = new Sony_MiddlewareUpdateAccountAsset.ListOfAccount_mdm  ();
					lacc.Account  = new list<Sony_MiddlewareUpdateAccountAsset.Account_mdm>{acc};
				  
				  responseElm.ListOfAccount = lacc;
				       		 
	       		  response.put('response_x', responseElm); 
	   		   }
    }
    
}