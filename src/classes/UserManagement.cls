public with sharing class UserManagement{

public static string CSTierProfile='';
public static string CSTierPG = '';

public static void AfterInsert(List<User> userList){
    for(User u : userList){
    Set<Id> profileids = new Set<Id>();
    if (u.profileId!=null){
    profileIds.add(u.profileId);
    }
        for(Profile p : [Select Id, Name FROM Profile WHERE Id in : profileids]){
            CSTierProfile = p.Name ;
            if(CSTierProfile == 'CS-Tier 1'){
                CSTierPG ='CS_Tier1';
            }else if(CSTierProfile == 'CS-Tier 2') {
                CSTierPG ='CS_Tier2';
            }else if(CSTierProfile == 'CS-Tier 3') {
                CSTierPG ='CS_Tier3';
            } else if(CSTierProfile == 'CS-Bureau Supervisor') {
                CSTierPG ='CS_Management';         
            } else if(CSTierProfile == 'CS-Read Only') {
                CSTierPG ='Can_View_CS_Reports_All';
            } else if(CSTierProfile == 'CS-Reporting Analyst') {
                CSTierPG ='CS_Reporting_Analytics';
            } else if(CSTierProfile == 'CS-Supervisor') {
                CSTierPG ='CS_Management';
            } else if(CSTierProfile == 'System Administrator') {
                CSTierPG ='Systems_Administrators';
            }

            for(Group g : [ Select ID, Name FROM Group WHERE Name Like : CSTierPG]){
                   Groupmember gm = new GroupMember();
                   gm.UserOrGroupId = u.id;
                   gm.GroupId = g.id;
                   insert gm;
             }
        }
    
    }
}



public static void AfterUpdate(List<User> userList,Map<id,User> oldUserMap){
  
  
  User oldUser;

  for(User u : userList){
    Set<Id> profileids = new Set<Id>();
    Set<ID> oldProfiledids = new Set<Id>();
    Set<Id> oldUserOrGroupId= new Set<Id>();
    Set<Id> oldGroupId= new Set<Id>();

    oldUser = oldUserMap.get(u.Id);
    if (u.profileId!=null && u.profileId != oldUser.profileId){
    profileIds.add(u.profileId);
    oldProfiledids.add(oldUser.profileId);
    }
    
        for(Profile p : [Select Id, Name FROM Profile WHERE Id in : profileids]){
            CSTierProfile = p.Name ;
            if(CSTierProfile == 'CS-Tier 1'){
                CSTierPG ='CS_Tier1';
            }else if(CSTierProfile == 'CS-Tier 2') {
                CSTierPG ='CS_Tier2';
            }else if(CSTierProfile == 'CS-Tier 3') {
                CSTierPG ='CS_Tier3';
            } else if(CSTierProfile == 'CS-Bureau Supervisor') {
                CSTierPG ='CS_Management';         
            } else if(CSTierProfile == 'CS-Read Only') {
                CSTierPG ='Can_View_CS_Reports_All';
            } else if(CSTierProfile == 'CS-Reporting Analyst') {
                CSTierPG ='CS_Reporting_Analytics';
            } else if(CSTierProfile == 'CS-Supervisor') {
                CSTierPG ='CS_Management';
            } else if(CSTierProfile == 'System Administrator') {
                CSTierPG ='Systems_Administrators';
            }

            for(Group g : [ Select ID, Name FROM Group WHERE Name Like : CSTierPG]){
                   Groupmember gm = new GroupMember();
                   gm.UserOrGroupId = u.id;
                   gm.GroupId = g.id;
                   insert gm;
             }
        }



for(Profile p : [Select Id, Name FROM Profile WHERE Id in : oldProfiledids]){
            CSTierProfile = p.Name ;
            if(CSTierProfile == 'CS-Tier 1'){
                CSTierPG ='CS_Tier1';
            }else if(CSTierProfile == 'CS-Tier 2') {
                CSTierPG ='CS_Tier2';
            }else if(CSTierProfile == 'CS-Tier 3') {
                CSTierPG ='CS_Tier3';
            } else if(CSTierProfile == 'CS-Bureau Supervisor') {
                CSTierPG ='CS_Management';         
            } else if(CSTierProfile == 'CS-Read Only') {
                CSTierPG ='Can_View_CS_Reports_All';
            } else if(CSTierProfile == 'CS-Reporting Analyst') {
                CSTierPG ='CS_Reporting_Analytics';
            } else if(CSTierProfile == 'CS-Supervisor') {
                CSTierPG ='CS_Management';
            } else if(CSTierProfile == 'System Administrator') {
                CSTierPG ='Systems_Administrators';
            }

            for(Group g : [ Select ID, Name FROM Group WHERE Name Like : CSTierPG]){
                   Groupmember gm = new GroupMember();
                   oldUserOrGroupId.add(u.id);
                   oldGroupId.add(g.id);
                   List<GroupMember> existingGm = [SELECT Id From GroupMember where UserOrGroupId in: oldUserOrGroupId and GroupId in :oldGroupId];
                   delete existingGm ;

             }
        }


    
    }


}

}