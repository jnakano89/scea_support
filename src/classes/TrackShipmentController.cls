//8/1/2013 : UV : created this class
public class TrackShipmentController {
	
  public String iframeSrc{get;set;}
  public String title{get;set;}
  public String subtitle{get;set;}
  
  String caseId;
  final String USPS = 'USPS';
  final String FED_EX = 'FedEx';
  final String PUROLATOR = 'Purolator';
  
  final String USPS_TRACKING_SRC = Label.USPS_TRACKING_SRC;
  final String FEDEX_TRACKING_SRC = 'https://www.fedex.com/fedextrack/?tracknumbers=<TRACKING-NUMBER>&cntry_code=us';
  final String PUROLATOR_TRACKING_SRC = 'https://eshiponline.purolator.com/ShipOnline/Public/Track/TrackingDetails.aspx?pup=N&pin=<TRACKING-NUMBER>';
  
  public TrackShipmentController() {
  	
  	Map<String, String> urlParamMap = Apexpages.currentPage().getParameters();
  	String trackingNumber = '';
  	String carrier = '';
  	if(urlParamMap.containsKey('CaseId')) {
  		caseId = urlParamMap.get('CaseId');
  	}
  	if(urlParamMap.containsKey('TrackingNumber')) {
  		trackingNumber = urlParamMap.get('TrackingNumber');
  	}
  	if(urlParamMap.containsKey('Carrier')) {
  		carrier = urlParamMap.get('Carrier');
  	}
  	
  	subtitle = trackingNumber;
  	
  	if(carrier == USPS){
  		title = USPS;
  		iframeSrc = USPS_TRACKING_SRC.replace('<TRACKING-NUMBER>',trackingNumber);
  	} else if (carrier == FED_EX) {
  		title = FED_EX;
  		iframeSrc = FEDEX_TRACKING_SRC.replace('<TRACKING-NUMBER>',trackingNumber);
  	} else if(carrier == PUROLATOR){
  		title = PUROLATOR;
  		iframeSrc = PUROLATOR_TRACKING_SRC.replace('<TRACKING-NUMBER>',trackingNumber);
  	}
  }
  
  public Pagereference returnToCase() {
  	return new Pagereference('/' + caseId);
  }
}