trigger UserTrigger on User (after insert, after update) 
{
  
  if(trigger.isAfter){
  
    if(trigger.isInsert){
  
      UserManagement.AfterInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
      UserManagement.AfterUpdate(Trigger.new, Trigger.oldMap);
 
        
        }//end-if  
        
  }//end-if  
}