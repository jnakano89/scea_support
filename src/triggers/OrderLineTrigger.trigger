trigger OrderLineTrigger on Order_Line__c (after insert, after update, before insert, 
before update) {
if(!CustomSettingLoader.DATA_MIGRATION_SETTING ){
    if(trigger.isBefore){

        if(trigger.isInsert){
  
            OrderLineManagement.beforeInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
            OrderLineManagement.beforeUpdate(Trigger.new, Trigger.oldMap);
 
        }//end-if  
        
    }else if(trigger.isAfter){
        if(trigger.isInsert){
  
            OrderLineManagement.afterInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
            OrderLineManagement.afterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}}