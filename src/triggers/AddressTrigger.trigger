trigger AddressTrigger on Address__c (after insert, after update, before insert, 
before update) {

if(!CustomSettingLoader.DATA_MIGRATION_SETTING ){
    if(trigger.isBefore){

        if(trigger.isInsert){
  
            AddressManagement.beforeInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
            AddressManagement.beforeUpdate(Trigger.new, Trigger.oldMap);
 
        
        }//end-if  
        
    }//end-if
    
    if(trigger.isAfter){

        if(trigger.isInsert){
            AddressManagement.afterInsert(Trigger.new);
        }
        else if(trigger.isUpdate){
            AddressManagement.afterUpdate(Trigger.new, Trigger.oldMap);
        }
        
    }
 }
}