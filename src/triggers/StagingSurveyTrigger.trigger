trigger StagingSurveyTrigger on Staging_Survey__c (after insert, after update, before insert, 
before update) {
if(!CustomSettingLoader.DATA_MIGRATION_SETTING ){
    if(trigger.isAfter){
        
        if(trigger.isInsert){
  
            StagingSurveyManagement.afterInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
            StagingSurveyManagement.afterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
  }

}