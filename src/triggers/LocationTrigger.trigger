trigger LocationTrigger on Location__c (after insert, before update) {
   if(!CustomSettingLoader.DATA_MIGRATION_SETTING ){
    if(trigger.isAfter && trigger.isInsert) {
      //LocationTriggerHandler.afterInsert(trigger.new);
    }
    
    if(trigger.isUpdate && Trigger.isBefore) {
        LocationTriggerHandler.beforeUpdate(trigger.new, trigger.oldMap);
    }
}}