trigger CaseTrigger on Case (after insert, after update, before insert, 
before update) {
   if(!CustomSettingLoader.DATA_MIGRATION_SETTING ){ 
    if(trigger.isBefore){
  
        if(trigger.isInsert){
  
            CaseManagement.beforeInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
            CaseManagement.beforeUpdate(Trigger.new, Trigger.oldMap, trigger.newMap);
 
        
        }//end-if  
        
    } else if(trigger.isAfter){
  
        if(trigger.isInsert){
  
            CaseManagement.AfterInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
            CaseManagement.AfterUpdate(Trigger.new, Trigger.oldMap);
 
        
        }//end-if  
        
    }//end-if   
}}