trigger EmailMessageTrigger on EmailMessage (after insert, after update, before insert, 
before update) {
	if(trigger.isBefore){

		if(trigger.isInsert){
  
			EmailMessageManagement.beforeInsert(Trigger.new);
        
        }  
        
	}
}