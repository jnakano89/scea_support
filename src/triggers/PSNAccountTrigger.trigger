trigger PSNAccountTrigger on PSN_Account__c (after insert, after update, before insert, 
before update) {
if(!CustomSettingLoader.DATA_MIGRATION_SETTING ){
    if(trigger.isBefore){

        if(trigger.isInsert){
  
            PSNAccountManagement.beforeInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
    
            PSNAccountManagement.beforeUpdate(Trigger.new, Trigger.oldMap);
 
        
        }//end-if  
        
    }
    if(trigger.isAfter){
        if(trigger.isInsert){
  
            PSNAccountManagement.afterInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
            PSNAccountManagement.afterUpdate(Trigger.new, Trigger.oldMap);
        }
    }

}

}