trigger StagingContactTrigger on Staging_Contact__c (after insert, after update, before insert, 
before update) {
if(!CustomSettingLoader.DATA_MIGRATION_SETTING ){
    if(trigger.isBefore){

        if(trigger.isInsert){
  
            StagingContactManagement.beforeInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
            StagingContactManagement.beforeUpdate(Trigger.new, Trigger.oldMap);
 
        
        }//end-if  
        
    }
    if(trigger.isAfter){

        if(trigger.isInsert){
  
            StagingContactManagement.afterInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
            StagingContactManagement.beforeUpdate(Trigger.new, Trigger.oldMap);
 
        
        }//end-if  
        
    }
}}