trigger StagingAssetTrigger on Staging_Asset__c (after insert, after update, before insert, 
before update) {

if(!CustomSettingLoader.DATA_MIGRATION_SETTING ){
    if(trigger.isBefore){

        if(trigger.isInsert){
  
            StagingAssetManagement.beforeInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
            StagingAssetManagement.beforeUpdate(Trigger.new, Trigger.oldMap);
 
        
        }//end-if  
        
    }//end-if
    if(trigger.isAfter){

        if(trigger.isInsert){
  
            StagingAssetManagement.afterInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
            StagingAssetManagement.afterUpdate(Trigger.new, Trigger.oldMap);
 
        
        }//end-if  
        
    }
}}