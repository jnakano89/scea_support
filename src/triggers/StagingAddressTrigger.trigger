trigger StagingAddressTrigger on Staging_Address__c (after insert, after update, before insert, 
before update) {

if(!CustomSettingLoader.DATA_MIGRATION_SETTING ){
    if(trigger.isBefore){

        if(trigger.isInsert){
  
            StagingAddressManagement.beforeInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
            StagingAddressManagement.beforeUpdate(Trigger.new, Trigger.oldMap);
 
        
        }//end-if  
        
    }//end-if
    if(trigger.isAfter){

        if(trigger.isInsert){
  
            StagingAddressManagement.afterInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
            StagingAddressManagement.afterUpdate(Trigger.new, Trigger.oldMap);
 
        
        }//end-if  
        
    }
  }
}