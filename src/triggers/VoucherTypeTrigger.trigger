trigger VoucherTypeTrigger on Voucher__c (after update) {
    if(trigger.isAfter){
        if(trigger.isUpdate){
            VoucherManagement.UpdateCaseAttributes(Trigger.new);
        }
    }
}