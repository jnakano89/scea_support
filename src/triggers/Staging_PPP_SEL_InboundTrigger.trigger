trigger Staging_PPP_SEL_InboundTrigger on Staging_PPP_SEL_Inbound__c (after insert) {
  if(!CustomSettingLoader.DATA_MIGRATION_SETTING ){
  if( (Trigger.isInsert) && Trigger.isAfter){
       List <CronTrigger> Lopscheduledjobs= new List <CronTrigger> ();
       
       Lopscheduledjobs=[SELECT Id, CronExpression, TimeZoneSidKey, OwnerId,  CreatedById, PreviousFireTime, State, StartTime, EndTime, TimesTriggered from CronTrigger where OwnerId=:Userinfo.getUserId() AND (State !='Completed' AND State !='Deleted' AND State !='Aborted')];
       
       System.Debug('@@@@@@@@@@@@@@@@Lopscheduledjobs'+Lopscheduledjobs);
       
     if(Lopscheduledjobs.size()==0){ 
        Datetime sysTime = System.now();
        String chron_exp = '';
        sysTime = sysTime.addSeconds(60);
        chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        system.debug(chron_exp);
        SELInboundScheduler m = new SELInboundScheduler();
        // String sch = '0 0,30 * * * *';
        System.schedule('SEL Inbound Job ' + sysTime.getTime(), chron_exp, m);
    }   
 }}
}