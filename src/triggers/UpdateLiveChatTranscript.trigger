trigger UpdateLiveChatTranscript on LiveChatTranscript (before insert) {
	String chatKeyTemp = '';
	//ChatTranscriptTempMapping__c cttm;
	List<ChatTranscriptTempMapping__c> cttmList = new List<ChatTranscriptTempMapping__c>();
	if(trigger.isBefore){			
		for(LiveChatTranscript lct : Trigger.new){
			chatKeyTemp = lct.ChatKey;
			//System.debug('****'+chatKeyTemp+'***');
			cttmList = [Select  CaseRefId__c, ChatKeyRefId__c, ContactRefId__c from ChatTranscriptTempMapping__c  where ChatKeyRefId__c = :chatKeyTemp  order by LastModifiedDate ASC Limit 1];
			if(!cttmList.isEmpty()){
				lct.CaseId = cttmList.get(0).CaseRefId__c;
				lct.ContactId = cttmList.get(0).ContactRefId__c;
				cttmList = [Select  CaseRefId__c, ChatKeyRefId__c, ContactRefId__c from ChatTranscriptTempMapping__c  where ChatKeyRefId__c = :chatKeyTemp];
				for(ChatTranscriptTempMapping__c cttmRec : cttmList){
					delete cttmRec;
				}			
			}			
		}				
	}
}