trigger OrderTrigger on Order__c (after insert, after update) {
	
	  if( Trigger.isAfter && Trigger.isInsert && !CustomSettingLoader.DATA_MIGRATION_SETTING ){
	  
	   OrderHelper.afterInsert(trigger.newMap);
	  }
	
	 if( Trigger.isAfter && Trigger.isUpdate && !CustomSettingLoader.DATA_MIGRATION_SETTING ){
      
        Orderhelper.afterUpdate(trigger.newMap, trigger.oldMap);
      
    }     

}