trigger StagingPPPOutboundTrigger on Staging_PPP_Outbound__c (before insert) {
   if(!CustomSettingLoader.DATA_MIGRATION_SETTING ){
    if(trigger.isBefore && trigger.isInsert) {
        StagingPPPOutboundManagement.beforeInsert(trigger.new); 
    }
   } 
}