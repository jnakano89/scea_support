/******************************************************************************
Trigger       : Siebel_CaseTrigger
Description   : Trigger called on before insert or before update of Case
Developed by  : Rahul Mittal
Date          : March 25, 2013              
******************************************************************************/
trigger Siebel_CaseTrigger on Case (before insert, before update) {
    if(!CustomSettingLoader.DATA_MIGRATION_SETTING ){
    if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
        
        Siebel_CaseTrigger_AC.updateCurrCaseOwner(trigger.new, trigger.oldMap);
    }
   } 
}