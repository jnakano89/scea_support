trigger UpdateLiveAgentSession on LiveAgentSession (Before Insert) {
    List<Id> LiveAgentSessionUserid = New List<Id>();
    Map<id,LiveAgentSession> NewMapLAS = New Map<Id,LiveAgentSession>();
    List<LiveAgentSession> ListAgent = New List<LiveAgentSession>();
    For(LiveAgentSession Las:Trigger.New){
        LiveAgentSessionUserid.add(las.agentId);
        NewMapLAS.put(las.agentId,las);
        ListAgent.add(las);
    }
    
    List<Tracking_Agent_Session_time__c> Track = New List<Tracking_Agent_Session_Time__C>();
    Map<id,Tracking_Agent_Session_time__c> Tas = New Map<Id,Tracking_Agent_Session_time__c>();
    Datetime CurrentTime =  System.now();
    Long tempTime = 0;
    
    Track = [Select Admin__c,Break__c,Meal__c,BillableTraining__c,ManagerApproved__C,AgentId__c,StatusOptionType__c,Id,ManagerApprovedTime__c from Tracking_Agent_Session_Time__c  where AgentId__c In:LiveAgentSessionUserid  order by LastModifiedDate DESC Limit 1];     
    for(Tracking_Agent_Session_time__c task:Track){         
            if(String.isNotBlank(task.StatusOptionType__c)){
                tempTime = CurrentTime.getTime() - task.ManagerApprovedTime__c.getTime();                        
                if(task.StatusOptionType__c == 'Billable Training'){                       
                    task.BillableTraining__c = task.BillableTraining__c + (tempTime.intValue()/1000);
                }
                if(task.StatusOptionType__c == 'Manager Approved'){                                    
                    task.ManagerApproved__c = task.ManagerApproved__c + (tempTime.intValue()/1000); 
                }
                if(task.StatusOptionType__c == 'Admin'){
                    task.Admin__c = task.Admin__c + (tempTime.intValue()/1000);            
                }
                if(task.StatusOptionType__c == 'Break'){
                    task.Break__c = task.Break__c + (tempTime.intValue()/1000);            
                }
                if(task.StatusOptionType__c == 'Meal'){
                    task.Meal__c = task.Meal__c + (tempTime.intValue()/1000);            
                }                
            }           
        tas.put(task.AgentId__c,task);
    }
    System.debug('****'+tas.size()+'***8');
    if(tas.size() > 0){
        for(LiveAgentSession last:Trigger.new){             
        last.TimeInBillableTraining__c = tas.get(last.AgentId).BillableTraining__c;
        last.TimeInManagerApproved__c = tas.get(last.AgentId).ManagerApproved__c;
        last.TimeinAdmin__c = tas.get(last.AgentId).Admin__c;
        last.TimeinBreak__c = tas.get(last.AgentId).Break__c;
        last.TimeinMeal__c = tas.get(last.AgentId).Meal__c;
        last.TimeinAwayDiff__c = last.TimeInAwayStatus - (last.TimeInBillableTraining__c + 
            last.TimeInManagerApproved__c + last.TimeinAdmin__c + last.TimeinBreak__c + last.TimeinMeal__c);
    }
    }        
    
    delete Track;
    
}