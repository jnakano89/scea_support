trigger AssetTrigger on Asset__c (after insert, after update, before insert, 
before update) {

if(!CustomSettingLoader.DATA_MIGRATION_SETTING ){
    if(trigger.isBefore){

        if(trigger.isInsert){
  
            AssetManagement.beforeInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
           AssetManagement.beforeUpdate(Trigger.new, Trigger.oldMap);
 
        
        }
    }else if(trigger.isAfter){
        if(trigger.isInsert){
  
            AssetManagement.afterInsert(Trigger.new);
        
        }else if(trigger.isUpdate){
            AssetManagement.afterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}}