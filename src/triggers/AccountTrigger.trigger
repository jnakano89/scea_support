trigger AccountTrigger on Account (after insert, after update, before insert, 
before update) {
    
  if(!CustomSettingLoader.DATA_MIGRATION_SETTING ){
    if(trigger.isBefore){

        if(trigger.isInsert){
  
            AccountManagement.beforeInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
            AccountManagement.beforeUpdate(Trigger.new, Trigger.oldMap);
 
        
        }//end-if  
        
    }else if(trigger.isAfter){
        
        if(trigger.isInsert){
  
            AccountManagement.afterInsert(Trigger.new);
        
        } else if(trigger.isUpdate){
 
            AccountManagement.afterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
 }
}