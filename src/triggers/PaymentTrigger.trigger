trigger PaymentTrigger on Payment__c (after update, after insert) {
   public static string PAYMENT_STATUS_CHARGED = 'Charged';

   if(!CustomSettingLoader.DATA_MIGRATION_SETTING ){ 
    Set<Id> orderIds = new Set<Id>();
    list<Order__c> orderList = new list<Order__c>();
    Map <id, id> OrderPaymentMap = new  Map <id, id>();
    
    
    
    for(Payment__c payment : trigger.new) {
       
       OrderPaymentMap.put( payment.Order__c, payment.id);
       
        orderIds.add(Payment.Order__c);
        
        
    }
    
    
    for(Order__c o : [select id, Payment__c, Asset__r.PPP_Status__c, Last_Modified_Date_Time__c  from Order__c where Id IN : orderIds]) {
        
       if(o.Asset__r.PPP_Status__c!='Pending Cancel' || (o.Payment__c != (OrderPaymentMap.get(o.Id)))){
        o.payment__c=trigger.newmap.get(OrderPaymentMap.get(o.Id)).id;
        
         orderList.add(o);
         System.debug('>>>>>>+o.payment__c<<<<<<<<<' +o.payment__c);
         System.debug('>>>>>>+orderlist<<<<<<<<<' +orderlist);
       }
    }
    
    if(!orderList.isEmpty()) {OrderHelper.processOutbound=true;update orderList;}

    if(Trigger.isInsert){
      //if payment status = charged, ppp eligible should = true
      //BLEBOFF 2/13/14
      List<Asset__c> assets = new List<Asset__c>();
      for(Payment__c payment : Trigger.new){
        Order__c order;
        if(payment.order__c <> null){
          order = [select id, asset__c, Payment_Status__c from Order__c where id =: payment.order__c];
        }
        
        if(order <> null && order.asset__c <> null && order.payment_Status__c == PAYMENT_STATUS_CHARGED){
          Asset__c asset = new Asset__c(id = order.asset__c);
          asset.PPP_Refund_Eligible__c = true;
          assets.add(asset);
        }
      }
      update assets;
    }
}}